<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512111840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD statut_direction VARCHAR(255) NOT NULL, ADD fonction LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', CHANGE role statut_encadrement VARCHAR(255) NOT NULL, CHANGE psc1 statut_assistant_sanitaire TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD role VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP statut_encadrement, DROP statut_direction, DROP fonction, CHANGE statut_assistant_sanitaire psc1 TINYINT(1) NOT NULL');
    }
}
