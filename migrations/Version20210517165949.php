<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517165949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE qualification_direction (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, qualite_direction VARCHAR(50) DEFAULT NULL, diplomes_direction LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', equivalence_diplome_direction VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_581ACA5E84CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE qualification_direction ADD CONSTRAINT FK_581ACA5E84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE qualification_direction');
    }
}
