<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210513095840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE infos_encadrant_non_eedf (id INT AUTO_INCREMENT NOT NULL, doc_consentement_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, telephone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', departement_ou_pays_naissance VARCHAR(255) DEFAULT NULL, commune_naissance VARCHAR(255) DEFAULT NULL, genre VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1E26A2E33B16344 (doc_consentement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE infos_encadrant_non_eedf ADD CONSTRAINT FK_1E26A2E33B16344 FOREIGN KEY (doc_consentement_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D57813FEF1BA4');
        $this->addSql('DROP INDEX IDX_E8D57813FEF1BA4 ON role_encadrant_sejour');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD infos_encadrant_non_eedf_id INT DEFAULT NULL, ADD est_encadrant_eedf TINYINT(1) NOT NULL, CHANGE encadrant_id encadrant_eedf_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D578136B84926 FOREIGN KEY (encadrant_eedf_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D578138584AEA2 FOREIGN KEY (infos_encadrant_non_eedf_id) REFERENCES infos_encadrant_non_eedf (id)');
        $this->addSql('CREATE INDEX IDX_E8D578136B84926 ON role_encadrant_sejour (encadrant_eedf_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E8D578138584AEA2 ON role_encadrant_sejour (infos_encadrant_non_eedf_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578138584AEA2');
        $this->addSql('DROP TABLE infos_encadrant_non_eedf');
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578136B84926');
        $this->addSql('DROP INDEX IDX_E8D578136B84926 ON role_encadrant_sejour');
        $this->addSql('DROP INDEX UNIQ_E8D578138584AEA2 ON role_encadrant_sejour');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD encadrant_id INT DEFAULT NULL, DROP encadrant_eedf_id, DROP infos_encadrant_non_eedf_id, DROP est_encadrant_eedf');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D57813FEF1BA4 FOREIGN KEY (encadrant_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_E8D57813FEF1BA4 ON role_encadrant_sejour (encadrant_id)');
    }
}
