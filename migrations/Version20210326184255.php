<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326184255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE effectifs_previsionnels (id INT AUTO_INCREMENT NOT NULL, nb_lutins INT NOT NULL, nb_louveteaux INT NOT NULL, nb_ecles INT NOT NULL, nb_aines INT NOT NULL, nb_encadrants_lutins INT NOT NULL, nb_encadrants_louveteaux INT NOT NULL, nb_encadrants_ecles INT NOT NULL, nb_encadrants_aines INT NOT NULL, nb_handicap INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_structure (sejour_id INT NOT NULL, structure_id INT NOT NULL, INDEX IDX_C7750E5884CF0CF (sejour_id), INDEX IDX_C7750E582534008B (structure_id), PRIMARY KEY(sejour_id, structure_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sejour_structure ADD CONSTRAINT FK_C7750E5884CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_structure ADD CONSTRAINT FK_C7750E582534008B FOREIGN KEY (structure_id) REFERENCES structure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour ADD effectifs_previsionnels_id INT NULL, ADD date_debut DATETIME DEFAULT NULL, ADD date_fin DATETIME DEFAULT NULL, ADD structures_regroupement_non_eedf LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F5202897D28BC9 FOREIGN KEY (effectifs_previsionnels_id) REFERENCES effectifs_previsionnels (id)');
       
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F5202897D28BC9 ON sejour (effectifs_previsionnels_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F5202897D28BC9');
        $this->addSql('DROP TABLE effectifs_previsionnels');
        $this->addSql('DROP TABLE sejour_structure');
        $this->addSql('DROP INDEX UNIQ_96F5202897D28BC9 ON sejour');
        $this->addSql('ALTER TABLE sejour DROP effectifs_previsionnels_id, DROP date_debut, DROP date_fin, DROP structures_regroupement_non_eedf');
    }
}
