<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220420130820 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fiche complémentaire';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour ADD fiche_complementaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028A9699A9 FOREIGN KEY (fiche_complementaire_id) REFERENCES document (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F52028A9699A9 ON sejour (fiche_complementaire_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028A9699A9');
        $this->addSql('DROP INDEX UNIQ_96F52028A9699A9 ON sejour');
        $this->addSql('ALTER TABLE sejour DROP fiche_complementaire_id');
    }
}
