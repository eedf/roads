<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324110923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent ADD nom_urgence VARCHAR(200) DEFAULT NULL, ADD prenom_urgence VARCHAR(200) DEFAULT NULL, ADD tel_urgence VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', ADD lien_urgence VARCHAR(200) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP nom_urgence, DROP prenom_urgence, DROP tel_urgence, DROP lien_urgence');
    }
}
