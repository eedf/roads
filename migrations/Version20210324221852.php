<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324221852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_formation_adherent (id INT AUTO_INCREMENT NOT NULL, document_id INT NOT NULL, date_modification DATETIME NOT NULL, nom_diplome VARCHAR(255) NOT NULL, statut VARCHAR(255) DEFAULT NULL, precisions VARCHAR(500) NOT NULL, UNIQUE INDEX UNIQ_5B9F0078C33F7837 (document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE document_formation_adherent ADD CONSTRAINT FK_5B9F0078C33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE document_formation_adherent');
    }
}
