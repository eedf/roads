<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326151714 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour ADD accompagnant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F520288A141997 FOREIGN KEY (accompagnant_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_96F520288A141997 ON sejour (accompagnant_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520288A141997');
        $this->addSql('DROP INDEX IDX_96F520288A141997 ON sejour');
        $this->addSql('ALTER TABLE sejour DROP accompagnant_id');
    }
}
