<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210327003352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE lieu_declaration_intention (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, defini TINYINT(1) NOT NULL, nom_lieu VARCHAR(255) DEFAULT NULL, type_lieu VARCHAR(50) NOT NULL, INDEX IDX_C804825D84CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD CONSTRAINT FK_C804825D84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE lieu_declaration_intention');
    }
}
