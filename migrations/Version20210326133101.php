<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326133101 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sejout (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sejour ADD type_sejour VARCHAR(50) DEFAULT NULL, ADD sejour_ouvert TINYINT(1) DEFAULT NULL, ADD sejour_itinerant TINYINT(1) DEFAULT NULL, ADD sejour_france_avec_accueil_etranger TINYINT(1) DEFAULT NULL, ADD colo_apprenante TINYINT(1) DEFAULT NULL, ADD dispositif_appui VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE sejout');
        $this->addSql('ALTER TABLE sejour DROP type_sejour, DROP sejour_ouvert, DROP sejour_itinerant, DROP sejour_france_avec_accueil_etranger, DROP colo_apprenante, DROP dispositif_appui');
    }
}
