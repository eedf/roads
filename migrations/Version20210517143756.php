<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517143756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE document SET updated_at = NOW() WHERE updated_at IS NULL');
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD diplomes_animation LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', ADD equivalence_diplome_animation VARCHAR(255) DEFAULT NULL, ADD qualite_animation VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP diplomes_animation, DROP equivalence_diplome_animation, DROP qualite_animation');
    }
}
