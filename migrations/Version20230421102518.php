<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Kernel;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230421102518 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $env = 'prod';
        $kernel = new Kernel($env, false);
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'roads:migration_nouveau_modele',
            '--env' => 'prod',
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();



        // this up() migration is auto-generated, please modify it to your needs
        /*$this->addSql('ALTER TABLE participation_sejour_unite DROP FOREIGN KEY FK_C91D9C0CEC4A74AB');
        $this->addSql('CREATE TABLE adhesion (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, adherent_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, date_modification DATETIME NOT NULL, annule TINYINT(1) NOT NULL, id_jeito BIGINT NOT NULL, INDEX IDX_C50CA65A2534008B (structure_id), INDEX IDX_C50CA65A25F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contrat_travail (id INT AUTO_INCREMENT NOT NULL, employe_id INT NOT NULL, date_debut DATETIME NOT NULL, date_modification DATETIME NOT NULL, date_fin DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, nom VARCHAR(255) DEFAULT NULL, id_jeito BIGINT NOT NULL, INDEX IDX_394729CD1B65292 (employe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employe (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, id_jeito BIGINT NOT NULL, date_modification DATETIME NOT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', matricule VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_F804D3B9A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipe (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, date_creation DATETIME DEFAULT NULL, date_modification DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, date_desactivation DATETIME DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, id_jeito BIGINT NOT NULL, UNIQUE INDEX UNIQ_2449BA152685EE64 (id_jeito), INDEX IDX_2449BA152534008B (structure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fonction (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, equipe_id INT NOT NULL, id_jeito BIGINT DEFAULT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, statut VARCHAR(255) NOT NULL, date_modification DATETIME NOT NULL, UNIQUE INDEX UNIQ_900D5BD2685EE64 (id_jeito), INDEX IDX_900D5BDA21BD112 (personne_id), INDEX IDX_900D5BD6D861B89 (equipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participation_sejour_unite_personne (participation_sejour_unite_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_8FFB6C57D1A99552 (participation_sejour_unite_id), INDEX IDX_8FFB6C57A21BD112 (personne_id), PRIMARY KEY(participation_sejour_unite_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, coordonnees_id INT DEFAULT NULL, responsable_legal1_id INT DEFAULT NULL, responsable_legal2_id INT DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT NULL, date_derniere_connexion DATETIME DEFAULT NULL, date_modification DATETIME NOT NULL, id_jeito BIGINT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATE DEFAULT NULL, genre VARCHAR(20) NOT NULL, adresse_mail VARCHAR(50) NOT NULL, nom_urgence VARCHAR(200) DEFAULT NULL, prenom_urgence VARCHAR(200) DEFAULT NULL, tel_urgence VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', lien_urgence VARCHAR(200) DEFAULT NULL, frequence_notification VARCHAR(50) NOT NULL, droit_image TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_FCEC9EF2685EE64 (id_jeito), UNIQUE INDEX UNIQ_FCEC9EF5853DEDF (coordonnees_id), INDEX IDX_FCEC9EF39FF2EE9 (responsable_legal1_id), INDEX IDX_FCEC9EF2B4A8107 (responsable_legal2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_personne (sejour_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_1C282E4284CF0CF (sejour_id), INDEX IDX_1C282E42A21BD112 (personne_id), PRIMARY KEY(sejour_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure_regroupement_sejour_equipe (structure_regroupement_sejour_id INT NOT NULL, equipe_id INT NOT NULL, INDEX IDX_4E0E2C03AE72291D (structure_regroupement_sejour_id), INDEX IDX_4E0E2C036D861B89 (equipe_id), PRIMARY KEY(structure_regroupement_sejour_id, equipe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE contrat_travail ADD CONSTRAINT FK_394729CD1B65292 FOREIGN KEY (employe_id) REFERENCES employe (id)');
        $this->addSql('ALTER TABLE employe ADD CONSTRAINT FK_F804D3B9A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE equipe ADD CONSTRAINT FK_2449BA152534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BDA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BD6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite_personne ADD CONSTRAINT FK_8FFB6C57D1A99552 FOREIGN KEY (participation_sejour_unite_id) REFERENCES participation_sejour_unite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE participation_sejour_unite_personne ADD CONSTRAINT FK_8FFB6C57A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF5853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF39FF2EE9 FOREIGN KEY (responsable_legal1_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF2B4A8107 FOREIGN KEY (responsable_legal2_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE sejour_personne ADD CONSTRAINT FK_1C282E4284CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_personne ADD CONSTRAINT FK_1C282E42A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_equipe ADD CONSTRAINT FK_4E0E2C03AE72291D FOREIGN KEY (structure_regroupement_sejour_id) REFERENCES structure_regroupement_sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_equipe ADD CONSTRAINT FK_4E0E2C036D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A2534008B');
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A25F06C53');
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490AEC4A74AB');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent DROP FOREIGN KEY FK_10E655D8D1A99552');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent DROP FOREIGN KEY FK_10E655D825F06C53');
        $this->addSql('ALTER TABLE sejour_adherent DROP FOREIGN KEY FK_833517CD25F06C53');
        $this->addSql('ALTER TABLE sejour_adherent DROP FOREIGN KEY FK_833517CD84CF0CF');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite DROP FOREIGN KEY FK_EAD22509EC4A74AB');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite DROP FOREIGN KEY FK_EAD22509AE72291D');
        $this->addSql('ALTER TABLE unite DROP FOREIGN KEY FK_1D64C118E422675C');
        $this->addSql('DROP TABLE fonction_associative');
        $this->addSql('DROP TABLE participation_sejour_unite_adherent');
        $this->addSql('DROP TABLE sejour_adherent');
        $this->addSql('DROP TABLE structure_regroupement_sejour_unite');
        $this->addSql('DROP TABLE unite');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF');
        $this->addSql('DROP INDEX UNIQ_90D3F0605853DEDF ON adherent');
        $this->addSql('DROP INDEX UNIQ_90D3F060EC5C58C ON adherent');
        $this->addSql('DROP INDEX UNIQ_90D3F0601C706A62 ON adherent');
        $this->addSql('ALTER TABLE adherent ADD personne_id INT NOT NULL, DROP coordonnees_id, DROP coordonnees_responsable_legal1_id, DROP coordonnees_responsable_legal2_id, DROP nom, DROP prenom, DROP date_naissance, DROP droit_image, DROP genre, DROP status_adhesion, DROP adresse_mail, DROP roles, DROP is_verified, DROP date_verification_email, DROP date_derniere_connexion, DROP nom_urgence, DROP prenom_urgence, DROP tel_urgence, DROP lien_urgence, DROP frequence_notification, CHANGE date_modification_portail date_modification DATETIME NOT NULL, CHANGE password statut VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F060A21BD112 ON adherent (personne_id)');
        $this->addSql('ALTER TABLE adresse_postale DROP habite_chez');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC25F06C53');
        $this->addSql('DROP INDEX IDX_67F068BC25F06C53 ON commentaire');
        $this->addSql('ALTER TABLE commentaire CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BCA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('CREATE INDEX IDX_67F068BCA21BD112 ON commentaire (personne_id)');
        $this->addSql('ALTER TABLE coordonnees ADD tel_mobile VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', ADD adresse_mail VARCHAR(100) DEFAULT NULL, DROP tel_mobile1, DROP tel_mobile2, DROP tel_travail, DROP adresse_mail1, DROP adresse_mail2');
        $this->addSql('ALTER TABLE demande_moderation_commentaire DROP FOREIGN KEY FK_EE52F1A525F06C53');
        $this->addSql('DROP INDEX IDX_EE52F1A525F06C53 ON demande_moderation_commentaire');
        $this->addSql('ALTER TABLE demande_moderation_commentaire CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE demande_moderation_commentaire ADD CONSTRAINT FK_EE52F1A5A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('CREATE INDEX IDX_EE52F1A5A21BD112 ON demande_moderation_commentaire (personne_id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC7F72333D');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC4A7FE23C');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC7F72333D FOREIGN KEY (visiteur_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC4A7FE23C FOREIGN KEY (nomme_par_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA25F06C53');
        $this->addSql('DROP INDEX IDX_BF5476CA25F06C53 ON notification');
        $this->addSql('ALTER TABLE notification CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('CREATE INDEX IDX_BF5476CAA21BD112 ON notification (personne_id)');
        $this->addSql('DROP INDEX IDX_C91D9C0CEC4A74AB ON participation_sejour_unite');
        $this->addSql('ALTER TABLE participation_sejour_unite CHANGE unite_id equipe_id INT NOT NULL');
        $this->addSql('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0C6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)');
        $this->addSql('CREATE INDEX IDX_C91D9C0C6D861B89 ON participation_sejour_unite (equipe_id)');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578136B84926');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D578136B84926 FOREIGN KEY (encadrant_eedf_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028E82E7EE8');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520288A141997');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028E82E7EE8 FOREIGN KEY (directeur_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F520288A141997 FOREIGN KEY (accompagnant_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE sejour_direction_adjointe DROP FOREIGN KEY FK_9C96E83A25F06C53');
        $this->addSql('DROP INDEX IDX_9C96E83A25F06C53 ON sejour_direction_adjointe');
        $this->addSql('DROP INDEX `primary` ON sejour_direction_adjointe');
        $this->addSql('ALTER TABLE sejour_direction_adjointe CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD CONSTRAINT FK_9C96E83AA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9C96E83AA21BD112 ON sejour_direction_adjointe (personne_id)');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD PRIMARY KEY (sejour_id, personne_id)');
        $this->addSql('ALTER TABLE sejour_encadrement DROP FOREIGN KEY FK_36F4A74F25F06C53');
        $this->addSql('DROP INDEX IDX_36F4A74F25F06C53 ON sejour_encadrement');
        $this->addSql('DROP INDEX `primary` ON sejour_encadrement');
        $this->addSql('ALTER TABLE sejour_encadrement CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_encadrement ADD CONSTRAINT FK_36F4A74FA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_36F4A74FA21BD112 ON sejour_encadrement (personne_id)');
        $this->addSql('ALTER TABLE sejour_encadrement ADD PRIMARY KEY (sejour_id, personne_id)');
        $this->addSql('ALTER TABLE sejour_suivi DROP FOREIGN KEY FK_C69CAC9B25F06C53');
        $this->addSql('DROP INDEX IDX_C69CAC9B25F06C53 ON sejour_suivi');
        $this->addSql('DROP INDEX `primary` ON sejour_suivi');
        $this->addSql('ALTER TABLE sejour_suivi CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_suivi ADD CONSTRAINT FK_C69CAC9BA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C69CAC9BA21BD112 ON sejour_suivi (personne_id)');
        $this->addSql('ALTER TABLE sejour_suivi ADD PRIMARY KEY (sejour_id, personne_id)');
        $this->addSql('ALTER TABLE structure ADD statut VARCHAR(255) NOT NULL, ADD echelon VARCHAR(255) DEFAULT NULL, DROP actif');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F0137EA2685EE64 ON structure (id_jeito)');
        $this->addSql('ALTER TABLE structure_organisation DROP FOREIGN KEY FK_A9AF96C425F06C53');
        $this->addSql('DROP INDEX IDX_A9AF96C425F06C53 ON structure_organisation');
        $this->addSql('DROP INDEX `primary` ON structure_organisation');
        $this->addSql('ALTER TABLE structure_organisation CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_organisation ADD CONSTRAINT FK_A9AF96C4A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A9AF96C4A21BD112 ON structure_organisation (personne_id)');
        $this->addSql('ALTER TABLE structure_organisation ADD PRIMARY KEY (structure_id, personne_id)');
        $this->addSql('ALTER TABLE structure_validation DROP FOREIGN KEY FK_24E26F2025F06C53');
        $this->addSql('DROP INDEX IDX_24E26F2025F06C53 ON structure_validation');
        $this->addSql('DROP INDEX `primary` ON structure_validation');
        $this->addSql('ALTER TABLE structure_validation CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_validation ADD CONSTRAINT FK_24E26F20A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_24E26F20A21BD112 ON structure_validation (personne_id)');
        $this->addSql('ALTER TABLE structure_validation ADD PRIMARY KEY (structure_id, personne_id)');
        $this->addSql('ALTER TABLE structure_suivi DROP FOREIGN KEY FK_FB6D911925F06C53');
        $this->addSql('DROP INDEX IDX_FB6D911925F06C53 ON structure_suivi');
        $this->addSql('DROP INDEX `primary` ON structure_suivi');
        $this->addSql('ALTER TABLE structure_suivi CHANGE adherent_id personne_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_suivi ADD CONSTRAINT FK_FB6D9119A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_FB6D9119A21BD112 ON structure_suivi (personne_id)');
        $this->addSql('ALTER TABLE structure_suivi ADD PRIMARY KEY (structure_id, personne_id)');
        $this->addSql('ALTER TABLE structure_regroupement_sejour CHANGE statut_demande statut_demande VARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE personne CHANGE nom nom VARCHAR(100) NOT NULL, CHANGE prenom prenom VARCHAR(100) NOT NULL, CHANGE adresse_mail adresse_mail VARCHAR(254) NOT NULL');
        $this->addSql('ALTER TABLE employe DROP matricule, CHANGE id_jeito numero_salarie BIGINT NOT NULL');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE reset_password_request');*/
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE participation_sejour_unite DROP FOREIGN KEY FK_C91D9C0C6D861B89');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060A21BD112');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BCA21BD112');
        $this->addSql('ALTER TABLE demande_moderation_commentaire DROP FOREIGN KEY FK_EE52F1A5A21BD112');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC7F72333D');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC4A7FE23C');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAA21BD112');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578136B84926');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028E82E7EE8');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520288A141997');
        $this->addSql('ALTER TABLE sejour_direction_adjointe DROP FOREIGN KEY FK_9C96E83AA21BD112');
        $this->addSql('ALTER TABLE sejour_encadrement DROP FOREIGN KEY FK_36F4A74FA21BD112');
        $this->addSql('ALTER TABLE sejour_suivi DROP FOREIGN KEY FK_C69CAC9BA21BD112');
        $this->addSql('ALTER TABLE structure_organisation DROP FOREIGN KEY FK_A9AF96C4A21BD112');
        $this->addSql('ALTER TABLE structure_validation DROP FOREIGN KEY FK_24E26F20A21BD112');
        $this->addSql('ALTER TABLE structure_suivi DROP FOREIGN KEY FK_FB6D9119A21BD112');
        $this->addSql('CREATE TABLE fonction_associative (id INT AUTO_INCREMENT NOT NULL, structure_id INT DEFAULT NULL, unite_id INT DEFAULT NULL, adherent_id INT NOT NULL, code_fonction VARCHAR(10) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, date_debut DATE DEFAULT NULL, date_fin DATE DEFAULT NULL, INDEX IDX_9700490A2534008B (structure_id), INDEX IDX_9700490A25F06C53 (adherent_id), INDEX IDX_9700490AEC4A74AB (unite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE participation_sejour_unite_adherent (participation_sejour_unite_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_10E655D825F06C53 (adherent_id), INDEX IDX_10E655D8D1A99552 (participation_sejour_unite_id), PRIMARY KEY(participation_sejour_unite_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE sejour_adherent (sejour_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_833517CD25F06C53 (adherent_id), INDEX IDX_833517CD84CF0CF (sejour_id), PRIMARY KEY(sejour_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE structure_regroupement_sejour_unite (structure_regroupement_sejour_id INT NOT NULL, unite_id INT NOT NULL, INDEX IDX_EAD22509AE72291D (structure_regroupement_sejour_id), INDEX IDX_EAD22509EC4A74AB (unite_id), PRIMARY KEY(structure_regroupement_sejour_id, unite_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE unite (id INT AUTO_INCREMENT NOT NULL, structure_parent_id INT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, date_modification DATETIME NOT NULL, actif TINYINT(1) NOT NULL, id_jeito BIGINT DEFAULT NULL, type VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_1D64C118E422675C (structure_parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490AEC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent ADD CONSTRAINT FK_10E655D8D1A99552 FOREIGN KEY (participation_sejour_unite_id) REFERENCES participation_sejour_unite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent ADD CONSTRAINT FK_10E655D825F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_adherent ADD CONSTRAINT FK_833517CD25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_adherent ADD CONSTRAINT FK_833517CD84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite ADD CONSTRAINT FK_EAD22509EC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite ADD CONSTRAINT FK_EAD22509AE72291D FOREIGN KEY (structure_regroupement_sejour_id) REFERENCES structure_regroupement_sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE unite ADD CONSTRAINT FK_1D64C118E422675C FOREIGN KEY (structure_parent_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE adhesion DROP FOREIGN KEY FK_C50CA65A2534008B');
        $this->addSql('ALTER TABLE adhesion DROP FOREIGN KEY FK_C50CA65A25F06C53');
        $this->addSql('ALTER TABLE contrat_travail DROP FOREIGN KEY FK_394729CD1B65292');
        $this->addSql('ALTER TABLE employe DROP FOREIGN KEY FK_F804D3B9A21BD112');
        $this->addSql('ALTER TABLE equipe DROP FOREIGN KEY FK_2449BA152534008B');
        $this->addSql('ALTER TABLE fonction DROP FOREIGN KEY FK_900D5BDA21BD112');
        $this->addSql('ALTER TABLE fonction DROP FOREIGN KEY FK_900D5BD6D861B89');
        $this->addSql('ALTER TABLE participation_sejour_unite_personne DROP FOREIGN KEY FK_8FFB6C57D1A99552');
        $this->addSql('ALTER TABLE participation_sejour_unite_personne DROP FOREIGN KEY FK_8FFB6C57A21BD112');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF5853DEDF');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF39FF2EE9');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF2B4A8107');
        $this->addSql('ALTER TABLE sejour_personne DROP FOREIGN KEY FK_1C282E4284CF0CF');
        $this->addSql('ALTER TABLE sejour_personne DROP FOREIGN KEY FK_1C282E42A21BD112');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_equipe DROP FOREIGN KEY FK_4E0E2C03AE72291D');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_equipe DROP FOREIGN KEY FK_4E0E2C036D861B89');
        $this->addSql('DROP TABLE adhesion');
        $this->addSql('DROP TABLE contrat_travail');
        $this->addSql('DROP TABLE employe');
        $this->addSql('DROP TABLE equipe');
        $this->addSql('DROP TABLE fonction');
        $this->addSql('DROP TABLE participation_sejour_unite_personne');
        $this->addSql('DROP TABLE personne');
        $this->addSql('DROP TABLE sejour_personne');
        $this->addSql('DROP TABLE structure_regroupement_sejour_equipe');
        $this->addSql('DROP INDEX UNIQ_90D3F060A21BD112 ON adherent');
        $this->addSql('ALTER TABLE adherent ADD coordonnees_id INT DEFAULT NULL, ADD coordonnees_responsable_legal1_id INT DEFAULT NULL, ADD coordonnees_responsable_legal2_id INT DEFAULT NULL, ADD nom VARCHAR(50) NOT NULL, ADD prenom VARCHAR(50) NOT NULL, ADD date_naissance DATE NOT NULL, ADD droit_image TINYINT(1) NOT NULL, ADD genre VARCHAR(20) NOT NULL, ADD status_adhesion VARCHAR(50) NOT NULL, ADD adresse_mail VARCHAR(50) NOT NULL, ADD roles JSON NOT NULL, ADD is_verified TINYINT(1) NOT NULL, ADD date_verification_email DATETIME DEFAULT NULL, ADD date_derniere_connexion DATETIME DEFAULT NULL, ADD nom_urgence VARCHAR(200) DEFAULT NULL, ADD prenom_urgence VARCHAR(200) DEFAULT NULL, ADD tel_urgence VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', ADD lien_urgence VARCHAR(200) DEFAULT NULL, ADD frequence_notification VARCHAR(50) NOT NULL, DROP personne_id, CHANGE date_modification date_modification_portail DATETIME NOT NULL, CHANGE statut password VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EC5C58C FOREIGN KEY (coordonnees_responsable_legal2_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0601C706A62 FOREIGN KEY (coordonnees_responsable_legal1_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0605853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F0605853DEDF ON adherent (coordonnees_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F060EC5C58C ON adherent (coordonnees_responsable_legal2_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F0601C706A62 ON adherent (coordonnees_responsable_legal1_id)');
        $this->addSql('ALTER TABLE adresse_postale ADD habite_chez VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_67F068BCA21BD112 ON commentaire');
        $this->addSql('ALTER TABLE commentaire CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_67F068BC25F06C53 ON commentaire (adherent_id)');
        $this->addSql('ALTER TABLE coordonnees ADD tel_mobile2 VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', ADD tel_travail VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', ADD adresse_mail2 VARCHAR(100) DEFAULT NULL, CHANGE tel_mobile tel_mobile1 VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', CHANGE adresse_mail adresse_mail1 VARCHAR(100) DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_EE52F1A5A21BD112 ON demande_moderation_commentaire');
        $this->addSql('ALTER TABLE demande_moderation_commentaire CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE demande_moderation_commentaire ADD CONSTRAINT FK_EE52F1A525F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_EE52F1A525F06C53 ON demande_moderation_commentaire (adherent_id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC7F72333D');
        $this->addSql('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC4A7FE23C');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC7F72333D FOREIGN KEY (visiteur_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC4A7FE23C FOREIGN KEY (nomme_par_id) REFERENCES adherent (id)');
        $this->addSql('DROP INDEX IDX_BF5476CAA21BD112 ON notification');
        $this->addSql('ALTER TABLE notification CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_BF5476CA25F06C53 ON notification (adherent_id)');
        $this->addSql('DROP INDEX IDX_C91D9C0C6D861B89 ON participation_sejour_unite');
        $this->addSql('ALTER TABLE participation_sejour_unite CHANGE equipe_id unite_id INT NOT NULL');
        $this->addSql('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0CEC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id)');
        $this->addSql('CREATE INDEX IDX_C91D9C0CEC4A74AB ON participation_sejour_unite (unite_id)');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578136B84926');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D578136B84926 FOREIGN KEY (encadrant_eedf_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028E82E7EE8');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520288A141997');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028E82E7EE8 FOREIGN KEY (directeur_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F520288A141997 FOREIGN KEY (accompagnant_id) REFERENCES adherent (id)');
        $this->addSql('DROP INDEX IDX_9C96E83AA21BD112 ON sejour_direction_adjointe');
        $this->addSql('DROP INDEX `PRIMARY` ON sejour_direction_adjointe');
        $this->addSql('ALTER TABLE sejour_direction_adjointe CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD CONSTRAINT FK_9C96E83A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9C96E83A25F06C53 ON sejour_direction_adjointe (adherent_id)');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD PRIMARY KEY (sejour_id, adherent_id)');
        $this->addSql('DROP INDEX IDX_36F4A74FA21BD112 ON sejour_encadrement');
        $this->addSql('DROP INDEX `PRIMARY` ON sejour_encadrement');
        $this->addSql('ALTER TABLE sejour_encadrement CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_encadrement ADD CONSTRAINT FK_36F4A74F25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_36F4A74F25F06C53 ON sejour_encadrement (adherent_id)');
        $this->addSql('ALTER TABLE sejour_encadrement ADD PRIMARY KEY (sejour_id, adherent_id)');
        $this->addSql('DROP INDEX IDX_C69CAC9BA21BD112 ON sejour_suivi');
        $this->addSql('DROP INDEX `PRIMARY` ON sejour_suivi');
        $this->addSql('ALTER TABLE sejour_suivi CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour_suivi ADD CONSTRAINT FK_C69CAC9B25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C69CAC9B25F06C53 ON sejour_suivi (adherent_id)');
        $this->addSql('ALTER TABLE sejour_suivi ADD PRIMARY KEY (sejour_id, adherent_id)');
        $this->addSql('DROP INDEX UNIQ_6F0137EA2685EE64 ON structure');
        $this->addSql('ALTER TABLE structure ADD actif TINYINT(1) NOT NULL, DROP statut, DROP echelon');
        $this->addSql('DROP INDEX IDX_A9AF96C4A21BD112 ON structure_organisation');
        $this->addSql('DROP INDEX `PRIMARY` ON structure_organisation');
        $this->addSql('ALTER TABLE structure_organisation CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_organisation ADD CONSTRAINT FK_A9AF96C425F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A9AF96C425F06C53 ON structure_organisation (adherent_id)');
        $this->addSql('ALTER TABLE structure_organisation ADD PRIMARY KEY (structure_id, adherent_id)');
        $this->addSql('ALTER TABLE structure_regroupement_sejour CHANGE statut_demande statut_demande VARCHAR(20) DEFAULT \'En attente\' NOT NULL');
        $this->addSql('DROP INDEX IDX_FB6D9119A21BD112 ON structure_suivi');
        $this->addSql('DROP INDEX `PRIMARY` ON structure_suivi');
        $this->addSql('ALTER TABLE structure_suivi CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_suivi ADD CONSTRAINT FK_FB6D911925F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_FB6D911925F06C53 ON structure_suivi (adherent_id)');
        $this->addSql('ALTER TABLE structure_suivi ADD PRIMARY KEY (structure_id, adherent_id)');
        $this->addSql('DROP INDEX IDX_24E26F20A21BD112 ON structure_validation');
        $this->addSql('DROP INDEX `PRIMARY` ON structure_validation');
        $this->addSql('ALTER TABLE structure_validation CHANGE personne_id adherent_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_validation ADD CONSTRAINT FK_24E26F2025F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_24E26F2025F06C53 ON structure_validation (adherent_id)');
        $this->addSql('ALTER TABLE structure_validation ADD PRIMARY KEY (structure_id, adherent_id)');
        $this->addSql('ALTER TABLE personne CHANGE nom nom VARCHAR(255) NOT NULL, CHANGE prenom prenom VARCHAR(255) NOT NULL, CHANGE adresse_mail adresse_mail VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE employe ADD matricule VARCHAR(255) NOT NULL, CHANGE numero_salarie id_jeito BIGINT NOT NULL');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, hashed_token VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES personne (id) ON DELETE CASCADE');
    }
}
