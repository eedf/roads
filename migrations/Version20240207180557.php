<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Kernel;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240207180557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Tranche 3 lot 1: lieux de séjour + migration des données';
    }

    public function up(Schema $schema): void
    {
        $env = 'prod';
        $kernel = new Kernel($env, false);
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'roads:migration_lieu_sejour'
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->doRun($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        $this->write($content);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lieu_sejour DROP FOREIGN KEY FK_1C55D963DD7FC659');
        $this->addSql('ALTER TABLE lieu_sejour DROP FOREIGN KEY FK_1C55D9637FDD45A9');
        $this->addSql('DROP TABLE lieu_sejour');
        $this->addSql('DROP TABLE localisation');
    }
}
