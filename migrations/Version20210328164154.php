<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210328164154 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE structure_regroupement_sejour (id INT AUTO_INCREMENT NOT NULL, structure_eedf_id INT DEFAULT NULL, est_eedf TINYINT(1) NOT NULL, nom_structure_non_eedf VARCHAR(255) DEFAULT NULL, description_structure_non_eedf LONGTEXT DEFAULT NULL, INDEX IDX_443A2F82C863794 (structure_eedf_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE structure_regroupement_sejour ADD CONSTRAINT FK_443A2F82C863794 FOREIGN KEY (structure_eedf_id) REFERENCES structure (id)');
        $this->addSql('DROP TABLE sejour_structure');
        $this->addSql('ALTER TABLE sejour DROP structures_regroupement_non_eedf');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sejour_structure (sejour_id INT NOT NULL, structure_id INT NOT NULL, INDEX IDX_C7750E5884CF0CF (sejour_id), INDEX IDX_C7750E582534008B (structure_id), PRIMARY KEY(sejour_id, structure_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE sejour_structure ADD CONSTRAINT FK_C7750E582534008B FOREIGN KEY (structure_id) REFERENCES structure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_structure ADD CONSTRAINT FK_C7750E5884CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE structure_regroupement_sejour');
        $this->addSql('ALTER TABLE sejour ADD structures_regroupement_non_eedf LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}
