<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210223151702 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent ADD adresse_mail VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE coordonnees ADD adresse_mail1 VARCHAR(100) DEFAULT NULL, ADD adresse_mail2 VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP adresse_mail');
        $this->addSql('ALTER TABLE coordonnees DROP adresse_mail1, DROP adresse_mail2');
    }
}
