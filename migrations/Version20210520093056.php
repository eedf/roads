<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520093056 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD adresse_lieu_id INT');
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD CONSTRAINT FK_C804825DC4ACAC53 FOREIGN KEY (adresse_lieu_id) REFERENCES adresse_postale (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C804825DC4ACAC53 ON lieu_declaration_intention (adresse_lieu_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825DC4ACAC53');
        $this->addSql('DROP INDEX UNIQ_C804825DC4ACAC53 ON lieu_declaration_intention');
        $this->addSql('ALTER TABLE lieu_declaration_intention DROP adresse_lieu_id');
    }
}
