<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313184505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0601C706A62 FOREIGN KEY (coordonnees_responsable_legal1_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0605853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EC5C58C FOREIGN KEY (coordonnees_responsable_legal2_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A25F06C53');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A550D68FC');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AED3534CB');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A550D68FC FOREIGN KEY (adherent_pour_reponsable_legal2_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AED3534CB FOREIGN KEY (adherent_pour_responsble_legal1_id) REFERENCES adherent (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0605853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0601C706A62 FOREIGN KEY (coordonnees_responsable_legal1_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EC5C58C FOREIGN KEY (coordonnees_responsable_legal2_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A25F06C53');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AED3534CB');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A550D68FC');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AED3534CB FOREIGN KEY (adherent_pour_responsble_legal1_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A550D68FC FOREIGN KEY (adherent_pour_reponsable_legal2_id) REFERENCES adherent (id) ON DELETE CASCADE');
    }
}
