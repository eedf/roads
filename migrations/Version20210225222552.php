<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225222552 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE date_modification_portail date_modification_portail DATETIME NOT NULL');
        $this->addSql('ALTER TABLE structure ADD actif TINYINT(1) NOT NULL DEFAULT 1');
        $this->addSql('ALTER TABLE unite ADD actif TINYINT(1) NOT NULL DEFAULT 1');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE date_modification_portail date_modification_portail DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE structure DROP actif');
        $this->addSql('ALTER TABLE unite DROP actif');
    }
}
