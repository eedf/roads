<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313175722 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE coordonnees ADD adherent_id INT DEFAULT NULL, ADD adherent_pour_responsble_legal1_id INT DEFAULT NULL, ADD adherent_pour_reponsable_legal2_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AED3534CB FOREIGN KEY (adherent_pour_responsble_legal1_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7A550D68FC FOREIGN KEY (adherent_pour_reponsable_legal2_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BC8EC7A25F06C53 ON coordonnees (adherent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BC8EC7AED3534CB ON coordonnees (adherent_pour_responsble_legal1_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BC8EC7A550D68FC ON coordonnees (adherent_pour_reponsable_legal2_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A25F06C53');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AED3534CB');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7A550D68FC');
        $this->addSql('DROP INDEX UNIQ_BC8EC7A25F06C53 ON coordonnees');
        $this->addSql('DROP INDEX UNIQ_BC8EC7AED3534CB ON coordonnees');
        $this->addSql('DROP INDEX UNIQ_BC8EC7A550D68FC ON coordonnees');
        $this->addSql('ALTER TABLE coordonnees DROP adherent_id, DROP adherent_pour_responsble_legal1_id, DROP adherent_pour_reponsable_legal2_id');
    }
}
