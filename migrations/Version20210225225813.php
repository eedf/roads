<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225225813 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent ADD roles JSON NOT NULL, ADD password VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F06031C43700 ON adherent (numero_adherent)');
        $this->addSql('ALTER TABLE structure CHANGE actif actif TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE unite CHANGE actif actif TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_90D3F06031C43700 ON adherent');
        $this->addSql('ALTER TABLE adherent DROP roles, DROP password');
        $this->addSql('ALTER TABLE structure CHANGE actif actif TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE unite CHANGE actif actif TINYINT(1) DEFAULT \'1\' NOT NULL');
    }
}
