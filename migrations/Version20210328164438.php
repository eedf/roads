<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210328164438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE structure_regroupement_sejour ADD sejour_id INT NOT NULL');
        $this->addSql('ALTER TABLE structure_regroupement_sejour ADD CONSTRAINT FK_443A2F884CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('CREATE INDEX IDX_443A2F884CF0CF ON structure_regroupement_sejour (sejour_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE structure_regroupement_sejour DROP FOREIGN KEY FK_443A2F884CF0CF');
        $this->addSql('DROP INDEX IDX_443A2F884CF0CF ON structure_regroupement_sejour');
        $this->addSql('ALTER TABLE structure_regroupement_sejour DROP sejour_id');
    }
}
