<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312142544 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour ADD structure_organisatrice_id INT NOT NULL');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F5202879882DEE FOREIGN KEY (structure_organisatrice_id) REFERENCES structure (id)');
        $this->addSql('CREATE INDEX IDX_96F5202879882DEE ON sejour (structure_organisatrice_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F5202879882DEE');
        $this->addSql('DROP INDEX IDX_96F5202879882DEE ON sejour');
        $this->addSql('ALTER TABLE sejour DROP structure_organisatrice_id');
    }
}
