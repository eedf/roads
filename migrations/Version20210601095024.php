<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210601095024 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE validation_internationale_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, document_validation_internationale_id INT NOT NULL, commentaires LONGTEXT DEFAULT NULL, est_valide TINYINT(1) DEFAULT NULL, date_modification DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_B0D32AEF84CF0CF (sejour_id), UNIQUE INDEX UNIQ_B0D32AEFC3A1BF68 (document_validation_internationale_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE validation_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, document_validation_id INT NOT NULL, commentaires LONGTEXT DEFAULT NULL, est_valide TINYINT(1) DEFAULT NULL, date_modification DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_12A372F384CF0CF (sejour_id), UNIQUE INDEX UNIQ_12A372F38588ECDB (document_validation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE validation_internationale_sejour ADD CONSTRAINT FK_B0D32AEF84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE validation_internationale_sejour ADD CONSTRAINT FK_B0D32AEFC3A1BF68 FOREIGN KEY (document_validation_internationale_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE validation_sejour ADD CONSTRAINT FK_12A372F384CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE validation_sejour ADD CONSTRAINT FK_12A372F38588ECDB FOREIGN KEY (document_validation_id) REFERENCES document (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE validation_internationale_sejour');
        $this->addSql('DROP TABLE validation_sejour');
    }
}
