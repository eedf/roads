<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313193532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C');
        $this->addSql('DROP INDEX UNIQ_90D3F060EC5C58C ON adherent');
        $this->addSql('DROP INDEX UNIQ_90D3F0605853DEDF ON adherent');
        $this->addSql('DROP INDEX UNIQ_90D3F0601C706A62 ON adherent');
        $this->addSql('ALTER TABLE adherent DROP coordonnees_id, DROP coordonnees_responsable_legal1_id, DROP coordonnees_responsable_legal2_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent ADD coordonnees_id INT DEFAULT NULL, ADD coordonnees_responsable_legal1_id INT DEFAULT NULL, ADD coordonnees_responsable_legal2_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0601C706A62 FOREIGN KEY (coordonnees_responsable_legal1_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0605853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EC5C58C FOREIGN KEY (coordonnees_responsable_legal2_id) REFERENCES coordonnees (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F060EC5C58C ON adherent (coordonnees_responsable_legal2_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F0605853DEDF ON adherent (coordonnees_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F0601C706A62 ON adherent (coordonnees_responsable_legal1_id)');
    }
}
