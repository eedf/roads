<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313174622 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A25F06C53');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A25F06C53');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
    }
}
