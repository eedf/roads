<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210517074208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE aspects_participants_besoins_specifiques_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, nombre_participants_handicap INT NOT NULL, desc_participants_besoins_specifiques LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_D925C24684CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE aspects_participants_besoins_specifiques_sejour ADD CONSTRAINT FK_D925C24684CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE sejour DROP texte_participants_besoins_specifiques');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE aspects_participants_besoins_specifiques_sejour');
        $this->addSql('ALTER TABLE sejour ADD texte_participants_besoins_specifiques LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
