<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210614154420 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evenement_grave (id INT AUTO_INCREMENT NOT NULL, retour_sejour_id INT NOT NULL, nature_evenement VARCHAR(255) NOT NULL, precisions LONGTEXT DEFAULT NULL, INDEX IDX_B204721FA3793100 (retour_sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE retour_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, vis_mon_camp TINYINT(1) DEFAULT NULL, precisions_vis_mon_camp LONGTEXT DEFAULT NULL, parution_medias TINYINT(1) NOT NULL, nom_media VARCHAR(255) DEFAULT NULL, date_media DATETIME DEFAULT NULL, visite_etat TINYINT(1) NOT NULL, evenements_graves TINYINT(1) NOT NULL, incidents_mineurs LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_ADD1E27B84CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visite_sejour_etat (id INT AUTO_INCREMENT NOT NULL, retour_sejour_id INT NOT NULL, date DATETIME NOT NULL, service_concerne VARCHAR(100) NOT NULL, retours LONGTEXT DEFAULT NULL, INDEX IDX_78B19093A3793100 (retour_sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evenement_grave ADD CONSTRAINT FK_B204721FA3793100 FOREIGN KEY (retour_sejour_id) REFERENCES retour_sejour (id)');
        $this->addSql('ALTER TABLE retour_sejour ADD CONSTRAINT FK_ADD1E27B84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE visite_sejour_etat ADD CONSTRAINT FK_78B19093A3793100 FOREIGN KEY (retour_sejour_id) REFERENCES retour_sejour (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement_grave DROP FOREIGN KEY FK_B204721FA3793100');
        $this->addSql('ALTER TABLE visite_sejour_etat DROP FOREIGN KEY FK_78B19093A3793100');
        $this->addSql('DROP TABLE evenement_grave');
        $this->addSql('DROP TABLE retour_sejour');
        $this->addSql('DROP TABLE visite_sejour_etat');
    }
}
