<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Kernel;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240122132357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {

        $env = 'prod';
        $kernel = new Kernel($env, false);
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'roads:migration_uuid',
            '--env' => 'prod',
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->doRun($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        $this->write($content);


        // this up() migration is auto-generated, please modify it to your needs
        /*$this->addSql('ALTER TABLE adherent ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE adhesion ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');
        $this->addSql('ALTER TABLE contrat_travail ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');
        $this->addSql('ALTER TABLE employe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_2449BA152685EE64 ON equipe');
        $this->addSql('ALTER TABLE equipe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');
        $this->addSql('DROP INDEX UNIQ_900D5BD2685EE64 ON fonction');
        $this->addSql('ALTER TABLE fonction ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');

        $this->addSql('DROP INDEX UNIQ_FCEC9EF2685EE64 ON personne');
        $this->addSql('ALTER TABLE personne ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');

        $this->addSql('DROP INDEX UNIQ_6F0137EA2685EE64 ON structure');
        $this->addSql('ALTER TABLE structure ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP id_jeito');


        $this->addSql('CREATE UNIQUE INDEX UNIQ_90D3F060D17F50A6 ON adherent (uuid)');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_C50CA65AD17F50A6 ON adhesion (uuid)');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_394729CDD17F50A6 ON contrat_travail (uuid)');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_F804D3B9D17F50A6 ON employe (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2449BA15D17F50A6 ON equipe (uuid)');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_900D5BDD17F50A6 ON fonction (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCEC9EFD17F50A6 ON personne (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F0137EAD17F50A6 ON structure (uuid)');*/
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_90D3F060D17F50A6 ON adherent');
        $this->addSql('ALTER TABLE adherent DROP uuid');
        $this->addSql('DROP INDEX UNIQ_C50CA65AD17F50A6 ON adhesion');
        $this->addSql('ALTER TABLE adhesion ADD id_jeito BIGINT NOT NULL, DROP uuid');
        $this->addSql('DROP INDEX UNIQ_394729CDD17F50A6 ON contrat_travail');
        $this->addSql('ALTER TABLE contrat_travail ADD id_jeito BIGINT NOT NULL, DROP uuid');
        $this->addSql('DROP INDEX UNIQ_F804D3B9D17F50A6 ON employe');
        $this->addSql('ALTER TABLE employe DROP uuid');
        $this->addSql('DROP INDEX UNIQ_2449BA15D17F50A6 ON equipe');
        $this->addSql('ALTER TABLE equipe ADD id_jeito BIGINT NOT NULL, DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2449BA152685EE64 ON equipe (id_jeito)');
        $this->addSql('DROP INDEX UNIQ_900D5BDD17F50A6 ON fonction');
        $this->addSql('ALTER TABLE fonction ADD id_jeito BIGINT DEFAULT NULL, DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_900D5BD2685EE64 ON fonction (id_jeito)');
        $this->addSql('DROP INDEX UNIQ_FCEC9EFD17F50A6 ON personne');
        $this->addSql('ALTER TABLE personne ADD id_jeito BIGINT NOT NULL, DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FCEC9EF2685EE64 ON personne (id_jeito)');
        $this->addSql('DROP INDEX UNIQ_6F0137EAD17F50A6 ON structure');
        $this->addSql('ALTER TABLE structure ADD id_jeito BIGINT DEFAULT NULL, DROP uuid');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F0137EA2685EE64 ON structure (id_jeito)');
    }
}
