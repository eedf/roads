<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210705102610 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour ADD compte_resultat_id INT DEFAULT NULL, ADD retour_camp_international_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028FE226308 FOREIGN KEY (compte_resultat_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F520289EE1635D FOREIGN KEY (retour_camp_international_id) REFERENCES document (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F52028FE226308 ON sejour (compte_resultat_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F520289EE1635D ON sejour (retour_camp_international_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028FE226308');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520289EE1635D');
        $this->addSql('DROP INDEX UNIQ_96F52028FE226308 ON sejour');
        $this->addSql('DROP INDEX UNIQ_96F520289EE1635D ON sejour');
        $this->addSql('ALTER TABLE sejour DROP compte_resultat_id, DROP retour_camp_international_id');
    }
}
