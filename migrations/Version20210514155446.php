<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210514155446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD structure_regroupee_non_eedf_id INT DEFAULT NULL, ADD attacher VARCHAR(50) NOT NULL DEFAULT "non"');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D5781343FBB0FB FOREIGN KEY (structure_regroupee_non_eedf_id) REFERENCES structure_regroupement_sejour (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E8D5781343FBB0FB ON role_encadrant_sejour (structure_regroupee_non_eedf_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D5781343FBB0FB');
        $this->addSql('DROP INDEX UNIQ_E8D5781343FBB0FB ON role_encadrant_sejour');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP structure_regroupee_non_eedf_id, DROP attacher');
    }
}
