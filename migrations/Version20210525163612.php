<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525163612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Correction sérialisation déclaration intention';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE sejour SET infos_declaration = REPLACE(infos_declaration, \'"effectifsPrevisionnels":{"id":null,"\', \'"effectifsPrevisionnels":{"\')');
    }

    public function down(Schema $schema): void
    {
    }
}
