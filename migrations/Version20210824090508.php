<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210824090508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Suppression des départements des structures';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM structure_regroupement_sejour WHERE structure_eedf_id IN ( select id FROM structure as struct_dept where struct_dept.type="Département")');
        $this->addSql('DELETE FROM fonction_associative WHERE structure_id IN ( select id FROM structure as struct_dept where struct_dept.type="Département")');
        $this->addSql('DELETE FROM structure WHERE type = "Département"');
    }

    public function down(Schema $schema): void
    {
    }
}
