<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419151403 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Gestion des visites';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE nomination_visite_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, visiteur_id INT NOT NULL, nomme_par_id INT NOT NULL, INDEX IDX_1080DFBC84CF0CF (sejour_id), INDEX IDX_1080DFBC7F72333D (visiteur_id), INDEX IDX_1080DFBC4A7FE23C (nomme_par_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visite_sejour (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT DEFAULT NULL, nomination_visite_id INT NOT NULL, date_visite DATE NOT NULL, eval_manques_classeur_directeur TINYINT(1) DEFAULT NULL, precisions_manques_classeur_directeur LONGTEXT DEFAULT NULL, eval_mesures_sanitaires VARCHAR(255) DEFAULT NULL, autres_mesures_sanitaires LONGTEXT DEFAULT NULL, eval_pedagogie VARCHAR(255) DEFAULT NULL, autres_pedagogie LONGTEXT DEFAULT NULL, eval_formation VARCHAR(255) DEFAULT NULL, autres_formation LONGTEXT DEFAULT NULL, eval_amenagement VARCHAR(255) DEFAULT NULL, autres_amenagement LONGTEXT DEFAULT NULL, eval_hygiene VARCHAR(255) DEFAULT NULL, autres_hygiene LONGTEXT DEFAULT NULL, eval_alimentation VARCHAR(255) DEFAULT NULL, autres_alimentation LONGTEXT DEFAULT NULL, eval_gestion_admin_financiere VARCHAR(255) DEFAULT NULL, autres_gestion_admin_financiere LONGTEXT DEFAULT NULL, sous_evaluation_budget LONGTEXT DEFAULT NULL, eval_communication_externe VARCHAR(255) DEFAULT NULL, autres_communication_externe LONGTEXT DEFAULT NULL, dispositif_infos_parents LONGTEXT DEFAULT NULL, eval_sante VARCHAR(255) DEFAULT NULL, autres_sante LONGTEXT DEFAULT NULL, eval_points_generaux LONGTEXT DEFAULT NULL, eval_avis_general LONGTEXT DEFAULT NULL, sujets_complementaires_formation LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_2D428CD94BC44A10 (compte_rendu_id), UNIQUE INDEX UNIQ_2D428CD9B81DAAD7 (nomination_visite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC7F72333D FOREIGN KEY (visiteur_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC4A7FE23C FOREIGN KEY (nomme_par_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE visite_sejour ADD CONSTRAINT FK_2D428CD94BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE visite_sejour ADD CONSTRAINT FK_2D428CD9B81DAAD7 FOREIGN KEY (nomination_visite_id) REFERENCES nomination_visite_sejour (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visite_sejour DROP FOREIGN KEY FK_2D428CD9B81DAAD7');
        $this->addSql('DROP TABLE nomination_visite_sejour');
        $this->addSql('DROP TABLE visite_sejour');
    }
}
