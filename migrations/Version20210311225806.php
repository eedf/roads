<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210311225806 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sejour (id INT AUTO_INCREMENT NOT NULL, directeur_id INT DEFAULT NULL, INDEX IDX_96F52028E82E7EE8 (directeur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_direction_adjointe (sejour_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_9C96E83A84CF0CF (sejour_id), INDEX IDX_9C96E83A25F06C53 (adherent_id), PRIMARY KEY(sejour_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_encadrement (sejour_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_36F4A74F84CF0CF (sejour_id), INDEX IDX_36F4A74F25F06C53 (adherent_id), PRIMARY KEY(sejour_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_suivi (sejour_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_C69CAC9B84CF0CF (sejour_id), INDEX IDX_C69CAC9B25F06C53 (adherent_id), PRIMARY KEY(sejour_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure_organisation (structure_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_A9AF96C42534008B (structure_id), INDEX IDX_A9AF96C425F06C53 (adherent_id), PRIMARY KEY(structure_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure_validation (structure_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_24E26F202534008B (structure_id), INDEX IDX_24E26F2025F06C53 (adherent_id), PRIMARY KEY(structure_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure_suivi (structure_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_FB6D91192534008B (structure_id), INDEX IDX_FB6D911925F06C53 (adherent_id), PRIMARY KEY(structure_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028E82E7EE8 FOREIGN KEY (directeur_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD CONSTRAINT FK_9C96E83A84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_direction_adjointe ADD CONSTRAINT FK_9C96E83A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_encadrement ADD CONSTRAINT FK_36F4A74F84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_encadrement ADD CONSTRAINT FK_36F4A74F25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_suivi ADD CONSTRAINT FK_C69CAC9B84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sejour_suivi ADD CONSTRAINT FK_C69CAC9B25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_organisation ADD CONSTRAINT FK_A9AF96C42534008B FOREIGN KEY (structure_id) REFERENCES structure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_organisation ADD CONSTRAINT FK_A9AF96C425F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_validation ADD CONSTRAINT FK_24E26F202534008B FOREIGN KEY (structure_id) REFERENCES structure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_validation ADD CONSTRAINT FK_24E26F2025F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_suivi ADD CONSTRAINT FK_FB6D91192534008B FOREIGN KEY (structure_id) REFERENCES structure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_suivi ADD CONSTRAINT FK_FB6D911925F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour_direction_adjointe DROP FOREIGN KEY FK_9C96E83A84CF0CF');
        $this->addSql('ALTER TABLE sejour_encadrement DROP FOREIGN KEY FK_36F4A74F84CF0CF');
        $this->addSql('ALTER TABLE sejour_suivi DROP FOREIGN KEY FK_C69CAC9B84CF0CF');
        $this->addSql('DROP TABLE sejour');
        $this->addSql('DROP TABLE sejour_direction_adjointe');
        $this->addSql('DROP TABLE sejour_encadrement');
        $this->addSql('DROP TABLE sejour_suivi');
        $this->addSql('DROP TABLE structure_organisation');
        $this->addSql('DROP TABLE structure_validation');
        $this->addSql('DROP TABLE structure_suivi');
    }
}
