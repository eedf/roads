<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210511220624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE aspect_alimentaire_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, grille_menus_id INT DEFAULT NULL, choix_organisation LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_54E5147B84CF0CF (sejour_id), UNIQUE INDEX UNIQ_54E5147B101E4E48 (grille_menus_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aspect_budget_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, budget_previsionnel_id INT DEFAULT NULL, prix_moyen INT DEFAULT NULL, UNIQUE INDEX UNIQ_F00F814484CF0CF (sejour_id), UNIQUE INDEX UNIQ_F00F8144396CD707 (budget_previsionnel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aspect_communication_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, presentation_amont LONGTEXT DEFAULT NULL, date_reunion_information DATETIME DEFAULT NULL, moyen_communication_pendant LONGTEXT DEFAULT NULL, moyen_restitution LONGTEXT DEFAULT NULL, site_valorisation VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_D7C312784CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aspect_international_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, participation_formation_internationale TINYINT(1) NOT NULL, date_formation DATETIME DEFAULT NULL, lieu_formation VARCHAR(255) DEFAULT NULL, referent_formation VARCHAR(255) DEFAULT NULL, partenaire_international LONGTEXT DEFAULT NULL, financements LONGTEXT DEFAULT NULL, plan_repli LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_590FAE0C84CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_ressource (id INT AUTO_INCREMENT NOT NULL, document_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, lien VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_81A76DE5C33F7837 (document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participant_non_adherent (id INT AUTO_INCREMENT NOT NULL, unite_id INT NOT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) NOT NULL, age INT NOT NULL, commentaire VARCHAR(255) DEFAULT NULL, INDEX IDX_7386CAB8EC4A74AB (unite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        
        $this->addSql('CREATE TABLE participation_sejour_unite (id INT AUTO_INCREMENT NOT NULL, unite_id INT NOT NULL, structure_regroupement_sejour_id INT DEFAULT NULL, sejour_organisateur_id INT DEFAULT NULL, INDEX IDX_C91D9C0CEC4A74AB (unite_id), INDEX IDX_C91D9C0CAE72291D (structure_regroupement_sejour_id), INDEX IDX_C91D9C0C1133E160 (sejour_organisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participation_sejour_unite_adherent (participation_sejour_unite_id INT NOT NULL, adherent_id INT NOT NULL, INDEX IDX_10E655D8D1A99552 (participation_sejour_unite_id), INDEX IDX_10E655D825F06C53 (adherent_id), PRIMARY KEY(participation_sejour_unite_id, adherent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_encadrant_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, encadrant_id INT DEFAULT NULL, unite_participante_id INT DEFAULT NULL, role VARCHAR(255) NOT NULL, directeur_adjoint TINYINT(1) NOT NULL, psc1 TINYINT(1) NOT NULL, INDEX IDX_E8D5781384CF0CF (sejour_id), INDEX IDX_E8D57813FEF1BA4 (encadrant_id), INDEX IDX_E8D5781345C7D113 (unite_participante_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure_regroupement_sejour_unite (structure_regroupement_sejour_id INT NOT NULL, unite_id INT NOT NULL, INDEX IDX_EAD22509AE72291D (structure_regroupement_sejour_id), INDEX IDX_EAD22509EC4A74AB (unite_id), PRIMARY KEY(structure_regroupement_sejour_id, unite_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE aspect_alimentaire_sejour ADD CONSTRAINT FK_54E5147B84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE aspect_alimentaire_sejour ADD CONSTRAINT FK_54E5147B101E4E48 FOREIGN KEY (grille_menus_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE aspect_budget_sejour ADD CONSTRAINT FK_F00F814484CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE aspect_budget_sejour ADD CONSTRAINT FK_F00F8144396CD707 FOREIGN KEY (budget_previsionnel_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE aspect_communication_sejour ADD CONSTRAINT FK_D7C312784CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE aspect_international_sejour ADD CONSTRAINT FK_590FAE0C84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE document_ressource ADD CONSTRAINT FK_81A76DE5C33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE participant_non_adherent ADD CONSTRAINT FK_7386CAB8EC4A74AB FOREIGN KEY (unite_id) REFERENCES participation_sejour_unite (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0CEC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0CAE72291D FOREIGN KEY (structure_regroupement_sejour_id) REFERENCES structure_regroupement_sejour (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0C1133E160 FOREIGN KEY (sejour_organisateur_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent ADD CONSTRAINT FK_10E655D8D1A99552 FOREIGN KEY (participation_sejour_unite_id) REFERENCES participation_sejour_unite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent ADD CONSTRAINT FK_10E655D825F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D5781384CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D57813FEF1BA4 FOREIGN KEY (encadrant_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D5781345C7D113 FOREIGN KEY (unite_participante_id) REFERENCES participation_sejour_unite (id)');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite ADD CONSTRAINT FK_EAD22509AE72291D FOREIGN KEY (structure_regroupement_sejour_id) REFERENCES structure_regroupement_sejour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE structure_regroupement_sejour_unite ADD CONSTRAINT FK_EAD22509EC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE document_formation_adherent');
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME');
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD coordonnees_lieu_id INT DEFAULT NULL, ADD convention_lieu_id INT DEFAULT NULL, ADD terrain_eedf TINYINT(1) DEFAULT NULL, ADD site_internet VARCHAR(255) DEFAULT NULL, ADD date_arrivee DATETIME DEFAULT NULL, ADD date_depart DATETIME DEFAULT NULL, ADD proprietaire_personne_physique TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD CONSTRAINT FK_C804825D6D011E42 FOREIGN KEY (coordonnees_lieu_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE lieu_declaration_intention ADD CONSTRAINT FK_C804825DDD7FC659 FOREIGN KEY (convention_lieu_id) REFERENCES document (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C804825D6D011E42 ON lieu_declaration_intention (coordonnees_lieu_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C804825DDD7FC659 ON lieu_declaration_intention (convention_lieu_id)');
        $this->addSql('ALTER TABLE sejour ADD document_projet_peda_id INT DEFAULT NULL, ADD document_socle_commun_colos_apprenantes_id INT DEFAULT NULL, ADD description_organisation_transport LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028A7726408 FOREIGN KEY (document_projet_peda_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE sejour ADD CONSTRAINT FK_96F5202823364340 FOREIGN KEY (document_socle_commun_colos_apprenantes_id) REFERENCES document (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F52028A7726408 ON sejour (document_projet_peda_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96F5202823364340 ON sejour (document_socle_commun_colos_apprenantes_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE participant_non_adherent DROP FOREIGN KEY FK_7386CAB8EC4A74AB');
        $this->addSql('ALTER TABLE participation_sejour_unite_adherent DROP FOREIGN KEY FK_10E655D8D1A99552');
        $this->addSql('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D5781345C7D113');
        $this->addSql('CREATE TABLE document_formation_adherent (id INT AUTO_INCREMENT NOT NULL, document_id INT NOT NULL, adherent_id INT NOT NULL, nom_diplome VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, statut VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, precisions VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_5B9F007825F06C53 (adherent_id), UNIQUE INDEX UNIQ_5B9F0078C33F7837 (document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE document_formation_adherent ADD CONSTRAINT FK_5B9F007825F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE document_formation_adherent ADD CONSTRAINT FK_5B9F0078C33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('DROP TABLE aspect_alimentaire_sejour');
        $this->addSql('DROP TABLE aspect_budget_sejour');
        $this->addSql('DROP TABLE aspect_communication_sejour');
        $this->addSql('DROP TABLE aspect_international_sejour');
        $this->addSql('DROP TABLE document_ressource');
        $this->addSql('DROP TABLE participant_non_adherent');
        $this->addSql('DROP TABLE participation_sejour_unite');
        $this->addSql('DROP TABLE participation_sejour_unite_adherent');
        $this->addSql('DROP TABLE role_encadrant_sejour');
        $this->addSql('DROP TABLE structure_regroupement_sejour_unite');
        $this->addSql('ALTER TABLE document CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825D6D011E42');
        $this->addSql('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825DDD7FC659');
        $this->addSql('DROP INDEX UNIQ_C804825D6D011E42 ON lieu_declaration_intention');
        $this->addSql('DROP INDEX UNIQ_C804825DDD7FC659 ON lieu_declaration_intention');
        $this->addSql('ALTER TABLE lieu_declaration_intention DROP coordonnees_lieu_id, DROP convention_lieu_id, DROP terrain_eedf, DROP site_internet, DROP date_arrivee, DROP date_depart, DROP proprietaire_personne_physique');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028A7726408');
        $this->addSql('ALTER TABLE sejour DROP FOREIGN KEY FK_96F5202823364340');
        $this->addSql('DROP INDEX UNIQ_96F52028A7726408 ON sejour');
        $this->addSql('DROP INDEX UNIQ_96F5202823364340 ON sejour');
        $this->addSql('ALTER TABLE sejour DROP document_projet_peda_id, DROP document_socle_commun_colos_apprenantes_id, DROP description_organisation_transport');
    }
}
