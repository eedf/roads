<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210514174647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour CHANGE attacher attacher VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE structure_regroupement_sejour ADD nb_enfant_moins14_struct_non_eedf INT DEFAULT NULL, ADD nb_enfants_plus14_struct_non_eedf INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_encadrant_sejour CHANGE attacher attacher VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'non\' NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE structure_regroupement_sejour DROP nb_enfant_moins14_struct_non_eedf, DROP nb_enfants_plus14_struct_non_eedf');
    }
}
