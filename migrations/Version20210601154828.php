<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210601154828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE validation_internationale_sejour CHANGE document_validation_internationale_id document_validation_internationale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE validation_sejour CHANGE document_validation_id document_validation_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE validation_internationale_sejour CHANGE document_validation_internationale_id document_validation_internationale_id INT NOT NULL');
        $this->addSql('ALTER TABLE validation_sejour CHANGE document_validation_id document_validation_id INT NOT NULL');
    }
}
