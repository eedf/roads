<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929141203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout champs de gestion d\'équipe pour les visites';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visite_sejour ADD autres_gestion_equipe LONGTEXT DEFAULT NULL, ADD eval_gestion_equipe VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visite_sejour DROP autres_gestion_equipe, DROP eval_gestion_equipe');
    }
}
