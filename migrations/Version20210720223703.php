<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720223703 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE demande_moderation_commentaire (id INT AUTO_INCREMENT NOT NULL, commentaire_id INT NOT NULL, adherent_id INT NOT NULL, raison LONGTEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_EE52F1A5BA9CD190 (commentaire_id), INDEX IDX_EE52F1A525F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE demande_moderation_commentaire ADD CONSTRAINT FK_EE52F1A5BA9CD190 FOREIGN KEY (commentaire_id) REFERENCES commentaire (id)');
        $this->addSql('ALTER TABLE demande_moderation_commentaire ADD CONSTRAINT FK_EE52F1A525F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE commentaire CHANGE modere modere TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE demande_moderation_commentaire');
        $this->addSql('ALTER TABLE commentaire CHANGE modere modere TINYINT(1) DEFAULT \'0\' NOT NULL');
    }
}
