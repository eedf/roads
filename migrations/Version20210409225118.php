<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\StructureRegroupementSejour;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210409225118 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE frequence_notification frequence_notification VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE structure_regroupement_sejour ADD statut_demande VARCHAR(20) NOT NULL DEFAULT "'.StructureRegroupementSejour::STATUT_DEMANDE_EN_ATTENTE.'"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE frequence_notification frequence_notification VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'En temps réel\' NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE structure_regroupement_sejour DROP statut_demande');
    }
}
