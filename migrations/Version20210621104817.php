<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210621104817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adresse_postale DROP FOREIGN KEY FK_C112B0FA5853DEDF');
        $this->addSql('DROP INDEX UNIQ_C112B0FA5853DEDF ON adresse_postale');
        $this->addSql('ALTER TABLE adresse_postale DROP coordonnees_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adresse_postale ADD coordonnees_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adresse_postale ADD CONSTRAINT FK_C112B0FA5853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C112B0FA5853DEDF ON adresse_postale (coordonnees_id)');
    }
}
