<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210223145038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adherent (id INT AUTO_INCREMENT NOT NULL, coordonnees_id INT NOT NULL, coordonnees_responsable_legal1_id INT DEFAULT NULL, coordonnees_responsable_legal2_id INT DEFAULT NULL, numero_adherent INT NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, date_naissance DATE NOT NULL, droit_image TINYINT(1) NOT NULL, genre VARCHAR(20) NOT NULL, status_adhesion VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_90D3F0605853DEDF (coordonnees_id), UNIQUE INDEX UNIQ_90D3F0601C706A62 (coordonnees_responsable_legal1_id), UNIQUE INDEX UNIQ_90D3F060EC5C58C (coordonnees_responsable_legal2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_postale (id INT AUTO_INCREMENT NOT NULL, adresse VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(10) NOT NULL, ville VARCHAR(255) NOT NULL, pays VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coordonnees (id INT AUTO_INCREMENT NOT NULL, adresse_postale_id INT NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, tel_mobile1 VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', tel_mobile2 VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', tel_domicile VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', tel_travail VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', UNIQUE INDEX UNIQ_BC8EC7AC96EEC07 (adresse_postale_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fonction_associative (id INT AUTO_INCREMENT NOT NULL, structure_id INT DEFAULT NULL, unite_id INT DEFAULT NULL, adherent_id INT NOT NULL, code_fonction VARCHAR(10) NOT NULL, INDEX IDX_9700490A2534008B (structure_id), INDEX IDX_9700490AEC4A74AB (unite_id), INDEX IDX_9700490A25F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure (id INT AUTO_INCREMENT NOT NULL, structure_parent_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, type VARCHAR(100) NOT NULL, numero VARCHAR(255) NOT NULL, INDEX IDX_6F0137EAE422675C (structure_parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE unite (id INT AUTO_INCREMENT NOT NULL, structure_parent_id INT NOT NULL, nom VARCHAR(255) NOT NULL, numero VARCHAR(255) NOT NULL, INDEX IDX_1D64C118E422675C (structure_parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0605853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0601C706A62 FOREIGN KEY (coordonnees_responsable_legal1_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EC5C58C FOREIGN KEY (coordonnees_responsable_legal2_id) REFERENCES coordonnees (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AC96EEC07 FOREIGN KEY (adresse_postale_id) REFERENCES adresse_postale (id)');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490AEC4A74AB FOREIGN KEY (unite_id) REFERENCES unite (id)');
        $this->addSql('ALTER TABLE fonction_associative ADD CONSTRAINT FK_9700490A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE structure ADD CONSTRAINT FK_6F0137EAE422675C FOREIGN KEY (structure_parent_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE unite ADD CONSTRAINT FK_1D64C118E422675C FOREIGN KEY (structure_parent_id) REFERENCES structure (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A25F06C53');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AC96EEC07');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C');
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A2534008B');
        $this->addSql('ALTER TABLE structure DROP FOREIGN KEY FK_6F0137EAE422675C');
        $this->addSql('ALTER TABLE unite DROP FOREIGN KEY FK_1D64C118E422675C');
        $this->addSql('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490AEC4A74AB');
        $this->addSql('DROP TABLE adherent');
        $this->addSql('DROP TABLE adresse_postale');
        $this->addSql('DROP TABLE coordonnees');
        $this->addSql('DROP TABLE fonction_associative');
        $this->addSql('DROP TABLE structure');
        $this->addSql('DROP TABLE unite');
    }
}
