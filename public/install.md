
# Installation R.O.A.D.S

## Configurer le .env (url db, mailer, token et URL Jeito)

`php bin/console doctrine:migrations:migrate`

`php bin/console roads:synchro-portail`

`php bin/console roads:init-first-admin`
(saisir le numéro d'adhérent du 1er admin: )

configurer la tache cron pour lancer `php bin/console roads:synchro-portail` tous les soirs à 0h00