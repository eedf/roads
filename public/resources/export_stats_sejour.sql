SELECT 
	region.nom as region,
	structure_org.nom as structure,
	sejour.intitule as intitule_sejour, 
    CONCAT(directeur.prenom,CONCAT(' ',directeur.nom)) as directeur,
    directeur.genre as genre_directeur,
    directeur.date_naissance as date_naissance_directeur,
    CONCAT(
		CASE
			WHEN qualif_directeur.diplomes_direction LIKE "%DSF%"
			THEN "DSF "
			ELSE ""
		END,
		CONCAT(
			CASE
				WHEN qualif_directeur.diplomes_direction LIKE "%BAFD%"
				THEN "BAFD "
				ELSE ""
			END,
			CASE
				WHEN qualif_directeur.diplomes_direction LIKE "%Equivalence%"
				THEN "Equivalence"
				ELSE ""
			END
		)
	) as diplomes_direction,
    qualif_directeur.qualite_direction,
    sejour.date_debut,
    sejour.date_fin,
    TIMESTAMPDIFF(DAY,sejour.date_debut,sejour.date_fin) + 1 as nb_jours,
    participants_unite_eedf_sejour.nb_lutins,
    infos_encadrants_sejour.nb_resp_lutins,
    participants_unite_eedf_sejour.nb_lous,
    infos_encadrants_sejour.nb_resp_lous,
    participants_unite_eedf_sejour.nb_ecles,
	infos_encadrants_sejour.nb_resp_ecles,
    participants_unite_eedf_sejour.nb_aines,
    infos_encadrants_sejour.nb_resp_aines,
    infos_encadrants_sejour.nb_encadrants_personnes_support,
    structure_non_eedf.nb_parcticipants_structure_non_eedf + participants_unite_eedf_sejour.nb_participants_unite_eedf_non_adherents as nb_participants_non_eedf,
    structure_non_eedf.nb_parcticipants_structure_non_eedf + participants_unite_eedf_sejour.nb_participants_unite_eedf_adherents as nb_participants_cloture,
    (effectifs_previsionnels.nb_lutins 
     + effectifs_previsionnels.nb_louveteaux 
     + effectifs_previsionnels.nb_ecles
     + effectifs_previsionnels.nb_aines
     )as nb_participants_declaration_intention,
    participants_unite_eedf_sejour.nb_participants_adherents_feminin,
    participants_unite_eedf_sejour.nb_participants_adherents_masculin,
    infos_encadrants_sejour.nb_encadrants_feminins,
    infos_encadrants_sejour.nb_encadrants_masculins,
    mineurs_handicap.nombre_participants_handicap as nb_pers_situation_handicap,
    budget.prix_moyen as prix_moyen,
    infos_encadrants_sejour.nb_anim_titulaires,
    infos_encadrants_sejour.nb_anim_stagiaires,
    infos_encadrants_sejour.nb_anim_non_diplomes,
    infos_encadrants_sejour.nb_permis_b,
    infos_encadrants_sejour.nb_psc1,
    retour_sejour.vis_mon_camp as vis_mon_camp,
    retour_sejour.parution_medias as medias,
    retour_sejour.visite_etat as visite_etat,
    COUNT(evenement_grave.id) as nb_evts_graves
FROM `sejour` as sejour
INNER JOIN structure as structure_org 
	ON(sejour.structure_organisatrice_id=structure_org.id)
LEFT OUTER JOIN structure as region
	ON(
        (structure_org.structure_parent_id = region.id)
        AND (region.type = 'REGION')
    )
INNER JOIN adherent as directeur
ON(sejour.directeur_id = directeur.id)
LEFT OUTER JOIN aspects_participants_besoins_specifiques_sejour as mineurs_handicap
ON(mineurs_handicap.sejour_id = sejour.id)
LEFT OUTER JOIN aspect_budget_sejour as budget
ON(budget.sejour_id = sejour.id)
LEFT OUTER JOIN qualification_direction as qualif_directeur
ON(sejour.id = qualif_directeur.sejour_id)
LEFT OUTER JOIN retour_sejour as retour_sejour
ON(retour_sejour.sejour_id = sejour.id)
LEFT OUTER JOIN effectifs_previsionnels as effectifs_previsionnels
ON(effectifs_previsionnels.id = sejour.effectifs_previsionnels_id)
LEFT OUTER JOIN evenement_grave evenement_grave
ON(retour_sejour.id = evenement_grave.retour_sejour_id)
LEFT OUTER JOIN 
(
	SELECT 
		structure_regroupement_sejour.sejour_id,
		SUM(
			CASE 
				WHEN structure_regroupement_sejour.id IS NOT NULL
				THEN structure_regroupement_sejour.nb_enfant_moins14_struct_non_eedf + structure_regroupement_sejour.nb_enfants_plus14_struct_non_eedf 
				ELSE 0
			END
		) as nb_parcticipants_structure_non_eedf
	FROM structure_regroupement_sejour
	WHERE (NOT structure_regroupement_sejour.est_eedf)
	GROUP BY structure_regroupement_sejour.sejour_id
)as structure_non_eedf
ON(structure_non_eedf.sejour_id=sejour.id)
LEFT OUTER JOIN 
(
	SELECT 
		temp_participants.sejour_id,
		SUM(
			CASE
				WHEN (temp_participants.est_adherent =0)
				THEN 1
				ELSE 0 
			END
		) nb_participants_unite_eedf_non_adherents,
		SUM(
			CASE
				WHEN (temp_participants.est_adherent = 1)
				THEN 1
				ELSE 0 
			END
		) nb_participants_unite_eedf_adherents,
		SUM(
			CASE
				WHEN (temp_participants.genre ='Féminin')
				THEN 1
				ELSE 0 
			END
		) as nb_participants_adherents_feminin,
		SUM(
			CASE
				WHEN (temp_participants.genre ='Masculin')
				THEN 1
				ELSE 0 
			END
		) as nb_participants_adherents_masculin,
		SUM(
			CASE
				WHEN (temp_participants.type ='Ronde')
				THEN 1
				ELSE 0 
			END
		) as nb_lutins,
		SUM(
			CASE
				WHEN (temp_participants.type ='Cercle')
				THEN 1
				ELSE 0 
			END
		) as nb_lous,
		SUM(
			CASE
				WHEN (temp_participants.type ='Unite Eclé')
				THEN 1
				ELSE 0 
			END
		) as nb_ecles,
		SUM(
			CASE
				WHEN (temp_participants.type ='Clan Ainé')
				THEN 1
				ELSE 0 
			END
		) as nb_aines
		
	FROM (
		/* Participants EEDF des unités des structures EEDF de regroupement */
		SELECT 
			adherent.genre,
			unite.type as type,
			sejour.id as sejour_id,
			1 as est_adherent
		FROM participation_sejour_unite as participation_sejour_unite
		INNER JOIN unite as unite
			ON ( unite.id =participation_sejour_unite.unite_id)
		INNER JOIN structure_regroupement_sejour_unite structure_regroupement_sejour_unite
			ON( structure_regroupement_sejour_unite.unite_id = unite.id)
		INNER JOIN structure_regroupement_sejour
			ON (structure_regroupement_sejour.id = structure_regroupement_sejour_unite.structure_regroupement_sejour_id)
		INNER JOIN sejour
			ON (sejour.id = structure_regroupement_sejour.sejour_id)
		INNER JOIN participation_sejour_unite_adherent as participation_sejour_unite_adherent
			ON ( participation_sejour_unite_adherent.participation_sejour_unite_id =participation_sejour_unite.id)
		INNER JOIN adherent
			ON ( participation_sejour_unite_adherent.adherent_id=adherent.id )  
			
		
		UNION ALL 
		
			/* Participants non EEDF des unités des structures EEDF de regroupement */
			SELECT 
			null as genre,
			unite.type as type,
			sejour.id as sejour_id,
			0 as est_adherent
		FROM participation_sejour_unite as participation_sejour_unite
		INNER JOIN unite as unite
			ON ( unite.id =participation_sejour_unite.unite_id)
		INNER JOIN structure_regroupement_sejour_unite structure_regroupement_sejour_unite
			ON( structure_regroupement_sejour_unite.unite_id = unite.id)
		INNER JOIN structure_regroupement_sejour
			ON (structure_regroupement_sejour.id = structure_regroupement_sejour_unite.structure_regroupement_sejour_id)
		INNER JOIN sejour
			ON (sejour.id = structure_regroupement_sejour.sejour_id)
		INNER JOIN participant_non_adherent
			ON ( participant_non_adherent.unite_id=participation_sejour_unite.id )  
		
		UNION ALL
		/* Participants EEDF des unités de la structure organisatrice */
		SELECT 
			genre,
			unite.type,
			sejour.id,
			1 as est_adherent
		FROM participation_sejour_unite as participation_sejour_unite
		INNER JOIN unite as unite
			ON ( unite.id =participation_sejour_unite.unite_id)
		INNER JOIN sejour
			ON (sejour.id = participation_sejour_unite.sejour_organisateur_id)
		INNER JOIN participation_sejour_unite_adherent as participation_sejour_unite_adherent
			ON ( participation_sejour_unite_adherent.participation_sejour_unite_id =participation_sejour_unite.id)
		INNER JOIN adherent
			ON ( participation_sejour_unite_adherent.adherent_id=adherent.id )
		
		UNION ALL

		/* Participants non EEDF des unités de la structure organisatrice */
		SELECT 
			null as genre,
			unite.type,
			sejour.id,
			0 as est_adherent
		FROM participation_sejour_unite as participation_sejour_unite
		INNER JOIN unite as unite
			ON ( unite.id =participation_sejour_unite.unite_id)
		INNER JOIN sejour
			ON (sejour.id = participation_sejour_unite.sejour_organisateur_id)
		INNER JOIN participant_non_adherent
			ON ( participant_non_adherent.unite_id=participation_sejour_unite.id )  
	) as temp_participants
	GROUP BY sejour_id
) as participants_unite_eedf_sejour
ON(
	participants_unite_eedf_sejour.sejour_id = sejour.id
)
LEFT OUTER JOIN (
	SELECT 
		SUM(
			CASE
				WHEN (temp_encadrants.qualite_animation  LIKE "%Titulaire%")
				THEN 1
				ELSE 0 
			END
		) as nb_anim_titulaires,
		SUM(
			CASE
				WHEN (temp_encadrants.qualite_animation ="Stagiaire")
				THEN 1
				ELSE 0 
			END
		) as nb_anim_stagiaires,
		SUM(
			CASE
				WHEN (temp_encadrants.qualite_animation ="Non qualifié")
				THEN 1
				ELSE 0 
			END
		) as nb_anim_non_diplomes,
		SUM(
			CASE
				WHEN (temp_encadrants.permis_b = 1)
				THEN 1
				ELSE 0 
			END
		) as nb_permis_b,
		SUM(
			CASE
				WHEN (temp_encadrants.psc1 =1)
				THEN 1
				ELSE 0 
			END
		) as nb_psc1,
		SUM(
			CASE
				WHEN (temp_encadrants.genre ='Féminin')
				THEN 1
				ELSE 0 
			END
		) as nb_encadrants_feminins,
		SUM(
			CASE
				WHEN (temp_encadrants.genre ='Masculin')
				THEN 1
				ELSE 0 
			END
		) as nb_encadrants_masculins,
		SUM(
			CASE
				WHEN (temp_encadrants.type ='Ronde')
				THEN 1
				ELSE 0 
			END
		) as nb_resp_lutins,
		SUM(
			CASE
				WHEN (temp_encadrants.type ='Unité Eclé')
				THEN 1
				ELSE 0 
			END
		) as nb_resp_ecles,
		SUM(
			CASE
				WHEN (temp_encadrants.type ='Cercle')
				THEN 1
				ELSE 0 
			END
		) as nb_resp_lous,
		SUM(
			CASE
				WHEN (temp_encadrants.type ='Clan Ainé')
				THEN 1
				ELSE 0 
			END
		) as nb_resp_aines,
		SUM(
			CASE
				WHEN (temp_encadrants.equipe ="L'équipe support")
				THEN 1
				ELSE 0 
			END
		) as nb_encadrants_personnes_support,
		temp_encadrants.sejour_id
	FROM (
		SELECT
			CASE 
				WHEN non_adherent.id IS NULL
				THEN adherent.genre
				ELSE non_adherent.genre
			END as genre,
			role_encadrant_sejour.statut_assistant_sanitaire as psc1,
			role_encadrant_sejour.permis_b as permis_b,
			role_encadrant_sejour.qualite_animation as qualite_animation,
			role_encadrant_sejour.equipe as equipe,
			unite.type as type,
			role_encadrant_sejour.sejour_id as sejour_id
		FROM role_encadrant_sejour as role_encadrant_sejour
		LEFT OUTER JOIN adherent as adherent
		ON (role_encadrant_sejour.encadrant_eedf_id = adherent.id)
		LEFT OUTER JOIN infos_encadrant_non_eedf as non_adherent
		ON (non_adherent.id = role_encadrant_sejour.infos_encadrant_non_eedf_id)
		LEFT OUTER JOIN participation_sejour_unite
		ON (role_encadrant_sejour.unite_participante_id = participation_sejour_unite.id)
		LEFT OUTER JOIN unite 
		ON (unite.id = participation_sejour_unite.unite_id)
	) as temp_encadrants
	GROUP BY sejour_id
) as infos_encadrants_sejour
ON (infos_encadrants_sejour.sejour_id = sejour.id)

WHERE
(
	(sejour.status LIKE '%sejour_cloture%' OR sejour.status LIKE '%sejour_termine%') 
	AND 
	(sejour.date_debut > '2021-01-01' AND sejour.date_debut < '2022-01-01')
)
GROUP BY sejour.id