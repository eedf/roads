
// En charge de gérer l'affichage (sans modification) d'une carte en fonction des coordonnées GPS
function affichageLocalisation(div) {
    console.log("[affichageLocalisation]Start")
    var $div_latitude = $(div).find('.latitude');
    var $div_longitude = $(div).find('.longitude');
    var $id_map = $(div).find('.map').attr('id');
    console.log($id_map)
    //On initialise la carte sur le centre de la France

    var map = L.map($id_map);
    //A chaque fois que l'accordéon parent est ouvert on recharge les tuiles
    div.parents(".accordion").each(
        function () {
            $(this).on('shown.bs.collapse', function () {
                console.log("[affichageLocalisation]collapse")
                map.invalidateSize()
            })
        }
    );

    zoom = 12;
    marker = L.marker([$div_latitude.text(), $div_longitude.text()]).addTo(map);
    map.setView([$div_latitude.text(), $div_longitude.text()], zoom);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    map.on("load", function () {
        map.trigger('resize')
    });
    console.log("[affichageLocalisation]End")
}
// En charge de gérer l'affichage (avec modification) d'une carte en fonction des coordonnées GPS, plus la barre de recherche et la déduction de l'adresse avec Nominatim
function rechercheLocalisation(div) {
    console.log("[rechercheLocalisation]Start")
    var $div_codePostalCommune = $(div).find('.codePostalCommune')
    var $div_nomCommune = $(div).find('.nomCommune')
    var $div_departement = $(div).find('.departement')
    var $div_adresseManuelle = $(div).find('.adresseManuelle')
    var $div_adresse = $(div).find('.adresse')
    var $div_pays = $(div).find('.pays')
    var $div_latitude = $(div).find('.latitude');
    var $div_longitude = $(div).find('.longitude');
    var $id_map = $(div).find('.map').attr('id');

    desactiverEtMasquerDivForm($div_adresseManuelle)
    console.log($id_map)

    //On initialise la carte sur le centre de la France

    var map = L.map($id_map);
    //A chaque fois que l'accordéon parent est ouvert on recharge les tuiles
    div.parents(".accordion").each(
        function () {
            $(this).on('shown.bs.collapse', function () {
                console.log("collapse")
                map.invalidateSize()
            })
        }
    );

    latitudeOrigine = 46.603354
    longitudeOrigine = 1.8883335
    zoom = 6
    var marker
    if ($div_latitude.val() !== "" && $div_longitude.val() !== "") {
        latitudeOrigine = $div_latitude.val();
        longitudeOrigine = $div_longitude.val();
        zoom = 12;
        marker = L.marker([$div_latitude.val(), $div_longitude.val()]).addTo(map);
        activerEtAfficherDivForm($div_adresseManuelle)
    }
    map.setView([latitudeOrigine, longitudeOrigine], zoom);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    map.on("load", function () {
        map.trigger('resize')
    });




    //Gestion d'un clic sur la carte:
    //Déplacer le marqueur existant au nouvel emplacement
    //Centrer la carte sur le nouvel emplacement
    //déduire la nouvelle adresse correspondant aux coordonnées GPS
    //Afficher la saisie de l'adresse manuelle
    function onMapClick(e) {
        //On enleve le marqueur s'il existe
        if (marker) {
            marker.remove()
        }
        //On positionne les coordonnées géo graphiques
        $div_latitude.val(e.latlng.lat);
        $div_longitude.val(e.latlng.lng);
        //On se déplace sur la carte vers la nouvelle localisation
        map.flyTo(new L.LatLng(e.latlng.lat, e.latlng.lng));
        //On ajoute un marqueur sur la carte
        marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
        //On recherche l'adresse correspondant aux coordonnées géo
        searchReverse(e.latlng.lat, e.latlng.lng)
        activerEtAfficherDivForm($div_adresseManuelle)
    }


    map.on('click', onMapClick);
    var autocompleteUrl = 'https://nominatim.openstreetmap.org/';


    /** Initialise le champ adresse en fonction des doordonées GPS */
    function searchReverse(latitude, longitude) {
        $.ajax({
            url: 'https://nominatim.openstreetmap.org/reverse?lat=' + latitude + '&lon=' + longitude + "&format=jsonv2&addressdetails=1"
        }).then(function (data) {
            console.log(data)
            initialisationAdresse(data.address)
        });
    }

    /** initialise les champs d'adresse du formulaire */
    function initialisationAdresse(address) {
        $div_codePostalCommune.val(address.postcode);
        $div_nomCommune.val(displayCommune(address));
        $div_departement.val(address.county);
        $div_adresse.val(displayAdresse(address, false))
        $div_pays.val(address.country)
    }



    //Recherche de tous les champs avec la classe 'rechercheVille' (cf LocalisationType)

    //On récupère les champs de codeInsee et Nom commune associés
    var $div_rechercheAdresse = $(div).find('.rechercheAdresse')
    var $div_code_pays = $(div).find('.codePays')
    var $div_latitude = $(div).find('.latitude')
    var $div_longitude = $(div).find('.longitude')

    //On initialise le Pays à France
    if ($div_pays.val() == "") {
        $div_pays.val("France")
        $div_code_pays.val("fr")
    }


    var autocompleteUrl = 'https://nominatim.openstreetmap.org/search';

    $div_rechercheAdresse.autocomplete({ hint: false }, [
        {

            source: function (query, cb) {
                $.ajax({
                    url: autocompleteUrl + '?q=' + query.replace(" ", "+") + "&countrycodes=" + $div_code_pays.val() + "&format=jsonv2&addressdetails=1&layer=address"
                }).then(function (data) {

                    cb(data);
                });
            },
            displayKey: function (item) {

                return (item.display_name)
            },
            debounce: 500 // only request every 1/2 second
        }
    ]).on('autocomplete:selected', function (event, suggestion, dataset, context) {

        //On positionne les coordonnées géographiques
        $div_longitude.val(suggestion.lon);
        $div_latitude.val(suggestion.lat);

        //On remplit l'adersse
        initialisationAdresse(suggestion.address)
        activerEtAfficherDivForm($div_adresseManuelle)
        //On enlève le marqueur précédent (s'il existe)
        if (marker) {
            marker.remove()
        }
        //On se déplace vers la nouvelle localisation
        map.flyTo(new L.LatLng(suggestion.lat, suggestion.lon));
        //On ajoute un marqueur
        marker = L.marker([suggestion.lat, suggestion.lon]).addTo(map);

    })

    //Déduit une adresse normalisée de ce qui est renvoyé par la recherche nominatim
    function displayAdresse(adresse, avecCommune = true) {
        result = "";
        if (adresse.farm) {
            result += adresse.farm + ", ";
        }
        if (adresse.hamlet) {
            result += adresse.hamlet + ", ";
        }
        if (adresse.croft) {
            result += adresse.croft + ", ";
        }
        if (adresse.isolated_dwelling) {
            result += adresse.isolated_dwelling + ", ";
        }
        if (adresse.house_number) {
            result += adresse.house_number + " ";
        }
        if (adresse.house_name) {
            result += adresse.house_name + " ";
        }
        if (adresse.road) {
            result += adresse.road + " ";
        }
        if (adresse.square in adresse) {
            result += adresse.square + " ";
        }


        if (avecCommune) {
            if (adresse.postcode) {
                result += ", "
                result += adresse.postcode + " ";
            }
            result += displayCommune(adresse);
        }
        console.log(result)
        return result;

    }

    function displayCommune(adresse) {
        result = "";
        if (adresse.city) {
            result += adresse.city;
        } else if (adresse.town) {
            result += adresse.town;
        } else if (adresse.village) {
            result += adresse.village;
        } else if (adresse.municipality) {
            result += adresse.municipality;
        } else if (adresse.city_district) {
            result += adresse.city_district;
        }
        return result;
    }

}
