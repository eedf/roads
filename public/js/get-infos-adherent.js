$(document).ready(function () {
    $('.js-personne-autocomplete').each(function () {
        var autocompleteUrl = $(this).data('autocomplete-url');
        $(this).autocomplete({ hint: false }, [
            {
                source: function (query, cb) {
                    $.ajax({
                        url: autocompleteUrl + '?query=' + query
                    }).then(function (data) {
                        cb(data.adherents);
                    });
                },
                displayKey: function (item) {
                    if (item.statusAdhesion == "Adhérent·e") {
                        return (item.prenom + ' ' + item.nom + ' (' + item.numeroAdherent + ')')
                    } else {
                        return ('<i>' + item.prenom + ' ' + item.nom + ' (' + item.numeroAdherent + ') - Non adhérent.e</i>')
                    }
                },
                debounce: 500 // only request every 1/2 second
            }
        ])

    })
});