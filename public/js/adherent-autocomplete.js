$(document).ready(function () {
    $('.js-personne-autocomplete').each(function () {
        var autocompleteUrl = $(this).data('autocomplete-url');
        $(this).autocomplete({ hint: false }, [
            {
                source: function (query, cb) {
                    $.ajax({
                        url: autocompleteUrl + '?query=' + query
                    }).then(function (data) {
                        cb(data.personnes);
                    });
                },
                displayKey: function (item) {
                    return item
                },
                debounce: 500 // only request every 1/2 second
            }
        ])

    })
});