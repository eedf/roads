function desactiverEtMasquerDivForm($div) {
    $div.hide();
    $div.find("select").prop('disabled', true);
    $div.find("input").prop('disabled', true);
    $div.find("textarea").prop('disabled', true);
}

function activerEtAfficherDivForm($div) {
    $div.show();
    $div.find("select").prop('disabled', false);
    $div.find("input").prop('disabled', false);
    $div.find("textarea").prop('disabled', false);
}

//Requete en BDD pour avoir les infos personnelles du directeur à partir du numéro d'adhérent ou de salarié
function affichageInfosDirecteur($selectAdherent) {
    var resultat_adh = $selectAdherent.val()
    var path = $(".infos-personnes").attr('infos-personnes-url');
    var sejour_id = $(".infos-personnes").attr('sejour_id');
    var structure_id = $(".infos-personnes").attr('structure_id');
    $.ajax({
        type: 'GET',
        url: path,
        data: {
            text: resultat_adh,
            sejour: sejour_id,
            structure: structure_id
        },
        success: function (data) {
            if (data.personne) {
                $("#nom_dir").text(data.personne.nom)
                $("#prenom_dir").text(data.personne.prenom)
                if (data.personne.employe) {
                    $("#num_adh_dir").text(data.personne.employe.numeroSalarie)
                    if (data.personne.employe.telephone !== null) {
                        $("#tel_dir").text(data.personne.employe.telephone)
                    } if (data.personne.coordonnees.telMobile !== null) {
                        $("#tel_dir").text(data.personne.coordonnees.telMobile)
                    } else {
                        $("#tel_dir").text("Non renseigné")
                    }
                    $("#mail_dir").text(data.personne.employe.email)
                } else {
                    $("#num_adh_dir").text(data.personne.adherent.numeroAdherent)
                    $("#mail_dir").text(data.adresseMail)

                    if (data.personne.coordonnees.telMobile !== null) {
                        $("#tel_dir").text(data.personne.coordonnees.telMobile)
                    } else {
                        $("#tel_dir").text("Non renseigné")
                    }
                }

            }
        }
    });
}

