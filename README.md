# ROADS #
ROADS est l'acronyme pour **"Route pour l'Organisation, l'Accompagnement, et la Déclaration de Séjours"**

## Documentation technique ##
Une documentation technique générée avec *PhpDocumentor* est disponible sous: 
[https://roads.eedf.fr/docs/api/index.html](https://roads.eedf.fr/docs/api/index.html)

## Installation ## 

### Première installation ###
Sur une distribution GNU/Linux Ubuntu, dans un shell, exécuter les commandes suivantes :
`sudo apt install git docker.io docker-compose`

`cd /srv/roads`
`git clone https://gitlab.com/eedf/roads.git`
`sudo docker-compose build --no-cache`
`sudo docker-compose down`
`sudo docker-compose run --rm symfony php bin/console cache:clear`
`sudo docker-compose run --rm symfony php bin/console doctrine:migrations:migrate`
`sudo docker-compose run --rm symfony php bin/console roads:init-documents-ressources`

`sudo chown -R www-data. var/cache/`
`sudo docker-compose up -d `

Initialisation du premier administrateur:

`sudo docker-compose run --rm symfony php bin/console roads:init-first-admin`

Saisir le numéro  d'adhérent·e correspondant à l'administrat·eur·rice du site

### Mise à jour ###

`cd /srv/roads`
`git fetch`
`git reset --hard $1`
`sudo docker-compose build --no-cache`
`sudo docker-compose down`
`sudo docker-compose run --rm symfony php bin/console cache:clear`
`sudo docker-compose run --rm symfony php bin/console doctrine:migrations:migrate`
`sudo docker-compose run --rm symfony php bin/console roads:init-documents-ressources`
`sudo chown -R www-data. var/cache/`
`sudo docker-compose up -d` 

