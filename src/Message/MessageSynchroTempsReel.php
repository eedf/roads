<?php

namespace App\Message;

class MessageSynchroTempsReel
{

    const OPERATION_CREATE_UPDATE = "CREATE_UPDATE";
    const OPERATION_DELETE = "DELETE";
    const ROUTING_KEY_PERSONNE = "jeito.person";
    const ROUTING_KEY_STRUCTURE = "jeito.structure";
    const ROUTING_KEY_EQUIPE = "jeito.team";
    const ROUTING_KEY_ADHERENT = "jeito.adherent";
    const ROUTING_KEY_ADHESION = "jeito.adhesion";
    const ROUTING_KEY_SALARIE = "jeito.employee";
    const ROUTING_KEY_CONTRAT_TRAVAIL = "jeito.employment";
    const ROUTING_KEY_FONCTION = "jeito.function";

    private array $data;
    private string $routingKey;
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function setRoutingKey(String $routingKey)
    {
        $this->routingKey = $routingKey;
    }

    /**
     * Get the value of routingKey
     */
    public function getRoutingKey()
    {
        return $this->routingKey;
    }

    /**
     * Get the value of operation
     */
    public function getOperation()
    {
        return $this->data["operation"];
    }


    /**
     * Get the value of uuid
     */
    public function getUuid(): String
    {
        return $this->data["uuid"];
    }

    /**
     * Get the value of attributes
     */
    public function getAttributes()
    {
        return $this->data["attributes"];
    }

    public function __toString()
    {
        return json_encode($this->data);
    }
}
