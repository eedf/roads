<?php

namespace App\Command;

use App\Repository\NotificationRepository;
use App\Service\NotificationHelper;
use DateTime;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Envoie périodiquement les notifications mails aux adhérent·e·s
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:envoi-notifications',
    description: 'Envoie périodiquement les notifications mails aux adhérent·e·s',
)]
class RoadsEnvoiNotificationsCommand extends Command
{

    private $notificationHelper;
    private $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository, NotificationHelper $notificationHelper)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->notificationHelper = $notificationHelper;
        $this->notificationRepository = $notificationRepository;

        parent::__construct();
    }


    protected function configure() {}

    /**
     * Envoie périodiquement les notifications mails aux adhérent·e·s
     * Recherche les notifications à envoyer en BDD, par rapport à la date d'envoi et les envoie de manière groupée, puis supprime les notifications en base vieiles de plus d'un an
     * @param  mixed $input
     * @param  mixed $output
     * @return int
     * @uses NotificationHelper helper pour l'envoi de notifications
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info("Recherche des notifications à envoyer...");
        $notificationsAEnvoyer = $this->notificationRepository->findNotificationsAEnvoyer(new DateTime('next year'));


        $notificationsAEnvoyerParPersonne = array();
        foreach ($notificationsAEnvoyer as $notificationAEnvoyer) {
            $personne = $notificationAEnvoyer->getPersonne();
            if (!array_key_exists($personne->getId(), $notificationsAEnvoyerParPersonne)) {
                $notificationsAEnvoyerParPersonne[$personne->getId()] =
                    [
                        'personne' => $personne,
                        'notifications' => [
                            $notificationAEnvoyer
                        ]
                    ];
            } else {
                $notificationsAEnvoyerParPersonne[$personne->getId()]['notifications'][] = $notificationAEnvoyer;
            }
        }

        foreach ($notificationsAEnvoyerParPersonne as $notificationsAEnvoyerPourUnPersonne) {
            $personne = $notificationsAEnvoyerPourUnPersonne['personne'];
            $notifications = $notificationsAEnvoyerPourUnPersonne['notifications'];
            $io->info("Envoi de " . count($notifications) . " nouvelles notifications à la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $this->notificationHelper->envoiNotificationsGroupees($personne, $notifications);
        }
        $io->success("Terminé");
        $io->info("Suppression des notifications vieilles de plus d'un an");
        $nbNotificationsSupprimees = $this->notificationRepository->deleteCreatedBefore(new DateTime('last year'));
        $io->success("Terminé: " . $nbNotificationsSupprimees . " notifications ont été supprimées");
        return Command::SUCCESS;
    }
}
