<?php

namespace App\Command;

use DateTime;
use App\Kernel;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Uid\Uuid;



/**
 * Commande de migration des ID Jéito en UUID (nécessaire pour synchro temps réel)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:migration_uuid',
    description: "Permet de migrer les identifiant id jéito vers des UUID"
)]
class RoadsMigrationUUIDCommand extends Command
{


    private $entityManager;


    private array $uuidPersonnesByIdJeito;
    private array $uuidEquipesByIdJeito;
    private array $uuidStructuresByIdJeito;

    /**
     * @var string 
     */
    private $jeitoApiUrl;
    /**
     * @var string 
     */
    private $jeitoApiToken;

    const NIVEAU_MESSAGE_SUCCESS = 0;
    const NIVEAU_MESSAGE_INFO = 1;
    const NIVEAU_MESSAGE_WARNING = 2;
    const NIVEAU_MESSAGE_TEXT = 3;
    const NIVEAU_MESSAGE_SECTION = 4;

    const INFOS_DECLARATION = 1;
    const INFOS_SEJOUR = 2;





    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    public function __construct(EntityManagerInterface $entityManager, String $jeitoApiToken, String $jeitoApiUrl)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;

        $this->uuidPersonnesByIdJeito = array();
        $this->uuidEquipesByIdJeito = array();
        $this->uuidStructuresByIdJeito = array();
        $this->jeitoApiToken = $jeitoApiToken;
        $this->jeitoApiUrl = $jeitoApiUrl;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
        $io = new SymfonyStyle($input, $output);

        //Création des colonnes uuid
        $io->section("Création des uuid dans les tables entités...");

        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_6F0137EA2685EE64 ON structure')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_900D5BD2685EE64 ON fonction')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_FCEC9EF2685EE64 ON personne')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_2449BA152685EE64 ON equipe')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE equipe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('ALTER TABLE adhesion ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();


        $this->entityManager->getConnection()->prepare('ALTER TABLE contrat_travail ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $this->entityManager->getConnection()->prepare('ALTER TABLE employe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'')->executeQuery();

        $io->section("Initialisation des uuid dans les tables entités...");


        //Suppression des structures qui n'ont rien a faire là
        $this->entityManager->getConnection()->prepare("DELETE  FROM structure WHERE (echelon = 3 and id_jeito IS NULL)")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE  FROM structure WHERE (echelon = 2 and id_jeito IS NULL)")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE  FROM structure WHERE ( id_jeito IS NULL and nom not like 'Pôle territoires')")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE  FROM structure WHERE ( id_jeito IS NULL)")->executeQuery();


        $io->text("Chargement des couples uuid<->idJéito pour les structures, equipes et personnes");
        $io->text("structure");
        $this->uuidStructuresByIdJeito = $this->loadUuidByIdJeito("structure");

        $io->text("personne");
        $this->uuidPersonnesByIdJeito = $this->loadUuidByIdJeito("person");


        $io->text("equipe");
        $this->uuidEquipesByIdJeito = $this->loadUuidByIdJeito("team");




        $this->migrerTable($this->uuidStructuresByIdJeito, "structure");
        $this->migrerTable($this->uuidPersonnesByIdJeito, "personne");

        $this->migrerTable($this->uuidEquipesByIdJeito, "equipe");


        $io->text("fonction");
        $uuidFonctionByIdJeito = $this->loadUuidByIdJeito("function");
        $this->migrerTable($uuidFonctionByIdJeito, "fonction");
        $uuidFonctionByIdJeito = null;
        unset($uuidFonctionByIdJeito);

        $io->text("adhesion");
        $uuidAdhesionByIdJeito = $this->loadUuidByIdJeito("adhesion");
        $this->migrerTable($uuidAdhesionByIdJeito, "adhesion");
        $uuidAdhesionByIdJeito = null;
        unset($uuidAdhesionByIdJeito);

        $io->text("contrat_travail");
        $uuidCotnratTravailByIdJeito = $this->loadUuidByIdJeito("employment");
        $this->migrerTable($uuidCotnratTravailByIdJeito, "contrat_travail");
        $uuidCotnratTravailByIdJeito = null;
        unset($uuidCotnratTravailByIdJeito);

        $io->text("adherent");
        $uuidAdherentByIdJeito = $this->loadUuidByIdJeito("adherent");
        $this->migrerTable($uuidAdherentByIdJeito, "adherent", "numero_adherent");
        $uuidAdherentByIdJeito = null;
        unset($uuidAdherentByIdJeito);

        $io->text("employe");
        $uuidSalarieByIdJeito = $this->loadUuidByIdJeito("employee");
        $this->migrerTable($uuidSalarieByIdJeito, "employe", "numero_salarie");
        $uuidSalarieByIdJeito = null;
        unset($uuidSalarieByIdJeito);


        $io->text("Terminé");
        $io->section("Migration des données sérialisées");
        //Migration des données de déclaration des séjours


        $io->text("Migration des infos de déclaration");
        $infos_decla_sejour_serialisees = $this->entityManager->getConnection()->prepare("SELECT id, infos_declaration FROM sejour WHERE infos_declaration IS NOT NULL")->executeQuery();
        $io->progressStart($infos_decla_sejour_serialisees->rowCount());
        foreach ($infos_decla_sejour_serialisees->fetchAllAssociative() as $infos_decla_sejour_serialisee) {
            $result_infos_decla = array();
            $infos_decla = $infos_decla_sejour_serialisee['infos_declaration'];
            $id_sejour = $infos_decla_sejour_serialisee['id'];

            $infos_decla = json_decode($infos_decla, true);

            $result_infos_decla = $this->migrationDonneesSerialisees($infos_decla, self::INFOS_DECLARATION);

            $this->entityManager->getConnection()->update(
                'sejour',
                [
                    'infos_declaration' => json_encode($result_infos_decla),
                ],
                ['id' => $id_sejour]
            );

            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->text("Terminé");
        //Libération mémoire
        $infos_decla_sejour_serialisees = null;
        unset($infos_decla_sejour_serialisees);
        $result_infos_decla = null;
        unset($result_infos_decla);

        //Migration des données 
        $io->text("Migration des infos de séjour cloturés");
        $infos_sejour_serialisees = $this->entityManager->getConnection()->prepare("SELECT id, infos_sejour FROM sejour WHERE infos_sejour IS NOT NULL")->executeQuery();
        $io->progressStart($infos_sejour_serialisees->rowCount());
        foreach ($infos_sejour_serialisees->fetchAllAssociative() as $infos_sejour_serialisee) {

            $infos_sejour = $infos_sejour_serialisee['infos_sejour'];
            $id_sejour = $infos_sejour_serialisee['id'];

            $infos_sejour = json_decode($infos_sejour, true);

            $result_infos_sejour = $this->migrationDonneesSerialisees($infos_sejour, self::INFOS_SEJOUR);


            $this->entityManager->getConnection()->update(
                'sejour',
                [
                    'infos_sejour' => json_encode($result_infos_sejour),
                ],
                ['id' => $id_sejour]
            );
            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->text("Terminé");
        //Libération mémoire
        $infos_sejour_serialisees = null;
        unset($infos_sejour_serialisees);
        $result_infos_sejour = null;
        unset($result_infos_sejour);



        gc_collect_cycles();

        $io->section("Création des index uniques sur les entités");
        //Suppression des personnes qui n'ont rien a faire là
        $this->entityManager->getConnection()->prepare("DELETE FROM fonction WHERE (uuid=0x0000000000000000000000000000000 )")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM adhesion WHERE id in (SELECT ad.id FROM (SELECT * FROM adhesion) ad INNER JOIN adherent a on (a.id = ad.adherent_id and a.uuid=0x0000000000000000000000000000000));")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM adherent WHERE (uuid=0x0000000000000000000000000000000 )")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM adhesion WHERE (uuid=0x0000000000000000000000000000000 )")->executeQuery();

        $this->entityManager->getConnection()->prepare("UPDATE sejour SET accompagnant_id=null WHERE accompagnant_id in (SELECT p.id from personne p where p.uuid = 0x0000000000000000000000000000000);")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE from adhesion  where adherent_id in(SELECT a.id from adherent a INNER JOIN personne p ON (p.id=a.personne_id AND p.uuid = 0x0000000000000000000000000000000));")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE from adherent  where personne_id in(SELECT p.id from personne p where p.uuid = 0x0000000000000000000000000000000);")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM role_encadrant_sejour WHERE id in( SELECT e.id from (select* from role_encadrant_sejour) e INNER JOIN personne p ON (p.id=e.encadrant_eedf_id AND p.uuid = 0x0000000000000000000000000000000));")->executeQuery();

        $this->entityManager->getConnection()->prepare("UPDATE `personne` p1 SET p1.`responsable_legal1_id` = NULL WHERE p1.id IN (SELECT p.id FROM (SELECT * FROM personne) p INNER JOIN (SELECT * FROM personne) rl1 ON (p.responsable_legal1_id = rl1.id AND rl1.uuid = 0x0000000000000000000000000000000));")->executeQuery();
        $this->entityManager->getConnection()->prepare("UPDATE `personne` p2 SET p2.`responsable_legal2_id` = NULL WHERE p2.id IN (SELECT p.id FROM (SELECT * FROM personne) p INNER JOIN (SELECT * FROM personne) rl2 ON (p.responsable_legal2_id = rl2.id AND rl2.uuid = 0x0000000000000000000000000000000));")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM personne WHERE (uuid=0x0000000000000000000000000000000 )")->executeQuery();


        $this->entityManager->getConnection()->prepare("DELETE FROM structure WHERE id in (
                    select s.id from (select * from structure) s 
                    inner join (select * from structure) parent 
                    ON (parent.id = s.structure_parent_id)
                    inner join (select * from structure) grand_parent 
                    ON (grand_parent.id = parent.structure_parent_id and grand_parent.uuid =0x0000000000000000000000000000000)
                  );")->executeQuery();

        $this->entityManager->getConnection()->prepare(" DELETE FROM structure WHERE id in (
                    select s.id from (select * from structure) s 
                    inner join (select * from structure) parent 
                    ON (parent.id = s.structure_parent_id and parent.uuid =0x0000000000000000000000000000000)
                  )")->executeQuery();
        $this->entityManager->getConnection()->prepare("DELETE FROM structure WHERE (uuid=0x0000000000000000000000000000000 )")->executeQuery();

        $io->text("Terminé");
        $io->text("Index Structures");
        $this->entityManager->getConnection()->prepare("CREATE UNIQUE INDEX UNIQ_6F0137EAD17F50A6 ON structure (uuid);")->executeQuery();
        $io->text("Index Personnes");
        $this->entityManager->getConnection()->prepare("CREATE UNIQUE INDEX UNIQ_FCEC9EFD17F50A6 ON personne (uuid);")->executeQuery();
        $io->text("Index Adherents");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_90D3F060D17F50A6 ON adherent (uuid)')->executeQuery();
        $io->text("Index Adhesion");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_C50CA65AD17F50A6 ON adhesion (uuid)')->executeQuery();
        $io->text("Index Contrat de travail");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_394729CDD17F50A6 ON contrat_travail (uuid)')->executeQuery();
        $io->text("Index Employe");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_F804D3B9D17F50A6 ON employe (uuid)')->executeQuery();
        $io->text("Index Equipes");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_2449BA15D17F50A6 ON equipe (uuid)')->executeQuery();
        $io->text("Index Fonctions");
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_900D5BDD17F50A6 ON fonction (uuid)')->executeQuery();


        $io->text("Suppression des idJéito");
        $this->entityManager->getConnection()->prepare('ALTER TABLE contrat_travail DROP id_jeito')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adhesion DROP id_jeito')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne DROP id_jeito')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction DROP id_jeito')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure DROP id_jeito')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE equipe DROP id_jeito')->executeQuery();
        $io->text("Terminé");

        $io->section("Lancement de la commmande de synchronisation");
        $env = 'prod';
        $kernel = new Kernel($env, false);
        $application = new Application($kernel);
        $returnCode = $application->run(new ArrayInput([
            'command' => 'roads:synchro-portail',
            '--env' => 'prod',
            '--no-debug' => true
        ]), $output);
        return Command::SUCCESS;
    }


    function loadUuidByIdJeito(String $modele): array
    {
        $result = array();

        $BATCH_SIZE = 1000;
        $apiResult = $this->getAPIResult($modele, $BATCH_SIZE, 0);
        $data  = $apiResult['results'];

        $nb_objets_traites = 0;
        $n = 0;
        $nombreTotalObjets = $apiResult['count'];

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {

            foreach ($data as $dataObject) {
                $result[$dataObject['id']] = $dataObject['uuid'];
                $nb_objets_traites++;
            }
            if ($nombreTotalObjets > $nb_objets_traites) {
                $n++;
                $apiResult =  $this->getAPIResult($modele, $BATCH_SIZE, $n * $BATCH_SIZE);

                $data  = $apiResult['results'];
            } else {
                $dernierePageParcourue = true;
            }
        }

        return $result;
    }
    function getAPIResult(String $urlSuffix, $limit = 100, $offset = 0)
    {
        $httpClientStructures = HttpClient::create();
        $httpClientStructures = new ScopingHttpClient(
            $httpClientStructures,
            [
                $this->jeitoApiUrl . $urlSuffix => [
                    'headers' => [
                        'Authorization' => 'token ' . $this->jeitoApiToken
                    ]
                ]
            ]
        );
        $response = $httpClientStructures->request(
            'GET',
            $this->jeitoApiUrl . $urlSuffix,
            [
                // these values are automatically encoded before including them in the URL
                'query' => [
                    'limit' => $limit,
                    'offset' => $offset
                ]
            ]
        );


        $content = $response->getContent();

        return json_decode($content, true);
    }

    private function migrerTable(array $array_uuid_by_idJeito, $nomTable, $nomIdJeito = "id_jeito")
    {
        $objets = $this->entityManager->getConnection()->prepare("SELECT id, " . $nomIdJeito . " FROM " . $nomTable)->executeQuery();

        while (($objet = $objets->fetchAssociative()) !== false) {
            if (array_key_exists($objet[$nomIdJeito], $array_uuid_by_idJeito)) {
                /** @var Uuid $uuid */
                $this->entityManager->getConnection()->update(
                    $nomTable,
                    [
                        'uuid' => (Uuid::fromString($array_uuid_by_idJeito[$objet[$nomIdJeito]]))->toBinary()
                    ],
                    [
                        'id' => $objet['id']
                    ]
                );
            }
        }
    }
    function migrationDonneesSerialisees(array $donnees, string $type_serialisation): array
    {
        //Infos de l'accompagnant:
        // Personne
        if (array_key_exists('accompagnant', $donnees)) {
            $donnees['accompagnant'] = $this->migrationPersonne($donnees['accompagnant'], $type_serialisation);
        }

        //Infos du directeur

        $donnees['directeur'] = $this->migrationPersonne($donnees['directeur'], $type_serialisation);

        //Infos de structure organisatrice
        if (array_key_exists('structureOrganisatrice', $donnees)) {
            $donnees['structureOrganisatrice'] = $this->migrationStructure($donnees['structureOrganisatrice'], $type_serialisation);
        }
        $resultStructuresRegroupees = array();
        foreach ($donnees['structuresRegroupees'] as $structureRegroupee) {
            $resultStructuresRegroupees[] = $this->migrationStructureRegroupement($structureRegroupee, $type_serialisation);
        }
        $donnees['structuresRegroupees'] = $resultStructuresRegroupees;

        if ($type_serialisation == self::INFOS_SEJOUR) {
            //unitesStructureOrganisatrice
            $unitesStructureOrganisatrice = array();
            if (array_key_exists('unitesStructureOrganisatrice', $donnees)) {
                foreach ($donnees['unitesStructureOrganisatrice'] as $uniteStructureOrganisatrice) {
                    $unitesStructureOrganisatrice[] = $this->migrationParticipationSejourUnite($uniteStructureOrganisatrice, $type_serialisation);
                }
                $donnees['unitesStructureOrganisatrice'] = $unitesStructureOrganisatrice;
            }

            if (array_key_exists('rolesEncadrants', $donnees)) {

                $donnees['rolesEncadrants'] = $this->migrationEncadrant($donnees['rolesEncadrants'], $type_serialisation);
            }
        }
        return $donnees;
    }

    function migrationPersonne(array $dataPersonne, string $type_serialisation): array
    {
        if (array_key_exists('idJeito', $dataPersonne)) {
            if (array_key_exists($dataPersonne["idJeito"], $this->uuidPersonnesByIdJeito)) {
                $dataPersonne["uuid"] = $this->uuidPersonnesByIdJeito[$dataPersonne["idJeito"]];
                unset($dataPersonne["idJeito"]);
            } else {
                $dataPersonne["uuid"] = Uuid::v5(Uuid::v5(Uuid::fromString('6ba7b810-9dad-11d1-80b4-00c04fd430c8'), "jeito.eedf.fr"), "person/" . $dataPersonne["idJeito"])->toRfc4122();
                unset($dataPersonne["idJeito"]);
            }
        }
        if ($type_serialisation == self::INFOS_SEJOUR) {
            if (array_key_exists('responsableLegal1', $dataPersonne)) {
                $dataPersonne['responsableLegal1'] = $this->migrationPersonne($dataPersonne['responsableLegal1'], $type_serialisation);
            }
            if (array_key_exists('responsableLegal2', $dataPersonne)) {
                $dataPersonne['responsableLegal2'] = $this->migrationPersonne($dataPersonne['responsableLegal2'], $type_serialisation);
            }
        }



        return $dataPersonne;
    }

    function migrationEquipe(array $dataEquipe, string $type_serialisation): array
    {
        if (array_key_exists('idJeito', $dataEquipe)) {
            if (array_key_exists($dataEquipe["idJeito"], $this->uuidEquipesByIdJeito)) {
                $dataEquipe["uuid"] = $this->uuidEquipesByIdJeito[$dataEquipe["idJeito"]];
                unset($dataEquipe["idJeito"]);
            }
        }
        return $dataEquipe;
    }
    function migrationStructure(array $dataStructure, string $type_serialisation): array
    {
        if (array_key_exists('idJeito', $dataStructure)) {
            if (array_key_exists($dataStructure["idJeito"], $this->uuidStructuresByIdJeito)) {
                $dataStructure['uuid'] = $this->uuidStructuresByIdJeito[$dataStructure["idJeito"]];
                unset($dataStructure["idJeito"]);
            }
        }

        if ($type_serialisation == self::INFOS_SEJOUR) {
            if (
                array_key_exists('equipes', $dataStructure)
            ) {
                $array_equipes = array();
                foreach ($dataStructure['equipes'] as $dataEquipe) {
                    $array_equipes[] = $this->migrationEquipe($dataEquipe, $type_serialisation);
                }
                $dataStructure['equipes'] = $array_equipes;
            }
        }
        if (
            array_key_exists('structureParent', $dataStructure)
            && ($dataStructure['structureParent'] !== null)
        ) {
            $dataStructure['structureParent'] = $this->migrationStructure($dataStructure['structureParent'], $type_serialisation);
        }
        return $dataStructure;
    }


    public function migrationStructureRegroupement(array $dataStructureRegroupee, $type_serialisation): array
    {

        //Cas du changement de nom d'unse structure entre temps... c'est mieux que rien !
        if (
            array_key_exists('structureEEDF', $dataStructureRegroupee)
            && ($dataStructureRegroupee['structureEEDF'] !== null)
        ) {
            $dataStructureRegroupee['structureEEDF'] = $this->migrationStructure($dataStructureRegroupee['structureEEDF'], self::INFOS_DECLARATION);
        }


        if ($type_serialisation == self::INFOS_SEJOUR) {

            if (
                array_key_exists('unitesParticipantes', $dataStructureRegroupee)
            ) {
                $unitesParticipantes = array();
                foreach ($dataStructureRegroupee['unitesParticipantes'] as $dataUniteParticipante) {
                    $unitesParticipantes[] = $this->migrationParticipationSejourUnite($dataUniteParticipante, $type_serialisation);
                }
                $dataStructureRegroupee['unitesParticipantes'] = $unitesParticipantes;
            }


            if (
                array_key_exists('encadrants', $dataStructureRegroupee)
            ) {
                $encadrants = array();
                foreach ($dataStructureRegroupee['encadrants'] as $dataEncadrant) {
                    $encadrants[] = $this->migrationEncadrant($dataEncadrant, $type_serialisation);
                }
                $dataStructureRegroupee['encadrants'] = $encadrants;
            }
        }
        return $dataStructureRegroupee;
    }
    //ParticipationSejourUnite
    public function migrationParticipationSejourUnite(array $dataUniteParticipante, $type_serialisation): array
    {
        $participantsAdherents = array();
        foreach ($dataUniteParticipante['participantsAdherents'] as $dataParticipantAdherent) {
            $participantsAdherents[] = $this->migrationPersonne($dataParticipantAdherent, $type_serialisation);
        }
        $result['participantsAdherents'] = $participantsAdherents;

        if (array_key_exists('equipe', $dataUniteParticipante)) {
            $dataUniteParticipante['equipe'] = $this->migrationEquipe($dataUniteParticipante['equipe'], $type_serialisation);
        }

        return $dataUniteParticipante;
    }


    public function migrationEncadrant(array $dataEncadrant, $type_serialisation): array
    {
        if (array_key_exists('encadrantEEDF', $dataEncadrant)) {
            $dataEncadrant['encadrantEEDF'] = $this->migrationPersonne($dataEncadrant['encadrantEEDF'], $type_serialisation);
        }

        //InfosEncadrantNonEEDF
        if (array_key_exists('uniteParticipante', $dataEncadrant)) {
            $dataEncadrant['uniteParticipante'] = $this->migrationParticipationSejourUnite($dataEncadrant['uniteParticipante'], $type_serialisation);
        }

        return $dataEncadrant;
    }

    function afficherMessage(String $message, int $niveauInformation, SymfonyStyle $io)
    {
        switch ($niveauInformation) {
            case self::NIVEAU_MESSAGE_SUCCESS:
                $io->success($message);
                break;
            case self::NIVEAU_MESSAGE_INFO:
                $io->info($message);
                break;
            case self::NIVEAU_MESSAGE_WARNING:
                $io->warning($message);
                $message = "Attention: " . $message;
                break;
            case self::NIVEAU_MESSAGE_TEXT:
                $io->text($message);
                break;
            case self::NIVEAU_MESSAGE_SECTION:
                $io->section($message);
                break;
        }
    }
}
