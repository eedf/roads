<?php

namespace App\Command;

use App\Entity\Sejour;
use App\Repository\SejourRepository;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Commande quotidienne de mise à jour des séjours pour les faire passer de "validé" à "en cours" et de "en cours" à "terminé", et de suppression des séjours de plus de 3 ans
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:maj-etats-sejours',
    description: 'Commande quotidienne de mise à jour des séjours pour les faire passer de "validé" à "en cours" et de "en cours" à "terminé", et de suppression des séjours de plus de 3 ans',
)]
class RoadsMajEtatsSejours extends Command
{

    private $sejourRepository;
    private $entityManager;
    private $sejourWorkflow;
    private $notificationHelper;
    private $serializer;

    /**
     * __construct
     *
     * @param  EntityManagerInterface $entityManager
     * @param  SejourRepository $sejourRepository
     * @param  WorkflowInterface $sejourWorkflow
     * @param  mixed $notificationHelper
     * @param  mixed $serializer
     * @return void
     */
    public function __construct(EntityManagerInterface $entityManager, SejourRepository $sejourRepository, WorkflowInterface $sejourWorkflow, NotificationHelper $notificationHelper, SerializerInterface  $serializer)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->sejourRepository = $sejourRepository;
        $this->entityManager = $entityManager;
        $this->sejourWorkflow = $sejourWorkflow;
        $this->notificationHelper = $notificationHelper;
        $this->serializer = $serializer;

        parent::__construct();
    }


    protected function configure() {}

    /**
     * Mise à jour des séjours pour les faire passer 
     *  - de "validé" à "en cours" lors de leur date prévue de démarrage
     *  - de "en cours" à "terminé" lors de leur date de fin
     *  - de "terminé" à "cloturé" si terminés depuis plus de 90 jours, 
     *  - et de suppression des séjours de plus de 3 ans
     *
     * @param  mixed $input
     * @param  mixed $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info("Recherche des séjours...");



        $io->info("Recherche des séjours validés à faire démarrer ...");
        $sejoursAFairePasserAEnCours = $this->sejourRepository->findSejoursADemarrer(new DateTime());
        /** @var Sejour $sejourAFairePasserAEnCours */
        $nbSejoursDemarres = 0;
        foreach ($sejoursAFairePasserAEnCours as $sejourAFairePasserAEnCours) {

            if ($this->sejourWorkflow->can($sejourAFairePasserAEnCours, 'debut_sejour')) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_026, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourAFairePasserAEnCours
                ]);
                $this->sejourWorkflow->apply($sejourAFairePasserAEnCours, 'debut_sejour');
                $this->entityManager->persist($sejourAFairePasserAEnCours);
                $this->entityManager->flush();
                $nbSejoursDemarres++;
            }
        }
        $io->success("Terminé: " . $nbSejoursDemarres . " séjours ont démarré.");


        $io->info("Recherche des séjours en cours à faire se terminer ...");
        $sejoursAFairePasserATermine = $this->sejourRepository->findSejoursATerminer(new DateTime());
        /** @var Sejour $sejourAFairePasserATermine */
        $nbSejoursTermines = 0;
        foreach ($sejoursAFairePasserATermine as $sejourAFairePasserATermine) {

            if ($this->sejourWorkflow->can($sejourAFairePasserATermine, 'fin_sejour')) {


                $jsonContent = $this->serializer->serialize(
                    $sejourAFairePasserATermine,
                    'json',
                    [
                        'groups' => 'fin_sejour',
                        AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                        AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                        AbstractObjectNormalizer::CIRCULAR_REFERENCE_LIMIT => 2,
                        AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                            return null;
                        },
                    ]
                );
                $sejourAFairePasserATermine->setInfosSejour($jsonContent);

                $this->sejourWorkflow->apply($sejourAFairePasserATermine, 'fin_sejour');
                $this->entityManager->persist($sejourAFairePasserATermine);
                $this->entityManager->flush();

                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_026bis, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourAFairePasserATermine
                ]);
                $nbSejoursTermines++;
            }
        }
        $io->success("Terminé: " . $nbSejoursTermines . " séjours se sont terminés.");


        //On cloture automatiquement les séjours terminés depuis 90 jours au moins
        $io->info("Recherche des séjours terminés à cloturer automatiquement ...");
        $dateLimiteSejourTermine = new DateTime();
        $dateLimiteSejourTermine->modify("- 90 days")->setTime(0, 0);
        $sejoursACloturer = $this->sejourRepository->findSejoursACloturer($dateLimiteSejourTermine);
        /** @var Sejour $sejoursACloturer */
        $nbSejoursClotures = 0;
        foreach ($sejoursACloturer as $sejourACloturer) {

            if ($this->sejourWorkflow->can($sejourACloturer, 'cloture_sejour')) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_027, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourACloturer
                ]);
                $this->sejourWorkflow->apply($sejourACloturer, 'cloture_sejour');
                $sejourACloturer->setDateModification(new DateTime());
                $this->entityManager->persist($sejourACloturer);
                $this->entityManager->flush();
                $nbSejoursClotures++;
            }
        }
        $io->success("Terminé: " . $nbSejoursClotures . " séjours ont été automatiquement cloturés.");

        /*

        //Suppression des Séjours non mis à jour depuis 3 ans:
        $dateDerniereMajSejour = new DateTime();
        $dateDerniereMajSejour->sub(new DateInterval('P3Y'));;
        $io->info("Recherche des séjours de plus de 3 ans à supprimer ...");

        $sejoursASupprimer = $this->sejourRepository->findEndedBefore($dateDerniereMajSejour);
        $io->progressStart(count($sejoursASupprimer));
        $nb_sejours_traites = 0;
        foreach ($sejoursASupprimer as $sejourASupprimer) {
            $this->entityManager->remove($sejourASupprimer);
            $io->progressAdvance();
            $nb_sejours_traites++;
            if ($nb_sejours_traites % 1000 == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $memoryUsage = memory_get_usage(true) / 1024 / 1024;
                $io->text("Batch finished with memory: ${memoryUsage}M");
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $io->progressFinish();

        $message = 'Terminé: ' . $nb_sejours_traites . ' anciens séjours ont été supprimés de R.O.A.D.S';
        $io->success($message);
*/
        return Command::SUCCESS;
    }
}
