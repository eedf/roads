<?php

namespace App\Command;

use App\Entity\Sejour;
use App\Repository\SejourRepository;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use DateTime;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 * Commande quotidienne d'envoi de notifications de rappels
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:envoi-rappels-notifications',
    description: 'Commande quotidienne d\'envoi de notifications de rappels',
)]
class RoadsEnvoiNotificationsRappelsSejours extends Command
{

    private $sejourRepository;
    private $notificationHelper;

    public function __construct(SejourRepository $sejourRepository, NotificationHelper $notificationHelper)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->sejourRepository = $sejourRepository;
        $this->notificationHelper = $notificationHelper;

        parent::__construct();
    }


    protected function configure() {}

    /**
     * Envoi des notifications de rappels sur les séjours
     * Envois des notifications pour :
     * - Séjours toujours en construction à 30 jours du début du séjour
     * - Séjours en attente de validation depuis plus de 15 jours
     * - Séjours en statut terminés depuis plus de 30 jours
     * - Séjours en statut terminés depuis 75 jours
     * - Séjours en attente de declaration d'intention depuis plus de 30 jours
     * @param  mixed $input
     * @param  mixed $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info("Recherche des séjours...");


        $dateCourante = new DateTime();
        $dateCourante->setTime(0, 0);

        //----------------------------------------------------------------------
        //Séjours en toujours en construction à 30 jours du début du séjour
        $io->info("Recherche des séjours en construction ...");
        $sejoursConstruction = $this->sejourRepository->findSejourParEtat(Sejour::STATUT_SEJOUR_CONSTRUCTION_PROJET);

        /** @var Sejour $sejourConstruction */
        $nbSejoursConstructionNotif = 0;
        foreach ($sejoursConstruction as $sejourConstruction) {
            /** @var DateTime $dateDebut */
            $dateDebut = clone $sejourConstruction->getDateDebut();

            if ($dateDebut->modify("- 30 days")->setTime(0, 0) == $dateCourante) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_076, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourConstruction
                ]);
                $nbSejoursConstructionNotif++;
            }
        }

        $io->success($nbSejoursConstructionNotif . " notifications de rappel ont été envoyées.");



        //----------------------------------------------------------------------
        //Séjours en attente de validation depuis plus de 15 jours
        $io->info("Recherche des séjours en attente de validation ...");
        $sejoursAttenteValidation = $this->sejourRepository->findSejourParEtat(Sejour::STATUT_SEJOUR_ATTENTE_VALIDATION);

        /** @var Sejour $sejourAttenteValidation */
        $nbSejoursTropLongtempsEnAttenteValidation = 0;
        foreach ($sejoursAttenteValidation as $sejourAttenteValidation) {

            if ($sejourAttenteValidation->getDateEnvoiValidation()) {
                /** @var DateTime $dateEnvoiValidation */
                $dateEnvoiValidation = clone $sejourAttenteValidation->getDateEnvoiValidation();
                if ($dateEnvoiValidation->modify("+ 15 days")->setTime(0, 0) == $dateCourante) {
                    $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_073, [
                        NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourAttenteValidation
                    ]);
                    $nbSejoursTropLongtempsEnAttenteValidation++;
                }
            }
        }
        $io->success($nbSejoursTropLongtempsEnAttenteValidation . " notifications de rappel ont été envoyées.");

        //----------------------------------------------------------------------
        //Séjours en terminés depuis plus de 30 jours
        $io->info("Recherche des séjours terminés depuis 30 jours ...");
        $sejoursTermines = $this->sejourRepository->findSejourParEtat(Sejour::STATUT_SEJOUR_TERMINE);

        /** @var Sejour $sejourTermine */
        $nbSejoursTropLongtempsTermines = 0;
        foreach ($sejoursTermines as $sejourTermine) {
            /** @var DateTime $dateTermine */
            $dateTermine = clone $sejourTermine->getDateFin();
            if ($dateTermine->modify("+ 30 days")->setTime(0, 0) == $dateCourante) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_074, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourTermine
                ]);
                $nbSejoursTropLongtempsTermines++;
            }
        }

        $io->success($nbSejoursTropLongtempsTermines . " notifications de rappel ont été envoyées.");
        //----------------------------------------------------------------------
        //Séjours en terminés depuis 75 jours
        $io->info("Recherche des séjours terminés depuis 75 jours ...");
        $sejoursTermines = $this->sejourRepository->findSejourParEtat(Sejour::STATUT_SEJOUR_TERMINE);

        /** @var Sejour $sejourTermine */
        $nbSejoursTropLongtempsTermines = 0;
        foreach ($sejoursTermines as $sejourTermine) {
            /** @var DateTime $dateTermine */
            $dateTermine = clone $sejourTermine->getDateFin();
            if ($dateTermine->modify("+ 75 days")->setTime(0, 0) == $dateCourante) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_079, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourTermine
                ]);
                $nbSejoursTropLongtempsTermines++;
            }
        }

        $io->success($nbSejoursTropLongtempsTermines . " notifications de rappel n°2 ont été envoyées.");

        //----------------------------------------------------------------------
        //Séjours qui commencent dans 45 jours
        $io->info("Recherche des séjours qui commencent dans 45 jours ...");
        $sejours = $this->sejourRepository->findTousSejoursDeclaresNonDemarres();

        /** @var Sejour $sejour */
        $nbSejoursNotifies = 0;
        foreach ($sejours as $sejour) {
            /** @var DateTime $dateDebutSejour */
            $dateDebutSejour = clone $sejour->getDateDebut();
            if ($dateDebutSejour->modify("- 45 days")->setTime(0, 0) == $dateCourante) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_084, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour
                ]);
                $nbSejoursNotifies++;
            }
        }

        $io->success($nbSejoursNotifies . " notifications de rappel n°3 ont été envoyées.");

        //----------------------------------------------------------------------
        //Séjours qui commencent dans 8 jours
        $io->info("Recherche des séjours qui commencent dans 45 jours ...");
        $sejours = $this->sejourRepository->findTousSejoursDeclaresNonDemarres();

        /** @var Sejour $sejour */
        $nbSejoursNotifies = 0;
        foreach ($sejours as $sejour) {
            /** @var DateTime $dateDebutSejour */
            $dateDebutSejour = clone $sejour->getDateDebut();
            if ($dateDebutSejour->modify("- 8 days")->setTime(0, 0) == $dateCourante) {
                $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_085, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour
                ]);
                $nbSejoursNotifies++;
            }
        }

        $io->success($nbSejoursNotifies . " notifications de rappel n°4 ont été envoyées.");
        //----------------------------------------------------------------------
        //Séjours en attente de declaration d'intention depuis plus de 30 jours
        $io->info("Recherche des séjours en cours de déclaration d'intention ...");
        $sejoursDeclarationIntention = $this->sejourRepository->findSejourParEtat(Sejour::STATUT_SEJOUR_DECLARATION_INTENTION);

        /** @var Sejour $sejourDeclarationIntention */
        $nbSejoursTropLongtempsEnDeclarationIntention = 0;
        foreach ($sejoursDeclarationIntention as $sejourDeclarationIntention) {
            if ($sejourDeclarationIntention->getDateCreation()) {
                /** @var DateTime $dateCreation */
                $dateCreation = clone $sejourDeclarationIntention->getDateCreation();

                if ($dateCreation->modify("+ 30 days")->setTime(0, 0) == $dateCourante) {
                    $this->notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_072, [
                        NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourDeclarationIntention
                    ]);
                    $nbSejoursTropLongtempsEnDeclarationIntention++;
                }
            }
        }

        $io->success($nbSejoursTropLongtempsEnDeclarationIntention . " notifications de rappel ont été envoyées.");



        return Command::SUCCESS;
    }
}
