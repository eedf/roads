<?php

namespace App\Command;

use DateTime;
use Exception;
use DateInterval;
use App\Entity\Equipe;
use App\Entity\Employe;
use App\Entity\Adherent;
use App\Entity\Adhesion;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Entity\ContratTravail;
use App\Entity\ParametreRoads;
use App\Repository\EquipeRepository;
use App\Repository\EmployeRepository;
use App\Repository\AdherentRepository;
use App\Repository\AdhesionRepository;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ContratTravailRepository;
use App\Repository\ParametreRoadsRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\OutputStyle;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Fonction;
use App\Repository\FonctionRepository;
use App\Service\JeitoSyncronisationHelper;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Commande de synchronisation des données depuis Jéito avec la BDD de ROADS
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:synchro-jeito',
    description: "Récupère les données depuis Jeito, et met à jour la base de données avec les objets Structure, Equipe, Personne, Adhérent, Adhesion, Salarié, Contrat de travail et Fonction"
)]
class RoadsSynchroJeitoCommand extends Command
{

    const NIVEAU_MESSAGE_SUCCESS = 0;
    const NIVEAU_MESSAGE_INFO = 1;
    const NIVEAU_MESSAGE_WARNING = 2;
    const NIVEAU_MESSAGE_TEXT = 3;
    const NIVEAU_MESSAGE_SECTION = 4;



    private $entityManager;
    private EquipeRepository $equipeRepository;
    private $structureRepository;
    private PersonneRepository $personneRepository;
    private AdherentRepository $adherentRepository;
    private EmployeRepository $employeRepository;
    private ContratTravailRepository $contratTravailRepository;
    private AdhesionRepository $adhesionRepository;
    private FonctionRepository $fonctionRepository;
    private JeitoSyncronisationHelper $jeitoSyncronisationHelper;
    private $parametreRoadsRepository;
    private $mailer;
    private array $infosForSupport;

    /**
     * @var string 
     */
    private $jeitoApiUrl;
    /**
     * @var string 
     */
    private $jeitoApiToken;
    public function __construct(
        EntityManagerInterface $entityManager,
        StructureRepository $structureRepository,
        EquipeRepository $equipeRepository,
        FonctionRepository $fonctionRepository,
        AdhesionRepository $adhesionRepository,
        PersonneRepository $personneRepository,
        EmployeRepository $employeRepository,
        AdherentRepository $adherentRepository,
        ContratTravailRepository $contratTravailRepository,
        ParametreRoadsRepository $parametreRoadsRepository,
        JeitoSyncronisationHelper $jeitoSyncronisationHelper,
        MailerInterface $mailer,
        String $jeitoApiToken,
        String $jeitoApiUrl
    ) {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;
        $this->structureRepository = $structureRepository;
        $this->equipeRepository = $equipeRepository;
        $this->personneRepository = $personneRepository;
        $this->parametreRoadsRepository = $parametreRoadsRepository;
        $this->adherentRepository = $adherentRepository;
        $this->employeRepository = $employeRepository;
        $this->contratTravailRepository = $contratTravailRepository;
        $this->adhesionRepository = $adhesionRepository;
        $this->fonctionRepository = $fonctionRepository;
        $this->mailer = $mailer;
        $this->jeitoApiToken = $jeitoApiToken;
        $this->jeitoApiUrl = $jeitoApiUrl;
        $this->jeitoSyncronisationHelper = $jeitoSyncronisationHelper;

        $this->infosForSupport = array();

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    /**
     * Récupère les données depuis Jeito, et met à jour la base de données des adhérent.e.s et des structures.
     * 
     * Phase 1: MAJ des structures et unités:
     *  - Appel de la prise des structures et unités Jeito 
     *  - Tri des structures pour ordonner la mise à jour/création du sommet vers la base
     *  - Mise à jour des structures
     *  - Désactivation des structures non mises à jour
     *  - Mise à jour des unités
     *  - Désactivation des unités non mises à jour
     * 
     * Phase 2: MAJ des adhérent·e·s et des fonctions associatives:
     *  - Appel de la prise des adhérent·e·s Jeito
     *  - Mise à jour/création des adhérents et de leurs fonctions à partir des résultats de Jeito (si l'adresse mail est incorrecte, l'adhérent·e n'est pas ajouté·e/mis·e à jour, et une erreur est affichée)
     *  - Désactivation des adhérent·e·s qui n'ont pas été mis.e·s à jour cette fois-ci (statut d'adhésion positionné à "Non adhérent.e")
     *  - Suppression de la BDD des adhérent·es non mis·e·s à jour depuis au moins 3 ans
     * 
     * Phase 3: Un résumé de la synchronisation est envoyé par mail à l'adresse de support de ROADS
     * @param  mixed $input
     * @param  mixed $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $dateStart = new DateTime();
            $sqlLogger = $this->entityManager->getConnection()->getConfiguration()->getSQLLogger();
            $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
            $io = new SymfonyStyle($input, $output);
            $dateCourante = new DateTime();
            $this->creationMAJStructure($io, $dateCourante);
            $this->creationMAJEquipes($io, $dateCourante);
            $this->creationMAJPersonnes($io, $dateCourante);
            $this->creationMAJAdherent($io, $dateCourante);
            $this->creationMAJAdhesions($io, $dateCourante);

            $this->creationMAJEmploye($io, $dateCourante);
            $this->creationMAJContratsTravail($io, $dateCourante);

            $this->creationMAJFonctions($io, $dateCourante);

            //A la fin, on supprime toutes les entrées qui n'ont pas été mises à jour avec cette synchro (cf date Modification)

            $this->suppressionFonctionsObsoletes($io, $dateCourante);
            $this->suppressionContratsTravailObsoletes($io, $dateCourante);
            $this->suppressionEmployesObsoletes($io, $dateCourante);
            $this->suppressionAdhesionsObsoletes($io, $dateCourante);
            $this->suppressionAdherentsObsoletes($io, $dateCourante);
            $this->suppressionPersonnesObsoletes($io, $dateCourante);
            $this->suppressionEquipesObsoletes($io, $dateCourante);
            $this->suppressionStructuresObsoletes($io, $dateCourante);

            $this->afficherConsoMemoire($io);

            $this->envoiMailRecapsynchroSupport($io);
            $dateFinish = new DateTime();
            $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
            $io->success('Commande de synchronisation avec Jéito terminée en ' . $dureeTraitement);
        } catch (Exception $e) {
            $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            $this->envoiMailRecapsynchroSupport($io, true);
            $io->error('Erreur lors de la commande de synchronisation avec Jéito');
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }


    /**
     * Transforme la valeur de numéro de téléphone pour la mettre au bon format
     *
     * @param  String $value
     * @return String
     */
    function transformTel(String $value): String
    {
        if (strlen($value) == 10 && str_starts_with($value, "0")) {
            $value = "+33" . substr($value, 1);
        }
        return ($value);
    }

    function creationMAJStructure(SymfonyStyle $io, DateTime $dateCourante)
    {
        $dateStart = new DateTime();
        $STRUCTURE = "structure";
        $BATCH_SIZE = 1000;

        //Création/mise à jour des structures:
        $this->afficherMessage('1 - Mise à jour des structures:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage('1 a) - 1er appel: API Récupération des structures:', self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult =  $this->getAPIResult($STRUCTURE, $BATCH_SIZE, 0);
        $dataStructures  = $apiResult['results'];
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);


        $message = '1 b) - Création et mise à jour des structures...';
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_INFO, $io);
        //On crée/met à jour d'abord les structures:
        $nb_structures_traitees = 0;
        $n = 0;
        $nombreTotalStructures = $apiResult['count'];


        $io->progressStart($nombreTotalStructures);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayStructuresUuidsACharger = array();
            foreach ($dataStructures as $dataStructure) {
                $arrayStructuresUuidsACharger[] = $dataStructure['uuid'];
                if ($dataStructure['parent_uuid']) {
                    $arrayStructuresUuidsACharger[] = $dataStructure['parent_uuid'];
                }
            }
            $structuresChargees = $this->structureRepository->findWhereUuidIn($arrayStructuresUuidsACharger);

            $structuresByUuid = array();
            /** @var Structure $structure */
            foreach ($structuresChargees as $structure) {
                $structuresByUuid[$structure->getUuid()->toRfc4122()] = $structure;
            }
            //Libération mémoire
            $structuresChargees = null;
            unset($structuresChargees);
            $arrayStructuresIdsACharger = null;
            unset($arrayStructuresIdsACharger);
            $apiResult = null;
            unset($apiResult);
            foreach ($dataStructures as  $donneesStructure) {
                $structure = null;
                $uuid = $donneesStructure['uuid'];
                if (array_key_exists($uuid, $structuresByUuid)) {
                    $structure = $structuresByUuid[$uuid];
                }
                $parent_uuid = $donneesStructure['parent_uuid'];
                $structureParent = null;
                if (array_key_exists($parent_uuid, $structuresByUuid)) {
                    $structureParent = $structuresByUuid[$parent_uuid];
                }

                try {
                    $structuresByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateStructure($structure, $structureParent, $donneesStructure, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }
                $nb_structures_traitees++;
                $io->progressAdvance();
            }
            //Libération mémoire
            $structuresByUuid = null;
            unset($structuresByUuid);
            $dataStructures = null;
            unset($dataStructures);
            if ($nombreTotalStructures > $nb_structures_traitees) {
                $n++;
                $apiResult =  $this->getAPIResult($STRUCTURE, $BATCH_SIZE, $n * $BATCH_SIZE);
                $dataStructures  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }


        $io->progressFinish();
        $this->entityManager->flush();
        $this->entityManager->clear();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_structures_traitees . ' structures traitées en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
        $this->afficherConsoMemoire($io);
    }

    function creationMAJEquipes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $EQUIPE = "team";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('2 - Mise à jour des équipes:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage('2 a) - API Récupération des équipes:', self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($EQUIPE, $BATCH_SIZE);
        $dataEquipes = $apiResult['results'];
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);

        $message = '2 b) - Création et mise à jour des équipes...';
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_INFO, $io);
        $n = 0;
        $nombreTotalEquipes = $apiResult['count'];


        $io->progressStart($nombreTotalEquipes);
        $nb_equipes_traitees = 0;

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayEquipesUuidsACharger = array();
            $arrayStructureUuidsACharger = array();
            $equipesByUuid = array();
            $structuresByUuid = array();
            foreach ($dataEquipes as $dataEquipe) {
                $arrayEquipesUuidsACharger[] = $dataEquipe['uuid'];
                $arrayStructureUuidsACharger[] = $dataEquipe['structure_uuid'];
            }
            $equipesChargees = $this->equipeRepository->findWhereUuidIn($arrayEquipesUuidsACharger);
            $structuresChargees = $this->structureRepository->findWhereUuidIn($arrayStructureUuidsACharger);
            /** @var Equipe $equipe */
            foreach ($equipesChargees as $equipe) {
                $equipesByUuid[$equipe->getUuid()->toRfc4122()] = $equipe;
            }
            //Libération mémoire
            $equipesChargees = null;
            unset($equipesChargees);
            $arrayEquipesUuidsACharger = null;
            unset($arrayEquipesUuidsACharger);
            /** @var Structure $structure */
            foreach ($structuresChargees as $structure) {
                $structuresByUuid[$structure->getUuid()->toRfc4122()] = $structure;
            }
            //Libération mémoire
            $structuresChargees = null;
            unset($structuresChargees);
            $arrayStructureUuidsACharger = null;
            unset($arrayStructureUuidsACharger);

            foreach ($dataEquipes as $dataEquipe) {

                /** @var Equipe $equipe */
                $equipe = null;

                //Objet Equipe
                $uuid = $dataEquipe['uuid'];
                if (array_key_exists($uuid, $equipesByUuid)) {
                    $equipe = $equipesByUuid[$uuid];
                }

                //Ajout de la structure parent
                $structure_uuid = $dataEquipe['structure_uuid'];
                /** @var Structure $structureParent */
                $structureParent = null;
                if ($structure_uuid) {
                    if (array_key_exists($structure_uuid, $structuresByUuid)) {
                        $structureParent = $structuresByUuid[$structure_uuid];
                    }
                }
                try {
                    $equipesByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateEquipe($equipe, $structureParent, $dataEquipe, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }


                $nb_equipes_traitees++;
                $io->progressAdvance();

                //Libération mémoire
                $equipe = null;
                unset($equipe);
                $structureParent = null;
                unset($structureParent);
                $dataEquipe = null;
                unset($dataEquipe);
            }
            //Libération mémoire
            $structuresByUuid = null;
            unset($structuresByUuid);
            $equipesByUuid = null;
            unset($equipesByUuid);
            $dataEquipes = null;
            unset($dataEquipes);

            if ($nombreTotalEquipes > $nb_equipes_traitees) {
                $n++;
                $apiResult =  $this->getAPIResult($EQUIPE, $BATCH_SIZE, $n * $BATCH_SIZE);
                $dataEquipes  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_equipes_traitees . ' équipes traitées en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
        $this->afficherConsoMemoire($io);
    }

    function creationMAJPersonnes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $PERSONNE = "person";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('3 - Mise à jour des personnes:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("3 a) - API de récupération des personnes:", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($PERSONNE, $BATCH_SIZE, 0);
        $dataPersonnes  = $apiResult['results'];
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);


        $this->afficherConsoMemoire($io);
        $this->afficherMessage("3 b) - Création et mise à jour des personnes", self::NIVEAU_MESSAGE_SECTION, $io);
        $nb_personnes_traitees = 0;

        $contrainteEmailValid = new Assert\Email([
            'message' => 'L\'email "{{ value }}" n\'est pas valide.'
        ]);

        $n = 0;
        $nombreTotalPersonnes = $apiResult['count'];
        $io->progressStart($nombreTotalPersonnes);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayPersonnesUuidsACharger = array();
            $personnesByUuid = array();

            foreach ($dataPersonnes as $dataPersonne) {
                $arrayPersonnesUuidsACharger[] = $dataPersonne['uuid'];
                if ($dataPersonne['legal_guardian1_uuid']) {
                    $arrayPersonnesUuidsACharger[] = $dataPersonne['legal_guardian1_uuid'];
                }
                if ($dataPersonne['legal_guardian2_uuid']) {
                    $arrayPersonnesUuidsACharger[] = $dataPersonne['legal_guardian2_uuid'];
                }
                //On charge les doublons déclarés
                foreach ($dataPersonne['merged_with'] as $uuidDoublon) {
                    $arrayPersonnesUuidsACharger[] = $uuidDoublon;
                }
            }
            $personnesChargees = $this->personneRepository->findWhereUuidIn($arrayPersonnesUuidsACharger);
            /** @var Personne $personne */
            foreach ($personnesChargees as $personne) {
                $personnesByUuid[$personne->getUuid()->toRfc4122()] = $personne;
            }
            //Libération mémoire
            $personnesChargees = null;
            unset($personnesChargees);
            $arrayPersonnesUuidsACharger = null;
            unset($arrayPersonnesUuidsACharger);
            $apiResult = null;
            unset($apiResult);
            foreach ($dataPersonnes as $dataPersonne) {
                $doublons = array();
                $personne = null;
                $responsableLegal1 = null;
                $responsableLegal2 = null;
                $uuid = $dataPersonne['uuid'];
                /** @var Personne $personne */
                if (array_key_exists($uuid, $personnesByUuid)) {
                    $personne = $personnesByUuid[$uuid];
                }

                $uuidResponsableLegal1 = $dataPersonne['legal_guardian1_uuid'];
                if (array_key_exists($uuidResponsableLegal1, $personnesByUuid)) {
                    $responsableLegal1 = $personnesByUuid[$uuidResponsableLegal1];
                }

                $uuidResponsableLegal2 = $dataPersonne['legal_guardian2_uuid'];
                if (array_key_exists($uuidResponsableLegal2, $personnesByUuid)) {
                    $responsableLegal2 = $personnesByUuid[$uuidResponsableLegal2];
                }
                foreach ($dataPersonne['merged_with'] as $uuidDoublon) {
                    if (array_key_exists($uuidDoublon, $personnesByUuid)) {
                        $doublons[] = $personnesByUuid[$uuidDoublon];
                    }
                }
                try {
                    $personnesByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdatePersonne($personne, $responsableLegal1, $responsableLegal2, $doublons, $dataPersonne, $dateCourante, $contrainteEmailValid);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }
                $nb_personnes_traitees++;
                $io->progressAdvance();
            }

            $personnesByUuid = null;
            unset($personnesByUuid);
            $dataPersonnes = null;
            unset($dataPersonnes);

            if ($nombreTotalPersonnes > $nb_personnes_traitees) {
                $n++;
                $apiResult =  $this->getAPIResult($PERSONNE, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataPersonnes  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_personnes_traitees . ' personnes traitées en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }

    function creationMAJAdherent(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $ADHERENT = "adherent";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('4 - Mise à jour des adhérent·e·s:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("4 a) - API de récupération des types des adhérent·e·s", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($ADHERENT, $BATCH_SIZE, 0);
        $dataAdherents = $apiResult['results'];
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);


        $this->afficherConsoMemoire($io);
        $this->afficherMessage("4 b) - Création et mise à jour des adhérent·e·s", self::NIVEAU_MESSAGE_SECTION, $io);

        $nb_adherents_traites = 0;
        $n = 0;
        $nombreTotalAdherents = $apiResult['count'];
        $io->progressStart($nombreTotalAdherents);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayUuidAdherentsACharger = array();
            $adherentsByUuid = array();
            $arrayPersonnesUuidsACharger = array();
            $personnesByUuid = array();
            foreach ($dataAdherents as $dataAdherent) {
                $arrayUuidAdherentsACharger[] = $dataAdherent['uuid'];
                $arrayPersonnesUuidsACharger[] = $dataAdherent['person_uuid'];
            }
            $adherentsCharges = $this->adherentRepository->findWhereUuidIn($arrayUuidAdherentsACharger);
            /** @var Adherent $adherent */
            foreach ($adherentsCharges as $adherent) {
                $adherentsByUuid[$adherent->getUuid()->toRfc4122()] = $adherent;
            }

            $personnesChargees = $this->personneRepository->findWhereUuidIn($arrayPersonnesUuidsACharger);
            /** @var Personne $personne */
            foreach ($personnesChargees as $personne) {
                $personnesByUuid[$personne->getUuid()->toRfc4122()] = $personne;
            }
            //Libération mémoire
            $adherentsCharges = null;
            unset($adherentsCharges);
            $arrayUuidAdherentsACharger = null;
            unset($arrayUuidAdherentsACharger);

            $personnesChargees = null;
            unset($personnesChargees);
            $arrayPersonnesUuidsACharger = null;
            unset($arrayPersonnesUuidsACharger);

            $apiResult = null;
            unset($apiResult);

            foreach ($dataAdherents as $dataAdherent) {
                $personne = null;
                $adherent = null;
                $uuid = $dataAdherent['uuid'];
                if (array_key_exists($uuid, $adherentsByUuid)) {
                    $adherent = $adherentsByUuid[$uuid];
                }
                $uuidPersonne = $dataAdherent['person_uuid'];
                if (array_key_exists($uuidPersonne, $personnesByUuid)) {
                    $personne = $personnesByUuid[$uuidPersonne];
                }

                try {
                    $adherentsByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateAdherent($adherent, $personne, $dataAdherent, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }

                $nb_adherents_traites++;
                $io->progressAdvance();


                //Libération mémoire
                $personne = null;
                unset($personne);
                $adherent = null;
                unset($adherent);
            }
            //Libération mémoire
            $adherentsByUuid = null;
            unset($adherentsByUuid);
            $personnesByUuid = null;
            unset($personnesByUuid);
            $dataAdherents = null;
            unset($dataAdherents);
            $apiResult = null;
            unset($apiResult);

            if ($nombreTotalAdherents > $nb_adherents_traites) {
                $n++;
                $apiResult =  $this->getAPIResult($ADHERENT, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataAdherents  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $message = 'Terminé: ' . $nb_adherents_traites . ' adherent·e·s traité·e·s en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }

    function creationMAJEmploye(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $EMPLOYE = "employee";
        $BATCH_SIZE = 1000;
        $this->afficherMessage('5 - Mise à jour des employé·e·s:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("5 a) - API de récupération des employé·e·s", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($EMPLOYE, $BATCH_SIZE, 0);
        $dataEmployes = $apiResult['results'];
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);


        $this->afficherConsoMemoire($io);
        $this->afficherMessage("4 b) - Création et mise à jour des employé·e·s", self::NIVEAU_MESSAGE_SECTION, $io);

        $nb_employe_traites = 0;
        $n = 0;
        $nombreTotalEmployes = $apiResult['count'];
        $io->progressStart($nombreTotalEmployes);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayUuidEmployesACharger = array();
            $employesByUuid = array();
            $arrayPersonnesUuidsACharger = array();
            $personnesByUuid = array();
            foreach ($dataEmployes as $dataEmploye) {
                $arrayUuidEmployesACharger[] = $dataEmploye['uuid'];
                $arrayPersonnesUuidsACharger[] = $dataEmploye['person_uuid'];
            }

            $employesCharges = $this->employeRepository->findWhereUuidIn($arrayUuidEmployesACharger);

            /** @var Employe $employe */
            foreach ($employesCharges as $employe) {
                $employesByUuid[$employe->getUuid()->toRfc4122()] = $employe;
            }

            $personnesChargees = $this->personneRepository->findWhereUuidIn($arrayPersonnesUuidsACharger);
            /** @var Personne $personne */
            foreach ($personnesChargees as $personne) {
                $personnesByUuid[$personne->getUuid()->toRfc4122()] = $personne;
            }
            //Libération mémoire
            $employesCharges = null;
            unset($employesCharges);
            $arrayUuidEmployesACharger = null;
            unset($arrayUuidEmployesACharger);

            $personnesChargees = null;
            unset($personnesChargees);
            $arrayPersonnesUuidsACharger = null;
            unset($arrayPersonnesUuidsACharger);

            $apiResult = null;
            unset($apiResult);



            foreach ($dataEmployes as $dataEmploye) {

                $personne = null;
                $employe = null;

                $uuid = $dataEmploye['uuid'];
                if (array_key_exists($uuid, $employesByUuid)) {
                    $employe = $employesByUuid[$uuid];
                }
                $uuidPersonne = $dataEmploye['person_uuid'];
                if (array_key_exists($uuidPersonne, $personnesByUuid)) {
                    $personne = $personnesByUuid[$uuidPersonne];
                }
                try {
                    $employesByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateEmploye($employe, $personne, $dataEmploye, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }
                $nb_employe_traites++;
                $io->progressAdvance();

                //Libération mémoire
                $personne = null;
                unset($personne);
                $employe = null;
                unset($employe);
            }
            //Libération mémoire
            $employesByUuid = null;
            unset($employesByUuid);
            $personnesByUuid = null;
            unset($personnesByUuid);
            $dataEmployes = null;
            unset($dataEmployes);
            $apiResult = null;
            unset($apiResult);

            if ($nombreTotalEmployes > $nb_employe_traites) {
                $n++;
                $apiResult =  $this->getAPIResult($EMPLOYE, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataEmployes  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();

        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_employe_traites . ' employé·e·s traité·e·s en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }


    function creationMAJContratsTravail(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $CONTRAT_TRAVAIL = "employment";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('6 - Mise à jour des contrats de travail:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("6 a) - API de récupération des contrats de travail", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($CONTRAT_TRAVAIL, $BATCH_SIZE, 0);
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);

        $this->afficherConsoMemoire($io);
        $this->afficherMessage("6 b) - Création et mise à jour des contrats de travail", self::NIVEAU_MESSAGE_SECTION, $io);

        $nb_contrats_travail_traites = 0;
        $n = 0;
        $dataContratsTravail = $apiResult['results'];
        $nombreTotalContratsTravail = $apiResult['count'];
        $io->progressStart($nombreTotalContratsTravail);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayUuidEmployesACharger = array();
            $employesByUuid = array();
            $arrayContratsTravailIdsACharger = array();
            $contratsTravailByUuid = array();
            foreach ($dataContratsTravail as $dataContratTravail) {
                $arrayUuidEmployesACharger[] = $dataContratTravail['employee_uuid'];
                $arrayContratsTravailIdsACharger[] = $dataContratTravail['uuid'];
            }
            $employesCharges = $this->employeRepository->findWhereUuidIn($arrayUuidEmployesACharger);
            /** @var Employe $employe */
            foreach ($employesCharges as $employe) {
                $employesByUuid[$employe->getUuid()->toRfc4122()] = $employe;
            }

            $contratsTravailCharges = $this->contratTravailRepository->findWhereUuidIn($arrayContratsTravailIdsACharger);
            /** @var ContratTravail $contratTravail */
            foreach ($contratsTravailCharges as $contratTravail) {
                $contratsTravailByUuid[$contratTravail->getUuid()->toRfc4122()] = $contratTravail;
            }
            //Libération mémoire
            $employesCharges = null;
            unset($employesCharges);
            $arrayUuidEmployesACharger = null;
            unset($arrayUuidEmployesACharger);

            $contratsTravailCharges = null;
            unset($contratsTravailCharges);
            $arrayContratsTravailIdsACharger = null;
            unset($arrayContratsTravailIdsACharger);

            $apiResult = null;
            unset($apiResult);

            foreach ($dataContratsTravail as $dataContratTravail) {

                $contratTravail = null;
                $employe = null;
                $uuid = $dataContratTravail['uuid'];
                if (array_key_exists($uuid, $contratsTravailByUuid)) {

                    $contratTravail = $contratsTravailByUuid[$uuid];
                }
                $uuidEmploye = $dataContratTravail['employee_uuid'];
                if (array_key_exists($uuidEmploye, $employesByUuid)) {
                    /** @var Employe $employe */
                    $employe = $employesByUuid[$uuidEmploye];
                }
                try {
                    $contratsTravailByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateContratTravail($contratTravail, $employe, $dataContratTravail, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }

                $nb_contrats_travail_traites++;
                $contratTravail = null;
                unset($contratTravail);

                $employe = null;
                unset($employe);

                $dataContratTravail = null;
                unset($dataContratTravail);
            }
            //Libération mémoire
            $employesByUuid = null;
            unset($employesByUuid);

            $contratsTravailByUuid = null;
            unset($contratsTravailByUuid);

            $dataContratsTravail = null;
            unset($dataContratsTravail);

            $apiResult = null;
            unset($apiResult);

            if ($nombreTotalContratsTravail > $nb_contrats_travail_traites) {
                $n++;
                $apiResult =  $this->getAPIResult($CONTRAT_TRAVAIL, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataContratsTravail  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();

        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_contrats_travail_traites . ' contrats de travail traités en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }


    function creationMAJAdhesions(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $ADHESION = "adhesion";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('7 - Mise à jour des adhésions:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("7 a) - API de récupération des adhésions", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($ADHESION, $BATCH_SIZE, 0);
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);

        $this->afficherConsoMemoire($io);
        $this->afficherMessage("7 b) - Création et mise à jour des adhésions", self::NIVEAU_MESSAGE_INFO, $io);

        $nb_adhesions_traites = 0;
        $n = 0;
        $dataAdhesions = $apiResult['results'];
        $nombreTotalAdhesions = $apiResult['count'];
        $io->progressStart($nombreTotalAdhesions);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayUuidAdherentsACharger = array();
            $adherentsByUuid = array();
            $arrayAdhesionsUuidsACharger = array();
            $adhesionsByUuid = array();
            $arrayStructuresUuidsACharger = array();
            $structuresByUuid = array();
            foreach ($dataAdhesions as $dataAdhesion) {
                $arrayUuidAdherentsACharger[] = $dataAdhesion['adherent_uuid'];
                $arrayAdhesionsUuidsACharger[] = $dataAdhesion['uuid'];
                $arrayStructuresUuidsACharger[] = $dataAdhesion['structure_uuid'];
            }
            $adherentsCharges = $this->adherentRepository->findWhereUuidIn($arrayUuidAdherentsACharger);
            /** @var Adherent $adherent */
            foreach ($adherentsCharges as $adherent) {
                $adherentsByUuid[$adherent->getUuid()->toRfc4122()] = $adherent;
            }

            $adhesionsChargees = $this->adhesionRepository->findWhereUuidIn($arrayAdhesionsUuidsACharger);
            /** @var Adhesion $adhesion */
            foreach ($adhesionsChargees as $adhesion) {
                $adhesionsByUuid[$adhesion->getUuid()->toRfc4122()] = $adhesion;
            }
            $structuresChargees = $this->structureRepository->findWhereUuidIn($arrayStructuresUuidsACharger);
            /** @var Structure $structure */
            foreach ($structuresChargees as $structure) {
                $structuresByUuid[$structure->getUuid()->toRfc4122()] = $structure;
            }
            //Libération mémoire
            $adherentsCharges = null;
            unset($adherentsCharges);
            $arrayUuidAdherentsACharger = null;
            unset($arrayUuidAdherentsACharger);

            $adhesionsChargees = null;
            unset($adhesionsChargees);
            $arrayAdhesionsUuidsACharger = null;
            unset($arrayAdhesionsUuidsACharger);

            $structuresChargees = null;
            unset($structuresChargees);
            $arrayStructuresUuidsACharger = null;
            unset($arrayStructuresUuidsACharger);

            $apiResult = null;
            unset($apiResult);


            foreach ($dataAdhesions as $dataAdhesion) {
                $adhesion = null;
                $adherent = null;
                $structure = null;

                $uuid = $dataAdhesion['uuid'];
                if (array_key_exists($uuid, $adhesionsByUuid)) {
                    $adhesion = $adhesionsByUuid[$uuid];
                }
                $uuidAdherent = $dataAdhesion['adherent_uuid'];
                if (array_key_exists($uuidAdherent, $adherentsByUuid)) {
                    $adherent = $adherentsByUuid[$uuidAdherent];
                }
                $uuidStructure = $dataAdhesion['structure_uuid'];

                if (array_key_exists($uuidStructure, $structuresByUuid)) {
                    $structure = $structuresByUuid[$uuidStructure];
                }
                try {
                    $adhesionsByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateAdhesion($adhesion, $adherent, $structure, $dataAdhesion, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }

                $nb_adhesions_traites++;
                $io->progressAdvance();

                //Libération mémoire
                $personne = null;
                unset($personne);

                $adherent = null;
                unset($adherent);

                $structure = null;
                unset($structure);
            }
            //Libération mémoire
            $structuresByUuid = null;
            unset($structuresByUuid);

            $adherentsByUuid = null;
            unset($adherentsByUuid);

            $adhesionsByUuid = null;
            unset($adhesionsByUuid);

            $dataAdhesions = null;
            unset($dataAdhesions);

            $apiResult = null;
            unset($apiResult);

            if ($nombreTotalAdhesions > $nb_adhesions_traites) {
                $n++;
                $apiResult =  $this->getAPIResult($ADHESION, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataAdhesions  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $message = 'Terminé: ' . $nb_adhesions_traites . ' adhésions traitées en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }

    function creationMAJFonctions(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $FONCTION = "function";
        $BATCH_SIZE = 1000;

        $this->afficherMessage('8 - Mise à jour des fonctions:', self::NIVEAU_MESSAGE_SECTION, $io);
        $this->afficherMessage("8 a) - API de récupération des fonctions", self::NIVEAU_MESSAGE_INFO, $io);
        $apiResult = $this->getAPIResult($FONCTION, $BATCH_SIZE, 0);
        $this->afficherMessage('Terminé', self::NIVEAU_MESSAGE_INFO, $io);

        $this->afficherConsoMemoire($io);
        $this->afficherMessage("8 b) - Création et mise à jour des fonctions", self::NIVEAU_MESSAGE_INFO, $io);

        $nb_fonctions_traitees = 0;
        $n = 0;
        $dataFonctions = $apiResult['results'];
        $nombreTotalFonctions = $apiResult['count'];
        $io->progressStart($nombreTotalFonctions);

        $dernierePageParcourue = false;
        while (!$dernierePageParcourue) {
            $arrayPersonneUuidsACharger = array();
            $personnesByUuid = array();

            $arrayEquipesUuidsACharger = array();
            $equipesByUuid = array();

            $arrayFonctionsUuidsACharger = array();
            $fonctionsByUuid = array();

            foreach ($dataFonctions as $dataFonction) {
                $arrayEquipesUuidsACharger[] = $dataFonction['team_uuid'];
                $arrayPersonneUuidsACharger[] = $dataFonction['person_uuid'];
                $arrayFonctionsUuidsACharger[] = $dataFonction['uuid'];
            }
            $equipesChargees = $this->equipeRepository->findWhereUuidIn($arrayEquipesUuidsACharger);
            /** @var Equipe $equipe */
            foreach ($equipesChargees as $equipe) {
                $equipesByUuid[$equipe->getUuid()->toRfc4122()] = $equipe;
            }

            $personnesChargees = $this->personneRepository->findWhereUuidIn($arrayPersonneUuidsACharger);
            /** @var Personne $personne */
            foreach ($personnesChargees as $personne) {
                $personnesByUuid[$personne->getUuid()->toRfc4122()] = $personne;
            }
            $fonctionsChargees = $this->fonctionRepository->findWhereUuidIn($arrayFonctionsUuidsACharger);
            /** @var Fonction $fonction */
            foreach ($fonctionsChargees as $fonction) {
                $fonctionsByUuid[$fonction->getUuid()->toRfc4122()] = $fonction;
            }
            //Libération mémoire
            $fonctionsChargees = null;
            unset($fonctionsChargees);
            $arrayFonctionsUuidsACharger = null;
            unset($arrayFonctionsUuidsACharger);

            $personnesChargees = null;
            unset($personnesChargees);
            $arrayPersonneUuidsACharger = null;
            unset($arrayPersonneUuidsACharger);

            $equipesChargees = null;
            unset($equipesChargees);
            $arrayEquipesUuidsACharger = null;
            unset($arrayEquipesUuidsACharger);

            $apiResult = null;
            unset($apiResult);
            foreach ($dataFonctions as $dataFonction) {
                $fonction = null;
                $equipe = null;
                $personne = null;

                $uuid = $dataFonction['uuid'];
                if (array_key_exists($uuid, $fonctionsByUuid)) {
                    $fonction = $fonctionsByUuid[$uuid];
                }
                $uuidPersonne = $dataFonction['person_uuid'];
                if (array_key_exists($uuidPersonne, $personnesByUuid)) {
                    /** @var Personne $personne */
                    $personne = $personnesByUuid[$uuidPersonne];
                }
                $uuidEquipe = $dataFonction['team_uuid'];
                if (array_key_exists($uuidEquipe, $equipesByUuid)) {
                    /** @var Equipe $equipe */
                    $equipe = $equipesByUuid[$uuidEquipe];
                }
                try {
                    $fonctionsByUuid[$uuid] = $this->jeitoSyncronisationHelper->createOrUpdateFonction($fonction, $personne, $equipe, $dataFonction, $dateCourante);
                } catch (Exception $e) {
                    $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                }

                $nb_fonctions_traitees++;
                $io->progressAdvance();

                //Libération mémoire
                $fonction = null;
                unset($fonction);

                $equipe = null;
                unset($equipe);

                $personne = null;
                unset($personne);
            }
            //Libération mémoire
            $fonctionsByUuid = null;
            unset($fonctionsByUuid);

            $equipesByUuid = null;
            unset($equipesByUuid);

            $personnesByUuid = null;
            unset($personnesByUuid);

            $dataFonctions = null;
            unset($dataFonctions);

            $apiResult = null;
            unset($apiResult);



            if ($nombreTotalFonctions > $nb_fonctions_traitees) {
                $n++;
                $apiResult =  $this->getAPIResult($FONCTION, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataFonctions  = $apiResult['results'];
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();

        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");
        $message = 'Terminé: ' . $nb_fonctions_traitees . ' fonctions traitées, et en ' . $dureeTraitement;
        $this->afficherMessage($message, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }





    function suppressionFonctionsObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des fonctions obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Fonction f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Fonction f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_fonctions_supprimees = 0;
        /** @var Fonction fonctionASupprimer */
        foreach ($query->toIterable() as $fonctionASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionFonction($fonctionASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_fonctions_supprimees++;
            $io->progressAdvance();
            if ($nb_fonctions_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_fonctions_supprimees . ' fonctions  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionAdhesionsObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des adhésions obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Adhesion f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Adhesion f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_adhesions_supprimees = 0;
        /** @var Adhesion adhesionASupprimer */
        foreach ($query->toIterable() as $adhesionASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionAdhesion($adhesionASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_adhesions_supprimees++;
            $io->progressAdvance();
            if ($nb_adhesions_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_adhesions_supprimees . ' adhésions  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionAdherentsObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des adhérent·es obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Adherent f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Adherent f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_adherents_supprimees = 0;
        /** @var Adherent adherentASupprimer */
        foreach ($query->toIterable() as $adherentASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionAdherent($adherentASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_adherents_supprimees++;
            $io->progressAdvance();
            if ($nb_adherents_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_adherents_supprimees . ' adhérent·e·s  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionContratsTravailObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des contrats de travail obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\ContratTravail f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\ContratTravail f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_contrats_travail_supprimees = 0;
        /** @var ContratTravail contratTravailASupprimer */
        foreach ($query->toIterable() as $contratTravailASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionContratTravail($contratTravailASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_contrats_travail_supprimees++;
            $io->progressAdvance();
            if ($nb_contrats_travail_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_contrats_travail_supprimees . ' contrats de travail  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionStructuresObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des structures obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        //On ordonne la requete pour avoir les enfants d'abord
        $query = $this->entityManager->createQuery('select f from App\Entity\Structure f where f.dateModification < :dateCourante ORDER BY f.structureParent DESC, f.id DESC')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Structure f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_structures_supprimees = 0;
        /** @var Structure structureASupprimer */
        foreach ($query->toIterable() as $structureASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionStructure($structureASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }

            $io->progressAdvance();
            if ($nb_structures_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_structures_supprimees . ' structures  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionEmployesObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des employé·e·s obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Employe f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Employe f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_employes_supprimes = 0;
        /** @var Employe employeASupprimer */
        foreach ($query->toIterable() as $employeASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionEmploye($employeASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_employes_supprimes++;
            $io->progressAdvance();
            if ($nb_employes_supprimes % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_employes_supprimes . ' employé·e·s  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function suppressionEquipesObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des équipes obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Equipe f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Equipe f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_equipes_supprimees = 0;
        /** @var Equipe equipeASupprimer */
        foreach ($query->toIterable() as $equipeASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionEquipe($equipeASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_equipes_supprimees++;
            $io->progressAdvance();
            if ($nb_equipes_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_equipes_supprimees . ' équipes  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    //On ne supprime pas les personnes qui ont encore un rôle d'accompagnement, de direction, de direcction adjointe, ou d'encadrement dans au moins un séjour de ROADS
    function suppressionPersonnesObsoletes(SymfonyStyle $io, Datetime $dateCourante)
    {
        $dateStart = new DateTime();
        $this->afficherMessage('Suppression des personnes obsolètes', self::NIVEAU_MESSAGE_SECTION, $io);
        $BATCH_SIZE = 1000;

        $query = $this->entityManager->createQuery('select f from App\Entity\Personne f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante);

        $io->progressStart($this->entityManager->createQuery('select count(f) from App\Entity\Personne f where f.dateModification < :dateCourante')->setParameter('dateCourante', $dateCourante)->getSingleScalarResult());
        $nb_personnes_supprimees = 0;
        /** @var Personne personneASupprimer */
        foreach ($query->toIterable() as $personneASupprimer) {
            try {
                $this->jeitoSyncronisationHelper->suppressionPersonne($personneASupprimer);
            } catch (Exception $e) {
                $this->afficherMessage($e->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
            }
            $nb_personnes_supprimees++;
            $io->progressAdvance();
            if ($nb_personnes_supprimees % $BATCH_SIZE == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->afficherConsoMemoire($io);
                gc_collect_cycles();
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->afficherConsoMemoire($io);
        gc_collect_cycles();

        $io->progressFinish();
        $dateFinish = new DateTime();
        $dureeTraitement = $dateStart->diff($dateFinish)->format("%Hh %Im %Ss");

        $this->afficherMessage('Suppression de ' . $nb_personnes_supprimees . ' personnes  en ' . $dureeTraitement, self::NIVEAU_MESSAGE_SUCCESS, $io);
    }
    function getAPIResult(String $urlSuffix, $limit = 100, $offset = 0)
    {
        $httpClientStructures = HttpClient::create();
        $httpClientStructures = new ScopingHttpClient(
            $httpClientStructures,
            [
                $this->jeitoApiUrl . $urlSuffix => [
                    'headers' => [
                        'Authorization' => 'token ' . $this->jeitoApiToken
                    ]
                ]
            ]
        );
        $response = $httpClientStructures->request(
            'GET',
            $this->jeitoApiUrl . $urlSuffix,
            [
                // these values are automatically encoded before including them in the URL
                'query' => [
                    'limit' => $limit,
                    'offset' => $offset
                ]
            ]
        );


        $content = $response->getContent();

        return json_decode($content, true);
    }


    function envoiMailRecapsynchroSupport(SymfonyStyle $io, bool $erreur = false)
    {

        /**************************** 
         * Phase 3: Un résumé de la synchronisation est envoyé par mail à l'adresse de support de ROADS
         *****************************/
        //envoi du mail à l'adresse support:
        $parametreAdresseSupport = $this->parametreRoadsRepository->findOneBy(['nomParametre' => ParametreRoads::PARAMETRE_ADRESSE_SUPPORT]);


        if ($erreur) {
            $sujet = "[Support R.O.A.D.S] Erreur lors de la synchronisation massive des données depuis Jéito";
        } else {
            $sujet = "[Support R.O.A.D.S] Récapitulatif de la synchronisation massive des données depuis Jéito";
        }
        if ($parametreAdresseSupport) {
            $adresseSupport = $parametreAdresseSupport->getValeurParametre();
            $message = (new TemplatedEmail())
                // On attribue l'expéditeur
                ->from($adresseSupport)

                // On attribue le destinataire
                ->to($adresseSupport)
                ->subject($sujet)
                ->htmlTemplate('commande_synchro_jeito/synchro_email.html.twig')
                ->context([
                    'infos' => $this->infosForSupport
                ]);
            $this->mailer->send($message);
        } else {
            $io->warning('L\'adresse de support n\'existe pas encore, aucun mail ne sera envoyé, n\'oubliez pas d\'initialiser le premier administrateur');
        }
    }
    function afficherMessage(String $message, int $niveauInformation, SymfonyStyle $io)
    {
        $afficherDansMail = true;
        switch ($niveauInformation) {
            case self::NIVEAU_MESSAGE_SUCCESS:
                $io->success($message);
                break;
            case self::NIVEAU_MESSAGE_INFO:
                $io->info($message);
                break;
            case self::NIVEAU_MESSAGE_WARNING:
                $io->warning($message);
                $message = "Attention: " . $message;
                break;
            case self::NIVEAU_MESSAGE_TEXT:
                $io->text($message);
                $afficherDansMail = false;
                break;
            case self::NIVEAU_MESSAGE_SECTION:
                $io->section($message);
                break;
        }
        if ($afficherDansMail) {
            $this->infosForSupport[] = $message;
        }
    }
    function afficherConsoMemoire(SymfonyStyle $io)
    {
        $memoryUsage = memory_get_usage(true) / 1024 / 1024;
        $this->afficherMessage("Consommation mémoire: " . $memoryUsage . "M", self::NIVEAU_MESSAGE_TEXT, $io);
    }
}
