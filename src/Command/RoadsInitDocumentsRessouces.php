<?php

namespace App\Command;

use App\Entity\DocumentRessource;
use App\Repository\DocumentRessourceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Commande d'intialisation et de mise à jour des documents ressources de ROADS
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:init-documents-ressources',
    description: 'Crée et supprime les documents ressources',
)]
class RoadsInitDocumentsRessouces extends Command
{

    private $documentRessourceRepository;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, DocumentRessourceRepository $documentRessourceRepository)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->documentRessourceRepository = $documentRessourceRepository;
        $this->entityManager = $entityManager;

        parent::__construct();
    }


    protected function configure() {}

    /**
     * Crée et modifie aux besoins les documents ressources de ROADS
     * 
     * - Recherche les documents ressources existant en base de donnnées
     * - Ajoute les nouveaux documents ressources (comparaison avec les documents existant et @see DocumentRessource::LISTE_DOCUMENTS_RESSOURCES)
     * - Supprime les documents ressource obsolètes
     * @param  InputInterface $input
     * @param  OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);




        //******************************* */
        //gestion des documents ressources
        //******************************* */
        $io->info("Recherche des documents ressources existants ...");
        $documentsRessourceExistants = $this->documentRessourceRepository->findAll();
        $documentsRessourceASupprimer = array();
        $documentsRessourceACreer = DocumentRessource::LISTE_DOCUMENTS_RESSOURCES;
        foreach ($documentsRessourceExistants as $documentRessourceExistant) {
            $nom = $documentRessourceExistant->getNom();
            //Si le document exsite et est dans la liste, il n'est pas à creer,
            //S'il existe et n'est pas dans la liste, il est alors a supprimer
            if (array_key_exists($nom, $documentsRessourceACreer)) {
                unset($documentsRessourceACreer[$nom]);
            } else {
                $documentsRessourceASupprimer[] = $documentRessourceExistant;
            }
        }
        $io->success("Terminé");

        $io->info("Ajout des nouveaux documents ressources ...");
        //Création
        $nbDocAjoutes = 0;
        foreach ($documentsRessourceACreer as $nom => $description) {
            $doc = new DocumentRessource();
            $doc->setNom($nom);
            $doc->setDescription($description);

            $this->entityManager->persist($doc);
            $this->entityManager->flush();
            $nbDocAjoutes++;
        }
        $io->success("Ajout terminé");

        $io->info("Suppression des documents ressources obsolètes ...");

        //Suppression
        $nbDocSupprimes = 0;
        foreach ($documentsRessourceASupprimer as $documentRessourceASupprimer) {
            $this->entityManager->remove($documentRessourceASupprimer);
            $this->entityManager->flush();
            $nbDocSupprimes++;
        }

        $io->success("Suppression terminée");

        $io->success("Terminé: " . $nbDocAjoutes . " documents ressources ont été rajoutés et " . $nbDocSupprimes . " ont été supprimés");
        return Command::SUCCESS;
    }
}
