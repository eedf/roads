<?php

namespace App\Command;

use App\Kernel;
use LongitudeOne\Spatial\PHP\Types\Geography\Point;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Uid\Uuid;

/**
 * Commande de migration des lieuSejourIntention en UtilisationLieuSejour, LieuSejour et Localisation
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:migration_lieu_sejour',
    description: "Permet de migrer les lieux de séjour vers des infos géocodées (février 2024)"
)]
class RoadsMigrationCoordonneesLieuSejourCommand extends Command
{


    private $entityManager;



    const NIVEAU_MESSAGE_SUCCESS = 0;
    const NIVEAU_MESSAGE_INFO = 1;
    const NIVEAU_MESSAGE_WARNING = 2;
    const NIVEAU_MESSAGE_TEXT = 3;
    const NIVEAU_MESSAGE_SECTION = 4;

    const INFOS_DECLARATION = 1;
    const INFOS_SEJOUR = 2;





    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    public function __construct(EntityManagerInterface $entityManager)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        //$this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
        $io = new SymfonyStyle($input, $output);

        //Création des nouvelles tables
        $donneesASerialiser = array();
        $donneesDeclaIntentionASerialiser = array();
        $io->section("Création des nouvelles tables...");

        $this->entityManager->getConnection()->prepare('CREATE TABLE localisation (id INT AUTO_INCREMENT NOT NULL, adresse VARCHAR(255) DEFAULT NULL, departement VARCHAR(255) DEFAULT NULL, complement_adresse VARCHAR(255) DEFAULT NULL, code_postal_commune VARCHAR(10) DEFAULT NULL, nom_commune VARCHAR(255) DEFAULT NULL, coordonnees_geo POINT DEFAULT NULL COMMENT \'(DC2Type:point)\', pays VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();

        $this->entityManager->getConnection()->prepare('CREATE TABLE lieu_sejour (id INT AUTO_INCREMENT NOT NULL, convention_lieu_id INT DEFAULT NULL, coordonnees_proprietaire_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, est_eedf TINYINT(1) NOT NULL, site_internet VARCHAR(255) DEFAULT NULL, proprietaire_est_personne_physique TINYINT(1) NOT NULL, localisation_id INT NOT NULL, UNIQUE INDEX UNIQ_1C55D963DD7FC659 (convention_lieu_id), UNIQUE INDEX UNIQ_1C55D9637FDD45A9 (coordonnees_proprietaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_sejour ADD CONSTRAINT FK_1C55D963DD7FC659 FOREIGN KEY (convention_lieu_id) REFERENCES document (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_sejour ADD CONSTRAINT FK_1C55D9637FDD45A9 FOREIGN KEY (coordonnees_proprietaire_id) REFERENCES coordonnees (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_sejour ADD CONSTRAINT FK_1C55D963C68BE09C FOREIGN KEY (localisation_id) REFERENCES localisation (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_1C55D963C68BE09C ON lieu_sejour (localisation_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE TABLE utilisation_lieu_sejour (id INT AUTO_INCREMENT NOT NULL, sejour_id INT NOT NULL, lieu_id INT DEFAULT NULL, date_depart DATETIME DEFAULT NULL, date_arrivee DATETIME DEFAULT NULL, zone_envisagee VARCHAR(255) DEFAULT NULL, est_defini TINYINT(1) NOT NULL, INDEX IDX_377E84C284CF0CF (sejour_id), UNIQUE INDEX UNIQ_377E84C26AB213CC (lieu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE utilisation_lieu_sejour ADD CONSTRAINT FK_377E84C284CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE utilisation_lieu_sejour ADD CONSTRAINT FK_377E84C26AB213CC FOREIGN KEY (lieu_id) REFERENCES lieu_sejour (id)')->executeQuery();

        //Initialisation des lieux
        $io->section("Lecture du fichier de migration");
        $arrayLocalisationByLieuId = array();
        $csv = new \ParseCsv\Csv();
        $csv->delimiter = ";";
        $csv->parseFile("public/temp/migration_lieux_sejour.csv");
        foreach ($csv->data as $data) {
            if ($data["longitude"] !== "") {
                $arrayLocalisationByLieuId[$data["lieu_id"]] = [
                    "adresse" => $data["ADRESSE"],
                    "complement_adresse" => $data["COMPLEMENT_ADRESSE"],
                    "code_postal_commune" => $data["CODE_POSTAL_COMMUNE"],
                    "nom_commune" => $data["NOM_COMMUNE"],
                    "departement" => $data["DÉPARTEMENT"],
                    "latitude" => str_replace(",", ".", trim($data["latitude"])),
                    "longitude" => str_replace(",", ".", trim($data["longitude"])),
                    "pays" => $data["PAYS"],
                ];
            }
        }






        $io->section("Initialisation des lieux...");
        $lieuxIntention = $this->entityManager->getConnection()->prepare(
            "SELECT 
                ldi.id,
                date_arrivee,
                date_depart,
                proprietaire_personne_physique,
                terrain_eedf,
                site_internet,
                convention_lieu_id,
                nom_lieu,
                coordonnees_lieu_id,
                sejour_id,
                defini,
                d.document_name,
                d.document_size
             FROM lieu_declaration_intention ldi
             LEFT OUTER JOIN document d
             ON (d.id = ldi.convention_lieu_id)"
        )->executeQuery();
        $io->progressStart($lieuxIntention->rowCount());
        foreach ($lieuxIntention->fetchAllAssociative() as $lieuIntention) {


            try {
                $id_localisation = null;
                $coordonnees = [];
                if (array_key_exists($lieuIntention['id'], $arrayLocalisationByLieuId)) {

                    $coordonnees = $arrayLocalisationByLieuId[$lieuIntention['id']];
                    $stmt = null;
                    $stmt = $this->entityManager->getConnection()->prepare("INSERT INTO localisation (adresse, complement_adresse, code_postal_commune, nom_commune, coordonnees_geo, pays, departement) VALUES (?, ?, ?, ?, ST_GeomFromText('POINT(" . $coordonnees['longitude'] . " " . $coordonnees['latitude'] . ")',0), ?, ?)");
                    $stmt->bindValue(1, $coordonnees['adresse']);
                    $stmt->bindValue(2, $coordonnees['complement_adresse']);
                    $stmt->bindValue(3, $coordonnees['code_postal_commune']);
                    $stmt->bindValue(4, $coordonnees['nom_commune']);
                    $stmt->bindValue(5, $coordonnees['pays']);
                    $stmt->bindValue(6, $coordonnees['departement']);
                    $stmt->executeQuery();

                    $id_localisation = $this->entityManager->getConnection()->lastInsertId();
                }
                $id_lieu_sejour = null;
                if ($id_localisation !== null) {
                    $this->entityManager->getConnection()->insert(
                        'lieu_sejour',
                        [
                            'localisation_id ' =>  $id_localisation,
                            'proprietaire_est_personne_physique' => $lieuIntention['proprietaire_personne_physique'] ? 1 : 0,
                            'site_internet' => $lieuIntention['site_internet'],
                            'est_eedf' => $lieuIntention['terrain_eedf'] ? 1 : 0,
                            'nom' => $lieuIntention['nom_lieu'] ? $lieuIntention['nom_lieu'] : ($coordonnees ? $coordonnees['adresse'] : "Non défini"),
                            'coordonnees_proprietaire_id' => $lieuIntention['coordonnees_lieu_id'],
                            'convention_lieu_id ' => $lieuIntention['convention_lieu_id']
                        ]
                    );
                    $id_lieu_sejour = $this->entityManager->getConnection()->lastInsertId();
                }


                $this->entityManager->getConnection()->insert(
                    'utilisation_lieu_sejour',
                    [
                        'lieu_id' => $id_localisation ? $id_lieu_sejour : null,
                        'sejour_id' =>  $lieuIntention['sejour_id'],
                        'date_depart' =>  $lieuIntention['date_depart'],
                        'date_arrivee' =>  $lieuIntention['date_arrivee'],
                        'est_defini' =>  $id_localisation ? 1 : 0,
                        'zone_envisagee' =>  $id_localisation ? ($lieuIntention['nom_lieu'] ? $lieuIntention['nom_lieu'] : "Non renseigné") : null
                    ]

                );
                //$io->success("Les coordonnées Géo du lieu " . $lieuIntention['nom_lieu'] . "(" . $lieuIntention['id'] . ") Ont été mises à jour");

                //Stockage des données sérialisées

                //Pour les déclarations d'intention
                if (!array_key_exists($lieuIntention['sejour_id'], $donneesDeclaIntentionASerialiser)) {
                    $donneesDeclaIntentionASerialiser[$lieuIntention['sejour_id']] = array();
                }

                $donneesDeclaIntentionASerialiser[$lieuIntention['sejour_id']][] = [
                    "estDefini" => $lieuIntention['defini'] == "1" ? true : false,
                    "zoneEnvisagee" => ($lieuIntention['nom_lieu'] ? $lieuIntention['nom_lieu'] : "Non renseigné"),
                    "lieu" => [
                        'estEedf' => $lieuIntention['terrain_eedf']  == "1" ? true : false,
                        'nom' => $lieuIntention['nom_lieu'],
                        "localisation" => $coordonnees ? [
                            "adresse" => $coordonnees['adresse'],
                            "complementAdresse" => $coordonnees['complement_adresse'],
                            "codePostalCommune" => $coordonnees['code_postal_commune'],
                            "nomCommune" => $coordonnees['nom_commune'],
                            "departement" => $coordonnees['departement'],
                            "pays" => $coordonnees['pays'],
                            "coordonneesGeo" => [
                                "type" => "Point",
                                "coordinates" => [
                                    $coordonnees['longitude'],
                                    $coordonnees['latitude']
                                ],
                                "srid" => null
                            ]
                        ] : [],
                    ]

                ];
                //Pour les infos de séjour
                if (!array_key_exists($lieuIntention['sejour_id'], $donneesASerialiser)) {
                    $donneesASerialiser[$lieuIntention['sejour_id']] = array();
                }
                $result_temp_convention_lieu = [];
                if ($lieuIntention['convention_lieu_id']) {
                    $result_temp_convention_lieu = [
                        "documentName" => $lieuIntention['document_name'],
                        "documentSize" => intval($lieuIntention['document_size'])
                    ];
                }

                $result_temp_lieu = [
                    'proprietaire_est_personne_physique' => $lieuIntention['proprietaire_personne_physique'] ? true : false,
                    'site_internet' => $lieuIntention['site_internet'],
                    'est_eedf' => $lieuIntention['terrain_eedf'] ? true : false,
                    'nom' => $lieuIntention['nom_lieu'],
                    "localisation" => $coordonnees ? [
                        "adresse" => $coordonnees['adresse'],
                        "complementAdresse" => $coordonnees['complement_adresse'],
                        "codePostalCommune" => $coordonnees['code_postal_commune'],
                        "nomCommune" => $coordonnees['nom_commune'],
                        "departement" => $coordonnees['departement'],
                        "pays" => $coordonnees['pays'],
                        "coordonneesGeo" => [
                            "type" => "Point",
                            "coordinates" => [
                                $coordonnees['longitude'],
                                $coordonnees['latitude']
                            ],
                            "srid" => null
                        ]
                    ] : [],
                    "conventionLieu" => $result_temp_convention_lieu,
                    'coordonnees_proprietaire' => null,
                ];
                $result_temp = [
                    "dateDepart" => $lieuIntention['date_depart'],
                    "dateArrivee" => $lieuIntention['date_arrivee'],
                    "lieu" => $result_temp_lieu
                ];

                $donneesASerialiser[$lieuIntention['sejour_id']][] = $result_temp;
            } catch (Exception $e) {

                $io->error($e->getMessage());
                $io->error($coordonnees);
            }

            $io->progressAdvance();
        }
        $io->progressFinish();



        //Mise à jour des données sérialisées
        $io->section("Mise à jour des données sérialisées...");


        //Migration des données 
        $io->text("Migration des infos de séjour cloturés");
        $infos_sejour_serialisees = $this->entityManager->getConnection()->prepare("SELECT id, infos_sejour FROM sejour WHERE infos_sejour IS NOT NULL")->executeQuery();
        $io->progressStart($infos_sejour_serialisees->rowCount());
        foreach ($infos_sejour_serialisees->fetchAllAssociative() as $infos_sejour_serialisee) {

            $infos_sejour = $infos_sejour_serialisee['infos_sejour'];
            $id_sejour = $infos_sejour_serialisee['id'];
            $infos_sejour = json_decode($infos_sejour, true);
            if (array_key_exists($id_sejour, $donneesASerialiser)) {
                //dans le cas de rejeu, supprimer les séjours précédents
                unset($infos_sejour['utilisationsLieuSejour']);
                $infos_sejour['utilisationsLieuSejour'] = $donneesASerialiser[$id_sejour];
                unset($infos_sejour["lieuxIntention"]);
                $this->entityManager->getConnection()->update(
                    'sejour',
                    [
                        'infos_sejour' => json_encode($infos_sejour),
                    ],
                    ['id' => $id_sejour]
                );
            }




            $io->progressAdvance();
        }

        $io->progressFinish();
        $io->text("Terminé");



        $io->text("Migration des infos de déclaration d'intention ");
        $decla_intention_serialisees = $this->entityManager->getConnection()->prepare("SELECT id, infos_declaration FROM sejour WHERE infos_declaration IS NOT NULL")->executeQuery();
        $io->progressStart($decla_intention_serialisees->rowCount());
        foreach ($decla_intention_serialisees->fetchAllAssociative() as  $decla_intention_serialisee) {

            $decla_sejour = $decla_intention_serialisee['infos_declaration'];
            $id_sejour = $decla_intention_serialisee['id'];
            $decla_sejour = json_decode($decla_sejour, true);
            if (array_key_exists($id_sejour, $donneesDeclaIntentionASerialiser)) {
                //dans le cas de rejeu, supprimer les séjours précédents
                unset($decla_sejour['utilisationsLieuSejour']);
                unset($decla_sejour['utilisationsLieuxSejour']);
                $decla_sejour['utilisationsLieuSejour'] = $donneesDeclaIntentionASerialiser[$id_sejour];
                unset($decla_sejour["lieuxIntention"]);
                $this->entityManager->getConnection()->update(
                    'sejour',
                    [
                        'infos_declaration' => json_encode($decla_sejour),
                    ],
                    ['id' => $id_sejour]
                );
            }
            $io->progressAdvance();
        }
        $io->progressFinish();
        $io->text("Terminé");

        //Suppression des anciennes tables

        $io->section("Suppression des anciennes tables...");
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825D6D011E42')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825DDD7FC659')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825D84CF0CF')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE lieu_declaration_intention DROP FOREIGN KEY FK_C804825DC4ACAC53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP TABLE lieu_declaration_intention')->executeQuery();
        $io->text("Terminé");
        return Command::SUCCESS;
    }
}
