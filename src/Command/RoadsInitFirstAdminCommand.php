<?php

namespace App\Command;

use App\Entity\Adherent;
use App\Entity\Employe;
use App\Entity\Personne;
use App\Entity\ParametreRoads;
use App\Repository\AdherentRepository;
use App\Repository\EmployeRepository;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ParametreRoadsRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Commande d'intialisation du·de la premièr·e administrat·eur·rice de ROADS et de l'adresse support
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[AsCommand(
    name: 'roads:init-first-admin',
    description: 'Initialise le premier admin',
)]
class RoadsInitFirstAdminCommand extends Command
{

    private $entityManager;
    private $validatorInterface;
    private $parametreRoadsRepository;
    private AdherentRepository $adherentRepository;
    private EmployeRepository $employeRepository;

    public function __construct(EntityManagerInterface $entityManager, ParametreRoadsRepository $parametreRoadsRepository, AdherentRepository $adherentRepository, EmployeRepository $employeRepository, ValidatorInterface $validatorInterface)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;
        $this->adherentRepository = $adherentRepository;
        $this->validatorInterface = $validatorInterface;
        $this->parametreRoadsRepository = $parametreRoadsRepository;
        $this->employeRepository = $employeRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Crée un utilisateur avec le rôle d\'administrateur et initialise l\'adresse de support avec l\'adresse e-mail de l\'administrateur');
    }

    /**
     * Crée un utilisateur avec le rôle d'administrateur et initialise l'adresse de support avec l'adresse e-mail de l'administrateur
     *
     * En fonction des données saisies par la personne qui a lancée la commande: 
     * L'utilisateur doit saisir le numéro d'adhérent. Si le format est incorrect ou si le numéro d'adhérent n'exsite pas, une erreur est renvoyée
     * @param  mixed $input
     * @param  mixed $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $em = $this->entityManager;
        $numAdh = $io->ask("Saisissez le numéro d'adhérent·e ou de salarié·e de la personne à nommer comme administrateur", null, function ($numAdh) {
            // use the validator to validate the value
            $errors = $this->validatorInterface->validate(
                $numAdh,
                [
                    new Assert\GreaterThan([
                        'message' => "Le numéro d'adhérent·e ou de salarié·e doit être compris entre 000000 et 999999",
                        'value' => 0
                    ]),
                    new Assert\LessThan([
                        'message' => "Le numéro d'adhérent·e ou de salarié·e doit être compris entre 000000 et 999999",
                        'value' => 1000000
                    ])
                ],
            );
            if (count($errors) > 0) {
                throw new \RuntimeException($errors[0]->getMessage());
            }

            return $numAdh;
        });
        $io->info("Recherche de l'existence de la personne en base...");

        /** @var Adherent $adherent */
        $adherent = $this->adherentRepository->findOneBy(['numeroAdherent' => $numAdh]);
        if (!$adherent) {
            /** @var Employe $employe */
            $employe = $this->employeRepository->findOneBy(['numeroSalarie' => $numAdh]);
            if (!$employe) {
                $io->error("La personne avec le numéro " . $numAdh . " n'existe pas!");
                return Command::FAILURE;
            } else {
                $personne = $employe->getPersonne();
                $io->success("Salarié·e trouvé·e!");
                if (!$personne->estSalarieValide()) {
                    $io->error("Le salarié·e avec le numéro " . $numAdh . " n'est pas actif!");
                    return Command::FAILURE;
                }
            }
        } else {
            $personne = $adherent->getPersonne();


            $io->success("Personne trouvée!");
            if (!$personne->estAdherentValide()) {
                $io->error("La personne avec le numéro " . $numAdh . " n'est pas à jour de sa cotisation ou n'est pas valide!");
                return Command::FAILURE;
            }
            $io->success("Personne à jour de sa cotisation!");
        }
        $personne->addRole(Personne::ROLE_ADMIN);

        $em->persist($personne);
        $em->flush();

        $adresse_support = $this->parametreRoadsRepository->findOneBy(['nomParametre' => ParametreRoads::PARAMETRE_ADRESSE_SUPPORT]);
        if (!$adresse_support) {
            $adresse_support = new ParametreRoads();
            $adresse_support->setNomParametre(ParametreRoads::PARAMETRE_ADRESSE_SUPPORT);
        }

        $adresse_support->setValeurParametre($personne->getAdresseMailUsage());

        $em->persist($adresse_support);
        $em->flush();

        $io->success('La personne ' . $personne->getPrenom() . ' ' . $personne->getNom() . ' a bien été nommée comme administrateur et l\'adresse de support a ete mise à jour avec son adresse');
        return Command::SUCCESS;
    }
}
