<?php

namespace App\Command;

use DateTime;
use App\Kernel;
use App\Entity\Equipe;
use App\Entity\Employe;
use App\Entity\Adherent;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Entity\ContratTravail;
use Doctrine\DBAL\ParameterType;
use App\Entity\RoleEncadrantSejour;
use App\Repository\AdherentRepository;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'roads:migration_nouveau_modele',
    description: "Permet de migrer les données de l'ancien modèle vers le nouveau modèle de données"

)]
class RoadsMigrationSynchroNouveauModeleCommand extends Command
{


    private $entityManager;

    private array $arrayIdJeitoDuplicataPersonnesByNumAdherent;

    private $jeitoApiUrl;
    private $jeitoApiToken;

    const NIVEAU_MESSAGE_SUCCESS = 0;
    const NIVEAU_MESSAGE_INFO = 1;
    const NIVEAU_MESSAGE_WARNING = 2;
    const NIVEAU_MESSAGE_TEXT = 3;
    const NIVEAU_MESSAGE_SECTION = 4;

    const INFOS_DECLARATION = 1;
    const INFOS_SEJOUR = 2;





    protected function configure(): void {}

    public function __construct(EntityManagerInterface $entityManager, PersonneRepository $personneRepository, AdherentRepository $adherentRepository, StructureRepository $structureRepository, String $jeitoApiUrl, String $jeitoApiToken)
    {
        // 3. Update the value of the private entityManager variable through injection
        $this->entityManager = $entityManager;

        $this->jeitoApiUrl = $jeitoApiUrl;
        $this->jeitoApiToken = $jeitoApiToken;
        $this->arrayIdJeitoDuplicataPersonnesByNumAdherent = array();

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateCourante = new DateTime();
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
        $io = new SymfonyStyle($input, $output);


        $io->section("Modification de la table adresse_postale");
        $this->entityManager->getConnection()->prepare('ALTER TABLE adresse_postale DROP habite_chez')->executeQuery();


        $io->section("Suppression de la table reset_password_request");
        $this->entityManager->getConnection()->prepare('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP TABLE reset_password_request')->executeQuery();

        $io->section("Modification de la table statut_demande");
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_regroupement_sejour CHANGE statut_demande statut_demande VARCHAR(20) NOT NULL')->executeQuery();

        //Etape 1: Migrer les structures:
        $io->section("Migration des structures");
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure ADD statut VARCHAR(255) NOT NULL, ADD echelon VARCHAR(255) DEFAULT NULL, DROP actif')->executeQuery();
        $this->entityManager->getConnection()->prepare('UPDATE structure set statut = ' . Structure::STATUT_STRUCTURE_AUTONOME);
        $this->entityManager->getConnection()->prepare('UPDATE structure set type ="' . Structure::TYPE_STRUCTURE_CENTRE . '", echelon = "' . Structure::ECHELON_STRUCTURE_LOCAL . '" WHERE type = "Centre permanent national"')->executeQuery();
        $this->entityManager->getConnection()->prepare('UPDATE structure set type ="' . Structure::TYPE_STRUCTURE_REGION . '", echelon = "' . Structure::ECHELON_STRUCTURE_REGIONAL . '" WHERE type = "Région"')->executeQuery();
        $this->entityManager->getConnection()->prepare('UPDATE structure set type ="' . Structure::TYPE_STRUCTURE_SERVICE_VACANCES . '", echelon = "' . Structure::ECHELON_STRUCTURE_LOCAL . '" WHERE type = "Service vacances"')->executeQuery();
        $this->entityManager->getConnection()->prepare('UPDATE structure set type ="' . Structure::TYPE_STRUCTURE_ASSOCIATION . '", echelon = "' . Structure::ECHELON_STRUCTURE_NATIONAL . '" WHERE type = "Sommet"')->executeQuery();
        $this->entityManager->getConnection()->prepare('UPDATE structure set type ="' . Structure::TYPE_STRUCTURE_GROUPE_LOCAL . '", echelon = "' . Structure::ECHELON_STRUCTURE_LOCAL . '" WHERE (type = "Structure locale d\'activité" OR type = "Structure locale rattachée")')->executeQuery();

        //Pas besoin de supprimer les structures sans échelon, elles seront d'elles memes supprimés lors de la prochaine synchro
        //$this->entityManager->getConnection()->prepare('DELETE from structure where echelon is null')->executeQuery();

        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_6F0137EA2685EE64 ON structure (id_jeito)')->executeQuery();
        $io->text("Terminé");

        //Etape 2: Migrer les unités en équipes:
        $io->section("Migration des unités en équipes");
        $this->entityManager->getConnection()->prepare('CREATE TABLE equipe (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, date_creation DATETIME DEFAULT NULL, date_modification DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, date_desactivation DATETIME DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, id_jeito BIGINT NOT NULL, UNIQUE INDEX UNIQ_2449BA152685EE64 (id_jeito), INDEX IDX_2449BA152534008B (structure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE equipe ADD CONSTRAINT FK_2449BA152534008B FOREIGN KEY (structure_id) REFERENCES structure (id)')->executeQuery();
        $data_unites = $this->entityManager->getConnection()->prepare("SELECT * FROM unite")->executeQuery();
        $io->progressStart($data_unites->rowCount());
        $arrayEquipesInserees = array();
        foreach ($data_unites->fetchAllAssociative() as $unite) {

            $id = $unite['id'];
            $date_modification = $unite['date_modification'];
            $structure_id = $unite['structure_parent_id'];
            $typeOriginel = $unite['type'];
            switch ($typeOriginel) {
                case "Cercle":
                    $type = Equipe::TYPE_EQUIPE_CERCLE_LOUVETEAUX;
                    break;
                case "Clan Ainé":
                    $type = Equipe::TYPE_EQUIPE_UNITE_AINE;
                    break;
                case "Ronde":
                    $type = Equipe::TYPE_EQUIPE_RONDE_LUTINS;
                    break;
                case "Unité Défi":
                    $type = Equipe::TYPE_EQUIPE_ACTIVITE_ADAPTEE;
                    break;
                case "Unité Eclé":
                    $type = Equipe::TYPE_EQUIPE_UNITE_ECLE;
                    break;
                case "Unité Nomade":
                    $type = Equipe::TYPE_EQUIPE_UNITE_NOMADE;
                    break;
                default:
                    $type = $typeOriginel;
                    break;
            }
            $nom = $unite['nom'];
            $id_jeito = $unite['id_jeito'];
            //Ne pas prendre en compte les unité sans type
            if ($type !== null) {
                $arrayEquipesInserees[] = $id;
                $this->entityManager->getConnection()->insert(
                    'equipe',
                    [
                        'id' => $id,
                        'structure_id' => $structure_id,
                        'date_creation' => $date_modification,
                        'date_modification' => $date_modification,
                        'type' => $type,
                        'date_desactivation' => null,
                        'nom' => $nom,
                        'id_jeito' => $id_jeito
                    ]
                );
            }

            $io->progressAdvance();
        }
        $io->text("Terminé");
        $io->progressFinish();
        //Libération mémoire
        $data_unites = null;
        unset($data_unites);

        //Migrer les tables relatives à unité
        $io->section("Migration des tables relatives à unité");
        $io->text("structure_regroupement_sejour_unite...");
        //structure_regroupement_sejour_unite -> structure_regroupement_sejour_equipe 
        $this->entityManager->getConnection()->prepare('CREATE TABLE structure_regroupement_sejour_equipe (structure_regroupement_sejour_id INT NOT NULL, equipe_id INT NOT NULL, INDEX IDX_4E0E2C03AE72291D (structure_regroupement_sejour_id), INDEX IDX_4E0E2C036D861B89 (equipe_id), PRIMARY KEY(structure_regroupement_sejour_id, equipe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_regroupement_sejour_equipe ADD CONSTRAINT FK_4E0E2C03AE72291D FOREIGN KEY (structure_regroupement_sejour_id) REFERENCES structure_regroupement_sejour (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_regroupement_sejour_equipe ADD CONSTRAINT FK_4E0E2C036D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id) ON DELETE CASCADE')->executeQuery();

        $data_structures_regroupement_sejour_unite = $this->entityManager->getConnection()->prepare("SELECT * FROM structure_regroupement_sejour_unite")->executeQuery();
        $io->progressStart($data_structures_regroupement_sejour_unite->rowCount());
        foreach ($data_structures_regroupement_sejour_unite->fetchAllAssociative() as $structure_regroupement_sejour_unite) {

            $structure_regroupement_sejour_id = $structure_regroupement_sejour_unite['structure_regroupement_sejour_id'];
            $equipe_id = $structure_regroupement_sejour_unite['unite_id'];
            //Ne pas prendre en compte les unité sans type
            if (in_array($equipe_id, $arrayEquipesInserees)) {
                $this->entityManager->getConnection()->insert(
                    'structure_regroupement_sejour_equipe',
                    [
                        'structure_regroupement_sejour_id' => $structure_regroupement_sejour_id,
                        'equipe_id' => $equipe_id,
                    ]
                );
            }
            $io->progressAdvance();
        }
        $io->progressFinish();
        //Libération mémoire
        $data_structures_regroupement_sejour_unite = null;
        unset($data_structures_regroupement_sejour_unite);


        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_regroupement_sejour_unite DROP FOREIGN KEY FK_EAD22509EC4A74AB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_regroupement_sejour_unite DROP FOREIGN KEY FK_EAD22509AE72291D')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP TABLE structure_regroupement_sejour_unite')->executeQuery();

        $io->text("Terminé");
        //participation_sejour_unite
        $io->text("participation_sejour_unite...");
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite DROP FOREIGN KEY FK_C91D9C0CEC4A74AB')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_C91D9C0CEC4A74AB ON participation_sejour_unite')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite ADD equipe_id INT NULL')->executeQuery();

        $data_participation_sejour_unite = $this->entityManager->getConnection()->prepare("SELECT * FROM participation_sejour_unite")->executeQuery();
        $io->progressStart($data_participation_sejour_unite->rowCount());
        foreach ($data_participation_sejour_unite->fetchAllAssociative() as $participation_sejour_unite) {

            $id = $participation_sejour_unite['id'];
            $equipe_id = $participation_sejour_unite['unite_id'];
            //Ne pas prendre en compte les unité sans type
            if (in_array($equipe_id, $arrayEquipesInserees)) {
                $this->entityManager->getConnection()->update(
                    'participation_sejour_unite',
                    [
                        'equipe_id' => $equipe_id,
                    ],
                    ['id' => $id]
                );
            } else {
                // on supprime des tables suivantes les références à des unités sans type
                $this->entityManager->getConnection()->delete('participant_non_adherent', ['unite_id' => $id]);
                $this->entityManager->getConnection()->delete('role_encadrant_sejour', ['unite_participante_id' => $id]);
                $this->entityManager->getConnection()->delete('participation_sejour_unite_adherent', ['participation_sejour_unite_id' => $id]);
                $this->entityManager->getConnection()->delete('participation_sejour_unite', ['id' => $id]);
                // DELETE FROM user WHERE id = ? (1)
            }
            $io->progressAdvance();
        }
        //Libération mémoire
        $data_participation_sejour_unite = null;
        unset($data_participation_sejour_unite);
        $arrayEquipesInserees = null;
        unset($arrayEquipesInserees);
        $io->progressFinish();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite CHANGE equipe_id equipe_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite ADD CONSTRAINT FK_C91D9C0C6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_C91D9C0C6D861B89 ON participation_sejour_unite (equipe_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite DROP unite_id')->executeQuery();
        //Suppression des anciennes tables
        $this->entityManager->getConnection()->prepare('ALTER TABLE unite DROP FOREIGN KEY FK_1D64C118E422675C')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490AEC4A74AB')->executeQuery();

        $this->entityManager->getConnection()->prepare('DROP TABLE unite')->executeQuery();
        $io->text("Terminé");

        //Etape 3: Migrer les adherents en personnes, adherents, adhesions, employes, et contrats de travail

        $io->section("Migration des adherents en personnes, adherents, adhesions, employes, et contrats de travail");
        //Migration des coordonnées:
        $io->text("Migration des coordonnées...");
        $this->entityManager->getConnection()->prepare('ALTER TABLE coordonnees CHANGE tel_mobile1 tel_mobile VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', CHANGE adresse_mail1 adresse_mail VARCHAR(100) DEFAULT NULL, DROP tel_mobile2, DROP tel_travail, DROP adresse_mail2')->executeQuery();
        $io->text("Terminé");

        //Adherent -> personne, adherent, etc.
        $this->entityManager->getConnection()->prepare('CREATE TABLE adhesion (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, adherent_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, date_modification DATETIME NOT NULL, annule TINYINT(1) NOT NULL, id_jeito BIGINT NOT NULL, INDEX IDX_C50CA65A2534008B (structure_id), INDEX IDX_C50CA65A25F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE TABLE contrat_travail (id INT AUTO_INCREMENT NOT NULL, employe_id INT NOT NULL, date_debut DATETIME NOT NULL, date_modification DATETIME NOT NULL, date_fin DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, nom VARCHAR(255) DEFAULT NULL, id_jeito BIGINT NOT NULL, INDEX IDX_394729CD1B65292 (employe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE TABLE employe (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, id_jeito BIGINT NOT NULL, date_modification DATETIME NOT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', matricule VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_F804D3B9A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, coordonnees_id INT DEFAULT NULL, responsable_legal1_id INT DEFAULT NULL, responsable_legal2_id INT DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT NULL, date_derniere_connexion DATETIME DEFAULT NULL, date_modification DATETIME NOT NULL, id_jeito BIGINT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATE DEFAULT NULL, genre VARCHAR(20) NOT NULL, adresse_mail VARCHAR(50) NOT NULL, nom_urgence VARCHAR(200) DEFAULT NULL, prenom_urgence VARCHAR(200) DEFAULT NULL, tel_urgence VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', lien_urgence VARCHAR(200) DEFAULT NULL, frequence_notification VARCHAR(50) NOT NULL, droit_image TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_FCEC9EF2685EE64 (id_jeito), UNIQUE INDEX UNIQ_FCEC9EF5853DEDF (coordonnees_id), INDEX IDX_FCEC9EF39FF2EE9 (responsable_legal1_id), INDEX IDX_FCEC9EF2B4A8107 (responsable_legal2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE contrat_travail ADD CONSTRAINT FK_394729CD1B65292 FOREIGN KEY (employe_id) REFERENCES employe (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE employe ADD CONSTRAINT FK_F804D3B9A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF5853DEDF FOREIGN KEY (coordonnees_id) REFERENCES coordonnees (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF39FF2EE9 FOREIGN KEY (responsable_legal1_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF2B4A8107 FOREIGN KEY (responsable_legal2_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE personne CHANGE nom nom VARCHAR(100) NOT NULL, CHANGE prenom prenom VARCHAR(100) NOT NULL, CHANGE adresse_mail adresse_mail VARCHAR(254) NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE employe DROP matricule, CHANGE id_jeito numero_salarie BIGINT NOT NULL')->executeQuery();


        $io->text("Suppression des clefs étrangeres sur l'adhérent·e dans les tables");
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EC5C58C')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0601C706A62')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0605853DEDF')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_90D3F0605853DEDF ON adherent')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_90D3F060EC5C58C ON adherent')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX UNIQ_90D3F0601C706A62 ON adherent')->executeQuery();
        //commentaire
        $this->entityManager->getConnection()->prepare('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_67F068BC25F06C53 ON commentaire')->executeQuery();
        //structure_organisation
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_organisation DROP FOREIGN KEY FK_A9AF96C425F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_A9AF96C425F06C53 ON structure_organisation')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON structure_organisation')->executeQuery();
        //structure_validation
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_validation DROP FOREIGN KEY FK_24E26F2025F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_24E26F2025F06C53 ON structure_validation')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON structure_validation')->executeQuery();
        //sejour_encadrement
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_encadrement DROP FOREIGN KEY FK_36F4A74F25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_36F4A74F25F06C53 ON sejour_encadrement')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON sejour_encadrement')->executeQuery();
        //sejour_suivi
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_suivi DROP FOREIGN KEY FK_C69CAC9B25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_C69CAC9B25F06C53 ON sejour_suivi')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON sejour_suivi')->executeQuery();
        //role_encadrant_sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE role_encadrant_sejour DROP FOREIGN KEY FK_E8D578136B84926')->executeQuery();
        //demande_moderation_commentaire
        $this->entityManager->getConnection()->prepare('ALTER TABLE demande_moderation_commentaire DROP FOREIGN KEY FK_EE52F1A525F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_EE52F1A525F06C53 ON demande_moderation_commentaire')->executeQuery();
        //structure_suivi
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_suivi DROP FOREIGN KEY FK_FB6D911925F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_FB6D911925F06C53 ON structure_suivi')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON structure_suivi')->executeQuery();
        //sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour DROP FOREIGN KEY FK_96F52028E82E7EE8')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour DROP FOREIGN KEY FK_96F520288A141997')->executeQuery();
        //notification
        $this->entityManager->getConnection()->prepare('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_BF5476CA25F06C53 ON notification')->executeQuery();
        //nomination_visite_sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC7F72333D')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE nomination_visite_sejour DROP FOREIGN KEY FK_1080DFBC4A7FE23C')->executeQuery();
        //sejour_direction_adjointe
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_direction_adjointe DROP FOREIGN KEY FK_9C96E83A25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_9C96E83A25F06C53 ON sejour_direction_adjointe')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON sejour_direction_adjointe')->executeQuery();
        //participation_sejour_unite_adherent
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite_adherent DROP FOREIGN KEY FK_10E655D8D1A99552')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite_adherent DROP FOREIGN KEY FK_10E655D825F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_10E655D8D1A99552 ON participation_sejour_unite_adherent')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX IDX_10E655D825F06C53 ON participation_sejour_unite_adherent')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP INDEX `primary` ON participation_sejour_unite_adherent')->executeQuery();

        $io->section("Suppression des fonctions existantes");
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A2534008B')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction_associative DROP FOREIGN KEY FK_9700490A25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP TABLE fonction_associative')->executeQuery();

        $io->text("Terminé");

        //on ajoute les colonnes dans adhérent
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent ADD personne_id INT NOT NULL, ADD statut VARCHAR(255) DEFAULT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent CHANGE date_modification_portail date_modification DATETIME NOT NULL')->executeQuery();


        //On a besoin de stocker les infos de paires: numéro adhérent <->idJéito de la personne qui correspond
        $io->text("Chargement des couples IDJeito de Personne <-> numero d'adhérent...");

        $arrayIdJeitoPersonnesByNumAdherent = array();
        $ADHERENTS = "adherents";
        $BATCH_SIZE = 1000;
        $n = 0;
        $apiResult = $this->getAPIResult($ADHERENTS, $BATCH_SIZE, $n * $BATCH_SIZE);
        $dataAdherents = $apiResult['results'];
        $nombreTotalAdherents = $apiResult['count'];
        $nb_adherents_traites = 0;
        $dernierePageParcourue = false;
        $io->progressStart($nombreTotalAdherents);
        while (!$dernierePageParcourue) {
            foreach ($dataAdherents as $dataAdherent) {
                $arrayIdJeitoPersonnesByNumAdherent[$dataAdherent['id']] = $dataAdherent['person_id'];
                //Si l'adhérent à des doublons, on les stocke
                foreach ($dataAdherent['duplicates'] as $duplicate) {
                    $this->arrayIdJeitoDuplicataPersonnesByNumAdherent[$duplicate] = $dataAdherent['person_id'];
                }
                $arrayIdJeitoPersonnesByNumAdherent[$dataAdherent['id']] = $dataAdherent['person_id'];
                $nb_adherents_traites++;
                $io->progressAdvance();
            }
            if ($nombreTotalAdherents > ($nb_adherents_traites)) {
                $n++;
                $apiResult =  $this->getAPIResult($ADHERENTS, $BATCH_SIZE, $n * $BATCH_SIZE);

                $dataAdherents  = $apiResult['results'];

                gc_collect_cycles();
            } else {
                $dernierePageParcourue = true;
            }
        }

        //Liberation mémoire
        $dataAdherents = null;
        unset($dataAdherents);
        $io->progressFinish();
        $io->text("Terminé");

        $data_adherents = $this->entityManager->getConnection()->prepare("SELECT * FROM adherent")->executeQuery();
        $io->progressStart($data_adherents->rowCount());
        $io->text("Séparation des adhérents en personnes et adhérents...");
        $arrayIdAdherentEnDoublonParIdJeitoPersonne = array();
        foreach ($data_adherents->fetchAllAssociative() as $data_adherent) {

            $id = $data_adherent['id']; // On conserve le même id pour la personne
            $numero_adherent = $data_adherent['numero_adherent'];
            $coordonnees_id = $data_adherent['coordonnees_id'];
            $responsable_legal1_id = null;
            $responsable_legal2_id = null;
            $roles = $data_adherent['roles'];
            $password = $data_adherent['password'];
            $date_derniere_connexion = $data_adherent['date_derniere_connexion'];
            $date_modification = $dateCourante->format("Y-m-d");

            $nom = $data_adherent['nom'];
            $prenom = $data_adherent['prenom'];
            $date_naissance = $data_adherent['date_naissance'];

            switch ($data_adherent['genre']) {
                case "Féminin":
                    $genre = Personne::PERSONNE_GENRE_FEMININ;
                    break;
                case "Masculin":
                    $genre = Personne::PERSONNE_GENRE_MASCULIN;
                    break;
                default:
                    $genre = Personne::PERSONNE_GENRE_AUTRE;
                    break;
            }
            $adresse_mail = $data_adherent['adresse_mail'];
            $nom_urgence = $data_adherent['nom_urgence'];
            $prenom_urgence = $data_adherent['prenom_urgence'];
            $tel_urgence = $data_adherent['tel_urgence'];
            $lien_urgence = $data_adherent['lien_urgence'];
            $frequence_notification = $data_adherent['frequence_notification'];
            $droit_image = $data_adherent['droit_image'];
            if (array_key_exists($numero_adherent, $arrayIdJeitoPersonnesByNumAdherent)) {
                $id_jeito_personne = $arrayIdJeitoPersonnesByNumAdherent[$numero_adherent];

                $this->entityManager->getConnection()->insert(
                    'personne',
                    [
                        'id' => $id,
                        'coordonnees_id' => $coordonnees_id,
                        'responsable_legal1_id' => $responsable_legal1_id,
                        'responsable_legal2_id' => $responsable_legal2_id,
                        'roles' => $roles,
                        'password' => $password,
                        'date_derniere_connexion' => $date_derniere_connexion,
                        'date_modification' => $date_modification,
                        'id_jeito' => $id_jeito_personne,
                        'nom' => $nom,
                        'prenom' => $prenom,
                        'date_naissance' => $date_naissance,
                        'genre' => $genre,
                        'adresse_mail' => $adresse_mail,
                        'nom_urgence' => $nom_urgence,
                        'prenom_urgence' => $prenom_urgence,
                        'tel_urgence' => $tel_urgence,
                        'lien_urgence' => $lien_urgence,
                        'frequence_notification' => $frequence_notification,
                        'droit_image' => $droit_image

                    ]
                );
                //On considère que dans toutes les entrées, chacune est adhérent·e:

                $this->entityManager->getConnection()->update(
                    'adherent',
                    [
                        'numero_adherent' => $numero_adherent,
                        'statut' => Adherent::STATUT_ADHERENT_OK,
                        'personne_id' => $id,
                    ],
                    ['id' => $id]
                );
                // UPDATE coordonnees VALUES (?) WHERE id = ? (jwage, 1)

            } else {
                if (!array_key_exists($numero_adherent, $this->arrayIdJeitoDuplicataPersonnesByNumAdherent)) {
                    $this->afficherMessage("La personne avec le numéro d'adhérent " . $numero_adherent . " n'existe pas dans Jéito", self::NIVEAU_MESSAGE_WARNING, $io);
                    $this->entityManager->getConnection()->delete('commentaire', ['adherent_id' => $id]);
                    $this->entityManager->getConnection()->delete('notification', ['adherent_id' => $id]);
                    $this->entityManager->getConnection()->delete('adherent', ['id' => $id]);
                    $this->entityManager->getConnection()->delete('participation_sejour_unite_adherent', ['adherent_id' => $id]);
                } else {
                    //S'il s'agit d'un doublon, on récupère la personne corresponde et on change toutes les référen
                    $this->afficherMessage("La personne avec le numéro d'adhérent " . $numero_adherent . " est en doublon", self::NIVEAU_MESSAGE_WARNING, $io);
                    $arrayIdAdherentEnDoublonParIdJeitoPersonne[$id] = $this->arrayIdJeitoDuplicataPersonnesByNumAdherent[$numero_adherent];
                }
            }
            $io->progressAdvance();
        }
        //Libération mémoire
        $data_adherents = null;
        unset($data_adherents);
        $arrayIdJeitoPersonnesByNumAdherent = null;
        unset($arrayIdJeitoPersonnesByNumAdherent);
        $io->progressFinish();
        $io->text("Terminé");

        $io->text("gestion des doublons");
        $io->progressStart(count($arrayIdAdherentEnDoublonParIdJeitoPersonne));
        foreach ($arrayIdAdherentEnDoublonParIdJeitoPersonne as $doublonIdAdherentATraiter => $idJeitoPersonne) {
            $this->gestionDoublonAdherent($doublonIdAdherentATraiter, $idJeitoPersonne, $io);
            $io->progressAdvance();
        }
        //Libération mémoire
        $arrayIdAdherentEnDoublonParIdJeitoPersonne = null;
        unset($arrayIdAdherentEnDoublonParIdJeitoPersonne);
        $io->progressFinish();
        $io->text("Terminé");


        $io->text("Suppression des colonnes en trop dans la table adhérent");
        //on enleve les colonnes en trop dans adhérent
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent DROP coordonnees_id, DROP coordonnees_responsable_legal1_id, DROP coordonnees_responsable_legal2_id, DROP nom, DROP prenom, DROP date_naissance, DROP droit_image, DROP genre, DROP status_adhesion, DROP adresse_mail, DROP roles, DROP is_verified, DROP date_verification_email, DROP date_derniere_connexion, DROP nom_urgence, DROP prenom_urgence, DROP tel_urgence, DROP lien_urgence, DROP frequence_notification, DROP password')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE UNIQUE INDEX UNIQ_90D3F060A21BD112 ON adherent (personne_id)')->executeQuery();
        $io->text("Terminé");
        //Commentaire:
        $io->text("Modification dans la table commentaire");
        $this->entityManager->getConnection()->prepare('ALTER TABLE commentaire CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BCA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_67F068BCA21BD112 ON commentaire (personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table structure_organisation");
        //structure_organisation
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_organisation CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_organisation ADD CONSTRAINT FK_A9AF96C4A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_A9AF96C4A21BD112 ON structure_organisation (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_organisation ADD PRIMARY KEY (structure_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table structure_validation");
        //structure_validation
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_validation CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_validation ADD CONSTRAINT FK_24E26F20A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_24E26F20A21BD112 ON structure_validation (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_validation ADD PRIMARY KEY (structure_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table structure_suivi");
        //structure_suivi
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_suivi CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_suivi ADD CONSTRAINT FK_FB6D9119A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_FB6D9119A21BD112 ON structure_suivi (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE structure_suivi ADD PRIMARY KEY (structure_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table sejour_encadrement");
        //sejour_encadrement
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_encadrement CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_encadrement ADD CONSTRAINT FK_36F4A74FA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_36F4A74FA21BD112 ON sejour_encadrement (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_encadrement ADD PRIMARY KEY (sejour_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table sejour_suivi");
        //sejour_suivi
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_suivi CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_suivi ADD CONSTRAINT FK_C69CAC9BA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_C69CAC9BA21BD112 ON sejour_suivi (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_suivi ADD PRIMARY KEY (sejour_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table sejour");
        //sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour ADD CONSTRAINT FK_96F52028E82E7EE8 FOREIGN KEY (directeur_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour ADD CONSTRAINT FK_96F520288A141997 FOREIGN KEY (accompagnant_id) REFERENCES personne (id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table sejour_direction_adjointe");
        //sejour_direction_adjointe
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_direction_adjointe CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_direction_adjointe ADD CONSTRAINT FK_9C96E83AA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_9C96E83AA21BD112 ON sejour_direction_adjointe (personne_id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_direction_adjointe ADD PRIMARY KEY (sejour_id, personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table role_encadrant_sejour");
        //role_encadrant_sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE role_encadrant_sejour ADD CONSTRAINT FK_E8D578136B84926 FOREIGN KEY (encadrant_eedf_id) REFERENCES personne (id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table demande_moderation_commentaire");
        //demande_moderation_commentaire
        $this->entityManager->getConnection()->prepare('ALTER TABLE demande_moderation_commentaire CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE demande_moderation_commentaire ADD CONSTRAINT FK_EE52F1A5A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_EE52F1A5A21BD112 ON demande_moderation_commentaire (personne_id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table nomination_visite_sejour");
        //nomination_visite_sejour
        $this->entityManager->getConnection()->prepare('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC7F72333D FOREIGN KEY (visiteur_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE nomination_visite_sejour ADD CONSTRAINT FK_1080DFBC4A7FE23C FOREIGN KEY (nomme_par_id) REFERENCES personne (id)')->executeQuery();
        $io->text("Terminé");

        $io->text("Modification dans la table notification");
        //notification
        $this->entityManager->getConnection()->prepare('ALTER TABLE notification CHANGE adherent_id personne_id INT NOT NULL')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)')->executeQuery();
        $this->entityManager->getConnection()->prepare('CREATE INDEX IDX_BF5476CAA21BD112 ON notification (personne_id)')->executeQuery();
        $io->text("Terminé");


        $io->text("Insertion dans la table sejour_personne");
        //sejour_adherent -> sejour_personne
        $this->entityManager->getConnection()->prepare('CREATE TABLE sejour_personne (sejour_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_1C282E4284CF0CF (sejour_id), INDEX IDX_1C282E42A21BD112 (personne_id), PRIMARY KEY(sejour_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_personne ADD CONSTRAINT FK_1C282E4284CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_personne ADD CONSTRAINT FK_1C282E42A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();

        $sejours_adherent = $this->entityManager->getConnection()->prepare("SELECT * FROM sejour_adherent")->executeQuery();
        $io->progressStart($sejours_adherent->rowCount());
        foreach ($sejours_adherent->fetchAllAssociative() as $sejour_adherent) {

            $sejour_id = $sejour_adherent['sejour_id'];
            //L'ID de personne est le meme que l'id d'adhérent
            $personne_id = $sejour_adherent['adherent_id'];

            $this->entityManager->getConnection()->insert(
                'sejour_personne',
                [
                    'sejour_id' => $sejour_id,
                    'personne_id' => $personne_id,
                ]
            );
            $io->progressAdvance();
        }
        //Libération mémoire
        $sejour_adherent = null;
        unset($sejour_adherent);
        $sejours_adherent = null;
        unset($sejours_adherent);

        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_adherent DROP FOREIGN KEY FK_833517CD25F06C53')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE sejour_adherent DROP FOREIGN KEY FK_833517CD84CF0CF')->executeQuery();
        $this->entityManager->getConnection()->prepare('DROP TABLE sejour_adherent')->executeQuery();
        $io->progressFinish();
        $io->text("Terminé");


        $io->text("Insertion dans la table participation_sejour_unite_personne");
        //participation_sejour_unite_adherent -> participation_sejour_unite_personne
        $this->entityManager->getConnection()->prepare('CREATE TABLE participation_sejour_unite_personne (participation_sejour_unite_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_8FFB6C57D1A99552 (participation_sejour_unite_id), INDEX IDX_8FFB6C57A21BD112 (personne_id), PRIMARY KEY(participation_sejour_unite_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite_personne ADD CONSTRAINT FK_8FFB6C57A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE participation_sejour_unite_personne ADD CONSTRAINT FK_8FFB6C57D1A99552 FOREIGN KEY (participation_sejour_unite_id) REFERENCES participation_sejour_unite (id) ON DELETE CASCADE')->executeQuery();

        $participations_sejour_unite_adherent = $this->entityManager->getConnection()->prepare("SELECT DISTINCT participation_sejour_unite_id, adherent_id  FROM participation_sejour_unite_adherent")->executeQuery();
        $io->progressStart($participations_sejour_unite_adherent->rowCount());
        foreach ($participations_sejour_unite_adherent->fetchAllAssociative() as $participation_sejour_unite_adherent) {

            $participation_sejour_unite_id = $participation_sejour_unite_adherent['participation_sejour_unite_id'];
            $personne_id = $participation_sejour_unite_adherent['adherent_id'];
            $this->entityManager->getConnection()->insert(
                'participation_sejour_unite_personne',
                [
                    'participation_sejour_unite_id' => $participation_sejour_unite_id,
                    'personne_id' => $personne_id,
                ]
            );
            $io->progressAdvance();
        }
        //Libération mémoire
        $participations_sejour_unite_adherent = null;
        unset($participations_sejour_unite_adherent);

        $this->entityManager->getConnection()->prepare('DROP TABLE participation_sejour_unite_adherent')->executeQuery();
        $io->progressFinish();
        $io->text("Terminé");


        //Etape 4: Migrer les fonctions:
        $this->entityManager->getConnection()->prepare('CREATE TABLE fonction (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, equipe_id INT NOT NULL, id_jeito BIGINT DEFAULT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, statut VARCHAR(255) NOT NULL, date_modification DATETIME NOT NULL, UNIQUE INDEX UNIQ_900D5BD2685EE64 (id_jeito), INDEX IDX_900D5BDA21BD112 (personne_id), INDEX IDX_900D5BD6D861B89 (equipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BDA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE')->executeQuery();
        $this->entityManager->getConnection()->prepare('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BD6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)')->executeQuery();

        $io->text("Terminé");
        gc_collect_cycles();


        $io->section("Lancement de la commmande de synchronisation");
        $env = 'prod';
        $kernel = new Kernel($env, false);
        $application = new Application($kernel);
        $returnCode = $application->run(new ArrayInput([
            'command' => 'roads:synchro-portail',
            '--env' => 'prod',
            '--no-debug' => true
        ]), $output);
        return Command::SUCCESS;
    }

    function afficherMessage(String $message, int $niveauInformation, SymfonyStyle $io)
    {
        switch ($niveauInformation) {
            case self::NIVEAU_MESSAGE_SUCCESS:
                $io->success($message);
                break;
            case self::NIVEAU_MESSAGE_INFO:
                $io->info($message);
                break;
            case self::NIVEAU_MESSAGE_WARNING:
                $io->warning($message);
                $message = "Attention: " . $message;
                break;
            case self::NIVEAU_MESSAGE_TEXT:
                $io->text($message);
                break;
            case self::NIVEAU_MESSAGE_SECTION:
                $io->section($message);
                break;
        }
    }
    function getAPIResult(String $urlSuffix, $limit = 100, $offset = 0)
    {
        $httpClientStructures = HttpClient::create();
        $httpClientStructures = new ScopingHttpClient(
            $httpClientStructures,
            [
                $this->jeitoApiUrl . $urlSuffix => [
                    'headers' => [
                        'Authorization' => 'token ' . $this->jeitoApiToken
                    ]
                ]
            ]
        );
        $response = $httpClientStructures->request(
            'GET',
            $this->jeitoApiUrl . $urlSuffix,
            [
                // these values are automatically encoded before including them in the URL
                'query' => [
                    'limit' => $limit,
                    'offset' => $offset
                ]
            ]
        );


        $content = $response->getContent();

        return json_decode($content, true);
    }

    function gestionDoublonAdherent($idAdherentDoublon, $idJeitoPersonne, $io)
    {
        $idPersonneCorrespondant = $this->entityManager->getConnection()->prepare("SELECT id FROM personne WHERE id_jeito=" . $idJeitoPersonne)->executeQuery()->fetchOne();
        if ($idPersonneCorrespondant !== null) {
            $this->afficherMessage("idPersonneCorrespondant:" . $idPersonneCorrespondant, self::NIVEAU_MESSAGE_WARNING, $io);

            $this->entityManager->getConnection()->update(
                'commentaire',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'notification',
                [
                    'adherent_id' => $idPersonneCorrespondant
                ],
                ['adherent_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'notification',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            /* On ne gère pas les doublons pour cette table, on supprime pourement et simplement 
            $this->entityManager->getConnection()->update(
                'structure_organisation',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );*/
            $this->entityManager->getConnection()->delete('structure_organisation', ['adherent_id' => $idAdherentDoublon]);
            /* On ne gère pas les doublons pour cette table, on supprime pourement et simplement 
            $this->entityManager->getConnection()->update(
                'structure_validation',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            */

            $this->entityManager->getConnection()->delete('structure_validation', ['adherent_id' => $idAdherentDoublon]);
            /* On ne gère pas les doublons pour cette table, on supprime pourement et simplement 
            $this->entityManager->getConnection()->update(
                'structure_suivi',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            */
            $this->entityManager->getConnection()->delete('structure_suivi', ['adherent_id' => $idAdherentDoublon]);
            /* On ne gère pas les doublons pour cette table, on supprime pourement et simplement 
            $this->entityManager->getConnection()->update(
                'sejour_encadrement',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            */
            $this->entityManager->getConnection()->delete('sejour_encadrement', ['adherent_id' => $idAdherentDoublon]);

            /* On ne gère pas les doublons pour cette table, on supprime pourement et simplement 
            $this->entityManager->getConnection()->update(
                'sejour_suivi',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            */
            $this->entityManager->getConnection()->delete('sejour_suivi', ['adherent_id' => $idAdherentDoublon]);
            $this->entityManager->getConnection()->update(
                'sejour',
                ['directeur_id' => $idPersonneCorrespondant],
                ['directeur_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'sejour',
                ['accompagnant_id' => $idPersonneCorrespondant],
                ['accompagnant_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'sejour_direction_adjointe',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'role_encadrant_sejour',
                ['encadrant_eedf_id' => $idPersonneCorrespondant],
                ['encadrant_eedf_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'demande_moderation_commentaire',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'nomination_visite_sejour',
                ['nomme_par_id' => $idPersonneCorrespondant],
                ['nomme_par_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'nomination_visite_sejour',
                ['visiteur_id' => $idPersonneCorrespondant],
                ['visiteur_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'sejour_adherent',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            $this->entityManager->getConnection()->update(
                'participation_sejour_unite_adherent',
                ['adherent_id' => $idPersonneCorrespondant],
                ['adherent_id' => $idAdherentDoublon]
            );
            //Puis on supprime l'adhérent correspondant
            $this->entityManager->getConnection()->delete('adherent', ['id' => $idAdherentDoublon]);
        } else {
            $this->afficherMessage("Bizarre la personne avec l'idJéito " . $idJeitoPersonne . "n'existe pas dans ROADS", self::NIVEAU_MESSAGE_WARNING, $io);
        }
    }
}
