<?php

namespace App\Middleware;

use App\Contracts\RetryCountSupportInterface;
use App\Messenger\Stamp\HandlerArgumentsStamp as StampHandlerArgumentsStamp;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

class GetRoutingKeyMiddleware implements MiddlewareInterface
{
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        # Check based on interface, class, stamp or something else
        if ($envelope->last(AmqpReceivedStamp::class)) {
            $message = $envelope->getMessage();
            $message->setRoutingKey($envelope->last(AmqpReceivedStamp::class)->getAmqpEnvelope()->getRoutingKey());
            $envelope->wrap($message);
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
