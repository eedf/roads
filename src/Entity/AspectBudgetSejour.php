<?php

namespace App\Entity;

use App\Repository\AspectBudgetSejourRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Aspects budgétaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AspectBudgetSejourRepository::class)]
class AspectBudgetSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'aspectBudgetSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotNull(groups: ["envoi_pour_validation"], message: "Le prix moyen du séjour doit être renseigné")]
    #[Groups(["fin_sejour"])]
    private ?int $prixMoyen = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Assert\NotNull(groups: ["envoi_pour_validation"], message: "Le budget prévisionnel doit être chargé")]
    #[Groups(["fin_sejour"])]
    private ?Document $budgetPrevisionnel = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getPrixMoyen(): ?int
    {
        return $this->prixMoyen;
    }

    public function setPrixMoyen(?int $prixMoyen): self
    {
        $this->prixMoyen = $prixMoyen;

        return $this;
    }

    public function getBudgetPrevisionnel(): ?Document
    {
        return $this->budgetPrevisionnel;
    }

    public function setBudgetPrevisionnel(?Document $budgetPrevisionnel): self
    {
        $this->budgetPrevisionnel = $budgetPrevisionnel;

        return $this;
    }
}
