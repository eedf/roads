<?php

namespace App\Entity;

use App\Repository\ParticipantNonAdherentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Informations personnelles d'un·e participant·e à un séjour sur une unité EEDF
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: ParticipantNonAdherentRepository::class)]
class ParticipantNonAdherent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: "participantsNonAdherents")]
    #[ORM\JoinColumn(nullable: false)]
    private ?ParticipationSejourUnite $unite = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $nom;


    #[ORM\Column(length: 255)]
    #[Groups(["fin_sejour"])]
    private ?string $prenom;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?int $age = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $commentaire = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUnite(): ?ParticipationSejourUnite
    {
        return $this->unite;
    }

    public function setUnite(?ParticipationSejourUnite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
}
