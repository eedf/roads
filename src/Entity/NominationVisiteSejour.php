<?php

namespace App\Entity;

use App\Repository\NominationVisiteSejourRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Nomination pour visiter un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: NominationVisiteSejourRepository::class)]
class NominationVisiteSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: "nominationsVisiteSejour")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;


    #[ORM\ManyToOne(inversedBy: "nominationsVisiteSejour")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $visiteur = null;

    #[ORM\ManyToOne(inversedBy: "attributionsVisiteSejour")]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $nommePar = null;


    #[ORM\OneToOne(mappedBy: "nominationVisite", cascade: ['persist', 'remove'])]
    private ?VisiteSejour $visiteSejour = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(?Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getVisiteur(): ?Personne
    {
        return $this->visiteur;
    }

    public function setVisiteur(?Personne $visiteur): self
    {
        $this->visiteur = $visiteur;

        return $this;
    }

    public function getNommePar(): ?Personne
    {
        return $this->nommePar;
    }

    public function setNommePar(?Personne $nommePar): self
    {
        $this->nommePar = $nommePar;

        return $this;
    }

    public function getVisiteSejour(): ?VisiteSejour
    {
        return $this->visiteSejour;
    }

    public function setVisiteSejour(VisiteSejour $visiteSejour): self
    {
        // set the owning side of the relation if necessary
        if ($visiteSejour->getNominationVisite() !== $this) {
            $visiteSejour->setNominationVisite($this);
        }

        $this->visiteSejour = $visiteSejour;

        return $this;
    }
}
