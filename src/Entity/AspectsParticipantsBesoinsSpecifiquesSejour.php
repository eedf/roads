<?php

namespace App\Entity;

use App\Repository\AspectsParticipantsBesoinsSpecifiquesSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Participants à besoin spécifique d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AspectsParticipantsBesoinsSpecifiquesSejourRepository::class)]
class AspectsParticipantsBesoinsSpecifiquesSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'aspectsParticipantsBesoinsSpecifiquesSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?int $nombreParticipantsHandicap;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $descParticipantsBesoinsSpecifiques = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getNombreParticipantsHandicap(): ?int
    {
        return $this->nombreParticipantsHandicap;
    }

    public function setNombreParticipantsHandicap(int $nombreParticipantsHandicap): self
    {
        $this->nombreParticipantsHandicap = $nombreParticipantsHandicap;

        return $this;
    }

    public function getDescParticipantsBesoinsSpecifiques(): ?string
    {
        return $this->descParticipantsBesoinsSpecifiques;
    }

    public function setDescParticipantsBesoinsSpecifiques(?string $descParticipantsBesoinsSpecifiques): self
    {
        $this->descParticipantsBesoinsSpecifiques = $descParticipantsBesoinsSpecifiques;

        return $this;
    }
}
