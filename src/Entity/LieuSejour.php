<?php

namespace App\Entity;

use App\Repository\LieuSejourRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Lieu de séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: LieuSejourRepository::class)]
class LieuSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nom = null;

    #[ORM\Column]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $estEEDF = false;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $siteInternet = null;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?bool $proprietaireEstPersonnePhysique = false;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(['fin_sejour'])]
    private ?Document $conventionLieu = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(['fin_sejour'])]
    private ?Coordonnees $coordonneesProprietaire = null;

    #[ORM\OneToOne(inversedBy: "lieuSejour", cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\Valid()]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Localisation $localisation = null;

    #[ORM\OneToOne(mappedBy: "lieu", cascade: ['persist', 'remove'])]
    private ?UtilisationLieuSejour $utilisationLieuSejour = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEstEEDF(): ?bool
    {
        return $this->estEEDF;
    }

    public function setEstEEDF(?bool $estEEDF): self
    {
        $this->estEEDF = $estEEDF;

        return $this;
    }

    public function getSiteInternet(): ?string
    {
        return $this->siteInternet;
    }

    public function setSiteInternet(?string $siteInternet): self
    {
        $this->siteInternet = $siteInternet;

        return $this;
    }

    public function getProprietaireEstPersonnePhysique(): ?bool
    {
        return $this->proprietaireEstPersonnePhysique;
    }

    public function setProprietaireEstPersonnePhysique(?bool $proprietaireEstPersonnePhysique): self
    {
        $this->proprietaireEstPersonnePhysique = $proprietaireEstPersonnePhysique;

        return $this;
    }

    public function getConventionLieu(): ?Document
    {
        return $this->conventionLieu;
    }

    public function setConventionLieu(?Document $conventionLieu): self
    {
        $this->conventionLieu = $conventionLieu;

        return $this;
    }

    public function getCoordonneesProprietaire(): ?Coordonnees
    {
        return $this->coordonneesProprietaire;
    }

    public function setCoordonneesProprietaire(?Coordonnees $coordonneesProprietaire): self
    {
        $this->coordonneesProprietaire = $coordonneesProprietaire;

        return $this;
    }

    public function getLocalisation(): ?Localisation
    {
        return $this->localisation;
    }

    public function setLocalisation(Localisation $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getUtilisationLieuSejour(): ?UtilisationLieuSejour
    {
        return $this->utilisationLieuSejour;
    }

    public function setUtilisationLieuSejour(?UtilisationLieuSejour $utilisationLieuSejour): self
    {
        // unset the owning side of the relation if necessary
        if ($utilisationLieuSejour === null && $this->utilisationLieuSejour !== null) {
            $this->utilisationLieuSejour->setLieu(null);
        }

        // set the owning side of the relation if necessary
        if ($utilisationLieuSejour !== null && $utilisationLieuSejour->getLieu() !== $this) {
            $utilisationLieuSejour->setLieu($this);
        }

        $this->utilisationLieuSejour = $utilisationLieuSejour;

        return $this;
    }
}
