<?php

namespace App\Entity;

use App\Repository\UtilisationLieuSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Utilisation d'un lieu par un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 * 
 */
#[ORM\Entity(repositoryClass: UtilisationLieuSejourRepository::class)]
#[Assert\Callback(callback: "validerLocalisation")]
class UtilisationLieuSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'utilisationsLieuSejour')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateDepart = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateArrivee = null;


    #[ORM\OneToOne(inversedBy: 'utilisationLieuSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\Valid]
    private ?LieuSejour $lieu = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention"])]
    private ?string $zoneEnvisagee = null;

    #[ORM\Column]
    #[Groups(["save_declaration_intention"])]
    private ?bool $estDefini = true;

    public function __construct()
    {

        $get_arguments       = func_get_args();
        $number_of_arguments = func_num_args();

        if ($number_of_arguments == 1) {
            /** @var Sejour $sehour */
            $sejour = $get_arguments[0];
            $this->dateArrivee = clone $sejour->getDateDebut();
            $this->dateDepart = clone $sejour->getDateFin();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(?Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->dateDepart;
    }

    public function setDateDepart(?\DateTimeInterface $dateDepart): self
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    public function getDateArrivee(): ?\DateTimeInterface
    {
        return $this->dateArrivee;
    }

    public function setDateArrivee(?\DateTimeInterface $dateArrivee): self
    {
        $this->dateArrivee = $dateArrivee;

        return $this;
    }

    public function getLieu(): ?LieuSejour
    {
        return $this->lieu;
    }

    public function setLieu(?LieuSejour $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getZoneEnvisagee(): ?string
    {
        return $this->zoneEnvisagee;
    }

    public function setZoneEnvisagee(?string $zoneEnvisagee): self
    {
        $this->zoneEnvisagee = $zoneEnvisagee;

        return $this;
    }

    public function getEstDefini(): ?bool
    {
        return $this->estDefini;
    }

    public function setEstDefini(bool $estDefini): self
    {
        $this->estDefini = $estDefini;

        return $this;
    }
    /**
     * Call back de validation de l'entité lors (déclenche une erreur si les contraintes ne sont pas respectées):
     * - Le lieu doit être renseigné géographiquement, c'est-à dire-pointé sur la carte (les coordonnées GPS doivent être renseignés)
     * @param  mixed $context
     * @param  mixed $payload
     * @return void
     */
    public function validerLocalisation(ExecutionContextInterface $context, $payload)
    {
        if ($this->estDefini && $this->lieu && $this->lieu->getLocalisation() && $this->lieu->getLocalisation()->getCoordonneesGeo() == null) {
            $context->buildViolation("Le lieu doit être renseigné géographiquement, c'est-à dire-pointé sur la carte")
                ->addViolation();
        }
    }
}
