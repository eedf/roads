<?php

namespace App\Entity;

use App\Repository\AspectAlimentaireSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Aspects alimentaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AspectAlimentaireSejourRepository::class)]
class AspectAlimentaireSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'aspectAlimentaireSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $choixOrganisation = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["fin_sejour"])]
    private ?Document $grilleMenus = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getChoixOrganisation(): ?string
    {
        return $this->choixOrganisation;
    }

    public function setChoixOrganisation(?string $choixOrganisation): self
    {
        $this->choixOrganisation = $choixOrganisation;

        return $this;
    }

    public function getGrilleMenus(): ?Document
    {
        return $this->grilleMenus;
    }

    public function setGrilleMenus(?Document $grilleMenus): self
    {
        $this->grilleMenus = $grilleMenus;

        return $this;
    }
}
