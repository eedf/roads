<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;


#[ORM\Entity(repositoryClass: PersonneRepository::class)]
#[UniqueEntity(fields: ["uuid"], message: "There is already an account with this uuid")]
class Personne implements UserInterface
{

    //Fréquences de notification
    const FREQUENCE_NOTIFICATION_JAMAIS = "Jamais";
    const FREQUENCE_NOTIFICATION_TEMPS_REEL = "En temps réel";
    const FREQUENCE_NOTIFICATION_QUOTIDIEN = "Quotidien";
    const FREQUENCE_NOTIFICATION_HEBDO = "Hebdomadaire";


    //Genre
    const PERSONNE_GENRE_FEMININ = "F";
    const PERSONNE_GENRE_MASCULIN = "M";
    const PERSONNE_GENRE_AUTRE = "-";


    //Rôles dans ROADS
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_ANIMATION = 'ROLE_ANIMATION';
    const ROLE_SUPPORT = 'ROLE_SUPPORT';

    const ROLE_VALIDATION_INTERNATIONALE = 'ROLE_VALIDATION_INTERNATIONALE';
    const LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE = "Validation des séjours internationaux";

    const ROLE_VALIDATION_NATIONALE = 'ROLE_VALIDATION_NATIONALE';
    const LABEL_ROLE_METIER_VALIDATION_NATIONALE = "Validation des séjours nationaux";

    const ROLE_SUIVI_GLOBAL = 'ROLE_SUIVI_GLOBAL';
    const LABEL_ROLE_METIER_SUIVI_GLOBAL = "Suivi global";

    const LABEL_ROLE_METIER_ORGANISATION_SEJOUR = 'Organisation de séjour';
    const LABEL_ROLE_METIER_VALIDATION_SEJOUR = 'Validation de séjour';
    const LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR = 'Validation déléguée d\'un séjour';
    const LABEL_ROLE_METIER_DIRECTION_SEJOUR = 'Direction de séjour';
    const LABEL_ROLE_METIER_DIRECTION_ADJOINTE_SEJOUR = 'Direction adjointe de séjour';
    const LABEL_ROLE_METIER_PARTICIPATION_SEJOUR = 'Participation au séjour';
    const LABEL_ROLE_METIER_SUIVI_SEJOUR = 'Suivi de séjour';
    const LABEL_ROLE_METIER_VISITE_SEJOUR = 'Visite de séjour';

    const LISTE_ROLES_METIERS_MODIFIABLES_PAR_ADMIN = [
        self::LABEL_ROLE_METIER_ORGANISATION_SEJOUR,
        self::LABEL_ROLE_METIER_SUIVI_SEJOUR,
        self::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE,
        self::LABEL_ROLE_METIER_VALIDATION_SEJOUR,
        self::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR,
        self::LABEL_ROLE_METIER_VALIDATION_NATIONALE
    ];

    const PERIMETRE_ROLE_METIER_STRUCTURE = "Structure";
    const PERIMETRE_ROLE_METIER_SEJOUR = "Séjour";
    const PERIMETRE_ROLE_METIER_GLOBAL = "Global";


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Uuid $uuid = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $password;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDerniereConnexion = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(length: 100)]
    #[Groups(["recherche_auto_complete", "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?string $nom = null;

    #[ORM\Column(length: 100)]
    #[Groups(["recherche_auto_complete", "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?string $prenom = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\OneToOne(inversedBy: 'personne', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?Coordonnees $coordonnees = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'personnesDontEstResponsableLegal1')]
    #[Groups(["fin_sejour"])]
    private ?self $responsableLegal1 = null;

    #[ORM\OneToMany(mappedBy: 'responsableLegal1', targetEntity: self::class)]
    private Collection $personnesDontEstResponsableLegal1;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'personnesDontEstResponsableLegal2')]
    #[Groups(["fin_sejour"])]
    private ?self $responsableLegal2 = null;

    #[ORM\OneToMany(mappedBy: 'responsableLegal2', targetEntity: self::class)]
    private Collection $personnesDontEstResponsableLegal2;

    #[ORM\Column(length: 20)]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?string $genre = null;

    #[ORM\Column(length: 254)]
    #[Groups(["infos_personne", "fin_sejour"])]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $adresseMail = null;

    #[ORM\Column(length: 200, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $nomUrgence = null;


    #[ORM\Column(length: 200, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $prenomUrgence = null;


    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    #[Groups(["fin_sejour"])]
    private $telUrgence = null;


    #[ORM\Column(length: 200, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $lienUrgence = null;


    #[ORM\OneToOne(mappedBy: 'personne', cascade: ['persist', 'remove'])]
    #[Groups(["recherche_auto_complete", "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?Adherent $adherent = null;

    #[ORM\OneToOne(mappedBy: 'personne', cascade: ['persist', 'remove'])]
    #[Groups(["recherche_auto_complete", "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?Employe $employe = null;

    #[ORM\Column(length: 50)]
    private ?string $frequenceNotification = self::FREQUENCE_NOTIFICATION_QUOTIDIEN;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: Notification::class, orphanRemoval: true)]
    private Collection $notifications;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: Commentaire::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $commentaires;

    #[ORM\OneToMany(mappedBy: 'nommePar', targetEntity: NominationVisiteSejour::class, orphanRemoval: true)]
    private Collection $attributionsVisitesSejour;

    #[ORM\OneToMany(mappedBy: 'visiteur', targetEntity: NominationVisiteSejour::class, orphanRemoval: true)]
    private Collection $nominationsVisiteSejour;

    #[ORM\ManyToMany(mappedBy: "participantsAdherents", targetEntity: ParticipationSejourUnite::class)]
    private Collection $participationsSejours;

    #[ORM\OneToMany(mappedBy: 'encadrantEEDF', targetEntity: RoleEncadrantSejour::class, orphanRemoval: true)]
    private Collection $rolesSejours;

    #[ORM\OneToMany(mappedBy: 'directeur', targetEntity: Sejour::class)]
    #[ORM\JoinTable(name: "sejour_direction")]
    private Collection $sejoursDirection;

    #[ORM\OneToMany(mappedBy: 'accompagnant', targetEntity: Sejour::class)]
    private Collection $sejoursAccompagnement;

    #[ORM\ManyToMany(mappedBy: "validateurs", targetEntity: Sejour::class)]
    private Collection  $sejoursValidation;

    #[ORM\ManyToMany(mappedBy: "encadrants", targetEntity: Sejour::class)]
    #[ORM\JoinTable(name: "sejour_encadrement")]
    private Collection $sejoursEncadrement;

    #[ORM\ManyToMany(mappedBy: "directeursAdjoints", targetEntity: Sejour::class)]
    #[ORM\JoinTable(name: "sejour_direction_adjointe")]
    private Collection $sejoursDirectionAdjointe;


    #[ORM\ManyToMany(mappedBy: "suiveurs", targetEntity: Sejour::class)]
    #[ORM\JoinTable(name: "sejour_suivi")]
    private Collection $sejoursSuivis;


    #[ORM\ManyToMany(mappedBy: "organisateurs", targetEntity: Structure::class)]
    #[ORM\JoinTable(name: "structure_organisation")]
    private Collection $structuresOrganisation;

    #[ORM\ManyToMany(mappedBy: "validateurs", targetEntity: Structure::class)]
    #[ORM\JoinTable(name: "structure_validation")]
    private Collection $structuresValidation;

    #[ORM\ManyToMany(mappedBy: "suiveurs", targetEntity: Structure::class)]
    #[ORM\JoinTable(name: "structure_suivi")]
    private Collection $structuresSuivi;

    #[ORM\OneToMany(mappedBy: 'personne', cascade: ['persist', 'remove'], targetEntity: Fonction::class, orphanRemoval: true)]
    private Collection $fonctions;

    #[ORM\Column]
    private ?bool $droitImage = null;

    public function __construct()
    {
        $this->personnesDontEstResponsableLegal1 = new ArrayCollection();
        $this->personnesDontEstResponsableLegal2 = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->nominationsVisiteSejour = new ArrayCollection();
        $this->attributionsVisitesSejour = new ArrayCollection();
        $this->participationsSejours = new ArrayCollection();
        $this->rolesSejours = new ArrayCollection();
        $this->sejoursDirection = new ArrayCollection();
        $this->sejoursDirectionAdjointe = new ArrayCollection();
        $this->sejoursEncadrement = new ArrayCollection();
        $this->sejoursSuivis = new ArrayCollection();
        $this->sejoursAccompagnement = new ArrayCollection();
        $this->sejoursValidation = new ArrayCollection();

        $this->structuresOrganisation = new ArrayCollection();
        $this->structuresValidation = new ArrayCollection();
        $this->structuresSuivi = new ArrayCollection();
        $this->fonctions = new ArrayCollection();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function removeRole($role): array
    {
        if (($key = array_search($role, $this->roles)) !== false) {
            unset($this->roles[$key]);
        }
        return $this->roles;
    }

    public function addRole($role): self
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function hasRole($role): bool
    {
        return in_array($role, $this->roles);
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->adresseMail;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->adresseMail;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDerniereConnexion(): ?\DateTimeInterface
    {
        return $this->dateDerniereConnexion;
    }

    public function setDateDerniereConnexion(?\DateTimeInterface $dateDerniereConnexion): self
    {
        $this->dateDerniereConnexion = $dateDerniereConnexion;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getResponsableLegal1(): ?self
    {
        return $this->responsableLegal1;
    }

    public function setResponsableLegal1(?self $responsableLegal1): self
    {
        $this->responsableLegal1 = $responsableLegal1;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPersonnesDontEstResponsableLegal1(): Collection
    {
        return $this->personnesDontEstResponsableLegal1;
    }

    public function addPersonnesDontEstResponsableLegal1(self $personneDontEstResponsableLegal1): self
    {
        if (!$this->personnesDontEstResponsableLegal1->contains($personneDontEstResponsableLegal1)) {
            $this->personnesDontEstResponsableLegal1[] = $personneDontEstResponsableLegal1;
            $personneDontEstResponsableLegal1->setResponsableLegal1($this);
        }

        return $this;
    }

    public function removePersonnesDontEstResponsableLegal1(self $personneDontEstResponsableLegal1): self
    {
        if ($this->personnesDontEstResponsableLegal1->removeElement($personneDontEstResponsableLegal1)) {
            // set the owning side to null (unless already changed)
            if ($personneDontEstResponsableLegal1->getResponsableLegal1() === $this) {
                $personneDontEstResponsableLegal1->setResponsableLegal1(null);
            }
        }

        return $this;
    }

    public function getResponsableLegal2(): ?self
    {
        return $this->responsableLegal2;
    }

    public function setResponsableLegal2(?self $responsableLegal2): self
    {
        $this->responsableLegal2 = $responsableLegal2;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPersonnesDontEstResponsableLegal2(): Collection
    {
        return $this->personnesDontEstResponsableLegal2;
    }

    public function addPersonnesDontEstResponsableLegal2(self $personnesDontEstResponsableLegal2): self
    {
        if (!$this->personnesDontEstResponsableLegal2->contains($personnesDontEstResponsableLegal2)) {
            $this->personnesDontEstResponsableLegal2[] = $personnesDontEstResponsableLegal2;
            $personnesDontEstResponsableLegal2->setResponsableLegal2($this);
        }

        return $this;
    }

    public function removePersonnesDontEstResponsableLegal2(self $personnesDontEstResponsableLegal2): self
    {
        if ($this->personnesDontEstResponsableLegal2->removeElement($personnesDontEstResponsableLegal2)) {
            // set the owning side to null (unless already changed)
            if ($personnesDontEstResponsableLegal2->getResponsableLegal2() === $this) {
                $personnesDontEstResponsableLegal2->setResponsableLegal2(null);
            }
        }

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getNomUrgence(): ?string
    {
        return $this->nomUrgence;
    }

    public function setNomUrgence(?string $nomUrgence): self
    {
        $this->nomUrgence = $nomUrgence;

        return $this;
    }

    public function getPrenomUrgence(): ?string
    {
        return $this->prenomUrgence;
    }

    public function setPrenomUrgence(?string $prenomUrgence): self
    {
        $this->prenomUrgence = $prenomUrgence;

        return $this;
    }

    public function getTelUrgence()
    {
        return $this->telUrgence;
    }

    public function setTelUrgence($telUrgence): self
    {
        $this->telUrgence = $telUrgence;

        return $this;
    }

    public function getLienUrgence(): ?string
    {
        return $this->lienUrgence;
    }

    public function setLienUrgence(?string $lienUrgence): self
    {
        $this->lienUrgence = $lienUrgence;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(Adherent $adherent): self
    {
        // set the owning side of the relation if necessary
        if ($adherent->getPersonne() !== $this) {
            $adherent->setPersonne($this);
        }

        $this->adherent = $adherent;

        return $this;
    }

    public function getEmploye(): ?Employe
    {
        return $this->employe;
    }

    public function setEmploye(Employe $employe): self
    {
        // set the owning side of the relation if necessary
        if ($employe->getPersonne() !== $this) {
            $employe->setPersonne($this);
        }

        $this->employe = $employe;

        return $this;
    }

    public function getFrequenceNotification(): ?string
    {
        return $this->frequenceNotification;
    }



    public function setFrequenceNotification(string $frequenceNotification): self
    {
        $this->frequenceNotification = $frequenceNotification;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }


    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setPersonne($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getPersonne() === $this) {
                $notification->setPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasNotificationsNonLues(): bool
    {
        foreach ($this->notifications as $notification) {
            /** @var Notification $notification */
            if (!$notification->getLu()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getNombreNotificationsNonLues(): int
    {
        $result = 0;
        foreach ($this->notifications as $notification) {
            /** @var Notification $notification */
            if (!$notification->getLu()) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setPersonne($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getPersonne() === $this) {
                $commentaire->setPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NominationVisiteSejour>
     */
    public function getAttributionsVisiteSejour(): Collection
    {
        return $this->attributionsVisitesSejour;
    }

    public function addAttributionsVisiteSejour(NominationVisiteSejour $attributionVisiteSejour): self
    {
        if (!$this->attributionsVisitesSejour->contains($attributionVisiteSejour)) {
            $this->attributionsVisitesSejour[] = $attributionVisiteSejour;
            $attributionVisiteSejour->setNommePar($this);
        }

        return $this;
    }

    public function removeAttributionsVisiteSejour(NominationVisiteSejour $attributionVisiteSejour): self
    {
        if ($this->attributionsVisitesSejour->removeElement($attributionVisiteSejour)) {
            // set the owning side to null (unless already changed)
            if ($attributionVisiteSejour->getNommePar() === $this) {
                $attributionVisiteSejour->setNommePar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NominationVisiteSejour>
     */
    public function getNominationsVisiteSejour(): Collection
    {
        return $this->nominationsVisiteSejour;
    }

    public function addNominationsVisiteSejour(NominationVisiteSejour $nominationsVisiteSejour): self
    {
        if (!$this->nominationsVisiteSejour->contains($nominationsVisiteSejour)) {
            $this->nominationsVisiteSejour[] = $nominationsVisiteSejour;
            $nominationsVisiteSejour->setVisiteur($this);
        }

        return $this;
    }

    public function removeNominationsVisiteSejour(NominationVisiteSejour $nominationsVisiteSejour): self
    {
        if ($this->nominationsVisiteSejour->removeElement($nominationsVisiteSejour)) {
            // set the owning side to null (unless already changed)
            if ($nominationsVisiteSejour->getVisiteur() === $this) {
                $nominationsVisiteSejour->setVisiteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ParticipantSejourUnite[]
     */
    public function getParticipationSejour(): Collection
    {
        return $this->participationsSejours;
    }

    /**
     * @return Collection|ParticipationSejourUnite[]
     */
    public function getParticipationsSejours(): Collection
    {
        return $this->participationsSejours;
    }

    public function addParticipationsSejour(ParticipationSejourUnite $participationsSejour): self
    {
        if (!$this->participationsSejours->contains($participationsSejour)) {
            $this->participationsSejours[] = $participationsSejour;
            $participationsSejour->addParticipantsAdherent($this);
        }

        return $this;
    }

    public function removeParticipationsSejour(ParticipationSejourUnite $participationsSejour): self
    {
        if ($this->participationsSejours->removeElement($participationsSejour)) {
            $participationsSejour->removeParticipantsAdherent($this);
        }

        return $this;
    }

    /**
     * @return Collection|RoleEncadrantSejour[]
     */
    public function getRolesSejours(): Collection
    {
        return $this->rolesSejours;
    }

    public function addRolesSejour(RoleEncadrantSejour $rolesSejour): self
    {
        if (!$this->rolesSejours->contains($rolesSejour)) {
            $this->rolesSejours[] = $rolesSejour;
            $rolesSejour->setEncadrantEEDF($this);
        }

        return $this;
    }

    public function removeRolesSejour(RoleEncadrantSejour $rolesSejour): self
    {
        if ($this->rolesSejours->removeElement($rolesSejour)) {
            // set the owning side to null (unless already changed)
            if ($rolesSejour->getEncadrantEEDF() === $this) {
                $rolesSejour->setEncadrantEEDF(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursDirection(): Collection
    {
        return $this->sejoursDirection;
    }

    public function addSejoursDirection(Sejour $sejoursDirection): self
    {
        if (!$this->sejoursDirection->contains($sejoursDirection)) {
            $this->sejoursDirection[] = $sejoursDirection;
            $sejoursDirection->setDirecteur($this);
        }

        return $this;
    }
    public function removeSejoursDirection(Sejour $sejoursDirection): self
    {
        if ($this->sejoursDirection->removeElement($sejoursDirection)) {
            // set the owning side to null (unless already changed)
            if ($sejoursDirection->getDirecteur() === $this) {
                $sejoursDirection->setDirecteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursDirectionAdjointe(): Collection
    {
        return $this->sejoursDirectionAdjointe;
    }

    public function addSejoursDirectionAdjointe(Sejour $sejoursDirectionAdjointe): self
    {
        if (!$this->sejoursDirectionAdjointe->contains($sejoursDirectionAdjointe)) {
            $this->sejoursDirectionAdjointe[] = $sejoursDirectionAdjointe;
            $sejoursDirectionAdjointe->addDirecteursAdjoint($this);
        }

        return $this;
    }

    public function removeSejoursDirectionAdjointe(Sejour $sejoursDirectionAdjointe): self
    {
        if ($this->sejoursDirectionAdjointe->removeElement($sejoursDirectionAdjointe)) {
            $sejoursDirectionAdjointe->removeDirecteursAdjoint($this);
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursEncadrement(): Collection
    {
        return $this->sejoursEncadrement;
    }

    public function addSejoursEncadrement(Sejour $sejoursEncadrement): self
    {
        if (!$this->sejoursEncadrement->contains($sejoursEncadrement)) {
            $this->sejoursEncadrement[] = $sejoursEncadrement;
            $sejoursEncadrement->addEncadrant($this);
        }

        return $this;
    }

    public function removeSejoursEncadrement(Sejour $sejoursEncadrement): self
    {
        if ($this->sejoursEncadrement->removeElement($sejoursEncadrement)) {
            $sejoursEncadrement->removeEncadrant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursSuivis(): Collection
    {
        return $this->sejoursSuivis;
    }

    public function addSejoursSuivi(Sejour $sejoursSuivi): self
    {
        if (!$this->sejoursSuivis->contains($sejoursSuivi)) {
            $this->sejoursSuivis[] = $sejoursSuivi;
            $sejoursSuivi->addSuiveur($this);
        }

        return $this;
    }

    public function removeSejoursSuivi(Sejour $sejoursSuivi): self
    {
        if ($this->sejoursSuivis->removeElement($sejoursSuivi)) {
            $sejoursSuivi->removeSuiveur($this);
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursAccompagnement(): Collection
    {
        return $this->sejoursAccompagnement;
    }

    public function addSejoursAccompagnement(Sejour $sejoursAccompagnement): self
    {
        if (!$this->sejoursAccompagnement->contains($sejoursAccompagnement)) {
            $this->sejoursAccompagnement[] = $sejoursAccompagnement;
            $sejoursAccompagnement->setAccompagnant($this);
        }

        return $this;
    }

    public function removeSejoursAccompagnement(Sejour $sejoursAccompagnement): self
    {
        if ($this->sejoursAccompagnement->removeElement($sejoursAccompagnement)) {
            // set the owning side to null (unless already changed)
            if ($sejoursAccompagnement->getAccompagnant() === $this) {
                $sejoursAccompagnement->setAccompagnant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursValidation(): Collection
    {
        return $this->sejoursValidation;
    }

    public function addSejoursValidation(Sejour $sejoursValidation): self
    {
        if (!$this->sejoursValidation->contains($sejoursValidation)) {
            $this->sejoursValidation[] = $sejoursValidation;
            $sejoursValidation->addValidateur($this);
        }

        return $this;
    }

    public function removeSejoursValidation(Sejour $sejoursValidation): self
    {
        if ($this->sejoursValidation->removeElement($sejoursValidation)) {
            $sejoursValidation->removeValidateur($this);
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getStructuresOrganisation(): Collection
    {
        return $this->structuresOrganisation;
    }

    public function addStructuresOrganisation(Structure $structuresOrganisation): self
    {
        if (!$this->structuresOrganisation->contains($structuresOrganisation)) {
            $this->structuresOrganisation[] = $structuresOrganisation;
            $structuresOrganisation->addOrganisateur($this);
        }

        return $this;
    }

    public function removeStructuresOrganisation(Structure $structuresOrganisation): self
    {
        if ($this->structuresOrganisation->removeElement($structuresOrganisation)) {
            $structuresOrganisation->removeOrganisateur($this);
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getStructuresValidation(): Collection
    {
        return $this->structuresValidation;
    }

    public function addStructuresValidation(Structure $structuresValidation): self
    {
        if (!$this->structuresValidation->contains($structuresValidation)) {
            $this->structuresValidation[] = $structuresValidation;
            $structuresValidation->addValidateur($this);
        }

        return $this;
    }

    public function removeStructuresValidation(Structure $structuresValidation): self
    {
        if ($this->structuresValidation->removeElement($structuresValidation)) {
            $structuresValidation->removeValidateur($this);
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getStructuresSuivi(): Collection
    {
        return $this->structuresSuivi;
    }

    public function addStructuresSuivi(Structure $structuresSuivi): self
    {
        if (!$this->structuresSuivi->contains($structuresSuivi)) {
            $this->structuresSuivi[] = $structuresSuivi;
            $structuresSuivi->addSuiveur($this);
        }

        return $this;
    }

    public function removeStructuresSuivi(Structure $structuresSuivi): self
    {
        if ($this->structuresSuivi->removeElement($structuresSuivi)) {
            $structuresSuivi->removeSuiveur($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Fonction>
     */
    public function getFonctions(): Collection
    {
        return $this->fonctions;
    }

    public function addFonction(Fonction $fonction): self
    {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions[] = $fonction;
            $fonction->setPersonne($this);
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): self
    {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getPersonne() === $this) {
                $fonction->setPersonne(null);
            }
        }

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(Coordonnees $coordonnees): self
    {
        $this->coordonnees = $coordonnees;

        return $this;
    }

    public function getAdresseMail(): ?string
    {
        return $this->adresseMail;
    }

    public function getAdresseMailUsage(): ?string
    {
        if ($this->estSalarieValide()) {
            if ($this->getEmploye()->getEmail() !== null) {
                return $this->getEmploye()->getEmail();
            }
        }
        return $this->adresseMail;
    }

    public function getTelephoneUsage(): ?PhoneNumber
    {
        if ($this->estSalarieValide()) {
            if ($this->getEmploye()->getTelephone() !== null) {
                return $this->getEmploye()->getTelephone();
            }
        }
        return $this->getCoordonnees()->getTelMobile();
    }

    public function setAdresseMail(string $adresseMail): self
    {
        $this->adresseMail = $adresseMail;

        return $this;
    }

    public function estAdherentValide(): bool
    {
        if ($this->getAdherent()) {
            if (
                $this->getAdherent()->getStatut() == Adherent::STATUT_ADHERENT_OK
                && $this->getAdherent()->getAdhesionCouranteActive() !== null
            ) {
                return true;
            }
        }

        return false;
    }

    public function getStatutAdherent(): String
    {
        if ($this->getAdherent()) {
            if (
                $this->getAdherent()->getStatut() == Adherent::STATUT_ADHERENT_OK
                && $this->getAdherent()->getAdhesionCouranteActive() !== null
            ) {
                return Adherent::ADHERENT;
            } else {
                return Adherent::ANCIEN_ADHERENT;
            }
        } else {
            return Adherent::NON_ADHERENT;
        }
    }


    public function estSalarieValide(?DateTime $date = null): bool
    {
        if ($this->getEmploye()) {
            return $this->getEmploye()->hasContratActif($date);
        }

        return false;
    }

    public function getDroitImage(): ?bool
    {
        return $this->droitImage;
    }

    public function setDroitImage(bool $droitImage): self
    {
        $this->droitImage = $droitImage;

        return $this;
    }

    public function getNumeroAdherentOuMatricule(): ?String
    {
        $estSalarie = false;
        $numeroAdherentOuMatriculeSalarie = "";
        if ($this->getEmploye() && $this->getEmploye()->hasContratActif()) {
            $numeroAdherentOuMatriculeSalarie = $this->getEmploye()->getNumeroSalarie();
            $estSalarie = true;
        }

        if ($this->getAdherent()) {
            if ($estSalarie) {
                $numeroAdherentOuMatriculeSalarie .= " - ";
            }
            $numeroAdherentOuMatriculeSalarie .= $this->getAdherent()->getNumeroAdherent();
        }
        return $numeroAdherentOuMatriculeSalarie;
    }

    /**
     * Get the hashed password
     *
     * @return  string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the hashed password
     *
     * @param  string  $password  The hashed password
     *
     * @return  self
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    public function getAffichagePourRechercheAuto(): String
    {
        $estAdherent = false;
        $result = $this->getPrenom() . " " . $this->getNom() . " (";
        if ($this->getStatutAdherent() != Adherent::NON_ADHERENT) {
            $result .= $this->getStatutAdherent() . " n°" . $this->getAdherent()->getNumeroAdherent();
            $estAdherent = true;
        }
        if ($this->estSalarieValide()) {
            if ($estAdherent) {
                $result .= " - ";
            }
            $result .= "Salarié·e n°" . $this->getEmploye()->getNumeroSalarie();
        }
        $result .= ")";
        return $result;
    }

    public function fusionDoublon(Personne $doublon): void
    {

        foreach ($doublon->getNotifications() as $notification) {
            $notification->setPersonne($this);
        }
        foreach ($doublon->getCommentaires() as $commentaire) {
            $commentaire->setPersonne($this);
        }
        foreach ($doublon->getNominationsVisiteSejour() as $nominationVisiteSejour) {
            $nominationVisiteSejour->setVisiteur($this);
        }
        foreach ($doublon->getAttributionsVisiteSejour() as $attributionVisiteSejour) {
            $attributionVisiteSejour->setNommePar($this);
        }
        foreach ($doublon->getParticipationsSejours() as $participationSejour) {
            $participationSejour->removeParticipantsAdherent($doublon);
            $participationSejour->addParticipantsAdherent($this);
        }
        foreach ($doublon->getRolesSejours() as $roleEncadrant) {
            $roleEncadrant->setEncadrantEEDF($this);
        }
        foreach ($doublon->getSejoursDirection() as $sejourDirection) {
            $sejourDirection->setDirecteur($this);
        }
        foreach ($doublon->getSejoursAccompagnement() as $sejourAccompagnement) {
            $sejourAccompagnement->setAccompagnant($this);
        }
        foreach ($doublon->getSejoursValidation() as $sejourValidation) {
            $sejourValidation->removeValidateur($doublon);
            $sejourValidation->addValidateur($this);
        }
        foreach ($doublon->getSejoursEncadrement() as $sejourEncadrement) {
            $sejourEncadrement->removeEncadrant($doublon);
            $sejourEncadrement->addEncadrant($this);
        }

        foreach ($doublon->getSejoursDirectionAdjointe() as $sejourDirectionAdjointe) {
            $sejourDirectionAdjointe->removeDirecteursAdjoint($doublon);
            $sejourDirectionAdjointe->addDirecteursAdjoint($this);
        }

        foreach ($doublon->getSejoursSuivis() as $sejourSuivi) {
            $sejourSuivi->removeSuiveur($doublon);
            $sejourSuivi->addSuiveur($this);
        }
        foreach ($doublon->getStructuresOrganisation() as $structureOrganisation) {
            $structureOrganisation->removeOrganisateur($doublon);
            $structureOrganisation->addOrganisateur($this);
        }

        foreach ($doublon->getStructuresValidation() as $structureValidation) {
            $structureValidation->removeValidateur($doublon);
            $structureValidation->addValidateur($this);
        }

        foreach ($doublon->getStructuresSuivi() as $structureSuivi) {
            $structureSuivi->removeSuiveur($doublon);
            $structureSuivi->addSuiveur($this);
        }
    }
}
