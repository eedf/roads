<?php

namespace App\Entity;

use App\Repository\EvenementGraveRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Evenement grave d'un séjour (clôture de séjour)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: EvenementGraveRepository::class)]
class EvenementGrave
{
    const NATURE_METEO = "Météo (orages ou canicules) dont évacuation";
    const NATURE_MEMBRE_EQUIPE_SANS_RENVOI = "Problème grave avec un ou plusieurs membres de l'équipe - sans renvoi";
    const NATURE_MEMBRE_EQUIPE_AVEC_RENVOI = "Renvoi d'un adulte membre de l'équipe d'animation ou d'organisation";
    const NATURE_ACCIDENT_SANS_RENVOI = "Accident - sans départ anticipé du séjour";
    const NATURE_ACCIDENT_AVEC_RENVOI = "Départ anticipé suite à un accident";
    const NATURE_SANTE_SANS_RENVOI = "Santé maladie - sans départ anticipé du séjour";
    const NATURE_SANTE_AVEC_RENVOI = "Départ anticipé pour raison de santé";
    const NATURE_PARTICIPANT_SANS_RENVOI = "Problème de comportement d'un·e participant·e - sans renvoi ou départ anticipé du séjour";
    const NATURE_PARTICIPANT_AVEC_RENVOI = "Exclusion ou départ anticipé d'un·e participant·e suite à un problème";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $natureEvenement = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $precisions = null;

    #[ORM\ManyToOne(inversedBy: 'descriptionEvenementsGraves')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RetourSejour $retourSejour = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNatureEvenement(): ?string
    {
        return $this->natureEvenement;
    }

    public function setNatureEvenement(string $natureEvenement): self
    {
        $this->natureEvenement = $natureEvenement;

        return $this;
    }

    public function getPrecisions(): ?string
    {
        return $this->precisions;
    }

    public function setPrecisions(?string $precisions): self
    {
        $this->precisions = $precisions;

        return $this;
    }

    public function getRetourSejour(): ?RetourSejour
    {
        return $this->retourSejour;
    }

    public function setRetourSejour(?RetourSejour $retourSejour): self
    {
        $this->retourSejour = $retourSejour;

        return $this;
    }
}
