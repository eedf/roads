<?php

namespace App\Entity;

use App\Repository\StructureRegroupementSejourRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Participation d'une structure à un séjour regroupé
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: StructureRegroupementSejourRepository::class)]
#[Assert\Callback(
    callback: "validateNombreUnites",
    groups: ["construction_sejour", "cloture_sejour"]
)]
class StructureRegroupementSejour
{
    const STATUT_DEMANDE_EN_ATTENTE = "En attente";
    const STATUT_DEMANDE_ACCEPTE = "Acceptée";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column()]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $estEEDF = null;

    #[ORM\ManyToOne(inversedBy: 'structuresRegroupementSejour')]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Structure $structureEEDF = null;


    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nomStructureNonEEDF = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $descriptionStructureNonEEDF = null;

    #[ORM\ManyToOne(inversedBy: 'structuresRegroupees')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Sejour $sejour = null;

    #[ORM\Column(length: 20)]
    #[Groups(["fin_sejour"])]
    private ?string $statutDemande = self::STATUT_DEMANDE_EN_ATTENTE;

    #[ORM\ManyToMany(targetEntity: Equipe::class, inversedBy: "structureRegroupementSejours")]
    private Collection $unites;

    #[ORM\OneToMany(mappedBy: 'structureRegroupementSejour', targetEntity: ParticipationSejourUnite::class, cascade: ["persist", "remove"])]
    #[Groups(["fin_sejour"])]
    #[Assert\Valid()]
    private Collection $unitesParticipantes;


    #[ORM\Column(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?int $nbEnfantMoins14StructNonEEDF;


    #[Groups(["fin_sejour"])]
    #[ORM\Column(nullable: true)]
    private ?int $nbEnfantsPlus14StructNonEEDF;

    #[ORM\OneToMany(mappedBy: 'structureNonEEDF', targetEntity: RoleEncadrantSejour::class, cascade: ["persist", "remove"])]
    #[Groups(["fin_sejour"])]
    #[Assert\Valid()]
    private Collection $encadrants;

    public function __construct()
    {
        $this->unites = new ArrayCollection();
        $this->unitesParticipantes = new ArrayCollection();
        $this->encadrants = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEstEEDF(): ?bool
    {
        return $this->estEEDF;
    }

    public function setEstEEDF(bool $estEEDF): self
    {
        $this->estEEDF = $estEEDF;

        return $this;
    }

    public function getStructureEEDF(): ?Structure
    {
        return $this->structureEEDF;
    }

    public function setStructureEEDF(?Structure $structureEEDF): self
    {
        $this->structureEEDF = $structureEEDF;

        return $this;
    }

    public function getNomStructureNonEEDF(): ?string
    {
        return $this->nomStructureNonEEDF;
    }

    public function setNomStructureNonEEDF(?string $nomStructureNonEEDF): self
    {
        $this->nomStructureNonEEDF = $nomStructureNonEEDF;

        return $this;
    }

    public function getDescriptionStructureNonEEDF(): ?string
    {
        return $this->descriptionStructureNonEEDF;
    }

    public function setDescriptionStructureNonEEDF(?string $descriptionStructureNonEEDF): self
    {
        $this->descriptionStructureNonEEDF = $descriptionStructureNonEEDF;

        return $this;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(?Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getStatutDemande(): ?string
    {
        return $this->statutDemande;
    }

    public function setStatutDemande(string $statutDemande): self
    {
        $this->statutDemande = $statutDemande;

        return $this;
    }

    /**
     * @return Collection|Equipe[]
     */
    public function getUnites(): Collection
    {
        return $this->unites;
    }

    public function addUnite(Equipe $unite): self
    {
        if (!$this->unites->contains($unite)) {
            $this->unites[] = $unite;
        }

        return $this;
    }

    public function removeUnite(Equipe $unite): self
    {
        $this->unites->removeElement($unite);

        return $this;
    }

    /**
     * @return Collection|ParticipationSejourUnite[]
     */
    public function getUnitesParticipantes(): Collection
    {
        return $this->unitesParticipantes;
    }

    public function addUnitesParticipante(ParticipationSejourUnite $unitesParticipante): self
    {
        if (!$this->unitesParticipantes->contains($unitesParticipante)) {
            $this->unitesParticipantes[] = $unitesParticipante;
            $unitesParticipante->setStructureRegroupementSejour($this);
        }

        return $this;
    }

    public function removeUnitesParticipante(ParticipationSejourUnite $unitesParticipante): self
    {
        if ($this->unitesParticipantes->removeElement($unitesParticipante)) {
            // set the owning side to null (unless already changed)
            if ($unitesParticipante->getStructureRegroupementSejour() === $this) {
                $unitesParticipante->setStructureRegroupementSejour(null);
            }
        }

        return $this;
    }

    public function getNbEnfantMoins14StructNonEEDF(): ?int
    {
        return $this->nbEnfantMoins14StructNonEEDF;
    }

    public function setNbEnfantMoins14StructNonEEDF(?int $nbEnfantMoins14StructNonEEDF): self
    {
        $this->nbEnfantMoins14StructNonEEDF = $nbEnfantMoins14StructNonEEDF;

        return $this;
    }

    public function getNbEnfantsPlus14StructNonEEDF(): ?int
    {
        return $this->nbEnfantsPlus14StructNonEEDF;
    }

    public function setNbEnfantsPlus14StructNonEEDF(?int $nbEnfantsPlus14StructNonEEDF): self
    {
        $this->nbEnfantsPlus14StructNonEEDF = $nbEnfantsPlus14StructNonEEDF;

        return $this;
    }

    /**
     * @return Collection|RoleEncadrantSejour[]
     */
    public function getEncadrants(): Collection
    {
        return $this->encadrants;
    }

    public function addEncadrant(RoleEncadrantSejour $encadrant): self
    {
        if (!$this->encadrants->contains($encadrant)) {
            $this->encadrants[] = $encadrant;
            $encadrant->setStructureNonEEDF($this);
        }

        return $this;
    }

    public function removeEncadrant(RoleEncadrantSejour $encadrant): self
    {
        if ($this->encadrants->removeElement($encadrant)) {
            // set the owning side to null (unless already changed)
            if ($encadrant->getStructureNonEEDF() === $this) {
                $encadrant->setStructureNonEEDF(null);
            }
        }

        return $this;
    }
    public function validateNombreUnites(ExecutionContextInterface $context, $payload)
    {
        if ($this->getEstEEDF() && $this->unitesParticipantes->count() == 0) {
            $context->buildViolation("Au moins une unité doit être séléctionnée pour la structure regroupée " . $this->getStructureEEDF()->getNom())
                ->addViolation();
        }
    }
}
