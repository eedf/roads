<?php

namespace App\Entity;

use App\Repository\AdherentRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AdherentRepository::class)]
class Adherent
{

    const STATUT_ADHERENT_OK = 1;
    const STATUT_ADHERENT_PARTI = 2;
    const STATUT_ADHERENT_SUSPENDU = 3;
    const STATUT_ADHERENT_EXCLU = 4;

    const ADHERENT = "Adhérent·e";
    const ANCIEN_ADHERENT = "Ancien·ne adhérent·e";
    const NON_ADHERENT = "Non adhérent·e";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(unique: true)]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(0)]
    #[Assert\LessThanOrEqual(999999)]
    #[Groups(['recherche_auto_complete', "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?int $numeroAdherent = null;


    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\OneToOne(inversedBy: 'adherent', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['recherche_auto_complete', "infos_personne"])]
    private ?string $statut = null;

    #[ORM\OneToMany(mappedBy: 'adherent', targetEntity: Adhesion::class, orphanRemoval: true, fetch: "EAGER")]
    #[Groups(['recherche_auto_complete', "infos_personne"])]
    private Collection $adhesions;

    public function __construct()
    {
        $this->adhesions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getNumeroAdherent(): ?int
    {
        return $this->numeroAdherent;
    }

    public function setNumeroAdherent(int $numeroAdherent): self
    {
        $this->numeroAdherent = $numeroAdherent;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(Personne $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection<int, Adhesion>
     */
    public function getAdhesions(): Collection
    {
        return $this->adhesions;
    }

    /**
     * @return Adhesion
     */
    public function getAdhesionCouranteActive(Datetime $date = null): ?Adhesion
    {
        if (!$date) {
            $date = new DateTime();
        }
        if ($this->getAdhesions()->count() > 0) {
            $adhesionActives = $this->getAdhesions()->filter(function (Adhesion $adhesion) use ($date) {
                return ($adhesion->estActive($date));
            });

            if ($adhesionActives->count() > 0) {
                return $adhesionActives->first();
            }
        }
        return null;
    }


    public function addAdhesion(Adhesion $adhesion): self
    {
        if (!$this->adhesions->contains($adhesion)) {
            $this->adhesions[] = $adhesion;
            $adhesion->setAdherent($this);
        }

        return $this;
    }

    public function removeAdhesion(Adhesion $adhesion): self
    {
        if ($this->adhesions->removeElement($adhesion)) {
            // set the owning side to null (unless already changed)
            if ($adhesion->getAdherent() === $this) {
                $adhesion->setAdherent(null);
            }
        }

        return $this;
    }
}
