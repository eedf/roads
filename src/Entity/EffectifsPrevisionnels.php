<?php

namespace App\Entity;

use App\Repository\EffectifsPrevisionnelsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Effectifs prévisionnels d'un séjour (déclaration d'intention)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: EffectifsPrevisionnelsRepository::class)]
#[Assert\Expression(
    "(this.getNbLutins() > 0 ) or (this.getNbLouveteaux() > 0 ) or (this.getNbEcles() > 0 ) or (this.getNbAines() > 0 ) or (this.getNbHandicap() > 0 )",
    message: "Le nombre de participant·e·s doit être supérieur à 0"
)]
#[Assert\Expression(
    "(this.getNbEncadrantsLutins() > 0 ) or (this.getNbEncadrantsLouveteaux() > 0 ) or (this.getNbEncadrantsEcles() > 0 ) or (this.getNbEncadrantsAines() > 0 )",
    message: "Le nombre d'encadrant·e·s doit être supérieur à 0"
)]
class EffectifsPrevisionnels
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbLutins = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbLouveteaux = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbEcles = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbAines = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbEncadrantsLutins = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbEncadrantsLouveteaux = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbEncadrantsEcles = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbEncadrantsAines = null;

    #[ORM\Column()]
    #[Assert\PositiveOrZero()]
    #[Groups(["save_declaration_intention"])]
    private ?int $nbHandicap = null;

    #[ORM\OneToOne(mappedBy: 'effectifsPrevisionnels', cascade: ['persist', 'remove'])]
    private ?Sejour $sejour = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbLutins(): ?int
    {
        return $this->nbLutins;
    }

    public function setNbLutins(int $nbLutins): self
    {
        $this->nbLutins = $nbLutins;

        return $this;
    }

    public function getNbLouveteaux(): ?int
    {
        return $this->nbLouveteaux;
    }

    public function setNbLouveteaux(int $nbLouveteaux): self
    {
        $this->nbLouveteaux = $nbLouveteaux;

        return $this;
    }

    public function getNbEcles(): ?int
    {
        return $this->nbEcles;
    }

    public function setNbEcles(int $nbEcles): self
    {
        $this->nbEcles = $nbEcles;

        return $this;
    }

    public function getNbAines(): ?int
    {
        return $this->nbAines;
    }

    public function setNbAines(int $nbAines): self
    {
        $this->nbAines = $nbAines;

        return $this;
    }

    public function getNbEncadrantsLutins(): ?int
    {
        return $this->nbEncadrantsLutins;
    }

    public function setNbEncadrantsLutins(int $nbEncadrantsLutins): self
    {
        $this->nbEncadrantsLutins = $nbEncadrantsLutins;

        return $this;
    }

    public function getNbEncadrantsLouveteaux(): ?int
    {
        return $this->nbEncadrantsLouveteaux;
    }

    public function setNbEncadrantsLouveteaux(int $nbEncadrantsLouveteaux): self
    {
        $this->nbEncadrantsLouveteaux = $nbEncadrantsLouveteaux;

        return $this;
    }

    public function getNbEncadrantsEcles(): ?int
    {
        return $this->nbEncadrantsEcles;
    }

    public function setNbEncadrantsEcles(int $nbEncadrantsEcles): self
    {
        $this->nbEncadrantsEcles = $nbEncadrantsEcles;

        return $this;
    }

    public function getNbEncadrantsAines(): ?int
    {
        return $this->nbEncadrantsAines;
    }

    public function setNbEncadrantsAines(int $nbEncadrantsAines): self
    {
        $this->nbEncadrantsAines = $nbEncadrantsAines;

        return $this;
    }

    public function getNbHandicap(): ?int
    {
        return $this->nbHandicap;
    }

    public function setNbHandicap(int $nbHandicap): self
    {
        $this->nbHandicap = $nbHandicap;

        return $this;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        // set the owning side of the relation if necessary
        if ($sejour->getEffectifsPrevisionnels() !== $this) {
            $sejour->setEffectifsPrevisionnels($this);
        }

        $this->sejour = $sejour;

        return $this;
    }
}
