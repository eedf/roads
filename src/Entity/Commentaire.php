<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: CommentaireRepository::class)]
class Commentaire
{

    //Sujets Déclaration intention
    const SUJET_SEJOUR_DECLARATION_INTENTION_INFOS_GENERALES = "SUJET_SEJOUR_DECLARATION_INTENTION_INFOS_GENERALES";
    const SUJET_SEJOUR_DECLARATION_INTENTION_DIRECTION = "SUJET_SEJOUR_DECLARATION_INTENTION_DIRECTION";
    const SUJET_SEJOUR_DECLARATION_INTENTION_MODALITES = "SUJET_SEJOUR_DECLARATION_INTENTION_MODALITES";
    const SUJET_SEJOUR_DECLARATION_INTENTION_LIEUX = "SUJET_SEJOUR_DECLARATION_INTENTION_LIEUX";
    const SUJET_SEJOUR_DECLARATION_INTENTION_EFFECTIFS_PREVISIONNELS = "SUJET_SEJOUR_DECLARATION_INTENTION_EFFECTIFS_PREVISIONNELS";

    //Projet de séjour
    //Onglet Informations générales
    const SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_INFOS_GENERALES = "SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_INFOS_GENERALES";
    const SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_DIRECTION = "SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_DIRECTION";
    const SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_UNITES_PARTICIPANTES = "SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_UNITES_PARTICIPANTES";
    const SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_MODALITES = "SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_MODALITES";

    //Onglet effectifs
    const SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_RESUME = "SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_RESUME";
    const SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_ENCADREMENT = "SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_ENCADREMENT";
    const SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_PARTICIPANTS = "SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_PARTICIPANTS";

    //Onglet Dates et lieux 
    const SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_DATES = "SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_DATES";
    const SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_LIEUX = "SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_LIEUX";


    //Onglet démarches pédagogiques
    const SUJET_SEJOUR_PROJET_SEJOUR_PEDA_PROJET_PEDA = "SUJET_SEJOUR_PROJET_SEJOUR_PEDA_PROJET_PEDA";
    const SUJET_SEJOUR_PROJET_SEJOUR_PEDA_COLOS_APPRENANTES = "SUJET_SEJOUR_PROJET_SEJOUR_PEDA_COLOS_APPRENANTES";
    const SUJET_SEJOUR_PROJET_SEJOUR_PEDA_INTERNATIONAL = "SUJET_SEJOUR_PROJET_SEJOUR_PEDA_INTERNATIONAL";

    //Onglet logistique
    const SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_BUDGET = "SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_BUDGET";
    const SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_TRANSPORT = "SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_TRANSPORT";
    const SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_ALIMENTAIRE = "SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_ALIMENTAIRE";
    const SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_COMMUNCATION = "SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_COMMUNCATION";
    //Validation de séjour
    const SUJET_SEJOUR_VALIDATION = "SUJET_SEJOUR_VALIDATION";
    //Validation internationale de séjour
    const SUJET_SEJOUR_VALIDATION_INTERNATIONALE = "SUJET_SEJOUR_VALIDATION_INTERNATIONALE";

    //Visites de séjour
    const SUJET_SEJOUR_VISITE = "SUJET_SEJOUR_VISITE";

    //Cloture de séjour
    //Onglet Informations générales
    const SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_INFOS_GENERALES = "SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_INFOS_GENERALES";
    const SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_DIRECTION = "SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_DIRECTION";
    const SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_UNITES_PARTICIPANTES = "SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_UNITES_PARTICIPANTES";
    const SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_MODALITES = "SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_MODALITES";


    //Onglet Retours généraux
    const SUJET_SEJOUR_CLOTURE_RETOURS_GENERAUX = "SUJET_SEJOUR_CLOTURE_RETOURS_GENERAUX";

    //Onglet effectifs
    const SUJET_SEJOUR_CLOTURE_EFFECTIFS_RESUME = "SUJET_SEJOUR_CLOTURE_EFFECTIFS_RESUME";
    const SUJET_SEJOUR_CLOTURE_EFFECTIFS_ENCADREMENT = "SUJET_SEJOUR_CLOTURE_EFFECTIFS_ENCADREMENT";
    const SUJET_SEJOUR_CLOTURE_EFFECTIFS_PARTICIPANTS = "SUJET_SEJOUR_CLOTURE_EFFECTIFS_PARTICIPANTS";

    //Onglet Dates et lieux 
    const SUJET_SEJOUR_CLOTURE_DATES_LIEUX_DATES = "SUJET_SEJOUR_CLOTURE_DATES_LIEUX_DATES";
    const SUJET_SEJOUR_CLOTURE_DATES_LIEUX_LIEUX = "SUJET_SEJOUR_CLOTURE_DATES_LIEUX_LIEUX";

    //Onglet Péda
    const SUJET_SEJOUR_CLOTURE_PEDA = "SUJET_SEJOUR_CLOTURE_PEDA";

    //Onglets Logistique
    const SUJET_SEJOUR_CLOTURE_LOGISTIQUE = "SUJET_SEJOUR_CLOTURE_LOGISTIQUE";

    const LISTE_SUJETS_COMMENTAIRES = [
        self::SUJET_SEJOUR_DECLARATION_INTENTION_INFOS_GENERALES,
        self::SUJET_SEJOUR_DECLARATION_INTENTION_DIRECTION,
        self::SUJET_SEJOUR_DECLARATION_INTENTION_MODALITES,
        self::SUJET_SEJOUR_DECLARATION_INTENTION_LIEUX,
        self::SUJET_SEJOUR_DECLARATION_INTENTION_EFFECTIFS_PREVISIONNELS,
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_INFOS_GENERALES,
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_DIRECTION,
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_UNITES_PARTICIPANTES,
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_MODALITES,
        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_RESUME,
        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_ENCADREMENT,
        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_PARTICIPANTS,
        self::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_DATES,
        self::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_LIEUX,
        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_PROJET_PEDA,
        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_COLOS_APPRENANTES,
        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_INTERNATIONAL,
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_BUDGET,
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_TRANSPORT,
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_ALIMENTAIRE,
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_COMMUNCATION,
        self::SUJET_SEJOUR_VALIDATION,
        self::SUJET_SEJOUR_VALIDATION_INTERNATIONALE,
        self::SUJET_SEJOUR_VISITE,
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_INFOS_GENERALES,
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_DIRECTION,
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_UNITES_PARTICIPANTES,
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_MODALITES,
        self::SUJET_SEJOUR_CLOTURE_RETOURS_GENERAUX,
        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_RESUME,
        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_ENCADREMENT,
        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_PARTICIPANTS,
        self::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_DATES,
        self::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_LIEUX,
        self::SUJET_SEJOUR_CLOTURE_PEDA,
        self::SUJET_SEJOUR_CLOTURE_LOGISTIQUE
    ];

    const MAP_EMPLACEMENTS_SUJETS_COMMENTAIRES = [
        self::SUJET_SEJOUR_DECLARATION_INTENTION_INFOS_GENERALES => "Declaration d'intention > Informations de structure et déclaration",
        self::SUJET_SEJOUR_DECLARATION_INTENTION_DIRECTION => "Declaration d'intention > Identification du·de la direct·eur·rice",
        self::SUJET_SEJOUR_DECLARATION_INTENTION_MODALITES => "Declaration d'intention > Modalités de séjour",
        self::SUJET_SEJOUR_DECLARATION_INTENTION_LIEUX => "Declaration d'intention > Lieu(x) du séjour",
        self::SUJET_SEJOUR_DECLARATION_INTENTION_EFFECTIFS_PREVISIONNELS => "Declaration d'intention > Effectifs prévisionnels",

        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_INFOS_GENERALES => "Projet de séjour > Informations Générales > Informations de structure et déclaration",
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_DIRECTION => "Projet de séjour > Informations Générales > Identification de la personne qui dirige le séjour",
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_UNITES_PARTICIPANTES => "Projet de séjour > Informations Générales > Unité(s) participante(s)",
        self::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_MODALITES => "Projet de séjour > Informations Générales > Modalités de séjour",

        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_RESUME => "Projet de séjour > Effectifs > Résumé",
        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_ENCADREMENT => "Projet de séjour > Effectifs > Equipe d'encadrement",
        self::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_PARTICIPANTS => "Projet de séjour > Effectifs > Participant·e·s",

        self::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_DATES => "Projet de séjour > Date et lieux > Dates du séjour",
        self::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_LIEUX => "Projet de séjour > Date et lieux > Lieu(x) du séjour",

        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_PROJET_PEDA => "Projet de séjour > Démarches pédagogiques > Projet pédagogique",
        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_COLOS_APPRENANTES => "Projet de séjour > Démarches pédagogiques > Dispositif colos apprenantes",
        self::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_INTERNATIONAL => "Projet de séjour > Démarches pédagogiques > Aspect international",

        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_BUDGET => "Projet de séjour > Logistique > Budget prévisionnel",
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_TRANSPORT => "Projet de séjour > Logistique > Transports",
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_ALIMENTAIRE => "Projet de séjour > Logistique > Le projet alimentaire",
        self::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_COMMUNCATION => "Projet de séjour > Logistique > Communication aux familles",

        self::SUJET_SEJOUR_VALIDATION => "Validation du séjour",
        self::SUJET_SEJOUR_VALIDATION_INTERNATIONALE => "Validation internationale du séjour",

        self::SUJET_SEJOUR_VISITE => "Visite(s) du séjour",

        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_INFOS_GENERALES => "Clôture du séjour > Informations Générales > Informations de structure et déclaration",
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_DIRECTION => "Clôture du séjour > Informations Générales > Identification de la personne qui dirige le séjour ",
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_UNITES_PARTICIPANTES => "Clôture du séjour > Informations Générales > Unité(s) participante(s)",
        self::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_MODALITES => "Clôture du séjour > Informations Générales > Modalités de séjour",

        self::SUJET_SEJOUR_CLOTURE_RETOURS_GENERAUX => "Clôture du séjour > Retours généraux",

        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_RESUME => "Clôture du séjour > Effectifs > Résumé",
        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_ENCADREMENT => "Clôture du séjour > Effectifs > Equipe d'encadrement",
        self::SUJET_SEJOUR_CLOTURE_EFFECTIFS_PARTICIPANTS => "Clôture du séjour > Effectifs > Participant·e·s",

        self::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_DATES => "Clôture du séjour > Date et lieux > Dates du séjour",
        self::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_LIEUX => "Clôture du séjour > Date et lieux > Lieu(x) du séjour",

        self::SUJET_SEJOUR_CLOTURE_PEDA => "Clôture du séjour > Démarches pédagogiques",

        self::SUJET_SEJOUR_CLOTURE_LOGISTIQUE => "Clôture du séjour > Logistique"
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'commentaires')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;


    #[ORM\ManyToOne(inversedBy: 'commentaires')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: false)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: Types::TEXT, nullable: false)]
    private ?string $texte = null;

    #[ORM\Column]
    private ?bool $modere = null;

    #[ORM\Column(length: 255, nullable: false)]
    private ?string $sujet = null;

    #[ORM\OneToMany(mappedBy: 'commentaire', targetEntity: DemandeModerationCommentaire::class, orphanRemoval: true)]
    private Collection $demandesModerationCommentaire;

    public function __construct()
    {
        $this->date = new DateTime();
        $this->demandesModerationCommentaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(?Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getModere(): ?bool
    {
        return $this->modere;
    }

    public function setModere(bool $modere): self
    {
        $this->modere = $modere;

        return $this;
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): self
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * @return Collection|DemandeModerationCommentaire[]
     */
    public function getDemandesModerationCommentaire(): Collection
    {
        return $this->demandesModerationCommentaire;
    }

    public function addDemandesModerationCommentaire(DemandeModerationCommentaire $demandesModerationCommentaire): self
    {
        if (!$this->demandesModerationCommentaire->contains($demandesModerationCommentaire)) {
            $this->demandesModerationCommentaire[] = $demandesModerationCommentaire;
            $demandesModerationCommentaire->setCommentaire($this);
        }

        return $this;
    }

    public function removeDemandesModerationCommentaire(DemandeModerationCommentaire $demandesModerationCommentaire): self
    {
        if ($this->demandesModerationCommentaire->removeElement($demandesModerationCommentaire)) {
            // set the owning side to null (unless already changed)
            if ($demandesModerationCommentaire->getCommentaire() === $this) {
                $demandesModerationCommentaire->setCommentaire(null);
            }
        }

        return $this;
    }
}
