<?php

namespace App\Entity;

use App\Repository\ParametreRoadsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Paramètres généraux de ROADS
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: ParametreRoadsRepository::class)]
class ParametreRoads
{
    const PARAMETRE_ADRESSE_SUPPORT = 'adresse_support';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, unique: true)]
    private ?string $nomParametre = null;

    #[ORM\Column(length: 255)]
    private ?string $valeurParametre = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomParametre(): ?string
    {
        return $this->nomParametre;
    }

    public function setNomParametre(string $nomParametre): self
    {
        $this->nomParametre = $nomParametre;

        return $this;
    }

    public function getValeurParametre(): ?string
    {
        return $this->valeurParametre;
    }

    public function setValeurParametre(string $valeurParametre): self
    {
        $this->valeurParametre = $valeurParametre;

        return $this;
    }
}
