<?php

namespace App\Entity;

use App\Repository\InfosEncadrantNonEEDFRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Informations personnelles d'un encadrant non adhérent sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */

#[ORM\Entity(repositoryClass: InfosEncadrantNonEEDFRepository::class)]
class InfosEncadrantNonEEDF
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column(length: 255)]
    #[Groups(['fin_sejour'])]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups(['fin_sejour'])]
    private ?string $prenom = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['fin_sejour'])]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[Groups(["fin_sejour"])]
    private $telephone = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['fin_sejour'])]
    private ?string $departementOuPaysNaissance = null;


    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['fin_sejour'])]
    private ?string $communeNaissance = null;


    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['fin_sejour'])]
    private ?string $genre = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['fin_sejour'])]
    private ?Document $docConsentement = null;


    #[ORM\OneToOne(mappedBy: "infosEncadrantNonEEDF", cascade: ['persist', 'remove'])]
    private ?RoleEncadrantSejour $roleEncadrantSejour;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDepartementOuPaysNaissance(): ?string
    {
        return $this->departementOuPaysNaissance;
    }

    public function setDepartementOuPaysNaissance(?string $departementOuPaysNaissance): self
    {
        $this->departementOuPaysNaissance = $departementOuPaysNaissance;

        return $this;
    }

    public function getCommuneNaissance(): ?string
    {
        return $this->communeNaissance;
    }

    public function setCommuneNaissance(?string $communeNaissance): self
    {
        $this->communeNaissance = $communeNaissance;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(?string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDocConsentement(): ?document
    {
        return $this->docConsentement;
    }

    public function setDocConsentement(document $docConsentement): self
    {
        $this->docConsentement = $docConsentement;

        return $this;
    }

    public function getRoleEncadrantSejour(): ?RoleEncadrantSejour
    {
        return $this->roleEncadrantSejour;
    }

    public function setRoleEncadrantSejour(?RoleEncadrantSejour $roleEncadrantSejour): self
    {
        // unset the owning side of the relation if necessary
        if ($roleEncadrantSejour === null && $this->roleEncadrantSejour !== null) {
            $this->roleEncadrantSejour->setInfosEncadrantNonEEDF(null);
        }

        // set the owning side of the relation if necessary
        if ($roleEncadrantSejour !== null && $roleEncadrantSejour->getInfosEncadrantNonEEDF() !== $this) {
            $roleEncadrantSejour->setInfosEncadrantNonEEDF($this);
        }

        $this->roleEncadrantSejour = $roleEncadrantSejour;

        return $this;
    }
}
