<?php

namespace App\Entity;

use App\Repository\AdressePostaleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Adresse postale 
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AdressePostaleRepository::class)]
class AdressePostale
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $adresse = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $complementAdresse = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $codePostal = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $ville = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $pays = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    public function setComplementAdresse(?string $complementAdresse): self
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getAdresseFormateePourTableur(): string
    {
        $result = $this->adresse;
        if (!(empty($this->complementAdresse))) {
            $result .= ", " . $this->complementAdresse;
        }
        $result .= ", " . $this->codePostal . " " . $this->ville;
        $result .= ", " . $this->pays;

        return $result;
    }
}
