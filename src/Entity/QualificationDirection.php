<?php

namespace App\Entity;

use App\Repository\QualificationDirectionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Qualification du directeur sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: QualificationDirectionRepository::class)]
class QualificationDirection
{

    const DIPLOME_DIRECTION_DSF = "DSF";
    const DIPLOME_DIRECTION_BAFD = "BAFD";
    const DIPLOME_DIRECTION_EQUIV = "Equivalence";

    const QUALITE_NON_DIPLOME = "Non qualifié";
    const QUALITE_STAGIAIRE = "Stagiaire";
    const QUALITE_TITULAIRE = "Titulaire";
    const QUALITE_TITULAIRE_STAGIAIRE = "Titulaire et stagiaire BAFD";

    const FONCTION_INTENDANT = "Intendant·e";
    const FONCTION_AS = "Assistant·e sanitaire";
    const FONCTION_TRESORIER = "Trésorier·e/comptable";
    const FONCTION_MATOS = "Référent·e matériel";
    const FONCTION_PEDA = "Référent·e pédagogie/activité";

    const STATUT_AS_NON_DIPLOME = "Non-diplômé·e";
    const STATUT_AS_PSC1 = "PSC1 ou équivalent";
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'qualificationDirection', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Assert\NotNull(
        groups: ["envoi_pour_validation"],
        message: "Les qualifications et diplômes du·de la direct·eur·rice doivent être renseignées."
    )]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $qualiteDirection = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?array $diplomesDirection = [];

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string  $equivalenceDiplomeDirection = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool  $permisB = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?array $fonction = [];


    #[ORM\Column]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool  $statutAssistantSanitaire = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getQualiteDirection(): ?string
    {
        return $this->qualiteDirection;
    }

    public function setQualiteDirection(?string $qualiteDirection): self
    {
        $this->qualiteDirection = $qualiteDirection;

        return $this;
    }

    public function getDiplomesDirection(): ?array
    {
        return $this->diplomesDirection;
    }

    public function setDiplomesDirection(?array $diplomesDirection): self
    {
        $this->diplomesDirection = $diplomesDirection;

        return $this;
    }

    public function getEquivalenceDiplomeDirection(): ?string
    {
        return $this->equivalenceDiplomeDirection;
    }

    public function setEquivalenceDiplomeDirection(?string $equivalenceDiplomeDirection): self
    {
        $this->equivalenceDiplomeDirection = $equivalenceDiplomeDirection;

        return $this;
    }

    public function getPermisB(): ?bool
    {
        return $this->permisB;
    }

    public function setPermisB(?bool $permisB): self
    {
        $this->permisB = $permisB;

        return $this;
    }

    public function getFonction(): ?array
    {
        return $this->fonction;
    }

    public function setFonction(?array $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getStatutAssistantSanitaire(): ?bool
    {
        return $this->statutAssistantSanitaire;
    }

    public function setStatutAssistantSanitaire(bool $statutAssistantSanitaire): self
    {
        $this->statutAssistantSanitaire = $statutAssistantSanitaire;

        return $this;
    }
}
