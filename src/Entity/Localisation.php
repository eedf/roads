<?php

namespace App\Entity;

use App\Repository\LocalisationRepository;
use LongitudeOne\Spatial\PHP\Types\Geography\Point;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Localisation d'un lieu de séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: LocalisationRepository::class)]
class Localisation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $adresse = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $complementAdresse = null;

    #[ORM\Column(length: 10, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $codePostalCommune = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nomCommune = null;

    #[ORM\Column(type: 'geography_point', nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private $coordonneesGeo;

    #[ORM\OneToOne(mappedBy: "localisation", cascade: ['persist', 'remove'])]
    #[Groups(['fin_sejour'])]
    private ?LieuSejour $lieuSejour = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $pays = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $departement = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    public function setComplementAdresse(?string $complementAdresse): self
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    public function getCodePostalCommune(): ?string
    {
        return $this->codePostalCommune;
    }

    public function setCodePostalCommune(?string $codePostalCommune): self
    {
        $this->codePostalCommune = $codePostalCommune;

        return $this;
    }

    public function getNomCommune(): ?string
    {
        return $this->nomCommune;
    }

    public function setNomCommune(?string $nomCommune): self
    {
        $this->nomCommune = $nomCommune;

        return $this;
    }

    public function getCoordonneesGeo(): ?Point
    {
        return $this->coordonneesGeo;
    }

    public function setCoordonneesGeo(?Point $coordonneesGeo): self
    {
        $this->coordonneesGeo = $coordonneesGeo;

        return $this;
    }

    public function getLieuSejour(): ?LieuSejour
    {
        return $this->lieuSejour;
    }

    public function setLieuSejour(LieuSejour $lieuSejour): self
    {
        // set the owning side of the relation if necessary
        if ($lieuSejour->getLocalisation() !== $this) {
            $lieuSejour->setLocalisation($this);
        }

        $this->lieuSejour = $lieuSejour;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }
}
