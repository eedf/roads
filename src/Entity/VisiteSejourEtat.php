<?php

namespace App\Entity;

use App\Repository\VisiteSejourEtatRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Informations de visites de l'état sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: VisiteSejourEtatRepository::class)]

class VisiteSejourEtat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE,)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 100)]
    private ?string $serviceConcerne = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $retours = null;

    #[ORM\ManyToOne(inversedBy: 'visites')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RetourSejour $retourSejour = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getServiceConcerne(): ?string
    {
        return $this->serviceConcerne;
    }

    public function setServiceConcerne(string $serviceConcerne): self
    {
        $this->serviceConcerne = $serviceConcerne;

        return $this;
    }

    public function getRetours(): ?string
    {
        return $this->retours;
    }

    public function setRetours(?string $retours): self
    {
        $this->retours = $retours;

        return $this;
    }

    public function getRetourSejour(): ?RetourSejour
    {
        return $this->retourSejour;
    }

    public function setRetourSejour(?RetourSejour $retourSejour): self
    {
        $this->retourSejour = $retourSejour;

        return $this;
    }
}
