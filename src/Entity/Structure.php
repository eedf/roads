<?php

namespace App\Entity;

use App\Repository\StructureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * Structure EEDF
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: StructureRepository::class)]
class Structure
{
    const ECHELON_STRUCTURE_NATIONAL = 1;
    const ECHELON_STRUCTURE_REGIONAL = 2;
    const ECHELON_STRUCTURE_LOCAL = 3;

    const TYPE_STRUCTURE_ASSOCIATION = 9;
    const TYPE_STRUCTURE_REGION = 6;
    const TYPE_STRUCTURE_GROUPE_LOCAL = 10;
    const TYPE_STRUCTURE_CENTRE = 15;
    const TYPE_STRUCTURE_SERVICE_VACANCES = 16;
    const TYPE_STRUCTURE_AUTRES = 4;

    const MAP_ECHELON_BY_TYPE_STRUCTURE = [
        self::TYPE_STRUCTURE_ASSOCIATION => self::ECHELON_STRUCTURE_NATIONAL,
        self::TYPE_STRUCTURE_REGION => self::ECHELON_STRUCTURE_REGIONAL,
        self::TYPE_STRUCTURE_GROUPE_LOCAL => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_CENTRE => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_SERVICE_VACANCES => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_AUTRES => self::ECHELON_STRUCTURE_LOCAL
    ];

    const STATUT_STRUCTURE_AUTONOME = 1;
    const STATUT_STRUCTURE_RATTACHEE = 2;
    const STATUT_STRUCTURE_EN_SOMMEIL = 3;
    const STATUT_STRUCTURE_FERMEE = 4;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column(length: 255)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nom;

    #[ORM\Column(length: 100)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $type;

    #[ORM\ManyToOne(inversedBy: 'structuresFilles', fetch: "EAGER")]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Structure $structureParent = null;

    #[ORM\OneToMany(mappedBy: 'structureParent', targetEntity: Structure::class)]
    private Collection $structuresFilles;

    #[ORM\Column(type: Types::DATETIME_MUTABLE,)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "structuresOrganisation")]
    #[ORM\JoinTable(name: "structure_organisation")]
    private Collection $organisateurs;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "structuresValidation")]
    #[ORM\JoinTable(name: "structure_validation")]
    private Collection $validateurs;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "structuresSuivi")]
    #[ORM\JoinTable(name: "structure_suivi")]
    private Collection $suiveurs;


    #[ORM\OneToMany(mappedBy: 'structureOrganisatrice', targetEntity: Sejour::class, orphanRemoval: true)]
    private Collection $sejoursOrganises;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\Regex(
        pattern: "/([0-9]{7})(AS|CL|SV|SC|SP|AJ|SS|AP)([0-9]{6})/",
        message: "Le format du numero de déclaration initiale est incorrect, il doit être composé de 7 chiffres, puis 2 lettres parmi AS, CL, SV, SC, SP, AJ, SS ou AP, puis suivi de 6 chiffres"
    )]
    private ?string $numeroDeclarationInitiale = null;



    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nomOrganisateurDDCS = null;


    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $prenomOrganisateurDDCS = null;

    #[ORM\OneToMany(mappedBy: 'structureEEDF', targetEntity: StructureRegroupementSejour::class)]
    private Collection  $structuresRegroupementSejour;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Uuid $uuid = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: Equipe::class, orphanRemoval: true)]
    private Collection  $equipes;

    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: Adhesion::class, orphanRemoval: true)]
    private Collection  $adhesions;

    #[ORM\Column(length: 255)]
    private ?string $statut = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $echelon = null;

    public function __construct()
    {
        $this->structuresFilles = new ArrayCollection();
        $this->organisateurs = new ArrayCollection();
        $this->validateurs = new ArrayCollection();
        $this->suiveurs = new ArrayCollection();
        $this->sejoursOrganises = new ArrayCollection();
        $this->structuresRegroupementSejour = new ArrayCollection();
        $this->equipes = new ArrayCollection();
        $this->adhesions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStructureParent(): ?self
    {
        return $this->structureParent;
    }

    public function setStructureParent(?self $structureParent): self
    {
        $this->structureParent = $structureParent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getStructuresFilles(): Collection
    {
        return $this->structuresFilles;
    }

    public function addStructuresFille(self $structuresFille): self
    {
        if (!$this->structuresFilles->contains($structuresFille)) {
            $this->structuresFilles[] = $structuresFille;
            $structuresFille->setStructureParent($this);
        }

        return $this;
    }

    public function removeStructuresFille(self $structuresFille): self
    {
        if ($this->structuresFilles->removeElement($structuresFille)) {
            // set the owning side to null (unless already changed)
            if ($structuresFille->getStructureParent() === $this) {
                $structuresFille->setStructureParent(null);
            }
        }

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getOrganisateurs(): Collection
    {
        return $this->organisateurs;
    }

    public function addOrganisateur(Personne $organisateur): self
    {
        if (!$this->organisateurs->contains($organisateur)) {
            $this->organisateurs[] = $organisateur;
        }

        return $this;
    }

    public function removeOrganisateur(Personne $organisateur): self
    {
        $this->organisateurs->removeElement($organisateur);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getValidateurs(): Collection
    {
        return $this->validateurs;
    }

    public function addValidateur(Personne $validateur): self
    {
        if (!$this->validateurs->contains($validateur)) {
            $this->validateurs[] = $validateur;
        }

        return $this;
    }

    public function removeValidateur(Personne $validateur): self
    {
        $this->validateurs->removeElement($validateur);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getSuiveurs(): Collection
    {
        return $this->suiveurs;
    }

    public function addSuiveur(Personne $suiveur): self
    {
        if (!$this->suiveurs->contains($suiveur)) {
            $this->suiveurs[] = $suiveur;
        }

        return $this;
    }

    public function removeSuiveur(Personne $suiveur): self
    {
        $this->suiveurs->removeElement($suiveur);

        return $this;
    }

    /**
     * @return Collection|Sejour[]
     */
    public function getSejoursOrganises(): Collection
    {
        return $this->sejoursOrganises;
    }

    public function addSejoursOrganise(Sejour $sejoursOrganise): self
    {
        if (!$this->sejoursOrganises->contains($sejoursOrganise)) {
            $this->sejoursOrganises[] = $sejoursOrganise;
            $sejoursOrganise->setStructureOrganisatrice($this);
        }

        return $this;
    }

    public function removeSejoursOrganise(Sejour $sejoursOrganise): self
    {
        if ($this->sejoursOrganises->removeElement($sejoursOrganise)) {
            // set the owning side to null (unless already changed)
            if ($sejoursOrganise->getStructureOrganisatrice() === $this) {
                $sejoursOrganise->setStructureOrganisatrice(null);
            }
        }

        return $this;
    }

    public function getNumeroDeclarationInitiale(): ?string
    {
        return $this->numeroDeclarationInitiale;
    }

    public function setNumeroDeclarationInitiale(?string $numeroDeclarationInitiale): self
    {
        //Pour être sûr·e d'avoir les lettres en Majuscules
        $this->numeroDeclarationInitiale = strtoupper($numeroDeclarationInitiale);

        return $this;
    }

    public function getNomOrganisateurDDCS(): ?string
    {
        return $this->nomOrganisateurDDCS;
    }

    public function setNomOrganisateurDDCS(?string $nomOrganisateurDDCS): self
    {
        $this->nomOrganisateurDDCS = $nomOrganisateurDDCS;

        return $this;
    }

    public function getPrenomOrganisateurDDCS(): ?string
    {
        return $this->prenomOrganisateurDDCS;
    }

    public function setPrenomOrganisateurDDCS(?string $prenomOrganisateurDDCS): self
    {
        $this->prenomOrganisateurDDCS = $prenomOrganisateurDDCS;

        return $this;
    }

    /**
     * @return Collection|StructureRegroupementSejour[]
     */
    public function getStructuresRegroupementSejour(): Collection
    {
        return $this->structuresRegroupementSejour;
    }

    public function addStructuresRegroupementSejour(StructureRegroupementSejour $structuresRegroupementSejour): self
    {
        if (!$this->structuresRegroupementSejour->contains($structuresRegroupementSejour)) {
            $this->structuresRegroupementSejour[] = $structuresRegroupementSejour;
            $structuresRegroupementSejour->setStructureEEDF($this);
        }

        return $this;
    }

    public function removeStructuresRegroupementSejour(StructureRegroupementSejour $structuresRegroupementSejour): self
    {
        if ($this->structuresRegroupementSejour->removeElement($structuresRegroupementSejour)) {
            // set the owning side to null (unless already changed)
            if ($structuresRegroupementSejour->getStructureEEDF() === $this) {
                $structuresRegroupementSejour->setStructureEEDF(null);
            }
        }

        return $this;
    }


    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return Collection<int, Equipe>
     */
    public function getEquipes(): Collection
    {
        return $this->equipes;
    }

    /**
     * @return Collection|Equipe[]
     */
    public function getEquipesActives(): Collection
    {
        return $this->equipes->filter(function (Equipe $equipe) {
            return $equipe->getActif();
        });
    }



    /**
     * @return Collection|Equipe[]
     */
    public function getEquipesUnitesActives(): Collection
    {
        return $this->equipes->filter(function (Equipe $equipe) {
            return ($equipe->getActif() && in_array($equipe->getType(), Equipe::LISTE_TYPES_EQUIPES_UNITES));
        });
    }

    public function addEquipe(Equipe $equipe): self
    {
        if (!$this->equipes->contains($equipe)) {
            $this->equipes[] = $equipe;
            $equipe->setStructure($this);
        }

        return $this;
    }


    /**
     * getEquipeGestion
     *  Retourne l'équipe de la structure qui correspond à l'équipe de gestion. S'il n'y a pas qu'un résultat, retourne null
     * @return Equipe
     */
    public function getEquipeGestion(): ?Equipe
    {
        return $this->getEquipeSpecifique(Equipe::TYPE_EQUIPE_EQUIPE_GESTION);
    }

    /**
     * getEquipeGestion
     *  Retourne l'équipe de la structure qui correspond à l'équipe de gestion. S'il n'y a pas qu'un résultat, retourne null
     * @return Equipe
     */
    public function getEquipeSpecifique(String $typeEquipe, $actif = true): ?Equipe
    {
        if ($actif) {
            $equipes_trouvees = $this->equipes->filter(function (Equipe $equipe) use ($typeEquipe) {
                return $equipe->getActif() && $equipe->getType() == $typeEquipe;
            });
        } else {
            $equipes_trouvees = $this->equipes->filter(function (Equipe $equipe) use ($typeEquipe) {
                return $equipe->getType() == $typeEquipe;
            });
        }


        if (count($equipes_trouvees) == 1) {
            return $equipes_trouvees->first();
        } else {
            return null;
        }
    }

    public function removeEquipe(Equipe $equipe): self
    {
        if ($this->equipes->removeElement($equipe)) {
            // set the owning side to null (unless already changed)
            if ($equipe->getStructure() === $this) {
                $equipe->setStructure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Adhesion>
     */
    public function getAdhesions(): Collection
    {
        return $this->adhesions;
    }

    public function addAdhesion(Adhesion $adhesion): self
    {
        if (!$this->adhesions->contains($adhesion)) {
            $this->adhesions[] = $adhesion;
            $adhesion->setStructure($this);
        }

        return $this;
    }

    public function removeAdhesion(Adhesion $adhesion): self
    {
        if ($this->adhesions->removeElement($adhesion)) {
            // set the owning side to null (unless already changed)
            if ($adhesion->getStructure() === $this) {
                $adhesion->setStructure(null);
            }
        }

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }
    public function isActiveOuSousTutelle(): bool
    {
        return $this->statut == $this::STATUT_STRUCTURE_AUTONOME || $this->statut == $this::STATUT_STRUCTURE_RATTACHEE;
    }

    public function getEchelon(): ?string
    {
        return $this->echelon;
    }

    public function setEchelon(?string $echelon): self
    {
        $this->echelon = $echelon;

        return $this;
    }
}
