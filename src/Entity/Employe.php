<?php

namespace App\Entity;

use App\Repository\EmployeRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

#[ORM\Entity(repositoryClass: EmployeRepository::class)]
class Employe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column(type: Types::BIGINT, unique: true)]
    #[Groups(["recherche_auto_complete", "infos_personne", "save_declaration_intention", "fin_sejour"])]
    private ?string $numeroSalarie = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Uuid $uuid = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\OneToOne(inversedBy: 'employe', cascade: ['persist', "remove"])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $email = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telephone = null;

    #[ORM\OneToMany(mappedBy: 'employe', targetEntity: ContratTravail::class, orphanRemoval: true, fetch: "EAGER")]
    #[Groups(["infos_personne", "save_declaration_intention", "fin_sejour"])]
    private Collection $contratsTravail;

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function __construct()
    {
        $this->contratsTravail = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroSalarie(): ?string
    {
        return $this->numeroSalarie;
    }

    public function setNumeroSalarie(string $numeroSalarie): self
    {
        $this->numeroSalarie = $numeroSalarie;

        return $this;
    }
    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(Personne $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return Collection<int, ContratTravail>
     */
    public function getContratsTravail(): Collection
    {
        return $this->contratsTravail;
    }

    public function addContratsTravail(ContratTravail $contratsTravail): self
    {
        if (!$this->contratsTravail->contains($contratsTravail)) {
            $this->contratsTravail[] = $contratsTravail;
            $contratsTravail->setEmploye($this);
        }

        return $this;
    }

    //Vérifie si l'employé à un contrat actif a la date indiquée (si pas de date, on prend la date courante)
    public function hasContratActif(?DateTime $date = null): bool
    {
        if (!$date) {
            $date = new DateTime();
        }
        $contratsActifs = $this->contratsTravail->filter(function (ContratTravail $contratTravail) use ($date) {
            return (($contratTravail->getDateDebut() <= $date
                && $contratTravail->getDateFin() >= $date
            ) || ($contratTravail->getDateDebut() <= $date
                && $contratTravail->getDateFin() == null)
            );
        });

        return count($contratsActifs) > 0;
    }

    /**
     * @return ContratTravail
     */
    public function getContratTravailCourantActif(): ContratTravail
    {
        return $this->contratsTravail->filter(function (ContratTravail $contratTravail) {
            $dateCourante = new DateTime();
            return ($contratTravail->getDateDebut() <= $dateCourante
                && ($contratTravail->getDateFin() == null || $contratTravail->getDateFin() >= $dateCourante)
            );
        })[0];
    }

    public function removeContratsTravail(ContratTravail $contratsTravail): self
    {
        if ($this->contratsTravail->removeElement($contratsTravail)) {
            // set the owning side to null (unless already changed)
            if ($contratsTravail->getEmploye() === $this) {
                $contratsTravail->setEmploye(null);
            }
        }

        return $this;
    }
}
