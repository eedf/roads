<?php

namespace App\Entity;

use App\Repository\RoleEncadrantSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Rôles et fonctions d'un encadrant sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: RoleEncadrantSejourRepository::class)]
class RoleEncadrantSejour
{

    const STATUT_AS_NON_DIPLOME = "Non-diplômé·e";
    const STATUT_AS_PSC1 = "PSC1 ou équivalent";

    const DIPLOME_ANIMATION_ASF = "ASF";
    const DIPLOME_ANIMATION_BAFA = "BAFA";
    const DIPLOME_ANIMATION_EQUIV = "Equivalence";

    const QUALITE_NON_DIPLOME = "Non qualifié";
    const QUALITE_STAGIAIRE = "Stagiaire";
    const QUALITE_TITULAIRE = "Titulaire";
    const QUALITE_TITULAIRE_STAGIAIRE = "Titulaire et stagiaire BAFA";


    const EQUIPE_ANIMATION = "L'équipe d'animation";
    const EQUIPE_SUPPORT = "L'équipe support";

    const FONCTION_DIRECTION_ADJOINTE = "Direction adjointe";
    const FONCTION_INTENDANT = "Intendant·e";
    const FONCTION_AS = "Assistant·e sanitaire";
    const FONCTION_TRESORIER = "Trésorier·e/comptable";
    const FONCTION_MATOS = "Référent·e matériel";
    const FONCTION_PEDA = "Référent·e pédagogie/activité";

    const ATTACHER_NON = "non";
    const ATTACHER_EEDF = "eedf";
    const ATTACHER_NON_EEDF = "non_eedf";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'rolesEncadrants')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;


    #[ORM\ManyToOne(inversedBy: 'rolesSejours')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?Personne $encadrantEEDF = null;

    #[ORM\ManyToOne(inversedBy: 'encadrants')]
    #[Groups(["fin_sejour"])]
    private ?ParticipationSejourUnite $uniteParticipante = null;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?bool  $statutAssistantSanitaire = null;

    #[ORM\Column(type: Types::ARRAY)]
    #[Groups(["fin_sejour"])]
    private ?array $fonction = [];

    #[ORM\OneToOne(inversedBy: 'roleEncadrantSejour', cascade: ['persist', 'remove'])]
    private ?InfosEncadrantNonEEDF $infosEncadrantNonEEDF = null;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?bool  $estEncadrantEEDF = null;


    #[ORM\ManyToOne(inversedBy: 'encadrants')]
    #[Groups(["fin_sejour"])]
    private ?StructureRegroupementSejour $structureNonEEDF = null;

    #[ORM\Column(length: 50)]
    #[Groups(["fin_sejour"])]
    private ?string $attacher = self::ATTACHER_NON;


    #[ORM\Column(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?bool  $permisB = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?array $diplomesAnimation = [];


    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $equivalenceDiplomeAnimation = null;

    #[ORM\Column(length: 50)]
    #[Groups(["fin_sejour"])]
    private ?string  $qualiteAnimation;

    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $equipe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(?Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getEncadrantEEDF(): ?Personne
    {
        return $this->encadrantEEDF;
    }

    public function setEncadrantEEDF(?Personne $encadrantEEDF): self
    {
        $this->encadrantEEDF = $encadrantEEDF;

        return $this;
    }

    public function getUniteParticipante(): ?ParticipationSejourUnite
    {
        return $this->uniteParticipante;
    }

    public function setUniteParticipante(?ParticipationSejourUnite $uniteParticipante): self
    {
        $this->uniteParticipante = $uniteParticipante;

        return $this;
    }

    public function getStatutAssistantSanitaire(): ?bool
    {
        return $this->statutAssistantSanitaire;
    }

    public function setStatutAssistantSanitaire(bool $statutAssistantSanitaire): self
    {
        $this->statutAssistantSanitaire = $statutAssistantSanitaire;

        return $this;
    }

    public function getFonction(): ?array
    {
        return $this->fonction;
    }

    public function setFonction(array $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getInfosEncadrantNonEEDF(): ?InfosEncadrantNonEEDF
    {
        return $this->infosEncadrantNonEEDF;
    }

    public function setInfosEncadrantNonEEDF(?InfosEncadrantNonEEDF $infosEncadrantNonEEDF): self
    {
        $this->infosEncadrantNonEEDF = $infosEncadrantNonEEDF;

        return $this;
    }

    public function getEstEncadrantEEDF(): ?bool
    {
        return $this->estEncadrantEEDF;
    }

    public function setEstEncadrantEEDF(?bool $estEncadrantEEDF): self
    {
        $this->estEncadrantEEDF = $estEncadrantEEDF;

        return $this;
    }

    public function getStructureNonEEDF(): ?StructureRegroupementSejour
    {
        return $this->structureNonEEDF;
    }

    public function setStructureNonEEDF(?StructureRegroupementSejour $structureNonEEDF): self
    {
        $this->structureNonEEDF = $structureNonEEDF;

        return $this;
    }

    public function getAttacher(): ?string
    {
        return $this->attacher;
    }

    public function setAttacher(string $attacher): self
    {
        $this->attacher = $attacher;

        return $this;
    }

    public function getPermisB(): ?bool
    {
        return $this->permisB;
    }

    public function setPermisB(?bool $permisB): self
    {
        $this->permisB = $permisB;

        return $this;
    }

    public function getDiplomesAnimation(): ?array
    {
        return $this->diplomesAnimation;
    }

    public function setDiplomesAnimation(?array $diplomesAnimation): self
    {
        $this->diplomesAnimation = $diplomesAnimation;

        return $this;
    }

    public function getEquivalenceDiplomeAnimation(): ?string
    {
        return $this->equivalenceDiplomeAnimation;
    }

    public function setEquivalenceDiplomeAnimation(?string $equivalenceDiplomeAnimation): self
    {
        $this->equivalenceDiplomeAnimation = $equivalenceDiplomeAnimation;

        return $this;
    }

    public function getQualiteAnimation(): ?string
    {
        return $this->qualiteAnimation;
    }

    public function setQualiteAnimation(string $qualiteAnimation): self
    {
        $this->qualiteAnimation = $qualiteAnimation;

        return $this;
    }

    public function getEquipe(): ?string
    {
        return $this->equipe;
    }

    public function setEquipe(?string $equipe): self
    {
        $this->equipe = $equipe;

        return $this;
    }
}
