<?php

namespace App\Entity;

use App\Repository\AspectCommunicationSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Aspects communication d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AspectCommunicationSejourRepository::class)]
class AspectCommunicationSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'aspectCommunicationSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[ORM\JoinColumn()]
    #[Groups(["fin_sejour"])]
    private ?string $presentationAmont = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateReunionInformation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $moyenCommunicationPendant = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $moyenRestitution = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $siteValorisation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getPresentationAmont(): ?string
    {
        return $this->presentationAmont;
    }

    public function setPresentationAmont(?string $presentationAmont): self
    {
        $this->presentationAmont = $presentationAmont;

        return $this;
    }

    public function getDateReunionInformation(): ?\DateTimeInterface
    {
        return $this->dateReunionInformation;
    }

    public function setDateReunionInformation(?\DateTimeInterface $dateReunionInformation): self
    {
        $this->dateReunionInformation = $dateReunionInformation;

        return $this;
    }

    public function getMoyenCommunicationPendant(): ?string
    {
        return $this->moyenCommunicationPendant;
    }

    public function setMoyenCommunicationPendant(?string $moyenCommunicationPendant): self
    {
        $this->moyenCommunicationPendant = $moyenCommunicationPendant;

        return $this;
    }

    public function getMoyenRestitution(): ?string
    {
        return $this->moyenRestitution;
    }

    public function setMoyenRestitution(?string $moyenRestitution): self
    {
        $this->moyenRestitution = $moyenRestitution;

        return $this;
    }

    public function getSiteValorisation(): ?string
    {
        return $this->siteValorisation;
    }

    public function setSiteValorisation(?string $siteValorisation): self
    {
        $this->siteValorisation = $siteValorisation;

        return $this;
    }
}
