<?php

namespace App\Entity;

use App\Repository\ParticipationSejourUniteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Participation d'une unité à un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: ParticipationSejourUniteRepository::class)]
class ParticipationSejourUnite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\OneToMany(mappedBy: 'uniteParticipante', targetEntity: RoleEncadrantSejour::class, cascade: ["remove"], orphanRemoval: true)]
    private Collection $encadrants;


    #[ORM\OneToMany(mappedBy: 'unite', targetEntity: ParticipantNonAdherent::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    #[Groups(["fin_sejour"])]
    private Collection $participantsNonAdherents;

    #[ORM\ManyToMany(inversedBy: "participationsSejours", targetEntity: Personne::class)]
    #[Groups(["fin_sejour"])]
    private Collection $participantsAdherents;

    #[ORM\ManyToOne(inversedBy: 'unitesParticipantes')]
    private ?StructureRegroupementSejour $structureRegroupementSejour = null;


    #[ORM\ManyToOne(inversedBy: 'unitesStructureOrganisatrice')]
    private ?Sejour $sejourOrganisateur = null;


    #[ORM\ManyToOne(inversedBy: 'participationsSejour')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["fin_sejour"])]
    private ?Equipe $equipe = null;


    public function __construct()
    {
        $this->encadrants = new ArrayCollection();
        $this->participantsNonAdherents = new ArrayCollection();
        $this->participantsAdherents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return Collection|RoleEncadrantSejour[]
     */
    public function getEncadrants(): Collection
    {
        return $this->encadrants;
    }

    public function addEncadrant(RoleEncadrantSejour $encadrant): self
    {
        if (!$this->encadrants->contains($encadrant)) {
            $this->encadrants[] = $encadrant;
            $encadrant->setUniteParticipante($this);
        }

        return $this;
    }

    public function removeEncadrant(RoleEncadrantSejour $encadrant): self
    {
        if ($this->encadrants->removeElement($encadrant)) {
            // set the owning side to null (unless already changed)
            if ($encadrant->getUniteParticipante() === $this) {
                $encadrant->setUniteParticipante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ParticipantNonAdherent[]
     */
    public function getParticipantsNonAdherents(): Collection
    {
        return $this->participantsNonAdherents;
    }

    public function addParticipantsNonAdherent(ParticipantNonAdherent $participantsNonAdherent): self
    {
        if (!$this->participantsNonAdherents->contains($participantsNonAdherent)) {
            $this->participantsNonAdherents[] = $participantsNonAdherent;
            $participantsNonAdherent->setUnite($this);
        }

        return $this;
    }

    public function removeParticipantsNonAdherent(ParticipantNonAdherent $participantsNonAdherent): self
    {
        if ($this->participantsNonAdherents->removeElement($participantsNonAdherent)) {
            // set the owning side to null (unless already changed)
            if ($participantsNonAdherent->getUnite() === $this) {
                $participantsNonAdherent->setUnite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getParticipantsAdherents(): Collection
    {
        return $this->participantsAdherents;
    }

    public function addParticipantsAdherent(Personne $participantsAdherent): self
    {
        if (!$this->participantsAdherents->contains($participantsAdherent)) {
            $this->participantsAdherents[] = $participantsAdherent;
        }

        return $this;
    }

    public function removeParticipantsAdherent(Personne $participantsAdherent): self
    {
        $this->participantsAdherents->removeElement($participantsAdherent);

        return $this;
    }

    public function getStructureRegroupementSejour(): ?StructureRegroupementSejour
    {
        return $this->structureRegroupementSejour;
    }

    public function setStructureRegroupementSejour(?StructureRegroupementSejour $structureRegroupementSejour): self
    {
        $this->structureRegroupementSejour = $structureRegroupementSejour;

        return $this;
    }

    public function getSejourOrganisateur(): ?Sejour
    {
        return $this->sejourOrganisateur;
    }

    public function setSejourOrganisateur(?Sejour $sejourOrganisateur): self
    {
        $this->sejourOrganisateur = $sejourOrganisateur;

        return $this;
    }

    public function getEquipe(): ?Equipe
    {
        return $this->equipe;
    }

    public function setEquipe(?Equipe $equipe): self
    {
        $this->equipe = $equipe;

        return $this;
    }
}
