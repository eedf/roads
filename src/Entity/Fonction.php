<?php

namespace App\Entity;

use App\Repository\FonctionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * Fonction associative d'un·e adhérent·e ou mission d'un·e salarié·e
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: FonctionRepository::class)]
class Fonction
{
    const CATEGORY_FONCTION_BENEVOLE = 1;
    const CATEGORY_FONCTION_PARTICIPANT = 2;
    const CATEGORY_FONCTION_SALARIE = 3;

    const TYPE_FONCTION_PARTICIPANT_SIMPLE = 1;
    const TYPE_FONCTION_MEMBRE = 2;
    const TYPE_FONCTION_RESPONSABLE = 3;
    const TYPE_FONCTION_TRESORIER = 4;
    const TYPE_FONCTION_SALARIE = 6;
    const TYPE_MISSION_ORGANISATION = 7;
    const TYPE_MISSION_COORDINATION = 8;
    const TYPE_MISSION_REPRESENTATION = 9;
    const TYPE_MISSION_TRESORERIE = 10;

    const MAP_CATEGORY_BY_TYPE_FONCTION = [
        self::TYPE_FONCTION_PARTICIPANT_SIMPLE => self::CATEGORY_FONCTION_PARTICIPANT,
        self::TYPE_FONCTION_MEMBRE => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_RESPONSABLE => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_TRESORIER => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_SALARIE => self::CATEGORY_FONCTION_SALARIE,
        self::TYPE_MISSION_ORGANISATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_COORDINATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_REPRESENTATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_TRESORERIE => self::CATEGORY_FONCTION_BENEVOLE,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions')]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Personne $personne = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Equipe $equipe = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups(['save_declaration_intention'])]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    #[Groups(['save_declaration_intention'])]
    private ?string $statut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): self
    {
        $this->personne = $personne;

        return $this;
    }

    public function getEquipe(): ?Equipe
    {
        return $this->equipe;
    }

    public function setEquipe(?Equipe $equipe): self
    {
        $this->equipe = $equipe;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }
}
