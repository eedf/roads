<?php

namespace App\Entity;

use App\Repository\CoordonneesRepository;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Coordonnées personnelles
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: CoordonneesRepository::class)]
class Coordonnees
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $nom = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $prenom = null;


    #[ORM\OneToOne(cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Assert\Valid]
    #[ORM\JoinColumn(nullable: true)]
    private ?AdressePostale $adressePostale = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[Groups(["infos_personne", "save_declaration_intention"])]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telMobile = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telDomicile = null;


    #[ORM\OneToOne(mappedBy: 'coordonnees', cascade: ['persist', 'remove'])]
    private ?Personne $personne = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $adresseMail = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdressePostale(): ?AdressePostale
    {
        return $this->adressePostale;
    }

    public function setAdressePostale(AdressePostale $adressePostale): self
    {
        $this->adressePostale = $adressePostale;

        return $this;
    }

    public function getTelMobile(): ?PhoneNumber
    {
        return $this->telMobile;
    }

    public function setTelMobile($telMobile): self
    {
        $this->telMobile = $telMobile;

        return $this;
    }

    public function getTelDomicile()
    {
        return $this->telDomicile;
    }

    public function setTelDomicile($telDomicile): self
    {
        $this->telDomicile = $telDomicile;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(Personne $personne): self
    {
        // set the owning side of the relation if necessary
        if ($personne->getCoordonnees() !== $this) {
            $personne->setCoordonnees($this);
        }

        $this->personne = $personne;

        return $this;
    }

    public function getAdresseMail(): ?string
    {
        return $this->adresseMail;
    }

    public function setAdresseMail(?string $adresseMail): self
    {
        $this->adresseMail = $adresseMail;

        return $this;
    }
}
