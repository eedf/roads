<?php

namespace App\Entity;

use App\Repository\VisiteSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Visite effective d'un·e membre de l'association sur un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: VisiteSejourRepository::class)]
class VisiteSejour
{

    const EVAL_OK = "OK";
    const EVAL_MOYENNEMENT_OK = "Moyennement OK";
    const EVAL_NON_OK = "Pas OK";
    const EVAL_NON_EVALUE = "Cet aspect n'a pas été évalué";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE,)]
    private ?\DateTimeInterface $dateVisite = null;

    #[ORM\Column(nullable: true)]
    private ?bool $evalManquesClasseurDirecteur = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $precisionsManquesClasseurDirecteur = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalMesuresSanitaires = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresMesuresSanitaires = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalPedagogie = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresPedagogie = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalFormation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresFormation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalAmenagement = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresAmenagement = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalHygiene = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresHygiene = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalAlimentation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresAlimentation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalGestionAdminFinanciere = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresGestionAdminFinanciere = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $sousEvaluationBudget = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalCommunicationExterne = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresCommunicationExterne = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $dispositifInfosParents = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalSante = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresSante = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[
        Assert\Expression(
            "((this.getEvalPointsGeneraux()!==null) || (this.getCompteRendu()))",
            message: "Les points généraux doivent être renseignés"
        )
    ]
    private ?string $evalPointsGeneraux = null;


    #[Assert\Expression(
        "((this.getEvalAvisGeneral()!==null) || (this.getCompteRendu()))",
        message: "L'avis général doit être renseigné",
    )]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $evalAvisGeneral = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Assert\Valid()]
    #[
        Assert\Expression(
            "((this.getEvalManquesClasseurDirecteur()!==null) || (this.getCompteRendu()))",
            message: "Le compte-rendu de visite doit être renseigné",
        )
    ]
    private ?Document $compteRendu = null;

    #[ORM\OneToOne(inversedBy: "visiteSejour", cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?NominationVisiteSejour $nominationVisite = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $sujetsComplementairesFormation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $autresGestionEquipe = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $evalGestionEquipe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateVisite(): ?\DateTimeInterface
    {
        return $this->dateVisite;
    }

    public function setDateVisite(\DateTimeInterface $dateVisite): self
    {
        $this->dateVisite = $dateVisite;

        return $this;
    }

    public function getEvalManquesClasseurDirecteur(): ?bool
    {
        return $this->evalManquesClasseurDirecteur;
    }

    public function setEvalManquesClasseurDirecteur(?bool $evalManquesClasseurDirecteur): self
    {
        $this->evalManquesClasseurDirecteur = $evalManquesClasseurDirecteur;

        return $this;
    }

    public function getPrecisionsManquesClasseurDirecteur(): ?string
    {
        return $this->precisionsManquesClasseurDirecteur;
    }

    public function setPrecisionsManquesClasseurDirecteur(?string $precisionsManquesClasseurDirecteur): self
    {
        $this->precisionsManquesClasseurDirecteur = $precisionsManquesClasseurDirecteur;

        return $this;
    }

    public function getEvalMesuresSanitaires(): ?string
    {
        return $this->evalMesuresSanitaires;
    }

    public function setEvalMesuresSanitaires(?string $evalMesuresSanitaires): self
    {
        $this->evalMesuresSanitaires = $evalMesuresSanitaires;

        return $this;
    }

    public function getAutresMesuresSanitaires(): ?string
    {
        return $this->autresMesuresSanitaires;
    }

    public function setAutresMesuresSanitaires(?string $autresMesuresSanitaires): self
    {
        $this->autresMesuresSanitaires = $autresMesuresSanitaires;

        return $this;
    }

    public function getEvalPedagogie(): ?string
    {
        return $this->evalPedagogie;
    }

    public function setEvalPedagogie(?string $evalPedagogie): self
    {
        $this->evalPedagogie = $evalPedagogie;

        return $this;
    }

    public function getAutresPedagogie(): ?string
    {
        return $this->autresPedagogie;
    }

    public function setAutresPedagogie(?string $autresPedagogie): self
    {
        $this->autresPedagogie = $autresPedagogie;

        return $this;
    }

    public function getEvalFormation(): ?string
    {
        return $this->evalFormation;
    }

    public function setEvalFormation(?string $evalFormation): self
    {
        $this->evalFormation = $evalFormation;

        return $this;
    }

    public function getAutresFormation(): ?string
    {
        return $this->autresFormation;
    }

    public function setAutresFormation(?string $autresFormation): self
    {
        $this->autresFormation = $autresFormation;

        return $this;
    }

    public function getEvalAmenagement(): ?string
    {
        return $this->evalAmenagement;
    }

    public function setEvalAmenagement(?string $evalAmenagement): self
    {
        $this->evalAmenagement = $evalAmenagement;

        return $this;
    }

    public function getAutresAmenagement(): ?string
    {
        return $this->autresAmenagement;
    }

    public function setAutresAmenagement(?string $autresAmenagement): self
    {
        $this->autresAmenagement = $autresAmenagement;

        return $this;
    }

    public function getEvalHygiene(): ?string
    {
        return $this->evalHygiene;
    }

    public function setEvalHygiene(?string $evalHygiene): self
    {
        $this->evalHygiene = $evalHygiene;

        return $this;
    }

    public function getAutresHygiene(): ?string
    {
        return $this->autresHygiene;
    }

    public function setAutresHygiene(?string $autresHygiene): self
    {
        $this->autresHygiene = $autresHygiene;

        return $this;
    }

    public function getEvalAlimentation(): ?string
    {
        return $this->evalAlimentation;
    }

    public function setEvalAlimentation(?string $evalAlimentation): self
    {
        $this->evalAlimentation = $evalAlimentation;

        return $this;
    }

    public function getAutresAlimentation(): ?string
    {
        return $this->autresAlimentation;
    }

    public function setAutresAlimentation(?string $autresAlimentation): self
    {
        $this->autresAlimentation = $autresAlimentation;

        return $this;
    }

    public function getEvalGestionAdminFinanciere(): ?string
    {
        return $this->evalGestionAdminFinanciere;
    }

    public function setEvalGestionAdminFinanciere(?string $evalGestionAdminFinanciere): self
    {
        $this->evalGestionAdminFinanciere = $evalGestionAdminFinanciere;

        return $this;
    }

    public function getAutresGestionAdminFinanciere(): ?string
    {
        return $this->autresGestionAdminFinanciere;
    }

    public function setAutresGestionAdminFinanciere(?string $autresGestionAdminFinanciere): self
    {
        $this->autresGestionAdminFinanciere = $autresGestionAdminFinanciere;

        return $this;
    }

    public function getSousEvaluationBudget(): ?string
    {
        return $this->sousEvaluationBudget;
    }

    public function setSousEvaluationBudget(?string $sousEvaluationBudget): self
    {
        $this->sousEvaluationBudget = $sousEvaluationBudget;

        return $this;
    }

    public function getEvalCommunicationExterne(): ?string
    {
        return $this->evalCommunicationExterne;
    }

    public function setEvalCommunicationExterne(?string $evalCommunicationExterne): self
    {
        $this->evalCommunicationExterne = $evalCommunicationExterne;

        return $this;
    }

    public function getAutresCommunicationExterne(): ?string
    {
        return $this->autresCommunicationExterne;
    }

    public function setAutresCommunicationExterne(?string $autresCommunicationExterne): self
    {
        $this->autresCommunicationExterne = $autresCommunicationExterne;

        return $this;
    }

    public function getDispositifInfosParents(): ?string
    {
        return $this->dispositifInfosParents;
    }

    public function setDispositifInfosParents(?string $dispositifInfosParents): self
    {
        $this->dispositifInfosParents = $dispositifInfosParents;

        return $this;
    }

    public function getEvalSante(): ?string
    {
        return $this->evalSante;
    }

    public function setEvalSante(?string $evalSante): self
    {
        $this->evalSante = $evalSante;

        return $this;
    }

    public function getAutresSante(): ?string
    {
        return $this->autresSante;
    }

    public function setAutresSante(?string $autresSante): self
    {
        $this->autresSante = $autresSante;

        return $this;
    }
    public function setEvalGestionEquipe(?string $evalGestionEquipe): self
    {
        $this->evalGestionEquipe = $evalGestionEquipe;

        return $this;
    }

    public function getAutresGestionEquipe(): ?string
    {
        return $this->autresGestionEquipe;
    }

    public function setAutresGestionEquipe(?string $autresGestionEquipe): self
    {
        $this->autresGestionEquipe = $autresGestionEquipe;

        return $this;
    }

    public function getEvalPointsGeneraux(): ?string
    {
        return $this->evalPointsGeneraux;
    }

    public function setEvalPointsGeneraux(?string $evalPointsGeneraux): self
    {
        $this->evalPointsGeneraux = $evalPointsGeneraux;

        return $this;
    }

    public function getEvalAvisGeneral(): ?string
    {
        return $this->evalAvisGeneral;
    }

    public function setEvalAvisGeneral(?string $evalAvisGeneral): self
    {
        $this->evalAvisGeneral = $evalAvisGeneral;

        return $this;
    }

    public function getCompteRendu(): ?Document
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(?Document $compteRendu): self
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }

    public function getNominationVisite(): ?NominationVisiteSejour
    {
        return $this->nominationVisite;
    }

    public function setNominationVisite(NominationVisiteSejour $nominationVisite): self
    {
        $this->nominationVisite = $nominationVisite;

        return $this;
    }

    public function getSujetsComplementairesFormation(): ?string
    {
        return $this->sujetsComplementairesFormation;
    }

    public function setSujetsComplementairesFormation(?string $sujetsComplementairesFormation): self
    {
        $this->sujetsComplementairesFormation = $sujetsComplementairesFormation;

        return $this;
    }
    public function getEvalGestionEquipe(): ?string
    {
        return $this->evalGestionEquipe;
    }
}
