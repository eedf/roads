<?php

namespace App\Entity;

use App\Repository\EquipeRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: EquipeRepository::class)]
class Equipe
{
    const TYPE_EQUIPE_SUPPORT_DEVELOPPEMENT = 1;
    const TYPE_EQUIPE_EQUIPE_NATIONALE = 2;
    const TYPE_EQUIPE_COMMISSION_CONTROLE = 4;
    const TYPE_EQUIPE_COMMISSION = 5;
    const TYPE_EQUIPE_EQUIPE_GESTION = 6;
    const TYPE_EQUIPE_FORMATION = 7;
    const TYPE_EQUIPE_ACTIVITE_ADAPTEE = 8;
    const TYPE_EQUIPE_UNITE_AINE = 9;
    const TYPE_EQUIPE_UNITE_ECLE = 10;
    const TYPE_EQUIPE_CERCLE_LOUVETEAUX = 11;
    const TYPE_EQUIPE_RONDE_LUTINS = 12;
    const TYPE_EQUIPE_UNITE_NOMADE = 13;
    const TYPE_EQUIPE_ACTIVITE_COMPLEMENTAIRE = 14;
    const TYPE_EQUIPE_PERSONNALISEE = 15;

    const LISTE_TYPES_EQUIPES_UNITES = [
        self::TYPE_EQUIPE_UNITE_AINE,
        self::TYPE_EQUIPE_UNITE_ECLE,
        self::TYPE_EQUIPE_CERCLE_LOUVETEAUX,
        self::TYPE_EQUIPE_RONDE_LUTINS
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'equipes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Structure $structure = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $type = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDesactivation = null;

    #[ORM\Column(length: 255)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $nom = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Uuid $uuid = null;

    #[ORM\OneToMany(mappedBy: 'equipe', targetEntity: Fonction::class, orphanRemoval: true)]
    private Collection $fonctions;

    #[ORM\OneToMany(mappedBy: 'equipe', targetEntity: ParticipationSejourUnite::class, orphanRemoval: true)]
    private Collection $participationsSejour;

    #[ORM\ManyToMany(mappedBy: 'unites', targetEntity: StructureRegroupementSejour::class)]
    private Collection $structureRegroupementSejours;

    public function __construct()
    {
        $this->fonctions = new ArrayCollection();
        $this->participationsSejour = new ArrayCollection();
        $this->structureRegroupementSejours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateDesactivation(): ?\DateTimeInterface
    {
        return $this->dateDesactivation;
    }

    public function setDateDesactivation(?\DateTimeInterface $dateDesactivation): self
    {
        $this->dateDesactivation = $dateDesactivation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return Collection<int, Fonction>
     */
    public function getFonctions(): Collection
    {
        return $this->fonctions;
    }

    public function addFonction(Fonction $fonction): self
    {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions[] = $fonction;
            $fonction->setEquipe($this);
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): self
    {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getEquipe() === $this) {
                $fonction->setEquipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ParticipationSejourUnite>
     */
    public function getParticipationsSejour(): Collection
    {
        return $this->participationsSejour;
    }

    public function addParticipationsSejour(ParticipationSejourUnite $participationsSejour): self
    {
        if (!$this->participationsSejour->contains($participationsSejour)) {
            $this->participationsSejour[] = $participationsSejour;
            $participationsSejour->setEquipe($this);
        }

        return $this;
    }

    public function removeParticipationsSejour(ParticipationSejourUnite $participationsSejour): self
    {
        if ($this->participationsSejour->removeElement($participationsSejour)) {
            // set the owning side to null (unless already changed)
            if ($participationsSejour->getEquipe() === $this) {
                $participationsSejour->setEquipe(null);
            }
        }

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->dateDesactivation == null;
    }

    /**
     * @return Collection|StructureRegroupementSejour[]
     */
    public function getStructureRegroupementSejours(): Collection
    {
        return $this->structureRegroupementSejours;
    }

    public function addStructureRegroupementSejour(StructureRegroupementSejour $structureRegroupementSejour): self
    {
        if (!$this->structureRegroupementSejours->contains($structureRegroupementSejour)) {
            $this->structureRegroupementSejours[] = $structureRegroupementSejour;
            $structureRegroupementSejour->addUnite($this);
        }

        return $this;
    }

    public function removeStructureRegroupementSejour(StructureRegroupementSejour $structureRegroupementSejour): self
    {
        if ($this->structureRegroupementSejours->removeElement($structureRegroupementSejour)) {
            $structureRegroupementSejour->removeUnite($this);
        }

        return $this;
    }

    public function getFonctionsActives(?String $typeFonction, ?DateTime $dateCourante)
    {
        if ($this->fonctions->count() > 0) {
            return $this->fonctions->filter(function (Fonction $fonction) use ($typeFonction, $dateCourante) {
                if ($dateCourante == null) {
                    $dateCourante = new DateTime();
                }
                if ($typeFonction == null) {
                    return ($fonction->getDateDebut() <= $dateCourante
                        && ($fonction->getDateFin() == null || $fonction->getDateFin() >= $dateCourante)
                    );
                } else {
                    return ($fonction->getDateDebut() <= $dateCourante
                        && ($fonction->getDateFin() == null || $fonction->getDateFin() >= $dateCourante)
                        && $fonction->getType() == $typeFonction
                    );
                }
            });
        } else {
            return null;
        }
    }
}
