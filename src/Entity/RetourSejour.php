<?php

namespace App\Entity;

use App\Repository\RetourSejourRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Retours généraux d'un séjour (clôture de séjour)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: RetourSejourRepository::class)]
class RetourSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\OneToOne(inversedBy: 'retourSejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotNull(groups: ["cloture_sejour"])]
    private ?bool $visMonCamp = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $precisionsVisMonCamp = null;

    #[ORM\Column]
    #[Assert\NotNull(groups: ["cloture_sejour"])]
    private ?bool $parutionMedias = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string  $nomMedia = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateMedia = null;


    #[ORM\Column]
    #[Assert\NotNull(groups: ["cloture_sejour"])]
    private ?bool $visiteEtat = null;

    #[ORM\OneToMany(mappedBy: 'retourSejour', targetEntity: VisiteSejourEtat::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    private Collection $visites;


    #[ORM\Column]
    #[Assert\NotNull(groups: ["cloture_sejour"])]
    private ?bool $evenementsGraves = null;

    #[ORM\OneToMany(mappedBy: 'retourSejour', targetEntity: EvenementGrave::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    private Collection $descriptionEvenementsGraves;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $incidentsMineurs = null;

    public function __construct()
    {
        $this->visites = new ArrayCollection();
        $this->descriptionEvenementsGraves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getVisMonCamp(): ?bool
    {
        return $this->visMonCamp;
    }

    public function setVisMonCamp(?bool $visMonCamp): self
    {
        $this->visMonCamp = $visMonCamp;

        return $this;
    }

    public function getPrecisionsVisMonCamp(): ?string
    {
        return $this->precisionsVisMonCamp;
    }

    public function setPrecisionsVisMonCamp(?string $precisionsVisMonCamp): self
    {
        $this->precisionsVisMonCamp = $precisionsVisMonCamp;

        return $this;
    }

    public function getParutionMedias(): ?bool
    {
        return $this->parutionMedias;
    }

    public function setParutionMedias(bool $parutionMedias): self
    {
        $this->parutionMedias = $parutionMedias;

        return $this;
    }

    public function getNomMedia(): ?string
    {
        return $this->nomMedia;
    }

    public function setNomMedia(?string $nomMedia): self
    {
        $this->nomMedia = $nomMedia;

        return $this;
    }

    public function getDateMedia(): ?\DateTimeInterface
    {
        return $this->dateMedia;
    }

    public function setDateMedia(?\DateTimeInterface $dateMedia): self
    {
        $this->dateMedia = $dateMedia;

        return $this;
    }

    public function getVisiteEtat(): ?bool
    {
        return $this->visiteEtat;
    }

    public function setVisiteEtat(bool $visiteEtat): self
    {
        $this->visiteEtat = $visiteEtat;

        return $this;
    }

    /**
     * @return Collection|VisiteSejourEtat[]
     */
    public function getVisites(): Collection
    {
        return $this->visites;
    }

    public function addVisite(VisiteSejourEtat $visite): self
    {
        if (!$this->visites->contains($visite)) {
            $this->visites[] = $visite;
            $visite->setRetourSejour($this);
        }

        return $this;
    }

    public function removeVisite(VisiteSejourEtat $visite): self
    {
        if ($this->visites->removeElement($visite)) {
            // set the owning side to null (unless already changed)
            if ($visite->getRetourSejour() === $this) {
                $visite->setRetourSejour(null);
            }
        }

        return $this;
    }

    public function getEvenementsGraves(): ?bool
    {
        return $this->evenementsGraves;
    }

    public function setEvenementsGraves(bool $evenementsGraves): self
    {
        $this->evenementsGraves = $evenementsGraves;

        return $this;
    }

    /**
     * @return Collection|EvenementGrave[]
     */
    public function getDescriptionEvenementsGraves(): Collection
    {
        return $this->descriptionEvenementsGraves;
    }

    public function addDescriptionEvenementsGrave(EvenementGrave $descriptionEvenementsGrave): self
    {
        if (!$this->descriptionEvenementsGraves->contains($descriptionEvenementsGrave)) {
            $this->descriptionEvenementsGraves[] = $descriptionEvenementsGrave;
            $descriptionEvenementsGrave->setRetourSejour($this);
        }

        return $this;
    }

    public function removeDescriptionEvenementsGrave(EvenementGrave $descriptionEvenementsGrave): self
    {
        if ($this->descriptionEvenementsGraves->removeElement($descriptionEvenementsGrave)) {
            // set the owning side to null (unless already changed)
            if ($descriptionEvenementsGrave->getRetourSejour() === $this) {
                $descriptionEvenementsGrave->setRetourSejour(null);
            }
        }

        return $this;
    }

    public function getIncidentsMineurs(): ?string
    {
        return $this->incidentsMineurs;
    }

    public function setIncidentsMineurs(?string $incidentsMineurs): self
    {
        $this->incidentsMineurs = $incidentsMineurs;

        return $this;
    }
}
