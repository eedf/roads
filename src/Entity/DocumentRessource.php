<?php

namespace App\Entity;

use App\Repository\DocumentRessourceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Document ressource de ROADS
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */

#[ORM\Entity(repositoryClass: DocumentRessourceRepository::class)]
class DocumentRessource
{
    const DOC_MODELE_BUDGET = 'Modèle de budget prévisionnel';
    const DOC_TYPO_SEJOUR = 'Typologie des séjours';
    const DOC_DESC_CAMPS_BASE = 'Description camps de bases';
    const DOC_DESC_CAMPS_ACCOMPAGNE = 'Description camps accompagnés';
    const DOC_CHARTE_CAMP_JUMELE = 'Charte des camps jumelés';
    const DOC_CAHIER_DES_CHARGES_COLOS_APPRENANTES = 'Protocoles dispositif colos apprenantes';
    const DOC_MODELE_SOCLE_COMMUN_COLOS_APPRENANTES = 'Modèle socle commun colos apprenantes';
    const DOC_MODELE_PROJET_SEJOUR = 'Modele de projet de séjour';
    const DOC_MODELE_PROJET_PEDA = 'Modèle de projet pédagogique';
    const DOC_FICHE_REPERE_EVAL_SEJOUR = 'Fiche repère évaluation';
    const DOC_OUTIL_PEDA_EEDF = 'Outils pédagogiques EEDF';
    const DOC_EQUIVALENCE_BAFA_BAFD = 'Equivalence BAFA / BAFD';
    const DOC_EQUIVALENCE_PSC1 = 'Equivalence PSC1';
    const DOC_DEF_EVENEMENT_GRAVE = 'Définition évenement grave';
    const DOC_MODELE_COMPTE_RESULTAT = 'Modèle compte de résultat';
    const DOC_BILAN_INTERNATIONAL = 'Fiche repère bilan international';
    const DOC_CHECKLIST_CLASSEUR_DIRECTEUR = 'Check list du classeur directeur';
    const DOC_OUTIL_CONSTRUCTION_MENUS = 'Outil construction de menus';
    const DOC_MODELE_GRILLE_MENUS = 'Modèle de menus';
    const DOC_GUIDE_PLANIFICATION_SEJOUR = 'Guide de planification de séjour';
    const DOC_CONVENTION_LIEU = 'Convention de mise à disposition de terrain';
    const DOC_WIKI_ROADS = 'Wiki ROADS';
    const DOC_FORMULAIRE_CONSENTEMENT = 'Formulaire coordonnées encadrant non EEDF';
    const DOC_MODELE_VALIDATION_INTERNATIONALE_SEJOUR = 'Modèle Validation Internationale de séjour';
    const DOC_MODELE_VALIDATION_SEJOUR = 'Modèle Validation de séjour';
    const DOC_MODELE_VISITE_SEJOUR = 'Support de visite de camp';

    const LISTE_DOCUMENTS_RESSOURCES =
    [
        self::DOC_MODELE_BUDGET => "Proposition d'un outil de budget et comptabilité très complet",
        self::DOC_TYPO_SEJOUR => "Présentation des différents types de séjours pour se repérer",
        self::DOC_DESC_CAMPS_BASE => "Comprendre ce qu'est un camp de base et toutes les opportunités qui y sont offertes",
        self::DOC_DESC_CAMPS_ACCOMPAGNE => "Comprendre ce qu'est un camp accompagné et comment se faire accompagner",
        self::DOC_CHARTE_CAMP_JUMELE => "Pour bien penser et construire son camp de regroupement",
        self::DOC_CAHIER_DES_CHARGES_COLOS_APPRENANTES => "Pour comprendre ce qui est attendu dans ce dispositif",
        self::DOC_MODELE_SOCLE_COMMUN_COLOS_APPRENANTES => "A remplir pour bien préparer la participation à ce dispositif",
        self::DOC_FICHE_REPERE_EVAL_SEJOUR => "Des pistes pour bien évaluer son séjour",
        self::DOC_MODELE_PROJET_SEJOUR => "Proposition d'un modèle de dossier de présentation du projet de séjour.",
        self::DOC_MODELE_PROJET_PEDA => "Une trame de dossier, en 14 chapitres, vous permettant de renseigner tous les aspects pédagogiques et organisationnels de votre séjour. Son utilisation n’est pas obligatoire, et vous pouvez adapter son contenu en fonction de vos réalités de séjours.",
        self::DOC_OUTIL_PEDA_EEDF => "Retrouvez les différents outils et propositions pédagogiques EEDF",
        self::DOC_EQUIVALENCE_BAFA_BAFD => "Liste des diplômes de direction et d'animation ayant équivalence BAFA et BAFD",
        self::DOC_EQUIVALENCE_PSC1 => "Liste des diplômes ayant équivalence PSC1",
        self::DOC_DEF_EVENEMENT_GRAVE => "Retrouvez ici la définition d'un \"évenement grave\" par les services de l'état. Pour bien savoir quand il est nécessaire de faire une déclaration.",
        self::DOC_MODELE_COMPTE_RESULTAT => "Modèle pour faire remonter la comptabilité",
        self::DOC_BILAN_INTERNATIONAL => "Pour bien évaluer son séjour à l'international",
        self::DOC_CHECKLIST_CLASSEUR_DIRECTEUR => "La liste des documents obligatoires ou essentiels que chaque direct·eur·ice doit avoir sur son séjour.",
        self::DOC_OUTIL_CONSTRUCTION_MENUS => "Proposition d'un outil de construction de menus avec des recettes intégrées et le calcul des apports journaliers.",
        self::DOC_GUIDE_PLANIFICATION_SEJOUR => "Guide pour anticiper et préparer tous les aspects d'un séjour",
        self::DOC_CONVENTION_LIEU => "Modèle de convention pour établir les liens avec le.la propriétaire d'un terrain où se déroule une activité EEDF.",
        self::DOC_WIKI_ROADS => "Quelques questions/réponses pour débuter sur la plateforme EEDF ROADS.",
        self::DOC_FORMULAIRE_CONSENTEMENT => "Formulaire pour récupérer les coordonnées et le consentement d'un encadrant non EEDF mais membre d'une association du scoutisme français participant à un séjour EEDF",
        self::DOC_MODELE_VALIDATION_SEJOUR => "Proposition de critères de validation et de conseils pour les entretiens.",
        self::DOC_MODELE_VALIDATION_INTERNATIONALE_SEJOUR => "Document de l'avis de validation internationale",
        self::DOC_MODELE_GRILLE_MENUS => "Exemple de menus pour 15 jours",
        self::DOC_MODELE_VISITE_SEJOUR => "Document à remplir pour évaluer un camp lors d'une visite de camp."
    ];

    const TYPE_LIEN = 'Lien';
    const TYPE_FICHIER = 'Fichier';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Document $document = null;

    #[ORM\Column(length: 255)]
    private ?string $type = self::TYPE_LIEN;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lien = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocument(): ?Document
    {
        return $this->document;
    }

    public function setDocument(?Document $document): self
    {
        $this->document = $document;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(?string $lien): self
    {
        $this->lien = $lien;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
