<?php

namespace App\Entity;

use App\Repository\AspectInternationalSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Dimension internationale d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: AspectInternationalSejourRepository::class)]
class AspectInternationalSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'aspectInternational', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Sejour $sejour = null;

    #[ORM\Column]
    #[Groups(["fin_sejour"])]
    private ?bool $participationFormationInternationale = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateFormation = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $lieuFormation = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $referentFormation = null;


    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $partenaireInternational = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $financements = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $planRepli = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getParticipationFormationInternationale(): ?bool
    {
        return $this->participationFormationInternationale;
    }

    public function setParticipationFormationInternationale(bool $participationFormationInternationale): self
    {
        $this->participationFormationInternationale = $participationFormationInternationale;

        return $this;
    }

    public function getDateFormation(): ?\DateTimeInterface
    {
        return $this->dateFormation;
    }

    public function setDateFormation(?\DateTimeInterface $dateFormation): self
    {
        $this->dateFormation = $dateFormation;

        return $this;
    }

    public function getLieuFormation(): ?string
    {
        return $this->lieuFormation;
    }

    public function setLieuFormation(?string $lieuFormation): self
    {
        $this->lieuFormation = $lieuFormation;

        return $this;
    }

    public function getReferentFormation(): ?string
    {
        return $this->referentFormation;
    }

    public function setReferentFormation(?string $referentFormation): self
    {
        $this->referentFormation = $referentFormation;

        return $this;
    }

    public function getPartenaireInternational(): ?string
    {
        return $this->partenaireInternational;
    }

    public function setPartenaireInternational(?string $partenaireInternational): self
    {
        $this->partenaireInternational = $partenaireInternational;

        return $this;
    }

    public function getFinancements(): ?string
    {
        return $this->financements;
    }

    public function setFinancements(?string $financements): self
    {
        $this->financements = $financements;

        return $this;
    }

    public function getPlanRepli(): ?string
    {
        return $this->planRepli;
    }

    public function setPlanRepli(?string $planRepli): self
    {
        $this->planRepli = $planRepli;

        return $this;
    }
}
