<?php

namespace App\Entity;

use App\Repository\SejourRepository;
use App\Utils\PanelActifSejour;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as RoadsAssert;
use Doctrine\DBAL\Types\Types;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Séjour Organisé
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: SejourRepository::class)]
#[Assert\Callback(
    groups: ["envoi_pour_validation", "cloture_sejour"],
    callback: "validateEnvoiValidation"
)]
#[Assert\Callback(
    callback: "validateDateSejour"
)]
class Sejour
{
    const STATUT_SEJOUR_DECLARATION_INTENTION = 'declaration_intention_en_cours';
    const STATUT_SEJOUR_ATTENTE_CONFIRMATION_DIRECTEUR = 'attente_confirmation_directeur';
    const STATUT_SEJOUR_CONSTRUCTION_PROJET = 'construction_projet';
    const STATUT_SEJOUR_ATTENTE_VALIDATION = 'attente_validation';
    const STATUT_SEJOUR_VALIDE = 'projet_valide';
    const STATUT_SEJOUR_SUSPENDU = 'sejour_suspendu';
    const STATUT_SEJOUR_ANNULE = 'sejour_annule';
    const STATUT_SEJOUR_EN_COURS = 'sejour_en_cours';
    const STATUT_SEJOUR_TERMINE = 'sejour_termine';
    const STATUT_SEJOUR_CLOTURE = 'sejour_cloture';

    const LISTES_STATUTS_SEJOURS_VISIBLES_VALIDATION = [
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET,
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
        self::STATUT_SEJOUR_SUSPENDU,
        self::STATUT_SEJOUR_TERMINE,
        self::STATUT_SEJOUR_CLOTURE
    ];

    const LISTES_STATUTS_SEJOURS_PROJET_MODIFIABLE = [
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET,
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS
    ];

    const LISTES_STATUTS_SEJOURS_VALIDATION = [
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
    ];

    const LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI = [
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET,
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
        self::STATUT_SEJOUR_SUSPENDU,
        self::STATUT_SEJOUR_TERMINE
    ];

    const LISTES_STATUTS_SEJOURS_VISITABLES = [
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
        self::STATUT_SEJOUR_TERMINE
    ];

    const LISTES_STATUTS_SEJOURS_VISIBLES_ORGANISATION = [
        self::STATUT_SEJOUR_DECLARATION_INTENTION,
        self::STATUT_SEJOUR_ATTENTE_CONFIRMATION_DIRECTEUR,
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET,
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
        self::STATUT_SEJOUR_SUSPENDU,
        self::STATUT_SEJOUR_TERMINE
    ];

    const LISTES_STATUTS_SEJOURS_SUSPENDABLES = [
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET,
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS
    ];

    const LISTES_STATUTS_SEJOURS_EXPORTABLES = [
        self::STATUT_SEJOUR_ATTENTE_VALIDATION,
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
        self::STATUT_SEJOUR_TERMINE
    ];

    const LISTES_STATUTS_SEJOURS_VERIFIER_MODIF_IMPORTANTE = [
        self::STATUT_SEJOUR_VALIDE,
        self::STATUT_SEJOUR_EN_COURS,
    ];

    const MAP_LABEL_PAR_STATUT_SEJOUR = [
        self::STATUT_SEJOUR_DECLARATION_INTENTION => 'En déclaration d\'intention',
        self::STATUT_SEJOUR_ATTENTE_CONFIRMATION_DIRECTEUR => 'En attente de confirmation du·de la direct·eur·rice',
        self::STATUT_SEJOUR_CONSTRUCTION_PROJET => 'En construction',
        self::STATUT_SEJOUR_ATTENTE_VALIDATION => 'En attente de validation',
        self::STATUT_SEJOUR_VALIDE => 'Validé',
        self::STATUT_SEJOUR_SUSPENDU => 'Suspendu',
        self::STATUT_SEJOUR_ANNULE => 'Annulé',
        self::STATUT_SEJOUR_EN_COURS => 'En cours',
        self::STATUT_SEJOUR_TERMINE => 'Terminé',
        self::STATUT_SEJOUR_CLOTURE => 'Clôturé',
    ];



    const TYPE_SEJOUR_UNITE = "d'unité";
    const TYPE_SEJOUR_GROUPE = "de groupe";
    const TYPE_SEJOUR_RASSEMBLEMENT = "rassemblement";
    const TYPE_SEJOUR_REGROUPEMENT = "de regroupement";
    const TYPE_SEJOUR_SV = "SV";

    const DISPOSITIF_APPUI_CAMP_ACCOMPAGNE = "Camp accompagné";
    const DISPOSITIF_APPUI_CAMP_BASE = "Camp de base";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'sejoursDirection')]
    #[ORM\JoinTable(name: "sejour_direction")]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\NotNull(
        message: "Le·la direct·eur·rice doit être renseigné·e",
        groups: ["envoi_pour_validation", "publication_declaration_intention", "cloture_sejour"]

    )]
    private ?Personne $directeur = null;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "sejoursDirectionAdjointe")]
    #[ORM\JoinTable(name: "sejour_direction_adjointe")]
    private Collection $directeursAdjoints;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "sejoursEncadrement")]
    #[ORM\JoinTable(name: "sejour_encadrement")]
    private Collection $encadrants;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "sejoursSuivis")]
    #[ORM\JoinTable(name: "sejour_suivi")]
    private Collection $suiveurs;

    #[ORM\Column(length: 200)]
    #[Assert\NotEqualTo(
        "Nouveau séjour",
        message: "Le titre du séjour doit être différent de {{ compared_value }}",
        groups: ["envoi_pour_validation", "publication_declaration_intention", "cloture_sejour"]
    )]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $intitule = null;

    #[ORM\ManyToOne(inversedBy: 'sejoursOrganises')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Structure $structureOrganisatrice = null;

    #[ORM\Column(type: Types::ARRAY)]
    private ?array $status = [];

    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\NotNull(
        message: "Il faut renseigner si le séjour est international ou non",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    private ?bool $international = null;

    #[Assert\NotNull(
        message: "Le type de séjour doit être renseigné",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[ORM\Column(length: 50, nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?string $typeSejour = null;

    #[Assert\NotNull(
        message: "Il faut renseigner si le séjour est ouvert ou non",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $sejourOuvert = null;

    #[Assert\NotNull(
        message: "Il faut renseigner si le séjour est itinérant ou non",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $sejourItinerant = null;

    #[Assert\NotNull(
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"],
        message: "Il faut renseigner si le séjour accueille un groupe étranger ou non",
    )]
    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $sejourFranceAvecAccueilEtranger = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?bool $coloApprenante = null;

    #[Assert\NotNull(
        message: "Il faut renseigner si le séjour bénéficie d'un dispositif d'appui ou non",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[ORM\Column(length: 50, nullable: true)]
    private ?string $dispositifAppui = null;


    #[ORM\ManyToOne(inversedBy: 'sejoursAccompagnement')]
    #[Assert\Valid()]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?Personne $accompagnant = null;


    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\Expression(
        "(this.getDureeSejour() >= 4 )",
        message: "La durée du séjour doit être d'au moins 5 jours",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDebut = null;

    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\Expression(
        "(this.getDureeSejour() < 30 )",
        message: "La durée du séjour ne peut être supérieure à 30 jours",
        groups: ["cloture_sejour", "envoi_pour_validation", "publication_declaration_intention"]
    )]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\OneToOne(inversedBy: 'sejour', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(["save_declaration_intention"])]
    #[Assert\Valid()]
    private ?EffectifsPrevisionnels $effectifsPrevisionnels = null;

    #[Groups(["save_declaration_intention"])]
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateModification = null;


    #[Assert\Valid()]
    #[RoadsAssert\UniqueInCollection(
        fields: [
            "structureEEDF",
            "nomStructureNonEEDF"
        ],
        message: "La structure de regroupement est déjà présente sur ce séjour"
    )]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[ORM\OneToMany(mappedBy: 'sejour', targetEntity: StructureRegroupementSejour::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    private Collection $structuresRegroupees;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $infosDeclaration = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToMany(mappedBy: 'sejour', targetEntity: RoleEncadrantSejour::class, cascade: ["persist", "remove"], orphanRemoval: true)]
    private Collection $rolesEncadrants;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?AspectCommunicationSejour $aspectCommunicationSejour = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    #[Assert\NotNull(groups: ["envoi_pour_validation"], message: "Les aspects budgétaires doivent être renseignés")]
    #[Assert\Valid()]
    private ?AspectBudgetSejour $aspectBudgetSejour = null;


    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?AspectAlimentaireSejour $aspectAlimentaireSejour = null;

    #[Groups(["fin_sejour"])]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $descriptionOrganisationTransport = null;


    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?AspectInternationalSejour $aspectInternational = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Assert\NotNull(groups: ["envoi_pour_validation"], message: "Le projet pédagogique doit être chargé")]
    #[Assert\Valid()]
    private ?Document $documentProjetPeda = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Assert\Valid()]
    private ?Document $documentSocleCommunColosApprenantes = null;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToMany(mappedBy: 'sejourOrganisateur', targetEntity: ParticipationSejourUnite::class, cascade: ["persist", "remove"])]
    #[Assert\Valid()]
    private Collection $unitesStructureOrganisatrice;

    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?AspectsParticipantsBesoinsSpecifiquesSejour $aspectsParticipantsBesoinsSpecifiquesSejour = null;


    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    #[Assert\Valid()]
    #[Assert\NotNull(groups: ["cloture_sejour", "envoi_pour_validation"], message: "Les qualifications et diplômes du·de la direct·eur·rice doivent être renseignées.")]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    private ?QualificationDirection $qualificationDirection = null;


    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?ValidationSejour $validationSejour = null;


    #[Groups(["fin_sejour"])]
    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    private ?ValidationInternationaleSejour $validationInternationaleSejour = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $infosSejour = null;


    #[ORM\OneToOne(mappedBy: 'sejour', cascade: ['persist', 'remove'])]
    #[Assert\NotNull(groups: ["cloture_sejour"], message: "Les retours généraux du séjour doivent être renseignés")]
    #[Assert\Valid()]
    private ?RetourSejour $retourSejour = null;

    #[Assert\Valid()]
    #[Assert\NotNull(groups: ["cloture_sejour"], message: "Le compte de résultat doit être chargé")]
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Document $compteResultat = null;


    #[Assert\Valid()]
    #[
        Assert\Expression(
            "((this.getInternational() || this.getSejourFranceAvecAccueilEtranger()) && this.getRetourCampInternational()) || (!this.getInternational() && !this.getSejourFranceAvecAccueilEtranger())",
            message: "Le document de bilan international doit être renseigné",
            groups: ["cloture_sejour"]
        )
    ]
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Document $retourCampInternational = null;

    #[Assert\NotNull(groups: ["cloture_sejour"], message: "Le bilan pédagogique doit être renseigné")]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $bilanPeda = null;


    private PanelActifSejour $panelActif;

    #[ORM\OneToMany(mappedBy: 'sejour', targetEntity: Commentaire::class, orphanRemoval: true)]
    private Collection $commentaires;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateEnvoiValidation = null;


    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDepotDeclarationIntention = null;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: "sejoursValidation")]
    private Collection $validateurs;

    #[Assert\Valid()]
    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Document $ficheComplementaire = null;

    #[ORM\OneToMany(mappedBy: 'sejour', targetEntity: NominationVisiteSejour::class, orphanRemoval: true)]
    private Collection  $nominationsVisiteSejour;

    #[Groups(["save_declaration_intention", "fin_sejour"])]
    #[Assert\Valid()]
    #[Assert\Count(
        min: 1,
        minMessage: "Un lieu au moins doit être renseigné",
        groups: ["cloture_sejour", "envoi_pour_validation"]
    )]
    #[ORM\OneToMany(mappedBy: 'sejour', targetEntity: UtilisationLieuSejour::class, orphanRemoval: true, cascade: ["persist", "remove"])]
    private Collection  $utilisationsLieuSejour;



    public function __construct()
    {
        $this->directeursAdjoints = new ArrayCollection();
        $this->encadrants = new ArrayCollection();
        $this->suiveurs = new ArrayCollection();
        $this->structuresRegroupees = new ArrayCollection();
        $this->rolesEncadrants = new ArrayCollection();
        $this->unitesStructureOrganisatrice = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->dateCreation = new DateTime();
        $this->validateurs = new ArrayCollection();
        $this->nominationsVisiteSejour = new ArrayCollection();
        $this->utilisationsLieuSejour = new ArrayCollection();
    }

    public function getDureeSejour(): int
    {
        return $this->dateDebut->diff($this->dateFin)->format('%a');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDirecteur(): ?Personne
    {
        return $this->directeur;
    }

    public function setDirecteur(?Personne $directeur): self
    {
        $this->directeur = $directeur;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getDirecteursAdjoints(): Collection
    {
        return $this->directeursAdjoints;
    }

    public function addDirecteursAdjoint(Personne $directeursAdjoint): self
    {
        if (!$this->directeursAdjoints->contains($directeursAdjoint)) {
            $this->directeursAdjoints[] = $directeursAdjoint;
        }

        return $this;
    }

    public function removeDirecteursAdjoint(Personne $directeursAdjoint): self
    {
        $this->directeursAdjoints->removeElement($directeursAdjoint);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getEncadrants(): Collection
    {
        return $this->encadrants;
    }

    public function addEncadrant(Personne $encadrant): self
    {
        if (!$this->encadrants->contains($encadrant)) {
            $this->encadrants[] = $encadrant;
        }

        return $this;
    }

    public function removeEncadrant(Personne $encadrant): self
    {
        $this->encadrants->removeElement($encadrant);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getSuiveurs(): Collection
    {
        return $this->suiveurs;
    }

    public function addSuiveur(Personne $suiveur): self
    {
        if (!$this->suiveurs->contains($suiveur)) {
            $this->suiveurs[] = $suiveur;
        }

        return $this;
    }

    public function removeSuiveur(Personne $suiveur): self
    {
        $this->suiveurs->removeElement($suiveur);

        return $this;
    }

    public function getintitule(): ?string
    {
        return $this->intitule;
    }

    public function setintitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getStructureOrganisatrice(): ?Structure
    {
        return $this->structureOrganisatrice;
    }

    public function setStructureOrganisatrice(?Structure $structureOrganisatrice): self
    {
        $this->structureOrganisatrice = $structureOrganisatrice;

        return $this;
    }

    public function getStatus(): ?array
    {
        return $this->status;
    }

    public function setStatus(array $status): void
    {
        $this->status = $status;
    }

    public function isStatusVisible(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI)) {
                return true;
            }
        }
        return false;
    }

    public function isValidationVisible(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_VISIBLES_VALIDATION)) {
                return true;
            }
        }
        return false;
    }

    public function isProjetModifiable(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_PROJET_MODIFIABLE)) {
                return true;
            }
        }
        return false;
    }

    public function isValidable(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_VALIDATION)) {
                return true;
            }
        }
        return false;
    }

    public function isVisitable(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_VISITABLES)) {
                return true;
            }
        }
        return false;
    }

    public function isSuspendable(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_SUSPENDABLES)) {
                return true;
            }
        }
        return false;
    }

    public function isExportable(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_EXPORTABLES)) {
                return true;
            }
        }
        return false;
    }

    public function isVerifierModifImportante(): ?bool
    {
        foreach ($this->status as $etat => $index) {
            if (in_array($etat, self::LISTES_STATUTS_SEJOURS_VERIFIER_MODIF_IMPORTANTE)) {
                return true;
            }
        }
        return false;
    }

    public function getInternational(): ?bool
    {
        return $this->international;
    }

    public function setInternational(bool $international): self
    {
        $this->international = $international;

        return $this;
    }

    public function getTypeSejour(): ?string
    {
        return $this->typeSejour;
    }

    public function setTypeSejour(?string $typeSejour): self
    {
        $this->typeSejour = $typeSejour;

        return $this;
    }

    public function getSejourOuvert(): ?bool
    {
        return $this->sejourOuvert;
    }

    public function setSejourOuvert(?bool $sejourOuvert): self
    {
        $this->sejourOuvert = $sejourOuvert;

        return $this;
    }

    public function getSejourItinerant(): ?bool
    {
        return $this->sejourItinerant;
    }

    public function setSejourItinerant(?bool $sejourItinerant): self
    {
        $this->sejourItinerant = $sejourItinerant;

        return $this;
    }

    public function getSejourFranceAvecAccueilEtranger(): ?bool
    {
        return $this->sejourFranceAvecAccueilEtranger;
    }

    public function setSejourFranceAvecAccueilEtranger(?bool $sejourFranceAvecAccueilEtranger): self
    {
        $this->sejourFranceAvecAccueilEtranger = $sejourFranceAvecAccueilEtranger;

        return $this;
    }

    public function getColoApprenante(): ?bool
    {
        return $this->coloApprenante;
    }

    public function setColoApprenante(?bool $coloApprenante): self
    {
        $this->coloApprenante = $coloApprenante;

        return $this;
    }

    public function getDispositifAppui(): ?string
    {
        return $this->dispositifAppui;
    }

    public function setDispositifAppui(?string $dispositifAppui): self
    {
        $this->dispositifAppui = $dispositifAppui;

        return $this;
    }

    public function getAccompagnant(): ?Personne
    {
        return $this->accompagnant;
    }

    public function setAccompagnant(?Personne $accompagnant): self
    {
        $this->accompagnant = $accompagnant;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;
        //S'il  n'ya qu'un lieu, on positionne la date d'arrivée sur le lieu sur la date de début du séjour
        if ($this->utilisationsLieuSejour->count() == 1) {
            foreach ($this->utilisationsLieuSejour as $lieu) {
                $lieu->setDateArrivee($dateDebut);
            }
        }
        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;
        //S'il  n'ya qu'un lieu, on positionne la date de départ du lieu sur la date de fin du séjour
        if ($this->utilisationsLieuSejour->count() == 1) {
            foreach ($this->utilisationsLieuSejour as $lieu) {
                $lieu->setDateDepart($dateFin);
            }
        }

        return $this;
    }

    public function getEffectifsPrevisionnels(): ?EffectifsPrevisionnels
    {
        return $this->effectifsPrevisionnels;
    }

    public function setEffectifsPrevisionnels(EffectifsPrevisionnels $effectifsPrevisionnels): self
    {
        $this->effectifsPrevisionnels = $effectifsPrevisionnels;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * @return Collection|StructureRegroupementSejour[]
     */
    public function getStructuresRegroupees(): Collection
    {
        return $this->structuresRegroupees;
    }

    public function addStructuresRegroupee(StructureRegroupementSejour $structuresRegroupee): self
    {
        if (!$this->structuresRegroupees->contains($structuresRegroupee)) {
            $this->structuresRegroupees[] = $structuresRegroupee;
            $structuresRegroupee->setSejour($this);
        }

        return $this;
    }

    public function removeStructuresRegroupee(StructureRegroupementSejour $structuresRegroupee): self
    {
        if ($this->structuresRegroupees->removeElement($structuresRegroupee)) {
            // set the owning side to null (unless already changed)
            if ($structuresRegroupee->getSejour() === $this) {
                $structuresRegroupee->setSejour(null);
            }
        }

        return $this;
    }

    public function getInfosDeclaration(): ?string
    {
        return $this->infosDeclaration;
    }

    public function setInfosDeclaration(?string $infosDeclaration): self
    {
        $this->infosDeclaration = $infosDeclaration;

        return $this;
    }

    /**
     * @return Collection|RoleEncadrantSejour[]
     */
    public function getRolesEncadrants(): Collection
    {
        return $this->rolesEncadrants;
    }

    public function addRolesEncadrant(RoleEncadrantSejour $rolesEncadrant): self
    {
        if (!$this->rolesEncadrants->contains($rolesEncadrant)) {
            $this->rolesEncadrants[] = $rolesEncadrant;
            $rolesEncadrant->setSejour($this);
        }

        return $this;
    }

    public function removeRolesEncadrant(RoleEncadrantSejour $rolesEncadrant): self
    {
        if ($this->rolesEncadrants->removeElement($rolesEncadrant)) {
            // set the owning side to null (unless already changed)
            if ($rolesEncadrant->getSejour() === $this) {
                $rolesEncadrant->setSejour(null);
            }
        }

        return $this;
    }

    //permet de récupérer la liste des structures regroupées participant au séjour, c'est à dire toutes les structures ayant accepté le regroupement
    public function getStructuresRegroupeesParticipantes(): Collection
    {
        $result = new ArrayCollection();

        foreach ($this->getStructuresRegroupees() as $structureRegroupee) {
            //On ajoute uniquement les structures qui ont accepté la demande de regroupement
            if ($structureRegroupee->getStatutDemande() == StructureRegroupementSejour::STATUT_DEMANDE_ACCEPTE) {
                $result->add($structureRegroupee);
            }
        }

        return $result;
    }

    public function getAspectCommunicationSejour(): ?AspectCommunicationSejour
    {
        return $this->aspectCommunicationSejour;
    }

    public function setAspectCommunicationSejour(AspectCommunicationSejour $aspectCommunicationSejour): self
    {
        // set the owning side of the relation if necessary
        if ($aspectCommunicationSejour->getSejour() !== $this) {
            $aspectCommunicationSejour->setSejour($this);
        }

        $this->aspectCommunicationSejour = $aspectCommunicationSejour;

        return $this;
    }

    public function getAspectBudgetSejour(): ?AspectBudgetSejour
    {
        return $this->aspectBudgetSejour;
    }

    public function setAspectBudgetSejour(AspectBudgetSejour $aspectBudgetSejour): self
    {
        // set the owning side of the relation if necessary
        if ($aspectBudgetSejour->getSejour() !== $this) {
            $aspectBudgetSejour->setSejour($this);
        }

        $this->aspectBudgetSejour = $aspectBudgetSejour;

        return $this;
    }

    public function getAspectAlimentaireSejour(): ?AspectAlimentaireSejour
    {
        return $this->aspectAlimentaireSejour;
    }

    public function setAspectAlimentaireSejour(AspectAlimentaireSejour $aspectAlimentaireSejour): self
    {
        // set the owning side of the relation if necessary
        if ($aspectAlimentaireSejour->getSejour() !== $this) {
            $aspectAlimentaireSejour->setSejour($this);
        }

        $this->aspectAlimentaireSejour = $aspectAlimentaireSejour;

        return $this;
    }

    public function getDescriptionOrganisationTransport(): ?string
    {
        return $this->descriptionOrganisationTransport;
    }

    public function setDescriptionOrganisationTransport(?string $descriptionOrganisationTransport): self
    {
        $this->descriptionOrganisationTransport = $descriptionOrganisationTransport;

        return $this;
    }

    public function getAspectInternational(): ?AspectInternationalSejour
    {
        return $this->aspectInternational;
    }

    public function setAspectInternational(AspectInternationalSejour $aspectInternational): self
    {
        // set the owning side of the relation if necessary
        if ($aspectInternational->getSejour() !== $this) {
            $aspectInternational->setSejour($this);
        }

        $this->aspectInternational = $aspectInternational;

        return $this;
    }

    public function getDocumentProjetPeda(): ?Document
    {
        return $this->documentProjetPeda;
    }

    public function setDocumentProjetPeda(?Document $documentProjetPeda): self
    {
        $this->documentProjetPeda = $documentProjetPeda;

        return $this;
    }

    public function getDocumentSocleCommunColosApprenantes(): ?Document
    {
        return $this->documentSocleCommunColosApprenantes;
    }

    public function setDocumentSocleCommunColosApprenantes(?Document $documentSocleCommunColosApprenantes): self
    {
        $this->documentSocleCommunColosApprenantes = $documentSocleCommunColosApprenantes;

        return $this;
    }

    /**
     * @return Collection|ParticipationSejourUnite[]
     */
    public function getUnitesStructureOrganisatrice(): Collection
    {
        return $this->unitesStructureOrganisatrice;
    }

    public function addUnitesStructureOrganisatrice(ParticipationSejourUnite $unitesStructureOrganisatrice): self
    {
        if (!$this->unitesStructureOrganisatrice->contains($unitesStructureOrganisatrice)) {
            $this->unitesStructureOrganisatrice[] = $unitesStructureOrganisatrice;
            $unitesStructureOrganisatrice->setSejourOrganisateur($this);
        }

        return $this;
    }

    public function removeUnitesStructureOrganisatrice(ParticipationSejourUnite $unitesStructureOrganisatrice): self
    {
        if ($this->unitesStructureOrganisatrice->removeElement($unitesStructureOrganisatrice)) {
            // set the owning side to null (unless already changed)
            if ($unitesStructureOrganisatrice->getSejourOrganisateur() === $this) {
                $unitesStructureOrganisatrice->setSejourOrganisateur(null);
            }
        }

        return $this;
    }

    public function getAspectsParticipantsBesoinsSpecifiquesSejour(): ?AspectsParticipantsBesoinsSpecifiquesSejour
    {
        return $this->aspectsParticipantsBesoinsSpecifiquesSejour;
    }

    public function setAspectsParticipantsBesoinsSpecifiquesSejour(AspectsParticipantsBesoinsSpecifiquesSejour $aspectsParticipantsBesoinsSpecifiquesSejour): self
    {
        // set the owning side of the relation if necessary
        if ($aspectsParticipantsBesoinsSpecifiquesSejour->getSejour() !== $this) {
            $aspectsParticipantsBesoinsSpecifiquesSejour->setSejour($this);
        }

        $this->aspectsParticipantsBesoinsSpecifiquesSejour = $aspectsParticipantsBesoinsSpecifiquesSejour;

        return $this;
    }

    public function getQualificationDirection(): ?QualificationDirection
    {
        return $this->qualificationDirection;
    }

    public function setQualificationDirection(QualificationDirection $qualificationDirection): self
    {
        // set the owning side of the relation if necessary
        if ($qualificationDirection->getSejour() !== $this) {
            $qualificationDirection->setSejour($this);
        }

        $this->qualificationDirection = $qualificationDirection;

        return $this;
    }

    /**
     * Call back de validation de l'entité lors de l'envoi en validation (déclenche une erreur si les contraintes ne sont pas respectées):
     * - Le séjour ne peut avoir plus d'un lieu s'il est itiérant
     * - Le séjour doit avoir au moins un lieu défini
     * - Si c'est un séjour d'unité, il ne peut y avoir qu'une unité participante
     * - Pour chacun des lieux: 
     *  L'adresse doit être définie
     *  La date d'arrivée sur le lieu ne doit pas être avant la date de début de séjour
     *  La date d'arrivée sur le lieu ne doit pas être avant la date de départ du lieu
     *  La date de départ du lieu ne doit pas être après la date de fin de séjour
     * - Un·e encadrant·e ne peut pas être déclaré·e deux fois
     * - Une structure de regroupement, quelle soit EEDF ou non ne peut pas être déclarée deux fois
     * - Le nombre de participant·e·s au séjour doit être supérieur à 0.
     * @param  mixed $context
     * @param  mixed $payload
     * @return void
     */
    public function validateEnvoiValidation(ExecutionContextInterface $context, $payload)
    {
        //Un seul lieu autorisé si pas séjour itinérant
        if (!$this->sejourItinerant && $this->utilisationsLieuSejour->count() > 1) {
            $context->buildViolation("Le séjour n'est pas itinérant, il ne peut se dérouler que sur un seul lieu")
                ->addViolation();
        }
        //Un lieu au minimum
        if ($this->utilisationsLieuSejour->count() == 0) {
            $context->buildViolation("Aucun lieu n'est défini pour ce séjour")
                ->addViolation();
        }

        //Si séjour unité, une seule unité participante
        if (
            ($this->typeSejour == self::TYPE_SEJOUR_UNITE)
            && $this->unitesStructureOrganisatrice->count() > 1
        ) {
            $context->buildViolation("Le séjour est un séjour d'unité, il ne peut se dérouler qu'avec une seule unité")
                ->atPath("typeSejour")
                ->addViolation();
        }

        //Dates des lieux de séjour
        /** @var UtilisationLieuSejour $lieu */
        foreach ($this->utilisationsLieuSejour as $lieu) {
            if ($lieu->getLieu()->getLocalisation()->getAdresse() == null) {
                $context->buildViolation("L'adresse du lieu \"" . $lieu->getLieu()->getNom() . "\" doit être renseignée.")
                    ->atPath("utilisationsLieuSejour")
                    ->addViolation();
            }
            if ($lieu->getDateArrivee() < $this->dateDebut) {
                $context->buildViolation("La date d'arrivée sur le lieu \"" . $lieu->getLieu()->getNom() . "\" ne doit pas être avant la date de début de séjour")
                    ->atPath("utilisationsLieuSejour")
                    ->addViolation();
            }
            if ($lieu->getDateArrivee() > $lieu->getDateDepart()) {
                $context->buildViolation("La date d'arrivée sur le lieu \"" . $lieu->getLieu()->getNom() . "\" doit être avant la date de départ du lieu")
                    ->atPath("utilisationsLieuSejour")
                    ->addViolation();
            }
            if ($lieu->getDateDepart() > $this->dateFin) {
                $context->buildViolation("La date de départ du lieu \"" . $lieu->getLieu()->getNom() . "\" ne doit pas être après la date de fin du séjour")
                    ->atPath("utilisationsLieuSejour")
                    ->addViolation();
            }
        }

        //Encadrants EEDF et non EEDF uniques
        $existingIdEncadrantsEEDF = [$this->directeur->getId()];
        $existingEncadrantsNonEEDF = [];
        /** @var RoleEncadrantSejour $roleEncadrant */
        foreach ($this->rolesEncadrants as $roleEncadrant) {
            if ($roleEncadrant->getEstEncadrantEEDF()) {
                if (in_array(
                    $roleEncadrant->getEncadrantEEDF()->getId(),
                    $existingIdEncadrantsEEDF
                )) {
                    $context->buildViolation("L'encadrant·e " . $roleEncadrant->getEncadrantEEDF()->getPrenom() . ' ' . $roleEncadrant->getEncadrantEEDF()->getNom() . " est déclaré deux fois sur ce séjour.")
                        ->addViolation();
                } else {
                    $existingIdEncadrantsEEDF[] = $roleEncadrant->getEncadrantEEDF()->getId();
                }
            } else {
                $cle = $roleEncadrant->getInfosEncadrantNonEEDF()->getNom() . $roleEncadrant->getInfosEncadrantNonEEDF()->getPrenom();
                if (in_array(
                    $cle,
                    $existingEncadrantsNonEEDF
                )) {
                    $context->buildViolation("L'encadrant·e " . $roleEncadrant->getInfosEncadrantNonEEDF()->getPrenom() . ' ' . $roleEncadrant->getInfosEncadrantNonEEDF()->getNom() . " est déclaré deux fois sur ce séjour.")
                        ->addViolation();
                } else {
                    $existingEncadrantsNonEEDF[] = $cle;
                }
            }
        }

        //Structures regroupees EEDF et non EEDF uniques
        $existingIdStructureEEDF = [$this->structureOrganisatrice->getId()];
        $existingStructuresNonEEDF = [];
        /** @var StructureRegroupementSejour $structureRegroupementSejour */
        foreach ($this->structuresRegroupees as $structureRegroupementSejour) {
            if ($structureRegroupementSejour->getEstEEDF()) {
                if (in_array(
                    $structureRegroupementSejour->getStructureEEDF()->getId(),
                    $existingIdStructureEEDF
                )) {
                    $context->buildViolation("La structure " .  $structureRegroupementSejour->getStructureEEDF()->getNom() . " est déclarée deux fois sur ce séjour.")
                        ->addViolation();
                } else {
                    $existingIdStructureEEDF[] = $structureRegroupementSejour->getStructureEEDF()->getId();
                }
            } else {
                $cle = $structureRegroupementSejour->getNomStructureNonEEDF();
                if (in_array(
                    $cle,
                    $existingStructuresNonEEDF
                )) {
                    $context->buildViolation("La structure " . $cle . " est déclarée deux fois sur ce séjour.")
                        ->addViolation();
                } else {
                    $existingStructuresNonEEDF[] = $cle;
                }
            }
        }

        $nb_participants = 0;
        /** @var ParticipationSejourUnite $uniteStructureOrganisatrice */
        foreach ($this->unitesStructureOrganisatrice as $uniteStructureOrganisatrice) {
            $nb_participants += $uniteStructureOrganisatrice->getParticipantsAdherents()->count();
            $nb_participants += $uniteStructureOrganisatrice->getParticipantsNonAdherents()->count();
        }

        /** @var StructureRegroupementSejour $structureRegroupee */
        foreach ($this->getStructuresRegroupees() as $structureRegroupee) {
            if ($structureRegroupee->getEstEEDF()) {
                /** @var ParticipationSejourUnite $uniteParticipante */
                foreach ($structureRegroupee->getUnitesParticipantes() as $uniteParticipante) {
                    $nb_participants += $uniteParticipante->getParticipantsAdherents()->count();
                    $nb_participants += $uniteParticipante->getParticipantsNonAdherents()->count();
                }
            } else {
                $nb_participants += $structureRegroupee->getNbEnfantMoins14StructNonEEDF();
                $nb_participants += $structureRegroupee->getNbEnfantsPlus14StructNonEEDF();
            }
        }
        if ($nb_participants == 0) {
            $context->buildViolation("Le nombre de participant·e·s au séjour est de 0.")
                ->addViolation();
        }
    }

    /**
     * Call back de validation de l'entité lors sur les dates (déclenche une erreur si les contraintes ne sont pas respectées):
     * - La date de début du séjour doit être avant la date de fin
     * @param  mixed $context
     * @param  mixed $payload
     * @return void
     */
    public function validateDateSejour(ExecutionContextInterface $context, $payload)
    {
        if ($this->dateDebut > $this->dateFin) {
            $context->buildViolation("La date de début du séjour doit être avant la date de fin")
                ->atPath("dateDebut")
                ->addViolation();
        }
    }


    public function getValidationSejour(): ?ValidationSejour
    {
        return $this->validationSejour;
    }

    public function setValidationSejour(ValidationSejour $validationSejour): self
    {
        // set the owning side of the relation if necessary
        if ($validationSejour->getSejour() !== $this) {
            $validationSejour->setSejour($this);
        }

        $this->validationSejour = $validationSejour;

        return $this;
    }

    public function getValidationInternationaleSejour(): ?ValidationInternationaleSejour
    {
        return $this->validationInternationaleSejour;
    }

    public function setValidationInternationaleSejour(ValidationInternationaleSejour $validationInternationaleSejour): self
    {
        // set the owning side of the relation if necessary
        if ($validationInternationaleSejour->getSejour() !== $this) {
            $validationInternationaleSejour->setSejour($this);
        }

        $this->validationInternationaleSejour = $validationInternationaleSejour;

        return $this;
    }

    public function getInfosSejour(): ?string
    {
        return $this->infosSejour;
    }

    public function setInfosSejour(?string $infosSejour): self
    {
        $this->infosSejour = $infosSejour;

        return $this;
    }

    public function getRetourSejour(): ?RetourSejour
    {
        return $this->retourSejour;
    }

    public function setRetourSejour(RetourSejour $retourSejour): self
    {
        // set the owning side of the relation if necessary
        if ($retourSejour->getSejour() !== $this) {
            $retourSejour->setSejour($this);
        }

        $this->retourSejour = $retourSejour;

        return $this;
    }

    public function getCompteResultat(): ?Document
    {
        return $this->compteResultat;
    }

    public function setCompteResultat(?Document $compteResultat): self
    {
        $this->compteResultat = $compteResultat;

        return $this;
    }

    public function getRetourCampInternational(): ?Document
    {
        return $this->retourCampInternational;
    }

    public function setRetourCampInternational(?Document $retourCampInternational): self
    {
        $this->retourCampInternational = $retourCampInternational;

        return $this;
    }

    public function getBilanPeda(): ?string
    {
        return $this->bilanPeda;
    }

    public function setBilanPeda(?string $bilanPeda): self
    {
        $this->bilanPeda = $bilanPeda;

        return $this;
    }

    /**
     * Get the value of panelActif
     */
    public function getPanelActif(): PanelActifSejour
    {
        return $this->panelActif;
    }

    /**
     * Set the value of panelActif
     *
     * @return  self
     */
    public function setPanelActif(PanelActifSejour $panelActif)
    {
        $this->panelActif = $panelActif;

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setSejour($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getSejour() === $this) {
                $commentaire->setSejour(null);
            }
        }

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateEnvoiValidation(): ?\DateTimeInterface
    {
        return $this->dateEnvoiValidation;
    }

    public function setDateEnvoiValidation(?\DateTimeInterface $dateEnvoiValidation): self
    {
        $this->dateEnvoiValidation = $dateEnvoiValidation;

        return $this;
    }

    public function getDateDepotDeclarationIntention(): ?\DateTimeInterface
    {
        return $this->dateDepotDeclarationIntention;
    }

    public function setDateDepotDeclarationIntention(?\DateTimeInterface $dateDepotDeclarationIntention): self
    {
        $this->dateDepotDeclarationIntention = $dateDepotDeclarationIntention;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getValidateurs(): Collection
    {
        return $this->validateurs;
    }

    public function addValidateur(Personne $validateur): self
    {
        if (!$this->validateurs->contains($validateur)) {
            $this->validateurs[] = $validateur;
        }

        return $this;
    }

    public function removeValidateur(Personne $validateur): self
    {
        $this->validateurs->removeElement($validateur);

        return $this;
    }

    public function getFicheComplementaire(): ?Document
    {
        return $this->ficheComplementaire;
    }

    public function setFicheComplementaire(?Document $ficheComplementaire): self
    {
        $this->ficheComplementaire = $ficheComplementaire;
        return $this;
    }

    /**
     * @return Collection<int, NominationVisiteSejour>
     */
    public function getNominationsVisiteSejour(): Collection
    {
        return $this->nominationsVisiteSejour;
    }

    public function addNominationsVisiteSejour(NominationVisiteSejour $nominationsVisiteSejour): self
    {
        if (!$this->nominationsVisiteSejour->contains($nominationsVisiteSejour)) {
            $this->nominationsVisiteSejour[] = $nominationsVisiteSejour;
            $nominationsVisiteSejour->setSejour($this);
        }

        return $this;
    }

    public function removeNominationsVisiteSejour(NominationVisiteSejour $nominationsVisiteSejour): self
    {
        if ($this->nominationsVisiteSejour->removeElement($nominationsVisiteSejour)) {
            // set the owning side to null (unless already changed)
            if ($nominationsVisiteSejour->getSejour() === $this) {
                $nominationsVisiteSejour->setSejour(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UtilisationLieuSejour>
     */
    public function getUtilisationsLieuSejour(): Collection
    {
        return $this->utilisationsLieuSejour;
    }

    public function addUtilisationsLieuSejour(UtilisationLieuSejour $utilisationsLieuSejour): self
    {
        if (!$this->utilisationsLieuSejour->contains($utilisationsLieuSejour)) {
            $this->utilisationsLieuSejour[] = $utilisationsLieuSejour;
            $utilisationsLieuSejour->setSejour($this);
        }

        return $this;
    }

    public function removeUtilisationsLieuSejour(UtilisationLieuSejour $utilisationsLieuSejour): self
    {
        if ($this->utilisationsLieuSejour->removeElement($utilisationsLieuSejour)) {
            // set the owning side to null (unless already changed)
            if ($utilisationsLieuSejour->getSejour() === $this) {
                $utilisationsLieuSejour->setSejour(null);
            }
        }

        return $this;
    }
}
