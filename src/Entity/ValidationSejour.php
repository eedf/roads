<?php

namespace App\Entity;

use App\Repository\ValidationSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Données de validation 'classique'd'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: ValidationSejourRepository::class)]
class ValidationSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: "validationSejour", cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["fin_sejour"])]
    private ?Sejour $sejour = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["fin_sejour"])]
    private ?Document $documentValidation = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $commentaires = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?bool $estValide = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?bool $necessiteVisite = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getDocumentValidation(): ?Document
    {
        return $this->documentValidation;
    }

    public function setDocumentValidation(?Document $documentValidation): self
    {
        $this->documentValidation = $documentValidation;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getEstValide(): ?bool
    {
        return $this->estValide;
    }

    public function setEstValide(?bool $estValide): self
    {
        $this->estValide = $estValide;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getNecessiteVisite(): ?bool
    {
        return $this->necessiteVisite;
    }

    public function setNecessiteVisite(?bool $necessiteVisite): self
    {
        $this->necessiteVisite = $necessiteVisite;

        return $this;
    }
}
