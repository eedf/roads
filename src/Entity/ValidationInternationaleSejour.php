<?php

namespace App\Entity;

use App\Repository\ValidationInternationaleSejourRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Données de validation internationale d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[ORM\Entity(repositoryClass: ValidationInternationaleSejourRepository::class)]
class ValidationInternationaleSejour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: "validationInternationaleSejour", cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["fin_sejour"])]
    private ?Sejour $sejour = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Groups(["fin_sejour"])]
    private ?Document $documentValidationInternationale = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?string $commentaires = null;

    #[ORM\Column(nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?bool $estValide = true;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(["fin_sejour"])]
    private ?\DateTimeInterface $dateModification = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSejour(): ?Sejour
    {
        return $this->sejour;
    }

    public function setSejour(Sejour $sejour): self
    {
        $this->sejour = $sejour;

        return $this;
    }

    public function getDocumentValidationInternationale(): ?Document
    {
        return $this->documentValidationInternationale;
    }

    public function setDocumentValidationInternationale(?Document $documentValidationInternationale): self
    {
        $this->documentValidationInternationale = $documentValidationInternationale;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getEstValide(): ?bool
    {
        return $this->estValide;
    }

    public function setEstValide(?bool $estValide): self
    {
        $this->estValide = $estValide;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }
}
