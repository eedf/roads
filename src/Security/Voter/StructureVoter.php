<?php

namespace App\Security\Voter;

use App\Entity\Personne;
use App\Entity\Structure;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Voter pour déterminer les droits afférents aux actions sur une structure
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class StructureVoter extends Voter
{
    const STRUCTURE_ORGANISER = 'organiser_sejour';
    const STRUCTURE_MODIFIER_INFOS = 'modifier_infos';
    const STRUCTURE_DONNER_SUIVI = 'donner_suivi';
    const STRUCTURE_VALIDER = 'valider';
    protected function supports($attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::STRUCTURE_ORGANISER,
            self::STRUCTURE_MODIFIER_INFOS,
            self::STRUCTURE_DONNER_SUIVI,
            self::STRUCTURE_VALIDER
        ])
            && $subject instanceof \App\Entity\Structure;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::STRUCTURE_ORGANISER:
                return $this->canOrganiser($subject, $user);
                break;
            case self::STRUCTURE_MODIFIER_INFOS:
                return $this->canRenseignerInfosStructure($subject, $user);
                break;
            case self::STRUCTURE_DONNER_SUIVI;
                return $this->canDonnerSuivi($subject, $user);
                break;
            case self::STRUCTURE_VALIDER:
                return $this->canValider($subject, $user);
                break;
        }

        return false;
    }

    /**
     * Détermine si une personne peut organiser un séjour sur une structure
     *
     * @param  Structure $structure
     * @param  Personne $personne
     * @return boolean
     */
    private function canOrganiser(Structure $structure, Personne $personne)
    {
        if ($structure->getOrganisateurs()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si une personne peut renseigner les ifnormations d'une structure
     *
     * @param  Structure $structure
     * @param  Personne $personne
     * @return boolean
     */
    private function canRenseignerInfosStructure(Structure $structure, Personne $personne)
    {
        if ($structure->getOrganisateurs()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si une personne peut valider un séjour sur une structure
     *
     * @param  Structure $structure
     * @param  Personne $personne
     * @return boolean
     */
    private function canValider(Structure $structure, Personne $personne)
    {
        if ($structure->getValidateurs()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Détermine si une personne peut donner les droits de suivi de séjour sur une structure
     *
     * @param  Structure $structure
     * @param  Personne $personne
     * @return boolean
     */
    private function canDonnerSuivi(Structure $structure, Personne $personne)
    {
        if ($this->canOrganiser($structure, $personne)) {
            return true;
        } else if ($this->canValider($structure, $personne)) {
            return true;
        } else {
            return false;
        }
    }
}
