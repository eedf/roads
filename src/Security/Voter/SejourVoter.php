<?php

namespace App\Security\Voter;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;
use App\Entity\Sejour;
use App\Entity\Structure;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Voter pour déterminer les droits afférents aux actions sur un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class SejourVoter extends Voter
{
    const SEJOUR_SUIVI = 'view_details';
    const SEJOUR_VISITE = 'visite';
    const SEJOUR_ORGANISER = 'organisation';
    const SEJOUR_MODIFICATION_PROJET = 'modif_projet';
    const SEJOUR_ENVOI_VALIDATION = 'envoi_validation';
    const SEJOUR_EXPORT_INFOS = 'export';
    const SEJOUR_VALIDATION = 'validation';
    const SEJOUR_VALIDATION_INTERNATIONALE = 'validation_internationale';
    const SEJOUR_CLOTURE = 'cloture';
    const SEJOUR_SUSPENSION = 'suspension';
    const SEJOUR_ANNULATION = 'annulation';
    const SEJOUR_NOMINATION_DIRECTEUR_ADJOINT = 'nomination_directeur_adjoint';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    protected function supports($attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::SEJOUR_SUIVI,
            self::SEJOUR_ORGANISER,
            self::SEJOUR_MODIFICATION_PROJET,
            self::SEJOUR_EXPORT_INFOS,
            self::SEJOUR_NOMINATION_DIRECTEUR_ADJOINT,
            self::SEJOUR_ENVOI_VALIDATION,
            self::SEJOUR_VALIDATION,
            self::SEJOUR_VALIDATION_INTERNATIONALE,
            self::SEJOUR_CLOTURE,
            self::SEJOUR_VISITE,
            self::SEJOUR_SUSPENSION,
            self::SEJOUR_ANNULATION
        ])
            && $subject instanceof \App\Entity\Sejour;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::SEJOUR_ORGANISER:
                return $this->canOrganiser($subject, $user);
                break;
            case self::SEJOUR_VALIDATION:
                return $this->canValider($subject, $user);
                break;
            case self::SEJOUR_VALIDATION_INTERNATIONALE:
                return $this->canValiderInternational($subject, $user);
                break;
            case self::SEJOUR_MODIFICATION_PROJET:
                return $this->canModifierProjet($subject, $user);
                break;
            case self::SEJOUR_NOMINATION_DIRECTEUR_ADJOINT:
                return $this->canNommerDirecteurAdjoint($subject, $user);
                break;
            case self::SEJOUR_ENVOI_VALIDATION:
                return $this->canEnvoyerPourValidation($subject, $user);
                break;
            case self::SEJOUR_EXPORT_INFOS:
                return $this->canExporter($subject, $user);
                break;
            case self::SEJOUR_CLOTURE:
                return $this->canCloturer($subject, $user);
                break;
            case self::SEJOUR_SUSPENSION:
                return $this->canSuspendre($subject, $user);
                break;
            case self::SEJOUR_ANNULATION:
                return $this->canAnnuler($subject, $user);
                break;
            case self::SEJOUR_SUIVI:
                return $this->canSuivre($subject, $user);
                break;
            case self::SEJOUR_VISITE:
                return $this->canVisiter($subject, $user);
                break;
        }

        return false;
    }


    /**
     * Détermine si un personne peut organiser un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canOrganiser(Sejour $sejour, Personne $personne)
    {
        if ($sejour->getStructureOrganisatrice()->getOrganisateurs()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut visiter un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canVisiter(Sejour $sejour, Personne $personne)
    {
        /** @var NominationVisiteSejour $nominationVisite  */
        foreach ($sejour->getNominationsVisiteSejour() as $nominationVisite) {
            if ($nominationVisite->getVisiteur() == $personne) {
                return true;
            }
        }
        return false;
    }

    /**
     * Détermine si un personne peut valider un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canValider(Sejour $sejour, Personne $personne)
    {
        if (
            $personne->hasRole(Personne::ROLE_VALIDATION_NATIONALE)
            && ($sejour->getStructureOrganisatrice()->getStructureParent()->getEchelon() == Structure::ECHELON_STRUCTURE_NATIONAL)
        ) {
            return true;
        } else if ( //Cas des séjours à qui l'on a délégué manuellement la validation
            $personne->getSejoursValidation()->contains($sejour)
        ) {
            return true;
        } else if ( //Cas des séjours organisés par les SLA et SLR: la personne doit etre validateur de la structure parente de celle qui organise
            ($sejour->getStructureOrganisatrice()->getStructureParent())
            && ($sejour->getStructureOrganisatrice()->getStructureParent()->getValidateurs()->contains($personne))
            && ($sejour->getStructureOrganisatrice()->getStructureParent()->getEchelon() == Structure::ECHELON_STRUCTURE_REGIONAL)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut valider 'internationalement' un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canValiderInternational(Sejour $sejour, Personne $personne)
    {
        if (
            $personne->hasRole(Personne::ROLE_VALIDATION_INTERNATIONALE)
            && ($sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger())
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut modifier un projet de séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canModifierProjet(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else if ($sejour->getDirecteursAdjoints()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut envoyer un séjour en validation
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canEnvoyerPourValidation(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut exporter les informations d'un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canExporter(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut suspendre un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canSuspendre(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut annuler un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canAnnuler(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut clôturer un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canCloturer(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteur() == $personne) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Détermine si un personne peut nommer des direct·eur·rice·s adjoint·e·s sur un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canNommerDirecteurAdjoint(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($sejour->getDirecteursAdjoints()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Détermine si un personne peut suivre un séjour
     *
     * @param  Sejour $sejour
     * @param  Personne $personne
     * @return boolean
     */
    private function canSuivre(Sejour $sejour, Personne $personne)
    {
        if ($this->canOrganiser($sejour, $personne)) {
            return true;
        } else if ($this->canValider($sejour, $personne)) {
            return true;
        } else if ($this->canVisiter($sejour, $personne)) {
            return true;
        } else if ($this->canValiderInternational($sejour, $personne)) {
            return true;
        } else if ($this->canModifierProjet($sejour, $personne)) {
            return true;
        } else if ($sejour->getEncadrants()->contains($personne)) {
            return true;
        } else if ($this->security->isGranted(Personne::ROLE_SUIVI_GLOBAL)) {
            return true;
        } else if ($sejour->getAccompagnant() == $personne) {
            return true;
        } else if ($sejour->getStructureOrganisatrice()->getSuiveurs()->contains($personne)) {
            return true;
        } else if ($sejour->getSuiveurs()->contains($personne)) {
            return true;
        } else {
            return false;
        }
    }
}
