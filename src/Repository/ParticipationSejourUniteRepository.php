<?php

namespace App\Repository;

use App\Entity\ParticipationSejourUnite;
use App\Entity\Sejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParticipationSejourUnite|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParticipationSejourUnite|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParticipationSejourUnite[]    findAll()
 * @method ParticipationSejourUnite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipationSejourUniteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParticipationSejourUnite::class);
    }

    // /**
    //  * @return ParticipationSejourUnite[] Returns an array of ParticipationSejourUnite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParticipationSejourUnite
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * findBySejour
     * Retourne un tableau résultant de la concaténation des:
     *  - Participations des unités EEDF de la structure organisatrice d'un séjour participant au séjour;
     *  - Participations des unités EEDF des structure regroupées d'un séjour participant au séjour;
     * @param  Sejour $sejour
     * @return array les participations au séjour
     */
    public function findBySejour(Sejour $sejour): ?array
    {

        return
            array_values(
                array_merge(
                    $this->createQueryBuilder('p')
                        ->join('p.sejourOrganisateur', 'sejourOrganisateur')
                        ->where('sejourOrganisateur.id = :sejour_id')
                        ->setParameter('sejour_id', $sejour->getId())->getQuery()->getResult(),
                    $this->createQueryBuilder('p')
                        ->join('p.structureRegroupementSejour', 's')
                        ->join('s.sejour', 'sejour')
                        ->where('sejour.id = :sejour_id')
                        ->setParameter('sejour_id', $sejour->getId())->getQuery()->getResult()
                )
            );
    }
}
