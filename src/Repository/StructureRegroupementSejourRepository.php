<?php

namespace App\Repository;

use App\Entity\Sejour;
use App\Entity\Structure;
use App\Entity\StructureRegroupementSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;;

use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StructureRegroupementSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method StructureRegroupementSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method StructureRegroupementSejour[]    findAll()
 * @method StructureRegroupementSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StructureRegroupementSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StructureRegroupementSejour::class);
    }

    // /**
    //  * @return StructureRegroupementSejour[] Returns an array of StructureRegroupementSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StructureRegroupementSejour
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * findNonEEDFBySejour
     * Renvoie tous les regroupements hors EEDF d'un séjour
     * @param  Sejour $sejour
     * @return array les regroupements concernés
     */
    public function findNonEEDFBySejour(Sejour $sejour): ?array
    {

        return
            $this->createQueryBuilder('s')
            ->join('s.sejour', 'sejour')
            ->where('sejour.id = :sejour_id')
            ->andWhere('s.estEEDF = FALSE')
            ->setParameter('sejour_id', $sejour->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * findSejoursRegroupement
     * Renvoie les regroupements pour lequels une structure EEDF a été invitée dans le statut de séjour modifiable
     * @uses Sejour::LISTES_STATUTS_SEJOURS_PROJET_MODIFIABLE liste des états de séjour modifiables
     * @param  Structure $structure
     * @return array les regroupements concernés
     */
    public function findSejoursRegroupement(Structure $structure): ?array
    {
        $query_builder = $this->createQueryBuilder('s')
            ->join('s.structureEEDF', 'struct')
            ->andWhere('s.estEEDF = TRUE')
            ->andWhere('struct.id = :struct_id')
            ->setParameter('struct_id', $structure->getId());

        $query_builder = $this->filterOnSejourStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_PROJET_MODIFIABLE);

        return $query_builder->getQuery()
            ->getResult();
    }

    /**
     * filterOnSejourStatusList
     * Permet de filter une requete passée en paramètre pour la filtrer sur des états du séjour
     * @param  QueryBuilder $query_builder la requête passée en paramètre
     * @param  array $statusList la liste des statuts de séjour sur lesquels filter
     * @return QueryBuilder la requête d'entrée modifiée
     */
    public function filterOnSejourStatusList(QueryBuilder $query_builder, array $statusList): QueryBuilder
    {
        $orStatusCondition = "";
        foreach ($statusList as $index => $statut_visible) {
            if ($orStatusCondition != "") {
                $orStatusCondition .= " OR ";
            }
            $orStatusCondition .= "(sejour.status LIKE :status_" . $index . ")";
            $query_builder->setParameter("status_" . $index, '%' . $statut_visible . '%');
        }
        $query_builder->join('s.sejour', 'sejour');
        $query_builder->andWhere($orStatusCondition);
        return $query_builder;
    }
}
