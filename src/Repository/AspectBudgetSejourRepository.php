<?php

namespace App\Repository;

use App\Entity\AspectBudgetSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AspectBudgetSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method AspectBudgetSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method AspectBudgetSejour[]    findAll()
 * @method AspectBudgetSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectBudgetSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AspectBudgetSejour::class);
    }

    // /**
    //  * @return AspectBudgetSejour[] Returns an array of AspectBudgetSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AspectBudgetSejour
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
