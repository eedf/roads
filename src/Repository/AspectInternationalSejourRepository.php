<?php

namespace App\Repository;

use App\Entity\AspectInternationalSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AspectInternationalSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method AspectInternationalSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method AspectInternationalSejour[]    findAll()
 * @method AspectInternationalSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectInternationalSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AspectInternationalSejour::class);
    }

    // /**
    //  * @return AspectInternationalSejour[] Returns an array of AspectInternationalSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AspectInternationalSejour
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
