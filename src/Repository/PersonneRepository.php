<?php

namespace App\Repository;

use App\Entity\Personne;
use App\Security\Provider\JeitoResourceOwner;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @method Personne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personne[]    findAll()
 * @method Personne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Personne::class);
    }

    // /**
    //  * @return Personne[] Returns an array of Personne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Personne
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * findByRole
     * Renvoie l'ensemble des personnes qui possèdent le rôle passé en paramètre
     * @param  String $role le rôle sur lequel filtrer
     * @return array l'ensemble des personnes concernées
     */
    public function findByRole(String $role)
    {
        return $this->createQueryBuilder('a')
            ->where('a.roles LIKE :roles')
            ->setParameter('roles', '%' . $role . '%')
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * findByNomPrenomOrNumAdhOrNumSalaraieField
     * renvoie a partir d'une chaine de caractères les personnes dont la chaine correspond (même partiellement) à son nom, prénom, ou numéro d'adhérent·e ou salarié·e
     * @param  string $query
     * @param  int $limit limite maximale du nombre de résultats
     * @return array l'ensemble des personnes concernées dans la limite du paramètre limite
     */
    public function findByNomPrenomOrNumAdhOrNumSalaraieField(string $query, int $limit = 5)
    {
        $query = trim($query);
        $pos_space = strpos($query, " ");


        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder->leftJoin("a.adherent", "adh");
        $queryBuilder->leftJoin("a.employe", "empl");

        //Cas de plusieurs mots: nom prenom ou prenom nom
        if ($pos_space) {
            $mot1 = substr($query, 0, $pos_space);
            $mot2 = substr($query, $pos_space + 1);
            $queryBuilder
                ->orWhere('a.nom LIKE :mot1 AND a.prenom LIKE :mot2')
                ->orWhere('a.nom LIKE :mot2 AND a.prenom LIKE :mot1')
                ->setParameter('mot1', '%' . $mot1 . '%')
                ->setParameter('mot2', '%' . $mot2 . '%');
            //Cas d'un numéro d'adhérent ou de salarié commençant par 0
        } else if (str_starts_with($query, "0")) {
            // On supprime les 0 en début de chaine
            while (str_starts_with($query, "0")) {
                $query = substr($query, 1);
            }
            $queryBuilder->orWhere('adh.numeroAdherent LIKE :str')
                ->setParameter('str', $query . '%');
            $queryBuilder->orWhere('empl.numeroSalarie LIKE :str')
                ->setParameter('str', $query . '%');
        } else {
            $queryBuilder
                ->orWhere('a.nom LIKE :str')
                ->orWhere('a.prenom LIKE :str')
                ->orWhere('adh.numeroAdherent LIKE :str')
                ->orWhere('empl.numeroSalarie LIKE :str')
                ->setParameter('str', '%' . $query . '%');
        }

        return $queryBuilder
            ->andWhere('(empl.id IS NOT NULL) OR (adh.id IS NOT NULL)') // On filtre uniquement parmi les salariés ou adhérents
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }


    /**
     * findByNumAdhOrMatriculeSalarieField
     * renvoie a partir d'une chaine de caractères les personnes dont le champ passé en paramètre correspond soit à son matricule, soit à son numéro d'adhérent·e
     * @param  string $number
     * @return Personne la personne concernée
     */
    public function findByNumAdhOrMatriculeSalarieField(string $number)
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.adherent', 'adh')
            ->leftJoin('a.employe', 'employe')
            ->where('adh.numeroAdherent=:number')
            ->orWhere('employe.numeroSalarie=:number')
            ->setParameter('number', $number)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * findUpdatedBefore
     * Renvoie la liste des personnes qui ont été mises à jour la dernière fois avant la date passée en paramètre
     * @param  DateTime $dateUpdate la date max de mise à jour
     * @return array l'ensemble des personnes concernées
     */

    public function findLastUpdatedBefore(DateTime $date)
    {
        return $this->createQueryBuilder('s')
            ->where("s.dateModification < :date")
            ->orderBy("s.responsableLegal1", 'DESC')
            ->orderBy("s.responsableLegal2", 'DESC')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }


    public function findWhereUuidIn(array $uuids)
    {
        return $this->createQueryBuilder('s')
            ->where("s.uuid in (:uuids)")
            ->setParameters([
                'uuids' => array_map(function ($uuid) {
                    return Uuid::fromString($uuid)->toBinary();
                }, $uuids)
            ])
            ->getQuery()
            ->getResult();
    }
}
