<?php

namespace App\Repository;

use App\Entity\AspectCommunicationSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AspectCommunicationSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method AspectCommunicationSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method AspectCommunicationSejour[]    findAll()
 * @method AspectCommunicationSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectCommunicationSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AspectCommunicationSejour::class);
    }

    // /**
    //  * @return AspectCommunicationSejour[] Returns an array of AspectCommunicationSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AspectCommunicationSejour
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
