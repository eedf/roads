<?php

namespace App\Repository;

use App\Entity\QualificationDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QualificationDirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method QualificationDirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method QualificationDirection[]    findAll()
 * @method QualificationDirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualificationDirectionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QualificationDirection::class);
    }

    // /**
    //  * @return QualificationDirection[] Returns an array of QualificationDirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QualificationDirection
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
