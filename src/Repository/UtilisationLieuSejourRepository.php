<?php

namespace App\Repository;

use App\Entity\UtilisationLieuSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UtilisationLieuSejour>
 *
 * @method UtilisationLieuSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method UtilisationLieuSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method UtilisationLieuSejour[]    findAll()
 * @method UtilisationLieuSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisationLieuSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UtilisationLieuSejour::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(UtilisationLieuSejour $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(UtilisationLieuSejour $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findUtilisationsLieuSejourActifs(array $statusList)
    {
        $query_builder = $this->createQueryBuilder('u')
            ->join('u.sejour', 's');

        $orStatusCondition = "";
        foreach ($statusList as $index => $statut_visible) {
            if ($orStatusCondition != "") {
                $orStatusCondition .= " OR ";
            }
            $orStatusCondition .= "(s.status LIKE :status_" . $index . ")";
            $query_builder->setParameter("status_" . $index, '%' . $statut_visible . '%');
        }
        $query_builder->andWhere($orStatusCondition);
        return $query_builder->getQuery()
            ->getResult();
    }

    // /**
    //  * @return UtilisationLieuSejour[] Returns an array of UtilisationLieuSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UtilisationLieuSejour
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
