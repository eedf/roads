<?php

namespace App\Repository;

use App\Entity\DemandeModerationCommentaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DemandeModerationCommentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method DemandeModerationCommentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method DemandeModerationCommentaire[]    findAll()
 * @method DemandeModerationCommentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandeModerationCommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemandeModerationCommentaire::class);
    }

    // /**
    //  * @return DemandeModerationCommentaire[] Returns an array of DemandeModerationCommentaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DemandeModerationCommentaire
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
