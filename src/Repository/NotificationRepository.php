<?php

namespace App\Repository;


use App\Entity\Notification;
use App\Entity\Personne;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\User;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    // /**
    //  * @return Notification[] Returns an array of Notification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notification
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * findNotificationsAEnvoyer
     * Récupère l'ensembles des notifications qui ne sont pas encore envoyées ni lues et pour lesquelles la date d'envoi est inférieure à la date limite
     * @param  DateTime $date La date limite
     * @return Notification[] l'ensemble des notifications retournées
     */
    public function findNotificationsAEnvoyer(DateTime $date)
    {
        return $this->createQueryBuilder('n')
            ->where('n.delivered = FALSE')
            ->andWhere('n.lu = FALSE')
            ->andWhere('n.dateEnvoi < :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();;
    }

    /**
     * findNotificationsAAfficher
     * Retourne l'ensemble des notifications concernant une personne
     * @param  Personne $personne la personne passée en paramètre
     * @return Notification[] l'ensemble des notifications correspondantes
     */
    public function findNotificationsAAfficher(Personne $personne)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.personne = :personne')
            ->setParameter('personne', $personne)
            ->getQuery()
            ->getResult();
    }

    /**
     * updateNotificationsCommeLues
     * Positionne toutes les notifications concernant une personne comme lues
     * @param  Personne $personne la personne passée en paramètre
     * @return void
     */
    public function updateNotificationsCommeLues(Personne $personne)
    {
        return $this->createQueryBuilder('n')
            ->update()
            ->set('n.lu', 'TRUE')
            ->andWhere('n.personne = :personne')
            ->setParameter('personne', $personne)
            ->getQuery()
            ->getResult();
    }

    /**
     * findNotificationsAAfficherPourAccueil
     *  Retourne l'ensemble des notifications non lues concernant une personne et qui ont été créées depuis une date passée en paramètre
     * @param  Personne $personne
     * @param  DateTime $date
     * @return array
     */
    public function findNotificationsAAfficherPourAccueil(Personne $personne, DateTime $date): ?array
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.personne = :personne')
            ->setParameter('personne', $personne)
            ->andWhere('n.dateCreation >= :date')
            ->andWhere('n.lu = FALSE')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    /**
     * deleteCreatedBefore
     * Supprime toutes les notifications crées avant une date
     * @param  DateTime $date
     * @return void
     */
    public function deleteCreatedBefore(DateTime $date)
    {
        return $this->createQueryBuilder('n')
            ->delete()
            ->andWhere('n.dateCreation < :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();;
    }
}
