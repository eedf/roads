<?php

namespace App\Repository;

use App\Entity\EffectifsPrevisionnels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EffectifsPrevisionnels|null find($id, $lockMode = null, $lockVersion = null)
 * @method EffectifsPrevisionnels|null findOneBy(array $criteria, array $orderBy = null)
 * @method EffectifsPrevisionnels[]    findAll()
 * @method EffectifsPrevisionnels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EffectifsPrevisionnelsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EffectifsPrevisionnels::class);
    }

    // /**
    //  * @return EffectifsPrevisionnels[] Returns an array of EffectifsPrevisionnels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EffectifsPrevisionnels
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
