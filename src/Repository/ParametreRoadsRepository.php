<?php

namespace App\Repository;

use App\Entity\ParametreRoads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParametreRoads|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParametreRoads|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParametreRoads[]    findAll()
 * @method ParametreRoads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametreRoadsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParametreRoads::class);
    }

    // /**
    //  * @return ParametreRoads[] Returns an array of ParametreRoads objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParametreRoads
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
