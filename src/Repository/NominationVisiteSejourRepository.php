<?php

namespace App\Repository;

use App\Entity\NominationVisiteSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NominationVisiteSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method NominationVisiteSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method NominationVisiteSejour[]    findAll()
 * @method NominationVisiteSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NominationVisiteSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NominationVisiteSejour::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(NominationVisiteSejour $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(NominationVisiteSejour $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return NominationVisiteSejour[] Returns an array of NominationVisiteSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NominationVisiteSejour
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
