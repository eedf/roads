<?php

namespace App\Repository;

use App\Entity\EvenementGrave;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvenementGrave|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvenementGrave|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvenementGrave[]    findAll()
 * @method EvenementGrave[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementGraveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvenementGrave::class);
    }

    // /**
    //  * @return EvenementGrave[] Returns an array of EvenementGrave objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvenementGrave
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
