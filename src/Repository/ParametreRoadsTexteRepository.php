<?php

namespace App\Repository;

use App\Entity\ParametreRoadsTexte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParametreRoadsTexte|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParametreRoadsTexte|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParametreRoadsTexte[]    findAll()
 * @method ParametreRoadsTexte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametreRoadsTexteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParametreRoadsTexte::class);
    }

    // /**
    //  * @return ParametreRoadsTexte[] Returns an array of ParametreRoadsTexte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParametreRoadsTexte
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
