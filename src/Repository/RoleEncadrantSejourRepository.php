<?php

namespace App\Repository;

use App\Entity\RoleEncadrantSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoleEncadrantSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoleEncadrantSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoleEncadrantSejour[]    findAll()
 * @method RoleEncadrantSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleEncadrantSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoleEncadrantSejour::class);
    }

    // /**
    //  * @return RoleEncadrantSejour[] Returns an array of RoleEncadrantSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoleEncadrantSejour
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
