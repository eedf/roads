<?php

namespace App\Repository;

use App\Entity\AspectAlimentaireSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AspectAlimentaireSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method AspectAlimentaireSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method AspectAlimentaireSejour[]    findAll()
 * @method AspectAlimentaireSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AspectAlimentaireSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AspectAlimentaireSejour::class);
    }

    // /**
    //  * @return AspectAlimentaireSejour[] Returns an array of AspectAlimentaireSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AspectAlimentaireSejour
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
