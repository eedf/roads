<?php

namespace App\Repository;

use App\Entity\RetourSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RetourSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method RetourSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method RetourSejour[]    findAll()
 * @method RetourSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RetourSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RetourSejour::class);
    }

    // /**
    //  * @return RetourSejour[] Returns an array of RetourSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RetourSejour
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
