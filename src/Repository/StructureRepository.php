<?php

namespace App\Repository;

use App\Entity\Structure;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @method Structure|null find($id, $lockMode = null, $lockVersion = null)
 * @method Structure|null findOneBy(array $criteria, array $orderBy = null)
 * @method Structure[]    findAll()
 * @method Structure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StructureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Structure::class);
    }

    // /**
    //  * @return Structure[] Returns an array of Structure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Structure
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * findStructuresAValidationNationale
     * Renvoie l'ensemble des structures nécessitant une validation nationale des séjours qu'elle organisele
     * @return array la liste des structures nécessitant une validation nationale
     */
    public function findStructuresAValidationNationale()
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonNational")
            ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL)
            ->orderBy('s.type, s.nom', 'ASC');
    }

    public function findLastUpdatedBefore(DateTime $date)
    {
        return $this->createQueryBuilder('s')
            ->where("s.dateModification < :date")
            ->orderBy("s.structureParent", 'DESC')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function findWhereUuidIn(array $uuids)
    {
        return $this->createQueryBuilder('s')
            ->where("s.uuid in (:uuids)")
            ->setParameters([
                'uuids' => array_map(function ($uuid) {
                    return Uuid::fromString($uuid)->toBinary();
                }, $uuids)
            ])
            ->getQuery()
            ->getResult();
    }
}
