<?php

namespace App\Repository;

use App\Entity\ValidationSejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ValidationSejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method ValidationSejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValidationSejour[]    findAll()
 * @method ValidationSejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValidationSejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ValidationSejour::class);
    }

    // /**
    //  * @return ValidationSejour[] Returns an array of ValidationSejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ValidationSejour
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
