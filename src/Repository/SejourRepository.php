<?php

namespace App\Repository;

use App\Entity\Sejour;
use App\Entity\Structure;
use App\Entity\StructureRegroupementSejour;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sejour[]    findAll()
 * @method Sejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SejourRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Sejour::class);
  }

  // /**
  //  * @return Sejour[] Returns an array of Sejour objects
  //  */
  /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

  /*
    public function findOneBySomeField($value): ?Sejour
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

  /**
   * findSejoursAValiderPourRegion
   * Renvoi la liste des séjour dont la structure organisatrice a comme parent la structure passée en paramètre, pour lesquels les statuts sont visibles pour le suivi
   * @param  Structure $structure la structure de validation
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI la liste des états pour lequel un séjour est visible en suivi
   * @return void La liste des séjours concernés
   */
  public function findSejoursAValiderPourRegion(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('struct.structureParent', 'region')
      ->andWhere('region.id = :region_id')
      ->setParameter('region_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);

    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findTousSejoursVisitables
   * retourne la liste de tous les séjours qui sont dans un statut visitable
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISITABLES la liste des états pour lequel un séjour est visitable
   * @return array la liste des séjours concernés
   */
  public function findTousSejoursVisitables()
  {
    $query_builder = $this->createQueryBuilder('s');

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISITABLES);

    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findTousSejoursVisitables
   * retourne la liste de tous les séjours qui sont dans un statut visitable
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISITABLES la liste des états pour lequel un séjour est visitable
   * @return array la liste des séjours concernés
   */
  public function findTousSejoursDeclaresNonDemarres()
  {
    $query_builder = $this->createQueryBuilder('s');

    $query_builder = $this->filterOnStatusList($query_builder, [
      Sejour::STATUT_SEJOUR_CONSTRUCTION_PROJET,
      Sejour::STATUT_SEJOUR_ATTENTE_VALIDATION,
      Sejour::STATUT_SEJOUR_VALIDE,
      Sejour::STATUT_SEJOUR_SUSPENDU
    ]);

    return $query_builder->getQuery()
      ->getResult();
  }



  /**
   * findTousSejoursNonClotures
   * retourne la liste de tous les séjours qui sont dans un statut visible en suivi (non cloturés)
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI la liste des états pour lequel un séjour est visible en suivi
   * @return array la liste des séjours concernés
   */
  public function findTousSejoursNonClotures()
  {
    $query_builder = $this->createQueryBuilder('s');

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);

    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findTousSejoursClotures
   * Renvoie la liste de tous les séjours cloturés
   * @uses Sejour::STATUT_SEJOUR_CLOTURE l'état séjour cloturé
   * @return array la liste des séjours concernés
   */
  public function findTousSejoursClotures()
  {
    $query_builder = $this->createQueryBuilder('s');

    $query_builder = $this->filterOnStatusList($query_builder, [Sejour::STATUT_SEJOUR_CLOTURE]);

    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSejoursOrganisesParStructure
   * Retourne la liste des séjours visibles en organisation (non cloturés) qu'une structure organise
   * @param  Structure $structure la structure organisatrice des séjours
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_ORGANISATION liste des états visibles en organisation d'un séjour
   * @return array la liste des séjours concernés
   */
  public function findSejoursOrganisesParStructure(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->andWhere('struct.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_ORGANISATION);
    return $query_builder->getQuery()
      ->getResult();
  }



  /**
   * findArchivesSejoursOrganisesParStructure
   * Retourne la liste des séjours cloturés qu'une structure organise
   * @param  Structure $structure la structure organisatrice des séjours
   * @uses Sejour::STATUT_SEJOUR_CLOTURE état de séjour cloturé
   * @return array la liste des séjours concernés
   */
  public function findArchivesSejoursOrganisesParStructure(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->andWhere('struct.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, [Sejour::STATUT_SEJOUR_CLOTURE]);
    return $query_builder->getQuery()
      ->getResult();
  }





  /**
   * findSejoursValidationParStructure
   * Retourne la liste des séjours visibles en suivi (non cloturés) qu'une structure a en validation
   * @param  Structure $structure la structure validatrice des séjours
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI liste des états visibles en suivi d'un séjour
   * @return array la liste des séjours concernés
   */
  public function findSejoursValidationParStructure(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('struct.structureParent', 'struct_valid')
      ->andWhere('struct_valid.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);


    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findSejoursVisitablesParStructure
   * Retourne la liste des séjours visitables qu'une structure a en validation
   * @param  Structure $structure la structure validatrice des séjours
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISITABLES liste des états visitables d'un séjour
   * @return array la liste des séjours concernés
   */
  public function findSejoursVisitablesParStructure(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('struct.structureParent', 'struct_valid')
      ->andWhere('struct_valid.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISITABLES);


    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSejoursNationauxVisitables
   * Renvoie la liste des séjour nécesitant une validation nationale, étant dans un état visitable
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISITABLES liste des états visitables d'un séjour
   * @return array la liste des séjours concernés
   */
  public function findSejoursNationauxVisitables()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->innerJoin("struct.structureParent", "structureParent")
      ->where("structureParent.echelon = :echelonNational")
      ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL);

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISITABLES);


    return $query_builder->getQuery()
      ->getResult();
  }



  /**
   * findArchivesSejoursValidesParStructure
   * Retourne la liste des séjours cloturés que la structure passée en paramètre a en charge de validation
   * @param  Structure $structure la structure validatrice
   * @uses Sejour::STATUT_SEJOUR_CLOTURE état de séjour cloturé
   * @return array la liste des séjours concernés
   */
  public function findArchivesSejoursValidesParStructure(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('struct.structureParent', 'struct_valid')
      ->andWhere('struct_valid.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, [Sejour::STATUT_SEJOUR_CLOTURE]);


    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSuivisSejoursPourStructureValid
   * Retourne la liste des séjours et des suiveu·r·se·s de ces séjours que la structure passée en paramètre a en charge de validation, visibles en suivi
   * @param  Structure $structure la structure validatrice
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI la liste des états de séjour visibles en suivi
   * @return array la liste des séjours concernés
   */
  public function findSuivisSejoursPourStructureValid(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->addSelect('adh')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('s.suiveurs', 'adh')
      ->join('struct.structureParent', 'struct_valid')
      ->andWhere('struct_valid.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);
    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findValidateursSejoursPourStructureValid
   * Renvoie la liste des séjours et des personnes validateurs de ces séjour pour lesquelles la structure validatrice est passée en paramètre, visibles en suivi
   * @param  Structure $structure la structure validatrice concernée
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI la liste des états de séjour visibles en suivi
   * @return array la liste des séjours et validat·eur·rice concerné·e·s
   */
  public function findValidateursSejoursPourStructureValid(Structure $structure)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->addSelect('adh')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('s.validateurs', 'adh')
      ->join('struct.structureParent', 'struct_valid')
      ->andWhere('struct_valid.id = :struct_id')
      ->setParameter('struct_id', $structure->getId());

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);
    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSuivisSejoursPourStructureOrga
   * Renvoie la liste des séjours organisés par la structure passée en paramère
   * @param  Structure $structure la structure organisatrice concernée
   * @return array la liste des séjours concernés
   */
  public function findSuivisSejoursPourStructureOrga(Structure $structure)
  {
    return $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->join('s.suiveurs', 'adh')
      ->where('struct.id=:struct_id')
      ->setParameter('struct_id', $structure->getId())
      ->getQuery()
      ->getResult();
  }


  /**
   * findSejoursInternationaux
   * Renvoie la liste des séjour internationaux ou accueillant des participants étrangers étant dans un état visible par le suivi
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI liste des états visibles en suivi
   * @return array la liste des séjours concernés
   */
  public function findSejoursInternationaux()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->where('s.sejourFranceAvecAccueilEtranger = TRUE')
      ->orWhere('s.international = TRUE');

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);
    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSejoursInternationauxVisitables
   * Renvoie la liste des séjour internationaux ou accueillant des participants étrangers étant dans un état visitable
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISITABLES liste des états visitables
   * @return array la liste des séjours concernés
   */
  public function findSejoursInternationauxVisitables()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->where('s.sejourFranceAvecAccueilEtranger = TRUE')
      ->orWhere('s.international = TRUE');

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISITABLES);


    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findArchivesSejoursInternationaux
   * Renvoie la liste des séjour internationaux ou accueillant des participants étrangers dans l'état cloturé
   * @uses Sejour::STATUT_SEJOUR_CLOTURE etat de séjour cloturé
   * @return array la liste des séjours concernés
   */
  public function findArchivesSejoursInternationaux()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->where('s.sejourFranceAvecAccueilEtranger = TRUE')
      ->orWhere('s.international = TRUE');

    $query_builder = $this->filterOnStatusList($query_builder, [Sejour::STATUT_SEJOUR_CLOTURE]);
    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findSejoursNationaux
   * Renvoie la liste des séjour organisés par des structures nationales qui ne sont pas cloturés
   * @uses Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI etats des séjours non cloturés
   * @return array la liste des séjours concernés
   */
  public function findSejoursNationaux()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->innerJoin("struct.structureParent", "structureParent")
      ->where("structureParent.echelon = :echelonNational")
      ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL);

    $query_builder = $this->filterOnStatusList($query_builder, Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_SUIVI);
    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findArchivesSejoursNationaux
   * Renvoie la liste des séjour organisés par des structures nationales dans l'état cloturé
   * @uses Sejour::STATUT_SEJOUR_CLOTURE etat de séjour cloturé
   * @return array la liste des séjours concernés
   */
  public function findArchivesSejoursNationaux()
  {
    $query_builder = $this->createQueryBuilder('s')
      ->join('s.structureOrganisatrice', 'struct')
      ->innerJoin("struct.structureParent", "structureParent")
      ->where("structureParent.echelon = :echelonNational")
      ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL);
    $query_builder = $this->filterOnStatusList($query_builder, [Sejour::STATUT_SEJOUR_CLOTURE]);
    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * filterOnStatusList
   * Permet de filtrer la requete en parametre sur les séjour en fonction d'une liste d'état
   * @param  QueryBuilder $query_builder la requête de départ
   * @param  array $statusList la liste des états de séjour à filtrer 
   * @return QueryBuilder la requete incluant le filtre
   */
  public function filterOnStatusList(QueryBuilder $query_builder, array $statusList): QueryBuilder
  {
    $orStatusCondition = "";
    foreach ($statusList as $index => $statut_visible) {
      if ($orStatusCondition != "") {
        $orStatusCondition .= " OR ";
      }
      $orStatusCondition .= "(s.status LIKE :status_" . $index . ")";
      $query_builder->setParameter("status_" . $index, '%' . $statut_visible . '%');
    }
    $query_builder->andWhere($orStatusCondition);
    return $query_builder;
  }

  /**
   * findSejoursADemarrer
   * Renvoie la lsite des séjours dans l'état "validé" pour lesquels la date de début est inférieure à la date passée en paramètre
   * @param  DateTime $dateDebut la date de début limite
   * @uses Sejour::STATUT_SEJOUR_VALIDE l'état "séjour validé"
   * @return array la liste des séjours concernés
   */
  public function findSejoursADemarrer(DateTime $dateDebut)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->andWhere('s.status LIKE :status')
      ->andWhere('s.dateDebut <= :dateDebut')
      ->setParameter("status", '%' . Sejour::STATUT_SEJOUR_VALIDE . '%')
      ->setParameter('dateDebut', $dateDebut);

    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findSejoursATerminer
   * Renvoie la lsite des séjours dans l'état en cours pour lesquels la date de fin est inférieure à la date passée en paramètre
   * @param  DateTime $dateFin la date de fin limite
   * @uses Sejour::STATUT_SEJOUR_EN_COURS l'état séjour en cours
   * @return array la liste des séjours concernés
   */
  public function findSejoursATerminer(DateTime $dateFin)
  {
    $query_builder = $this->createQueryBuilder('s')
      ->andWhere('s.status LIKE :status')
      ->andWhere('s.dateFin <= :dateFin')
      ->setParameter("status", '%' . Sejour::STATUT_SEJOUR_EN_COURS . '%')
      ->setParameter('dateFin', $dateFin);

    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findSejoursACloturer
   * Renvoie la lsite des séjours dans l'état terminé qui se sont terminés au moins depuis la date passée en paramètre
   * @param  DateTime $dateFinSejourLimite la date limite
   * @uses Sejour::STATUT_SEJOUR_TERMINE l'état terminé du séjour
   * @return array la liste des séjours concernés 
   */
  public function findSejoursACloturer(DateTime $dateFinSejourLimite): array
  {
    $query_builder = $this->createQueryBuilder('s')
      ->andWhere('s.status LIKE :status')
      ->andWhere('s.dateFin <= :dateFin')
      ->setParameter("status", '%' . Sejour::STATUT_SEJOUR_TERMINE . '%')
      ->setParameter('dateFin', $dateFinSejourLimite);

    return $query_builder->getQuery()
      ->getResult();
  }


  /**
   * findSejourParEtat
   * Renvoie la liste des séjours dans l'état passé en paramètre
   * @param  String $etat l'état de séjour
   * @return array la liste des séjours concernés
   */
  public function findSejourParEtat(String $etat)
  {
    $query_builder = $this->createQueryBuilder('s');

    $query_builder = $this->filterOnStatusList($query_builder, [$etat]);

    return $query_builder->getQuery()
      ->getResult();
  }

  /**
   * findEndedBefore
   * Renvoie la liste des séjours dont la date de fin est inférieure à la date passée en paramètre
   * @param  DateTime $dateUpdate
   * @return array la liste des séjours concernés
   */
  public function findEndedBefore(DateTime $dateUpdate)
  {
    return $this->createQueryBuilder('s')
      ->where('s.dateFin < :dateUpdate')
      ->setParameter('dateUpdate', $dateUpdate)
      ->getQuery()
      ->getResult();;
  }
}
