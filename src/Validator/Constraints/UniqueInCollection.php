<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Attribute\HasNamedArguments;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class UniqueInCollection extends Constraint
{

    #[HasNamedArguments]
    public function __construct(
        public mixed $fields,
        public ?string $message,
        ?array $groups = null,
        mixed $payload = null,
    ) {
        parent::__construct([], $groups, $payload);
    }


    public function __sleep(): array
    {
        return array_merge(
            parent::__sleep(),
            [
                'fields'
            ]
        );
    }
    public function validatedBy()
    {
        return static::class . 'Validator';
    }
}
