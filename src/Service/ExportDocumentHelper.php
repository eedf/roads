<?php

namespace App\Service;

use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\ParticipantNonAdherent;
use App\Entity\ParticipationSejourUnite;
use App\Entity\QualificationDirection;
use App\Entity\RoleEncadrantSejour;
use App\Entity\Sejour;
use App\Entity\StructureRegroupementSejour;
use App\Entity\UtilisationLieuSejour;
use App\Utils\ResumeEffectifsSejour;
use libphonenumber\PhoneNumber;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Element;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpWord\Element\Table;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use ZipArchive;

/**
 * Helper pour l'export de documents conernant un séjour particulier
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ExportDocumentHelper
{

    const NOM_TEMPLATE_EXPORT_TAM = "";
    const CORRESPONDANCES_DIPLOMES_TAM = [
        QualificationDirection::DIPLOME_DIRECTION_BAFD => [
            "categorie" => "MSJS DIR",
            "diplome" => "BAFD"
        ],
        QualificationDirection::DIPLOME_DIRECTION_DSF => [
            "categorie" => "SCOUT DIR",
            "diplome" => "CA DIR SF"
        ],
        RoleEncadrantSejour::DIPLOME_ANIMATION_BAFA => [
            "categorie" => "MSJS ANIM",
            "diplome" => "BAFA"
        ],
        RoleEncadrantSejour::DIPLOME_ANIMATION_ASF => [
            "categorie" => "SCOUT ANIM",
            "diplome" => "CAFA SF"
        ],
        QualificationDirection::DIPLOME_DIRECTION_EQUIV => [
            "categorie" => "reportez-vous au tableau des équivalences",
            "diplome" => "reportez-vous au tableau des équivalences"
        ]
    ];

    const STYLE_ENTETE_EXPORT_TABLEUR = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => [
                'argb' => 'AEC7D2',
            ],
        ],
    ];

    const FORMAT_DATE_NAISSANCE_TABLEUR = 'dd/mm/yyyy';


    /**
     * Permet d'exporter un document au format .docx contenant les informations nécessaire à la déclaration TAM du séjour
     *
     * Génération du fichier à partir du template **templates/Template TAM ROADS.docx**
     * Le fichier est généré sous **temp/exportTam_{id}.docx**
     * @param  Sejour $sejour
     * @param  ResumeEffectifsSejour $resumeEffectifs Helper pour calculer les effectifs d'un séjour
     * @return void
     */
    public function exportDocumentTAM(Sejour $sejour, ResumeEffectifsSejour $resumeEffectifs)
    {
        $templateProcessor = new TemplateProcessor('templates/Template TAM ROADS.docx');

        //Période et effectifs maximum accueillis 
        $templateProcessor->setValue('intituleSejour', $sejour->getIntitule());

        $templateProcessor->setValue('dateDebutSejour', $sejour->getDateDebut()->format('d/m/Y'));
        $templateProcessor->setValue('dateFinSejour', $sejour->getDateFin()->format('d/m/Y'));
        $templateProcessor->setValue('nbMineurs6a13ans', $resumeEffectifs->getNbEnfantsMoins14ans());
        $templateProcessor->setValue('nbMineurs14a17ans', $resumeEffectifs->getNbEnfantsPlus14ans());
        $templateProcessor->setValue('nbMineursTotal', $resumeEffectifs->getNbTotalEnfants());
        $templateProcessor->setValue('nbMineursHandicapes', $resumeEffectifs->getNbEnfantsSituationHandicap());

        if ($sejour->getColoApprenante()) {
            $coloApprenantes = "Oui";
        } else {
            $coloApprenantes = "Non";
        }
        $templateProcessor->setValue('coloApprenantes', $coloApprenantes);

        //---------------------
        //LOCALISATION OU ITINERAIRE
        $replacements_lieux = array();

        /** @var UtilisationLieuSejour $lieu */
        foreach ($sejour->getUtilisationsLieuSejour() as $lieu) {
            if ($sejour->getInternational()) {
                $lieuEnFrance = "Non";
                $libellePaysOuDepartement = "Pays";
            } else {
                $lieuEnFrance = "Oui";
                $libellePaysOuDepartement = "Département";
            }
            if ($lieu->getLieu()) {
                $replacements_lieux[] = [
                    "dateDebutLieu" => $lieu->getDateArrivee()->format('d/m/Y'),
                    "dateFinLieu" => $lieu->getDateDepart()->format('d/m/Y'),
                    "lieuEnFrance" => $lieuEnFrance,
                    "adresseLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getAdresse() . " " . $lieu->getLieu()->getLocalisation()->getComplementAdresse() :  "Non renseignée",
                    "communeLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getNomCommune() :  "Non renseignée",
                    "codePostalLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getCodePostalCommune() :  "Non renseigné",
                    "paysLieu" => $lieu->getLieu()->getLocalisation() ? ($sejour->getInternational() ? $lieu->getLieu()->getLocalisation()->getPays() : $lieu->getLieu()->getLocalisation()->getDepartement()) :  "Non renseigné",
                    "libellePaysOuDepartement" => $libellePaysOuDepartement
                ];
            } else {
                $replacements_lieux[] = [
                    "dateDebutLieu" => $lieu->getDateArrivee()->format('d/m/Y'),
                    "dateFinLieu" => $lieu->getDateDepart()->format('d/m/Y'),
                    "lieuEnFrance" => $lieuEnFrance,
                    "adresseLieu" =>   "Non renseignée",
                    "communeLieu" =>  "Non renseignée",
                    "codePostalLieu" => "Non renseigné",
                    "paysLieu" =>  "Non renseigné",
                    "libellePaysOuDepartement" => $libellePaysOuDepartement
                ];
            }
        }
        $templateProcessor->cloneBlock('bloc_lieu', 0, true, false, $replacements_lieux);


        //---------------------
        //DECLARANT
        /** @var Personne $organisateur */
        $organisateur = $sejour->getStructureOrganisatrice()->getOrganisateurs()[0];

        if ($organisateur->getGenre() == Personne::PERSONNE_GENRE_FEMININ) {
            $civiliteResponsableStructure = "Madame";
        } else if ($organisateur->getGenre() == Personne::PERSONNE_GENRE_MASCULIN) {
            $civiliteResponsableStructure = "Monsieur";
        } else {
            $civiliteResponsableStructure = "Madame ou Monsieur";
        }
        $templateProcessor->setValue('civiliteResponsableStructure', $civiliteResponsableStructure);
        $templateProcessor->setValue('nomStructureOrganisatrice', $sejour->getStructureOrganisatrice()->getNom());
        $templateProcessor->setValue('nomResponsableStructure', $organisateur->getNom());
        $templateProcessor->setValue('prenomResponsableStructure', $organisateur->getPrenom());
        $templateProcessor->setValue('dateNaissanceResponsableStructure', $organisateur->getDateNaissance()->format('d/m/Y'));

        //---------------------
        //PERSONNE A PREVENIR EN CAS D'URGENCE

        $templateProcessor->setValue('nomDirecteur', $sejour->getDirecteur()->getNom());
        $templateProcessor->setValue('prenomDirecteur', $sejour->getDirecteur()->getPrenom());
        $templateProcessor->setValue('telDirecteur', $this->ecritureNumeroTelephone($sejour->getDirecteur()->getTelephoneUsage()));
        $templateProcessor->setValue('emailDirecteur', $sejour->getDirecteur()->getAdresseMailUsage());

        //---------------------
        //EQUIPE ENCADREMENT
        $replacements_encadrants = array();

        //pour le directeur:
        if ($sejour->getDirecteur()->getGenre() == Personne::PERSONNE_GENRE_FEMININ) {
            $civiliteEncadrant = "Madame";
        } else if ($sejour->getDirecteur()->getGenre() == Personne::PERSONNE_GENRE_MASCULIN) {
            $civiliteEncadrant = "Monsieur";
        } else {
            $civiliteEncadrant = "Madame ou Monsieur";
        }

        $nomNaissanceEncadrant = $sejour->getDirecteur()->getNom();
        $prenomEncadrant = $sejour->getDirecteur()->getPrenom();
        $dateNaissanceEncadrant = $sejour->getDirecteur()->getDateNaissance()->format('d/m/Y');
        $telEncadrant = $this->ecritureNumeroTelephone($sejour->getDirecteur()->getTelephoneUsage());
        $departementOuPaysNaissance = "";
        $communeNaissance = "";
        $qualiteDiplomeEncadrant = $sejour->getQualificationDirection() ? $sejour->getQualificationDirection()->getQualiteDirection() : 'Non renseigné';
        $correspondance_trouvee = false;
        $diplomeEncadrant = "inconnu";
        $categorieDiplomeEncadrant = "inconnue";

        foreach (self::CORRESPONDANCES_DIPLOMES_TAM as $diplome => $correspondance) {
            if ($sejour->getQualificationDirection()) {
                if (!$correspondance_trouvee && in_array($diplome, $sejour->getQualificationDirection()->getDiplomesDirection())) {
                    $correspondance_trouvee = true;
                    $diplomeEncadrant = $correspondance['diplome'];
                    $categorieDiplomeEncadrant = $correspondance['categorie'];
                }
            }
        }

        $replacements_encadrants[] = [
            "fonctionEncadrant" => "Direct·eur·rice",
            "civiliteEncadrant" => $civiliteEncadrant,
            "nomNaissanceEncadrant" => $nomNaissanceEncadrant,
            "prenomEncadrant" => $prenomEncadrant,
            "dateNaissanceEncadrant" => $dateNaissanceEncadrant,
            "telEncadrant" => $telEncadrant,
            "categorieDiplomeEncadrant" => $categorieDiplomeEncadrant,
            "diplomeEncadrant" => $diplomeEncadrant,
            "qualiteDiplomeEncadrant" => $qualiteDiplomeEncadrant,
            'departementOuPaysNaissance' => $departementOuPaysNaissance,
            'communeNaissance' => $communeNaissance,
        ];



        //pour les autres encadrant·e·s
        /** @var RoleEncadrantSejour $roleEncadrant */
        foreach ($sejour->getRolesEncadrants() as $roleEncadrant) {
            if ($roleEncadrant->getEstEncadrantEEDF()) {
                if ($roleEncadrant->getEncadrantEEDF()->getGenre() == Personne::PERSONNE_GENRE_FEMININ) {
                    $civiliteEncadrant = "Madame";
                } else if ($roleEncadrant->getEncadrantEEDF()->getGenre() == Personne::PERSONNE_GENRE_MASCULIN) {
                    $civiliteEncadrant = "Monsieur";
                } else {
                    $civiliteEncadrant = "Madame ou Monsieur";
                }
                $nomNaissanceEncadrant = $roleEncadrant->getEncadrantEEDF()->getNom();
                $prenomEncadrant = $roleEncadrant->getEncadrantEEDF()->getPrenom();
                $dateNaissanceEncadrant = $roleEncadrant->getEncadrantEEDF()->getDateNaissance()->format('d/m/Y');
                $telEncadrant = $this->ecritureNumeroTelephone($roleEncadrant->getEncadrantEEDF()->getTelephoneUsage());
                $departementOuPaysNaissance = "";
                $communeNaissance = "";
            } else {
                if ($roleEncadrant->getInfosEncadrantNonEEDF()->getGenre() == Personne::PERSONNE_GENRE_FEMININ) {
                    $civiliteEncadrant = "Madame";
                } else if ($roleEncadrant->getInfosEncadrantNonEEDF()->getGenre() == Personne::PERSONNE_GENRE_MASCULIN) {
                    $civiliteEncadrant = "Monsieur";
                } else {
                    $civiliteEncadrant = "Madame ou Monsieur";
                }
                $nomNaissanceEncadrant = $roleEncadrant->getInfosEncadrantNonEEDF()->getNom();
                $prenomEncadrant = $roleEncadrant->getInfosEncadrantNonEEDF()->getPrenom();
                $dateNaissanceEncadrant = $roleEncadrant->getInfosEncadrantNonEEDF()->getDateNaissance()->format('d/m/Y');
                $telEncadrant = $this->ecritureNumeroTelephone($roleEncadrant->getInfosEncadrantNonEEDF()->getTelephone());
                $departementOuPaysNaissance = $roleEncadrant->getInfosEncadrantNonEEDF()->getDepartementOuPaysNaissance();
                $communeNaissance = $roleEncadrant->getInfosEncadrantNonEEDF()->getCommuneNaissance();
            }

            if (in_array(RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE, $roleEncadrant->getFonction())) {
                $fonctionEncadrant = "Direct·eur·rice adjoint·e";
            } else if ($roleEncadrant->getEquipe() == RoleEncadrantSejour::EQUIPE_ANIMATION) {
                $fonctionEncadrant = "Animat·eur·rice";
            } else {
                $fonctionEncadrant = "Autre";
            }

            $qualiteDiplomeEncadrant = $roleEncadrant->getQualiteAnimation();
            $correspondance_trouvee = false;
            $diplomeEncadrant = "inconnu";
            $categorieDiplomeEncadrant = "inconnue";
            foreach (self::CORRESPONDANCES_DIPLOMES_TAM as $diplome => $correspondance) {
                if (!$correspondance_trouvee && in_array($diplome, $roleEncadrant->getDiplomesAnimation())) {
                    $correspondance_trouvee = true;
                    $diplomeEncadrant = $correspondance['diplome'];
                    $categorieDiplomeEncadrant = $correspondance['categorie'];
                }
            }
            $replacements_encadrants[] = [
                "fonctionEncadrant" => $fonctionEncadrant,
                "civiliteEncadrant" => $civiliteEncadrant,
                "nomNaissanceEncadrant" => $nomNaissanceEncadrant,
                "prenomEncadrant" => $prenomEncadrant,
                "dateNaissanceEncadrant" => $dateNaissanceEncadrant,
                "telEncadrant" => $telEncadrant,
                "categorieDiplomeEncadrant" => $categorieDiplomeEncadrant,
                "diplomeEncadrant" => $diplomeEncadrant,
                "qualiteDiplomeEncadrant" => $qualiteDiplomeEncadrant,
                'departementOuPaysNaissance' => $departementOuPaysNaissance,
                'communeNaissance' => $communeNaissance,
            ];
        }

        $templateProcessor->cloneBlock('bloc_encadrant', 0, true, false, $replacements_encadrants);

        $templateProcessor->saveAs('temp/exportTam' . $sejour->getId() . '.docx');
    }

    /**
     * Permet de générer un document au format docx écapitulant les informations du séjour
     *
     * Génération à partir du template **templates/Template projet de séjour.docx**
     * @param  Sejour $sejour
     * @param  ResumeEffectifsSejour $resumeEffectifs helper permettant de calculer les effectifs
     * @return string l'emplacement du fichier généré
     */
    public function generationDocumentProjetSejour(Sejour $sejour, ResumeEffectifsSejour $resumeEffectifs): string
    {
        $templateProcessor = new TemplateProcessor('templates/Template projet de séjour.docx');

        //Identité :
        $templateProcessor->setValue('intituleSejour', $sejour->getIntitule());
        $templateProcessor->setValue('prenomDirecteur', $sejour->getDirecteur()->getPrenom());
        $templateProcessor->setValue('nomDirecteur', $sejour->getDirecteur()->getNom());

        $datesSejourFormatCourt = $sejour->getDateDebut()->format('d/m/Y') . " - " . $sejour->getDateFin()->format('d/m/Y');

        $templateProcessor->setValue('datesSejourFormatCourt', $datesSejourFormatCourt);


        //IDENTITE DE LA STRUCTURE ORGANISATRICE :
        $templateProcessor->setValue('nomStructureOrganisatrice', $sejour->getStructureOrganisatrice()->getNom());

        $repsonsablesStructures = array();
        /** @var Fonction $fonction */
        foreach ($sejour->getStructureOrganisatrice()->getEquipeGestion()->getFonctions() as $fonction) {
            if (
                $fonction->getType() == Fonction::TYPE_FONCTION_RESPONSABLE
                && $fonction->getDateDebut() <= $sejour->getDateDebut()

                && ($fonction->getDateFin() == null || $fonction->getDateFin() >= $sejour->getDateDebut())
            ) {
                $repsonsablesStructures[] = $fonction->getPersonne()->getPrenom() . " " . $fonction->getPersonne()->getNom();
            }
        }
        $templateProcessor->setValue('prenomNomResponsableStructure', implode(", ", $repsonsablesStructures));
        $templateProcessor->setValue('regionRattachement', $sejour->getStructureOrganisatrice()->getStructureParent()->getNom());
        $templateProcessor->setValue(
            'prenomNomOrganisateurFicheInitiale',
            $sejour->getStructureOrganisatrice()->getPrenomOrganisateurDDCS() . " " . $sejour->getStructureOrganisatrice()->getNomOrganisateurDDCS()
        );

        $templateProcessor->setValue('numeroDeclarationDDCS', $sejour->getStructureOrganisatrice()->getNumeroDeclarationInitiale());
        $templateProcessor->setValue('regionRattachement', $sejour->getStructureOrganisatrice()->getStructureParent()->getNom());
        if ($sejour->getAccompagnant()) {
            $valeurAcc = $sejour->getAccompagnant()->getPrenom() . " " . $sejour->getAccompagnant()->getNom();
        } else {
            $valeurAcc = "Non défini";
        }
        $templateProcessor->setValue('prenomNomPersonneChargeSuiviSejour', $valeurAcc);



        //CARACTÉRISTIQUES GÉNÉRALES DU SÉJOUR
        $templateProcessor->setValue('typeSejour', $sejour->getTypeSejour());
        if ($sejour->getInternational()) {
            $international = "Oui";
        } else {
            $international = "Non";
        }
        $templateProcessor->setValue('international', $international);
        if ($sejour->getSejourFranceAvecAccueilEtranger()) {
            $accueilGroupeEtranger = "Oui";
        } else {
            $accueilGroupeEtranger = "Non";
        }
        $templateProcessor->setValue('accueilGroupeEtranger', $accueilGroupeEtranger);
        if ($sejour->getColoApprenante()) {
            $coloApprenante = "Oui";
        } else {
            $coloApprenante = "Non";
        }
        $templateProcessor->setValue('coloApprenante', $coloApprenante);
        if ($sejour->getSejourOuvert()) {
            $sejourOuvert = "Oui";
        } else {
            $sejourOuvert = "Non";
        }
        $templateProcessor->setValue('sejourOuvert', $sejourOuvert);
        if ($sejour->getSejourItinerant()) {
            $sejourItinerant = "Oui";
        } else {
            $sejourItinerant = "Non";
        }
        $templateProcessor->setValue('sejourItinerant', $sejourItinerant);
        if ($sejour->getDispositifAppui() == "Non") {
            $dispositifAppui = "sans Accompagnement";
        } else {
            $dispositifAppui = $sejour->getDispositifAppui();
        }
        $templateProcessor->setValue('dispositifAppui', $dispositifAppui);

        $replacement_structures_regroupement = array();
        /** @var StructureRegroupementSejour $structureRegroupee */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupee) {
            if (!$structureRegroupee->getEstEEDF()) {
                $nom = $structureRegroupee->getNomStructureNonEEDF() . " (Non EEDF)";
            } else {
                $nom = $structureRegroupee->getStructureEEDF()->getNom();
            }

            $replacement_structures_regroupement[] = [
                "nom" => $nom,
            ];
        }
        $templateProcessor->cloneBlock('bloc_structures_regroupement', 0, true, false, $replacement_structures_regroupement);

        $replacement_unites_participantes = array();

        /** @var ParticipationSejourUnite $participationSejourUnite */
        foreach ($sejour->getUnitesStructureOrganisatrice() as $participationSejourUnite) {
            $replacement_unites_participantes[] = [
                "nom" => $participationSejourUnite->getEquipe()->getNom(),
            ];
        }
        /** @var StructureRegroupementSejour $structureRegroupee */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupee) {
            foreach ($structureRegroupee->getUnitesParticipantes() as $participationSejourUnite) {
                $replacement_unites_participantes[] = [
                    "nom" => $participationSejourUnite->getEquipe()->getNom(),
                ];
            }
        }
        $templateProcessor->cloneBlock('bloc_unites_participantes', 0, true, false, $replacement_unites_participantes);



        date_default_timezone_set('Europe/Paris');

        setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
        $templateProcessor->setValue('dateDebutSejour', strftime("%A %d %B %G", $sejour->getDateDebut()->getTimestamp()));
        $templateProcessor->setValue('dateFinSejour', strftime("%A %d %B %G", $sejour->getDateFin()->getTimestamp()));

        $replacements_lieux = array();

        /** @var UtilisationLieuSejour $lieu */
        foreach ($sejour->getUtilisationsLieuSejour() as $lieu) {
            if ($sejour->getInternational()) {
                $lieuEnFrance = "Non";
                $libellePaysOuDepartement = "Pays";
            } else {
                $lieuEnFrance = "Oui";
                $libellePaysOuDepartement = "Département";
            }


            if ($lieu->getLieu()) {
                $replacements_lieux[] = [
                    "nomLieu" => $lieu->getLieu()->getNom(),
                    "dateDebutLieu" => $lieu->getDateArrivee()->format('d/m/Y'),
                    "dateFinLieu" => $lieu->getDateDepart()->format('d/m/Y'),
                    "lieuEnFrance" => $lieuEnFrance,
                    "adresseLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getAdresse() . " " . $lieu->getLieu()->getLocalisation()->getComplementAdresse() :  "Non renseignée",
                    "communeLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getNomCommune() :  "Non renseignée",
                    "codePostalLieu" => $lieu->getLieu()->getLocalisation() ? $lieu->getLieu()->getLocalisation()->getCodePostalCommune() :  "Non renseigné",
                    "paysLieu" => $lieu->getLieu()->getLocalisation() ? ($sejour->getInternational() ? $lieu->getLieu()->getLocalisation()->getPays() : $lieu->getLieu()->getLocalisation()->getDepartement()) :  "Non renseigné",
                    "libellePaysOuDepartement" => $libellePaysOuDepartement
                ];
            } else {
                $replacements_lieux[] = [
                    "nomLieu" =>  "Non renseigné",
                    "dateDebutLieu" => $lieu->getDateArrivee()->format('d/m/Y'),
                    "dateFinLieu" => $lieu->getDateDepart()->format('d/m/Y'),
                    "lieuEnFrance" => $lieuEnFrance,
                    "adresseLieu" =>   "Non renseignée",
                    "communeLieu" =>  "Non renseignée",
                    "codePostalLieu" => "Non renseigné",
                    "paysLieu" =>  "Non renseigné",
                    "libellePaysOuDepartement" => $libellePaysOuDepartement
                ];
            }
        }
        $templateProcessor->cloneBlock('bloc_lieu', 0, true, false, $replacements_lieux);


        //---------------------
        //DIRECTION
        /** @var Personne $directeur */
        $directeur = $sejour->getDirecteur();

        $templateProcessor->setValue('nomDirecteur', $directeur->getNom());
        $templateProcessor->setValue('prenomDirecteur', $directeur->getPrenom());

        $templateProcessor->setValue('numeroAdherentDirecteur', $directeur->getNumeroAdherentOuMatricule());
        $templateProcessor->setValue('dateNaissanceDirecteur', $directeur->getDateNaissance()->format('d/m/Y'));
        $templateProcessor->setValue('telephoneDirecteur', $this->ecritureNumeroTelephone($directeur->getTelephoneUsage()));
        $templateProcessor->setValue('mailDirecteur', $directeur->getAdresseMailUsage());

        $diplomesDirection = "Non défini";
        if ($sejour->getQualificationDirection() && ($sejour->getQualificationDirection()->getDiplomesDirection() != null)) {
            $diplomesDirection = implode(", ", $sejour->getQualificationDirection()->getDiplomesDirection());
            if (in_array(QualificationDirection::DIPLOME_DIRECTION_EQUIV, $sejour->getQualificationDirection()->getDiplomesDirection())) {
                $diplomesDirection .= " (" . $sejour->getQualificationDirection()->getEquivalenceDiplomeDirection() . ")";
            }
        }

        $templateProcessor->setValue('typeDiplomeDirection', $diplomesDirection);

        //---------------------
        //EFFECTIFS
        $templateProcessor->setValue('nbEncadrantsLutins', $resumeEffectifs->getNbEncadrantsLutins());
        $templateProcessor->setValue('nbEncadrantsLouveteaux', $resumeEffectifs->getNbEncadrantsLouveteaux());
        $templateProcessor->setValue('nbEncadrantsEcles', $resumeEffectifs->getNbEncadrantsEcles());
        $templateProcessor->setValue('nbEncadrantsAines', $resumeEffectifs->getNbEncadrantsAines());
        $templateProcessor->setValue('nbEncadrantsNonEEDF', $resumeEffectifs->getNbEncadrantsStructureNonEEDF());
        $templateProcessor->setValue('nbEncadrantsSupport', $resumeEffectifs->getNbEncadrantsEquipeSupport());
        $templateProcessor->setValue('nbEncadrantsNonAffectes', $resumeEffectifs->getNbEncadrantsEquipeAnimationAutres());
        $templateProcessor->setValue('nbTotalEncadrants', $resumeEffectifs->getNbTotalEncadrants());

        $templateProcessor->setValue('nbEnfantsLutins', $resumeEffectifs->getNbEnfantsLutins());
        $templateProcessor->setValue('nbEnfantsLouveteaux', $resumeEffectifs->getNbEnfantsLouveteaux());
        $templateProcessor->setValue('nbEnfantsEcles', $resumeEffectifs->getNbEnfantsEcles());
        $templateProcessor->setValue('nbEnfantsAines', $resumeEffectifs->getNbEnfantsAines());
        $templateProcessor->setValue('nbEnfantsNonEEDF', $resumeEffectifs->getNbEnfantsStructureNonEEDF());
        $templateProcessor->setValue('nbTotalEnfants', $resumeEffectifs->getNbTotalEnfants());
        $templateProcessor->setValue('nbMineursHandicap', $resumeEffectifs->getNbEnfantsSituationHandicap());

        $templateProcessor->setValue('nbDiplomes', $resumeEffectifs->getNbEncDiplomes());
        $templateProcessor->setValue('nbStagiaires', $resumeEffectifs->getNbEncStagiaires());
        $templateProcessor->setValue('nbNonDiplomes', $resumeEffectifs->getNbEncNonDiplomes());
        $templateProcessor->setValue('nbPSC1', $resumeEffectifs->getNbEncPSC1());
        $templateProcessor->setValue('nbPermisB', $resumeEffectifs->getNbEncPermisB());

        //DEMARCHES PEDAGOGIQUES

        if ($sejour->getDocumentProjetPeda() && $sejour->getDocumentProjetPeda()->getDocumentName() && $sejour->getDocumentProjetPeda()->getDocumentName()) {
            $templateProcessor->setValue('annexePeda', "annexe n°2");
        } else {
            $templateProcessor->setValue('annexePeda', "annexe n°2 (annexe manquante)");
        }


        if ($sejour->getColoApprenante()) {
            if ($sejour->getDocumentSocleCommunColosApprenantes()) {
                $replacements_colos_apprenantes[] = [
                    'annexe' => "annexe n°5"
                ];
            } else {
                $replacements_colos_apprenantes[] = [
                    'annexe' => "annexe n°5 (annexe manquante)"
                ];
            }
            $templateProcessor->cloneBlock('bloc_colos_apprenantes', 0, true, false, $replacements_colos_apprenantes);
        } else {
            $templateProcessor->cloneBlock('bloc_colos_apprenantes', 0);
        }
        if ($sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger()) {
            if ($sejour->getAspectInternational()) {
                $replacements_international[] = [
                    "identificationPartenaireInternational" => $sejour->getAspectInternational()->getPartenaireInternational()
                ];
            } else {
                $replacements_international[] = [
                    "identificationPartenaireInternational" => "Non rensigné"
                ];
            }

            $templateProcessor->cloneBlock('bloc_sejour_international', 0, true, false, $replacements_international);
        } else {
            $templateProcessor->cloneBlock('bloc_sejour_international', 0);
        }


        //ASPECTS LOGISTIQUES

        $prixSejour = "Non renseigné";
        $annexeBudget = "annexe n°3 (annexe manquante)";
        if ($sejour->getAspectBudgetSejour()) {
            $prixSejour = $sejour->getAspectBudgetSejour()->getPrixMoyen() . " €";
            if ($sejour->getAspectBudgetSejour()->getBudgetPrevisionnel() && $sejour->getAspectBudgetSejour()->getBudgetPrevisionnel()->getDocumentName()) {
                $annexeBudget = "annexe n°3";
            }
        }
        $templateProcessor->setValue('prixSejour', $prixSejour);
        $templateProcessor->setValue('annexeBudget', $annexeBudget);


        $templateProcessor->setComplexBlock('transportParticipants', $this->ecritureTexteHtmlDansWord($sejour->getDescriptionOrganisationTransport()));
        $templateProcessor->setComplexBlock('projetAlimentaire', $this->ecritureTexteHtmlDansWord($sejour->getAspectAlimentaireSejour() ? $sejour->getAspectAlimentaireSejour()->getChoixOrganisation() : 'Non renseigné'));

        if ($sejour->getAspectAlimentaireSejour() && $sejour->getAspectAlimentaireSejour()->getGrilleMenus() != null) {
            $replacements_grille_menus[] = [
                'annexe' => "annexe n°4"
            ];
        } else {
            $replacements_grille_menus[] = [
                'annexe' => "annexe n°4 (annexe manquante)"
            ];
        }
        $templateProcessor->cloneBlock('bloc_grille_menus', 0, true, false, $replacements_grille_menus);
        $templateProcessor->setComplexBlock('moyensPresentationAmont', $this->ecritureTexteHtmlDansWord($sejour->getAspectCommunicationSejour() ? $sejour->getAspectCommunicationSejour()->getPresentationAmont() : 'Non renseigné'));


        if ($sejour->getAspectCommunicationSejour() && $sejour->getAspectCommunicationSejour()->getDateReunionInformation()) {
            $valeurDatePres = strftime("%A %d %B %G", $sejour->getAspectCommunicationSejour()->getDateReunionInformation()->getTimestamp());
        } else {
            $valeurDatePres = "Non défini";
        }
        $templateProcessor->setValue('dateReunionPresentation', $valeurDatePres);
        $templateProcessor->setComplexBlock('moyenComPendant', $this->ecritureTexteHtmlDansWord($sejour->getAspectCommunicationSejour() ? $sejour->getAspectCommunicationSejour()->getMoyenCommunicationPendant() : "Non renseigné"));
        $templateProcessor->setComplexBlock('moyenComApres', $this->ecritureTexteHtmlDansWord($sejour->getAspectCommunicationSejour() ? $sejour->getAspectCommunicationSejour()->getMoyenRestitution() : "Non renseigné"));
        $templateProcessor->setValue('lienSiteValorisation', $sejour->getAspectCommunicationSejour() ? $sejour->getAspectCommunicationSejour()->getSiteValorisation() : "Non renseigné");

        $filename = 'temp/exportProjet' . $sejour->getId() . '.docx';

        $templateProcessor->saveAs($filename);

        return $filename;
    }

    /**
     * Génere une archive Zip du projet de séjour, en générant les documents nécessaires
     * Les documents dans l'archive sont:
     *  - Le document récapitulant les informations du projet de séjour (généré)
     *  - Le tableur des participant·e·s est encadrant·e·s (généré)
     *  - Le document de projet pédagogique
     *  - Le budget prévisionnel du séjour
     *  - La liste des menus du séjour
     *  - Le document socle des colos apprenantes
     * 
     * @param  Sejour $sejour
     * @param  UploaderHelper $uploaderHelper
     * @param  ResumeEffectifsSejour $resumeEffectifs helper pour le calcul des effectifs
     * @return string l'emplacement de l'archive générée
     * @see ExportDocumentHelper::generationDocumentProjetSejour()
     * @see ExportDocumentHelper::generationTableurParticipants()
     */
    public function generationArchiveProjetSejour(Sejour $sejour, UploaderHelper $uploaderHelper,  ResumeEffectifsSejour $resumeEffectifs): string
    {
        $zip = new ZipArchive();
        $filename = 'temp/projetSejour' . $sejour->getId() . '.zip';

        $zip->open($filename, ZipArchive::CREATE);
        $filenameDocProjetSejour = $this->generationDocumentProjetSejour($sejour, $resumeEffectifs);
        $zip->addFile($filenameDocProjetSejour, "Informations générales du séjour " . $sejour->getintitule() . ".docx");
        $filenameEffectifsSejour = $this->generationTableurParticipants($sejour);
        $zip->addFile($filenameEffectifsSejour, "annexe 1 : Effectifs du séjour " . $sejour->getintitule() . ".xlsx");


        if ($sejour->getDocumentProjetPeda() && $sejour->getDocumentProjetPeda()->getDocumentName()) {
            $zip->addFile("." . $uploaderHelper->asset($sejour->getDocumentProjetPeda()), "annexe 2 : Projet pédagogique du séjour " . $sejour->getintitule() . "." . $sejour->getDocumentProjetPeda()->getExtension());
        }
        if (
            $sejour->getAspectBudgetSejour() && $sejour->getAspectBudgetSejour()->getBudgetPrevisionnel() != null
            && $sejour->getAspectBudgetSejour()->getBudgetPrevisionnel()->getDocumentName()
        ) {
            $zip->addFile("." . $uploaderHelper->asset($sejour->getAspectBudgetSejour()->getBudgetPrevisionnel()), "annexe 3 : Budget du séjour " . $sejour->getintitule() . "." . $sejour->getAspectBudgetSejour()->getBudgetPrevisionnel()->getExtension());
        }

        if (
            $sejour->getAspectAlimentaireSejour() && $sejour->getAspectAlimentaireSejour()->getGrilleMenus() != null
            && $sejour->getAspectAlimentaireSejour()->getGrilleMenus()->getDocumentName()
        ) {
            $zip->addFile("." . $uploaderHelper->asset($sejour->getAspectAlimentaireSejour()->getGrilleMenus()), "annexe 4 : Menus du séjour " . $sejour->getintitule() . "." . $sejour->getAspectAlimentaireSejour()->getGrilleMenus()->getExtension());
        }
        if ($sejour->getDocumentSocleCommunColosApprenantes()) {
            $zip->addFile("." . $uploaderHelper->asset($sejour->getDocumentSocleCommunColosApprenantes()), "annexe 5 : Liens socle commun du séjour " . $sejour->getintitule() . "." . $sejour->getDocumentSocleCommunColosApprenantes()->getExtension());
        }

        $zip->close();
        // suppression
        unlink($filenameDocProjetSejour);
        unlink($filenameEffectifsSejour);


        return $filename;
    }

    /**
     * Génération d'un tableur au format xlsx contenant la liste des participant·e·s et encadrant·e·s du séjour
     *
     * @param  Sejour $sejour
     * @return string l'emplacement du fichier généré
     * @see ExportDocumentHelper::exportOngletsParticipants() pour générer l'onglet des participant·e.s
     * @see ExportDocumentHelper::exportOngletsEncadrants() pour générer l'onglet des participant·e.s
     */
    public function generationTableurParticipants(Sejour $sejour): string
    {
        $spreadsheet = new Spreadsheet();

        /* @var $sheetAccueilIndividuel \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */

        $spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
        $this->exportOngletsParticipants($spreadsheet, $sejour);
        $this->exportOngletsEncadrants($spreadsheet, $sejour);

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system

        // Create the excel file in the tmp directory of the system

        $filename = 'temp/exportParticipants' . $sejour->getId() . '.xlsx';
        $writer->save($filename);
        return $filename;
    }

    /**
     * Genère l'onglet de tableur des participant·e·s d'un séjour
     *
     * @param  Spreadsheet $spreadsheet le classeur auquel ajouter l'onglet
     * @param  Sejour $sejour
     * @return void
     */
    public function exportOngletsParticipants(Spreadsheet $spreadsheet, Sejour $sejour)
    {


        $styleContenuArray = [
            'borders' => [
                'vertical' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
                ],
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
                ],
            ],
        ];

        $sheetParticipants = $spreadsheet->getActiveSheet();
        $sheetParticipants->setTitle("Participant·e·s");



        $n = 1;
        $COL_PARTICIPANTS_STRUCTURE = $n++;
        $COL_PARTICIPANTS_UNITE = $n++;

        $COL_PARTICIPANTS_GENRE = $n++;
        $COL_PARTICIPANTS_NOM = $n++;
        $COL_PARTICIPANTS_PRENOM = $n++;
        $COL_PARTICIPANTS_DATE_NAISSANCE = $n++;
        $COL_PARTICIPANTS_LIEU_NAISSANCE = $n++;

        $COL_PARTICIPANTS_NUMERO_ADHERENT = $n++;
        $COL_PARTICIPANTS_TEL_1 = $n++;
        $COL_PARTICIPANTS_MAIL_1 = $n++;
        $COL_PARTICIPANTS_ADRESSE_POSTALE = $n++;

        $COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_1 = $n++;
        $COL_PARTICIPANTS_MAIL_RESP_LEGAL_1 = $n++;
        $COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_1 = $n++;

        $COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_2 = $n++;
        $COL_PARTICIPANTS_MAIL_RESP_LEGAL_2 = $n++;
        $COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_2 = $n++;

        $COL_PARTICIPANTS_DROIT_IMAGE = $n++;


        $HEADER_PARTICIPANTS = [
            $COL_PARTICIPANTS_STRUCTURE => ["label" => "Statut"],
            $COL_PARTICIPANTS_UNITE => ["label" => "Unité"],
            $COL_PARTICIPANTS_GENRE => ["label" => "Civilité"],
            $COL_PARTICIPANTS_NOM => ["label" => "Nom"],
            $COL_PARTICIPANTS_PRENOM => ["label" => "Prénom"],
            $COL_PARTICIPANTS_DATE_NAISSANCE => ["label" => "Date de naissance"],
            $COL_PARTICIPANTS_LIEU_NAISSANCE => ["label" => "Lieu de naissance"],
            $COL_PARTICIPANTS_GENRE => ["label" => "Civilité"],
            $COL_PARTICIPANTS_NUMERO_ADHERENT => ["label" => "Numéro d'adhérent·e"],
            $COL_PARTICIPANTS_TEL_1 => ["label" => "Numéro de téléphone 1"],
            $COL_PARTICIPANTS_MAIL_1 => ["label" => "Adresse-mail 1"],
            $COL_PARTICIPANTS_ADRESSE_POSTALE => ["label" => "Adresse postale"],

            $COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_1 => ["label" => "Nom prénom responsable légal·e 1"],
            $COL_PARTICIPANTS_MAIL_RESP_LEGAL_1 => ["label" => "Adresse mail responsable légal·e 2"],
            $COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_1 => ["label" => "Adresse postale responsable légal·e 1"],

            $COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_2 => ["label" => "Nom prénom responsable légal·e 2"],
            $COL_PARTICIPANTS_MAIL_RESP_LEGAL_2 => ["label" => "Adresse mail responsable légal·e 2"],
            $COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_2 => ["label" => "Adresse postale responsable légal·e 2"],

            $COL_PARTICIPANTS_DROIT_IMAGE => ["label" => "Droit à l'image"],
        ];

        foreach ($HEADER_PARTICIPANTS as $index_colonne => $info_colonne) {
            $sheetParticipants->setCellValueByColumnAndRow($index_colonne, 1, $info_colonne["label"]);
            $sheetParticipants->getColumnDimensionByColumn($index_colonne)->setAutoSize(true);
        }

        //Application du style d'en-têtes
        $sheetParticipants->getStyleByColumnAndRow($COL_PARTICIPANTS_STRUCTURE, 1, $COL_PARTICIPANTS_DROIT_IMAGE, 1)->applyFromArray(self::STYLE_ENTETE_EXPORT_TABLEUR);


        //Liste de tous les participants au séjour: 
        $structures = array();
        $structures[$sejour->getStructureOrganisatrice()->getNom()] = $sejour->getUnitesStructureOrganisatrice();


        /** @var StructureRegroupementSejour $structureRegroupement */
        foreach ($sejour->getStructuresRegroupeesParticipantes() as $structureRegroupement) {
            //On ne connait que les participants des structures EEDF
            if ($structureRegroupement->getEstEEDF()) {
                $structures[$structureRegroupement->getStructureEEDF()->getNom()] = $structureRegroupement->getUnitesParticipantes();
            }
        }

        $row = 2;

        /** @var ParticipationSejourUnite $unitesParticipantes */
        foreach ($structures as $nomStructure => $unitesParticipantes) {
            /** @var ParticipationSejourUnite $uniteParticipante */
            foreach ($unitesParticipantes as $uniteParticipante) {

                /** @var Personne $personne */
                foreach ($uniteParticipante->getParticipantsAdherents() as $personne) {
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_STRUCTURE, $row, $nomStructure);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_UNITE, $row, $uniteParticipante->getEquipe()->getNom());

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM, $row, $personne->getNom());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_PRENOM, $row, $personne->getPrenom());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_DATE_NAISSANCE, $row, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($personne->getDateNaissance()));
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_GENRE, $row, $personne->getGenre());

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NUMERO_ADHERENT, $row, $personne->getNumeroAdherentOuMatricule());

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_TEL_1, $row, $this->ecritureNumeroTelephone($personne->getTelephoneUsage()));
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_1, $row, $personne->getAdresseMailUsage());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE, $row, $personne->getCoordonnees()->getAdressePostale()->getAdresseFormateePourTableur());

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_TEL_1, $row, $this->ecritureNumeroTelephone($personne->getTelephoneUsage()));
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_1, $row, $personne->getAdresseMailUsage());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE, $row, $personne->getCoordonnees()->getAdressePostale()->getAdresseFormateePourTableur());

                    if ($personne->getResponsableLegal1()) {
                        $coordonneeRL1 = $personne->getResponsableLegal1()->getCoordonnees();
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_1, $row, $coordonneeRL1->getPrenom() . " " . $coordonneeRL1->getNom());
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_RESP_LEGAL_1, $row, $coordonneeRL1->getAdresseMail());
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_1, $row, $coordonneeRL1->getAdressePostale()->getAdresseFormateePourTableur());
                    }
                    if ($personne->getResponsableLegal2()) {
                        $coordonneeRL2 = $personne->getResponsableLegal2()->getCoordonnees();
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_2, $row, $coordonneeRL2->getPrenom() . " " . $coordonneeRL2->getNom());
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_RESP_LEGAL_2, $row, $coordonneeRL2->getAdresseMail());
                        $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_2, $row, $coordonneeRL2->getAdressePostale()->getAdresseFormateePourTableur());
                    }
                    $valeurDroitImage = "Non";
                    if ($personne->getDroitImage()) {
                        $valeurDroitImage = "Oui";
                    }
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_DROIT_IMAGE, $row, $valeurDroitImage);
                    $row++;
                }
                /** @var ParticipantNonAdherent $nonAdherent */
                foreach ($uniteParticipante->getParticipantsNonAdherents() as $nonAdherent) {
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_STRUCTURE, $row, $nomStructure);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_UNITE, $row, $uniteParticipante->getEquipe()->getNom());

                    $valeur_non_renseigne = "Non renseigné";
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM, $row, $nonAdherent->getNom());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_PRENOM, $row, $nonAdherent->getPrenom());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_DATE_NAISSANCE, $row, $nonAdherent->getAge());
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_GENRE, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NUMERO_ADHERENT, $row, "Non adhérent·e");

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_TEL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE, $row, $valeur_non_renseigne);

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_TEL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE, $row, $valeur_non_renseigne);

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_RESP_LEGAL_1, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_1, $row, $valeur_non_renseigne);

                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_NOM_PRENOM_RESP_LEGAL_2, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_MAIL_RESP_LEGAL_2, $row, $valeur_non_renseigne);
                    $sheetParticipants->setCellValueByColumnAndRow($COL_PARTICIPANTS_ADRESSE_POSTALE_RESP_LEGAL_2, $row, $valeur_non_renseigne);
                    $row++;
                }
            }
        }

        //Application du style de contenu
        $sheetParticipants->getStyleByColumnAndRow($COL_PARTICIPANTS_DATE_NAISSANCE, 2, $COL_PARTICIPANTS_DATE_NAISSANCE, $row - 1)
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_DATE_NAISSANCE_TABLEUR);
    }

    /**
     * Genère l'onglet de tableur des encadrant·e.s d'un séjour
     *
     * @param  Spreadsheet $spreadsheet le classeur auquel ajouter l'onglet
     * @param  Sejour $sejour
     * @return void
     */
    public function exportOngletsEncadrants(Spreadsheet $spreadsheet, Sejour $sejour)
    {

        $sheetEncadrement = $spreadsheet->createSheet();
        $sheetEncadrement->setTitle("Encadrement");

        $valeur_non_renseigne = "Non renseigné";

        //Header des encadrant·e·s
        $n = 1;
        $COL_ENCADRANTS_STRUCTURE = $n++;
        $COL_ENCADRANTS_UNITE_SUPP = $n++;
        $COL_ENCADRANTS_GENRE = $n++;
        $COL_ENCADRANTS_NOM = $n++;
        $COL_ENCADRANTS_PRENOM = $n++;
        $COL_ENCADRANTS_NUMERO_ADHERENT = $n++;
        $COL_ENCADRANTS_DATE_NAISSANCE = $n++;
        $COL_ENCADRANTS_LIEU_NAISSANCE = $n++;


        $COL_ENCADRANTS_ROLE_FONCTION = $n++;

        $COL_ENCADRANTS_DIPLOME = $n++;
        $COL_ENCADRANTS_QUALITE_DIPLOME = $n++;
        $COL_ENCADRANTS_PSC1 = $n++;
        $COL_ENCADRANTS_PERMIS_B = $n++;

        $COL_ENCADRANTS_TEL_1 = $n++;
        $COL_ENCADRANTS_MAIL_1 = $n++;
        $COL_ENCADRANTS_ADRESSE_POSTALE = $n++;

        $COL_ENCADRANTS_DROIT_IMAGE = $n++;

        $COL_ENCADRANTS_PERSONNE_URGENCE = $n++;
        $COL_ENCADRANTS_TEL_URGENCE = $n++;


        $HEADER_ECADRANTS = [
            $COL_ENCADRANTS_STRUCTURE => ["label" => "Structure"],
            $COL_ENCADRANTS_UNITE_SUPP => ["label" => "Unité ou support"],
            $COL_ENCADRANTS_GENRE => ["label" => "Civilité"],
            $COL_ENCADRANTS_NOM => ["label" => "Nom"],
            $COL_ENCADRANTS_PRENOM => ["label" => "Prénom"],
            $COL_ENCADRANTS_DATE_NAISSANCE => ["label" => "Date de naissance"],
            $COL_ENCADRANTS_LIEU_NAISSANCE => ["label" => "Lieu de naissance"],

            $COL_ENCADRANTS_NUMERO_ADHERENT => ["label" => "Numéro d'adhérent.e"],

            $COL_ENCADRANTS_ROLE_FONCTION => ["label" => "Rôle et fonction"],
            $COL_ENCADRANTS_DIPLOME => ["label" => "Diplôme"],
            $COL_ENCADRANTS_QUALITE_DIPLOME => ["label" => "Qualité du diplôme"],
            $COL_ENCADRANTS_PSC1 => ["label" => "PSC1 (oui/non)"],
            $COL_ENCADRANTS_PERMIS_B => ["label" => "Permis B (oui/non)"],

            $COL_ENCADRANTS_TEL_1 => ["label" => "Numéro de téléphone 1"],
            $COL_ENCADRANTS_MAIL_1 => ["label" => "Adresse-mail 1"],
            $COL_ENCADRANTS_ADRESSE_POSTALE => ["label" => "Adresse postale"],

            $COL_ENCADRANTS_DROIT_IMAGE => ["label" => "Droit à l'image"],
            $COL_ENCADRANTS_PERSONNE_URGENCE => ["label" => "Personne à prévenir en cas d’urgence"],
            $COL_ENCADRANTS_TEL_URGENCE => ["label" => "Numéro de la personne à prévenir"],

        ];


        foreach ($HEADER_ECADRANTS as $index_colonne => $info_colonne) {
            $sheetEncadrement->setCellValueByColumnAndRow($index_colonne, 1, $info_colonne["label"]);
            $sheetEncadrement->getColumnDimensionByColumn($index_colonne)->setAutoSize(true);
        }

        //Application du style d'en-têtes
        $sheetEncadrement->getStyleByColumnAndRow($COL_ENCADRANTS_STRUCTURE, 1, $COL_ENCADRANTS_TEL_URGENCE, 1)->applyFromArray(self::STYLE_ENTETE_EXPORT_TABLEUR);
        $row = 2;

        $qualificationDirecteur = $sejour->getQualificationDirection();
        $directeur = $sejour->getDirecteur();


        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_GENRE, $row, $directeur->getGenre());
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NOM, $row, $directeur->getNom());
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PRENOM, $row, $directeur->getPrenom());
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DATE_NAISSANCE, $row, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($directeur->getDateNaissance()));
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_LIEU_NAISSANCE, $row,  $valeur_non_renseigne);
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NUMERO_ADHERENT, $row, $directeur->getNumeroAdherentOuMatricule());

        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_1, $row, $this->ecritureNumeroTelephone($directeur->getTelephoneUsage()));
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_MAIL_1, $row,  $directeur->getAdresseMailUsage());
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_ADRESSE_POSTALE, $row, $directeur->getCoordonnees()->getAdressePostale()->getAdresseFormateePourTableur());

        $valeurTelUrgence = $valeur_non_renseigne;
        if (!empty($directeur->getTelUrgence())) {
            $valeurTelUrgence = $this->ecritureNumeroTelephone($directeur->getTelUrgence());
        }
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_URGENCE, $row, $valeurTelUrgence);

        if (!empty($directeur->getPrenomUrgence()) || !empty($directeur->getNomUrgence())) {
            $valeurPersonneUrgence = $directeur->getPrenomUrgence() . " " . $directeur->getNomUrgence();
            if (!empty($directeur->getLienUrgence())) {
                $valeurPersonneUrgence .= " (" . $directeur->getLienUrgence() . ")";
            }
        } else {
            $valeurPersonneUrgence = $valeur_non_renseigne;
        }

        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PERSONNE_URGENCE, $row,  $valeurPersonneUrgence);

        if ($directeur->getDroitImage()) {
            $valeurDroitImage = "Oui";
        } else {
            $valeurDroitImage = "Non";
        }
        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DROIT_IMAGE, $row, $valeurDroitImage);


        $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_ROLE_FONCTION, $row, "Direct·eur·rice");

        if ($qualificationDirecteur) {
            $valeurDiplomesDirection = implode(", ", $qualificationDirecteur->getDiplomesDirection());
            if ($qualificationDirecteur->getEquivalenceDiplomeDirection()) {
                $valeurDiplomesDirection .= "(" . $qualificationDirecteur->getEquivalenceDiplomeDirection() . ")";
            }
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DIPLOME, $row, $valeurDiplomesDirection);
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_QUALITE_DIPLOME, $row, $qualificationDirecteur->getQualiteDirection());
            if ($qualificationDirecteur->getPermisB()) {
                $valeurPermisB = "Oui";
            } else {
                $valeurPermisB = "Non";
            }
            if ($qualificationDirecteur->getStatutAssistantSanitaire()) {
                $valeurPSC1 = "Oui";
            } else {
                $valeurPSC1 = "Non";
            }
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PERMIS_B, $row, $valeurPermisB);
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PSC1, $row, $valeurPSC1);
        }


        $row++;


        /** @var RoleEncadrantSejour $roleEncadrant */
        foreach ($sejour->getRolesEncadrants() as $roleEncadrant) {
            if ($roleEncadrant->getStructureNonEEDF()) {
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_STRUCTURE, $row, $roleEncadrant->getStructureNonEEDF()->getNomStructureNonEEDF());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_UNITE_SUPP, $row, "");
            } else if ($roleEncadrant->getUniteParticipante()) {
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_STRUCTURE, $row, $roleEncadrant->getUniteParticipante()->getEquipe()->getStructure()->getNom());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_UNITE_SUPP, $row, $roleEncadrant->getUniteParticipante()->getEquipe()->getNom());
            } else {
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_STRUCTURE, $row, "");
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_UNITE_SUPP, $row, RoleEncadrantSejour::EQUIPE_SUPPORT);
            }

            if ($roleEncadrant->getEstEncadrantEEDF()) {
                $personne = $roleEncadrant->getEncadrantEEDF();
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_GENRE, $row, $personne->getGenre());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NOM, $row, $personne->getNom());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PRENOM, $row, $personne->getPrenom());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DATE_NAISSANCE, $row, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($personne->getDateNaissance()));
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_LIEU_NAISSANCE, $row,  $valeur_non_renseigne);

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NUMERO_ADHERENT, $row, $personne->getNumeroAdherentOuMatricule());

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_1, $row, $this->ecritureNumeroTelephone($personne->getTelephoneUsage()));
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_MAIL_1, $row,  $personne->getAdresseMailUsage());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_ADRESSE_POSTALE, $row, $personne->getCoordonnees()->getAdressePostale()->getAdresseFormateePourTableur());


                $valeurTelUrgence = $valeur_non_renseigne;
                if (!empty($personne->getTelUrgence())) {
                    $valeurTelUrgence = $this->ecritureNumeroTelephone($personne->getTelUrgence());
                }
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_URGENCE, $row, $valeurTelUrgence);

                if (!empty($personne->getPrenomUrgence()) || !empty($personne->getNomUrgence())) {
                    $valeurPersonneUrgence = $personne->getPrenomUrgence() . " " . $personne->getNomUrgence();
                    if (!empty($personne->getLienUrgence())) {
                        $valeurPersonneUrgence .= " (" . $personne->getLienUrgence() . ")";
                    }
                } else {
                    $valeurPersonneUrgence = $valeur_non_renseigne;
                }

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PERSONNE_URGENCE, $row,  $valeurPersonneUrgence);


                if ($personne->getDroitImage()) {
                    $valeurDroitImage = "Oui";
                } else {
                    $valeurDroitImage = "Non";
                }
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DROIT_IMAGE, $row, $valeurDroitImage);;
            } else {
                $nonAdherent = $roleEncadrant->getInfosEncadrantNonEEDF();
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_GENRE, $row, $nonAdherent->getGenre());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NOM, $row, $nonAdherent->getNom());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PRENOM, $row, $nonAdherent->getPrenom());
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DATE_NAISSANCE, $row, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($nonAdherent->getDateNaissance()));

                if ($nonAdherent->getCommuneNaissance()) {
                    $valeurLieuNaissance = $nonAdherent->getCommuneNaissance();
                    if ($nonAdherent->getDepartementOuPaysNaissance()) {
                        $nonAdherent->getCommuneNaissance() . " (" . $nonAdherent->getDepartementOuPaysNaissance() . ")";
                    }
                } else {
                    $valeurLieuNaissance = $valeur_non_renseigne;
                }
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_LIEU_NAISSANCE, $row,  $valeurLieuNaissance);
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_NUMERO_ADHERENT, $row, $valeur_non_renseigne);

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_1, $row, $this->ecritureNumeroTelephone($nonAdherent->getTelephone()));
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_MAIL_1, $row,  $valeur_non_renseigne);
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_ADRESSE_POSTALE, $row, $valeur_non_renseigne);

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_TEL_URGENCE, $row, $valeur_non_renseigne);

                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PERSONNE_URGENCE, $row, $valeur_non_renseigne);
                $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DROIT_IMAGE, $row, $valeur_non_renseigne);
            }

            $valeurDiplomesAnimation = implode(", ", $roleEncadrant->getDiplomesAnimation());
            if ($roleEncadrant->getEquivalenceDiplomeAnimation()) {
                $valeurDiplomesAnimation .= "(" . $roleEncadrant->getEquivalenceDiplomeAnimation() . ")";
            }
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_DIPLOME, $row, $valeurDiplomesAnimation);
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_ROLE_FONCTION, $row, implode(", ", $roleEncadrant->getFonction()));
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_QUALITE_DIPLOME, $row, $roleEncadrant->getQualiteAnimation());
            if ($roleEncadrant->getPermisB()) {
                $valeurPermisB = "Oui";
            } else {
                $valeurPermisB = "Non";
            }
            if ($roleEncadrant->getStatutAssistantSanitaire()) {
                $valeurPSC1 = "Oui";
            } else {
                $valeurPSC1 = "Non";
            }


            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PERMIS_B, $row, $valeurPermisB);
            $sheetEncadrement->setCellValueByColumnAndRow($COL_ENCADRANTS_PSC1, $row, $valeurPSC1);
            $row++;
        }

        //Application du style de contenu
        $sheetEncadrement->getStyleByColumnAndRow($COL_ENCADRANTS_DATE_NAISSANCE, 2, $COL_ENCADRANTS_DATE_NAISSANCE, $row - 1)
            ->getNumberFormat()
            ->setFormatCode(self::FORMAT_DATE_NAISSANCE_TABLEUR);
    }


    /**
     * Permet d'écrire un texte avec des balises HTML dans un document WORD (en utilisant phpWord)
     *
     * @param  String $htmlValue
     * @return Table la valeur transformée
     */
    public function ecritureTexteHtmlDansWord($htmlValue): Table
    {
        $wordTable = new \PhpOffice\PhpWord\Element\Table();
        $wordTable->addRow();
        $cell = $wordTable->addCell();
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $htmlValue);

        return $wordTable;
    }

    /**
     * formattageNomFichier
     * Permet de formatter un nom de fichier pour en retirer / transformer les caratères interdits 
     * @param  String $value le nom de fichier en entrée
     * @return String la valeur correctement formattée
     */
    public function formattageNomFichier(String $value): String
    {
        $str = preg_replace('/[\r\n\t ]+/', ' ', $value);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = str_replace('%', '-', $str);
        return $str;
    }

    public function ecritureNumeroTelephone(?PhoneNumber $phoneNumber): String
    {
        $result = "";
        if ($phoneNumber) {
            $result = "(+" . $phoneNumber->getCountryCode() . ") " . $phoneNumber->getNationalNumber();
        }

        return $result;
    }
}
