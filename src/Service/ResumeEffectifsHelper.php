<?php

namespace App\Service;

use App\Entity\Equipe;
use App\Entity\ParticipantNonAdherent;
use App\Entity\ParticipationSejourUnite;
use App\Entity\RoleEncadrantSejour;
use App\Entity\Personne;
use App\Entity\Sejour;
use App\Entity\Unite;
use App\Utils\ResumeEffectifsSejour;
use DateTime;


/**
 * Helper pour le caclul des effectifs
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ResumeEffectifsHelper
{


    /**
     * Permet de générer un objet récapitulant les effectifs des participant·e·s et des encadrant·e·s d'un séjour
     * 
     * Comptabilise :
     * - le nombre d'enfants et d'encadrant·e.s au total et par tranche d'âge
     * - le nombre d'encadrant.e·s par diplome (ou équivalence), permis, PSC1, par fonctions dans le séjour
     * - le nombre d'enfants de plus de 14 ans et de moins de 14 ans
     * Calcule aussi le taux d'encadrement, sa validité, et les erreurs dans le cas où il est invalide
     *
     * @param  Sejour $sejour
     * @return ResumeEffectifsSejour l'objet récapitulant les effectifs des participant.e·s et encadrant·e·s
     */
    public function getResumeEffectifsSejour(Sejour $sejour): ResumeEffectifsSejour
    {

        $resumeEffectifsSejour = new ResumeEffectifsSejour();
        if ($sejour->getQualificationDirection()) {
            if ($sejour->getQualificationDirection()->getPermisB()) {
                $resumeEffectifsSejour->addNbEncPermisB(1);
            }
            if ($sejour->getQualificationDirection()->getStatutAssistantSanitaire()) {
                $resumeEffectifsSejour->addNbEncPSC1(1);
            }
        }


        /** @var RoleEncadrantSejour $roleEncadrant */
        foreach ($sejour->getRolesEncadrants() as $roleEncadrant) {
            if ($roleEncadrant->getEquipe() == RoleEncadrantSejour::EQUIPE_ANIMATION) {
                if ($roleEncadrant->getStructureNonEEDF()) {
                    $resumeEffectifsSejour->addNbEncadrantsStructureNonEEDF(1);
                } else if ($roleEncadrant->getUniteParticipante()) {
                    switch ($roleEncadrant->getUniteParticipante()->getEquipe()->getType()) {
                        case Equipe::TYPE_EQUIPE_RONDE_LUTINS:
                            $resumeEffectifsSejour->addNbEncadrantsLutins(1);
                            break;
                        case Equipe::TYPE_EQUIPE_CERCLE_LOUVETEAUX:
                            $resumeEffectifsSejour->addNbEncadrantsLouveteaux(1);
                            break;
                        case Equipe::TYPE_EQUIPE_UNITE_ECLE:

                            $resumeEffectifsSejour->addNbEncadrantsEcles(1);
                            break;
                        case Equipe::TYPE_EQUIPE_UNITE_AINE:
                            $resumeEffectifsSejour->addNbEncadrantsAines(1);
                            break;
                        case Equipe::TYPE_EQUIPE_ACTIVITE_ADAPTEE:
                            break;
                    }
                } else {
                    $resumeEffectifsSejour->addNbEncadrantsEquipeAnimationAutres(1);
                }
            } else {
                $resumeEffectifsSejour->addNbEncadrantsEquipeSupport(1);
            }



            //On ne compte dans le taux d'encadrement que les directeur adjoints et l'équipe d'animation
            if ($roleEncadrant->getEquipe() == RoleEncadrantSejour::EQUIPE_ANIMATION || in_array(RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE, $roleEncadrant->getFonction())) {
                switch ($roleEncadrant->getQualiteAnimation()) {
                    case RoleEncadrantSejour::QUALITE_NON_DIPLOME:
                        $resumeEffectifsSejour->addNbEncNonDiplomes(1);
                        break;
                    case RoleEncadrantSejour::QUALITE_STAGIAIRE:
                        $resumeEffectifsSejour->addNbEncStagiaires(1);
                        break;
                    case RoleEncadrantSejour::QUALITE_TITULAIRE:
                    case RoleEncadrantSejour::QUALITE_TITULAIRE_STAGIAIRE:
                        $resumeEffectifsSejour->addNbEncDiplomes(1);
                        break;
                }
            }


            if ($roleEncadrant->getPermisB()) {
                $resumeEffectifsSejour->addNbEncPermisB(1);
            }
            if ($roleEncadrant->getStatutAssistantSanitaire()) {
                $resumeEffectifsSejour->addNbEncPSC1(1);
            }
            $resumeEffectifsSejour->addNbTotalEncadrants(1);
        }

        /** @var ParticipationSejourUnite $uniteParticipante */
        foreach ($sejour->getUnitesStructureOrganisatrice() as $uniteParticipante) {
            $nbEnfantsTemp = 0;
            /** @var Personne $participant */
            foreach ($uniteParticipante->getParticipantsAdherents() as $participant) {
                $nbEnfantsTemp++;
                if ($this->age($participant->getDateNaissance()) < 14) {
                    $resumeEffectifsSejour->setAuMoinsUnEnfantsDeMoinsDe14Ans(true);
                    $resumeEffectifsSejour->addNbEnfantsMoins14ans(1);
                } else {
                    $resumeEffectifsSejour->addNbEnfantsPlus14ans(1);
                }
            }
            /** @var ParticipantNonAdherent $participant */
            foreach ($uniteParticipante->getParticipantsNonAdherents() as $participant) {
                $nbEnfantsTemp++;
                if ($participant->getAge() < 14) {
                    $resumeEffectifsSejour->setAuMoinsUnEnfantsDeMoinsDe14Ans(true);
                    $resumeEffectifsSejour->addNbEnfantsMoins14ans(1);
                } else {
                    $resumeEffectifsSejour->addNbEnfantsPlus14ans(1);
                }
            }
            switch ($uniteParticipante->getEquipe()->getType()) {
                case Equipe::TYPE_EQUIPE_RONDE_LUTINS:
                    $resumeEffectifsSejour->addNbEnfantsLutins($nbEnfantsTemp);
                    break;
                case Equipe::TYPE_EQUIPE_CERCLE_LOUVETEAUX:
                    $resumeEffectifsSejour->addNbEnfantsLouveteaux($nbEnfantsTemp);
                    break;
                case Equipe::TYPE_EQUIPE_UNITE_ECLE:
                    $resumeEffectifsSejour->addNbEnfantsEcles($nbEnfantsTemp);
                    break;
                case Equipe::TYPE_EQUIPE_UNITE_AINE:
                    $resumeEffectifsSejour->addNbEnfantsAines($nbEnfantsTemp);
                    break;
                case Equipe::TYPE_EQUIPE_ACTIVITE_ADAPTEE:
                    break;
            }
            $resumeEffectifsSejour->addNbTotalEnfants($nbEnfantsTemp);
        }
        /** @var StructureRegroupementSejour $structureRegroupee */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupee) {
            if ($structureRegroupee->getEstEEDF()) {
                /** @var ParticipationSejourUnite $uniteParticipante */
                foreach ($structureRegroupee->getUnitesParticipantes() as $uniteParticipante) {
                    $nbEnfantsTemp = 0;
                    foreach ($uniteParticipante->getParticipantsAdherents() as $participant) {
                        $nbEnfantsTemp++;
                        if ($this->age($participant->getDateNaissance()) < 14) {
                            $resumeEffectifsSejour->setAuMoinsUnEnfantsDeMoinsDe14Ans(true);
                            $resumeEffectifsSejour->addNbEnfantsMoins14ans(1);
                        } else {
                            $resumeEffectifsSejour->addNbEnfantsPlus14ans(1);
                        }
                    }
                    foreach ($uniteParticipante->getParticipantsNonAdherents() as $participant) {
                        $nbEnfantsTemp++;
                        if ($participant->getAge() < 14) {
                            $resumeEffectifsSejour->setAuMoinsUnEnfantsDeMoinsDe14Ans(true);
                            $resumeEffectifsSejour->addNbEnfantsMoins14ans(1);
                        } else {
                            $resumeEffectifsSejour->addNbEnfantsPlus14ans(1);
                        }
                    }
                    switch ($uniteParticipante->getEquipe()->getType()) {
                        case Equipe::TYPE_EQUIPE_RONDE_LUTINS:
                            $resumeEffectifsSejour->addNbEnfantsLutins($nbEnfantsTemp);
                            break;
                        case Equipe::TYPE_EQUIPE_CERCLE_LOUVETEAUX:
                            $resumeEffectifsSejour->addNbEnfantsLouveteaux($nbEnfantsTemp);
                            break;
                        case Equipe::TYPE_EQUIPE_UNITE_ECLE:
                            $resumeEffectifsSejour->addNbEnfantsEcles($nbEnfantsTemp);
                            break;
                        case Equipe::TYPE_EQUIPE_UNITE_AINE:
                            $resumeEffectifsSejour->addNbEnfantsAines($nbEnfantsTemp);
                            break;
                        case Equipe::TYPE_EQUIPE_ACTIVITE_ADAPTEE:
                            break;
                    }
                    $resumeEffectifsSejour->addNbTotalEnfants($nbEnfantsTemp);
                }
            } else {
                $resumeEffectifsSejour->addNbTotalEnfants($structureRegroupee->getNbEnfantMoins14StructNonEEDF());
                $resumeEffectifsSejour->addNbTotalEnfants($structureRegroupee->getNbEnfantsPlus14StructNonEEDF());
                if ($structureRegroupee->getNbEnfantMoins14StructNonEEDF() > 0) {
                    $resumeEffectifsSejour->setAuMoinsUnEnfantsDeMoinsDe14Ans(true);
                    $resumeEffectifsSejour->addNbEnfantsMoins14ans($structureRegroupee->getNbEnfantMoins14StructNonEEDF());
                }
                $resumeEffectifsSejour->addNbEnfantsPlus14ans($structureRegroupee->getNbEnfantsPlus14StructNonEEDF());


                $resumeEffectifsSejour->addNbEnfantsStructureNonEEDF($structureRegroupee->getNbEnfantMoins14StructNonEEDF());
                $resumeEffectifsSejour->addNbEnfantsStructureNonEEDF($structureRegroupee->getNbEnfantsPlus14StructNonEEDF());
            }
        }
        if ($sejour->getAspectsParticipantsBesoinsSpecifiquesSejour()) {
            $resumeEffectifsSejour->setNbEnfantsSituationHandicap($sejour->getAspectsParticipantsBesoinsSpecifiquesSejour()->getNombreParticipantsHandicap());
        }


        //Calcul du taux d'encadrement:

        $nbEncadrantsLegal = ceil($resumeEffectifsSejour->getNbTotalEnfants() / 12); // arrondi au sup
        $nbEncadrantsFormesLegal = ceil($nbEncadrantsLegal / 2); // arrondi au sup

        //Si au moins 1 enfant de moins de 14 ans, le directeur ne compte pas dans le taux
        //si uniquement des enfant de plus de 14, mais nbEnfants >= 50, le directeur ne compte pas dans le taux
        if (!$resumeEffectifsSejour->getAuMoinsUnEnfantsDeMoinsDe14Ans() && $resumeEffectifsSejour->getNbTotalEnfants() < 50) {
            $resumeEffectifsSejour->addNbEncDiplomes(1);
            $resumeEffectifsSejour->addNbTotalEncadrants(1);
            $resumeEffectifsSejour->addNbEncadrantsEquipeAnimationAutres(1);
        }

        if (($resumeEffectifsSejour->getNbEncDiplomes() + $resumeEffectifsSejour->getNbEncStagiaires() + $resumeEffectifsSejour->getNbEncNonDiplomes())  < $nbEncadrantsLegal) {
            $resumeEffectifsSejour->setTaux_encadrement_correct(false);
            $resumeEffectifsSejour->addErrors_taux_encadrement("Nombre d'encadrant·e·s insuffisant pour le nombre de participant·e·s - au regard des effectifs du séjour, il faut " . $nbEncadrantsLegal . " personnes minimum en animation");
        }

        if ($resumeEffectifsSejour->getNbEncDiplomes() < $nbEncadrantsFormesLegal) {
            $resumeEffectifsSejour->setTaux_encadrement_correct(false);
            $resumeEffectifsSejour->addErrors_taux_encadrement("Nombre d'encadrant·e·s titulaires insuffisant - au regard des effectifs du séjour, il faut " . $nbEncadrantsFormesLegal . " titulaires minimum");
        } else {
            if ($nbEncadrantsLegal > $resumeEffectifsSejour->getNbEncDiplomes()) {
                $nbEncadrantsStagiairesEtNonDiplomesLegal = $nbEncadrantsLegal - $resumeEffectifsSejour->getNbEncDiplomes();
                if ($nbEncadrantsStagiairesEtNonDiplomesLegal > $resumeEffectifsSejour->getNbEncStagiaires()) {
                    $nbEncadrantsNonDiplomesPotentiel = $nbEncadrantsStagiairesEtNonDiplomesLegal - $resumeEffectifsSejour->getNbEncStagiaires();
                    $nbEncadrantsNonFormesLegal = floor($nbEncadrantsLegal / 5); // arrondi au inf
                    if ($nbEncadrantsNonDiplomesPotentiel > $nbEncadrantsNonFormesLegal) {
                        $resumeEffectifsSejour->setTaux_encadrement_correct(false);
                        $resumeEffectifsSejour->addErrors_taux_encadrement("Nombre d'encadrant·e·s non diplômé·e·s trop élevé - au regard des effectifs du séjour, il faut " . $nbEncadrantsNonFormesLegal . " personnes non diplômées maximum");
                    }
                }
            }
        }

        return $resumeEffectifsSejour;
    }


    /**
     * Permet de déterminer l'age à partir d'une date de naissance
     *
     * @param  DateTime $date la date de naissance
     * @return int l'age par rapport à la date courante
     */
    function age(DateTime $date): int
    {
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }
}
