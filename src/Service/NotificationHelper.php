<?php

namespace App\Service;

use App\Entity\DemandeModerationCommentaire;
use App\Entity\NominationVisiteSejour;
use App\Entity\Notification;
use App\Entity\Personne;
use App\Entity\Sejour;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Utils\NotificationConstants;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

/**
 * Helper pour la gestion des notifications
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class NotificationHelper
{


    private $entityManager;
    private $mailer;
    private $twig;
    private $personneRepository;

    public function __construct(EntityManagerInterface $entityManager, MailerInterface $mailer, Environment $twig, PersonneRepository $personneRepository)
    {
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->twig->addExtension(new \Twig\Extension\StringLoaderExtension());
        $this->personneRepository = $personneRepository;
    }

    /**
     * Permet de générer une notification, construite à partir du modèle et de élements de contexte, et de l'envoyer au destinataires prévu·e·s
     * 
     * Etapes:
     *  - Vérification de la validité de la notification
     *  - Construction de la liste des destinataires et dédoublonnement éventuel
     *  - Envoi de la notification aux adhérent·e·s ayant le statut d'ancien·ne ou d'adhérent·e
     * 
     * @param  String $nom_modele_notif modèle de notification à envoyer
     * @param  array $contexte éléments de contexte permettant d'envoyer la notification
     * @param  array|null $destinataires destinaaires supplémentaires à ceux prévus dans le modèle
     * @return void
     * @throws Exception si le modèle de notification n'exsite pas
     * @throws Exception si un élément necessaire dans le contexte n'est pas présent ou qu'il n'a pas la classe attendue
     * @see NotificationHelper::envoiNotification() pour envoyer la notification
     */
    public function gestionNotification($nom_modele_notif, array $contexte, $destinataires = null)
    {

        //On vérifie si la notification existe
        if (!array_key_exists($nom_modele_notif, NotificationConstants::LISTE_NOTIF)) {
            throw new Exception("Le modèle de notification " . $nom_modele_notif . " n'existe pas");
        }

        //On vérifie si le contexte existe
        $modele_notif = NotificationConstants::LISTE_NOTIF[$nom_modele_notif];
        foreach ($modele_notif["contexte"] as $cle_contexte_attendu => $classe_contexte_attendu) {
            if (!array_key_exists($cle_contexte_attendu, $contexte)) {
                throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . $cle_contexte_attendu . " n'est pas présent dans le contexte");
            }
            if (!get_class($contexte[$cle_contexte_attendu]) == ($classe_contexte_attendu)) {
                throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . $cle_contexte_attendu . " n'est pas de la classe " . $classe_contexte_attendu);
            }
        }
        //On récupere les destinataires désignés
        if (!$destinataires) {
            $destinataires = array();
        }
        if (array_key_exists('destinataires', $modele_notif)) {
            foreach ($modele_notif['destinataires'] as $role) {
                switch ($role) {
                    case NotificationConstants::ROLE_PERSONNE_CONCERNEE:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_PERSONNE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_PERSONNE . " n'est pas présent dans le contexte");
                        }
                        /**  @var Personne $personne */
                        $personne = $contexte[NotificationConstants::LABEL_PERIMETRE_PERSONNE];
                        $destinataires[] = $personne;
                        break;
                    case NotificationConstants::ROLE_AUTEUR_COMMENTAIRE:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE . " n'est pas présent dans le contexte");
                        }
                        /**  @var DemandeModerationCommentaire $demandeModeration */
                        $demandeModeration = $contexte[NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE];
                        $destinataires[] = $demandeModeration->getCommentaire()->getPersonne();
                        break;
                    case NotificationConstants::ROLE_DENONCIATEUR_COMMENTAIRE:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE . " n'est pas présent dans le contexte");
                        }
                        /**  @var DemandeModerationCommentaire $demandeModeration */
                        $demandeModeration = $contexte[NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE];
                        $destinataires[] = $demandeModeration->getPersonne();
                        break;
                    case NotificationConstants::ROLE_ANIMATEUR:
                        $destinataires = array_merge($destinataires, $this->personneRepository->findByRole(Personne::ROLE_ANIMATION));
                        break;
                    case NotificationConstants::ROLE_ADMINISTRATEUR:
                        $destinataires = array_merge($destinataires, $this->personneRepository->findByRole(Personne::ROLE_ADMIN));
                        break;
                    case NotificationConstants::ROLE_VALIDATION_NATIONALE:
                        $destinataires = array_merge($destinataires, $this->personneRepository->findByRole(Personne::ROLE_VALIDATION_NATIONALE));
                        break;
                    case NotificationConstants::ROLE_VALIDATION_INTERNATIONALE:
                        $destinataires = array_merge($destinataires, $this->personneRepository->findByRole(Personne::ROLE_VALIDATION_INTERNATIONALE));
                        break;
                    case NotificationConstants::ROLE_ORGANISATION_SEJOUR:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        $destinataires = array_merge($destinataires, $sejour->getStructureOrganisatrice()->getOrganisateurs()->toArray());
                        break;
                    case NotificationConstants::ROLE_DIRECTION_SEJOUR:
                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        $destinataires = array_merge($destinataires, [$sejour->getDirecteur()]);
                        break;
                    case NotificationConstants::ROLE_DIRECTION_ADJOINTE_SEJOUR:
                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        $destinataires = array_merge($destinataires, $sejour->getDirecteursAdjoints()->toArray());
                        break;
                    case NotificationConstants::ROLE_SUIVI_SEJOUR:
                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        $structureOrganisatrice = $sejour->getStructureOrganisatrice();
                        $destinataires = array_merge(
                            $destinataires,
                            $sejour->getSuiveurs()->toArray(),
                            $structureOrganisatrice->getSuiveurs()->toArray(),
                            $this->personneRepository->findByRole(Personne::ROLE_SUIVI_GLOBAL),
                            $this->personneRepository->findByRole(Personne::ROLE_ANIMATION)
                        );
                        break;
                    case NotificationConstants::ROLE_VISITE_SEJOUR:
                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        $visiteurs = array();
                        /** @var NominationVisiteSejour $nomination_visite */
                        foreach ($sejour->getNominationsVisiteSejour() as $nomination_visite) {
                            $visiteurs[] = $nomination_visite->getVisiteur();
                        }
                        $destinataires = array_merge(
                            $destinataires,
                            $visiteurs
                        );
                        break;
                    case NotificationConstants::ROLE_VALIDATION_SEJOUR:
                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_SEJOUR, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_SEJOUR . " n'est pas présent dans le contexte");
                        }
                        /**  @var Sejour $sejour */
                        $sejour = $contexte[NotificationConstants::LABEL_PERIMETRE_SEJOUR];
                        //Si le séjour est internation, on ajoute les validations Internationales
                        if ($sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger()) {
                            $destinataires = array_merge(
                                $destinataires,
                                $this->personneRepository->findByRole(Personne::ROLE_VALIDATION_INTERNATIONALE)
                            );
                        }

                        //Validation de séjours nationaux
                        if ($sejour->getStructureOrganisatrice()->getStructureParent()->getEchelon() == Structure::ECHELON_STRUCTURE_NATIONAL) {
                            $destinataires = array_merge(
                                $destinataires,
                                $this->personneRepository->findByRole(Personne::ROLE_VALIDATION_NATIONALE)
                            );
                            //validation des séjours régionaux
                        } else {
                            $destinataires = array_merge(
                                $destinataires,
                                $sejour->getStructureOrganisatrice()->getStructureParent()->getValidateurs()->toArray()
                            );
                        }
                        break;
                    case NotificationConstants::ROLE_ORGANISATEUR_STRUCTURE:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_STRUCTURE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_STRUCTURE . " n'est pas présent dans le contexte");
                        }
                        /**  @var Structure $structure */
                        $structure = $contexte[NotificationConstants::LABEL_PERIMETRE_STRUCTURE];
                        $destinataires = array_merge($destinataires, $structure->getOrganisateurs()->toArray());
                        break;
                    case NotificationConstants::ROLE_VALIDATEUR_STRUCTURE:

                        if (!array_key_exists(NotificationConstants::LABEL_PERIMETRE_STRUCTURE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . NotificationConstants::LABEL_PERIMETRE_STRUCTURE . " n'est pas présent dans le contexte");
                        }
                        /**  @var Structure $structure */
                        $structure = $contexte[NotificationConstants::LABEL_PERIMETRE_STRUCTURE];
                        $destinataires = array_merge($destinataires, $structure->getStructureParent()->getValidateurs()->toArray());
                        break;
                }
            }
        }

        //On dédoublonne les destinataires pour n'envoyer qu'une notif par personne, même si cette personne a plusieurs rôles
        $destinataires_dedoublonnes = array();
        /**  @var Personne $destinataire_a_dedoublonner */
        foreach ($destinataires as $destinataire_a_dedoublonner) {
            $destinataires_dedoublonnes[$destinataire_a_dedoublonner->getId()] = $destinataire_a_dedoublonner;
        }

        /**  @var Personne $destinataire */
        foreach ($destinataires_dedoublonnes as $destinataire) {
            //On envoie les notifications uniquement aux personnes adhérentes
            if (($destinataire->estAdherentValide())
                || ($destinataire->estSalarieValide())
            ) {
                $pathTemplate = 'notifications/' . $modele_notif["template"] . '.html.twig';
                $this->envoiNotification($destinataire, $pathTemplate, $contexte, $modele_notif["titre"]);
            }
        }
    }

    /**
     * Envoi d'une notification pour une personne unique
     * La Notification est stokée en base et notée comme 'non lue'
     * - Si le paramètre de notification de la personne est poisitionnée à 'temps réel', la notification est aussi envoyée directement par mail à la personne
     * - Si le paramètre de notification de la personne est poisitionnée à 'quotidien', la date pour envoi est positionnée à la date courante
     * - Si le paramètre de notification de la personne est poisitionnée à 'hebdomadaire', la date pour envoi est positionnée à la date du prochain mercredi à minuit
     * - Si le paramètre de notification de la personne est poisitionnée à 'jamis', la notification est notée comme envoyée en base
     * @param  Personne $personne personne à qui envoyer la notification
     * @param  String $pathTemplate emplacement du tmplate de la notification
     * @param  mixed[] $contexte contexte de la notification à envoyer
     * @param  String $titre titre de la notification à envoyer
     * @return void
     */
    public function envoiNotification(Personne $personne, String $pathTemplate, array $contexte, string $titre)
    {
        $notification = new Notification();

        $notification->setPersonne($personne);
        $notification->setContenuMessage($this->twig->render($pathTemplate, $contexte));

        $templateTitre = $this->twig->createTemplate($titre);
        $notification->setTitre($this->twig->render($templateTitre, $contexte));

        $notification->setLu(false);

        $currentDate = new DateTime();
        $notification->setDateCreation($currentDate);

        $envoi_immediat = false;

        switch ($personne->getFrequenceNotification()) {
                //La notification est créée sans être envoyée
            case Personne::FREQUENCE_NOTIFICATION_JAMAIS:
                $notification->setDelivered(true);
                break;

                //La notification est créée et directement envoyée par mail
            case Personne::FREQUENCE_NOTIFICATION_TEMPS_REEL:
                $notification->setDelivered(true);
                $envoi_immediat = true;
                $notification->setDateEnvoi($currentDate);
                break;

                //La notification est créée et la date d'envoi est positionnée au lendemain à minuit
            case Personne::FREQUENCE_NOTIFICATION_QUOTIDIEN:
                $notification->setDelivered(false);
                $date_envoi = new DateTime('tomorrow');
                $notification->setDateEnvoi($date_envoi);
                break;

                //La notification est créée et la date d'envoi est positionnée au mercredi suivant à minuit
            case Personne::FREQUENCE_NOTIFICATION_HEBDO:
                $notification->setDelivered(false);
                $date_envoi = new DateTime('wednesday midnight');
                $notification->setDateEnvoi($date_envoi);
                break;
        }

        $this->entityManager->persist($notification);
        $this->entityManager->flush();

        if ($envoi_immediat) {
            if ($personne->getAdresseMailUsage()) {
                // Ici nous enverrons l'e-mail
                $message = (new TemplatedEmail())

                    ->from(new Address('roads@ecles.fr', '[EEDF-R.O.A.D.S] Notifications'))
                    ->to($personne->getAdresseMailUsage())
                    ->subject("[Notification R.O.A.D.S]" . $notification->getTitre())
                    ->htmlTemplate('notifications/mail_notif_unique.html.twig')
                    ->context([
                        'notification' => $notification,
                    ]);

                $this->mailer->send($message);
            }
        }
    }


    /**
     * Permet d'envoyer un mail groupé avec un ensemble de notifications à l'interieur
     *
     * Toutes les notification envoyées sont notées comme 'délivrées' en base de données
     * @param  Personne $personne la personne à qui envoyer les notification
     * @param  Notification[] $notifications la liste des notifications à envoyer
     * @return void
     */
    public function envoiNotificationsGroupees(Personne $personne, array $notifications)
    {
        //envoi du message
        if ($personne->getAdresseMailUsage()) {
            $message = (new TemplatedEmail())

                ->from(new Address('roads@ecles.fr', '[EEDF-R.O.A.D.S] Notifications'))
                ->to($personne->getAdresseMailUsage())
                ->subject("[Notification R.O.A.D.S]: vous avez " . count($notifications) . " nouvelle(s) notification(s)")
                ->htmlTemplate('notifications/mail_notif_multiple.html.twig')
                ->context([
                    'personne' => $personne,
                    'notifications' => $notifications,
                ]);

            $this->mailer->send($message);
        }

        //Marquer les notifications comme envoyées:
        foreach ($notifications as $notification) {
            $notification->setDelivered(true);
            $this->entityManager->persist($notification);
        }
        $this->entityManager->flush();
    }
}
