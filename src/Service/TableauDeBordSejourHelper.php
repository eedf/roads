<?php

namespace App\Service;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;
use App\Entity\Sejour;
use App\Repository\SejourRepository;
use App\Utils\ResumeSejours;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Helper pour la présentation des liste des séjours concernant une personne dans ROADS
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class TableauDeBordSejourHelper
{

    private $sejourRepository;
    private $security;
    private $sejourWorkflow;

    public function __construct(SejourRepository $sejourRepository, Security $security, WorkflowInterface $sejourWorkflow)
    {
        $this->sejourRepository = $sejourRepository;
        $this->security = $security;
        $this->sejourWorkflow = $sejourWorkflow;
    }


    /**
     * Génère un tableau contenant les données de tableaux de bord des séjours d'une personne
     * 
     * Le résultat contient:
     * - La liste des séjours que la personne a en charge de validation, validation déléguée, visite, direction, ou organisation
     * - La liste des séjours que la personne a en suivi
     * - La liste des séjours auxquel la personne participe
     * - La liste des séjours archivés sur lequels la personne a encore des droits
     * - Le récapitulatif des séjours le·la concernant
     *
     * @param  Personne $personne
     * @return mixed[] résultat synthèses séjours
     * @uses ResumeSejours synthèse des séjours
     */
    public function getTableauDeBordSejour(Personne $personne): array
    {

        $sejoursSuivisAAfficher = array();
        $archivesSejours = array();
        //Si rôle de suivi global, on affiche tous les séjours
        if (($this->security->isGranted(Personne::ROLE_SUIVI_GLOBAL)) || ($this->security->isGranted(Personne::ROLE_ANIMATION))) {
            $tousSejours = $this->sejourRepository->findTousSejoursNonClotures();
            foreach ($tousSejours as $sejour) {
                $sejoursSuivisAAfficher[] = $sejour;
            }
            $tousSejoursClotures = $this->sejourRepository->findTousSejoursClotures();
            foreach ($tousSejoursClotures as $sejour) {
                $archivesSejours[$sejour->getId()] = $sejour;
            }
        } else {
            $sejoursSuivis = $personne->getSejoursSuivis()->toArray();
            /**  @var Sejour $sejourSuivi */
            foreach ($sejoursSuivis as $sejourSuivi) {

                if ($sejourSuivi->isStatusVisible()) {
                    $sejoursSuivisAAfficher[] = $sejourSuivi;
                }
                $statusSejour = $this->sejourWorkflow->getMarking($sejourSuivi);
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                    $archivesSejours[$sejourSuivi->getId()] = $sejourSuivi;
                }
            }
            $structuresSuivies = $personne->getStructuresSuivi()->toArray();

            /**  @var Structure $structureSuivie */
            foreach ($structuresSuivies as $structureSuivie) {
                foreach ($structureSuivie->getSejoursOrganises() as $sejour) {
                    /**  @var Sejour $sejour */
                    if ($sejour->isStatusVisible()) {
                        $sejoursSuivisAAfficher[] = $sejour;
                    }
                    $statusSejour = $this->sejourWorkflow->getMarking($sejour);
                    if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                        $archivesSejours[$sejour->getId()] = $sejour;
                    }
                }
            }
        }



        $sejoursValidOrgaDirAAfficher = array();

        //Récupération des séjours en Direction
        $sejoursDirection = $personne->getSejoursDirection()->toArray();

        foreach ($sejoursDirection as $sejourDirection) {
            if ($sejourDirection->isStatusVisible()) {
                $sejoursValidOrgaDirAAfficher[$sejourDirection->getId()] =
                    [
                        'sejour' => $sejourDirection,
                        'roles' => [Personne::LABEL_ROLE_METIER_DIRECTION_SEJOUR]
                    ];
            }
            $statusSejour = $this->sejourWorkflow->getMarking($sejourDirection);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                $archivesSejours[$sejourDirection->getId()] = $sejourDirection;
            }
        }

        //Récupération des séjours en Direction Adjointe
        $sejoursDirectionAdjointe = $personne->getSejoursDirectionAdjointe()->toArray();

        foreach ($sejoursDirectionAdjointe as $sejourDirectionAdjointe) {
            if ($sejourDirectionAdjointe->isStatusVisible()) {
                $sejoursValidOrgaDirAAfficher[$sejourDirectionAdjointe->getId()] =
                    [
                        'sejour' => $sejourDirectionAdjointe,
                        'roles' => [Personne::LABEL_ROLE_METIER_DIRECTION_ADJOINTE_SEJOUR]
                    ];
            }
            $statusSejour = $this->sejourWorkflow->getMarking($sejourDirectionAdjointe);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                $archivesSejours[$sejourDirectionAdjointe->getId()] = $sejourDirectionAdjointe;
            }
        }
        //Récupération des séjours en organisation
        $structuresOrganisation = $personne->getStructuresOrganisation();

        /**  @var Structure $structureOrganisation */
        foreach ($structuresOrganisation as $structureOrganisation) {
            /**  @var Sejour $sejourOrganisation */
            foreach ($structureOrganisation->getSejoursOrganises() as $sejourOrganisation) {
                if ($sejourOrganisation->isStatusVisible()) {
                    if (array_key_exists($sejourOrganisation->getId(), $sejoursValidOrgaDirAAfficher)) {
                        $sejoursValidOrgaDirAAfficher[$sejourOrganisation->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR;
                    } else {
                        $sejoursValidOrgaDirAAfficher[$sejourOrganisation->getId()] =
                            [
                                'sejour' => $sejourOrganisation,
                                'roles' => [Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR]
                            ];
                    }
                }

                $statusSejour = $this->sejourWorkflow->getMarking($sejourOrganisation);
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                    $archivesSejours[$sejourOrganisation->getId()] = $sejourOrganisation;
                }
            }
        }

        //Récupération des séjours en validation régionale
        $structuresValidation = $personne->getStructuresValidation();



        /**  @var Structure $structureValidation */
        foreach ($structuresValidation as $structureValidation) {
            /**  @var Structure $structureAValider */
            foreach ($structureValidation->getStructuresFilles() as $structureAValider) {
                foreach ($structureAValider->getSejoursOrganises() as $sejourAValider) {
                    if ($sejourAValider->isStatusVisible()) {
                        if (array_key_exists($sejourAValider->getId(), $sejoursValidOrgaDirAAfficher)) {
                            $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR;
                        } else {
                            $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()] =
                                [
                                    'sejour' => $sejourAValider,
                                    'roles' => [Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR]
                                ];
                        }
                    }
                    $statusSejour = $this->sejourWorkflow->getMarking($sejourAValider);
                    if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                        $archivesSejours[$sejourAValider->getId()] = $sejourAValider;
                    }
                }
            }
        }

        //Récupération des séjours en validation déléguée
        $sejoursValidation = $personne->getSejoursValidation()->toArray();
        /**  @var Sejour $sejourValidationDeleguee */
        foreach ($sejoursValidation as $sejourValidationDeleguee) {
            if ($sejourValidationDeleguee->isStatusVisible()) {
                if (array_key_exists($sejourValidationDeleguee->getId(), $sejoursValidOrgaDirAAfficher)) {
                    $sejoursValidOrgaDirAAfficher[$sejourValidationDeleguee->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR;
                } else {
                    $sejoursValidOrgaDirAAfficher[$sejourValidationDeleguee->getId()] =
                        [
                            'sejour' => $sejourValidationDeleguee,
                            'roles' => [Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR]
                        ];
                }
            }
            $statusSejour = $this->sejourWorkflow->getMarking($sejourValidationDeleguee);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                $archivesSejours[$sejourValidationDeleguee->getId()] = $sejourValidationDeleguee;
            }
        }

        //Récupération des séjours en validation internationale
        if ($this->security->isGranted(Personne::ROLE_VALIDATION_INTERNATIONALE)) {
            $sejoursValidationInternationale = $this->sejourRepository->findSejoursInternationaux();

            /**  @var Sejour $sejoursValidationInternationale */
            foreach ($sejoursValidationInternationale as $sejourAValider) {
                if ($sejourAValider->isStatusVisible()) {
                    if (array_key_exists($sejourAValider->getId(), $sejoursValidOrgaDirAAfficher)) {
                        $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE;
                    } else {
                        $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()] =
                            [
                                'sejour' => $sejourAValider,
                                'roles' => [Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE]
                            ];
                    }
                }
            }
            //Ajout des archives des séjours internationaux
            $archivesSejoursValidationInternationale = $this->sejourRepository->findArchivesSejoursInternationaux();
            foreach ($archivesSejoursValidationInternationale as $sejourInternationalArchive) {
                $archivesSejours[$sejourInternationalArchive->getId()] = $sejourInternationalArchive;
            }
        }




        //Récupération des séjours en validation nationale
        if ($this->security->isGranted('ROLE_VALIDATION_NATIONALE')) {
            $sejoursValidationNationale = $this->sejourRepository->findSejoursNationaux();
            /**  @var Sejour $sejoursValidationInternationale */
            foreach ($sejoursValidationNationale as $sejourAValider) {
                if ($sejourAValider->isStatusVisible()) {
                    if (array_key_exists($sejourAValider->getId(), $sejoursValidOrgaDirAAfficher)) {
                        $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE;
                    } else {
                        $sejoursValidOrgaDirAAfficher[$sejourAValider->getId()] =
                            [
                                'sejour' => $sejourAValider,
                                'roles' => [Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE]
                            ];
                    }
                }
            }
            //Ajout des archives des séjours internationaux
            $archivesSejoursValidationNationale = $this->sejourRepository->findArchivesSejoursNationaux();
            foreach ($archivesSejoursValidationNationale as $sejourNationalArchive) {
                $archivesSejours[$sejourNationalArchive->getId()] = $sejourNationalArchive;
            }
        }


        //Récupération des séjours en visite
        $nominationsVisites = $personne->getNominationsVisiteSejour();

        /**  @var NominationVisiteSejour $nominationVisite */
        foreach ($nominationsVisites as $nominationVisite) {
            $sejour_a_visiter = $nominationVisite->getSejour();
            if ($sejour_a_visiter->isStatusVisible()) {
                if (array_key_exists($sejour_a_visiter->getId(), $sejoursValidOrgaDirAAfficher)) {
                    $sejoursValidOrgaDirAAfficher[$sejour_a_visiter->getId()]['roles'][] = Personne::LABEL_ROLE_METIER_VISITE_SEJOUR;
                } else {
                    $sejoursValidOrgaDirAAfficher[$sejour_a_visiter->getId()] =
                        [
                            'sejour' => $sejour_a_visiter,
                            'roles' => [Personne::LABEL_ROLE_METIER_VISITE_SEJOUR]
                        ];
                }
            }

            $statusSejour = $this->sejourWorkflow->getMarking($sejour_a_visiter);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                $archivesSejours[$sejour_a_visiter->getId()] = $sejour_a_visiter;
            }
        }



        $sejoursParticipationAAfficher = array();
        //Récupération des séjours en Encadrement
        $sejoursParticipation = $personne->getSejoursEncadrement();

        foreach ($sejoursParticipation as $sejourParticipation) {
            if ($sejourParticipation->isStatusVisible()) {
                $sejoursParticipationAAfficher[] = $sejourParticipation;
            }
            $statusSejour = $this->sejourWorkflow->getMarking($sejourParticipation);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
                $archivesSejours[$sejourParticipation->getId()] = $sejourParticipation;
            }
        }

        $resumeSejours = new ResumeSejours($this->sejourWorkflow);

        $liste_tous_sejours = array();

        foreach ($sejoursValidOrgaDirAAfficher as $sejour) {
            $liste_tous_sejours[$sejour['sejour']->getId()] = $sejour['sejour'];
        }
        foreach ($sejoursSuivisAAfficher as $sejour) {
            $liste_tous_sejours[$sejour->getId()] = $sejour;
        }
        foreach ($sejoursParticipationAAfficher as $sejour) {
            $liste_tous_sejours[$sejour->getId()] = $sejour;
        }

        foreach ($archivesSejours as $sejour) {
            $liste_tous_sejours[$sejour->getId()] = $sejour;
        }

        foreach ($liste_tous_sejours as $sejour) {
            $resumeSejours->ajouterSejour($sejour);
        }

        return
            [
                "sejoursValidOrgaDir" => $sejoursValidOrgaDirAAfficher,
                "sejoursSuivis" => $sejoursSuivisAAfficher,
                "sejoursParticipation" => $sejoursParticipationAAfficher,
                "sejoursArchives" => $archivesSejours,
                'resumeSejours' => $resumeSejours
            ];
    }
}
