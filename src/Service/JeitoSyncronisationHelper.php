<?php

namespace App\Service;

use App\Entity\Adherent;
use App\Entity\Adhesion;
use App\Entity\AdressePostale;
use App\Entity\CompteRenduInstance;
use App\Entity\ContratTravail;
use App\Entity\Coordonnees;
use App\Entity\Employe;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\ParametreRoads;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Message\MessageSynchroTempsReel;
use App\Repository\AdherentRepository;
use App\Repository\AdhesionRepository;
use App\Repository\ContratTravailRepository;
use App\Repository\EmployeRepository;
use App\Repository\EquipeRepository;
use App\Repository\FonctionRepository;
use App\Repository\ParametreRoadsRepository;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use libphonenumber\PhoneNumber;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Helper pour la gestion des droits dans Agora
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class JeitoSyncronisationHelper
{

    public function __construct(
        private  HttpClientInterface $client,
        private String $jeitoApiToken,
        private String $jeitoApiUrl,
        private StructureRepository $structureRepository,
        private EquipeRepository $equipeRepository,
        private  PersonneRepository $personneRepository,
        private  AdherentRepository $adherentRepository,
        private EmployeRepository $employeRepository,
        private ContratTravailRepository $contratTravailRepository,
        private  FonctionRepository $fonctionRepository,
        private AdhesionRepository $adhesionRepository,
        private  ParametreRoadsRepository $parametreRoadsRepository,
        private  EntityManagerInterface $entityManager,
        private  ValidatorInterface $validatorInterface,
        private  MailerInterface $mailer
    ) {}


    public function createOrUpdateStructureUnitaire(array $donneesStructure)
    {

        $dateCourante = new DateTime();


        $uuid = $donneesStructure['uuid'];
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuid]);

        $parent_uuid = $donneesStructure['parent_uuid'];
        $structureParent = $this->structureRepository->findOneBy(['uuid' => $parent_uuid]);


        $this->createOrUpdateStructure($structure, $structureParent, $donneesStructure, $dateCourante);
        $this->entityManager->flush();
    }


    public function createOrUpdateStructure(?Structure $structure, ?Structure $structureParent, array $donneesStructure, DateTime $dateCourante): Structure
    {

        $nom = $donneesStructure['name'];
        $type = $donneesStructure['type'];
        if (!$structure) {
            $uuid = $donneesStructure['uuid'];
            //On crée une Structure
            $structure = new Structure();
            $structure->setUuid(Uuid::fromString($uuid));
        }

        /** @var Structure $structure */
        $structure->setStatut($donneesStructure['status']);
        $structure->setNom($nom);
        $structure->setType($type);
        $structure->setEchelon(Structure::MAP_ECHELON_BY_TYPE_STRUCTURE[$type]);
        $structure->setDateModification($dateCourante);

        //Ajout de la structure parent

        if ($structureParent) {
            $structure->setStructureParent($structureParent);
        } else {
            $parent_uuid = $donneesStructure['parent_uuid'];
            if ($parent_uuid) { // On envoie une erreur uniquement si le parentId est défini
                throw new Exception("La structure parent \"" . $parent_uuid . "\" de la structure " . $nom . " (" . $uuid . ") n'existe pas, la structure sera ajoutée sans parent");
            }
            $structure->setStructureParent(null);
        }

        $this->entityManager->persist($structure);
        return $structure;
    }

    public function createOrUpdateEquipeUnitaire(?array $dataEquipe)
    {

        $dateCourante = new DateTime();
        $uuid = $dataEquipe['uuid'];

        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuid]);

        $structure_uuid = $dataEquipe['structure_uuid'];
        $structureParent = $this->structureRepository->findOneBy(['uuid' => $structure_uuid]);

        $this->createOrUpdateEquipe($equipe, $structureParent, $dataEquipe, $dateCourante);
        $this->entityManager->flush();
    }
    public function createOrUpdateEquipe(?Equipe $equipe, ?Structure $structureParent, array $dataEquipe, DateTime $dateCourante): Equipe
    {
        //Cas des types d'équipe personnalisés
        $nom = $dataEquipe['name'];
        $uuid = $dataEquipe['uuid'];
        $type = $dataEquipe['type'];
        if (!$equipe) {
            //On crée une Equipe
            $equipe = new Equipe();
            $equipe->setUuid(Uuid::fromString($uuid));
        }
        $creationDateAsString = $dataEquipe['creation_date'];
        if (null !== $creationDateAsString) {
            $equipe->setDateCreation(DateTime::createFromFormat("U", strtotime($creationDateAsString)));
        }
        $desactivationDateAsString = $dataEquipe['deactivation_date'];
        if (null !== $desactivationDateAsString) {
            $equipe->setDateDesactivation(DateTime::createFromFormat("U", strtotime($desactivationDateAsString)));
        }
        $equipe->setNom($nom);
        $equipe->setType($type);
        $equipe->setDateModification($dateCourante);

        if ($structureParent !== null) {
            $equipe->setStructure($structureParent);
            $this->entityManager->persist($equipe);
        } else {

            $structure_uuid = $dataEquipe['structure_uuid'];
            throw new Exception("La structure parent \"" . $structure_uuid . "\" de l'équipe " . $nom . " (" . $uuid . ") n'existe pas, l'équipe ne sera pas ajoutée ni mise à jour");
        }
        return $equipe;
    }

    public function createOrUpdatePersonneUnitaire(array $dataPersonne)
    {
        $contrainteEmailValid = new Assert\Email([
            'message' => 'L\'email "{{ value }}" n\'est pas valide.'
        ]);

        $dateCourante = new DateTime();
        $doublons = array();
        $uuid = $dataPersonne['uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuid]);
        $uuidResponsableLegal1 = $dataPersonne['legal_guardian1_uuid'];
        $responsableLegal1 = $this->personneRepository->findOneBy(['uuid' => $uuidResponsableLegal1]);
        $uuidResponsableLegal2 = $dataPersonne['legal_guardian2_uuid'];
        $responsableLegal2 = $this->personneRepository->findOneBy(['uuid' => $uuidResponsableLegal2]);
        foreach ($dataPersonne['merged_with'] as $uuidDoublon) {
            $doublon = $this->personneRepository->findOneBy(['uuid' => $uuidDoublon]);
            if ($doublon) {
                $doublons[] = $doublon;
            }
        }

        $this->createOrUpdatePersonne($personne, $responsableLegal1, $responsableLegal2, $doublons, $dataPersonne, $dateCourante, $contrainteEmailValid);
        $this->entityManager->flush();
    }


    public function createOrUpdatePersonne(?Personne $personne, ?Personne $responsableLegal1, ?Personne $responsableLegal2, array $doublonsPersonne, array $dataPersonne, DateTime $dateCourante, Constraint $contrainteEmailValid): Personne
    {
        $uuid = $dataPersonne['uuid'];
        $uuidResponsableLegal1 = $dataPersonne['legal_guardian1_uuid'];
        $uuidResponsableLegal2 = $dataPersonne['legal_guardian2_uuid'];
        $genre = $dataPersonne['gender'];
        $prenom = $dataPersonne['first_name'];
        $nom = $dataPersonne['last_name'];
        $dateNaissanceString = $dataPersonne['birthdate'];
        $dateNaissance = null;
        if ($dateNaissanceString !== null) {
            $dateNaissance = DateTime::createFromFormat("Y-m-d|", $dateNaissanceString);
        }

        //Coordonnées
        $address1 = $dataPersonne['address1'];
        $address2 = $dataPersonne['address2'];
        $address3 = $dataPersonne['address3'];
        $codePostal = $dataPersonne['postal_code'];
        $ville = $dataPersonne['city'];
        $pays = $dataPersonne['country'];

        $email = $dataPersonne['email'];

        $droitImage = $dataPersonne['image_rights'];
        if ($droitImage == null) {
            $droitImage = false;
        }

        /** @var Personne $personne */
        if (!$personne) {
            //On crée une Personne
            $personne = new Personne();
        }

        foreach ($doublonsPersonne as $doublon) {
            $personne->fusionDoublon($doublon);
            $this->entityManager->persist($doublon);
        }


        $personne->setUuid(Uuid::fromString($uuid));
        $personne->setDateModification($dateCourante);
        $personne->setNom($nom);
        $personne->setPrenom($prenom);
        $personne->setDateNaissance($dateNaissance);
        $personne->setGenre($genre);
        $personne->setDroitImage($droitImage);
        $personne->setAdresseMail($email);

        $coordonnees = $personne->getCoordonnees();
        if (!$coordonnees) {
            $coordonnees = new Coordonnees();
            $personne->setCoordonnees($coordonnees);
        }
        $coordonnees->setNom($nom);
        $coordonnees->setPrenom($prenom);
        $coordonnees->setAdresseMail($email);
        if ($dataPersonne['home_phone'] !== null && $dataPersonne['home_phone'] !== "") {
            $telDomicile = new PhoneNumber();
            $telDomicile->setRawInput($dataPersonne['home_phone']);
        } else {
            $telDomicile = null;
        }
        $coordonnees->setTelDomicile($telDomicile);
        if ($dataPersonne['mobile_phone'] !== null && $dataPersonne['mobile_phone'] !== "") {

            $telMobile = new PhoneNumber();
            $telMobile->setRawInput($dataPersonne['mobile_phone']);
        } else {
            $telMobile = null;
        }
        $coordonnees->setTelMobile($telMobile);

        //Adresse postale
        $adressePostale = $coordonnees->getAdressePostale();
        if (!$adressePostale) {
            $adressePostale = new AdressePostale();
            $coordonnees->setAdressePostale($adressePostale);
        }
        $adressePostale->setAdresse($address1);
        $adressePostale->setComplementAdresse($address2 . " " . $address3);
        $adressePostale->setCodePostal($codePostal);
        $adressePostale->setVille($ville);
        $adressePostale->setPays($pays);

        //Responsable Légal N°1
        if (!$uuidResponsableLegal1) {
            $personne->setResponsableLegal1(null);
        } else {
            if ($responsableLegal1) {
                $personne->setResponsableLegal1($responsableLegal1);
            } else {
                throw new Exception("Le responsable légal n° 1 \"" . $uuidResponsableLegal1 . "\" de la personne " . $prenom . " " . $nom . " (" . $uuid . ") n'existe pas, la personne n'est pas ajoutée ni mise à jour");
            }
        }
        //Responsable Légal N°2
        if (!$uuidResponsableLegal2) {
            $personne->setResponsableLegal2(null);
        } else {
            /** @var Personne $responsableLegal2 */
            if ($responsableLegal2) {
                $personne->setResponsableLegal2($responsableLegal2);
            } else {
                throw new Exception("Le responsable légal n° 2 \"" . $uuidResponsableLegal2 . "\" de la personne " . $prenom . " " . $nom . " (" . $uuid . ") n'existe pas, la personne n'est pas ajoutée ni mise à jour");
            }
        }
        //Validation de la personne

        //Cas spécifique d'une adresse mail mal renseignée (mauvais format)
        $errors_email = $this->validatorInterface->validate($personne->getAdresseMail(), $contrainteEmailValid);
        if (0 !== count($errors_email)) {
            //Si l'adresse mail est dans un format incorrect, on positionne l'adresse mail à null
            $personne->setAdresseMail("");
            foreach ($errors_email as $violation) {

                throw new Exception("Attention: L'adresse e-mail à été positionnée à vide pour la personne n°" . $personne->getUuid() . ': CHAMP='
                    . $violation->getPropertyPath() . " (VALEUR INITIALE=\"" . $violation->getInvalidValue() . "\"): " . $violation->getMessage());
            }
        }

        $errors = $this->validatorInterface->validate($personne);

        if (0 !== count($errors)) {
            // there are errors, now you can show them
            foreach ($errors as $violation) {

                throw new Exception("Erreur pour la personne n°" . $personne->getUuid() . ': CHAMP='
                    . $violation->getPropertyPath() . " (VALEUR=\"" . $violation->getInvalidValue() . "\"): " . $violation->getMessage());
            }
        } else {

            $this->entityManager->persist($personne);
        }
        return $personne;
    }
    public function createOrUpdateAdherentUnitaire(array $dataAdherent)
    {
        $dateCourante = new DateTime();

        $uuid = $dataAdherent['uuid'];
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuid]);
        $uuidPersonne = $dataAdherent['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);

        $this->createOrUpdateAdherent($adherent, $personne, $dataAdherent, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateAdherent(?Adherent $adherent, ?Personne $personne, array $dataAdherent, DateTime $dateCourante): Adherent
    {
        $uuid = $dataAdherent['uuid'];
        $numeroAdherent = $dataAdherent['id'];
        $status = $dataAdherent['status'];
        $uuidPersonne = $dataAdherent['person_uuid'];
        /** @var Adherent $adherent */
        if (!$adherent) {
            //On crée un adhérent
            $adherent = new Adherent();
        }
        $adherent->setUuid(Uuid::fromString($uuid));
        /** @var Personne $personne */
        if ($personne) {
            $adherent->setPersonne($personne);
            $adherent->setDateModification($dateCourante);
            $adherent->setStatut($status);
            $adherent->setNumeroAdherent($numeroAdherent);
            $this->entityManager->persist($adherent);
        } else {
            throw new Exception("La personne d'uuid \"" . $uuidPersonne . "\" de l'adhérent·e de numéro " . $numeroAdherent . " n'existe pas, l'adhérent·e n'est pas inséré");
        }
        return $adherent;
    }

    public function createOrUpdateAdhesionUnitaire(array $dataAdhesion)
    {
        $dateCourante = new DateTime();

        $uuid = $dataAdhesion['uuid'];
        $adhesion = $this->adhesionRepository->findOneBy(['uuid' => $uuid]);
        $uuidAdherent = $dataAdhesion['adherent_uuid'];
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuidAdherent]);
        $uuidStructure = $dataAdhesion['structure_uuid'];
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuidStructure]);

        $this->createOrUpdateAdhesion($adhesion, $adherent, $structure, $dataAdhesion, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateAdhesion(?Adhesion $adhesion, ?Adherent $adherent, ?Structure $structure, array $dataAdhesion, DateTime $dateCourante)
    {
        $message = array();

        $uuid = $dataAdhesion['uuid'];
        $dateDebutAsString = $dataAdhesion['begin'];
        $dateFin = $dataAdhesion['end'];
        $canceled = $dataAdhesion['canceled'];
        $uuidStructure = $dataAdhesion['structure_uuid'];
        $uuidAdherent = $dataAdhesion['adherent_uuid'];
        if (!$adhesion) {
            //On crée une adhésion
            $adhesion = new Adhesion();
        }
        if ($adherent) {
            if ($structure) {
                $adhesion->setStructure($structure);
                $adhesion->setUuid(Uuid::fromString($uuid));
                $adhesion->setAdherent($adherent);
                $adhesion->setDateModification($dateCourante);
                $dateDebut = DateTime::createFromFormat("Y-m-d|", $dateDebutAsString);
                if ($dateDebut) {
                    $adhesion->setDateDebut($dateDebut);
                    $adhesion->setDateFin(DateTime::createFromFormat("Y-m-d|", $dateFin));
                    $adhesion->setAnnule($canceled);
                    $this->entityManager->persist($adhesion);
                } else {
                    throw new Exception("Pour l'adhésion d'id Jéito\"" . $uuid . "\" la date de début est incorrecte, l'adhésion n'est pas insérée");
                }
            } else {
                throw new Exception("La structure d'id Jéito\"" . $uuidStructure . "\" pour l'adhésion d'id Jéito" . $uuid . " n'existe pas, l'adhésion n'est pas insérée");
            }
        } else {
            throw new Exception("L'adhérent·e avec le numéro \"" . $uuidAdherent . "\" pour l'adhésion d'id Jéito " . $uuid . " n'existe pas, l'adhésion n'est pas insérée");
        }
        return $message;
    }

    public function createOrUpdateEmployeUnitaire(array $dataEmploye)
    {
        $dateCourante = new DateTime();

        $uuid = $dataEmploye['uuid'];
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuid]);
        $uuidPersonne = $dataEmploye['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);


        $this->createOrUpdateEmploye($employe, $personne, $dataEmploye, $dateCourante);

        $this->entityManager->flush();
    }

    public function createOrUpdateEmploye(?Employe $employe, ?Personne $personne, array $dataEmploye, DateTime $dateCourante)
    {
        $message = array();

        $uuid = $dataEmploye['uuid'];
        $numeroSalarie = $dataEmploye['id'];
        $uuidPersonne = $dataEmploye['person_uuid'];


        if (!$employe) {
            //On crée un employe
            $employe = new Employe();
        }
        /** @var Personne $personne */
        if ($personne) {
            $employe->setUuid(Uuid::fromString($uuid));
            $employe->setNumeroSalarie($numeroSalarie);
            $employe->setPersonne($personne);
            $employe->setDateModification($dateCourante);
            $employe->setEmail($dataEmploye['email']);
            if ($dataEmploye['phone'] && $dataEmploye['phone'] !== "") {
                $telEmploye = new PhoneNumber();
                $telEmploye->setRawInput($dataEmploye['phone']);
            } else {
                $telEmploye = null;
            }

            $employe->setTelephone($telEmploye);
            $this->entityManager->persist($employe);
        } else {
            throw new Exception("La personne \"" . $uuidPersonne . "\" de l'employé·e " . $numeroSalarie . " n'existe pas, l'adhésion n'est pas insérée");
        }
        return $message;
    }


    public function createOrUpdateContratTravailUnitaire(array $dataContratTravail)
    {
        $dateCourante = new DateTime();

        $uuid = $dataContratTravail['uuid'];
        $contratTravail = $this->contratTravailRepository->findOneBy(['uuid' => $uuid]);
        $uuidEmploye = $dataContratTravail['employee_uuid'];
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuidEmploye]);

        $this->createOrUpdateContratTravail($contratTravail, $employe, $dataContratTravail, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateContratTravail(?ContratTravail $contratTravail, ?Employe $employe, array $dataContratTravail, DateTime $dateCourante)
    {
        $message = array();

        $uuid = $dataContratTravail['uuid'];
        $dateDebut = $dataContratTravail['begin'];
        $dateFinString = $dataContratTravail['end'];
        $nom = $dataContratTravail['name'];
        $type = $dataContratTravail['type'];
        $uuidEmploye = $dataContratTravail['employee_uuid'];
        /** @var ContratTravail $contratTravail */
        if (!$contratTravail) {
            //On crée un contrat de travail
            $contratTravail = new ContratTravail();
        }
        if ($employe) {
            $contratTravail->setUuid(Uuid::fromString($uuid));
            $contratTravail->setEmploye($employe);
            $contratTravail->setDateModification($dateCourante);
            $contratTravail->setDateDebut(DateTime::createFromFormat("Y-m-d|", $dateDebut));
            $dateFin = null;
            if ($dateFinString !== null) {
                $dateFin = DateTime::createFromFormat("Y-m-d|", $dateFinString);
            }

            $contratTravail->setDateFin($dateFin);
            $contratTravail->setNom($nom);
            $contratTravail->setType($type);
            $this->entityManager->persist($contratTravail);
        } else {
            throw new Exception("L'employé·e \"" . $uuidEmploye . "\" du contrat de travail " . $uuid . " n'existe pas, le contrat de travail n'est pas inséré");
        }

        return $message;
    }
    public function createOrUpdateFonctionUnitaire(array $dataFonction)
    {
        $dateCourante = new DateTime();

        $uuid = $dataFonction['uuid'];
        $fonction = $this->fonctionRepository->findOneBy(['uuid' => $uuid]);

        $uuidPersonne = $dataFonction['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);

        $uuidEquipe = $dataFonction['team_uuid'];
        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuidEquipe]);

        $type_uuid = $dataFonction['type_uuid'];


        $this->createOrUpdateFonction($fonction, $personne, $equipe, $dataFonction, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateFonction(?Fonction $fonction, ?Personne $personne, ?Equipe $equipe, array $dataFonction, DateTime $dateCourante)
    {
        $message = array();
        $uuid = $dataFonction['uuid'];
        $dateDebut = $dataFonction['begin'];
        $dateFin = $dataFonction['end'];
        $nom = $dataFonction['name'];
        $uuidEquipe = $dataFonction['team_uuid'];
        $uuidPersonne = $dataFonction['person_uuid'];
        $type = $dataFonction['type'];
        if ($personne) {
            if ($equipe) {
                if (!$fonction) {
                    //On crée une fonction
                    $fonction = new Fonction();
                }
                /** @var Fonction $fonction */
                $fonction->setEquipe($equipe);
                $fonction->setUuid(Uuid::fromString($uuid));
                $fonction->setPersonne($personne);
                $fonction->setDateModification($dateCourante);
                $fonction->setDateDebut(DateTime::createFromFormat("Y-m-d|", $dateDebut));
                if ($dateFin !== null) {
                    $fonction->setDateFin(DateTime::createFromFormat("Y-m-d|", $dateFin));
                } else {
                    $fonction->setDateFin(null);
                }
                $fonction->setNom($nom);
                $fonction->setType($type);
                $fonction->setStatut(Fonction::MAP_CATEGORY_BY_TYPE_FONCTION[$type]);
                //Si la fonction est active:
                if (
                    $dateCourante >= $fonction->getDateDebut()
                    && ($fonction->getDateFin() == null || $dateCourante <= $fonction->getDateFin())
                ) {
                    $this->ajouterDroitsFonction($fonction);
                } else {
                    //Si la fonction est inactive, on ne supprime les droits que si la fonction s'est terminée la veille
                    $dateHier = clone $dateCourante;
                    $dateHier->modify('-1 day');
                    if ($fonction->getDateFin() !== null && $fonction->getDateFin() >= $dateHier)
                        $this->supprimerDroitsFonction($fonction);
                }

                $this->entityManager->persist($fonction);
            } else {
                throw new Exception("L'équipe \"" . $uuidEquipe . "\" pour la fonction " . $uuid . " n'existe pas, la fonction n'est pas insérée");
            }
        } else {
            throw new Exception("La personne \"" . $uuidPersonne . "\" pour la fonction " . $uuid . " n'existe pas, la fonction n'est pas insérée");
        }


        return $message;
    }

    public function suppressionFonctionUnitaire(String $uuid)
    {
        $fonction = $this->fonctionRepository->findOneBy(['uuid' => $uuid]);
        if ($fonction) {
            $this->suppressionFonction($fonction);
            $this->entityManager->flush();
        } else {
            throw new Exception("La fonction \"" . $uuid . "\" n'existe pas, la fonction n'est pas supprimée");
        }
    }
    public function suppressionFonction(Fonction $fonction)
    {
        $this->supprimerDroitsFonction($fonction);
        $this->entityManager->remove($fonction);
    }

    public function suppressionAdhesionUnitaire(String $uuid)
    {
        $adhesion = $this->adhesionRepository->findOneBy(['uuid' => $uuid]);
        if ($adhesion) {
            $this->suppressionAdhesion($adhesion);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'adhésion \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionAdhesion(Adhesion $adhesion)
    {
        $this->entityManager->remove($adhesion);
    }

    public function suppressionAdherentUnitaire(String $uuid)
    {
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuid]);
        if ($adherent) {
            $this->suppressionAdherent($adherent);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'adhérent·e \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionAdherent(Adherent $adherent)
    {
        $this->entityManager->remove($adherent);
    }

    public function suppressionStructureUnitaire(String $uuid)
    {
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuid]);
        if ($structure) {
            $this->suppressionStructure($structure);
            $this->entityManager->flush();
        } else {
            throw new Exception("La structure \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionStructure(Structure $structure)
    {
        $this->entityManager->remove($structure);
        //Si la structure à supprimer avait organisé des séjours, on ne la supprime pas, mais on la ferme et on met un message de warning
        if ($structure->getSejoursOrganises()->count() > 0) {
            $structure->setStatut(Structure::STATUT_STRUCTURE_FERMEE);
            $this->entityManager->persist($structure);
            $this->entityManager->flush();
            throw new Exception('La structure obsolète ' . $structure->getNom() . " a organisé des séjours, elle ne sera pas supprimée.");
        } else {
            //Pour chacun des regroupements concernés, on passe la structure en non eedf en mettant que cette structure a été supprimée
            foreach ($structure->getStructuresRegroupementSejour() as $structureRegroupement) {
                $structureRegroupement->setEstEEDF(false);
                $structureRegroupement->setNomStructureNonEEDF($structure->getNom() . " (structure EEDF supprimée de Jéito)");
                $structureRegroupement->setStructureEEDF(null);
                $this->entityManager->persist($structureRegroupement);
                $this->entityManager->remove($structure);
                $this->entityManager->flush();
                throw new Exception('La structure obsolète ' . $structure->getNom() . " a participé à des séjours, elle ne sera bien supprimée, mais ses regroupements seront positionnés à non EEDF.");
            }
        }
    }

    public function suppressionEquipeUnitaire(String $uuid)
    {
        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuid]);
        if ($equipe) {
            $this->suppressionEquipe($equipe);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'équipe \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionEquipe(Equipe $equipe)
    {
        $this->entityManager->remove($equipe);
    }

    public function suppressionEmployeUnitaire(String $uuid)
    {
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuid]);
        if ($employe) {
            $this->suppressionEmploye($employe);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'employé " . $uuid . " n'existe pas, l'employé n'est pas supprimé");
        }
    }
    public function suppressionEmploye(Employe $employe)
    {
        $this->entityManager->remove($employe);
    }

    public function suppressionContratTravailUnitaire(String $uuid)
    {
        $contratTravail = $this->contratTravailRepository->findOneBy(['uuid' => $uuid]);
        if ($contratTravail) {
            $this->suppressionContratTravail($contratTravail);
            $this->entityManager->flush();
        } else {
            throw new Exception("Le contrat de travail \"" . $uuid . "\" n'existe pas, le contrat de travail n'est pas supprimé");
        }
    }
    public function suppressionContratTravail(ContratTravail $contratTravail)
    {
        $this->entityManager->remove($contratTravail);
    }

    public function suppressionPersonneUnitaire(String $uuid)
    {
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuid]);
        if ($personne) {
            $this->suppressionPersonne($personne);
            $this->entityManager->flush();
        } else {
            throw new Exception("La personne \"" . $uuid . "\" n'existe pas, la personne n'est pas supprimée");
        }
    }
    public function suppressionPersonne(Personne $personne)
    {
        if ($personne->getSejoursAccompagnement()->count() > 0) {
            $premierSejour = true;
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle est définie comme accompagnante: ";
            foreach ($personne->getSejoursAccompagnement() as $sejourAccompagnement) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourAccompagnement->getintitule() . " (" . $sejourAccompagnement->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getSejoursDirection()->count() > 0) {
            $premierSejour = true;
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle est définie comme directrice: ";
            foreach ($personne->getSejoursDirection() as $sejourDirection) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourDirection->getintitule() . " (" . $sejourDirection->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getSejoursDirectionAdjointe()->count() > 0) {
            $premierSejour = true;
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle est définie comme directrice adjointe: ";
            foreach ($personne->getSejoursDirectionAdjointe() as $sejourDirectionAdjointe) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourDirectionAdjointe->getintitule() . " (" . $sejourDirectionAdjointe->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getSejoursEncadrement()->count() > 0) {
            $premierSejour = true;
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle est définie comme encadrante: ";
            foreach ($personne->getSejoursEncadrement() as $sejourEncadrement) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourEncadrement->getintitule() . " (" . $sejourEncadrement->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getCommentaires()->count() > 0) {
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle a déposé des commentaires: ";
            $sejoursCommentes = array();
            foreach ($personne->getCommentaires() as $commentaire) {
                $sejoursCommentes[] = $commentaire->getSejour();
            }
            $premierSejour = true;
            foreach (array_unique($sejoursCommentes) as $sejourCommente) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourCommente->getintitule() . " (" . $sejourCommente->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getNominationsVisiteSejour()->count() > 0) {
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle a été nommée comme visiteuse: ";
            $sejoursVisites = array();
            foreach ($personne->getNominationsVisiteSejour() as $nominationVisite) {
                $sejoursVisites[] = $nominationVisite->getSejour();
            }
            $premierSejour = true;
            foreach (array_unique($sejoursVisites) as $sejourVisite) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourVisite->getintitule() . " (" . $sejourVisite->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        } else if ($personne->getAttributionsVisiteSejour()->count() > 0) {
            $message = "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (uuid n°" . $personne->getUuid()->toRfc4122() . ") ne peut pas être supprimée, car elle a encore au moins un séjour encore présent dans ROADS pour lequel elle a attribué des visites: ";
            $sejoursAttribueVisites = array();
            foreach ($personne->getAttributionsVisiteSejour() as $attributionVisite) {
                $sejoursAttribueVisites[] = $attributionVisite->getSejour();
            }
            $premierSejour = true;
            foreach (array_unique($sejoursAttribueVisites) as $sejourAttribueVisite) {
                if (!$premierSejour) {
                    $message .= ", ";
                } else {
                    $premierSejour = false;
                }
                $message .= $sejourAttribueVisite->getintitule() . " (" . $sejourAttribueVisite->getStructureOrganisatrice()->getNom() . ")";
            }
            throw new Exception($message);
        }
        foreach ($personne->getPersonnesDontEstResponsableLegal1() as $personnesDontEstResponsableLegal1) {
            $personnesDontEstResponsableLegal1->setResponsableLegal1(null);
            $this->entityManager->persist($personnesDontEstResponsableLegal1);
        }
        foreach ($personne->getPersonnesDontEstResponsableLegal2() as $personnesDontEstResponsableLegal2) {
            $personnesDontEstResponsableLegal2->setResponsableLegal2(null);
            $this->entityManager->persist($personnesDontEstResponsableLegal2);
        }

        $this->entityManager->remove($personne);
    }
    function supprimerDroitsFonction(Fonction $fonction)
    {
        $equipe = $fonction->getEquipe();
        $personne = $fonction->getPersonne();
        switch ($fonction->getStatut()) {
            case Fonction::CATEGORY_FONCTION_BENEVOLE:
                switch ($fonction->getType()) {
                    //Ajout du rôle de suivi pour les trésorier·e·s
                    case Fonction::TYPE_FONCTION_TRESORIER:
                        if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                            $personne->removeStructuresSuivi($equipe->getStructure());
                            //Pour le rôle de trésorier·e régional·e, on ajoute le suivi aussi pour toutes les structures filles
                            if ($equipe->getStructure()->getEchelon() == Structure::ECHELON_STRUCTURE_REGIONAL) {
                                /** @var Structure $structureFille */
                                foreach ($equipe->getStructure()->getStructuresFilles() as $structureFille) {
                                    $personne->removeStructuresSuivi($structureFille);
                                }
                            }
                        }
                        break;
                    case Fonction::TYPE_FONCTION_RESPONSABLE:
                        if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                            $personne->removeStructuresOrganisation($equipe->getStructure());
                            //suppression du rôle d'organisat·eur·rice (validat·eur·rice pour les responsables région·ales·aux) pour les responsables de structure
                            if ($equipe->getStructure()->getEchelon() == Structure::ECHELON_STRUCTURE_REGIONAL) {
                                $personne->removeStructuresValidation($equipe->getStructure());
                            }
                        }
                        break;
                }
                break;
            case Fonction::CATEGORY_FONCTION_SALARIE:
                //Suppression du rôle de suivi global pour les salarié·e·s de l'équipe nationale et du pôle de ressources
                if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_NATIONALE) {
                    $personne->removeRole(Personne::ROLE_SUIVI_GLOBAL);
                }
                break;
        }
        $this->entityManager->persist($personne);
    }


    function ajouterDroitsFonction(Fonction $fonction)
    {
        $equipe = $fonction->getEquipe();
        $personne = $fonction->getPersonne();
        switch ($fonction->getStatut()) {
            case Fonction::CATEGORY_FONCTION_BENEVOLE:
                switch ($fonction->getType()) {
                    //Ajout du rôle de suivi pour les trésorier·e·s
                    case Fonction::TYPE_FONCTION_TRESORIER:
                        if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                            $personne->addStructuresSuivi($equipe->getStructure());
                            //Pour le rôle de trésorier·e régional·e, on ajoute le suivi aussi pour toutes les structures filles
                            if ($equipe->getStructure()->getEchelon() == Structure::ECHELON_STRUCTURE_REGIONAL) {
                                /** @var Structure $structureFille */
                                foreach ($equipe->getStructure()->getStructuresFilles() as $structureFille) {
                                    $personne->addStructuresSuivi($structureFille);
                                }
                            }
                        }


                        break;
                    case Fonction::TYPE_FONCTION_RESPONSABLE:
                        if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                            $personne->addStructuresOrganisation($equipe->getStructure());
                            //Ajout du rôle d'organisat·eur·rice (validat·eur·rice pour les responsables région·ales·aux) pour les responsables de structure
                            if ($equipe->getStructure()->getEchelon() == Structure::ECHELON_STRUCTURE_REGIONAL) {
                                $personne->addStructuresValidation($equipe->getStructure());
                            }
                            //On ajoute le role d'organisation de cette personne pour toutes les structures filles sous tutelle
                            foreach ($equipe->getStructure()->getStructuresFilles() as $structureFille) {
                                if ($structureFille->getStatut() == Structure::STATUT_STRUCTURE_RATTACHEE) {
                                    $personne->addStructuresOrganisation($structureFille);
                                }
                            }
                        }
                        break;
                }
                break;
            case Fonction::CATEGORY_FONCTION_SALARIE:
                //Ajout du rôle de suivi global pour les salarié·e·s de l'équipe nationale et du pôle de ressources
                if ($equipe->getType() == Equipe::TYPE_EQUIPE_EQUIPE_NATIONALE) {
                    $personne->addRole(Personne::ROLE_SUIVI_GLOBAL);
                }
                break;
        }
        $this->entityManager->persist($personne);
    }






    function envoiMailErreurSupport(String $erreur, MessageSynchroTempsReel $message)
    {
        $parametreAdresseSupport = $this->parametreRoadsRepository->findOneBy(['nomParametre' => ParametreRoads::PARAMETRE_ADRESSE_SUPPORT]);

        //envoi du mail à l'adresse support:
        if ($parametreAdresseSupport) {
            $adresseSupport = $parametreAdresseSupport->getValeurParametre();
            $message = (new TemplatedEmail())
                // On attribue le destinataire
                ->from($adresseSupport)
                ->to($adresseSupport)
                ->subject("[Support ROADS] Erreur lors de la synchronisation temps réel depuis Jéito vers ROADS via le broker")
                ->htmlTemplate('mails_monitoring_support/erreur_synchro_temps_reel_email.html.twig')
                ->context([
                    'erreur' => $erreur,
                    'message' => $message
                ]);

            $this->mailer->send($message);
        }
    }
}
