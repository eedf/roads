<?php

namespace App\Service;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;

/**
 * Helper pour la présentation des rôles métier d'une personne dans ROADS
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class RolesPersonneHelper
{

    /**
     * Génère un array des rôles métiers d'une personne
     *
     * @param  Personne $personne
     * @return mixed[] array avec trois colonnes: 
     * - intitulé du rôle (*ex: 'suivi de séjour', 'organisation de séjour' … *)
     * - type de périmètre du role (*ex: 'structure', 'sejour' …*)
     * - périmètre( la structure, le séjour …)
     */
    public function rolesPersonne(Personne $personne): array
    {
        $roles_metier_personne = array();


        //Rôles organisateur
        foreach ($personne->getStructuresOrganisation() as $structureOrganisation) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR,
                    'perimetre' => $structureOrganisation,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_STRUCTURE,

                ];
        }

        //Rôles validation
        foreach ($personne->getStructuresValidation() as $structureValidation) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR,
                    'perimetre' => $structureValidation,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_STRUCTURE,

                ];
        }

        //Rôles suivi pour une structure entière
        foreach ($personne->getStructuresSuivi() as $structureSuivi) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR,
                    'perimetre' => $structureSuivi,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_STRUCTURE,

                ];
        }

        //Rôles délégation de validation d'un séjour
        foreach ($personne->getSejoursValidation() as $sejourValidation) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR,
                    'perimetre' => $sejourValidation,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Direction de séjour
        foreach ($personne->getSejoursDirection() as $sejour) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_DIRECTION_SEJOUR,
                    'perimetre' => $sejour,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Direction adjointe de séjour
        foreach ($personne->getSejoursDirectionAdjointe() as $sejour) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_DIRECTION_ADJOINTE_SEJOUR,
                    'perimetre' => $sejour,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Participation de séjour
        foreach ($personne->getSejoursEncadrement() as $sejour) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_PARTICIPATION_SEJOUR,
                    'perimetre' => $sejour,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Suivi de séjour
        foreach ($personne->getSejoursSuivis() as $sejour) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR,
                    'perimetre' => $sejour,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Visite de séjour
        /** @var NominationVisiteSejour $nominationVisite */
        foreach ($personne->getNominationsVisiteSejour() as $nominationVisite) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_VISITE_SEJOUR,
                    'perimetre' => $nominationVisite->getSejour(),
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_SEJOUR,

                ];
        }

        //Validation des séjours internationaux
        if ($personne->hasRole(Personne::ROLE_VALIDATION_INTERNATIONALE)) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE,
                    'perimetre' => null,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_GLOBAL,
                ];
        }
        //Validation des séjours nationaux
        if ($personne->hasRole(Personne::ROLE_VALIDATION_NATIONALE)) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE,
                    'perimetre' => null,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_GLOBAL,
                ];
        }
        //Suivi global des séjours
        if ($personne->hasRole(Personne::ROLE_SUIVI_GLOBAL)) {
            $roles_metier_personne[] =
                [
                    'role' => Personne::LABEL_ROLE_METIER_SUIVI_GLOBAL,
                    'perimetre' => null,
                    'type_perimetre' => Personne::PERIMETRE_ROLE_METIER_GLOBAL,
                ];
        }
        return $roles_metier_personne;
    }
}
