<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;

/**
 * @author Jáchym Toušek <enumag@gmail.com>
 */
final class HandlerArgumentsStamp implements NonSendableStampInterface
{
    private array $additionalArguments;
    public function __construct(
        $additionalArguments
    ) {
        $this->additionalArguments = $additionalArguments;
    }

    /**
     * @return array
     */
    public function getAdditionalArguments()
    {
        return $this->additionalArguments;
    }
}
