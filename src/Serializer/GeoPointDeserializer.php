<?php

namespace App\Serializer;

use LongitudeOne\Spatial\PHP\Types\Geography\Point;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


/**
 * Désierialize un array de coordonnées pour le transformer en point géographique
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class GeoPointDeserializer implements DenormalizerInterface
{


    public function __construct() {}
    public function denormalize($data, string $type, ?string $format = null, array $context = []): ?Point
    {
        if ('' === $data || null === $data) {
            throw new NotNormalizableValueException('The data is either an empty string or null, you should pass a string that can be parsed as a Geographic point.');
        }

        try {
            $point = new Point(0, 0);
            $point->setLatitude($data["coordinates"][1]);
            $point->setLongitude($data["coordinates"][0]);
            return $point;
        } catch (\Exception $e) {
            throw new NotNormalizableValueException($e->getMessage());
        }
    }


    public function supportsDenormalization($data, string $type, ?string $format = null): bool
    {
        return Point::class === $type;
    }
    public function getSupportedTypes(?string $format): array
    {
        return [
            Point::class => true
        ];
    }
}
