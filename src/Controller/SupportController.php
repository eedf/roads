<?php

namespace App\Controller;

use App\Entity\ParametreRoads;
use App\Form\EnvoiMessageSupportType;
use App\Repository\ParametreRoadsRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller pour le support utilisateur
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class SupportController extends AbstractController
{

    /**
     * Permet la saisie d'un message et son envoi par mail à l'adresse mail de support
     * Si l'utilisat·eur est connecté·e, les données personnelles sont remplies automatiquement avec les données du compte. Sinon un formulaire est proposé
     * @param  Request $request la requete HTTP
     * @param  MailerInterface $mailer utilitaire d'envoi de mail
     * @param  ParametreRoadsRepository $parametreRoadsRepository pour accéder à l'adresse mail de support
     * @return Response
     * @uses EnvoiMessageSupportType formulaire de saisie du message de support
     */
    #[Route('/support', name: 'support')]
    public function envoiMessageSupportUtilisateursConnectes(Request $request, MailerInterface $mailer, ParametreRoadsRepository $parametreRoadsRepository): Response
    {
        /** @var Personne $user */
        $user = $this->getUser();
        $besoinCoordonnees = true;
        if ($user !== null) {
            $besoinCoordonnees = false;
        }
        $form = $this->createForm(EnvoiMessageSupportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parametreAdresseSupport = $parametreRoadsRepository->findOneBy(['nomParametre' => ParametreRoads::PARAMETRE_ADRESSE_SUPPORT]);

            $adresseSupport = $parametreAdresseSupport->getValeurParametre();
            if ($besoinCoordonnees) {
                $expediteur = $form->get('email')->getData();
                $nomExpediteur = $form->get('nom')->getData();
                $prenomExpediteur = $form->get('prenom')->getData();
                $numeroAdherentOuMatricule = $form->get('numeroAdherentOuMatricule')->getData();
            } else {
                $expediteur = $user->getAdresseMailUsage();
                $nomExpediteur = $user->getNom();
                $prenomExpediteur = $user->getPrenom();
                $numeroAdherentOuMatricule = $user->getNumeroAdherentOuMatricule();
            }
            $contenuMessage = $form->get('message')->getData();
            $rubrique = $form->get('rubrique')->getData();

            // Ici nous enverrons l'e-mail
            $message = (new TemplatedEmail())
                // On attribue l'expéditeur
                ->from($expediteur)

                // On attribue le destinataire
                ->to($adresseSupport)
                ->subject("[Support R.O.A.D.S] Demande de support de la part de $prenomExpediteur $nomExpediteur")
                ->htmlTemplate('support/support_email.html.twig')
                ->context([
                    'numeroAdherentOuMatricule' => $numeroAdherentOuMatricule,
                    'nomExpediteur' => $nomExpediteur,
                    'prenomExpediteur' => $prenomExpediteur,
                    'adresseExpediteur' => $expediteur,
                    'contenuMessage' => $contenuMessage,
                    'rubrique' => $rubrique,
                ]);

            $mailer->send($message);

            return $this->render('support/confirmation_envoi_mail.html.twig', []);
        }
        return $this->render('support/saisie_message.html.twig', [
            'formSupport' => $form->createView(),
            'besoinCoordonnees' => $besoinCoordonnees
        ]);
    }
}
