<?php

namespace App\Controller;

use App\Entity\Adherent;
use App\Entity\Personne;
use App\Entity\DocumentRessource;
use App\Entity\ParametreRoadsTexte;
use App\Repository\PersonneRepository;
use App\Repository\DocumentRessourceRepository;
use App\Repository\NotificationRepository;
use App\Repository\ParametreRoadsTexteRepository;
use App\Security\Client\JeitoClient;
use App\Service\RolesPersonneHelper;
use App\Service\TableauDeBordSejourHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour l'entrée de ROADS
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class RoadsController extends AbstractController
{

    public function __construct() {}


    #[Route('/oauth/login', name: 'oauth_login')]
    public function index2(ClientRegistry $clientRegistry): RedirectResponse
    {
        /** @var JeitoClient $client */
        $client = $clientRegistry->getClient('jeito');
        return $client->redirect();
    }

    #[Route('/oauth/callback', name: 'oauth_check')]
    public function check() {}


    /**
     * Page d'accueil d'une personne connectée.
     *
     * @param  NotificationRepository $notificationRepository Pour accéder au notifications
     * @param  TableauDeBordSejourHelper $tableauDeBordSejourHelper Helper pour afficher les tableaux de bord des séjours
     * @param  DocumentRessourceRepository $documentRessourceRepository Pour accéder aux documents ressources
     * @param  ParametreRoadsTexteRepository $parametreRoadsTexteRepository Pour afficher les textes paramétrable d'accueil
     * @param  RolesPersonneHelper $rolesPersonneHelper pourr afficher les rôles et fonction d'une personne
     * @return Response
     */
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/', name: 'home')]
    public function index(NotificationRepository $notificationRepository, TableauDeBordSejourHelper $tableauDeBordSejourHelper, DocumentRessourceRepository $documentRessourceRepository, ParametreRoadsTexteRepository $parametreRoadsTexteRepository, RolesPersonneHelper $rolesPersonneHelper, PersonneRepository $personneRepository): Response
    {

        $parametrageTexteAccueil = $parametreRoadsTexteRepository->findOneBy(['categorie' => ParametreRoadsTexte::CATEGORIE_ACCUEIL_CONNECTE]);

        /**  @var Personne $personne */
        $personne = $personneRepository->findOneById($this->getUser()->getId());
        //Une personne peut se connecter si elle est adhérent·e active ou si elles est salariée
        if (!$personne->estSalarieValide()) {
            if ($personne->getStatutAdherent() == Adherent::ANCIEN_ADHERENT) {
                $this->addFlash('warning_adhesion_ancien', "Attention, votre cotisation n'est plus à jour, passé un délai d'un mois à partir du commencement de la nouvelle saison, vous n'aurez plus accès à R.O.A.D.S.");
            }
        }

        $notificationsAAfficher = $notificationRepository->findNotificationsAAfficherPourAccueil($personne, new DateTime('7 days ago'));
        $donneesSejours = $tableauDeBordSejourHelper->getTableauDeBordSejour($personne);
        $docWikiRoads = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_WIKI_ROADS]);

        //Roles utilisateur:
        $roles_metier_personne = $rolesPersonneHelper->rolesPersonne($personne);
        return $this->render('roads/home.html.twig', [
            'donneesSejours' => $donneesSejours,
            'docWikiRoads' => $docWikiRoads,
            'parametrageTexteAccueil' => $parametrageTexteAccueil,
            'notificationsAAfficher' => $notificationsAAfficher,
            'roles_metier_personne' => $roles_metier_personne,
            'user' => $personne
        ]);
    }

    /**
     * Page d'accueil de ROADS pour les utilisat·eur·rice·s non connecté·e·s
     * Permet l'authentification ou l'inscription d'un nouvel utilisateur, en vérifiant la validité de leur adhésion
     * @param  PersonneRepository $personneRepository pour accéder aux personnes en BDD
     * @param  Request $request requete HTTP
     * @param  AuthenticationUtils $authenticationUtils Utilitaire d'authentification
     * @return void
     * 
     */
    #[Route('/roads/accueil', name: 'accueil_non_connecte')]
    public function accueil_non_connecte(AuthenticationUtils $authenticationUtils)
    {

        /** @var Personne $userExistant */
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('roads/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * Route pour afficher les mentions légales de ROADS
     * 
     * @return Response
     */
    #[Route('/mentions/legales', name: 'mentions_legales')]
    public function mentionsLegales(): Response
    {
        return $this->render('roads/mentions_legales.html.twig');
    }

    /**
     * API de recherche de personne via tout ou partie de leur nom, prénom, numéro salarié·e ou d'ahérent·e à partir d'une chaîne de caractères passé en paramètre de la requête
     *
     * @param  PersonneRepository $personnceRepository pour accéder à la base de données des personnes
     * @param  Request $request la requête contenant la chaine de caractères ('query') à rechercher
     * @return jspn la liste des personnes correspondant·e·s, ainsi que le code 200
     * @api
     */
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/roads/utility/personnes', methods: ["GET"], name: 'roads_utility_personnes')]
    public function getPeronnesApi(PersonneRepository $personneRepository, Request $request)
    {
        $result_personnes = array();
        $personnes = $personneRepository->findByNomPrenomOrNumAdhOrNumSalaraieField($request->query->get('query'));
        /** @var Personne $personne */
        foreach ($personnes as $personne) {

            $result_personnes[] = $personne->getAffichagePourRechercheAuto();
        }


        return $this->json([
            'personnes' => $result_personnes
        ], 200, [], ['groups' => ['recherche_auto_complete']]);
    }
}
