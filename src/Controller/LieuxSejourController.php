<?php

namespace App\Controller;

use App\Entity\Sejour;
use App\Repository\UtilisationLieuSejourRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Controller pour la gestion des lieux de séjours
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class LieuxSejourController extends AbstractController
{
    /**
     * Permet d'afficher la liste de tous les séjours de ROADS
     *
     * @param  UtilisationLieuSejourRepository $utilisationLieuSejourRepository
     * @return Response
     */
    #[Route('/lieux/sejour', name: 'lieux_sejours')]
    public function listeLieuxsejour(UtilisationLieuSejourRepository $utilisationLieuSejourRepository): Response
    {

        $lieux_sejours = $utilisationLieuSejourRepository->findUtilisationsLieuSejourActifs(Sejour::LISTES_STATUTS_SEJOURS_VISIBLES_VALIDATION);

        return $this->render('lieux_sejour/liste_lieux_sejours.html.twig', [
            'lieux_sejours' => $lieux_sejours,
        ]);
    }
}
