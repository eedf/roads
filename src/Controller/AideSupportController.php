<?php

namespace App\Controller;

use App\Entity\ParametreRoads;
use App\Entity\Personne;
use App\Form\AdresseSupportType;
use App\Form\RecherchePersonneUniqueType;
use App\Repository\ParametreRoadsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour les fonctions d'aide au support
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
#[IsGranted('ROLE_SUPPORT')]
class AideSupportController extends AbstractController
{

    /**
     * Route pour la page d'aide au support
     *  - Changement de l'adresse du support
     *  - Recherche du statut d'un compte d'une personne
     *
     * @param  Request $request Requete HTTP
     * @param  ParametreRoadsRepository $parametreRoadsRepository Pour accéder à l'adresse support actuelle
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @return Response
     * @uses RecherchePersonneUniqueType formulaire de recherche d'un compte utilisateur.
     * @uses AdresseSupportType formulaire pour le changement de l'adresse support
     */
    #[Route('/aide_support', name: 'aide_support')]
    public function aide_support(Request $request, ParametreRoadsRepository $parametreRoadsRepository, EntityManagerInterface $manager, FormFactoryInterface $formFactory): Response
    {

        $adresse_support = $parametreRoadsRepository->findOneBy(['nomParametre' => ParametreRoads::PARAMETRE_ADRESSE_SUPPORT]);
        //Formulaire de changement d'adresse du support
        $formAdresseSupport = $this->createForm(AdresseSupportType::class, $adresse_support, array());
        $formAdresseSupport->handleRequest($request);

        if ($formAdresseSupport->isSubmitted() && $formAdresseSupport->isValid()) {
            $manager->persist($adresse_support);
            $manager->flush();
            $this->addFlash('success', "Adresse de support modifiée");
            return $this->redirectToRoute('aide_support');
        }

        //Formulaire de recherche de statut du compte
        $formRecherchePersonneUniqueStatutCompte = $formFactory->createNamedBuilder("formRecherchePersonneUniqueStatutCompte",  RecherchePersonneUniqueType::class)->getForm();
        $formRecherchePersonneUniqueStatutCompte->handleRequest($request);

        if ($formRecherchePersonneUniqueStatutCompte->isSubmitted() && $formRecherchePersonneUniqueStatutCompte->isValid()) {
            $personne = $formRecherchePersonneUniqueStatutCompte->get('champRecherche')->getData();

            return $this->redirectToRoute('verification_statut_compte_personne', [
                'id' => $personne->getId()
            ]);
        }
        return $this->render('aide_support/general.html.twig', [
            'formRecherchePersonneUniqueStatutCompte' => $formRecherchePersonneUniqueStatutCompte->CreateView(),
            'formAdresseSupport' => $formAdresseSupport->createView()
        ]);
    }

    /**
     * Route pour afficher les informations de compte ROADS d'un·e utilisa·eur·rice
     *
     * @param  Personne $personne la personne concernée
     * @return Response
     */

    #[Route('/aide_support/{id}', name: 'verification_statut_compte_personne')]
    public function statutCompteUtilisateur(Personne $personne): Response
    {
        return $this->render('aide_support/statut_compte.html.twig', [
            'personne' => $personne
        ]);
    }

    /**
     * Permet d'envoyer manuellement le mail d'inscription à une personne
     *
     * @param  Personne $personne la personne concerné·e
     * @return Response retour vers la route de statut du compte utlisateur
     */
    #[Route('/aide_support/{id}/envoi_mail', name: 'renvoi_mail_inscription')]
    public function renvoiMailInscription(Personne $personne): Response
    {
        try {
            $response = $this->forward('App\Controller\RoadsController::envoiMailValidationAdresse', [
                'personne' => $personne,
            ]);
            $this->addFlash('success', "Email d'inscription envoyé!");
        } catch (Exception $e) {
            $this->addFlash('danger', "Erreur lors de l'envoi: " . $e->getMessage());
        }

        return $this->redirectToRoute('verification_statut_compte_personne', [
            'id' => $personne->getId()
        ]);
    }
}
