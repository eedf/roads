<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\Sejour;
use App\Entity\Structure;
use App\Form\AdminAjoutRolePersonneType;
use App\Form\RecherchePersonneUniqueType;
use App\Repository\PersonneRepository;
use App\Repository\SejourRepository;
use App\Service\NotificationHelper;
use App\Service\RolesPersonneHelper;
use App\Utils\NotificationConstants;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour les fonctions d'administration
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('ROLE_ADMIN')]
class AdministrationController extends AbstractController
{
    /**
     * Permet de gérer la route d'adminsitration globale de ROADS:
     *
     *  
     * @param  Request $request
     * @param PersonneRepository $personneRepository pour accéder aux personnes ayant des droits actuellement
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @uses RecherchePersonneUniqueType formulaire de recherche de personnes pour l'ajouter en tant qu'administrateur, d'animateur, d'aide au support, ou pour connaître ses rôles
     * @return Response
     */
    #[Route('/administration/general', name: 'admin_general')]
    public function administration(Request $request, PersonneRepository $personneRepository, EntityManagerInterface $manager, FormFactoryInterface $formFactory): Response
    {
        //******************************* */
        //gestion des administrateurs
        //******************************* */
        $admins = $personneRepository->findByRole(Personne::ROLE_ADMIN);
        //Formulaire de recherche d'utilisateurs unique

        $formRecherchePersonneUniqueAdministrateur = $formFactory->createNamedBuilder("userRechercheUniqueAdmin",  RecherchePersonneUniqueType::class)->getForm();
        $formRecherchePersonneUniqueAdministrateur->handleRequest($request);

        if ($formRecherchePersonneUniqueAdministrateur->isSubmitted() && $formRecherchePersonneUniqueAdministrateur->isValid()) {
            /** @var Personne $personne */
            $personne = $formRecherchePersonneUniqueAdministrateur->get('champRecherche')->getData();
            if ($personne->hasRole(Personne::ROLE_ADMIN)) {
                $this->addFlash('danger_administration', "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ") a déjà le rôle d'administration !");
            } else {
                $personne->addRole(Personne::ROLE_ADMIN);

                if ($personne->estSalarieValide()  || $personne->estAdherentValide()) {
                    $this->addFlash('success_administration', "Rôle d'administration ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
                } else {
                    $this->addFlash('warning_administration', "Rôle d'administration ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . "), dont l'adhésion ou le contrat de travail n'est pas valide.");
                }

                $manager->persist($personne);
                $manager->flush();
            }

            return $this->redirectToRoute('admin_general');
        }

        //******************************* */
        //gestion des animateurs
        //******************************* */
        $animateurs = $personneRepository->findByRole(Personne::ROLE_ANIMATION);
        $formRecherchePersonneUniqueAnimateur = $formFactory->createNamedBuilder("userRechercheUniqueAnim",  RecherchePersonneUniqueType::class)->getForm();
        $formRecherchePersonneUniqueAnimateur->handleRequest($request);

        if ($formRecherchePersonneUniqueAnimateur->isSubmitted() && $formRecherchePersonneUniqueAnimateur->isValid()) {
            /** @var Personne $personne */
            $personne = $formRecherchePersonneUniqueAnimateur->get('champRecherche')->getData();
            if ($personne->hasRole(Personne::ROLE_ANIMATION)) {
                $this->addFlash('danger_animation', "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ") a déjà le rôle d'animation/modération !");
            } else {
                $personne->addRole(Personne::ROLE_ANIMATION);

                if ($personne->estSalarieValide()  || $personne->estAdherentValide()) {
                    $this->addFlash('success_animation', "Rôle d'animation/modération ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
                } else {
                    $this->addFlash('warning_animation', "Rôle d'animation/modération ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . "), dont l'adhésion ou le contrat de travail n'est pas valide.");
                }

                $manager->persist($personne);
                $manager->flush();
            }

            return $this->redirectToRoute('admin_general');
        }

        //******************************* */
        //gestion des membres du support
        //******************************* */
        $supports = $personneRepository->findByRole(Personne::ROLE_SUPPORT);
        $formRecherchePersonneUniqueSupport = $formFactory->createNamedBuilder("userRechercheUniqueSupport",  RecherchePersonneUniqueType::class)->getForm();
        $formRecherchePersonneUniqueSupport->handleRequest($request);

        if ($formRecherchePersonneUniqueSupport->isSubmitted() && $formRecherchePersonneUniqueSupport->isValid()) {
            /** @var Personne $personne */
            $personne = $formRecherchePersonneUniqueSupport->get('champRecherche')->getData();
            if ($personne->hasRole(Personne::ROLE_SUPPORT)) {
                $this->addFlash('danger_support', "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ") a déjà le rôle de support!");
            } else {
                $personne->addRole(Personne::ROLE_SUPPORT);

                if ($personne->estSalarieValide()  || $personne->estAdherentValide()) {
                    $this->addFlash('success_support', "Rôle de support ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
                } else {
                    $this->addFlash('warning_support', "Rôle de support ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . "), dont l'adhésion ou le contrat de travail n'est pas valide.");
                }

                $manager->persist($personne);
                $manager->flush();
            }

            return $this->redirectToRoute('admin_general');
        }

        /****************************** */
        // Gestion des rôles métiers
        /****************************** */
        $formRecherchePersonneUniqueRoles = $formFactory->createNamedBuilder("userRechercheUniqueRoles",  RecherchePersonneUniqueType::class)->getForm();
        $formRecherchePersonneUniqueRoles->handleRequest($request);
        if ($formRecherchePersonneUniqueRoles->isSubmitted() && $formRecherchePersonneUniqueRoles->isValid()) {
            /** @var Personne $personne */
            $personne = $formRecherchePersonneUniqueRoles->get('champRecherche')->getData();
            return $this->redirectToRoute('admin_roles_personne', ['id' => $personne->getId()]);
        }


        return $this->render('administration/general.html.twig', [
            'admins' => $admins,
            'formRecherchePersonneUniqueAdministrateur' => $formRecherchePersonneUniqueAdministrateur->createView(),
            'animateurs' => $animateurs,
            'formRecherchePersonneUniqueAnimateur' => $formRecherchePersonneUniqueAnimateur->createView(),
            'supports' => $supports,
            'formRecherchePersonneUniqueSupport' => $formRecherchePersonneUniqueSupport->createView(),
            'formRecherchePersonneUniqueRoles' => $formRecherchePersonneUniqueRoles->createView()
        ]);
    }
    /**
     * Route pour supprimer le droit d'administration d'une personne. 
     * Si la personne est l'utilisateur faisant la demande, l'utilisat·eur·rice est déconnecté·e de sa session et redirigé vers la page de connexion. 
     * Un message flash est affiché pour confirmé la suppression.
     * Enfin, redirection vers la route d'administration globale
     *  
     * @param  Personne $personne la personne à qui revoquer les droits d'administration
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @return Response route vers administration globale
     */
    #[Route('/admin/role_admin/{id}/suppression', name: 'admin_supprimer_admin')]
    public function suppressionAdmin(Personne $personne, EntityManagerInterface $manager): Response
    {

        $personne->removeRole(Personne::ROLE_ADMIN);
        $this->addFlash('success_administration', "Rôle d'administration supprimé pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
        $manager->persist($personne);
        $manager->flush();


        //Cas spécifique ou l'admin se supprime en tant qu'admin
        if ($this->getUser() == $personne) {
            return $this->redirectToRoute('app_logout');
        }


        return $this->redirectToRoute('admin_general');
    }

    /**
     * Route pour supprimer le droit d'aide au support d'une personne. 
     * Un message flash est affiché pour confirmer la suppression.
     * Enfin, redirection vers la route d'administration globale
     *  
     * @param  Personne $personne la personne à qui revoquer les droits d'aide au support
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @return Response route vers administration globale
     */
    #[Route('/admin/role_support/{id}/suppression', name: 'admin_supprimer_support')]
    public function suppressionSupport(Personne $personne, EntityManagerInterface $manager): Response
    {

        $personne->removeRole(Personne::ROLE_SUPPORT);
        $this->addFlash('success_support', "Rôle de support supprimé pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
        $manager->persist($personne);
        $manager->flush();


        return $this->redirectToRoute('admin_general');
    }
    /**
     * Route pour supprimer le droit d'animation d'une personne. 
     * Un message flash est affiché pour confirmer la suppression.
     * Enfin, redirection vers la route d'administration globale
     *  
     * @param  Personne $personne la personne à qui revoquer les droits d'animation
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @return Response route vers administration globale
     */
    #[Route('/admin/role_animation/{id}/suppression', name: 'admin_supprimer_animation')]
    public function suppressionAnimation(Personne $personne, EntityManagerInterface $manager): Response
    {

        $personne->removeRole(Personne::ROLE_ANIMATION);
        $this->addFlash('success_animation', "Rôle d'animation/modération supprimé pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
        $manager->persist($personne);
        $manager->flush();
        return $this->redirectToRoute('admin_general');
    }


    /**
     * 
     *  Permet de visualiser et de gérer les rôles d'une personne dans ROADS
     * - Affichage des rôles existants
     * - Ajout de rôles
     * Une notification est envoyée à la personne concerné·e lors de l'ajout d'un rôle
     * @param  Request $request la requête HTTP
     * @param  Personne $personne personne dont on souhaite gérer les rôles
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  RolesPersonneHelper $rolesPersonneHelper Helper pour afficher les rôles métier
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi de notifications
     * @throws Exception Lors de l'ajout d'un rôle, si la personne concernée à déjà ce rôle
     * @throws Exception Si le rôle à ajouter concerne une structure (*ex: Organisation de séjour*), et que la structure n'est pas renseignée
     * @throws Exception Si le rôle à ajouter n'existe pas
     * @return Response 
     * 
     */
    #[Route('/admin/roles/{id}', name: 'admin_roles_personne')]
    public function rolesPersonne(Request $request, Personne $personne, EntityManagerInterface $manager, SejourRepository $sejourRepository, RolesPersonneHelper $rolesPersonneHelper, NotificationHelper $notificationHelper, FormFactoryInterface $formFactory): Response
    {
        $roles_metier_personne = $rolesPersonneHelper->rolesPersonne($personne);


        $formAjoutRole = $formFactory->createNamedBuilder("formAjoutRole",  AdminAjoutRolePersonneType::class)->getForm();
        $formAjoutRole->handleRequest($request);
        if ($formAjoutRole->isSubmitted() && $formAjoutRole->isValid()) {
            $role = $formAjoutRole->get('role')->getData();
            $touteLaStructure = $formAjoutRole->get('all')->getData();
            $sejour = $formAjoutRole->get('sejour')->getData();
            $structure = $formAjoutRole->get('structure')->getData();
            try {
                $role_message_flash = "";
                switch ($role) {
                    case Personne::ROLE_VALIDATION_INTERNATIONALE:
                        if ($personne->hasRole(Personne::ROLE_VALIDATION_INTERNATIONALE)) {
                            throw new Exception("La personne a déjà le rôle de " . Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE);
                        } else {
                            $personne->addRole(Personne::ROLE_VALIDATION_INTERNATIONALE);
                            $role_message_flash = Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE;
                        }
                        break;
                    case Personne::ROLE_VALIDATION_NATIONALE:
                        if ($personne->hasRole(Personne::ROLE_VALIDATION_NATIONALE)) {
                            throw new Exception("La personne a déjà le rôle de " . Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE);
                        } else {
                            $personne->addRole(Personne::ROLE_VALIDATION_NATIONALE);
                            $role_message_flash = Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE;
                        }
                        break;
                    case Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR:
                        if (!$structure) {
                            throw new Exception("La structure n'est pas renseignée, impossible d'ajouter le rôle de " . $role);
                        }
                        if (in_array($structure, $personne->getStructuresValidation()->toArray())) {
                            throw new Exception("La personne a déjà le rôle de " . $role . " pour la structure " . $structure->getNom());
                        } else {
                            $personne->addStructuresValidation($structure);
                            $role_message_flash = $role . " (" . $structure->getNom() . ")";
                        }
                        break;
                    case Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR:
                        if (!$structure) {
                            throw new Exception("La structure n'est pas renseignée, impossible d'ajouter le rôle de " . $role);
                        }
                        if (in_array($structure, $personne->getStructuresOrganisation()->toArray())) {
                            throw new Exception("La personne a déjà le rôle de " . $role . " pour la structure " . $structure->getNom());
                        } else {
                            $personne->addStructuresOrganisation($structure);
                            $role_message_flash = $role . " (" . $structure->getNom() . ")";
                        }
                        break;
                    case Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR:
                        if (!$structure) {
                            throw new Exception("La structure n'est pas renseignée, impossible d'ajouter le rôle de " . $role);
                        }
                        if (!$sejour && !$touteLaStructure) {
                            throw new Exception("Le séjour n'est pas renseigné, impossible d'ajouter le rôle de " . $role);
                        }
                        if ($touteLaStructure) {
                            if (in_array($structure, $personne->getStructuresSuivi()->toArray())) {
                                throw new Exception("La personne a déjà le rôle de " . $role . " pour la structure " . $structure->getNom());
                            } else {
                                $personne->addStructuresSuivi($structure);
                                $role_message_flash = $role . " (" . $structure->getNom() . " - tous les sejours)";
                            }
                        } else {
                            if (in_array($structure, $personne->getSejoursSuivis()->toArray())) {
                                throw new Exception("La personne a déjà le rôle de " . $role . " pour le séjour " . $sejour->getIntitule());
                            } else {
                                $personne->addSejoursSuivi($sejour);
                                $role_message_flash = $role . " (" . $structure->getNom() . " - sejour " . $sejour->getIntitule() . ")";
                            }
                        }
                        break;
                    case Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR:
                        if (!$structure) {
                            throw new Exception("La structure n'est pas renseignée, impossible d'ajouter le rôle de " . $role);
                        }
                        if (!$sejour && !$touteLaStructure) {
                            throw new Exception("Le séjour n'est pas renseigné, impossible d'ajouter le rôle de " . $role);
                        }
                        if ($touteLaStructure) {
                            $sejours_validation = $sejourRepository->findSejoursOrganisesParStructure($structure);
                            foreach ($sejours_validation as $sejour) {
                                if (in_array($sejour, $personne->getSejoursValidation()->toArray())) {
                                    throw new Exception("La personne a déjà les rôles de " . $role . " pour le séjour " . $sejour->getIntitule());
                                } else {
                                    $sejour->addValidateur($personne);
                                    $manager->persist($sejour);
                                    $role_message_flash = $role . " (" . $structure->getNom() . " - tous les sejours)";
                                }
                            }
                        } else {
                            if (in_array($structure, $personne->getSejoursValidation()->toArray())) {
                                throw new Exception("La personne a déjà le rôle de " . $role . " pour le séjour " . $sejour->getIntitule());
                            } else {
                                $personne->addSejoursValidation($sejour);
                                $role_message_flash = $role . " (" . $structure->getNom() . " - sejour " . $sejour->getIntitule() . ")";
                            }
                        }
                        break;
                    default:
                        throw new Exception("Rôle inconnu: " . $role);
                        break;
                }
                $manager->persist($personne);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_062bis, [
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_ROLE => $role_message_flash,
                ]);
                $this->addFlash('success', "Rôle de " . $role_message_flash . " ajouté pour cette personne.");
            } catch (Exception $e) {
                $this->addFlash('danger', "Erreur: " . $e->getMessage());
            }
            return $this->redirectToRoute('admin_roles_personne', ['id' => $personne->getId()]);
        }

        return $this->render('administration/roles_personne.html.twig', [
            'roles_metier_personne' => $roles_metier_personne,
            'personne' => $personne,
            'formAjoutRole' => $formAjoutRole->createView()
        ]);
    }

    /**
     * Supprime un rôle spécifique d'une personne.
     * Le rôle à supprimer peut être un rôle générique, ou spécifique (concernant un séjour ou une structure).
     * Dans tous les cas (échec ou succès), un message flash est affiché.
     * Si la suppression est un success, une notification est envoyée à l'adéhrent·e concerné·e
     *
     * @param  string $role Le rôle à supprimer (*ex: 'Suivi de séjour', 'Organisation de séjour'…*)
     * @param  Personne $personne la personne dont il faut supprimer le rôle
     * @param  Structure|null $structure la structure concernée par le rôle à supprimer s'il s'agit d'un rôle spécifique
     * @param  Sejour|null $sejour le séjour concerné par le rôle à supprimer s'il s'agit d'un rôle spécifique
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi de notifications
     * @return void
     */
    #[Route('/admin/supprimer/role/{personne}/{role}', name: 'admin_supprimer_role_personne_global')]
    #[Route('/admin/supprimer/role/{personne}/structure/{structure}/{role}', name: 'admin_supprimer_role_personne_structure')]
    #[Route('/admin/supprimer/role/{personne}/sejour/{sejour}/{role}', name: 'admin_supprimer_role_personne_sejour')]
    public function suppressionRolePersonne(string $role, Personne $personne, ?Structure $structure, ?Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        try {
            $role_message_flash = $role;
            $message_warning = false;
            if ($structure != null) {
                switch ($role) {
                    case Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR:
                        $structure->removeOrganisateur($personne);
                        $message_warning = true;
                        break;
                    case Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR:
                        $structure->removeValidateur($personne);
                        $message_warning = true;
                        break;
                    case Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR:
                        $structure->removeSuiveur($personne);
                        break;
                }
                $role_message_flash = $role . " pour la structure " . $structure->getNom() . " (" . $structure->getType() . ")";
                $manager->persist($personne);
            } else if ($sejour != null) {
                switch ($role) {
                    case Personne::LABEL_ROLE_METIER_DIRECTION_SEJOUR:
                        throw new Exception('Ce rôle ne peut pas être supprimé par l\'administrat.eur.rice');
                        break;
                    case Personne::LABEL_ROLE_METIER_DIRECTION_ADJOINTE_SEJOUR:
                        throw new Exception('Ce rôle ne peut pas être supprimé par l\'administrat.eur.rice');
                        break;
                    case Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR:
                        $sejour->removeSuiveur($personne);
                        break;
                    case Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR:
                        $sejour->removeValidateur($personne);
                        break;
                    case Personne::LABEL_ROLE_METIER_PARTICIPATION_SEJOUR:
                        throw new Exception('Ce rôle ne peut pas être supprimé par l\'administrat.eur.rice');
                        break;
                        $manager->persist($sejour);
                        $role_message_flash = $role . " pour le séjour " . $sejour->getIntitule();
                }
            } else {
                switch ($role) {
                    case Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE:
                        $personne->removeRole(Personne::ROLE_VALIDATION_INTERNATIONALE);
                        break;
                    case Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE:
                        $personne->removeRole(Personne::ROLE_VALIDATION_NATIONALE);
                        break;
                    case Personne::LABEL_ROLE_METIER_SUIVI_GLOBAL:
                        $personne->removeRole(Personne::ROLE_SUIVI_GLOBAL);
                        $message_warning = true;
                        break;
                }

                $role_message_flash = $role;
                $manager->persist($personne);
            }

            $manager->flush();
            if ($message_warning) {
                $this->addFlash('warning', "Rôle de " . $role_message_flash . " supprimé pour cette personne, cependant ce rôle est un rôle ajouté lors de la synchronisation quotidienne avec Jéito, et ce rôle pourrait être automatiquement rajouté à cette personne");
            } else {
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_062, [
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_ROLE => $role_message_flash,
                ]);
                $this->addFlash('success', "Rôle de " . $role_message_flash . " supprimé pour cette personne.");
            }
        } catch (Exception $e) {
            $this->addFlash('danger', "Erreur lors de la suppression du rôle de " . $role_message_flash . " pour cette personne: " . $e->getMessage());
        }
        return $this->redirectToRoute('admin_roles_personne', ['id' => $personne->getId()]);
    }
}
