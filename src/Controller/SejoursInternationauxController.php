<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;
use App\Entity\Sejour;
use App\Form\AjoutRoleSuiviSejourInternationalType;
use App\Form\AjoutRoleVisiteSejourInternationalType;
use App\Repository\SejourRepository;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour la gestion globale des séjours internationaux
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class SejoursInternationauxController extends AbstractController
{
    /**
     * Route pour gérer globalement les séjour internationaux:
     * 
     * - Affichage des séjour internationaux en cours et cloturés
     * - Affichage du suivi des séjours internationaux
     * - Affichage de l'état des visites des séjours internationaux
     * - Ajout et suppression du rôle de suivi (lorsqu'un suivi est ajouté, une notification est envoyée aux personnes concernées)
     * - Ajout et suppression du rôle de visiteur·se (lorsqu'une visite est ajoutée, une notification est envoyée aux personnes concernées)
     *
     * @param  Request $request requete HTTP
     * @param  EntityManagerInterface $manager
     * @param  SejourRepository $sejourRepository pour accéder aux séjours internationaux
     * @param  NotificationHelper $notificationHelper helper pour gérer l'envoi de notifications
     * @return Response
     * @uses AjoutRoleSuiviSejourInternationalType le formulaire pour ajouter le suivi à une personne sur un séjour international
     * @uses AjoutRoleVisiteSejourInternationalType le formulaire pour ajouter le rôle de visit·eur·euse sur un séjour international
     */

    #[IsGranted("ROLE_VALIDATION_INTERNATIONALE")]
    #[Route('/sejours/internationaux', name: 'gestion_sejours_internationaux')]
    public function gestionStructure(Request $request, EntityManagerInterface $manager, SejourRepository $sejourRepository, NotificationHelper $notificationHelper): Response
    {
        $form_ajout_suivi_international = $this->createForm(AjoutRoleSuiviSejourInternationalType::class);
        $form_ajout_suivi_international->handleRequest($request);

        if ($form_ajout_suivi_international->isSubmitted() && $form_ajout_suivi_international->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_suivi_international->get('personne')->getData();
            try {
                /**  @var Sejour $sejour */
                $sejour = $form_ajout_suivi_international->get('sejour')->getData();

                if (in_array($sejour, $personne->getSejoursSuivis()->toArray())) {
                    throw new Exception("La personne a déjà le rôle de suivi du séjour international \"" . $sejour->getIntitule() . "\"");
                }
                $sejour->addSuiveur($personne);
                $manager->persist($sejour);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_053, [
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                ]);
                $this->addFlash('success_suivi', "Rôle de suivi du séjour international \"" . $sejour->getIntitule() . "\" ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            } catch (Exception $e) {
                $this->addFlash('danger_suivi', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
            }
        }

        //---------------------------------------------------------
        //Gestion formulaire attribution visites
        $form_ajout_visiteur = $this->createForm(AjoutRoleVisiteSejourInternationalType::class);
        $form_ajout_visiteur->handleRequest($request);
        if ($form_ajout_visiteur->isSubmitted() && $form_ajout_visiteur->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_visiteur->get('personne')->getData();

            // Ajout du rôle de visite pour un séjour spécifique
            try {
                /**  @var Sejour $sejour */
                $sejour = $form_ajout_visiteur->get('sejour')->getData();

                /** @var NominationVisiteSejour $nominations_visites */
                foreach ($sejour->getNominationsVisiteSejour() as $nominations_visites) {
                    if ($nominations_visites->getVisiteur() == $personne) {
                        throw new Exception("La personne a déjà le rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ")");
                    }
                }

                $nouvelle_nomination_visite = new NominationVisiteSejour();
                $nouvelle_nomination_visite->setSejour($sejour);
                $nouvelle_nomination_visite->setVisiteur($personne);
                $nouvelle_nomination_visite->setNommePar($this->getUser());
                $manager->persist($nouvelle_nomination_visite);
                $manager->persist($sejour);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_082, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_NOMINATION_VISITE => $nouvelle_nomination_visite
                ]);
                $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                return $this->redirectToRoute('gestion_sejours_internationaux');
            } catch (Exception $e) {
                $this->addFlash('danger_visite', "Erreur lors de l'ajout de rôle de visite : " . $e->getMessage());
            }
        }


        //Liste des séjours

        $sejours_internationaux = $sejourRepository->findSejoursInternationaux();
        $archives_sejours_internationaux = $sejourRepository->findArchivesSejoursInternationaux();
        $sejours_visitables = $sejourRepository->findSejoursInternationauxVisitables();

        return $this->render('sejours_internationaux/gestion_sejours_internationaux.html.twig', [
            'sejours_internationaux' => $sejours_internationaux,
            'archives_sejours_internationaux' => $archives_sejours_internationaux,
            'form_ajout_suivi_international' => $form_ajout_suivi_international->createView(),
            'sejours_visitables' => $sejours_visitables,
            'form_ajout_visiteur' => $form_ajout_visiteur->createView()
        ]);
    }

    /**
     * Permet de supprimer le rôle de suivi d'un séjour international pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[IsGranted("ROLE_VALIDATION_INTERNATIONALE")]
    #[Route('/sejours/internationaux/suivi/{sejour}/supprimer/{personne}', name: 'supprimer_suivi_sejour_international')]
    public function suppressionRoleSuiviSejour(Personne $personne, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        try {
            $sejour->removeSuiveur($personne);
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_suivi', "Rôle de suivi du séjour international \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_054, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi', "Erreur lors de la suppression du rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_internationaux');
    }

    /**
     * Permet de supprimer le rôle de visite d'un séjour pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de visite
     * @param  Personne $personne la personne à qui enlever la visite
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[IsGranted("ROLE_VALIDATION_INTERNATIONALE")]
    #[Route('/sejours/internationaux/role/visite/supprimer/{nominationVisiteSejour}', name: 'supprimer_role_visite_sejour_international')]
    public function suppressionRoleVisiteSejour(NominationVisiteSejour $nominationVisiteSejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        try {

            $sejour = $nominationVisiteSejour->getSejour();
            $personne = $nominationVisiteSejour->getVisiteur();
            $manager->remove($nominationVisiteSejour);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_083, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
            ]);
            $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" supprimé pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_visite', "Erreur lors de la suppression du rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_internationaux');
    }
}
