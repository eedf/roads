<?php

namespace App\Controller;

use App\Repository\SejourRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour accéder à la base des séjours
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class BaseSejoursController extends AbstractController
{
    /**
     * Route pour afficher la liste de séjours cloturés
     *
     * @param  SejourRepository $sejourRepository pour accéder à la liste des séjours
     * @return Response
     */
    #[Route('/base/sejours', name: 'base_sejours')]
    public function index(SejourRepository $sejourRepository): Response
    {

        $sejoursClotures = $sejourRepository->findTousSejoursClotures();

        return $this->render('base_sejours/index.html.twig', [
            'sejoursClotures' => $sejoursClotures,
        ]);
    }
}
