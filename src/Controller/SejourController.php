<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\Commentaire;
use App\Entity\DocumentRessource;
use App\Entity\LieuSejour;
use App\Entity\NominationVisiteSejour;
use App\Entity\ParticipationSejourUnite;
use App\Entity\RetourSejour;
use App\Entity\RoleEncadrantSejour;
use App\Entity\Sejour;
use App\Entity\Structure;
use App\Entity\StructureRegroupementSejour;
use App\Entity\Equipe;
use App\Entity\UtilisationLieuSejour;
use App\Entity\ValidationInternationaleSejour;
use App\Entity\ValidationSejour;
use App\Entity\VisiteSejour;
use App\Form\AspectAlimentaireType;
use App\Form\AspectBudgetType;
use App\Form\AspectCommunicationType;
use App\Form\AspectEffectifsBesoinsSpecifiquesType;
use App\Form\AspectsLogistiquesClotureSejourType;
use App\Form\AspectsTransportsSejourType;
use App\Form\CommentaireType;
use App\Form\DataTransformer\NumAdhToPersonneTransformer;
use App\Form\DatesLieuSejourDatesType;
use App\Form\DatesLieuSejourLieuType;
use App\Form\DemarchePedaSejourColoApprenanteType;
use App\Form\DemarchePedaSejourProjetInternationalType;
use App\Form\DemarchePedaSejourProjetPedaType;
use App\Form\DemarchePedaSejourType;
use App\Form\DirectionSejourType;
use App\Form\EffectifStructureRegroupementNonEEDFType;
use App\Form\EncadrantSejourType;
use App\Form\InformationsGeneralesSejourType;
use App\Form\ModalitesSejourType;
use App\Form\ParticipationParticipantsSejourUniteType;
use App\Form\PedaClotureSejourType;
use App\Form\QualificationDirectionSejourType;
use App\Form\RetoursSejourType;
use App\Form\SejourDeclarationIntentionType;
use App\Form\StructureRegroupementConstructionSejourType;
use App\Form\UnitesParticipantesSejourType;
use App\Form\ValidationInternationaleSejourType;
use App\Form\ValidationSejourType;
use App\Form\VisiteSejourType;
use App\Repository\PersonneRepository;
use App\Repository\DocumentRessourceRepository;
use App\Repository\SejourRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\SejourVoter;
use App\Security\Voter\StructureVoter;
use App\Service\ExportDocumentHelper;
use App\Service\NotificationHelper;
use App\Service\ResumeEffectifsHelper;
use App\Utils\NotificationConstants;
use App\Utils\PanelActifSejour;
use App\Utils\ResumeEffectifsSejour;
use ArrayObject;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Workflow\Exception\LogicException as ExceptionLogicException;
use Symfony\Component\Workflow\WorkflowInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Controller pour la gestion des séjours
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class SejourController extends AbstractController
{
    /**
     * Gestionnaire d'état symfony
     *
     * @var WorkflowInterface
     */
    public $sejourWorkflow;

    /**
     * constructeur
     *
     * @param  WorkflowInterface $sejourWorkflow
     * @return void
     */
    public function __construct(WorkflowInterface $sejourWorkflow)
    {
        $this->sejourWorkflow = $sejourWorkflow;
    }

    /**
     * Permet d'annuler un séjour, Le séjour est alors supprimé définitivement de la Base de données
     * Si le directeur était renseigné, une notification est alors envoyée.
     * Un message flash est affiché
     * 
     *
     * @param  Sejour $sejour le séjour à annuler
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi de notifications
     * @return Response Si succès, retour vers la page de la structure organisatrice, Sinon retour vers la page de détail du séjour
     */
    #[Route('/sejour/annulation/{id}', name: 'sejour_annulation')]
    public function annulationSejour(Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_ORGANISER, $sejour);
        try {
            $manager->remove($sejour);
            $manager->flush();
            $this->addFlash('success_infos_structure', "Le séjour a été annulé");
            //On envoie la notification uniquement si un directeur a été nommé
            if ($sejour->getInfosDeclaration()) {
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_030, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
                ]);
            }

            return $this->redirectToRoute('gestion_structure', ['id' => $sejour->getStructureOrganisatrice()->getId()]);
        } catch (Exception $e) {
            $this->addFlash('danger_sejour', "Erreur lors de l'annulation du séjour: " . $e->getMessage());
            return $this->redirectToRoute('detail_sejour', ['id' => $sejour->getId()]);
        }
    }

    /**
     * Permet de passer le séjour à l'état suspendu. La date de modification du séjour est placée à la date courante. Une notification est envoyée aux intéressé·e·s et un message flash est affiché
     *
     * @param  Sejour $sejour le séjour à suspendre
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper hlper pour l'envoi de notifications
     * @return Response retrour vers la page de détail du séjour
     */
    #[Route('/sejour/suspension/{id}', name: 'sejour_suspension')]
    public function suspension(Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_ORGANISER, $sejour);

        try {
            $statusSejour = $this->sejourWorkflow->getMarking($sejour);
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_CONSTRUCTION_PROJET)) {
                $this->sejourWorkflow->apply($sejour, 'suspension_sejour_depuis_construction_sejour');
            } else if ($statusSejour->has(Sejour::STATUT_SEJOUR_ATTENTE_VALIDATION)) {
                $this->sejourWorkflow->apply($sejour, 'suspension_sejour_depuis_attente_validation');
            } else if ($statusSejour->has(Sejour::STATUT_SEJOUR_VALIDE)) {
                $this->sejourWorkflow->apply($sejour, 'suspension_sejour_depuis_sejour_valide');
            } else {
                throw new Exception("Le séjour ne peut pas être suspendu depuis cet état");
            }
            $sejour->setDateModification(new DateTime());
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_028, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
            ]);
            $this->addFlash('success_suspension', "Le séjour a été suspendu");
        } catch (Exception $e) {
            $this->addFlash('danger_suspension', "Erreur lors de la suspension du séjour: " . $e->getMessage());
        }
        return $this->redirectToRoute('detail_sejour', ['id' => $sejour->getId()]);
    }


    /**
     * Permet d'envoyer pour validation un séjour (changement d'état, mise à jour de la date de modification et d'envoi pour validation avec la date courante).
     * Si des données de validation sont présentes, on les positionne à null
     * Un message flash est affiché, et une notification est envoyée aux personnes concernées
     * @param  Sejour $sejour le séjour à envoyer pour validation
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper Helper pour l'envoi de notifications
     * @param  ValidatorInterface $validatorInterface
     * @return Response retour à la page de détail du séjour
     * @todo désactivation temporaire des critères d'envoi pour validation
     */
    #[Route('/sejour/envoi/validation/{id}', name: 'sejour_envoi_validation')]
    public function envoiValidation(Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper, ValidatorInterface $validatorInterface): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_ENVOI_VALIDATION, $sejour);
        try {
            /* @todo désactivation temporaire des critères d'envoi pour validation */
            $errors = [];
            //$errors = $validatorInterface->validate($sejour, null, ['Default','envoi_pour_validation']);
            if (0 !== count($errors)) {
                $message_erreur = "<p>
                Pour pouvoir envoyer votre projet de séjour en validation, veuillez corriger les aspects suivants:
                <ul>";
                // there are errors, now you can show them
                foreach ($errors as $violation) {
                    $message_erreur .= '<li>';
                    $message_erreur .= $violation->getMessage();
                    $message_erreur .= '</li>';
                }
                $message_erreur .= '</ul></p>';
                $this->addFlash('danger_envoi_validation', $message_erreur);
            } else {

                //Si le séjour a été invalidé précédemment, les avis défavorables sont passé à null
                if ($sejour->getValidationSejour() && $sejour->getValidationSejour()->getEstValide() == false) {
                    $sejour->getValidationSejour()->setEstValide(null);
                }
                if ($sejour->getValidationInternationaleSejour() && $sejour->getValidationInternationaleSejour()->getEstValide() == false) {
                    $sejour->getValidationInternationaleSejour()->setEstValide(null);
                }

                $this->sejourWorkflow->apply($sejour, 'envoi_pour_validation');
                $sejour->setDateEnvoiValidation(new DateTime());
                $sejour->setDateModification(new DateTime());
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_020, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                ]);
                $this->addFlash('success_envoi_validation', "Le séjour a été envoyé pour validation");
            }
        } catch (Exception $e) {
            $this->addFlash('danger_envoi_validation', "Erreur lors de l'envoi pour validation du séjour: " . $e->getMessage());
        }
        return $this->redirectToRoute('detail_sejour', ['id' => $sejour->getId()]);
    }

    /**
     * Permet de cloturer le séjour. 
     * Vérifie que les conditions de clôture sont réunies.
     * Si ce n'est pas le cas, un message flash est affiché et le séjour n'est pas clôturé
     * Si c'est le cas le séjour est clôturé (passage de l'état à cloturé et changement de la date de modification du séjour à la date du jour), un message flash est affiché, et une notification est envoyée aux personnes concernées
     *
     * @param  Sejour $sejour le séjour à clôturer
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper Helper pour l'envoi de notifications
     * @param  ValidatorInterface $validatorInterface pour valider les conditions de clôture
     * @return Response retour vers la page de détail du séjour
     */
    #[Route('/sejour/cloture/{id}', name: 'sejour_cloture')]
    public function clotureSejour(Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper, ValidatorInterface $validatorInterface): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_CLOTURE, $sejour);

        try {
            $errors = [];
            $errors = $validatorInterface->validate($sejour, null, ['Default', 'cloture_sejour']);
            if (0 !== count($errors)) {
                $message_erreur = "<p>
                Pour pouvoir cloturer votre séjour, veuillez corriger les aspects suivants:
                <ul>";
                // there are errors, now you can show them
                foreach ($errors as $violation) {
                    $message_erreur .= '<li>';
                    $message_erreur .= $violation->getMessage();
                    $message_erreur .= '</li>';
                }
                $message_erreur .= '</ul></p>';
                $this->addFlash('danger_envoi_cloture', $message_erreur);
            } else {
                $this->sejourWorkflow->apply($sejour, 'cloture_sejour');
                $sejour->setDateModification(new DateTime());
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_027, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                ]);
                $this->addFlash('success_envoi_cloture', "Le séjour a été cloturé");
            }
        } catch (Exception $e) {
            $this->addFlash('danger_envoi_cloture', "Erreur lors de la clôture du séjour: " . $e->getMessage());
        }
        return $this->redirectToRoute('detail_sejour', ['id' => $sejour->getId()]);
    }

    /**
     * Permet de réactiver un séjour suspendu. 
     * Le séjour repasse alors dans l'état en construction la date de modification est placée à la date courante, une notification est envoyée aux personnes concernées, et un message flash est affiché
     *
     * @param  Sejour $sejour le séjour à réactiver
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper hlper pour l'envoi de notifications
     * @return Response retour vers la page de détail du séjour
     */
    #[Route('/sejour/reactivation/{id}', name: 'sejour_reactivation')]
    public function reactivation(Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_ORGANISER, $sejour);

        try {
            $this->sejourWorkflow->apply($sejour, 'reprise_sejour');
            $sejour->setDateModification(new DateTime());
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_029, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
            ]);
            $this->addFlash('success_reactivation', "Le séjour a été réactivé");
        } catch (Exception $e) {
            $this->addFlash('danger_reactivation', "Erreur lors de la récativation du séjour: " . $e->getMessage());
        }
        return $this->redirectToRoute('detail_sejour', ['id' => $sejour->getId()]);
    }

    /**
     * Permet de gérer les formulaires de déclaration d'intention d'un séjour.
     * Dans le cas d'un enregistrement, les critères de validation non remplis sont affichés, mais non bloquants. Lors d'un premier enregistrement, une notification est envoyée
     * Dans le cas de la publication, les critères de validation non remplis sont bloquants.
     * Si tous les critères sont remplis et que la publication est demandée:
     * - les données de déclaration sont sérialisées et stockées en BDD,
     * - le séjour passe dans l'état 'en construction'
     * - une notification est envoyée aux personnnes intéressées
     * - si le séjour commence dans les 45 jours, une notification de rappel de la décla TAM est envoyée
     * - si le séjour commence dans les 8 jours, une notification de rappel de vérification de la décla TAM est envoyée
     * - on renvoie vers la page de construction de séjour
     * 
     *
     * @param  Request $request la requête HTTP
     * @param  Sejour|null $sejour le séjour s'il existe, null sinon
     * @param  Structure|null $structure la structure si le séjour n'existe pas, null sinon
     * @param  EntityManagerInterface $manager
     * @param  ValidatorInterface $validatorInterface Utilitaire pour la gestion des critères de validation
     * @param  SerializerInterface $serializer utilitaire de sérialisation
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @param  DocumentRessourceRepository $documentRessourceRepository pour aller chercher les documents ressources nécessaires
     * @return Response
     * @uses SejourDeclarationIntentionType formulaire de déclaration d'intention du séjour
     */
    #[Route('/sejour/declaration/intention/{id}', name: 'sejour_declaration_intention')]
    #[Route('/sejour/creation/{structure}', name: 'creation_sejour')]
    public function declarationIntention(Request $request, ?Sejour $sejour, ?Structure $structure, EntityManagerInterface $manager, SerializerInterface  $serializer, NotificationHelper $notificationHelper, DocumentRessourceRepository $documentRessourceRepository, ValidatorInterface $validatorInterface): Response
    {
        if (!$this->isGranted(SejourVoter::SEJOUR_ORGANISER, $sejour) && !$this->isGranted(StructureVoter::STRUCTURE_ORGANISER, $structure)) {
            throw $this->createAccessDeniedException('not allowed');
        }
        $nouveau_sejour = false;
        $error_validation = false;

        //Récupération des documentsRessource à afficher:
        $docTypoSejour = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_TYPO_SEJOUR]);
        $docCahierDesChargesColosApprenantes = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CAHIER_DES_CHARGES_COLOS_APPRENANTES]);
        $docCampsDeBase = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_DESC_CAMPS_BASE]);
        $docCampsAccompagnes = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_DESC_CAMPS_ACCOMPAGNE]);
        $docConventionLieu = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CONVENTION_LIEU]);
        $docEquivalenceAnimationDirection = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_EQUIVALENCE_BAFA_BAFD]);
        $docEquivalencePSC1 = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_EQUIVALENCE_PSC1]);
        $docFormulaireConsentement = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_FORMULAIRE_CONSENTEMENT]);

        //Création du séjour s'il n'existe pas
        if (!$sejour) {
            $sejour = new Sejour();
            $nouveau_sejour = true;
            $sejour->setStructureOrganisatrice($structure);
            $sejour->setintitule("Nouveau séjour");

            $lieu = new UtilisationLieuSejour();
            $lieu->setEstDefini(true);
            $sejour->addUtilisationsLieuSejour($lieu);

            $sejour->setDateDebut(new DateTime('+3 months'));
            $sejour->setDateFin(new DateTime('+4 months'));
            $sejour->setDateModification(new DateTime());
            try {
                $this->sejourWorkflow->apply($sejour, 'vers_declaration_intention');
            } catch (LogicException $exception) {
            }
        }

        $form_declaration_intention = $this->createForm(SejourDeclarationIntentionType::class, $sejour, [
            'sejour' => $sejour,
            'validation_groups' => ['Default', 'publication_declaration_intention']
        ]);
        $form_declaration_intention->handleRequest($request);
        if ($form_declaration_intention->isSubmitted()) {
            /* -------------------------------------------------------------------- */
            //Gestion des structuresRegroupees
            $listeStructuresRegroupees = $form_declaration_intention->get('structuresRegroupees')->getData();
            // On supprime les structures regroupées
            /** @var StructureRegroupementSejour $ancienne_structure_regroupee */
            foreach ($sejour->getStructuresRegroupees() as $ancienne_structure_regroupee) {
                if (!$listeStructuresRegroupees->contains($ancienne_structure_regroupee)) {
                    $sejour->removeStructuresRegroupee($ancienne_structure_regroupee);
                }
            }
            /** @var StructureRegroupementSejour $nouvelle_structure_regroupee */
            foreach ($listeStructuresRegroupees as $nouvelle_structure_regroupee) {
                $sejour->addStructuresRegroupee($nouvelle_structure_regroupee);
            }


            $sejour->setDateModification(new DateTime());

            //On supprime les lieu pour s'ils ne sont pas définis
            foreach ($sejour->getUtilisationsLieuSejour() as $utilisationLieu) {
                if (!$utilisationLieu->getEstDefini()) {
                    $manager->remove($utilisationLieu->getLieu());
                    $utilisationLieu->setLieu(null);
                    $manager->persist($utilisationLieu);
                }
            }

            /** @var Form $form_declaration_intention */
            if ('publish' === $form_declaration_intention->getClickedButton()->getName()) {
                if (!$form_declaration_intention->isValid()) {
                    $errors = [];
                    $errors = $validatorInterface->validate($sejour, null, ['Default', 'publication_declaration_intention']);
                    if (0 !== count($errors)) {
                        $message_erreur = "<p>
                        Pour pouvoir publier votre déclaration d'intention, veuillez corriger les aspects suivants:
                        <ul>";
                        // there are errors, now you can show them
                        foreach ($errors as $violation) {
                            $message_erreur .= '<li>';
                            $message_erreur .= $violation->getMessage();
                            $message_erreur .= '</li>';
                        }
                        $message_erreur .= '</ul></p>';
                        $this->addFlash('danger_sejour', $message_erreur);
                    }
                } else {
                    $manager->persist($sejour);
                    $manager->flush();
                    $jsonContent = $serializer->serialize(
                        $sejour,
                        'json',
                        [
                            'groups' => 'save_declaration_intention',
                            AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                            AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                            AbstractObjectNormalizer::CIRCULAR_REFERENCE_LIMIT => 2,
                            AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                                return null;
                            },
                        ]
                    );

                    try {
                        $dateCourante = new DateTime();
                        $sejour->setInfosDeclaration($jsonContent);
                        $this->sejourWorkflow->apply($sejour, 'publication_declaration_intention');
                        $sejour->setDateDepotDeclarationIntention($dateCourante);


                        /**  @var StructureRegroupementSejour $structure_regroupee */
                        foreach ($sejour->getStructuresRegroupees() as $structure_regroupee) {
                            if ($structure_regroupee->getEstEEDF()) {
                                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_007, [
                                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                    NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_regroupee->getStructureEEDF()
                                ]);
                            }
                        }
                        $manager->persist($sejour);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_005, [NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour]);
                        /**  @var DateTime $dateDebutSejour */
                        $dateDebutSejour = clone $sejour->getDateDebut();
                        if ($dateDebutSejour->modify("- 44 days")->setTime(0, 0) < $dateCourante) {
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_084, [NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour]);
                        }
                        /**  @var DateTime $dateDebutSejour */
                        $dateDebutSejour = clone $sejour->getDateDebut();
                        if ($dateDebutSejour->modify("- 7 days")->setTime(0, 0) < $dateCourante) {
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_085, [NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour]);
                        }
                        return $this->redirectToRoute('sejour_construction_projet', ['id' => $sejour->getId()]);
                    } catch (ExceptionLogicException $exception) {
                        $this->addFlash('danger_sejour', "Erreur lors du changement d'état du séjour: " . $exception->getMessage());
                    }
                }
            } else {

                if (!$form_declaration_intention->isValid()) {
                    $this->addFlash('danger_sejour', "Des erreurs sont présentes dans le formulaire, veuillez corriger.");
                    $error_validation = true;
                } else {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_sejour', "Modifications enregistrées avec succès");
                    if ($nouveau_sejour) {
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_001, [NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour]);
                        return $this->redirectToRoute('sejour_declaration_intention', ['id' => $sejour->getId()]);
                    }
                }
            }
        }
        /*--------------------------------------------------------------------- */
        // Gestion des dates
        $today = new DateTime();
        if ($sejour->getDateDebut() < $today) {
            $this->addFlash('warning_dates_decla_sejour', "Attention, la date de début du séjour est antérieure à la date d'aujourd'hui.");
        }
        if (!$error_validation) {
            $this->gestionResteAvantPublicationDeclarationIntention($sejour, $validatorInterface);
        }

        return $this->render('sejour/declaration_intention.html.twig', [
            'sejour' => $sejour,
            'form_declaration_intention' => $form_declaration_intention->createView(),
            'docTypoSejour' => $docTypoSejour,
            'docCahierDesChargesColosApprenantes' => $docCahierDesChargesColosApprenantes,
            'docCampsDeBase' => $docCampsDeBase,
            'docCampsAccompagnes' => $docCampsAccompagnes,
            'docConventionLieu' => $docConventionLieu,
            'docEquivalenceAnimationDirection' => $docEquivalenceAnimationDirection,
            'docEquivalencePSC1' => $docEquivalencePSC1,
            'docFormulaireConsentement' => $docFormulaireConsentement

        ]);
    }

    /**
     * Permet de rediriger vers la bonne route de détail de séjour, en fonction du droit de l'utilisat·eur·rice, et de l'état du séjour 
     *
     * @param  Sejour $sejour le séjour concerné
     * @return Response les route correspondante
     * @throws AccessDeniedException Exception lorsque la personne n'a pas les droits nécessaires accéder aux détils du séjour
     */
    #[Route('/sejour/detail/{id}', name: 'detail_sejour')]
    public function detailSejour(Sejour $sejour): Response
    {

        $statusSejour = $this->sejourWorkflow->getMarking($sejour);


        if ($statusSejour->has(Sejour::STATUT_SEJOUR_DECLARATION_INTENTION)) {
            if ($this->isGranted(SejourVoter::SEJOUR_ORGANISER, $sejour)) {
                return $this->redirectToRoute('sejour_declaration_intention', ['id' => $sejour->getId()]);
            } else {
                throw new AccessDeniedException('Accès non autorisé');
            }
        } else if (
            ($statusSejour->has(Sejour::STATUT_SEJOUR_CONSTRUCTION_PROJET))
            || ($statusSejour->has(Sejour::STATUT_SEJOUR_ATTENTE_VALIDATION))
            || ($statusSejour->has(Sejour::STATUT_SEJOUR_VALIDE))
            || ($statusSejour->has(Sejour::STATUT_SEJOUR_EN_COURS))
        ) {

            if ($this->isGranted(SejourVoter::SEJOUR_MODIFICATION_PROJET, $sejour)) {
                return $this->redirectToRoute('sejour_construction_projet', ['id' => $sejour->getId()]);
            } else if ($this->isGranted(SejourVoter::SEJOUR_SUIVI, $sejour)) {
                return $this->redirectToRoute('sejour_construction_projet_suivi', ['id' => $sejour->getId()]);
            } else {
                throw new AccessDeniedException('Accès non autorisé');
            }
        } else if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
            if ($this->isGranted(SejourVoter::SEJOUR_CLOTURE, $sejour)) {
                return $this->redirectToRoute('sejour_construction_projet', ['id' => $sejour->getId()]);
            } else if ($this->isGranted(SejourVoter::SEJOUR_SUIVI, $sejour)) {
                return $this->redirectToRoute('sejour_construction_projet_suivi', ['id' => $sejour->getId()]);
            } else {
                throw new AccessDeniedException('Accès non autorisé');
            }
        } else if ($statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {

            if ($this->isGranted(SejourVoter::SEJOUR_SUIVI, $sejour)) {
                return $this->redirectToRoute('sejour_construction_projet_suivi', ['id' => $sejour->getId()]);
            } else {
                return $this->redirectToRoute('detail_sejour_archive', ['id' => $sejour->getId()]);
            }
        }

        if ($statusSejour->has(Sejour::STATUT_SEJOUR_SUSPENDU)) {
            if ($this->isGranted(SejourVoter::SEJOUR_SUIVI, $sejour)) {
                return $this->redirectToRoute('sejour_suspendu', ['id' => $sejour->getId()]);
            }
        }
        throw new AccessDeniedException('Accès non autorisé');
    }

    /**
     * Permet des gérer tous les formulaires de construction de séjour.
     * En fonction de l'état dans lequel se retrouve 
     * @param  Request $request la requête HTTP
     * @param  Sejour $sejour le séjour en construction
     * @param  SerializerInterface $serializer Utilitaire de sérialisation
     * @param  EntityManagerInterface $manager
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources nécessaires
     * @param  NotificationHelper $notificationHelper Helper pour l'envoi de notification
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper Helper pour le calcul des effectifs du séjour
     * @param  ValidatorInterface $validatorInterface Utilitaire pour la gestion des critères de validation
     * @return Response
     * @todo désactivation temporaire des critères d'envoi pour validation
     * @uses PanelActifSejour permet de déterminer le panel actif du séjour à afficher, en fonction des modification opérées
     * @see SejourController::gestionConstructionSejour() Pour gérer tous les formulaires de contruction de séjour
     * @see SejourController::gestionValidationClassique() Pour gérer tous les formulaires de validation de séjour
     * @see SejourController::gestionValidationInternationale() Pour gérer tous les formulaires de validation internationale de séjour
     * @see SejourController::gestionClotureSejour() Pour gérer tous les formulaires de clôture de séjour
     * @see SejourController::gestionCommentaires() Pour gérer tous les formulaires concernants les commentaires
     * @see SejourController::gestionVisites() Pour gérer tous les formulaires concernants les comptes rendus de visite de séjour
     * 
     */
    #[Route('/sejour/construction/{id}', name: 'sejour_construction_projet')]
    public function constructionProjet(Request $request, Sejour $sejour, SerializerInterface  $serializer, EntityManagerInterface $manager, DocumentRessourceRepository $documentRessourceRepository, NotificationHelper $notificationHelper, ResumeEffectifsHelper $resumeEffectifsHelper, ValidatorInterface $validatorInterface, FormFactoryInterface $formFactory): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_MODIFICATION_PROJET, $sejour);

        $sejour->setPanelActif(new PanelActifSejour());
        $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
        $parametresVue = array();


        //Si le séjour est en attente de validation, on doit valider tous les critères à l'enregistrement des formulaires
        $criteresValidationFormulaires = ['Default', 'publication_declaration_intention', 'construction_sejour'];

        $statusSejour = $this->sejourWorkflow->getMarking($sejour);
        /* désactivation temporaire des critères d'envoi pour validation */
        /* if($statusSejour->has(Sejour::STATUT_SEJOUR_ATTENTE_VALIDATION))
        {
            $criteresValidationFormulaires=['Default','envoi_pour_validation'];
        }
        */

        if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
            $criteresValidationFormulaires = ['Default', 'envoi_cloture_sejour'];
        }


        $docModeleProjetSejour = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_PROJET_SEJOUR]);



        /* désactivation temporaire des critères d'envoi pour validation */
        /*
        if ($statusSejour->has(Sejour::STATUT_SEJOUR_CONSTRUCTION_PROJET))
        {
            $this->gestionResteAvantEnvoiPourValidation($sejour,$validatorInterface);
        }
        */

        if ($sejour->isValidable()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_VALIDATION);
            // Chargement des doc ressources nécessaires:
            $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);

            $docModeleValidation = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VALIDATION_SEJOUR]);
            $docModeleValidationInternationale = $documentRessourceRepository->findOneBy(['nom' =>  DocumentRessource::DOC_MODELE_VALIDATION_INTERNATIONALE_SEJOUR]);

            $form_validation_sejour = $this->gestionValidationClassique($request, $sejour,  $notificationHelper, $manager);
            $form_validation_internationale_sejour = $this->gestionValidationInternationale($request, $sejour,  $notificationHelper, $manager);

            //VALIDATION DE SEJOUR
            $parametresVue['form_validation_sejour'] = $form_validation_sejour->createView();
            $parametresVue['form_validation_internationale_sejour'] = $form_validation_internationale_sejour->createView();
            $parametresVue['docModeleValidation'] = $docModeleValidation;
            $parametresVue['docModeleValidationInternationale'] = $docModeleValidationInternationale;
            $parametresVue['docModeleVisiteCamp'] = $docModeleVisiteCamp;
        }

        if ($sejour->isProjetModifiable()) {
            $parametresVue = array_merge(
                $parametresVue,
                $this->gestionConstructionSejour($request, $sejour, $documentRessourceRepository, $criteresValidationFormulaires, $notificationHelper, $manager, $validatorInterface, $resumeEffectifsHelper, $formFactory)
            );
        }
        if ($sejour->isValidationVisible() and $sejour->getValidationSejour() != null) {
            $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);
            $parametresVue['docModeleVisiteCamp'] = $docModeleVisiteCamp;
        }



        //Informations de déclaration d'intention
        $decla_intention_sejour = $serializer->deserialize($sejour->getInfosDeclaration(), Sejour::class, 'json');
        $parametresVue['decla_intention_sejour'] = $decla_intention_sejour;

        $parametresVue['sejour'] = $sejour;
        $parametresVue['docModeleProjetSejour'] = $docModeleProjetSejour;




        //Gestion des formulaires de cloture du séjour
        if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
            $parametresVue = array_merge(
                $parametresVue,
                $this->gestionClotureSejour($request, $sejour, $manager, $documentRessourceRepository, $resumeEffectifsHelper, $criteresValidationFormulaires, $serializer, $notificationHelper, $validatorInterface, $formFactory)
            );
            $this->gestionResteAvantCloture($sejour, $validatorInterface);
        }

        //VISITE DE SEJOUR
        if ($sejour->isVisitable()) {
            //Gestion des formulaires de visite du séjour
            $form_visite_sejour = $this->gestionVisites($request, $sejour,  $notificationHelper, $manager, $validatorInterface);
            $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);
            $docModeleCheckListDirecteur = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CHECKLIST_CLASSEUR_DIRECTEUR]);

            $parametresVue['form_visite_sejour'] = $form_visite_sejour->createView();
            $parametresVue['docModeleCheckListDirecteur'] = $docModeleCheckListDirecteur;
            $parametresVue['docModeleVisiteCamp'] = $docModeleVisiteCamp;
        }


        $parametresVue['arrayFormsCommentairesParSujet'] = $this->gestionCommentaires($request, $manager, $sejour, $notificationHelper, $formFactory);

        return $this->render('sejour/construction_sejour.html.twig', $parametresVue);
    }

    /**
     * Gestion des formulaire de construction d'un séjour
     *
     * Gestion des aspects:
     * - Informations générales
     * - Effectifs
     * - Dates et lieux de séjour
     * - Démarches pédagogiques, dont:
     *  - Projet pédagogique
     *  - Colos apprenantes (si le séjour est une colo apprenante)
     *  - Projet international (si le séjour est international ou accueille des étrangers)
     * - Aspects logistiques, dont:
     *  - Budget
     *  - Transports
     *  - Communication
     *  - Alimentaire
     * 
     *  A partir du moment où le séjour est dans le statut "En attente de validation", Une image du séjour est mémorisée avant modification, et est comparée avec les séjour modifié, et le cas échéant, une notification est envoyée aux personnes concernées s'il y a eu une modification importante
     * @param  Request $request requête HTTP
     * @param  Sejour $sejour le séjour concerné
     * @param  DocumentRessourceRepository $documentRessourceRepository permet d'accéder aux documents ressources nécessaires
     * @param  array $criteresValidationFormulaires les critères de validation des formulaires
     * @param  NotificationHelper $notificationHelper helper pour la gestion des notification
     * @param  EntityManagerInterface $manager
     * @param  ValidatorInterface $validatorInterface utilitaire de validation des données du séjour
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour l'affichage du résumé des effectifs du séjour
     * @return array liste des paramètres pour la vue twig
     * @see SejourController::gestionFormulairesInformationsGeneralesSejour() permet de gérer le formulaire des informations générales du séjour
     * @see SejourController::gestionFormulairesEffectifsSejours() permet de gérer les formulaires des effectifs du séjour
     * @see SejourController::gestionFormulairesDatesLieuxSejour() permet de gérer les formulaires des dates et des lieux de séjour
     * @uses DemarchePedaSejourProjetPedaType formulaire du projet péda
     * @uses DemarchePedaSejourColoApprenanteType formulaire des colos apprenantes
     * @uses DemarchePedaSejourProjetInternationalType formulaire de projet international
     * @uses AspectBudgetSejourType formulaire des aspects budgetaires
     * @uses AspectsTransportsSejourType formulaire des aspects transports
     * @uses AspectCommunicationSejourType formuliaire des aspects communication
     * @uses AspectAlimentaireSejourType formulaire des aspects alimentaires du séjour
     * @see SejourController::gestionModificationsImportantesSejour() gestion des modifications importantes d'un séjour
     * 
     */
    public function gestionConstructionSejour(Request $request, Sejour $sejour, DocumentRessourceRepository $documentRessourceRepository, array $criteresValidationFormulaires, NotificationHelper $notificationHelper, EntityManagerInterface $manager, ValidatorInterface $validatorInterface, ResumeEffectifsHelper $resumeEffectifsHelper, FormFactoryInterface $formFactory): array
    {

        $verifierModifImportante = false;
        //Si l'on doit vérifier s'il y a eu une modification importante, on prépare les données
        if ($sejour->isVerifierModifImportante()) {
            $verifierModifImportante = true;
            $sejour_avant_modif = clone $sejour;

            $structuresRegroupeesAvantModif = array();
            $unitesAvantModif = array();
            /** @var StructureRegroupementSejour $structureRegroupeeAvantModif */
            foreach ($sejour_avant_modif->getStructuresRegroupees() as $structureRegroupeeAvantModif) {
                $structuresRegroupeesAvantModif[$structureRegroupeeAvantModif->getId()] = $structureRegroupeeAvantModif;
                foreach ($structureRegroupeeAvantModif->getUnitesParticipantes() as $uniteParticipante) {
                    $unitesAvantModif[$uniteParticipante->getEquipe()->getId()] = $uniteParticipante->getEquipe();
                }
            }
            foreach ($sejour_avant_modif->getUnitesStructureOrganisatrice() as $uniteParticipante) {
                $unitesAvantModif[$uniteParticipante->getEquipe()->getId()] = $uniteParticipante->getEquipe();
            }
            $utilisationLieuxAvantModif = array();
            foreach ($sejour_avant_modif->getUtilisationsLieuSejour() as $utilisationLieuAvantModif) {
                $tempLieu = clone $utilisationLieuAvantModif;
                /** @var UtilisationLieuSejour $tempLieu */
                if ($utilisationLieuAvantModif->getLieu() !== null) {
                    $tempLieu->setLieu(clone ($utilisationLieuAvantModif->getLieu()));
                }

                $utilisationLieuxAvantModif[$utilisationLieuAvantModif->getId()] = $tempLieu;
            }

            $resumeEffectifsAvant = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour_avant_modif);
        }


        $parametresVue = array();
        //----------------------------------------------------------
        //Gestion Informations générales
        $result = $this->gestionFormulairesInformationsGeneralesSejour($documentRessourceRepository, $sejour, $request,  $manager, $criteresValidationFormulaires, $notificationHelper, $validatorInterface);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );
        $verifierModifImportante &= $result['enregistrementOk'];
        //----------------------------------------------------------
        //Gestion Effectifs
        $result = $this->gestionFormulairesEffectifsSejours($documentRessourceRepository, $sejour, $request, $manager, $resumeEffectifsHelper, $criteresValidationFormulaires, $formFactory);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );
        $verifierModifImportante &= $result['enregistrementOk'];

        //----------------------------------------------------------
        //Gestion Dates et lieux
        $result = $this->gestionFormulairesDatesLieuxSejour($documentRessourceRepository, $sejour, $request,  $manager, $criteresValidationFormulaires);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );
        $verifierModifImportante &= $result['enregistrementOk'];

        //--------------------------------------------------
        //Gestion Demarches Pédagogiques
        $docModeleColosApprenantes = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_SOCLE_COMMUN_COLOS_APPRENANTES]);
        $docModeleProjetPeda = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_PROJET_PEDA]);

        $form_projet_peda_sejour = $this->createForm(DemarchePedaSejourProjetPedaType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_projet_peda_sejour->handleRequest($request);
        if ($form_projet_peda_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_DEMARCHES_PEDA);
            if ($form_projet_peda_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_peda', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_peda', "Erreur lors de la modification du séjour: " . $e->getMessage());
                    $verifierModifImportante = false;
                }
            } else {
                $this->addFlash('danger_peda', "Données incorrectes, veuillez corriger");
                $verifierModifImportante = false;
            }
        }

        //Colos apprenantes
        if ($sejour->getColoApprenante()) {
            $form_colo_apprenante_sejour = $this->createForm(DemarchePedaSejourColoApprenanteType::class, $sejour, [
                'validation_groups' => $criteresValidationFormulaires
            ]);

            $form_colo_apprenante_sejour->handleRequest($request);
            if ($form_colo_apprenante_sejour->isSubmitted()) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_DEMARCHES_PEDA);
                if ($form_colo_apprenante_sejour->isValid()) {
                    $sejour->setDateModification(new DateTime());
                    try {
                        $manager->persist($sejour);
                        $manager->flush();

                        $this->addFlash('success_peda', "Modifications enregistrées avec succès");
                    } catch (Exception $e) {
                        $this->addFlash('danger_peda', "Erreur lors de la modification du séjour: " . $e->getMessage());
                        $verifierModifImportante = false;
                    }
                } else {
                    $this->addFlash('danger_peda', "Données incorrectes, veuillez corriger");
                    $verifierModifImportante = false;
                }
            }
            $parametresVue['form_colo_apprenante_sejour'] = $form_colo_apprenante_sejour->createView();
        }

        //Projet international
        if ($sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger()) {
            $form_projet_international_sejour = $this->createForm(DemarchePedaSejourProjetInternationalType::class, $sejour, [
                'validation_groups' => $criteresValidationFormulaires
            ]);
            $form_projet_international_sejour->handleRequest($request);
            if ($form_projet_international_sejour->isSubmitted()) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_DEMARCHES_PEDA);
                if ($form_projet_international_sejour->isValid()) {
                    $sejour->setDateModification(new DateTime());
                    try {
                        $manager->persist($sejour);
                        $manager->flush();

                        $this->addFlash('success_peda', "Modifications enregistrées avec succès");
                    } catch (Exception $e) {
                        $this->addFlash('danger_peda', "Erreur lors de la modification du séjour: " . $e->getMessage());
                        $verifierModifImportante = false;
                    }
                } else {
                    $this->addFlash('danger_peda', "Données incorrectes, veuillez corriger");
                    $verifierModifImportante = false;
                }
            }
            $parametresVue['form_projet_international_sejour'] = $form_projet_international_sejour->createView();
        }

        //Démarches pédagogiques
        $parametresVue['form_projet_peda_sejour'] = $form_projet_peda_sejour->createView();


        $parametresVue['docModeleProjetPeda'] = $docModeleProjetPeda;
        $parametresVue['docModeleColosApprenantes'] = $docModeleColosApprenantes;

        //------------------------------------------------------------------------------------
        //Gestion Aspect Logisitiques
        $docGrilleMenus = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_GRILLE_MENUS]);
        $docElaborationMenus = $documentRessourceRepository->findOneBy(['nom' =>  DocumentRessource::DOC_OUTIL_CONSTRUCTION_MENUS]);
        $docModeleBudget = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_BUDGET]);

        //Budget
        $form_logistique_budget_sejour = $this->createForm(AspectBudgetType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_logistique_budget_sejour->handleRequest($request);

        if ($form_logistique_budget_sejour->isSubmitted()) {

            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_LOGISTIQUE);

            if ($form_logistique_budget_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_logistique', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_logistique', "Erreur lors de la modification du séjour: " . $e->getMessage());
                    $verifierModifImportante = false;
                }
            } else {
                $this->addFlash('danger_logistique', "Données incorrectes, veuillez corriger");
                $verifierModifImportante = false;
            }
        }

        //Transports
        $form_logistique_transports_sejour = $this->createForm(AspectsTransportsSejourType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_logistique_transports_sejour->handleRequest($request);

        if ($form_logistique_transports_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_LOGISTIQUE);
            if ($form_logistique_transports_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_logistique', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_logistique', "Erreur lors de la modification du séjour: " . $e->getMessage());
                    $verifierModifImportante = false;
                }
            } else {
                $this->addFlash('danger_logistique', "Données incorrectes, veuillez corriger");
                $verifierModifImportante = false;
            }
        }

        //Communication
        $form_logistique_communication_sejour = $this->createForm(AspectCommunicationType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_logistique_communication_sejour->handleRequest($request);

        if ($form_logistique_communication_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_LOGISTIQUE);
            if ($form_logistique_communication_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_logistique', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_logistique', "Erreur lors de la modification du séjour: " . $e->getMessage());
                    $verifierModifImportante = false;
                }
            } else {
                $this->addFlash('danger_logistique', "Données incorrectes, veuillez corriger");
                $verifierModifImportante = false;
            }
        }

        //Alimentaire
        $form_logistique_alimentaire_sejour = $this->createForm(AspectAlimentaireType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_logistique_alimentaire_sejour->handleRequest($request);

        if ($form_logistique_alimentaire_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_LOGISTIQUE);
            if ($form_logistique_alimentaire_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_logistique', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_logistique', "Erreur lors de la modification du séjour: " . $e->getMessage());
                    $verifierModifImportante = false;
                }
            } else {
                $this->addFlash('danger_logistique', "Données incorrectes, veuillez corriger");
                $verifierModifImportante = false;
            }
        }

        if ($verifierModifImportante) {
            $this->gestionModificationsImportantesSejour($sejour, $sejour_avant_modif, $utilisationLieuxAvantModif, $unitesAvantModif, $structuresRegroupeesAvantModif, $resumeEffectifsHelper, $resumeEffectifsAvant, $notificationHelper, $manager);
        }


        //Logistique
        $parametresVue['form_logistique_budget_sejour'] = $form_logistique_budget_sejour->createView();
        $parametresVue['form_logistique_alimentaire_sejour'] = $form_logistique_alimentaire_sejour->createView();
        $parametresVue['form_logistique_transports_sejour'] = $form_logistique_transports_sejour->createView();
        $parametresVue['form_logistique_communication_sejour'] = $form_logistique_communication_sejour->createView();
        $parametresVue['docGrilleMenus'] = $docGrilleMenus;
        $parametresVue['docElaborationMenus'] = $docElaborationMenus;
        $parametresVue['docModeleBudget'] = $docModeleBudget;

        return $parametresVue;
    }
    /**
     * Permet de détecter les changements importants du séjour et d'envoyer des notifications le cas échéant
     *
     * On vérifie :
     * - Si les structures des regroupement ont changé
     * - Si les lieux ont changé
     * - Si les unités participantes ont changé
     * - Si le type de séjour, l'ouverture du séjour, l'aspect international, itinérant ont changé
     * - Si les dates du séjour ont changé
     * - Si le taux d'encadrement a changé 
     * - Si l'intitulé du séjour a changé
     * - Si le·la direct·eur·rice a changé
     * - Si le dispositif d'appui du camp a changé
     *  Une notification récapitulants les changements majeurs est alors envoyée aux personnes concernée
     * 
     * @param  Sejour $sejour le séjour après modification
     * @param  Sejour $sejour_avant_modif l'image du séjour avant modification
     * @param  UtilisationLieuSejour[] $utilisationLieuxAvantModif lieux du séjour avant modification
     * @param  Equipe[] $unitesAvantModif unités du séjour avant modification
     * @param  StructureRegroupementSejour[] $structuresRegroupeesAvantModif structures regroupées avant modification
     * @param  ResumeEffectifsSejour $resumeEffectifs récapitualitf des effectifs avant modification
     * @param  ResumeEffectifsSejour $resumeEffectifsAvant récapitualitf des effectifs après modification
     * @param  NotificationHelper $notificationHelper helper pour l'envoi des notifications
     * @param  EntityManagerInterface $manager
     * @return void
     */
    public function gestionModificationsImportantesSejour(Sejour $sejour, Sejour $sejour_avant_modif, array $utilisationLieuxAvantModif, array $unitesAvantModif, array $structuresRegroupeesAvantModif, ResumeEffectifsHelper $resumeEffectifsHelper, ResumeEffectifsSejour $resumeEffectifsAvant, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        $array_texte_modifs = array();
        //On vérifie si les lieux ont changé

        /** @var UtilisationLieuSejour $utilisationLieuApresModif */
        foreach ($sejour->getUtilisationsLieuSejour() as $utilisationLieuApresModif) {
            $lieuApresModif = $utilisationLieuApresModif->getLieu();
            $lieuAvantModif = $utilisationLieuxAvantModif[$utilisationLieuApresModif->getId()]->getLieu();
            if (!in_array($utilisationLieuApresModif->getId(), array_keys($utilisationLieuxAvantModif))) {
                if ($lieuApresModif) {
                    $array_texte_modifs[] = "Le séjour a ajouté un lieu supplémentaire <b>" . $lieuApresModif->getNom() . "</b> à son itinérance";
                }
            } else {

                if (
                    (($lieuAvantModif == null)  && ($lieuApresModif !== null))
                    || (($lieuAvantModif !== null)  && ($lieuApresModif == null))
                    ||
                    (
                        $lieuApresModif->getNom() != $lieuAvantModif->getNom()
                        || ($lieuApresModif->getLocalisation() && $lieuApresModif->getLocalisation()
                            && ($lieuApresModif->getLocalisation()->getAdresse() !== $lieuAvantModif->getLocalisation()->getAdresse()
                                || $lieuApresModif->getLocalisation()->getComplementAdresse() !== $lieuAvantModif->getLocalisation()->getComplementAdresse()
                                || $lieuApresModif->getLocalisation()->getCodePostalCommune() !== $lieuAvantModif->getLocalisation()->getCodePostalCommune()
                                || $lieuApresModif->getLocalisation()->getPays() !== $lieuAvantModif->getLocalisation()->getPays()
                                || $lieuApresModif->getLocalisation()->getNomCommune() !== $lieuAvantModif->getLocalisation()->getNomCommune())
                        ))
                ) {
                    $array_texte_modifs[] = "Le séjour a déclaré un changement concernant le lieu <b>" . $lieuApresModif->getNom() . "</b>";
                }
                unset($utilisationLieuxAvantModif[$utilisationLieuApresModif->getId()]);
            }
        }
        //S'il reste des structure avant modif, ça veut dire qu'il ya eu modification
        foreach ($utilisationLieuxAvantModif as $utilisationLieuAvantModif) {
            $array_texte_modifs[] = "Le séjour a supprimé un lieu <b>" . $utilisationLieuAvantModif->getLieu()->getNom() . "</b> de son itinérance";
        }
        //---------------------------------------------------
        //On vérifie si les structures des regroupement ont changé

        /** @var StructureRegroupementSejour $structureRegroupeeApresModif */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupeeApresModif) {
            if (!in_array($structureRegroupeeApresModif->getId(), array_keys($structuresRegroupeesAvantModif))) {
                if ($structureRegroupeeApresModif->getEstEEDF()) {
                    $array_texte_modifs[] = "La liste des structures participantes au séjour a évolué - Ajout de la participation de " . $structureRegroupeeApresModif->getStructureEEDF()->getNom();
                } else {
                    $array_texte_modifs[] = "La liste des structures participantes au séjour a évolué - Ajout de la participation de " . $structureRegroupeeApresModif->getNomStructureNonEEDF();
                }
            } else {
                unset($structuresRegroupeesAvantModif[$structureRegroupeeApresModif->getId()]);
            }
        }
        /** @var StructureRegroupementSejour $structureRegroupeeAvantModif */
        //S'il reste des structure avant modif, ça veut dire qu'il ya eu modification
        foreach ($structuresRegroupeesAvantModif as $structureRegroupeeAvantModif) {
            if ($structureRegroupeeAvantModif->getEstEEDF()) {
                $array_texte_modifs[] = "La liste des structures participantes au séjour a évolué - Retrait de la participation de " . $structureRegroupeeAvantModif->getStructureEEDF()->getNom();
            } else {
                $array_texte_modifs[] = "La liste des structures participantes au séjour a évolué - Retrait de la participation de " . $structureRegroupeeAvantModif->getNomStructureNonEEDF();
            }
        }

        //-------------------------------------------------
        //On vérifie si les unité participantes ont changé

        $unitesApresModif = array();
        /** @var StructureRegroupementSejour $structureRegroupeeApresModif */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupeeApresModif) {
            foreach ($structureRegroupeeApresModif->getUnitesParticipantes() as $uniteParticipante) {
                $unitesApresModif[$uniteParticipante->getEquipe()->getId()] = $uniteParticipante->getEquipe();
            }
        }
        foreach ($sejour->getUnitesStructureOrganisatrice() as $uniteParticipante) {
            $unitesApresModif[$uniteParticipante->getEquipe()->getId()] = $uniteParticipante->getEquipe();
        }
        /** @var Unite $uniteApresModif */
        foreach ($unitesApresModif as $idUniteApresModif => $uniteApresModif) {
            if (!in_array($idUniteApresModif, array_keys($unitesAvantModif))) {
                $array_texte_modifs[] = "La liste des unités participantes au séjour a évolué - Ajout de la participation de " . $uniteApresModif->getNom();
            } else {
                unset($unitesAvantModif[$idUniteApresModif]);
            }
        }
        //S'il reste des structure avant modif, ça veut dire qu'il ya eu modification
        /** @var Unite $uniteAvantModif */
        foreach ($unitesAvantModif as $uniteAvantModif) {
            $array_texte_modifs[] = "La liste des unités participantes au séjour a évolué - Retrait de la participation de " . $uniteAvantModif->getNom();
        }




        if ($sejour_avant_modif->getTypeSejour() != $sejour->getTypeSejour()) {
            $array_texte_modifs[] = "Le type de séjour a été modifié en <b>séjour " . $sejour->getTypeSejour() . "</b>";
        }
        if ($sejour_avant_modif->getSejourOuvert() != $sejour->getSejourOuvert()) {
            $valeur = "<b>non ouvert</b>";
            if ($sejour->getSejourOuvert()) {
                $valeur = "<b>ouvert</b>";
            }
            $array_texte_modifs[] = "Le type de séjour a été modifié a changé de type. Il est désormais un séjour " . $valeur;
        }
        if ($sejour_avant_modif->getSejourItinerant() != $sejour->getSejourItinerant()) {
            $valeur = "<b>non itinérant</b>";
            if ($sejour->getSejourItinerant()) {
                $valeur = "<b>itinérant</b>";
            }
            $array_texte_modifs[] = "Le type de séjour a été modifié a changé de type. Il est désormais un séjour " . $valeur;
        }
        if ($sejour_avant_modif->getInternational() != $sejour->getInternational()) {
            $valeur = "<b>en France</b>";
            if ($sejour->getInternational()) {
                $valeur = "<b>à l'international</b>";
            }
            $array_texte_modifs[] = "Le type de séjour a été modifié a changé de type. Il est désormais un séjour " . $valeur;
        }
        if ($sejour_avant_modif->getSejourFranceAvecAccueilEtranger() != $sejour->getSejourFranceAvecAccueilEtranger()) {
            $valeur = "<b>sans accueil d’un groupe étranger</b>";
            if ($sejour->getInternational()) {
                $valeur = "<b>avec accueil d’un groupe étranger</b>";
            }
            $array_texte_modifs[] = "Le type de séjour a été modifié a changé de type. Il est désormais un séjour " . $valeur;
        }
        if (
            ($sejour_avant_modif->getDateDebut() != $sejour->getDateDebut())
            || ($sejour_avant_modif->getDateFin() != $sejour->getDateFin())
        ) {
            $array_texte_modifs[] = "Le séjour a changé de dates - il se déroule désormais du <b>" . $sejour->getDateDebut()->format('d/m/Y') . "</b> au <b>" . $sejour->getDateFin()->format('d/m/Y') . "</b>";
        }
        if ($sejour_avant_modif->getDirecteur()->getId() != $sejour->getDirecteur()->getId()) {
            $array_texte_modifs[] = "La personne en charge de la direction du séjour a changé. Il s'agit désormais de <b>" . $sejour->getDirecteur()->getPrenom() . " " . $sejour->getDirecteur()->getNom() . "</b> qui remplace <b>" . $sejour_avant_modif->getDirecteur()->getPrenom() . " " . $sejour_avant_modif->getDirecteur()->getNom() . "</b>";
        }
        if ($sejour_avant_modif->getintitule() != $sejour->getintitule()) {
            $array_texte_modifs[] = "Le séjour a changé d'intitulé. Sa nouvelle appelation est <b>" . $sejour->getintitule() . "</b>.";
        }


        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);
        if ($resumeEffectifsAvant->getTaux_encadrement_correct() && !$resumeEffectifs->getTaux_encadrement_correct()) {
            $array_texte_modifs[] = "L'encadrement du séjour n'est plus conforme aux exigences réglementaires sur les quotats de diplômes ou sur le nombre d'encadrants par participants.";
        }
        if ($sejour_avant_modif->getDispositifAppui() != $sejour->getDispositifAppui()) {
            $valeur = "<b>sans accueil d’un groupe étranger</b>";
            if (
                ($sejour->getDispositifAppui() != Sejour::DISPOSITIF_APPUI_CAMP_BASE)
                && $sejour->getDispositifAppui() != Sejour::DISPOSITIF_APPUI_CAMP_ACCOMPAGNE
            ) {
                $valeur = "<b>sans accompagnement</b>";
            } else {
                $valeur = "<b>" . $sejour->getDispositifAppui() . "</b>";
            }
            $array_texte_modifs[] = "Le séjour a changé la nature de son accompagnement - il est désormais considéré comme " . $valeur;
        }


        if (!empty($array_texte_modifs)) {
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_025, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_LISTE_MODIFS => new ArrayObject($array_texte_modifs),
            ]);
        }
    }


    /**
     * Gestion du formulaire de compte rendu de visites d'un séjour
     * Dès lors qu'un compte-rendu est déposé, une notification est envoyée
     * 
     * @param  Request $request requête HTTP
     * @param  Sejour $sejour le séjour concerné
     * @param  NotificationHelper $notificationHelper helper pour l'envoi des notifications
     * @param  EntityManagerInterface $manager
     * @param  ValidatorInterface $validatorInterface utilitaire de validation des données du séjour
     * @uses VisiteSejourType le formulaire de dépot de compte rendu de visite
     * @return FormInterface
     */
    public function gestionVisites(Request $request, Sejour $sejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager, ValidatorInterface $validatorInterface): FormInterface
    {
        $nouvelle_visite = true;
        $visiteSejour = null;
        /** @var NominationVisiteSejour $nomination_visite */
        foreach ($sejour->getNominationsVisiteSejour() as $nomination_visite) {
            if ($nomination_visite->getVisiteur() == $this->getUser()) {
                if ($nomination_visite->getVisiteSejour()) {
                    $visiteSejour = $nomination_visite->getVisiteSejour();
                    $nouvelle_visite = false;
                } else {
                    $visiteSejour = new VisiteSejour();
                    $visiteSejour->setNominationVisite($nomination_visite);
                }
            }
        }


        $form_visite_sejour = $this->createForm(VisiteSejourType::class, $visiteSejour, []);

        $form_visite_sejour->handleRequest($request);

        if ($form_visite_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_VISITE);
            $errors = $validatorInterface->validate($visiteSejour, null, "Default");
            if (count($errors) == 0) {
                try {
                    $compteRenduEnLigne = $form_visite_sejour->get('choixCompteRendu')->getData();
                    if ($compteRenduEnLigne) {
                        $visiteSejour->setCompteRendu(null);
                    }
                    $manager->persist($visiteSejour);
                    $manager->flush();
                    if ($nouvelle_visite) {
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_081, [
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                        ]);
                    }
                    $this->addFlash('success_visite', "Modifications de la visite enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_visite', "Erreur lors de la modification de la visite du séjour: " . $e->getMessage());
                }
            } else {
                $message_erreur = "<p>
                        Données de la visite incorrectes, veuillez corriger:
                        <ul>";
                // there are errors, now you can show them
                foreach ($errors as $violation) {
                    $message_erreur .= '<li>';
                    $message_erreur .= $violation->getMessage();
                    $message_erreur .= '</li>';
                }
                $message_erreur .= '</ul></p>';
                $this->addFlash('danger_visite', $message_erreur);
            }
        }


        return $form_visite_sejour;
    }

    /**
     * Permet de gérer les formulaires de validation "classique" et de passer dans les états adéquats
     *
     * Si l'avis est défavorable, le séjour retourne dans l'état en construction et une notification est envoyée aux personne concernées
     * Si l'avis est favorable,
     *  si le séjour est international, 
     *     si le séjour a été validé à l'international, le séjour passe à "validé" et une notification est envoyée aux personne concernées
     *     sinon le séjour reste en attente de validation  et une notification est envoyée aux personne concernées
     *  sinon le séjour passe à "validé"  et une notification est envoyée aux personne concernées
     * @param  Request $request requête HTTP
     * @param  Sejour $sejour séjour concerné
     * @param  NotificationHelper $notificationHelper helper pour la gestion des notifications
     * @param  EntityManagerInterface $manager
     * @return FormInterface le formulaire de validation du séjour
     * @uses ValidationSejourType formulaire de validation du séjour
     */
    public function gestionValidationClassique(Request $request, Sejour $sejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager): FormInterface
    {

        if ($sejour->getValidationSejour()) {
            $validationSejour = $sejour->getValidationSejour();
        } else {
            $validationSejour = new ValidationSejour();
            $validationSejour->setSejour($sejour);
        }


        $form_validation_sejour = $this->createForm(ValidationSejourType::class, $validationSejour, []);

        $form_validation_sejour->handleRequest($request);

        if ($form_validation_sejour->isSubmitted()) {
            if ($form_validation_sejour->isValid()) {
                $validationSejour->setDateModification(new DateTime());
                try {

                    if ($sejour->isValidable()) {
                        if ($validationSejour->getEstValide()) {
                            if (
                                (!$sejour->getInternational() && !$sejour->getSejourFranceAvecAccueilEtranger())
                                || ($sejour->getValidationInternationaleSejour() && $sejour->getValidationInternationaleSejour()->getEstValide())
                            ) {

                                if ($this->sejourWorkflow->can($sejour, 'validation_sejour')) {
                                    $this->sejourWorkflow->apply($sejour, 'validation_sejour');
                                }
                            }

                            // Dans tous les cas on envoie la notification:
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_021, [
                                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                            ]);
                        } else { // Si l'avis est défavorable, de toute façon on repasse en contruction de séjour

                            if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_attente_validation')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_attente_validation');
                            } else if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_sejour_valide')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_sejour_valide');
                            } else if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_sejour_en_cours')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_sejour_en_cours');
                            } else {
                                throw new Exception("Le séjour ne peut pas être invalidé depuis cet état");
                            }
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_022, [
                                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                            ]);
                        }
                    }

                    $manager->persist($validationSejour);
                    $manager->persist($sejour);
                    $manager->flush();
                    $this->addFlash('success_validation', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_validation', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_validation', "Données incorrectes, veuillez corriger");
            }
        }

        return $form_validation_sejour;
    }

    /**
     * Permet de gérer les formulaires de validation Internationale et de passer dans les états adéquats
     *
     * - Si l'avis est défavorable, le séjour retourne dans l'état en construction et une notification est envoyée aux personnes concernées
     * - Si l'avis est favorable:
     *     - si le séjour a été validé de façon classique, le séjour passe à "validé" et une notification est envoyée aux personnes concernées
     *     - sinon le séjour reste en attente de validation et une notification est envoyée aux personnes concernées
     * @param  Request $request requête HTTP
     * @param  Sejour $sejour séjour concerné
     * @param  NotificationHelper $notificationHelper helper pour la gestion des notifications
     * @param  EntityManagerInterface $manager
     * @return FormInterface le formulaire de validation internationale
     * @uses ValidationInternationaleSejourType formulaire de validation internationale du séjour
     */
    public function gestionValidationInternationale(Request $request, Sejour $sejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager): FormInterface
    {

        if ($sejour->getValidationInternationaleSejour()) {
            $validationInternationaleSejour = $sejour->getValidationInternationaleSejour();
        } else {
            $validationInternationaleSejour = new ValidationInternationaleSejour();
            $validationInternationaleSejour->setSejour($sejour);
        }

        $form_validation_internationale_sejour = $this->createForm(ValidationInternationaleSejourType::class, $validationInternationaleSejour, []);

        $form_validation_internationale_sejour->handleRequest($request);

        if ($form_validation_internationale_sejour->isSubmitted()) {
            if ($form_validation_internationale_sejour->isValid()) {
                $validationInternationaleSejour->setDateModification(new DateTime());
                try {

                    if ($sejour->isValidable()) {

                        if ($validationInternationaleSejour->getEstValide()) {

                            if ($sejour->getValidationSejour() && $sejour->getValidationSejour()->getEstValide()) {
                                if ($this->sejourWorkflow->can($sejour, 'validation_sejour')) {
                                    $this->sejourWorkflow->apply($sejour, 'validation_sejour');
                                }
                            }

                            // Dans tous les cas on envoie la notification:
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_023, [
                                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                            ]);
                        } else { // Si l'avis est défavorable, de toute façon on repasse en contruction de séjour
                            if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_attente_validation')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_attente_validation');
                            } else if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_sejour_valide')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_sejour_valide');
                            } else if ($this->sejourWorkflow->can($sejour, 'invalidation_sejour_depuis_sejour_en_cours')) {
                                $this->sejourWorkflow->apply($sejour, 'invalidation_sejour_depuis_sejour_en_cours');
                            } else {
                                throw new Exception("Le séjour ne peut pas être invalidé depuis cet état");
                            }
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_024, [
                                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                            ]);
                        }
                    }

                    $manager->persist($validationInternationaleSejour);
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_validation_internationale', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_validation_internationale', "Erreur lors de la validation internationale du séjour: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_validation_internationale', "Données incorrectes, veuillez corriger");
            }
        }

        return $form_validation_internationale_sejour;
    }

    /**
     * Gère les formulaires de clôture de séjour:
     *  - Informations générales
     *  - Effectifs
     *  - Dates et lieux du séjour
     *  - Retours généraux
     *  - Bilan pédagogique
     *  - Bilan logistique
     * 
     * Désérialise aussi les éléments de construction du séjour pour un affichage non modifiable, et calcule le tableau de asumé des effectifs du séjour
     *
     * @param  Request $request la requête HTTP
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  DocumentRessourceRepository $documentRessourceRepository
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour calculer les effectifs du séjour
     * @param  array $criteresValidationFormulaires liste des critères de validation des formulaires
     * @param  SerializerInterface $serializer utilitaire de sérialisation
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @param  mixed $validatorInterface
     * @return array les paramètres de vue twig correspondants
     * @uses RetoursSejourType formulaire de retour general du séjour
     * @uses PedaClotureSejourType formulaire de cloture des éléments pédagogiques du séjour
     * @uses AspectsLogistiquesClotureSejourType formulaire de clôture des éléments logisitiques du séjour
     * @see SejourController::gestionFormulairesInformationsGeneralesSejour() gestion des formulaires d'informations générales
     * @see SejourController::gestionFormulairesEffectifsSejours() gestion des formulaires d'effectifs'
     * @see SejourController::gestionFormulairesDatesLieuxSejour() gestion des formulaires de dates et lieux du séjour
     */
    public function gestionClotureSejour(Request $request, Sejour $sejour, EntityManagerInterface $manager, DocumentRessourceRepository $documentRessourceRepository, ResumeEffectifsHelper $resumeEffectifsHelper, array $criteresValidationFormulaires, SerializerInterface $serializer, NotificationHelper $notificationHelper, ValidatorInterface $validatorInterface, FormFactoryInterface $formFactory): array
    {
        $parametresVue = array();

        //----------------------------------------------------------
        //Gestion Informations générales
        $result = $this->gestionFormulairesInformationsGeneralesSejour($documentRessourceRepository, $sejour, $request,  $manager, $criteresValidationFormulaires, $notificationHelper, $validatorInterface);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );
        //-------------------------------------------------------------------------------
        //Retour généraux
        if ($sejour->getRetourSejour()) {
            $retoursGenerauxSejour = $sejour->getRetourSejour();
        } else {
            $retoursGenerauxSejour = new RetourSejour();
            $retoursGenerauxSejour->setSejour($sejour);
        }
        $form_cloture_retours_generaux_sejour = $this->createForm(RetoursSejourType::class, $retoursGenerauxSejour, []);
        $form_cloture_retours_generaux_sejour->handleRequest($request);

        if ($form_cloture_retours_generaux_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_RETOURS_GENERAUX);
            if ($form_cloture_retours_generaux_sejour->isValid()) {

                /* -------------------------------------------------------------------- */
                //Gestion des Visites
                $listeVisites = $form_cloture_retours_generaux_sejour->get('visites')->getData();


                // On supprime les Visites
                foreach ($retoursGenerauxSejour->getVisites() as $anciennesVisites) {
                    if (!$listeVisites->contains($anciennesVisites)) {
                        $retoursGenerauxSejour->removeVisite($anciennesVisites);
                    }
                }
                foreach ($listeVisites as $nouvelleVisite) {
                    $retoursGenerauxSejour->addVisite($nouvelleVisite);
                }

                /* -------------------------------------------------------------------- */
                //Gestion des Evenements graves
                $listeEvenementsGraves = $form_cloture_retours_generaux_sejour->get('descriptionEvenementsGraves')->getData();


                // On supprime les Evenements graves
                foreach ($retoursGenerauxSejour->getDescriptionEvenementsGraves() as $ancienEvenementsGrave) {
                    if (!$listeEvenementsGraves->contains($ancienEvenementsGrave)) {
                        $retoursGenerauxSejour->removeDescriptionEvenementsGrave($ancienEvenementsGrave);
                    }
                }
                foreach ($listeEvenementsGraves as $nouvelEvenementGrave) {
                    $retoursGenerauxSejour->addDescriptionEvenementsGrave($nouvelEvenementGrave);
                }


                $sejour->setDateModification(new DateTime());
                try {

                    $manager->persist($retoursGenerauxSejour);
                    $manager->flush();

                    $this->addFlash('success_cloture_retours_generaux', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_cloture_retours_generaux', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_cloture_retours_generaux', "Données incorrectes, veuillez corriger");
            }
        }
        //----------------------------------------------------------
        //Gestion Effectifs
        $result = $this->gestionFormulairesEffectifsSejours($documentRessourceRepository, $sejour, $request, $manager, $resumeEffectifsHelper, $criteresValidationFormulaires, $formFactory);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );

        //----------------------------------------------------------
        //Gestion Dates et lieux

        $result = $this->gestionFormulairesDatesLieuxSejour($documentRessourceRepository, $sejour, $request,  $manager, $criteresValidationFormulaires);

        $parametresVue = array_merge(
            $parametresVue,
            $result['parametresVue']
        );

        //-------------------------------------------------------------------------------
        //Péda
        $form_cloture_peda_sejour = $this->createForm(PedaClotureSejourType::class, $sejour, ["sejour" => $sejour]);

        $form_cloture_peda_sejour->handleRequest($request);

        if ($form_cloture_peda_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_DEMARCHES_PEDA);
            if ($form_cloture_peda_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_cloture_peda', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_cloture_peda', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_cloture_peda', "Données incorrectes, veuillez corriger");
            }
        }
        //-------------------------------------------------------------------------------
        //Logistique
        $form_cloture_logistique_sejour = $this->createForm(AspectsLogistiquesClotureSejourType::class, $sejour, []);

        $form_cloture_logistique_sejour->handleRequest($request);

        if ($form_cloture_logistique_sejour->isSubmitted()) {
            $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_LOGISTIQUE);
            if ($form_cloture_logistique_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_cloture_logistique', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $this->addFlash('danger_cloture_logistique', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_cloture_logistique', "Données incorrectes, veuillez corriger");
            }
        }

        $parametresVue["form_cloture_retours_generaux_sejour"] = $form_cloture_retours_generaux_sejour->createView();
        $parametresVue["form_cloture_logistique_sejour"] = $form_cloture_logistique_sejour->createView();
        $parametresVue["form_cloture_logistique_sejour"] = $form_cloture_logistique_sejour->createView();
        $parametresVue["form_cloture_peda_sejour"] = $form_cloture_peda_sejour->createView();
        $parametresVue["form_cloture_logistique_sejour"] = $form_cloture_logistique_sejour->createView();

        $docRepereEvaluation = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_FICHE_REPERE_EVAL_SEJOUR]);
        $parametresVue["docRepereEvaluation"] = $docRepereEvaluation;
        $docBilanInternational = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_BILAN_INTERNATIONAL]);
        $parametresVue["docBilanInternational"] = $docBilanInternational;
        $docCompteResultat = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_COMPTE_RESULTAT]);
        $parametresVue["docCompteResultat"] = $docCompteResultat;
        $docDefAccidentGrave = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_DEF_EVENEMENT_GRAVE]);
        $parametresVue["docDefAccidentGrave"] = $docDefAccidentGrave;

        $infos_construction_sejour = $serializer->deserialize($sejour->getInfosSejour(), Sejour::class, 'json');
        $resume_effectifs_infos_construction_sejour = $resumeEffectifsHelper->getResumeEffectifsSejour($infos_construction_sejour);
        $parametresVue['infos_construction_sejour'] = $infos_construction_sejour;
        $parametresVue['resume_effectifs_infos_construction_sejour'] = $resume_effectifs_infos_construction_sejour;


        return $parametresVue;
    }


    /**
     * Détermine les éléments restants avant la publication de la déclaration d'intention, et affiche un message flash correspondant
     *
     * @param  Sejour $sejour le séjour concerné
     * @param  ValidatorInterface $validatorInterface l'utilitaire de validation
     * @return void
     */
    public function gestionResteAvantPublicationDeclarationIntention(Sejour $sejour, ValidatorInterface $validatorInterface)
    {
        $errors = $validatorInterface->validate($sejour, null, ['Default', 'publication_declaration_intention']);
        if (0 !== count($errors)) {
            $message_erreur = "<p>
            Pour pouvoir publier votre déclaration d'intention, vous devez corriger les aspects suivants:
            <ul>";
            // there are errors, now you can show them
            foreach ($errors as $violation) {
                $message_erreur .= '<li>';
                $message_erreur .= $violation->getMessage();
                $message_erreur .= '</li>';
            }
            $message_erreur .= '</ul></p>';
            $this->addFlash('warning_sejour', $message_erreur);
        } else {
            $this->addFlash('success_sejour', "Tous les aspects nécessaires à la publication de la déclaration d'intention du séjour sont remplis. Pour déclarer votre séjour, vous devez maintenant cliquer sur le bouton \"Publier la déclaration d'intention\"");
        }
    }

    /**
     * Détermine les éléments restants avant l'envoi pour validation, et affiche un message flash correspondant
     *
     * @param  Sejour $sejour le séjour concerné
     * @param  ValidatorInterface $validatorInterface l'utilitaire de validation
     * @return void
     */
    public function gestionResteAvantEnvoiPourValidation(Sejour $sejour, ValidatorInterface $validatorInterface)
    {
        $errors = $validatorInterface->validate($sejour, null, ['Default', 'envoi_pour_validation']);
        if (0 !== count($errors)) {
            $message_erreur = "<p>
            Pour pouvoir envoyer votre projet de séjour en validation, vous devez corriger les aspects suivants:
            <ul>";
            // there are errors, now you can show them
            foreach ($errors as $violation) {
                $message_erreur .= '<li>';
                $message_erreur .= $violation->getMessage();
                $message_erreur .= '</li>';
            }
            $message_erreur .= '</ul></p>';
            $this->addFlash('warning_envoi_validation', $message_erreur);
        } else {
            $this->addFlash('info_envoi_validation', "Tous les aspects nécessaires à l'envoi pour validation du séjour sont remplis");
        }
    }

    /**
     * Détermine les éléments restants avant la clôture du séjour, et affiche un message flash correspondant
     *
     * @param  Sejour $sejour le séjour concerné
     * @param  ValidatorInterface $validatorInterface l'utilitaire de validation
     * @return void
     */
    public function gestionResteAvantCloture(Sejour $sejour, ValidatorInterface $validatorInterface)
    {
        $errors = $validatorInterface->validate($sejour, null, ['Default', 'cloture_sejour']);
        if (0 !== count($errors)) {
            $message_erreur = "<p>
            Pour pouvoir clôturer votre séjour, vous devez renseigner les aspects suivants:
            <ul>";
            // there are errors, now you can show them
            foreach ($errors as $violation) {
                $message_erreur .= '<li>';
                $message_erreur .= $violation->getMessage();
                $message_erreur .= '</li>';
            }
            $message_erreur .= '</ul></p>';
            $this->addFlash('warning_envoi_cloture', $message_erreur);
        } else {
            $this->addFlash('info_envoi_cloture', "Tous les aspects nécessaires à la clôture du séjour sont remplis");
        }
    }

    /**
     * API permettant de renvoyer le formulaire avec les unités correspondantes en fonction de l'ID de structure passé en paramètres
     * @param  Request $request requête HTTP contenant l'Identifiant de la structure
     * @param  StructureRepository $structureRepository permet d'accéder à la structure
     * @return void
     * @api
     * @uses StructureRegroupementConstructionSejourType formulaire de sélection des structure de regroupement
     */
    #[Route('/sejour/utility/structure-select', name: 'sejour_construction_structure-select')]
    public function getSpecificStructureEEDFSelect(Request $request, StructureRepository $structureRepository, FormFactoryInterface $formFactory)
    {
        $structure = $structureRepository->findOneBy(['id' => $request->query->get('structure')]);
        $index = $request->query->get('index');
        $structure_regroupee = new StructureRegroupementSejour();
        $structure_regroupee->setStructureEEDF($structure);
        $form = $formFactory->createNamed(
            'unites_participantes_sejour_structuresRegroupees_' . $index,
            StructureRegroupementConstructionSejourType::class,
            $structure_regroupee
        );

        return $this->render('sejour/components/modification/_form_specific_structure_eedf.html.twig', [

            'structure_regroupee_form' => $form->createView(),
        ]);
    }


    /**
     * Route pour afficher les données du séjour non modifiables suivant sont état.
     * 
     * Les données de déclaration d'intention sont désérialisées
     * Les données de construction de séjour sont désérialisées dans le cas ou le séjour est terminé ou cloture
     * Les commentaires restent gérés
     * Si la personne est validatrice ou validatrice internationale, des formulaires de validation sont affichés
     * Si la personne est visiteurse du séjour, la saisie du compte rendu de visite est activée
     * @param  EntityManagerInterface $manager
     * @param  Request $request requete HTTP
     * @param  Sejour $sejour séjour à afficher
     * @param  SerializerInterface $serializer utilitaire de déserialisation
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour calculer les effectifs
     * @param  DocumentRessourceRepository $documentRessourceRepository permet d'accéder aux documents ressources
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return Response
     * @see SejourController::gestionCommentaires()
     * @see SejourController::gestionVisites():: pour gérer les compte rendus de visite de camp
     * @see SejourController::gestionValidationClassique():: pour gérer la validation de séjour classique
     * @see SejourController::gestionValidationInternationale():: pour gérer validation de séjour internationale
     */
    #[Route('/sejour/construction/suivi/{id}', name: 'sejour_construction_projet_suivi')]
    public function constructionProjetSuivi(EntityManagerInterface $manager, Request $request, Sejour $sejour, SerializerInterface $serializer, ResumeEffectifsHelper $resumeEffectifsHelper, DocumentRessourceRepository $documentRessourceRepository, NotificationHelper $notificationHelper, ValidatorInterface $validatorInterface, FormFactoryInterface $formFactory): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_SUIVI, $sejour);

        $sejour->setPanelActif(new PanelActifSejour());
        $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);
        $decla_intention_sejour = $serializer->deserialize($sejour->getInfosDeclaration(), Sejour::class, 'json');

        $parametresVue = [
            'decla_intention_sejour' => $decla_intention_sejour,
            'sejour' => $sejour,
            'resumeEffectifs' => $resumeEffectifs,
        ];
        $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);
        $parametresVue['docModeleVisiteCamp'] = $docModeleVisiteCamp;
        if ($sejour->isValidable()) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_VALIDATION);
            // Chargement des doc ressources nécessaires:
            $docModeleValidation = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VALIDATION_SEJOUR]);
            $docModeleValidationInternationale = $documentRessourceRepository->findOneBy(['nom' =>  DocumentRessource::DOC_MODELE_VALIDATION_INTERNATIONALE_SEJOUR]);

            $form_validation_sejour = $this->gestionValidationClassique($request, $sejour,  $notificationHelper, $manager);
            $form_validation_internationale_sejour = $this->gestionValidationInternationale($request, $sejour,  $notificationHelper, $manager);

            //VALIDATION DE SEJOUR
            $parametresVue['form_validation_sejour'] = $form_validation_sejour->createView();
            $parametresVue['form_validation_internationale_sejour'] = $form_validation_internationale_sejour->createView();
            $parametresVue['docModeleValidation'] = $docModeleValidation;
            $parametresVue['docModeleValidationInternationale'] = $docModeleValidationInternationale;
        }

        //VISITE DE SEJOUR
        if ($sejour->isVisitable()) {

            $form_visite_sejour = $this->gestionVisites($request, $sejour,  $notificationHelper, $manager, $validatorInterface);
            $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);
            $docModeleCheckListDirecteur = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CHECKLIST_CLASSEUR_DIRECTEUR]);

            $parametresVue['form_visite_sejour'] = $form_visite_sejour->createView();
            $parametresVue['docModeleCheckListDirecteur'] = $docModeleCheckListDirecteur;
            $parametresVue['docModeleVisiteCamp'] = $docModeleVisiteCamp;
        }
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);
        if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE) || $statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
            //CLOTURE DE SEJOUR 
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
            $docDefAccidentGrave = $documentRessourceRepository->findOneBy(['nom' =>  DocumentRessource::DOC_DEF_EVENEMENT_GRAVE]);
            //Déserialisation des infos de construction
            $infos_construction_sejour = $serializer->deserialize($sejour->getInfosSejour(), Sejour::class, 'json');
            $resume_effectifs_infos_construction_sejour = $resumeEffectifsHelper->getResumeEffectifsSejour($infos_construction_sejour);
            $parametresVue['infos_construction_sejour'] = $infos_construction_sejour;
            $parametresVue['resume_effectifs_infos_construction_sejour'] = $resume_effectifs_infos_construction_sejour;
            $parametresVue['docDefAccidentGrave'] = $docDefAccidentGrave;
        }
        $parametresVue['arrayFormsCommentairesParSujet'] = $this->gestionCommentaires($request, $manager, $sejour, $notificationHelper, $formFactory);
        return $this->render('sejour/construction_sejour_sans_modif.html.twig', $parametresVue);
    }

    /**
     * Affiche les données archivées d'un séjour cloturé
     *
     * @param  Sejour $sejour le séjour cloturé
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour le calcul des effectifs
     * @return Response
     */
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/sejour/archive/detail/{id}', name: 'detail_sejour_archive')]
    public function detailProjetArchive(Sejour $sejour, ResumeEffectifsHelper $resumeEffectifsHelper): Response
    {
        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);

        $parametresVue = [
            'sejour' => $sejour,
            'resumeEffectifs' => $resumeEffectifs
        ];
        return $this->render('sejour/detail_sejour_archive.html.twig', $parametresVue);
    }

    /**
     * supprimerEncadrant
     *
     * @param  Sejour $sejour
     * @param  RoleEncadrantSejour $encadrant
     * @return void
     */
    #[Route('/sejour/{sejour}/supprimer/encadrant/{encadrant}', name: 'supprimer_encadrant_sejour')]
    public function supprimerEncadrant(Request $request, Sejour $sejour, RoleEncadrantSejour $encadrant, EntityManagerInterface $manager)
    {
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);
        $sejour->setPanelActif(new PanelActifSejour());
        if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
            $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
        } else {
            $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
            $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
        }
        //Si l'encadrant supprimé est EEDF, on met à jour ses droits de suivi
        if ($encadrant->getEstEncadrantEEDF()) {
            if (in_array(RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE, $encadrant->getFonction())) {
                $sejour->removeDirecteursAdjoint($encadrant->getEncadrantEEDF());
                //TODO envoi notification suppression directeur adjoint
            } else {
                $sejour->removeEncadrant($encadrant->getEncadrantEEDF());
                //TODO envoi notification suppression encadrant
            }
        }

        $sejour->removeRolesEncadrant($encadrant);

        try {
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_effectifs_encadrants', "Encadrant·e supprimé·e du séjour avec succès");
        } catch (Exception $e) {
            $enregistrementOk = false;
            $this->addFlash('danger_effectifs_encadrants', "Erreur lors de la suppression d'un·e encadrant·e: " . $e->getMessage());
        }
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }
    /**
     * Route pour l'affichage d'un séjour suspendu (non modifiable)
     *
     * @param  Request $request
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper
     * @param  Sejour $sejour
     * @param  SerializerInterface $serializer utilitaire sérialisation/désérialisation
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour le calcul des effectifs
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources
     * @return Response
     * @see SejourController::gestionCommentaires()
     */
    #[Route('/sejour/suspendu/{id}', name: 'sejour_suspendu')]
    public function sejourSuspendu(Request $request, EntityManagerInterface $manager, NotificationHelper $notificationHelper, Sejour $sejour, SerializerInterface  $serializer, ResumeEffectifsHelper $resumeEffectifsHelper, DocumentRessourceRepository $documentRessourceRepository, FormFactoryInterface $formFactory): Response
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_SUIVI, $sejour);

        $arrayFormsCommentairesParSujet = $this->gestionCommentaires($request, $manager, $sejour, $notificationHelper, $formFactory);
        $sejour->setPanelActif(new PanelActifSejour());
        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);
        $decla_intention_sejour = $serializer->deserialize($sejour->getInfosDeclaration(), Sejour::class, 'json');
        $docModeleVisiteCamp = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_MODELE_VISITE_SEJOUR]);



        return $this->render('sejour/sejour_suspendu.html.twig', [
            'decla_intention_sejour' => $decla_intention_sejour,
            'sejour' => $sejour,
            'docModeleVisiteCamp' => $docModeleVisiteCamp,
            'resumeEffectifs' => $resumeEffectifs,
            'arrayFormsCommentairesParSujet' => $arrayFormsCommentairesParSujet
        ]);
    }

    /**
     * API pour avoir les infos d'un·e personne à partir du numéro d'adhérent·e passé en requête ('numAdh')
     * La personne ne peut avoir accès aux données que si elle est organisatrice de la structure organisatrice, ou organisatrice du séjour
     * @param  PersonneRepository $personneRepository pour avoir accès aux données de la personne
     * @param  StructureRepository $structureRepository pour vérifier les droits d'organisation de la structure organisatrice
     * @param  SejourRepository $sejourRepository pour vérifier les droits d'organisation du séjour
     * @param  Request $request requete HTTP
     * @return json les infos de la personne et le statut 200 OK
     * @api
     */
    #[Route('/sejour/infos/personne', methods: ["GET"], name: 'sejour_infos_personnes')]
    public function getPersonnesApi(PersonneRepository $personneRepository, StructureRepository $structureRepository, SejourRepository $sejourRepository, Request $request)
    {
        $sejour_id = $request->query->get('sejour');
        if ($sejour_id) {
            $sejour = $sejourRepository->findOneById($sejour_id);
            $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_ORGANISER, $sejour);
        } else {
            $structure_id = $request->query->get('structure');
            $structure = $structureRepository->findOneById($structure_id);
            $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_ORGANISER, $structure);
        }
        $transformer = new NumAdhToPersonneTransformer($personneRepository);

        $personne = $transformer->reverseTransform($request->query->get('text'));
        return $this->json(
            [
                'personne' => $personne,
                'adresseMail' => ($personne !== null) ? $personne->getAdresseMailUsage() : "" //Gestion adresse mail pro si salarié·e
            ],
            200,
            [],
            ['groups' => ['infos_personne']]
        );
    }

    /**
     * Permet d'exporter les informations pour la déclaration TAM
     *
     * Le fichier est téléchargé automatiquement sur le PC de l'utilisat·eur·rice, puis supprimé
     * @param Sejour $sejour le séjour dont il faut exporter les informations
     * @param ExportDocumentHelper $exportDocumentHelper utilitaire pour exporter les documents
     * @param ResumeEffectifsHelper $resumeEffectifsHelper helper pour calculer les effectifs
     * @return void
     */
    #[Route('export/tam/{sejour}', name: 'export_tam')]
    public function exportDocumentTAM(Sejour $sejour, ExportDocumentHelper $exportDocumentHelper, ResumeEffectifsHelper $resumeEffectifsHelper)
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_MODIFICATION_PROJET, $sejour);

        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);
        $exportDocumentHelper->exportDocumentTAM($sejour, $resumeEffectifs);

        $tempFile = 'temp/exportTam' . $sejour->getId() . '.docx';
        // Return the excel file as an attachment
        $response = $this->file($tempFile, $exportDocumentHelper->formattageNomFichier('export TAM ' . $sejour->getintitule() . '.docx'), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->deleteFileAfterSend(true);
        return $response;
    }


    /**
     * Permet d'exporter les informations du séjour sous forme d'archive
     *
     * L'archive est téléchargée automatiquement sur le PC de l'utilisat·eur·rice, puis supprimée
     * @param Sejour $sejour le séjour dont il faut exporter les informations
     * @param ExportDocumentHelper $exportDocumentHelper utilitaire pour exporter les documents
     * @param ResumeEffectifsHelper $resumeEffectifsHelper helper pour calculer les effectifs
     * @param  UploaderHelper $uploaderHelper helper pour la génération de l'archive
     * @return void
     */
    #[Route('export/projet/{sejour}', name: 'export_projet')]
    public function exportDocumentProjetSejour(Sejour $sejour, UploaderHelper $uploaderHelper, ExportDocumentHelper $exportDocumentHelper, ResumeEffectifsHelper $resumeEffectifsHelper)
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_MODIFICATION_PROJET, $sejour);

        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);
        $tempFile = $exportDocumentHelper->generationArchiveProjetSejour($sejour, $uploaderHelper, $resumeEffectifs);

        $response = $this->file($tempFile, $exportDocumentHelper->formattageNomFichier('Éléments du projet du séjour ' . $sejour->getintitule() . '.zip'), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->deleteFileAfterSend(true);
        return $response;
    }


    /**
     * Permet d'exporter la liste des participant·e·s au séjour sous forme de tableur
     *
     * Le tableur est téléchargée automatiquement sur le PC de l'utilisat·eur·rice, puis supprimée
     * @param Sejour $sejour le séjour dont il faut exporter les informations
     * @param ExportDocumentHelper $exportDocumentHelper utilitaire pour exporter les documents
     * @return void
     */
    #[Route('export/participants/{sejour}', name: 'export_participants')]
    public function exportParticipants(Sejour $sejour, ExportDocumentHelper $exportDocumentHelper)
    {
        $this->denyAccessUnlessGranted(SejourVoter::SEJOUR_MODIFICATION_PROJET, $sejour);

        $exportDocumentHelper->generationTableurParticipants($sejour);

        $tempFile = 'temp/exportParticipants' . $sejour->getId() . '.xlsx';
        // Return the excel file as an attachment
        $response = $this->file($tempFile, $exportDocumentHelper->formattageNomFichier('export participants ' . $sejour->getintitule() . '.xlsx'), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->deleteFileAfterSend(true);
        return $response;
    }

    /**
     * Permet de gérer les formulaires concernant les effectifs d'un séjour
     *
     * Notamment:
     *  - les qualifications du·de la direct·eur·rice
     *  - la liste des encadrant·e·s du séjour (lorsqu'un·e encadrant·e est ajouté/supprimé·e, on lui ajoute les droit nécessaire (encadrement ou direction adjointe))
     *  - les participant·e·s par unité EEDF participant au séjour
     *  - les participant·e·s par structure non EEDF participant au séjour
     *  - les participant·e·s à besoins spécifiques
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources stockées en BDD
     * @param  Sejour $sejour le séjour concerné
     * @param  Request $request la requête HTTP
     * @param  EntityManagerInterface $manager
     * @param  ResumeEffectifsHelper $resumeEffectifsHelper helper pour le calcul des efectifs et du taux d'encadrement
     * @param  array $criteresValidationFormulaires la liste des critères de validation des formulaires
     * @return array contenant les paramètres de la vue, et si les données renseignées ont été enregistrées ou non
     * @uses QualificationDirectionType formulaire pour les diplômes et qualification du·de la direct·eur·rice
     * @uses AspectEffectifsEncadrementSejourType formulaire pour les encadrant·e·s du séjour
     * @uses ParticipationParticipantsSejourUniteType formulaires pour les participants au séjour d'une unité EEDF
     * @uses EffectifStructureRegroupementNonEEDFType formulaires pour les participants au séjour d'une structure non EEDF
     * @uses AspectEffectifsBesoinsSpecifiquesType formulaire pour les participant·e·s à besoins spécifiques
     * @used-by SejourController::gestionConstructionSejour() route pour effectuer la construction du séjour
     * @used-by SejourController::gestionClotureSejour() route pour effectuer la clôture du séjour
     */
    public function gestionFormulairesEffectifsSejours(DocumentRessourceRepository $documentRessourceRepository, Sejour $sejour, Request $request, EntityManagerInterface $manager, ResumeEffectifsHelper $resumeEffectifsHelper, array $criteresValidationFormulaires, FormFactoryInterface $formFactory): array
    {
        $enregistrementOk = true;
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);

        $docEquivalenceAnimationDirection = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_EQUIVALENCE_BAFA_BAFD]);
        $docEquivalencePSC1 = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_EQUIVALENCE_PSC1]);
        $docFormulaireConsentement = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_FORMULAIRE_CONSENTEMENT]);

        //Gestion De la qualification du directeur
        $form_qualification_direction_sejour = $this->createForm(QualificationDirectionSejourType::class, $sejour, [
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_qualification_direction_sejour->handleRequest($request);
        if ($form_qualification_direction_sejour->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
            }
            if ($form_qualification_direction_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_effectifs_direction', "Modifications de la qualification du·de la direct·eur·rice enregistrées avec succès");
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_direction', "Erreur lors de l'enregistrement de la qualification du·de la direct·eur·rice: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger_effectifs_direction', "Données incorrectes, veuillez corriger");
                $enregistrementOk = false;
            }
        }
        // Gestion d'un·e nouvel·le encadrant·e:
        $nouvel_encadrant = new RoleEncadrantSejour();
        $form_nouvel_encadrant = $this->createForm(EncadrantSejourType::class, $nouvel_encadrant, [
            'sejour' => $sejour,
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_nouvel_encadrant->handleRequest($request);
        if ($form_nouvel_encadrant->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
            }
            if ($form_nouvel_encadrant->isValid()) {
                if ($nouvel_encadrant->getEncadrantEEDF() != null) {
                    if ($nouvel_encadrant->getEstEncadrantEEDF()) {
                        $nouvel_encadrant->setInfosEncadrantNonEEDF(null);
                        if (in_array(RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE, $nouvel_encadrant->getFonction())) {
                            $sejour->addDirecteursAdjoint($nouvel_encadrant->getEncadrantEEDF());
                            //TODO envoi notification suppression directeur adjoint
                        } else {
                            $sejour->addEncadrant($nouvel_encadrant->getEncadrantEEDF());
                            //TODO envoi notification suppression encadrant
                        }
                    }
                }
                $sejour->addRolesEncadrant($nouvel_encadrant);

                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_effectifs_encadrants', "Encadrant·e ajouté·e avec succès");
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_encadrants', "Erreur lors de l'ajout de l'encadrant·e : " . $e->getMessage());
                }
            } else {
                $enregistrementOk = false;
                $this->addFlash('danger_effectifs_encadrants', "Données incorrectes, veuillez corriger");
            }
        }
        //Gestion des Encadrant·e·s existant·e·s

        $arrayFormEncadrant = array();
        /** @var RoleEncadrantSejour $encadrant */
        foreach ($sejour->getRolesEncadrants() as $encadrant) {
            $form = $formFactory->createNamed('encadrant_sejour_' . $encadrant->getId(), EncadrantSejourType::class, $encadrant, [
                'sejour' => $sejour,
                'validation_groups' => $criteresValidationFormulaires
            ]);
            $arrayFormEncadrant[$encadrant->getId()] = $form;
        }

        foreach ($arrayFormEncadrant as  $formEncadrant) {
            /** @var RoleEncadrantSejour $encadrant_existant */
            $encadrant_existant = $formEncadrant->getData();
            $formEncadrant->handleRequest($request);
            if ($formEncadrant->isSubmitted()) {
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                    $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
                } else {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                    $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
                }

                if ($formEncadrant->isValid()) {
                    // On supprime les encadrant n'apparaissant plus dans le form
                    /** @var RoleEncadrantSejour $encadrant_existant */
                    if ($encadrant_existant->getEncadrantEEDF() != null) {
                        if ($encadrant_existant->getInfosEncadrantNonEEDF()) {
                            $manager->remove($encadrant_existant->getInfosEncadrantNonEEDF());
                            $encadrant_existant->setInfosEncadrantNonEEDF(null);
                        }



                        if (in_array(RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE, $encadrant_existant->getFonction())) {
                            $sejour->addDirecteursAdjoint($encadrant_existant->getEncadrantEEDF());
                            $sejour->removeEncadrant($encadrant_existant->getEncadrantEEDF());
                            //TODO envoi notification suppression directeur adjoint
                        } else {

                            $sejour->removeDirecteursAdjoint($encadrant_existant->getEncadrantEEDF());
                            $sejour->addEncadrant($encadrant_existant->getEncadrantEEDF());
                            //TODO envoi notification suppression encadrant
                        }
                    }




                    $sejour->setDateModification(new DateTime());
                    try {
                        $manager->persist($sejour);
                        $manager->flush();

                        $this->addFlash('success_effectifs_encadrants', "Modifications de l'encadrant·e enregistrées avec succès");
                    } catch (Exception $e) {
                        $enregistrementOk = false;
                        $this->addFlash('danger_effectifs_encadrants', "Erreur lors de l'enregistrement de l'encadrant·e : " . $e->getMessage());
                    }
                } else {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_encadrants', "Données incorrectes, veuillez corriger");
                }
            }
        }



        //===========================================//
        //Gestion des Particpant.e.s
        $arrayFormParticipantsUniteEEDF = [];
        $arrayFormParticipantsStructureNonEEDF = [];


        /** @var ParticipationSejourUnite $uniteParticipante */
        foreach ($sejour->getUnitesStructureOrganisatrice() as $uniteParticipante) {
            $form = $formFactory->createNamed('participants_sejour_' . $uniteParticipante->getEquipe()->getId(), ParticipationParticipantsSejourUniteType::class, $uniteParticipante, [
                'equipe' => $uniteParticipante->getEquipe(),
                'sejour' => $sejour,
                'validation_groups' => $criteresValidationFormulaires
            ]);
            $arrayFormParticipantsUniteEEDF[$uniteParticipante->getEquipe()->getId()] = $form;
        }

        /** @var StructureRegroupementSejour $structureRegroupee */
        foreach ($sejour->getStructuresRegroupees() as $structureRegroupee) {
            if ($structureRegroupee->getEstEEDF()) {
                /** @var ParticipationSejourUnite $uniteParticipante */
                foreach ($structureRegroupee->getUnitesParticipantes() as $uniteParticipante) {
                    $form = $formFactory->createNamed('participants_sejour_' . $uniteParticipante->getEquipe()->getId(), ParticipationParticipantsSejourUniteType::class, $uniteParticipante, [
                        'equipe' => $uniteParticipante->getEquipe(),
                        'sejour' => $sejour,
                        'validation_groups' => $criteresValidationFormulaires
                    ]);

                    $arrayFormParticipantsUniteEEDF[$uniteParticipante->getEquipe()->getId()] = $form;
                }
            } else {
                $form = $formFactory->createNamed('participants_sejour_structure_non_eedf_' . $structureRegroupee->getId(), EffectifStructureRegroupementNonEEDFType::class, $structureRegroupee, []);

                $arrayFormParticipantsStructureNonEEDF[$structureRegroupee->getId()] = $form;
            }
        }

        //Gestion des formulaires des unités EEDF
        foreach ($arrayFormParticipantsUniteEEDF as $formParticipantsUniteEEDF) {
            $formParticipantsUniteEEDF->handleRequest($request);
            if ($formParticipantsUniteEEDF->isSubmitted()) {
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                    $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
                } else {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                    $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
                }
                if ($formParticipantsUniteEEDF->isValid()) {
                    $sejour->setDateModification(new DateTime());
                    try {
                        $manager->persist($sejour);
                        $manager->flush();
                        $enregistrementOk = false;
                        $this->addFlash('success_effectifs_participants', "Modifications des participant·e·s de l'unité enregistrées avec succès");
                    } catch (Exception $e) {
                        $enregistrementOk = false;
                        $this->addFlash('danger_effectifs_participants', "Erreur lors de l'enregistrement des participant·e·s: " . $e->getMessage());
                    }
                } else {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_participants', "Données incorrectes, veuillez corriger");
                }
            }
        }

        //Gestion des formulaires des structures non EEDF
        foreach ($arrayFormParticipantsStructureNonEEDF as $formParticipantStructureNonEEDF) {
            $formParticipantStructureNonEEDF->handleRequest($request);
            if ($formParticipantStructureNonEEDF->isSubmitted()) {
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                    $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
                } else {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                    $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
                }
                if ($formParticipantStructureNonEEDF->isValid()) {
                    $sejour->setDateModification(new DateTime());
                    try {
                        $manager->persist($sejour);
                        $manager->flush();

                        $this->addFlash('success_effectifs_participants', "Modifications des participant·e·s de la structure enregistrées avec succès");
                    } catch (Exception $e) {
                        $enregistrementOk = false;
                        $this->addFlash('danger_effectifs_participants', "Erreur lors de l'enregistrement des participant·e·s: " . $e->getMessage());
                    }
                } else {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_participants', "Données incorrectes, veuillez corriger");
                }
            }
        }

        //Gestions des particpants à besoins spécifiques
        $form_besoins_specifiques_sejour = $this->createForm(
            AspectEffectifsBesoinsSpecifiquesType::class,
            $sejour,
            [
                'validation_groups' => $criteresValidationFormulaires
            ]
        );
        $form_besoins_specifiques_sejour->handleRequest($request);
        if ($form_besoins_specifiques_sejour->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_EFFECTIFS);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_EFFECTIFS);
            }
            if ($form_besoins_specifiques_sejour->isValid()) {
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_effectifs_participants', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_effectifs_participants', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $enregistrementOk = false;
                $this->addFlash('danger_effectifs_participants', "Données incorrectes, veuillez corriger");
            }
        }

        $resumeEffectifs = $resumeEffectifsHelper->getResumeEffectifsSejour($sejour);


        $arrayFormViewEncadrant = [];
        /** @var RoleEncadrantSejour $encadrant */
        foreach ($arrayFormEncadrant as $id_encadrant => $formEncadrant) {
            $arrayFormViewEncadrant[$id_encadrant]
                = $formEncadrant->createView();
        }

        $arrayFormViewParticipantsParUniteEEDF = [];
        foreach ($arrayFormParticipantsUniteEEDF as $unite => $formParticipantsUniteEEDF) {
            $arrayFormViewParticipantsParUniteEEDF[$unite]
                = $formParticipantsUniteEEDF->createView();
        }

        $arrayFormViewParticipantsParStructureNonEEDF = [];
        foreach ($arrayFormParticipantsStructureNonEEDF as $structureRegroupee => $formParticipantsStructureNonEEDF) {
            $arrayFormViewParticipantsParStructureNonEEDF[$structureRegroupee]
                = $formParticipantsStructureNonEEDF->createView();
        }
        //Effectifs
        $parametresVue['form_nouvel_encadrant'] = $form_nouvel_encadrant->createView();
        $parametresVue['resumeEffectifs'] = $resumeEffectifs;
        $parametresVue['arrayFormViewEncadrant'] = $arrayFormViewEncadrant;
        $parametresVue['form_qualification_direction_sejour'] = $form_qualification_direction_sejour->createView();
        $parametresVue['arrayFormViewParticipantsParUniteEEDF'] = $arrayFormViewParticipantsParUniteEEDF;
        $parametresVue['arrayFormViewParticipantsParStructureNonEEDF'] = $arrayFormViewParticipantsParStructureNonEEDF;
        $parametresVue['form_besoins_specifiques_sejour'] = $form_besoins_specifiques_sejour->createView();
        $parametresVue['docEquivalenceAnimationDirection'] = $docEquivalenceAnimationDirection;
        $parametresVue['docEquivalencePSC1'] = $docEquivalencePSC1;
        $parametresVue['docFormulaireConsentement'] = $docFormulaireConsentement;


        return [
            'parametresVue' => $parametresVue,
            'enregistrementOk' => $enregistrementOk
        ];
    }



    /**
     * Permet de gérer les formulaires concernant les dates et lieux d'un séjour
     * Notamment:
     * - les dates du séjour
     * - le(s) lieu(x) du séjour
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources stockées en BDD
     * @param  Séjour $sejour le séjour concerné
     * @param  Request $request la requête HTTP
     * @param  EntityManagerInterface $manager
     * @param  array $criteresValidationFormulaires la liste des critères de validation des formulaires
     * @return array contenant les paramètres de la vue, et si les données renseignées ont été enregistrées ou non
     * @uses DatesLieuSejourDatesType formulaire concernant les dates du sejour
     * @uses DatesLieuSejourLieuType formulaires concernant les lieux du sejour
     * @used-by SejourController::gestionConstructionSejour() route pour effectuer la construction du séjour
     * @used-by SejourController::gestionClotureSejour() route pour effectuer la clôture du séjour
     */
    public function gestionFormulairesDatesLieuxSejour(DocumentRessourceRepository $documentRessourceRepository, Sejour $sejour,  Request $request, EntityManagerInterface $manager, array $criteresValidationFormulaires): array
    {
        $enregistrementOk = true;
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);

        $docConventionLieu = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CONVENTION_LIEU]);
        $form_dates_sejour = $this->createForm(DatesLieuSejourDatesType::class, $sejour, [
            'sejour' => $sejour,
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_dates_sejour->handleRequest($request);
        if ($form_dates_sejour->isSubmitted()) {

            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_DATES_LIEUX);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_DATES_LIEUX);
            }

            if ($form_dates_sejour->isValid()) {
                //---------------------------------------------
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_dates_lieux', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_dates_lieux', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $enregistrementOk = false;
                $this->addFlash('danger_dates_lieux', "Données incorrectes, veuillez corriger");
            }
        }

        //On crée un lieu pour chaque utilisation pour laquelle le lieu n'est pas défini, en initialisant les dates de départ et d'arrivée avec celle du séjour, et le nom avec la zone envisagée (suivie de "A préciser")
        foreach ($sejour->getUtilisationsLieuSejour() as $utilisationLieu) {
            if (!$utilisationLieu->getEstDefini()) {
                $lieuSejour = new LieuSejour();

                $utilisationLieu->setDateArrivee($sejour->getDateDebut());
                $utilisationLieu->setDateDepart($sejour->getDateFin());
                $utilisationLieu->setEstDefini(true);
                $lieuSejour->setNom($utilisationLieu->getZoneEnvisagee() . " (à préciser)");
                $lieuSejour->setEstEEDF(false);
                $utilisationLieu->setLieu($lieuSejour);
            }
        }

        $form_lieux_sejour = $this->createForm(DatesLieuSejourLieuType::class, $sejour, [
            'sejour' => $sejour,
            'validation_groups' => $criteresValidationFormulaires
        ]);
        $form_lieux_sejour->handleRequest($request);
        if ($form_lieux_sejour->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_DATES_LIEUX);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_DATES_LIEUX);
            }
            if ($form_lieux_sejour->isValid()) {

                //Si le séjour n'est pas itinérant, les dates d'arrivée et de départ sont forcément celles du séjour, et on supprime les lieux éventuels pour n'en garder qu'un
                if (!$sejour->getSejourItinerant()) {
                    $premierLieuTraite = false;
                    foreach ($sejour->getUtilisationsLieuSejour() as $utilisationLieu) {
                        if ($premierLieuTraite) {
                            $sejour->removeUtilisationsLieuSejour($utilisationLieu);
                            $manager->remove($utilisationLieu);
                        } else {
                            $premierLieuTraite = true;
                            $utilisationLieu->setDateArrivee($sejour->getDateDebut());
                            $utilisationLieu->setDateDepart($sejour->getDateFin());
                            $manager->persist($utilisationLieu);
                        }
                    }
                    $manager->persist($sejour);
                    $manager->flush();
                }
                //---------------------------------------------
                $sejour->setDateModification(new DateTime());
                try {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_dates_lieux', "Modifications enregistrées avec succès");
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_dates_lieux', "Erreur lors de la modification du séjour: " . $e->getMessage());
                }
            } else {
                $enregistrementOk = false;
                $this->addFlash('danger_dates_lieux', "Données incorrectes, veuillez corriger");
            }
        }

        //Dates et lieux
        $parametresVue['form_dates_lieux_dates_sejour'] = $form_dates_sejour->createView();
        $parametresVue['form_dates_lieux_lieux_sejour'] = $form_lieux_sejour->createView();
        $parametresVue['docConventionLieu'] = $docConventionLieu;

        return [
            'parametresVue' => $parametresVue,
            'enregistrementOk' => $enregistrementOk
        ];
    }
    /**
     * Permet de gérer les formulaires concernant les Informations générales d'un séjour
     * Notamment:
     *  - Les informations de déclaration et de structure (titre, personne accompagnante, etc…)
     *  - Le·la direct·eur·rice
     *  - Les structures et unités participantes
     *  - Les modalités de séjour (itinérance, international, etc…)
     * Lors d'un changement de direct·eur·rice, une notification est envoyée, et les données de qualification de la direction sont réinitialisées
     * Lorsqu'une structure EEDF de regroupement est ajoutée ou supprimée, une notifica tion est envoyée aux personnes concernées
     * Lorsque le séjour passe devient à dimension internationale, ou arrête de l'être, un notification est envoyée
     * Lorsque le séjour cesse d'être itinérant, seul le 1er lieu de séjour renseigné est conservé, et les dates d'arrive et de départ sur ce lieu sont positionnées aux date de début et de fin de séjour
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources stockées en BDD
     * @param  Séjour $sejour le séjour concerné
     * @param  Request $request la requête HTTP
     * @param  EntityManagerInterface $manager
     * @param  array $criteresValidationFormulaires la liste des critères de validation des formulaires
     * @param  NotificationHelper $notificationHelper helper pour l'envoi des notifications
     * @param  ValidatorInterface $validatorInterface utilitaire pour valider les 
     * @return array contenant les paramètres de la vue, et si les données renseignées ont été enregistrées ou non
     * @uses InformationsGeneralesSejourType le formulaire des informations générales du séjour
     * @uses DirectionSejourType le formulaire permettant de désigner la personne en charge de la direction du séjour
     * @uses UnitesParticipantesSejourType permettant de préciser les structures et unités participantes
     * @uses ModalitesSejourType formlaire permettant de préciser les modalités de séjour
     * @used-by SejourController::gestionConstructionSejour() route pour effectuer la construction du séjour
     * @used-by SejourController::gestionClotureSejour() route pour effectuer la clôture du séjour
     */
    public function gestionFormulairesInformationsGeneralesSejour(DocumentRessourceRepository $documentRessourceRepository, Sejour $sejour,  Request $request, EntityManagerInterface $manager, array $criteresValidationFormulaires, NotificationHelper $notificationHelper, ValidatorInterface $validatorInterface): array
    {
        $enregistrementOk = true;
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);

        $parametresVue = array();
        //Gestion Informations Générales
        $docTypoSejour = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_TYPO_SEJOUR]);
        $docCahierDesChargesColosApprenantes = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_CAHIER_DES_CHARGES_COLOS_APPRENANTES]);
        $docCampsDeBase = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_DESC_CAMPS_BASE]);
        $docCampsAccompagnes = $documentRessourceRepository->findOneBy(['nom' => DocumentRessource::DOC_DESC_CAMPS_ACCOMPAGNE]);

        $directeur_actuel = $sejour->getDirecteur();
        $est_international_actuellement = $sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger();

        //INFOS GENERALES
        $form_infos_generales_sejour = $this->createForm(
            InformationsGeneralesSejourType::class,
            $sejour,
            [
                'sejour' => $sejour,
                'validation_groups' => $criteresValidationFormulaires,
            ]
        );
        $form_infos_generales_sejour->handleRequest($request);

        if ($form_infos_generales_sejour->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_INFOS_GENERALES);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_INFOS_GENERALES);
            }
            //if($form_construction_infos_sejour->isValid())
            //{
            $sejour->setDateModification(new DateTime());
            try {


                if (
                    count(
                        $validatorInterface->validate($sejour, null, $criteresValidationFormulaires)
                    )
                    == 0
                ) {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_infos', "Modifications des informations générales du séjour enregistrées avec succès");
                } else {
                    $this->addFlash('danger_infos', "Données des informations générales du séjour incorrectes, veuillez corriger");
                    $enregistrementOk = false;
                }
            } catch (Exception $e) {
                $enregistrementOk = false;
                $this->addFlash('danger_infos', "Erreur lors de la modification des informations générales du séjour: " . $e->getMessage());
            }
        }

        //DIRECTION
        if ($this->isGranted(SejourVoter::SEJOUR_ORGANISER, $sejour)) {

            $form_direction_sejour = $this->createForm(
                DirectionSejourType::class,
                $sejour,
                [
                    'sejour' => $sejour,
                    'validation_groups' => $criteresValidationFormulaires,
                ]
            );
            $form_direction_sejour->handleRequest($request);

            if ($form_direction_sejour->isSubmitted()) {
                if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                    $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_INFOS_GENERALES);
                } else {
                    $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                    $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_INFOS_GENERALES);
                }
                //if($form_construction_infos_sejour->isValid())
                //{
                $sejour->setDateModification(new DateTime());
                try {

                    //--------------------------------------------------------------------
                    //On envoie une notification s'il y a eu changement de directeur, et on supprime les infos de diplomes
                    if ($sejour->getDirecteur() !== $directeur_actuel) {
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_006, [
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $directeur_actuel,
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
                        ]);
                        if ($sejour->getQualificationDirection()) {
                            $sejour->getQualificationDirection()->setDiplomesDirection(null);
                            $sejour->getQualificationDirection()->setEquivalenceDiplomeDirection(null);
                            $sejour->getQualificationDirection()->setQualiteDirection(null);
                            $sejour->getQualificationDirection()->setPermisB(null);
                        }
                    }

                    if (
                        count(
                            $validatorInterface->validate($sejour, null, $criteresValidationFormulaires)
                        )
                        == 0
                    ) {
                        $manager->persist($sejour);
                        $manager->flush();

                        $this->addFlash('success_infos', "Modifications de la direction du séjour enregistrées avec succès");
                    } else {
                        $enregistrementOk = false;
                        $this->addFlash('danger_infos', "Données de la direction du séjour incorrectes, veuillez corriger");
                    }
                } catch (Exception $e) {
                    $enregistrementOk = false;
                    $this->addFlash('danger_infos', "Erreur lors de la modification de la direction du séjour: " . $e->getMessage());
                }
                //}
            }
            $parametresVue['form_direction_sejour'] = $form_direction_sejour->createView();
        }

        //UNITES PARTICIPANTES
        $form_unites_participantes_sejour = $this->createForm(
            UnitesParticipantesSejourType::class,
            $sejour,
            [
                'sejour' => $sejour,
                'validation_groups' => $criteresValidationFormulaires,
            ]
        );
        $form_unites_participantes_sejour->handleRequest($request);

        if ($form_unites_participantes_sejour->isSubmitted()) {
            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_INFOS_GENERALES);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_INFOS_GENERALES);
            }
            //if($form_construction_infos_sejour->isValid())
            //{
            $sejour->setDateModification(new DateTime());
            try {
                /* -------------------------------------------------------------------- */
                //Gestion des unités participantes de la structure organisatrice
                $arrayUnitesParticipantes = [];
                /** @var ParticipationSejourUnite $uniteParticîpante */
                foreach ($sejour->getUnitesStructureOrganisatrice() as $uniteParticîpante) {
                    $arrayUnitesParticipantes[$uniteParticîpante->getEquipe()->getId()] = $uniteParticîpante;
                }
                foreach ($form_unites_participantes_sejour->get('unites')->getData() as $unite) {
                    if (!array_key_exists($unite->getId(), $arrayUnitesParticipantes)) {
                        $uniteParticipante = new ParticipationSejourUnite();
                        $uniteParticipante->setEquipe($unite);
                        $sejour->addUnitesStructureOrganisatrice($uniteParticipante);
                    } else {
                        unset($arrayUnitesParticipantes[$unite->getId()]);
                    }
                }
                //On supprime les unités participantes restantes
                foreach ($arrayUnitesParticipantes as $uniteParticpanteASupprimer) {
                    $sejour->removeUnitesStructureOrganisatrice($uniteParticpanteASupprimer);
                    $manager->remove($uniteParticpanteASupprimer);
                }


                /* -------------------------------------------------------------------- */
                //Gestion des structuresRegroupees

                $arrayStructuresRegroupees = [];
                /** @var Sejour $sejour */
                /** @var StructureRegroupementSejour $structure_regroupee */
                foreach ($sejour->getStructuresRegroupees() as $structure_regroupee) {
                    if ($structure_regroupee->getEstEEDF()) {
                        $arrayStructuresRegroupees[$structure_regroupee->getStructureEEDF()->getId()] = $structure_regroupee;
                    }
                }
                //On ajoute les structure regroupées nouvelles et on envoie une notification
                /** @var StructureRegroupementSejour $structure_regroupee_form */
                foreach ($form_unites_participantes_sejour->get('structuresRegroupees')->getData() as $structure_regroupee_form) {
                    if ($structure_regroupee_form->getEstEEDF()) {
                        if (!array_key_exists($structure_regroupee_form->getStructureEEDF()->getId(), $arrayStructuresRegroupees)) {
                            $sejour->addStructuresRegroupee($structure_regroupee_form);
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_007, [
                                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_regroupee_form->getStructureEEDF()
                            ]);
                        } else {
                            unset($arrayStructuresRegroupees[$structure_regroupee_form->getStructureEEDF()->getId()]);
                        }
                    }
                }
                //On supprime les structure regroupées restantes et on envoie une notification
                /** @var StructureRegroupementSejour $structureRegroupeeASupprimer */
                foreach ($arrayStructuresRegroupees as $structureRegroupeeASupprimer) {
                    $sejour->removeStructuresRegroupee($structureRegroupeeASupprimer);
                    $manager->remove($structureRegroupeeASupprimer);
                    foreach ($structureRegroupeeASupprimer->getStructureEEDF()->getOrganisateurs() as $organisateur) {
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_007bis, [
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $organisateur,
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structureRegroupeeASupprimer->getStructureEEDF()
                        ]);
                    }
                }
                //On crée/supprime les structures participant au séjour
                /** @var StructureRegroupementSejour $structureRegroupee */
                foreach ($sejour->getStructuresRegroupees() as $structureRegroupee) {


                    $arrayUnitesParticipantes = [];
                    /** @var ParticipationSejourUnite $uniteParticîpante */
                    foreach ($structureRegroupee->getUnitesParticipantes() as $uniteParticîpante) {
                        $arrayUnitesParticipantes[$uniteParticîpante->getEquipe()->getId()] = $uniteParticîpante;
                    }
                    foreach ($structureRegroupee->getUnites() as $unite) {
                        if (!array_key_exists($unite->getId(), $arrayUnitesParticipantes)) {
                            $uniteParticipante = new ParticipationSejourUnite();
                            $uniteParticipante->setEquipe($unite);
                            $structureRegroupee->addUnitesParticipante($uniteParticipante);
                        } else {
                            unset($arrayUnitesParticipantes[$unite->getId()]);
                        }
                    }
                    //On supprime les unités participantes restantes
                    foreach ($arrayUnitesParticipantes as $uniteParticpanteASupprimer) {
                        $structureRegroupee->removeUnitesParticipante($uniteParticpanteASupprimer);
                        $manager->remove($uniteParticpanteASupprimer);
                    }
                }
                $errors = $validatorInterface->validate($sejour, null, $criteresValidationFormulaires);
                if (count($errors) == 0) {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_infos', "Modifications des unités participantes enregistrées avec succès");
                } else {

                    $enregistrementOk = false;
                    $message_erreur = "<p>
                        Données des unités participantes incorrectes, veuillez corriger:
                        <ul>";
                    // there are errors, now you can show them
                    foreach ($errors as $violation) {
                        $message_erreur .= '<li>';
                        $message_erreur .= $violation->getMessage();
                        $message_erreur .= '</li>';
                    }
                    $message_erreur .= '</ul></p>';
                    $this->addFlash('danger_infos', $message_erreur);
                }
            } catch (Exception $e) {
                $enregistrementOk = false;
                $this->addFlash('danger_infos', "Erreur lors de la modification des unités participantes du séjour: " . $e->getMessage());
            }
        }

        //MODALITES DE SEJOUR
        $form_modalites_sejour = $this->createForm(
            ModalitesSejourType::class,
            $sejour,
            [
                'sejour' => $sejour,
                'validation_groups' => $criteresValidationFormulaires,
            ]
        );
        $form_modalites_sejour->handleRequest($request);

        if ($form_modalites_sejour->isSubmitted()) {
            //Si le séjour n'est pas itinérant, les dates d'arrivée et de départ sont forcément celles du séjour, et on supprime les lieux éventuels pour n'en garder qu'un
            if (!$sejour->getSejourItinerant()) {
                $premierLieuTraite = false;
                foreach ($sejour->getUtilisationsLieuSejour() as $lieu) {
                    if ($premierLieuTraite) {
                        $sejour->removeUtilisationsLieuSejour($lieu);
                        $manager->remove($lieu);
                    } else {
                        $premierLieuTraite = true;
                        $lieu->setDateArrivee($sejour->getDateDebut());
                        $lieu->setDateDepart($sejour->getDateFin());
                        $manager->persist($lieu);
                    }
                }
                $manager->persist($sejour);
                $manager->flush();
            }

            if ($statusSejour->has(Sejour::STATUT_SEJOUR_TERMINE)) {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CLOTURE);
                $sejour->getPanelActif()->setPanelActifCloture(PanelActifSejour::PANEL_CLOTURE_INFOS_GENERALES);
            } else {
                $sejour->getPanelActif()->setPanelActifGeneral(PanelActifSejour::PANEL_GENERAL_CONSTRUCTION);
                $sejour->getPanelActif()->setPanelActifConstruction(PanelActifSejour::PANEL_CONSTRUCTION_INFOS_GENERALES);
            }
            //if($form_construction_infos_sejour->isValid())
            //{
            $sejour->setDateModification(new DateTime());
            try {
                //--------------------------------------------------------------------
                //On envoie une notification s'il y a eu changement international 

                $est_international = $sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger();
                if (!$est_international && $est_international_actuellement) {
                    $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_069, [
                        NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    ]);
                }
                if ($est_international && !$est_international_actuellement) {
                    $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_068, [
                        NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,

                    ]);
                }




                if (
                    count(
                        $validatorInterface->validate($sejour, null, $criteresValidationFormulaires)
                    )
                    == 0
                ) {
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_infos', "Modifications des modalités du séjour enregistrées avec succès");
                } else {
                    $enregistrementOk = false;
                    $this->addFlash('danger_infos', "Données des modalités du séjour incorrectes, veuillez corriger");
                }
            } catch (Exception $e) {
                $enregistrementOk = false;
                $this->addFlash('danger_infos', "Erreur lors de la modification des modalités du séjour: " . $e->getMessage());
            }
        }

        $parametresVue['form_infos_generales_sejour'] = $form_infos_generales_sejour->createView();

        $parametresVue['form_unites_participantes_sejour'] = $form_unites_participantes_sejour->createView();
        $parametresVue['form_modalites_sejour'] = $form_modalites_sejour->createView();
        $parametresVue['docTypoSejour'] = $docTypoSejour;
        $parametresVue['docCahierDesChargesColosApprenantes'] = $docCahierDesChargesColosApprenantes;
        $parametresVue['docCampsDeBase'] = $docCampsDeBase;
        $parametresVue['docCampsAccompagnes'] = $docCampsAccompagnes;
        return [
            'parametresVue' => $parametresVue,
            'enregistrementOk' => $enregistrementOk
        ];
    }

    /**
     * Permet de gérer la saisie des commentaires d'un séjour et de leur saisie par sujet traité
     *
     * Parcours la liste de tous les sujet de commentaire, et gère la saisie d'un commentaire pour chacun d'entre eux
     * Lors de la saisie d'un commentaire sur un séjour, les act·eur·rice·s du séjour en sont notifié·e·s
     * @param  Request $request la requête HTTP
     * @param  EntityManagerInterface $manager
     * @param  Sejour $sejour le séjour concerné
     * @param  NotificationHelper $notificationHelper helper pour l'envoi des notifications
     * @return array le tableau des formulaires de commentaires à afficher
     * @uses CommentaireType formulaire permettant de gérer les commentaires d'un sujet du séjour
     */
    public function gestionCommentaires(Request $request, EntityManagerInterface $manager, Sejour $sejour, NotificationHelper $notificationHelper, FormFactoryInterface $formFactory): array
    {
        $arrayFormCommentaireParSujet = [];
        /** @var Personne $personne */
        $personne = $this->getUser();
        foreach (Commentaire::LISTE_SUJETS_COMMENTAIRES as $sujetCommentaire) {

            $commentaire = new Commentaire();
            $form = $formFactory->createNamed('commentaire_' . $sujetCommentaire, CommentaireType::class, $commentaire, []);


            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                $commentaire->setSujet($sujetCommentaire);
                $sejour->getPanelActif()->setPanelActifEnFonctionDuCommentaire($commentaire);


                if ($form->isValid()) {
                    $commentaire->setDate(new DateTime());

                    $commentaire->setPersonne($personne);
                    $commentaire->setSejour($sejour);
                    try {
                        $manager->persist($commentaire);
                        $manager->flush();
                        $this->addFlash('success_commentaire', "Commentaire enregistré");

                        //Envoi de la notification
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_031, [
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_COMMENTAIRE => $commentaire,
                        ]);
                    } catch (Exception $e) {
                        $this->addFlash('danger_commentaire', "Erreur lors de l'enregistrement du commentaire: " . $e->getMessage());
                    }
                } else {
                    $this->addFlash('danger_commentaire', "Données incorrectes, veuillez corriger");
                }
            }

            $arrayFormCommentaireParSujet[$sujetCommentaire] = $form->createView();
        }
        return $arrayFormCommentaireParSujet;
    }
}
