<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\Sejour;
use App\Form\InfosPersoProfilType;
use App\Form\ModificationMotDePassePersonneType;
use App\Form\ParametresNotificationProfilType;
use App\Service\NotificationHelper;
use App\Service\RolesPersonneHelper;
use App\Utils\NotificationConstants;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour gérer le profil de l'utilisa·eur·rice
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class ProfilUtilisateurController extends AbstractController
{
    /**
     * Affichage du profil de l'utilisat·eur·rice:
     *  - Modification de coordonnées de la personne à contacter en cas d'urgence
     *  - Affichage des rôles de l'utilisat·eur·rice
     *  - Changement de mot de passe
     *  - Modification des paramètres de notification
     *
     * @param  Request $request
     * @param  EntityManagerInterface $manager
     * @param  RolesPersonneHelper $rolesPersonneHelper
     * @return Response
     * @uses InfosPersoProfilType formulaire pour la personne à contacter en cas d'urgence
     * @uses ModificationMotDePassePersonneType formulaire de changement de mot de passe
     * @uses ParametresNotificationProfilType formulaire pour la modification des paramètres de notifications
     */
    #[Route('/profil/utilisateur', name: 'profil_utilisateur')]
    public function profilPersonne(Request $request, EntityManagerInterface $manager, RolesPersonneHelper $rolesPersonneHelper): Response
    {
        /** @var Personne $user */
        $user = $this->getUser();

        //Coordonnées personnelles modifiables
        $form_user_coordonnees = $this->createForm(InfosPersoProfilType::class, $user);

        $form_user_coordonnees->handleRequest($request);
        if ($form_user_coordonnees->isSubmitted() && $form_user_coordonnees->isValid()) {

            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success_coordonnees', "Modifications enregistrées avec succès");
        }

        //Roles utilisateur:
        $roles_metier_personne = $rolesPersonneHelper->rolesPersonne($user);

        //Modification des parametres de notification
        $formParametresNotification = $this->createForm(ParametresNotificationProfilType::class, $user);
        $formParametresNotification->handleRequest($request);
        if ($formParametresNotification->isSubmitted() && $formParametresNotification->isValid()) {
            try {
                $manager->persist($user);
                $manager->flush();
                $this->addFlash('success_param_notif', "Modification de vos paramètres de notification enregistrée avec succès");
            } catch (Exception $e) {
                $this->addFlash('danger_param_notif', "Problème lors de la modification de vos paramètres de notification: " . $e->getMessage());
            }
        }

        return $this->render('profil_utilisateur/profil.html.twig', [
            'form_user_coordonnees' => $form_user_coordonnees->createView(),
            'formParametresNotification' => $formParametresNotification->createView(),
            'user' => $user,
            'roles_metier_personne' => $roles_metier_personne
        ]);
    }

    /**
     * Permet de désincrire la personne de ROADS:
     *  - Suppression du mot de passe
     *  - Réinitialisation des paramètres de notification
     *  - Suppression des données de la personne à contacter en cas d'urgence
     *  - Suppression de toutes ses notifications
     *  - Suppression des rôles de la personne:
     *      - Si la personne etait direct·eur·rice d'un séjour, on positionne l'organisat·eur·rice en direction sur ce séjour, et on envoie une notification,
     *      - Si la personne était organisat·eur·rice d'une structure, on envoie une notification
     *      - Si la personne était validat·eur·rice d'une structure, on envoie une notification
     *      - Si la personne était direct·eur·rice adjoint·e d'un séjour, on envoie une notification
     *      - Si la personne était encadrant·e d'un séjour, on envoie une notification
     *
     * @param  NotificationHelper $notificationHelper Helper pour l'envoi de notifications
     * @param  EntityManagerInterface $manager
     * @return Response On redirige vers la page d'acceuil non connectée.
     */
    #[Route('/desinscription', name: 'desinscription_personne')]
    public function desinscriptionPersonne(NotificationHelper $notificationHelper, EntityManagerInterface $manager): Response
    {
        /**  @var Personne $personne */
        $personne = $this->getUser();

        //On supprime les rôles de suivi
        $personne->setDateDerniereConnexion(null);
        $personne->setFrequenceNotification(Personne::FREQUENCE_NOTIFICATION_QUOTIDIEN);
        $personne->setNomUrgence(null);
        $personne->setPrenomUrgence(null);
        $personne->setTelUrgence(null);
        $personne->setLienUrgence(null);

        $personne->setRoles([]);

        foreach ($personne->getFonctions() as $fonction) {
            $manager->remove($fonction);
        }

        foreach ($personne->getStructuresOrganisation() as $structureOrganisation) {
            $personne->removeStructuresOrganisation($structureOrganisation);
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_061, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structureOrganisation
            ]);
        }
        foreach ($personne->getStructuresValidation() as $structureValidation) {
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_061, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structureValidation
            ]);
            $personne->removeStructuresValidation($structureValidation);
        }
        foreach ($personne->getStructuresSuivi() as $structureSuivi) {
            $personne->removeStructuresSuivi($structureSuivi);
        }
        /**  @var Sejour $sejourDirection */
        foreach ($personne->getSejoursDirection() as $sejourDirection) {
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_060, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourDirection
            ]);

            $sejourDirection->setDirecteur($sejourDirection->getStructureOrganisatrice()->getOrganisateurs()->toArray()[0]);
            $manager->persist($sejourDirection);
        }
        /**  @var Sejour $sejourDirectionAdjointe */
        foreach ($personne->getSejoursDirectionAdjointe() as $sejourDirectionAdjointe) {
            $personne->removeSejoursDirectionAdjointe($sejourDirectionAdjointe);
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_059, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourDirectionAdjointe
            ]);
        }
        /**  @var Sejour $sejourEncadrement */
        foreach ($personne->getSejoursEncadrement() as $sejourEncadrement) {
            $personne->removeSejoursEncadrement($sejourEncadrement);
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_059, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejourEncadrement
            ]);
        }
        /**  @var Sejour $sejourSuivi */
        foreach ($personne->getSejoursSuivis() as $sejourSuivi) {
            $personne->removeSejoursSuivi($sejourSuivi);
        }
        /**  @var Sejour $sejourValidation */
        foreach ($personne->getSejoursValidation() as $sejourValidation) {
            $personne->removeSejoursValidation($sejourValidation);
        }
        /**  @var Sejour $sejourAccompagnement */
        foreach ($personne->getSejoursAccompagnement() as $sejourAccompagnement) {
            $personne->removeSejoursAccompagnement($sejourAccompagnement);
        }
        foreach ($personne->getNotifications() as $notification) {
            $manager->remove($notification);
        }
        $manager->persist($personne);
        $manager->flush();
        $this->addFlash('success', "Votre désinscription a bien été prise en compte");

        return $this->redirectToRoute('app_logout');
    }
}
