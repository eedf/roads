<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Service\TableauDeBordSejourHelper;
use App\Utils\ResumeSejours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour afficher le tableau de bord des séjours concernant l'utilisat·eur·rice
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class TableauDeBordController extends AbstractController
{
    /**
     * Route pour afficher les séjours concernant l'utilisat·eur·ice connecté·e
     * @param  TableauDeBordSejourHelper $tableauDeBordSejourHelper helper pour sélectionner les séjours à afficher
     * @return Response
     */
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/tableau/bord/sejours', name: 'tableau_de_bord')]
    public function tableauDeBordSejours(TableauDeBordSejourHelper $tableauDeBordSejourHelper): Response
    {

        /**  @var Personne $personne */
        $personne = $this->getUser();
        $donneesSejours = $tableauDeBordSejourHelper->getTableauDeBordSejour($personne);


        return $this->render('tableau_de_bord/index.html.twig', [
            'donneesSejours' => $donneesSejours,
        ]);
    }
}
