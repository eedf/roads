<?php

namespace App\Controller;

use Drenso\OidcBundle\OidcClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'app_login')]
    #[IsGranted('PUBLIC_ACCESS')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->redirectToRoute('jeito_connect');
    }

    #[Route('/connect/jeito', name: 'jeito_connect')]
    #[IsGranted('PUBLIC_ACCESS')]
    public function connect(OidcClientInterface $oidcClient): RedirectResponse
    {
        // Redirect to authorization @ OIDC provider
        return $oidcClient->generateAuthorizationRedirect(null, ['openid', 'jeito']);
    }

    #[Route('/oauth/check/jeito', name: 'oauth_check')]
    public function oauthCheck(AuthenticationUtils $authenticationUtils)
    {

        if ($this->getUser()) {
            $this->addFlash("success", "Vous êtes connecté·e dans ROADS");
            return $this->redirectToRoute('home');
        }
        $this->addFlash("error", '
        <p>Cet·te utilisat·eur·rice Jéito n\'existe pas dans R.O.A.D.S. Veuillez rééssayer. Si le problème persiste, assurez-vous de ne pas être connécté·e sur un autre compte depuis Jéito.
        <br><a class="btn btn-danger" href="' . $this->generateUrl('app_logout') . '">Déconnexion</a></p>');
        return $this->redirectToRoute('accueil_non_connecte');
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
