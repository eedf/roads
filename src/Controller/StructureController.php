<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;
use App\Entity\ParticipationSejourUnite;
use App\Entity\Sejour;
use App\Entity\Structure;
use App\Entity\StructureRegroupementSejour;
use App\Form\AjoutRoleSuiviStructureOrgaType;
use App\Form\AjoutRoleSuiviStructureValidType;
use App\Form\AjoutRoleValidationStructureType;
use App\Form\AjoutRoleVisiteSejourType;
use App\Form\InfosStructureFormType;
use App\Form\ParticipationParticipantsSejourUniteType;
use App\Repository\SejourRepository;
use App\Repository\StructureRegroupementSejourRepository;
use App\Security\Voter\StructureVoter;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use ArrayObject;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use phpDocumentor\Reflection\Types\Nullable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Controller pour la gestion globale des de l'organisation et de validation des séjours d'une structure
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class StructureController extends AbstractController
{

    /**
     * Permet d'accepter la participation d'un structure à un séjour organisé par une autre structure
     * 
     * Une notification est envoyée aux personnes concernées, et un message de confirmation flash est affiché. De plus le(s) organisat·eur·rice(s) de la structure acceptant la demande acquièrent les droits de suivi de ce séjour, et peuvent rajouter des participant·e·s à ce séjour
     * @param  Structure $structure la structure acceptant le regroupement
     * @param  Sejour $sejour le séjour pour lequel le regroupement est accepté
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure}/regroupement/{sejour}/accepter', name: 'accepter_regroupement')]
    public function accepterDemandeRegroupement(Structure $structure, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_ORGANISER, $structure);
        try {
            //On rajoute les organisateurs de la structure en suivi du séjour
            foreach ($structure->getOrganisateurs() as $organisateur) {
                $sejour->addSuiveur($organisateur);
            }
            $manager->persist($sejour);
            /**  @var StructureRegroupementSejour $structure_regroupee */
            foreach ($sejour->getStructuresRegroupees() as $structure_regroupee) {
                if ($structure_regroupee->getStructureEEDF() === $structure) {
                    $structure_regroupee->setStatutDemande(StructureRegroupementSejour::STATUT_DEMANDE_ACCEPTE);
                    $manager->persist($structure_regroupee);
                }
            }

            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_008, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser()
            ]);

            $manager->flush();

            $this->addFlash('success_regroupement', "Regroupement de votre structure avec le séjour \"" . $sejour->getintitule() . "\" accepté, La structure organisatrice a désormais accès aux participant·e·s de votre structure, et vous avez un droit de suivi sur le séjour.");
        } catch (Exception $e) {
            $this->addFlash('danger_regroupement', "Erreur lors de l'acceptation du regoupement avec le séjour \"" . $sejour->getIntitule() . "\": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
    }

    /**
     * Permet de refuser la participation d'un structure à un séjour organisé par une autre structure
     * 
     * Une notification est envoyée aux personnes concernées, et un message de confirmation flash est affiché.
     *
     * @param  Structure $structure la structure refusant le regroupement
     * @param  Sejour $sejour le séjour pour lequel le regroupement est refusé
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure}/regroupement/{sejour}/refuser', name: 'refuser_regroupement')]
    public function refuserDemandeRegroupement(Structure $structure, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_ORGANISER, $structure);
        try {

            /**  @var StructureRegroupementSejour $structure_regroupee */
            foreach ($sejour->getStructuresRegroupees() as $structure_regroupee) {
                if ($structure_regroupee->getStructureEEDF() === $structure) {
                    $manager->remove($structure_regroupee);
                }
            }

            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_009, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser()
            ]);

            $manager->flush();

            $this->addFlash('success_regroupement', "Regroupement de votre structure avec le séjour \"" . $sejour->getintitule() . "\" refusé");
        } catch (Exception $e) {
            $this->addFlash('danger_regroupement', "Erreur lors du refus du regoupement avec le séjour \"" . $sejour->getIntitule() . "\": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
    }

    /**
     * Permet d'afficher le "tableau de bord d'une structure":
     * Pour les utilisat·eur·rice·s avec le rôle d'**organistation de séjour** sur la structure concernée:
     * - Modifier les infos de déclaration SDJES de la structure
     * - Organiser un nouveau séjour (si les informations SDJES sont renseignées)
     * - Afficher les demandes de regroupement adressées à cette structure, et gérer les paticipant·e·s de ses unités pour les séjour de regroupement auquel elles sont asociées,
     * - Visualiser les séjours en cours d'organisation pour la structure
     * - Visualiser les séjours organisés et clôturés pour la structure
     * - Visualiser les personnes ayant le rôle de suivi sur les séjours organisés pas la structure 
     * - Ajouter le rôle de suivi de séjour à une personne pour un séjour ou la structure entière (Dans ce cas une notification est envoyée aux personnes concernées)
     * Pour les utilisat·eur·rice·s avec le rôle de **validation de séjour** sur la structure concernée:
     * - Visualiser les séjours en cours d'organisation que la structure a en validation
     * - Visualiser les séjours organisés et clôturés que la structure a en validation
     * - Visualiser l'état des visites que la structure a en validation
     * - Visualiser les personnes ayant le rôle de suivi sur les séjours en validation par la structure
     * - Visualiser les personnes ayant le rôle de validation déléguée sur les séjours en validation par la structure
     * - Ajouter le rôle de validation déléguée à des séjours en validation par la structure
     * - Ajouter le rôle de visit·eur·se sur des séjours en validation par la structure
     * - Ajouter le rôle de suivi de séjour à une personne sur ces séjours, ou sur les séjours en cours et à venir d'une structure dépendante (Dans ce cas une notification est envoyée aux personnes concernées)
     *
     * @param  Request $request la reqête HTTP
     * @param  EntityManagerInterface $manager
     * @param  Structure $structure la structure concernée
     * @param  SejourRepository $sejourRepository pour accéder aux séjours concernés
     * @param  StructureRegroupementSejourRepository $structureRegroupementSejourRepository pour accéder aux séjours regroupés
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return Response
     * @uses InfosStructureFormType formulaire pour le numéro de SDJES
     * @uses AjoutRoleSuiviStructureOrgaType formulaire pour ajouter le rôle de suivi sur les séjours organisés par la structure
     * @uses AjoutRoleSuiviStructureValidType formulaire pour ajouter le rôle de suivi sur les séjours en validation par la structure
     * @uses AjoutRoleValidationStructureType formulaire pour ajouter le rôle de validation déléguée sur les séjours en validation par la structure
     * @uses AjoutRoleVisiteSejourType formulaire pour ajouter le rôle de visit·eur·se sur les séjours en validation par la structure
     */
    #[Route('/structure/{id}', name: 'gestion_structure')]
    public function gestionStructure(Request $request, EntityManagerInterface $manager, Structure $structure, StructureRegroupementSejourRepository $structureRegroupementSejourRepository, SejourRepository $sejourRepository, NotificationHelper $notificationHelper, FormFactoryInterface $formFactory): Response
    {
        if (!$this->isGranted(StructureVoter::STRUCTURE_ORGANISER, $structure) && !$this->isGranted(StructureVoter::STRUCTURE_VALIDER, $structure)) {
            throw $this->createAccessDeniedException('not allowed');
        }
        $form_ajout_suivi_orga = $this->createForm(AjoutRoleSuiviStructureOrgaType::class, null, ['structure' => $structure]);
        $form_infos_structure = $this->createForm(InfosStructureFormType::class, $structure);
        $organisation_sejour = false;
        if ($this->isGranted(StructureVoter::STRUCTURE_ORGANISER, $structure)) {

            $form_infos_structure->handleRequest($request);
            if ($form_infos_structure->isSubmitted() && $form_infos_structure->isValid()) {
                $manager->persist($structure);
                $manager->flush();
                $this->addFlash('success_infos_structure', "Modifications enregistrées avec succès");
            }


            if ($structure->getNumeroDeclarationInitiale()) {
                $organisation_sejour = true;
            } else {
                $this->addFlash('danger_organisation_sejour', "Vous ne pouvez pas organiser de séjour tant que le numéro de déclaration initiale n'est pas renseigné");
            }
            //---------------------------------------------------------
            //Gestion formulaire attribution suivi séjours (onglet "Organisation")
            $form_ajout_suivi_orga->handleRequest($request);

            if ($form_ajout_suivi_orga->isSubmitted() && $form_ajout_suivi_orga->isValid()) {

                /**  @var Personne $personne */
                $personne = $form_ajout_suivi_orga->get('personne')->getData();
                $touteLaStructure = $form_ajout_suivi_orga->get('all')->getData();

                if ($touteLaStructure) // Ajout du rôle de suivi pour toute la structure
                {
                    try {

                        if (in_array($structure, $personne->getStructuresSuivi()->toArray())) {
                            throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de suivi de tous les séjours de la structure \"" . $structure->getNom() . "\"");
                        }
                        $structure->addSuiveur($personne);
                        $manager->persist($structure);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_032bis, [
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                            NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
                        ]);
                        $this->addFlash('success_suivi', "Rôle de suivi de tous les séjours de la structure ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    } catch (Exception $e) {
                        $this->addFlash('danger_suivi', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                    }
                } else { // Ajout du rôle de suivi pour un séjour spécifique
                    try {
                        /**  @var Sejour $sejour */
                        $sejour = $form_ajout_suivi_orga->get('sejour')->getData();

                        if (in_array($sejour, $personne->getSejoursSuivis()->toArray())) {
                            throw new Exception("La personne a déjà le rôle de suivi du séjour \"" . $sejour->getIntitule() . "\"");
                        }
                        $sejour->addSuiveur($personne);
                        $manager->persist($sejour);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_032, [
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
                        ]);
                        $this->addFlash('success_suivi', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    } catch (Exception $e) {
                        $this->addFlash('danger_suivi', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                    }
                }
            }
        }
        //---------------------------------------------------------
        //Gestion formulaire délégation de validation
        $form_ajout_validateur = $this->createForm(AjoutRoleValidationStructureType::class, null, ['structure' => $structure]);

        if ($this->isGranted(StructureVoter::STRUCTURE_VALIDER, $structure)) {
            $form_ajout_validateur->handleRequest($request);

            if ($form_ajout_validateur->isSubmitted() && $form_ajout_validateur->isValid()) {

                /**  @var Personne $personne */
                $personne = $form_ajout_validateur->get('personne')->getData();
                /**  @var Structure $structure_validation */
                $structure_validation = $form_ajout_validateur->get('structure')->getData();
                $touteLaStructure = $form_ajout_validateur->get('all')->getData();

                if ($touteLaStructure) // Ajout du rôle de validation pour chacun des séjours en cours d'une structure
                {
                    try {

                        if (in_array($structure_validation, $personne->getStructuresValidation()->toArray())) {
                            throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de validation de tous les séjours de la structure \"" . $structure_validation->getNom() . "\"");
                        }

                        $sejours_validation = $sejourRepository->findSejoursOrganisesParStructure($structure_validation);

                        $sejours_delegues_en_validation = [];
                        foreach ($sejours_validation as $sejour) {
                            if (in_array($sejour, $personne->getSejoursValidation()->toArray())) {
                                $this->addFlash('warning_ajout_valid', "La personne a déjà le rôles de validation déléguée pour le séjour " . $sejour->getIntitule());
                            } else {
                                $sejours_delegues_en_validation[] = $sejour;
                                $sejour->addValidateur($personne);
                                $manager->persist($sejour);
                            }
                        }
                        $manager->flush();
                        if (count($sejours_delegues_en_validation) > 0) {
                            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_077, [
                                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_validation,
                                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                                NotificationConstants::LABEL_PERIMETRE_SEJOURS =>  new ArrayObject($sejours_delegues_en_validation),
                            ]);
                            $this->addFlash('success_ajout_valid', "Rôle de validation déléguée pour chacun des séjours de la structure " . $structure_validation->getNom() . " ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                        }
                    } catch (Exception $e) {
                        $this->addFlash('danger_ajout_valid', "Erreur lors de l'ajout de rôle de validation validation déléguée : " . $e->getMessage());
                    }
                } else { // Ajout du rôle de validation pour un séjour spécifique
                    try {
                        /**  @var Sejour $sejour */
                        $sejour = $form_ajout_validateur->get('sejour')->getData();

                        if (in_array($sejour, $personne->getSejoursValidation()->toArray())) {
                            throw new Exception("La personne a déjà le rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_validation->getNom() . ")");
                        }
                        $sejour->addValidateur($personne);
                        $manager->persist($sejour);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_077, [
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_validation,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                            NotificationConstants::LABEL_PERIMETRE_SEJOURS => new ArrayObject([$sejour]),
                        ]);
                        $this->addFlash('success_ajout_valid', "Rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_validation->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    } catch (Exception $e) {
                        $this->addFlash('danger_ajout_valid', "Erreur lors de l'ajout de rôle de validation : " . $e->getMessage());
                    }
                }
            }
        }

        //---------------------------------------------------------
        //Gestion formulaire attribution visites
        $form_ajout_visiteur = $this->createForm(AjoutRoleVisiteSejourType::class, null, ['structure' => $structure]);
        $form_ajout_visiteur->handleRequest($request);
        if ($form_ajout_visiteur->isSubmitted() && $form_ajout_visiteur->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_visiteur->get('personne')->getData();

            // Ajout du rôle de visite pour un séjour spécifique
            try {
                /**  @var Sejour $sejour */
                $sejour = $form_ajout_visiteur->get('sejour')->getData();

                /** @var NominationVisiteSejour $nominations_visites */
                foreach ($sejour->getNominationsVisiteSejour() as $nominations_visites) {
                    if ($nominations_visites->getVisiteur() == $personne) {
                        throw new Exception("La personne a déjà le rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ")");
                    }
                }

                $nouvelle_nomination_visite = new NominationVisiteSejour();
                $nouvelle_nomination_visite->setSejour($sejour);
                $nouvelle_nomination_visite->setVisiteur($personne);
                $nouvelle_nomination_visite->setNommePar($this->getUser());
                $manager->persist($nouvelle_nomination_visite);
                $manager->persist($sejour);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_082, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_NOMINATION_VISITE => $nouvelle_nomination_visite
                ]);
                $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
            } catch (Exception $e) {
                $this->addFlash('danger_visite', "Erreur lors de l'ajout de rôle de visite : " . $e->getMessage());
            }
        }



        //Gestion formulaire attribution suivi séjours (onglet "Validation")
        $form_ajout_suivi_valid = $this->createForm(AjoutRoleSuiviStructureValidType::class, null, ['structure' => $structure]);

        if ($this->isGranted(StructureVoter::STRUCTURE_VALIDER, $structure)) {
            $form_ajout_suivi_valid->handleRequest($request);

            if ($form_ajout_suivi_valid->isSubmitted() && $form_ajout_suivi_valid->isValid()) {

                /**  @var Personne $personne */
                $personne = $form_ajout_suivi_valid->get('personne')->getData();
                /**  @var Structure $structure_suivie */
                $structure_suivie = $form_ajout_suivi_valid->get('structure')->getData();
                $touteLaStructure = $form_ajout_suivi_valid->get('all')->getData();
                $touteLesStructuresFilles = $form_ajout_suivi_valid->get('allStructures')->getData();

                if ($touteLesStructuresFilles) // Ajout du rôle de suivi pour toutes les structures filles
                {
                    /**  @var Structure $structure_suivie */
                    foreach ($structure->getStructuresFilles() as $structure_suivie) {
                        if (($structure_suivie->getStatut() == Structure::STATUT_STRUCTURE_AUTONOME) || ($structure_suivie->getStatut() == Structure::STATUT_STRUCTURE_FERMEE)) {
                            try {

                                if (in_array($structure_suivie, $personne->getStructuresSuivi()->toArray())) {
                                    throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de suivi de tous les séjours de la structure \"" . $structure_suivie->getNom() . "\"");
                                }
                                $structure_suivie->addSuiveur($personne);

                                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_046bis, [
                                    NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_suivie,
                                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                                    NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                                    NotificationConstants::LABEL_PERIMETRE_STRUCTURE_VALIDATION => $structure,
                                ]);
                                $manager->persist($structure_suivie);
                                $this->addFlash('success_suivi_valid', "Rôle de suivi de tous les séjours de la structure " . $structure_suivie->getNom() . " ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                            } catch (Exception $e) {
                                $this->addFlash('danger_suivi_valid', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                            }
                        }
                    }

                    $manager->flush();
                } else if ($touteLaStructure) // Ajout du rôle de suivi pour toute une structure
                {
                    try {

                        if (in_array($structure_suivie, $personne->getStructuresSuivi()->toArray())) {
                            throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de suivi de tous les séjours de la structure \"" . $structure_suivie->getNom() . "\"");
                        }
                        $structure_suivie->addSuiveur($personne);
                        $manager->persist($structure_suivie);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_046bis, [
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_suivie,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE_VALIDATION => $structure,
                        ]);

                        $this->addFlash('success_suivi_valid', "Rôle de suivi de tous les séjours de la structure " . $structure_suivie->getNom() . " ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    } catch (Exception $e) {
                        $this->addFlash('danger_suivi_valid', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                    }
                } else { // Ajout du rôle de suivi pour un séjour spécifique
                    try {
                        /**  @var Sejour $sejour */
                        $sejour = $form_ajout_suivi_valid->get('sejour')->getData();

                        if (in_array($sejour, $personne->getSejoursSuivis()->toArray())) {
                            throw new Exception("La personne a déjà le rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_suivie->getNom());
                        }
                        $sejour->addSuiveur($personne);
                        $manager->persist($sejour);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_046, [
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE_VALIDATION => $structure,
                        ]);
                        $this->addFlash('success_suivi_valid', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_suivie->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    } catch (Exception $e) {
                        $this->addFlash('danger_suivi_valid', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                    }
                }
            }
        }


        //Liste des séjours

        $sejours_organises = $sejourRepository->findSejoursOrganisesParStructure($structure);
        $archives_sejours_organises = $sejourRepository->findArchivesSejoursOrganisesParStructure($structure);
        $structures_regroupement = $structureRegroupementSejourRepository->findSejoursRegroupement($structure);

        $arrayFormParticipantsUniteEEDFRegroupees = [];
        /**  @var StructureRegroupementSejour $regroupement */
        foreach ($structures_regroupement as $regroupement) {
            if ($regroupement->getStatutDemande() == StructureRegroupementSejour::STATUT_DEMANDE_ACCEPTE) {
                /** @var ParticipationSejourUnite $uniteParticipante */
                foreach ($regroupement->getUnitesParticipantes() as $uniteParticipante) {
                    $form = $formFactory->createNamed('participants_sejour_' . $regroupement->getId() . '_' . $uniteParticipante->getEquipe()->getId(), ParticipationParticipantsSejourUniteType::class, $uniteParticipante, [
                        'equipe' => $uniteParticipante->getEquipe(),
                        'sejour' => $regroupement->getSejour(),
                        'validation_groups' => ['Default', 'publication_declaration_intention']
                    ]);

                    $arrayFormParticipantsUniteEEDFRegroupees[$regroupement->getId() . '_' . $uniteParticipante->getEquipe()->getId()] = $form;
                }
            }
        }

        //Gestion des formulaires des unités EEDF
        foreach ($arrayFormParticipantsUniteEEDFRegroupees as $formParticipantsUniteEEDF) {
            $formParticipantsUniteEEDF->handleRequest($request);
            if ($formParticipantsUniteEEDF->isSubmitted()) {
                if ($formParticipantsUniteEEDF->isValid()) {
                    $regroupement->getSejour()->setDateModification(new DateTime());
                    try {
                        $manager->persist($regroupement->getSejour());
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_080, [
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $this->getUser(),
                            NotificationConstants::LABEL_PERIMETRE_SEJOUR => $regroupement->getSejour(),
                        ]);
                        $this->addFlash('success_effectifs_participants_regroupement', "Modifications des participant·e·s de l'unité enregistrées avec succès");
                    } catch (Exception $e) {
                        $this->addFlash('danger_effectifs_participants_regroupement', "Erreur lors de l'enregistrement des participant·e·s: " . $e->getMessage());
                    }
                } else {;
                    $this->addFlash('danger_effectifs_participants_regroupement', "Données incorrectes, veuillez corriger");
                }
            }
        }


        $arrayFormViewParticipantsUniteEEDFRegroupees = [];
        foreach ($arrayFormParticipantsUniteEEDFRegroupees as $unite => $formParticipantsUniteEEDF) {
            $arrayFormViewParticipantsUniteEEDFRegroupees[$unite]
                = $formParticipantsUniteEEDF->createView();
        }


        $sejours_suivis_orga = $sejourRepository->findSuivisSejoursPourStructureOrga($structure);
        $sejours_en_validation = $sejourRepository->findSejoursValidationParStructure($structure);
        $sejours_visitables = $sejourRepository->findSejoursVisitablesParStructure($structure);
        $archives_sejours_valides = $sejourRepository->findArchivesSejoursValidesParStructure($structure);
        $sejours_suivis_valid = $sejourRepository->findSuivisSejoursPourStructureValid($structure);
        $sejours_validateurs = $sejourRepository->findValidateursSejoursPourStructureValid($structure);

        return $this->render('structure/gestion_structure.html.twig', [
            'structure' => $structure,
            'form_infos_structure' =>  $form_infos_structure ? $form_infos_structure->createView() : null,
            'organisation_sejour' => $organisation_sejour,
            'sejours_organises' => $sejours_organises,
            'sejours_en_validation' => $sejours_en_validation,
            'sejours_visitables' => $sejours_visitables,
            'archives_sejours_organises' => $archives_sejours_organises,
            'archives_sejours_valides' => $archives_sejours_valides,
            'sejours_suivis_orga' => $sejours_suivis_orga,
            'arrayFormViewParticipantsUniteEEDFRegroupees' => $arrayFormViewParticipantsUniteEEDFRegroupees,
            'regroupements' => $structures_regroupement,
            'sejours_suivis_valid' => $sejours_suivis_valid,
            'sejours_validateurs' => $sejours_validateurs,
            'form_ajout_suivi_orga' => $form_ajout_suivi_orga ? $form_ajout_suivi_orga->createView() : null,
            'form_ajout_suivi_valid' => $form_ajout_suivi_valid ? $form_ajout_suivi_valid->createView() : null,
            'form_ajout_visiteur' => $form_ajout_visiteur ? $form_ajout_visiteur->createView() : null,
            'form_ajout_validation' => $form_ajout_validateur ? $form_ajout_validateur->createView() : null
        ]);
    }

    /**
     * Permet de supprimer le rôle de suivi d'un séjour pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne la personne à qui enlever le suivi du séjour
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure}/suivi/orga/{sejour}/supprimer/{personne}', name: 'supprimer_suivi_sejour_orga')]
    public function suppressionRoleSuiviSejourOrga(Personne $personne, Structure $structure, Sejour $sejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_ORGANISER, $structure);
        try {
            $sejour->removeSuiveur($personne);
            $manager->persist($sejour);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_033, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
            ]);
            $this->addFlash('success_suivi', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_suivi', "Erreur lors de la suppression du rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
    }

    /**
     * Permet de supprimer le rôle de suivi d'un séjour d'une structure dépendante pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne la personne à qui enlever le suivi du séjour
     * @param  Structure $structure_valid la structure concernée
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications     
     * @return void
     */
    #[Route('/structure/{structure_valid}/suivi/valid/{sejour}/supprimer/{personne}', name: 'supprimer_suivi_sejour_valid')]
    public function suppressionRoleSuiviSejourValid(Personne $personne, Structure $structure_valid, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_VALIDER, $structure_valid);
        try {
            $sejour->removeSuiveur($personne);
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_suivi_valid', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_047, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE_VALIDATION => $structure_valid,
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi_valid', "Erreur lors de la suppression du rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure_valid->getId()]);
    }

    /**
     * Permet de supprimer le rôle de validation déléguée d'un séjour d'une structure dépendante pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de validation
     * @param  Personne $personne la personne à qui enlever la validation du séjour
     * @param  Structure $structure_valid la structure concernée
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications     
     * @return void
     */
    #[Route('/structure/{structure_valid}/valid/{sejour}/supprimer/{personne}', name: 'supprimer_validation_sejour')]
    public function suppressionRoleValidationSejour(Personne $personne, Structure $structure_valid, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_VALIDER, $structure_valid);
        try {
            $sejour->removeValidateur($personne);
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_ajout_valid', "Rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_078, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi_valid', "Erreur lors de la suppression du rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure_valid->getId()]);
    }



    /**
     * Permet de supprimer le rôle de suivi de tous les séjours de la structure pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne la personne concerné·e à qui supprimer le suivi
     * @param  Structure $structure la structure concernée sur qui supprimer le suivi (*ex: SLA Lille 3*)
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure}/suivi/orga/supprimer/{personne}', name: 'supprimer_suivi_structure_orga')]
    public function suppressionRoleSuiviStructureOrganisation(Personne $personne, Structure $structure, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_ORGANISER, $structure);
        try {
            $structure->removeSuiveur($personne);
            $manager->persist($structure);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_033bis, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                NotificationConstants::LABEL_PERIMETRE_ORGANISATEUR => $this->getUser()
            ]);
            $this->addFlash('success_suivi', "Rôle de suivi de tous les séjours supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_suivi', "Erreur lors de la suppression du rôle de suivi de tous les séjours pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
    }


    /**
     * Permet de supprimer le rôle de suivi de tous les séjours d'une structure dépendante de la structure en validation pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne la personne concerné·e à qui supprimer le suivi
     * @param  Structure $structure la structure concernée sur qui supprimer le suivi (*ex: SLA Lille 3*)
     * @param  Structure $structure_valid la structure en charge de la validation concernée (*ex: Région Hauts de France*)
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure_valid}/suivi/{structure}/valid/supprimer/{personne}', name: 'supprimer_suivi_structure_valid')]
    public function suppressionRoleSuiviStructureValidation(Personne $personne, Structure $structure_valid, Structure $structure, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_VALIDER, $structure_valid);
        try {
            $structure->removeSuiveur($personne);
            $manager->persist($structure);
            $manager->flush();

            $this->addFlash('success_suivi_valid', "Rôle de suivi de tous les séjours de la structure " . $structure->getNom() . " supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_047bis, [
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE_VALIDATION => $structure_valid,
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi_valid', "Erreur lors de la suppression du rôle de suivi de tous les séjours de la structure " . $structure->getNom() . " pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure_valid->getId()]);
    }

    /**
     * Permet de supprimer le rôle de visite d'un séjour pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de visite
     * @param  Personne $personne la personne à qui enlever la visite
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[Route('/structure/{structure}/role/visite/supprimer/{nominationVisiteSejour}', name: 'supprimer_role_visite_sejour')]
    public function suppressionRoleVisiteSejour(Structure $structure, NominationVisiteSejour $nominationVisiteSejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        $this->denyAccessUnlessGranted(StructureVoter::STRUCTURE_VALIDER, $structure);
        try {

            $sejour = $nominationVisiteSejour->getSejour();
            $personne = $nominationVisiteSejour->getVisiteur();
            $manager->remove($nominationVisiteSejour);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_083, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
            ]);
            $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" supprimé pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_visite', "Erreur lors de la suppression du rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_structure', ['id' => $structure->getId()]);
    }
}
