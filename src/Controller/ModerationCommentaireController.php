<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\DemandeModerationCommentaire;
use App\Form\DemandeModerationType;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour faire des demandes de modération de commentaires
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class ModerationCommentaireController extends AbstractController
{

    /**
     * Permet de créer une demande de modération pour un commentaire, d'envoyer une notification aux intéressé·e·s, et d'afficher un message flash de succès/échec
     *
     * @param  Request $request Requête HTTP
     * @param  Commentaire $commentaire le commentaire pour lequel est faite la demande
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  NotificationHelper  $notificationHelper Helper pour l'envoi de notification
     * @uses DemandeModerationType le formulaire de demande de modération du commentaire
     * @return Response Retour vers la route du séjour concerné par le commentaire
     * @throws Exception Lorsque la création de la demande de modération ou l'envoi de la notification échoue
     */
    #[Route('demande/moderation/commentaire/{commentaire}', name: 'signaler_commentaire')]
    public function demandeModerationCommentaire(Request $request, Commentaire $commentaire, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        $demandeModeration = new DemandeModerationCommentaire();

        $form = $this->createForm(DemandeModerationType::class, $demandeModeration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $demandeModeration->setPersonne($this->getUser());
            $demandeModeration->setCommentaire($commentaire);
            $demandeModeration->setDate(new DateTime());
            try {
                $manager->persist($demandeModeration);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_071, []);
                $this->addFlash('success_envoi_demande_moderation', "Votre demande de modération a été envoyée avec succès");
            } catch (Exception $e) {
                $this->addFlash('danger_envoi_demande_moderation', "Erreur lors de l'envoi de la demande de modération: " . $e->getMessage());
            }
            return $this->redirectToRoute('detail_sejour', ['id' => $commentaire->getSejour()->getId()]);
        }
        return $this->render('moderation_commentaire/demande_moderation.html.twig', [
            'formSignalement' => $form->createView(),
            'commentaire' => $commentaire
        ]);
    }
}
