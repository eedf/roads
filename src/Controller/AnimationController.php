<?php

namespace App\Controller;

use App\Entity\DemandeModerationCommentaire;
use App\Entity\DocumentRessource;
use App\Entity\NominationVisiteSejour;
use App\Entity\ParametreRoadsTexte;
use App\Entity\Personne;
use App\Entity\Sejour;
use App\Form\AjoutRoleVisiteSejourAnimationType;
use App\Form\DocumentRessourceType;
use App\Form\ParametreRoadsTexteType;
use App\Repository\DemandeModerationCommentaireRepository;
use App\Repository\DocumentRessourceRepository;
use App\Repository\ParametreRoadsTexteRepository;
use App\Repository\SejourRepository;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * 
 * Controller pour les fonctions d'animation de ROADS
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('ROLE_ANIMATION')]
class AnimationController extends AbstractController
{

    const PANEL_ACTIF_TEXTES_DOCUMENTS = 1;
    const PANEL_ACTIF_MODERATION = 2;
    const PANEL_ACTIF_VISITES = 3;

    /**
     * Route pour les fonctionnalités d'animation et de modération
     * - Le paramétrage du texte de la page d'accueil
     * - La gestion des documents ressources
     * - La gestion globale des visites
     * - L'afficahge de la liste des demandes de modération en cours
     * 
     * @uses ParametreRoadsTexteType formulaire pour saisir/modifier le texte de la page d'acceuil
     * @uses DocumentRessourceType formulaire pour modifier les documents ressources
     * @param  Request $request requête HTTP
     * @param  ParametreRoadsTexteRepository $parametreRoadsTexteRepository pour accéder au texte d'acceuil paramétré existant
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder aux documents ressources existants
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  SejourRepository $sejourRepository pour accéder aux séjours et à leur programmation de vistes
     * @param  DemandeModerationCommentaireRepository $demandeModerationCommentaireRepository pour accéder aux demandes de modération existantes
     * @return Response
     *  @uses AjoutRoleVisiteSejourAnimationType le formulaire pour ajouter le rôle de visit·eur·euse sur un séjour
     */
    #[Route('/animation/general', name: 'animation_general')]
    public function animation(Request $request, ParametreRoadsTexteRepository $parametreRoadsTexteRepository, DocumentRessourceRepository $documentRessourceRepository, EntityManagerInterface $manager, DemandeModerationCommentaireRepository $demandeModerationCommentaireRepository, SejourRepository $sejourRepository, NotificationHelper $notificationHelper, FormFactoryInterface $formFactory): Response
    {

        $panelActif = self::PANEL_ACTIF_TEXTES_DOCUMENTS;
        /* GESTION DES TEXTES PARAMETRABLES*/
        $parametrageTexteAccueil = $parametreRoadsTexteRepository->findOneBy(['categorie' => ParametreRoadsTexte::CATEGORIE_ACCUEIL_CONNECTE]);

        if ($parametrageTexteAccueil == null) {
            $parametrageTexteAccueil = new ParametreRoadsTexte();
            $parametrageTexteAccueil->setCategorie(ParametreRoadsTexte::CATEGORIE_ACCUEIL_CONNECTE);
        }

        $formParametrageTexteAccueil = $formFactory->createNamed(
            $parametrageTexteAccueil->getCategorie(),
            ParametreRoadsTexteType::class,
            $parametrageTexteAccueil
        );

        $formParametrageTexteAccueil->handleRequest($request);
        if ($formParametrageTexteAccueil->isSubmitted() && $formParametrageTexteAccueil->isValid()) {
            $panelActif = self::PANEL_ACTIF_TEXTES_DOCUMENTS;
            try {
                $manager->persist($parametrageTexteAccueil);
                $manager->flush();
                $this->addFlash('success_parametrage_texte', "Modification du texte paramétrable enregistrée avec succès");
            } catch (Exception $e) {
                $this->addFlash('danger_parametrage_texte', "Erreur lors de la modification du texte paramétrable: " . $e->getMessage());
            }
        }

        /* GESTION DES DOCUMENTS RESSOURCES*/
        $documentsRessourceExistants = $documentRessourceRepository->findAll();
        $liste_form_docs = array();
        foreach ($documentsRessourceExistants as $documentRessourceExistant) {
            $liste_form_docs[] =
                [
                    'form' =>
                    $formFactory->createNamed(
                        $documentRessourceExistant->getId(),
                        DocumentRessourceType::class,
                        $documentRessourceExistant
                    ),
                    'doc' => $documentRessourceExistant
                ];
        }

        $docs_ressource = array();
        foreach ($liste_form_docs as $formDocRessources) {
            $formDocRessources['form']->handleRequest($request);
            if ($formDocRessources['form']->isSubmitted() && $formDocRessources['form']->isValid()) {
                $panelActif = self::PANEL_ACTIF_TEXTES_DOCUMENTS;
                if ($formDocRessources['form']->get('type')->getData() == DocumentRessource::TYPE_LIEN) {
                    $manager->remove($formDocRessources['doc']->getDocument());
                    $formDocRessources['doc']->setDocument(null);
                }
                $manager->persist($formDocRessources['doc']);
                $manager->flush();
                $this->addFlash('success_doc', "Modification du document \"" . $formDocRessources['doc']->getNom() . "\" enregistrée avec succès");
            }
            $docs_ressource[] =
                [
                    'form' => $formDocRessources['form']->createView(),
                    'doc' => $formDocRessources['doc']
                ];
        }

        /* GESTION DES DEMANDES DE MODERATION */
        $demandesModerationCommentaires = $demandeModerationCommentaireRepository->findAll();


        //---------------------------------------------------------
        //Gestion formulaire attribution visites
        $form_ajout_visiteur = $this->createForm(AjoutRoleVisiteSejourAnimationType::class, null);
        $form_ajout_visiteur->handleRequest($request);
        if ($form_ajout_visiteur->isSubmitted() && $form_ajout_visiteur->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_visiteur->get('personne')->getData();

            // Ajout du rôle de visite pour un séjour spécifique
            try {
                /**  @var Sejour $sejour */
                $sejour = $form_ajout_visiteur->get('sejour')->getData();

                /** @var NominationVisiteSejour $nominations_visites */
                foreach ($sejour->getNominationsVisiteSejour() as $nominations_visites) {
                    if ($nominations_visites->getVisiteur() == $personne) {
                        throw new Exception("La personne a déjà le rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ")");
                    }
                }

                $nouvelle_nomination_visite = new NominationVisiteSejour();
                $nouvelle_nomination_visite->setSejour($sejour);
                $nouvelle_nomination_visite->setVisiteur($personne);
                $nouvelle_nomination_visite->setNommePar($this->getUser());
                $manager->persist($nouvelle_nomination_visite);
                $manager->persist($sejour);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_082, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_NOMINATION_VISITE => $nouvelle_nomination_visite
                ]);
                $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                return $this->redirectToRoute('animation_general');
            } catch (Exception $e) {
                $this->addFlash('danger_visite', "Erreur lors de l'ajout de rôle de visite : " . $e->getMessage());
            }
        }


        $sejours_visitables = $sejourRepository->findTousSejoursVisitables();
        return $this->render('animation/general.html.twig', [
            'sejours_visitables' => $sejours_visitables,
            'docs_ressource' => $docs_ressource,
            'formParametrageTexteAccueil' => $formParametrageTexteAccueil->createView(),
            'demandesModerationCommentaires' => $demandesModerationCommentaires,
            'panelActif' => $panelActif,
            'form_ajout_visiteur' => $form_ajout_visiteur->createView()
        ]);
    }

    /**
     * Permet d'accepter une demande de modération.
     * Le commentaire est alors supprimé, ainsi que la demande, un message flash est affiché, et une notification est envoyée
     *
     * @param  DemandeModerationCommentaire $demande la demande de modération à accepter
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi des notifications
     * @return Response retour vers la page d'animation générale
     */
    #[Route('/animation/accepter/moderation/{demande}', name: 'accepter_moderation')]
    public function accepterModeration(DemandeModerationCommentaire $demande, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        try {
            $manager->remove($demande->getCommentaire());
            $manager->remove($demande);

            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_064, [
                NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE => $demande
            ]);
            $this->addFlash('success_moderation', "La modération a été acceptée avec succès");
        } catch (Exception $e) {
            $this->addFlash('danger_moderation', "Erreur lors de l'acceptation de la modération: " . $e->getMessage());
        }
        return $this->redirectToRoute('animation_general');
    }

    /**
     * Permet de refuser une demande de modération.
     * La demande est alors supprimée, un message flash est affiché, et une notification est envoyée
     * @param  DemandeModerationCommentaire $demande la demande de modération à refuser
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi des notifications
     * @return Response retour vers la page d'animation générale
     */
    #[Route('/animation/refuser/moderation/{demande}', name: 'refuser_moderation')]
    public function refuserModeration(DemandeModerationCommentaire $demande, EntityManagerInterface $manager, NotificationHelper $notificationHelper): Response
    {
        try {
            $manager->remove($demande);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_065, [
                NotificationConstants::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE => $demande
            ]);
            $this->addFlash('success_moderation', "La modération a été refusée avec succès");
        } catch (Exception $e) {
            $this->addFlash('danger_moderation', "Erreur lors du refus de la modération: " . $e->getMessage());
        }
        return $this->redirectToRoute('animation_general');
    }


    /**
     * Permet de supprimer le rôle de visite d'un séjour pour une personne spécifique, et une notification est envoyée
     * @param  NominationVisiteSejour nominationVisiteSejour la nomination de visite à supprimer
     * @param  EntityManagerInterface $manager gestionnaire d'entités pour Doctrine
     * @param  NotificationHelper $notificationHelper Helper pour gérer l'envoi des notifications
     * @return Response retour vers la page d'animation générale
     */
    #[Route('/animation/visite/supprimer/{nominationVisiteSejour}', name: 'animation_supprimer_role_visite_sejour')]
    public function suppressionRoleVisiteSejour(NominationVisiteSejour $nominationVisiteSejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        try {

            $sejour = $nominationVisiteSejour->getSejour();
            $personne = $nominationVisiteSejour->getVisiteur();
            $manager->remove($nominationVisiteSejour);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_083, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
            ]);
            $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" supprimé pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_visite', "Erreur lors de la suppression du rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('animation_general');
    }
}
