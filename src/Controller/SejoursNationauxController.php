<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\NominationVisiteSejour;
use App\Entity\Sejour;
use App\Entity\Structure;
use App\Form\AjoutRoleSuiviSejourNationalType;
use App\Form\AjoutRoleValidationSejourNationalType;
use App\Form\AjoutRoleVisiteSejourNationalType;
use App\Repository\SejourRepository;
use App\Repository\StructureRepository;
use App\Service\NotificationHelper;
use App\Utils\NotificationConstants;
use ArrayObject;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour la gestion globale des séjours à dimension nationale
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class SejoursNationauxController extends AbstractController
{

    /**
     * Route pour gérer globalement les séjours à dmiension nationale:
     * 
     * - Affichage des séjour nationaux en cours et cloturés
     * - Affichage du suivi des séjours nationaux
     * - Affichage de la gestion des visites des séjours nationaux
     * - Ajout du rôle de validation d'un séjour (lorsqu'une délégation de validation est ajoutée, une notifications est envoyée aux personnes concernée) sur un séjour ou tous les séjours en cours d'une structure nationale
     * - Ajout du rôle de suivi (lorsqu'un suivi est ajouté, une notification est envoyée aux personnes concernées) sur un séjour ou une structure nationale (*ex: Centres permanents Nationaux*)
     * - Ajout du rôle de visiteur·se sur un séjour national
     * 
     * @param  Request $request requete HTTP
     * @param  EntityManagerInterface $manager
     * @param  StructureRepository $structureRepository pour accéder aux structures nationales
     * @param  SejourRepository $sejourRepository pour accéder aux séjours nationaux
     * @param  NotificationHelper $notificationHelper helper pour gérer l'envoi de notifications
     * @return Response
     * @uses AjoutRoleSuiviSejourNationalType le formulaire pour ajouter le suivi à une personne sur un séjour national
     * @uses AjoutRoleValidationSejourNationalType le formulaire pour ajouter la délégation de validation à une personne sur un séjour national
     * @uses AjoutRoleVisiteSejourNationalType le formulaire pour ajouter le rôle de visit·eur·euse à un séjour national
     * @return Response
     */
    #[IsGranted("ROLE_VALIDATION_NATIONALE")]
    #[Route('/national', name: 'gestion_sejours_nationaux')]
    public function gestionStructure(Request $request, EntityManagerInterface $manager, StructureRepository $structureRepository, SejourRepository $sejourRepository, NotificationHelper $notificationHelper): Response
    {
        $form_ajout_suivi_national = $this->createForm(AjoutRoleSuiviSejourNationalType::class);
        $form_ajout_suivi_national->handleRequest($request);

        if ($form_ajout_suivi_national->isSubmitted() && $form_ajout_suivi_national->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_suivi_national->get('personne')->getData();
            /**  @var Structure $structure_suivie */
            $structure_suivie = $form_ajout_suivi_national->get('structure')->getData();
            $touteLaStructure = $form_ajout_suivi_national->get('all')->getData();

            if ($touteLaStructure) // Ajout du rôle de suivi pour toute une structure
            {
                try {

                    if (in_array($structure_suivie, $personne->getStructuresSuivi()->toArray())) {
                        throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de suivi de tous les séjours de la structure \"" . $structure_suivie->getNom() . "\"");
                    }
                    $structure_suivie->addSuiveur($personne);
                    $manager->persist($structure_suivie);
                    $manager->flush();
                    $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_049bis, [
                        NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                        NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                        NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_suivie,
                    ]);
                    $this->addFlash('success_suivi', "Rôle de suivi de tous les séjours de la structure " . $structure_suivie->getNom() . " ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                } catch (Exception $e) {
                    $this->addFlash('danger_suivi', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                }
            } else { // Ajout du rôle de suivi pour un séjour spécifique
                try {
                    /**  @var Sejour $sejour */
                    $sejour = $form_ajout_suivi_national->get('sejour')->getData();

                    if (in_array($sejour, $personne->getSejoursSuivis()->toArray())) {
                        throw new Exception("La personne a déjà le rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_suivie->getNom());
                    }
                    $sejour->addSuiveur($personne);
                    $manager->persist($sejour);
                    $manager->flush();

                    $this->addFlash('success_suivi', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_suivie->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_049, [
                        NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                        NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                        NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    ]);
                } catch (Exception $e) {
                    $this->addFlash('danger_suivi', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                }
            }
        }

        $form_ajout_validateur_sejour_national = $this->createForm(AjoutRoleValidationSejourNationalType::class);

        $form_ajout_validateur_sejour_national->handleRequest($request);

        if ($form_ajout_validateur_sejour_national->isSubmitted() && $form_ajout_validateur_sejour_national->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_validateur_sejour_national->get('personne')->getData();
            /**  @var Structure $structure_validation */
            $structure_validation = $form_ajout_validateur_sejour_national->get('structure')->getData();
            $touteLaStructure = $form_ajout_validateur_sejour_national->get('all')->getData();

            if ($touteLaStructure) // Ajout du rôle de suivi pour chacun des séjours en cours d'une structure
            {
                try {

                    if (in_array($structure_validation, $personne->getStructuresValidation()->toArray())) {
                        throw new Exception("La personne " . $personne->getPrenom() . " " . $personne->getNom() . " a déjà le rôle de validation de tous les séjours de la structure \"" . $structure_validation->getNom() . "\"");
                    }

                    $sejours_validation = $sejourRepository->findSejoursOrganisesParStructure($structure_validation);

                    $sejours_delegues_en_validation = [];
                    foreach ($sejours_validation as $sejour) {
                        if (in_array($sejour, $personne->getSejoursValidation()->toArray())) {
                            $this->addFlash('warning_ajout_valid', "La personne a déjà le rôles de validation déléguée pour le séjour " . $sejour->getIntitule());
                        } else {
                            $sejours_delegues_en_validation[] = $sejour;
                            $sejour->addValidateur($personne);
                            $manager->persist($sejour);
                        }
                    }

                    $manager->flush();

                    if (count($sejours_delegues_en_validation) > 0) {
                        $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_077, [
                            NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_validation,
                            NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                            NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                            NotificationConstants::LABEL_PERIMETRE_SEJOURS => new ArrayObject($sejours_delegues_en_validation),
                        ]);
                        $this->addFlash('success_ajout_valid', "Rôle de validation de chacun des séjours de la structure " . $structure_validation->getNom() . " ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                    }
                } catch (Exception $e) {
                    $this->addFlash('danger_ajout_valid', "Erreur lors de l'ajout de rôle de suivi : " . $e->getMessage());
                }
            } else { // Ajout du rôle de validation pour un séjour spécifique
                try {
                    /**  @var Sejour $sejour */
                    $sejour = $form_ajout_validateur_sejour_national->get('sejour')->getData();

                    if (in_array($sejour, $personne->getSejoursValidation()->toArray())) {
                        throw new Exception("La personne a déjà le rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_validation->getNom() . ")");
                    }
                    $sejour->addValidateur($personne);
                    $manager->persist($sejour);
                    $manager->flush();
                    $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_077, [
                        NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure_validation,
                        NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                        NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                        NotificationConstants::LABEL_PERIMETRE_SEJOURS => new ArrayObject([$sejour]),
                    ]);
                    $this->addFlash('success_ajout_valid', "Rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" (" . $structure_validation->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                } catch (Exception $e) {
                    $this->addFlash('danger_ajout_valid', "Erreur lors de l'ajout de rôle de validation déléguée : " . $e->getMessage());
                }
            }
        }

        //---------------------------------------------------------
        //Gestion formulaire attribution visites
        $form_ajout_visiteur = $this->createForm(AjoutRoleVisiteSejourNationalType::class);
        $form_ajout_visiteur->handleRequest($request);
        if ($form_ajout_visiteur->isSubmitted() && $form_ajout_visiteur->isValid()) {

            /**  @var Personne $personne */
            $personne = $form_ajout_visiteur->get('personne')->getData();

            // Ajout du rôle de visite pour un séjour spécifique
            try {
                /**  @var Sejour $sejour */
                $sejour = $form_ajout_visiteur->get('sejour')->getData();

                /** @var NominationVisiteSejour $nominations_visites */
                foreach ($sejour->getNominationsVisiteSejour() as $nominations_visites) {
                    if ($nominations_visites->getVisiteur() == $personne) {
                        throw new Exception("La personne a déjà le rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ")");
                    }
                }

                $nouvelle_nomination_visite = new NominationVisiteSejour();
                $nouvelle_nomination_visite->setSejour($sejour);
                $nouvelle_nomination_visite->setVisiteur($personne);
                $nouvelle_nomination_visite->setNommePar($this->getUser());
                $manager->persist($nouvelle_nomination_visite);
                $manager->persist($sejour);
                $manager->flush();
                $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_082, [
                    NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                    NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                    NotificationConstants::LABEL_PERIMETRE_NOMINATION_VISITE => $nouvelle_nomination_visite
                ]);
                $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $sejour->getIntitule() . "\" (" . $sejour->getStructureOrganisatrice()->getNom() . ") ajouté pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
                return $this->redirectToRoute('gestion_sejours_nationaux');
            } catch (Exception $e) {
                $this->addFlash('danger_visite', "Erreur lors de l'ajout de rôle de visite : " . $e->getMessage());
            }
        }

        //Liste des séjours

        $sejours_nationaux = $sejourRepository->findSejoursNationaux();
        $archives_sejours_nationaux = $sejourRepository->findArchivesSejoursNationaux();
        $structuresNationales = $structureRepository->findStructuresAValidationNationale();
        $sejours_visitables = $sejourRepository->findSejoursNationauxVisitables();


        return $this->render('sejours_nationaux/gestion_sejours_nationaux.html.twig', [
            'sejours_nationaux' => $sejours_nationaux,
            'archives_sejours_nationaux' => $archives_sejours_nationaux,
            'sejours_visitables' => $sejours_visitables,
            'form_ajout_visiteur' => $form_ajout_visiteur->createView(),
            'form_ajout_suivi_national' => $form_ajout_suivi_national->createView(),
            'form_ajout_validateur_sejour_national' => $form_ajout_validateur_sejour_national->createView(),
            'structuresNationales' => $structuresNationales
        ]);
    }
    /**
     * Permet de supprimer le rôle de suivi d'un séjour national pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     **/
    #[IsGranted("ROLE_VALIDATION_NATIONALE")]
    #[Route('/national/suivi/{sejour}/supprimer/{personne}', name: 'supprimer_suivi_sejour_national')]
    public function suppressionRoleSuiviSejourValid(Personne $personne, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        try {
            $sejour->removeSuiveur($personne);
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_suivi', "Rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_050, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi', "Erreur lors de la suppression du rôle de suivi du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_nationaux');
    }

    /**
     * Permet de supprimer le rôle de validation d'un séjour national pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de validation
     * @param  Personne $personne la personne à qui enlever la validation du séjour
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications     
     * @return void
     */
    #[IsGranted("ROLE_VALIDATION_NATIONALE")]
    #[Route('/national/validation/{sejour}/supprimer/{personne}', name: 'supprimer_validation_sejour_national')]
    public function suppressionRoleValidationSejour(Personne $personne, Sejour $sejour, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        try {
            $sejour->removeValidateur($personne);
            $manager->persist($sejour);
            $manager->flush();

            $this->addFlash('success_ajout_valid', "Rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_078, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi_valid', "Erreur lors de la suppression du rôle de validation déléguée du séjour \"" . $sejour->getIntitule() . "\" pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_nationaux');
    }


    /**
     * Permet de supprimer le rôle de suivi de tous les séjours d'une structure nationale pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de suivi
     * @param  Personne $personne
     * @param  Structure $structure la structure nationale concernée (ex: "La planche")
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     **/
    #[IsGranted("ROLE_VALIDATION_NATIONALE")]
    #[Route('/national/{structure}/suivi/supprimer/{personne}', name: 'supprimer_suivi_structure_nationale')]
    public function suppressionRoleSuiviStructureNationale(Personne $personne, Structure $structure, EntityManagerInterface $manager, NotificationHelper $notificationHelper)
    {
        try {
            $structure->removeSuiveur($personne);
            $manager->persist($structure);
            $manager->flush();

            $this->addFlash('success_suivi', "Rôle de suivi de tous les séjours de la structure " . $structure->getNom() . " supprimé pour la personne " . $personne->getPrenom() . " " . $personne->getNom());
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_050bis, [
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
                NotificationConstants::LABEL_PERIMETRE_VALIDATEUR => $this->getUser(),
                NotificationConstants::LABEL_PERIMETRE_STRUCTURE => $structure,
            ]);
        } catch (Exception $e) {
            $this->addFlash('danger_suivi', "Erreur lors de la suppression du rôle de suivi de tous les séjours de la structure " . $structure->getNom() . " pour la personne " . $personne->getPrenom() . " " . $personne->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_nationaux');
    }

    /**
     * Permet de supprimer le rôle de visite d'un séjour pour une personne spécifique
     * Une notification est envoyée et un message flash est affiché lors de la suppression du rôle de visite
     * @param  Personne $personne la personne à qui enlever la visite
     * @param  Sejour $sejour le séjour concerné
     * @param  EntityManagerInterface $manager
     * @param  NotificationHelper $notificationHelper helper pour l'envoi de notifications
     * @return void
     */
    #[IsGranted("ROLE_VALIDATION_NATIONALE")]
    #[Route('nationale/role/visite/supprimer/{nominationVisiteSejour}', name: 'supprimer_role_visite_sejour_national')]
    public function suppressionRoleVisiteSejour(NominationVisiteSejour $nominationVisiteSejour, NotificationHelper $notificationHelper, EntityManagerInterface $manager)
    {
        try {

            $sejour = $nominationVisiteSejour->getSejour();
            /** @var Personne $personne */
            $personne = $nominationVisiteSejour->getVisiteur();
            $manager->remove($nominationVisiteSejour);
            $manager->flush();
            $notificationHelper->gestionNotification(NotificationConstants::NOM_NOTIF_083, [
                NotificationConstants::LABEL_PERIMETRE_SEJOUR => $sejour,
                NotificationConstants::LABEL_PERIMETRE_PERSONNE => $personne,
            ]);
            $this->addFlash('success_visite', "Rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" supprimé pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom());
        } catch (Exception $e) {
            $this->addFlash('danger_visite', "Erreur lors de la suppression du rôle de visite du séjour \"" . $nominationVisiteSejour->getSejour()->getIntitule() . "\" pour la personne " . $nominationVisiteSejour->getVisiteur()->getPrenom() . " " . $nominationVisiteSejour->getVisiteur()->getNom() . ": " . $e->getMessage());
        }
        return $this->redirectToRoute('gestion_sejours_nationaux');
    }
}
