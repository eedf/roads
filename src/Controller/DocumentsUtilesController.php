<?php

namespace App\Controller;

use App\Repository\DocumentRessourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour accéder à la liste des documents ressources
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class DocumentsUtilesController extends AbstractController
{
    /**
     * Route pour afficher la liste des documents ressources de ROADS
     *
     * @param  DocumentRessourceRepository $documentRessourceRepository pour accéder à la liste des documents ressources en BDD
     * @return Response
     */
    #[Route('/documents/utiles', name: 'documents_utiles')]
    public function index(DocumentRessourceRepository $documentRessourceRepository): Response
    {
        $documents = $documentRessourceRepository->findAll();
        return $this->render('documents_utiles/index.html.twig', [
            'documents' => $documents,
        ]);
    }
}
