<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\Personne;
use App\Repository\NotificationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\Security\Http\Attribute\IsGranted;

/**
 * Controller pour gérer les notifications de l'utilisa·eur·rice
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
#[IsGranted('IS_AUTHENTICATED_FULLY')]
class NotificationsController extends AbstractController
{
    /**
     * Permete d'afficher la liste des notification de l'utilisat·eur·rice connecté·e
     * @param  notificationRepository $notificationRepository pour accéder aux notifications stockées en BDD
     * @return Response
     */
    #[Route('/notifications', name: 'notifications')]
    public function index(NotificationRepository $notificationRepository): Response
    {

        /** @var Personne $personne */
        $personne = $this->getUser();

        $notificationsAAfficher = $notificationRepository->findNotificationsAAfficher($personne);

        return $this->render('notifications/index.html.twig', [
            'notificationsAAfficher' => $notificationsAAfficher,
        ]);
    }

    /**
     * Permet de changer le statut d'une notification à "Lu". L'Identifiant de la notification est passé en paramêtre request
     *
     * @param  HttpFoundationRequest $request requête HTTP dans laquelle se trouve le l'identifiant de la notification
     * @param  EntityManagerInterface $entityManagerInterface gestionnaire d'entités pour Doctrine
     * @param  NotificationRepository $notificationRepository pour accéder aux notifications
     * @return Response Etat OK (200)
     * @api
     */
    #[Route('/notifications/lu', methods: ["POST"], name: 'notification_lue')]
    public function marquerNotificationCommeLue(HttpFoundationRequest $request, EntityManagerInterface $entityManagerInterface, NotificationRepository $notificationRepository): Response
    {
        $notification = $notificationRepository->findOneBy(['id' => $request->request->get('id')]);

        $notification->setLu(true);
        $entityManagerInterface->persist($notification);
        $entityManagerInterface->flush();

        return $this->json([
            'etat' => 'OK'
        ], 200);
    }

    /**
     * 
     * Permet de supprimer une notification. L'Identifiant de la notification est passé en paramêtre request
     *
     * @param  HttpFoundationRequest $request requête HTTP dans laquelle se trouve le l'identifiant de la notification
     * @param  EntityManagerInterface $entityManagerInterface gestionnaire d'entités pour Doctrine
     * @param  NotificationRepository $notificationRepository pour accéder aux notifications
     * @return Response Etat OK (200)
     * @api
     */
    #[Route('/notifications/supprimer', methods: ["POST"], name: 'supprimer_notification')]
    public function supprimerNotification(HttpFoundationRequest $request, EntityManagerInterface $entityManagerInterface, NotificationRepository $notificationRepository): Response
    {
        $notification = $notificationRepository->findOneBy(['id' => $request->request->get('id')]);

        $entityManagerInterface->remove($notification);
        $entityManagerInterface->flush();

        return $this->json([
            'etat' => 'OK'
        ], 200);
    }

    /**
     * Permet de marquer toutes les notifications comme "lues" pour l'utilisat·eur·rice
     *
     * @param  NotificationRepository $notificationRepository pour accéder aux notifications
     * @return Response retour vers la page de notifications
     */
    #[Route('/notifications/toutes/lues', name: 'marquer_toutes_notification_comme_lues')]
    public function marquerToutesNotificationsCommeLues(NotificationRepository $notificationRepository): Response
    {
        $notificationRepository->updateNotificationsCommeLues($this->getUser());

        return $this->redirectToRoute('notifications');
    }
}
