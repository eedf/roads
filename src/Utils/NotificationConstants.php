<?php

namespace App\Utils;

use App\Entity\Commentaire;
use App\Entity\DemandeModerationCommentaire;
use App\Entity\NominationVisiteSejour;
use App\Entity\Personne;
use App\Entity\Sejour;
use App\Entity\Structure;
use ArrayObject;

/**
 * Constantes décrivant le catalogue de notifications disponibles
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class NotificationConstants
{
    const ROLE_ORGANISATION_SEJOUR = "organisateur_sejour";
    const ROLE_DIRECTION_SEJOUR = "direction_sejour";
    const ROLE_DIRECTION_ADJOINTE_SEJOUR = "direction_adjointe_sejour";
    const ROLE_SUIVI_SEJOUR = "suivi_sejour";
    const ROLE_VALIDATION_SEJOUR = "validation_sejour";
    const ROLE_VISITE_SEJOUR = "visite_sejour";
    const ROLE_ORGANISATEUR_STRUCTURE = "organisateur_structure";
    const ROLE_VALIDATEUR_STRUCTURE = "validation_structure";
    const ROLE_PERSONNE_CONCERNEE = "personne_concernee";
    const ROLE_AUTEUR_COMMENTAIRE = "auteur_commentaire";
    const ROLE_DENONCIATEUR_COMMENTAIRE = "denonciateur_commentaire";

    const ROLE_ADMINISTRATEUR = "admin";
    const ROLE_ANIMATEUR = "animateur";
    const ROLE_VALIDATION_NATIONALE = "validation_nationale_sejour";
    const ROLE_VALIDATION_INTERNATIONALE = "validation_internationale_sejour";

    const PERIMETRE_SEJOUR = Sejour::class;
    const LABEL_PERIMETRE_SEJOUR = 'sejour';

    const PERIMETRE_COMMENTAIRE = Commentaire::class;
    const LABEL_PERIMETRE_COMMENTAIRE = 'commentaire';

    const PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE = DemandeModerationCommentaire::class;
    const LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE = 'demandeModerationCommentaire';

    const PERIMETRE_NOMINATION_VISITE = NominationVisiteSejour::class;
    const LABEL_PERIMETRE_NOMINATION_VISITE = 'nominationVisiteSejour';



    const PERIMETRE_STRUCTURE = Structure::class;
    const LABEL_PERIMETRE_STRUCTURE = 'structure';
    const LABEL_PERIMETRE_STRUCTURE_VALIDATION = 'structureValidation';

    const PERIMETRE_PERSONNE = Personne::class;
    const LABEL_PERIMETRE_PERSONNE = 'personne';

    const LABEL_PERIMETRE_VALIDATEUR = 'validateur';
    const LABEL_PERIMETRE_ORGANISATEUR = 'organisateur';

    const LABEL_PERIMETRE_ROLE = 'role';
    const LABEL_PERIMETRE_LISTE_MODIFS = 'modifications_importantes';
    const LABEL_PERIMETRE_SEJOURS = 'sejours_validation';

    const NOM_NOTIF_001 = "NOTIF_001";
    const NOM_NOTIF_005 = "NOTIF_005";
    const NOM_NOTIF_006 = "NOTIF_006";
    const NOM_NOTIF_007 = "NOTIF_007";
    const NOM_NOTIF_007bis = "NOTIF_007bis";
    const NOM_NOTIF_008 = "NOTIF_008";
    const NOM_NOTIF_009 = "NOTIF_009";
    const NOM_NOTIF_010 = "NOTIF_010";
    const NOM_NOTIF_011 = "NOTIF_012";
    const NOM_NOTIF_012 = "NOTIF_012";
    const NOM_NOTIF_013 = "NOTIF_013";
    const NOM_NOTIF_014 = "NOTIF_014";
    const NOM_NOTIF_017 = "NOTIF_017";
    const NOM_NOTIF_015 = "NOTIF_015";
    const NOM_NOTIF_016 = "NOTIF_016";
    const NOM_NOTIF_020 = "NOTIF_020";
    const NOM_NOTIF_021 = "NOTIF_021";
    const NOM_NOTIF_022 = "NOTIF_022";
    const NOM_NOTIF_023 = "NOTIF_023";
    const NOM_NOTIF_024 = "NOTIF_024";
    const NOM_NOTIF_025 = "NOTIF_025";
    const NOM_NOTIF_026bis = "NOTIF_026bis";
    const NOM_NOTIF_026 = "NOTIF_026";
    const NOM_NOTIF_027 = "NOTIF_027";
    const NOM_NOTIF_028 = "NOTIF_028";
    const NOM_NOTIF_029 = "NOTIF_029";
    const NOM_NOTIF_030 = "NOTIF_030";
    const NOM_NOTIF_031 = "NOTIF_031";
    const NOM_NOTIF_032 = "NOTIF_032";
    const NOM_NOTIF_032bis = "NOTIF_032bis";
    const NOM_NOTIF_033 = "NOTIF_033";
    const NOM_NOTIF_033bis = "NOTIF_033bis";
    const NOM_NOTIF_046 = "NOTIF_046";
    const NOM_NOTIF_046bis = "NOTIF_046bis";
    const NOM_NOTIF_047 = "NOTIF_047";
    const NOM_NOTIF_047bis = "NOTIF_047bis";
    const NOM_NOTIF_049 = "NOTIF_049";
    const NOM_NOTIF_049bis = "NOTIF_049bis";
    const NOM_NOTIF_050 = "NOTIF_050";
    const NOM_NOTIF_050bis = "NOTIF_050bis";
    const NOM_NOTIF_053 = "NOTIF_053";
    const NOM_NOTIF_054 = "NOTIF_054";
    const NOM_NOTIF_059 = "NOTIF_059";
    const NOM_NOTIF_060 = "NOTIF_060";
    const NOM_NOTIF_061 = "NOTIF_061";
    const NOM_NOTIF_062 = "NOTIF_062";
    const NOM_NOTIF_062bis = "NOTIF_062bis";
    const NOM_NOTIF_064 = "NOTIF_064";
    const NOM_NOTIF_065 = "NOTIF_065";
    const NOM_NOTIF_068 = "NOTIF_068";
    const NOM_NOTIF_069 = "NOTIF_069";
    const NOM_NOTIF_071 = "NOTIF_071";
    const NOM_NOTIF_072 = "NOTIF_072";
    const NOM_NOTIF_073 = "NOTIF_073";
    const NOM_NOTIF_074 = "NOTIF_074";
    const NOM_NOTIF_075 = "NOTIF_075";
    const NOM_NOTIF_076 = "NOTIF_076";
    const NOM_NOTIF_077 = "NOTIF_077";
    const NOM_NOTIF_078 = "NOTIF_078";
    const NOM_NOTIF_079 = "NOTIF_079";
    const NOM_NOTIF_080 = "NOTIF_080";
    const NOM_NOTIF_081 = "NOTIF_081";
    const NOM_NOTIF_082 = "NOTIF_082";
    const NOM_NOTIF_083 = "NOTIF_083";
    const NOM_NOTIF_084 = "NOTIF_084";
    const NOM_NOTIF_085 = "NOTIF_085";


    const LISTE_NOTIF =
    [
        self::NOM_NOTIF_001 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_001",
            "titre" => "Création d'une déclaration d'intention pour le séjour {{sejour.intitule}} de {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_005 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_005",
            "titre" => "Dépôt d'une déclaration d'intention pour le séjour {{sejour.intitule}} de {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR
            ]
        ],
        //Changement de direction d'un séjour
        self::NOM_NOTIF_006 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_006",
            "titre" => "Changement de direction du séjour \"{{sejour.intitule}}\" de {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        //Demandes de regroupement
        self::NOM_NOTIF_007 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE
            ],
            'template' => "notif_007",
            "titre" => "Demande de regroupement avec une structure",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE
            ]
        ],
        self::NOM_NOTIF_007bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_007bis",
            "titre" => "Annulation d'un regroupement",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_ORGANISATEUR_STRUCTURE,
            ]
        ],
        self::NOM_NOTIF_008 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_008",
            "titre" => "Acceptation de la demande de participation au séjour de regroupement {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_ORGANISATEUR_STRUCTURE,
            ]
        ],
        self::NOM_NOTIF_009 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_009",
            "titre" => "Refus de la demande de participation au séjour de regroupement \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_ORGANISATEUR_STRUCTURE,
            ]
        ],
        self::NOM_NOTIF_010 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_010",
            "titre" => "Nomination d'un·e directeur·ice adjoint·e pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        //Refus du directeur adjoint
        self::NOM_NOTIF_011 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_011",
            "titre" => "Refus de direction adjointe du séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_012 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_012",
            "titre" => "Acceptation de direction adjointe pour le séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_013 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_013",
            "titre" => "Suppression d'un·e directeur·ice adjoint·e pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_014 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_014",
            "titre" => "Demande de participation à l'encadrement du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_015 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_015",
            "titre" => "Demande de participation à l'encadrement du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}} acceptée.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_016 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_016",
            "titre" => "Demande de participation à l'encadrement du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}} refusée.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_017 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_017",
            "titre" => "Annulation de la participation à l'encadrement du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE,
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        //Envoi du projet de séjour pour validation
        self::NOM_NOTIF_020 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_020",
            "titre" => "Demande de validation du séjour \"{{sejour.intitule}}\" déposée.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Validation du projet de séjour
        self::NOM_NOTIF_021 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_021",
            "titre" => "Validation du projet de séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Non validation du projet de séjour
        self::NOM_NOTIF_022 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_022",
            "titre" => "Non validation du projet de séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Validation de l'aspect international du séjour
        self::NOM_NOTIF_023 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_023",
            "titre" => "Validation de l'aspect international du séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Non validation de l'aspect international du séjour
        self::NOM_NOTIF_024 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_024",
            "titre" => "Non validation de l'aspect international du séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Modifications importantes du projet de séjour
        self::NOM_NOTIF_025 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_LISTE_MODIFS => ArrayObject::class,
            ],
            'template' => "notif_025",
            "titre" => "Modifications importantes du projet de séjour validé \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Séjour débuté
        self::NOM_NOTIF_026 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_026",
            "titre" => "Le séjour \"{{sejour.intitule}}\" a débuté.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_VISITE_SEJOUR
            ]
        ],
        //Séjour terminé
        self::NOM_NOTIF_026bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_026bis",
            "titre" => "Séjour \"{{sejour.intitule}}\" terminé.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_VISITE_SEJOUR
            ]
        ],
        //Séjour cloturé
        self::NOM_NOTIF_027 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_027",
            "titre" => "Séjour \"{{sejour.intitule}}\" clôturé.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        //Suspension projet de séjour
        self::NOM_NOTIF_028 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_028",
            "titre" => "Projet de séjour \"{{sejour.intitule}}\" suspendu.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        //Réactivation projet de séjour
        self::NOM_NOTIF_029 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_029",
            "titre" => "Projet de séjour \"{{sejour.intitule}}\" réactivé.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        //Annulation projet de séjour
        self::NOM_NOTIF_030 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_030",
            "titre" => "Projet de séjour \"{{sejour.intitule}}\" annulé.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
            ]
        ],
        //COmmentaire projet de séjour
        self::NOM_NOTIF_031 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_COMMENTAIRE => self::PERIMETRE_COMMENTAIRE,
            ],
            'template' => "notif_031",
            "titre" => "Commentaire sur le projet de séjour \"{{sejour.intitule}}\".",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
            ]
        ],
        //Droits de suivi accordés par l'organisat·eur·rice
        self::NOM_NOTIF_032 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_032",
            "titre" => "Droits de suivi accordés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_032bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_032bis",
            "titre" => "Droits de suivi accordés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_033 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_033",
            "titre" => "Droits de suivi retirés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_033bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_ORGANISATEUR => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_033bis",
            "titre" => "Droits de suivi retirés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        //Droits de suivi accordés par la validation régionale
        self::NOM_NOTIF_046 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE_VALIDATION => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_046",
            "titre" => "Droits de suivi accordés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_046bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE_VALIDATION => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_046bis",
            "titre" => "Droits de suivi accordés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_VALIDATEUR_STRUCTURE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_047 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE_VALIDATION => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_047",
            "titre" => "Droits de suivi retirés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_047bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE_VALIDATION => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_047bis",
            "titre" => "Droits de suivi retirés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_VALIDATEUR_STRUCTURE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],

        //Droits de suivi accordés par la validation des séjours nationaux
        self::NOM_NOTIF_049 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_049",
            "titre" => "Droits de suivi accordés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_NATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_049bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_049bis",
            "titre" => "Droits de suivi accordés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_VALIDATION_NATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_050 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_050",
            "titre" => "Droits de suivi retirés pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_NATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_050bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
            ],
            'template' => "notif_050bis",
            "titre" => "Droits de suivi retirés pour tous les séjours de la structure {{structure.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_VALIDATION_NATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],

        //Droits de suivi accordés par la validation internationale
        self::NOM_NOTIF_053 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_053",
            "titre" => "Droits de suivi accordés pour le séjour international \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_INTERNATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_054 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_VALIDATEUR => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_054",
            "titre" => "Droits de suivi retirés pour le séjour international \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_INTERNATIONALE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],


        //Désinscription
        self::NOM_NOTIF_059 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_059",
            "titre" => "Désinscription d'un·e utilisat·eur·rice de R.O.A.D.S",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_060 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_060",
            "titre" => "Désinscription de R.O.A.D.S d'un·e direct·eur·rice pour le séjour {{sejour}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_ANIMATEUR
            ]
        ],
        //Gestion des droits métiers
        self::NOM_NOTIF_061 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE
            ],
            'template' => "notif_061",
            "titre" => "Désinscription de R.O.A.D.S d'un·e organisat·eur·rice/validat·eur·rice",
            "destinataires" => [
                self::ROLE_ADMINISTRATEUR,
                self::ROLE_ANIMATEUR
            ]
        ],
        self::NOM_NOTIF_062 => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_062",
            "titre" => "Un rôle vous concernant a été supprimé par un·e administrat·eur·rice de ROADS.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_062bis => [
            'contexte' => [
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
            ],
            'template' => "notif_062bis",
            "titre" => "Un nouveau rôle vous a été attribué par un·e administrat·eur·rice de ROADS.",
            "destinataires" => [
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_064 => [
            'contexte' => [
                self::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE => self::PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE
            ],
            'template' => "notif_064",
            "titre" => "Une demande de modération d'un commentaire a été acceptée.",
            "destinataires" => [
                self::ROLE_AUTEUR_COMMENTAIRE,
                self::ROLE_ANIMATEUR,
                self::ROLE_DENONCIATEUR_COMMENTAIRE
            ]
        ],
        self::NOM_NOTIF_065 => [
            'contexte' => [
                self::LABEL_PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE => self::PERIMETRE_DEMANDE_MODERATION_COMMENTAIRE
            ],
            'template' => "notif_065",
            "titre" => "Une demande de modération d'un commentaire a été refusée.",
            "destinataires" => [
                self::ROLE_AUTEUR_COMMENTAIRE,
                self::ROLE_ANIMATEUR,
                self::ROLE_DENONCIATEUR_COMMENTAIRE
            ]
        ],
        self::NOM_NOTIF_068 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_068",
            "titre" => "Le séjour {{sejour.intitule}} se déclare comme un projet à dimension internationale.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_INTERNATIONALE,
                self::ROLE_VALIDATION_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_069 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_069",
            "titre" => "Le séjour {{sejour.intitule}} se déclare comme n'étant plus un projet a dimension internationale.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VALIDATION_INTERNATIONALE,
                self::ROLE_VALIDATION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_071 => [
            'contexte' => [],
            'template' => "notif_071",
            "titre" => "Une demande de modération d'un commentaire a été faite.",
            "destinataires" => [
                self::ROLE_ANIMATEUR
            ]
        ],
        self::NOM_NOTIF_072 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_072",
            "titre" => "Le séjour {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}} est depuis 30 jours en attente de déclaration d'intention.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_073 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_073",
            "titre" => "Le séjour {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}} est en attente de validation depuis 15 jours.",
            "destinataires" => [
                self::ROLE_VALIDATION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_074 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_074",
            "titre" => "Clôture du séjour {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}} en attente.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_075 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_075",
            "titre" => "Le séjour {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}} est en attente de confirmation de son·sa direct·eur·rice.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
            ]
        ],
        self::NOM_NOTIF_076 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
            ],
            'template' => "notif_076",
            "titre" => "Le séjour {{sejour.intitule}} de la structure {{sejour.structureOrganisatrice.nom}}, qui commence dans 30 jours, est encore 'en construction'.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR
            ]
        ],
        //Attribution de droits de validation de séjour par un validateur
        self::NOM_NOTIF_077 => [
            'contexte' => [
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE,
                self::LABEL_PERIMETRE_SEJOURS => ArrayObject::class,
            ],
            'template' => "notif_077",
            "titre" => "Droits de validation accordés pour des séjours de la structure {{structure.nom}}.",
            "destinataires" => [
                self::ROLE_ORGANISATEUR_STRUCTURE,
                self::ROLE_VALIDATEUR_STRUCTURE,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_078 => [
            'contexte' => [
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_078",
            "titre" => "Droits de validation retirés pour des séjours de la structure {{structure.nom}}.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_079 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_079",
            "titre" => "Clôture prochaine de la fiche du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}} .",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR
            ]
        ],
        self::NOM_NOTIF_080 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_STRUCTURE => self::PERIMETRE_STRUCTURE,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_080",
            "titre" => "Évolution du nombre de participant·e·s de la structure {{structure.nom}} pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_ORGANISATEUR_STRUCTURE,
            ]
        ],
        self::NOM_NOTIF_081 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_081",
            "titre" => "Publication du compte-rendu de visite pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_VISITE_SEJOUR
            ]
        ],
        self::NOM_NOTIF_082 => [
            'contexte' => [
                self::LABEL_PERIMETRE_NOMINATION_VISITE => self::PERIMETRE_NOMINATION_VISITE,
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_082",
            "titre" => "Programmation d'une visite pour le séjour \"{{nominationVisiteSejour.sejour.intitule}}\" de la structure {{nominationVisiteSejour.sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_083 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR,
                self::LABEL_PERIMETRE_PERSONNE => self::PERIMETRE_PERSONNE
            ],
            'template' => "notif_083",
            "titre" => "Annulation d'une visite programmée pour le séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}}",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR,
                self::ROLE_DIRECTION_ADJOINTE_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_SUIVI_SEJOUR,
                self::ROLE_PERSONNE_CONCERNEE
            ]
        ],
        self::NOM_NOTIF_084 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_084",
            "titre" => "Déclaration à Jeunesse et Sports du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}} qui commence dans 45 jours.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR
            ]
        ],
        self::NOM_NOTIF_085 => [
            'contexte' => [
                self::LABEL_PERIMETRE_SEJOUR => self::PERIMETRE_SEJOUR
            ],
            'template' => "notif_085",
            "titre" => "Vérification de la déclaration à Jeunesse et Sports du séjour \"{{sejour.intitule}}\" de la structure {{sejour.structureOrganisatrice.nom}} qui commence dans 8 jours.",
            "destinataires" => [
                self::ROLE_ORGANISATION_SEJOUR,
                self::ROLE_VALIDATION_SEJOUR,
                self::ROLE_DIRECTION_SEJOUR
            ]
        ],
    ];
}
