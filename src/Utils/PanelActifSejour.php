<?php

namespace App\Utils;

use App\Entity\Commentaire;


/**
 * Permet de déterminer le panel et les sous panels actifs d'un séjour lors de sa construction
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class PanelActifSejour
{

    const PANEL_GENERAL_DECLARATION_INTENTION = 1;
    const PANEL_GENERAL_CONSTRUCTION = 2;
    const PANEL_GENERAL_VALIDATION = 3;
    const PANEL_GENERAL_VALIDATION_INTERNATIONALE = 4;
    const PANEL_GENERAL_VISITE = 5;
    const PANEL_GENERAL_CLOTURE = 6;

    const PANEL_CONSTRUCTION_INFOS_GENERALES = 1;
    const PANEL_CONSTRUCTION_EFFECTIFS = 2;
    const PANEL_CONSTRUCTION_DATES_LIEUX = 3;
    const PANEL_CONSTRUCTION_DEMARCHES_PEDA = 4;
    const PANEL_CONSTRUCTION_LOGISTIQUE = 5;

    const PANEL_CLOTURE_INFOS_GENERALES = 1;
    const PANEL_CLOTURE_RETOURS_GENERAUX = 2;
    const PANEL_CLOTURE_EFFECTIFS = 3;
    const PANEL_CLOTURE_DATES_LIEUX = 4;
    const PANEL_CLOTURE_DEMARCHES_PEDA = 5;
    const PANEL_CLOTURE_LOGISTIQUE = 6;


    const MAP_PANEL_ACTIF_PAR_COMMENTAIRE = [
        Commentaire::SUJET_SEJOUR_DECLARATION_INTENTION_INFOS_GENERALES => [
            "panelActifGeneral" => self::PANEL_GENERAL_DECLARATION_INTENTION
        ],
        Commentaire::SUJET_SEJOUR_DECLARATION_INTENTION_DIRECTION => [
            "panelActifGeneral" => self::PANEL_GENERAL_DECLARATION_INTENTION
        ],
        Commentaire::SUJET_SEJOUR_DECLARATION_INTENTION_MODALITES => [
            "panelActifGeneral" => self::PANEL_GENERAL_DECLARATION_INTENTION
        ],
        Commentaire::SUJET_SEJOUR_DECLARATION_INTENTION_EFFECTIFS_PREVISIONNELS => [
            "panelActifGeneral" => self::PANEL_GENERAL_DECLARATION_INTENTION
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_INFOS_GENERALES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_DIRECTION => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_UNITES_PARTICIPANTES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_INFOS_GENERALES_MODALITES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_RESUME => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_ENCADREMENT => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_EFFECTIFS_PARTICIPANTS => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_DATES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_DATES_LIEUX
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_DATES_LIEUX_LIEUX => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_DATES_LIEUX
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_PROJET_PEDA => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_DEMARCHES_PEDA
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_COLOS_APPRENANTES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_DEMARCHES_PEDA
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_PEDA_INTERNATIONAL => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_DEMARCHES_PEDA
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_BUDGET => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_LOGISTIQUE
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_TRANSPORT => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_LOGISTIQUE
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_ALIMENTAIRE => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_LOGISTIQUE
        ],
        Commentaire::SUJET_SEJOUR_PROJET_SEJOUR_LOGISTIQUE_COMMUNCATION => [
            "panelActifGeneral" => self::PANEL_GENERAL_CONSTRUCTION,
            "panelActifConstruction" => self::PANEL_CONSTRUCTION_LOGISTIQUE
        ],
        Commentaire::SUJET_SEJOUR_VALIDATION => [
            "panelActifGeneral" => self::PANEL_GENERAL_VALIDATION
        ],
        Commentaire::SUJET_SEJOUR_VALIDATION_INTERNATIONALE => [
            "panelActifGeneral" => self::PANEL_GENERAL_VALIDATION_INTERNATIONALE
        ],
        Commentaire::SUJET_SEJOUR_VISITE => [
            "panelActifGeneral" => self::PANEL_GENERAL_VISITE
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_INFOS_GENERALES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_DIRECTION => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_UNITES_PARTICIPANTES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_INFOS_GENERALES_MODALITES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_INFOS_GENERALES
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_RETOURS_GENERAUX => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_RETOURS_GENERAUX
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_EFFECTIFS_RESUME => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_EFFECTIFS_ENCADREMENT => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_EFFECTIFS_PARTICIPANTS => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_EFFECTIFS
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_DATES => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_DATES_LIEUX
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_DATES_LIEUX_LIEUX => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_DATES_LIEUX
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_PEDA => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_DEMARCHES_PEDA
        ],
        Commentaire::SUJET_SEJOUR_CLOTURE_LOGISTIQUE => [
            "panelActifGeneral" => self::PANEL_GENERAL_CLOTURE,
            "panelActifCloture" => self::PANEL_CLOTURE_LOGISTIQUE
        ]
    ];

    private int $panelActifGeneral = self::PANEL_GENERAL_DECLARATION_INTENTION;
    private int $panelActifConstruction = self::PANEL_CONSTRUCTION_INFOS_GENERALES;
    private int $panelActifCloture = self::PANEL_CLOTURE_RETOURS_GENERAUX;



    /**
     * Get the value of panelActifGeneral
     */
    public function getPanelActifGeneral()
    {
        return $this->panelActifGeneral;
    }

    /**
     * Set the value of panelActifGeneral
     *
     * @return  self
     */
    public function setPanelActifGeneral($panelActifGeneral)
    {
        $this->panelActifGeneral = $panelActifGeneral;

        return $this;
    }

    /**
     * Get the value of panelActifConstruction
     */
    public function getPanelActifConstruction()
    {
        return $this->panelActifConstruction;
    }

    /**
     * Set the value of panelActifConstruction
     *
     * @return  self
     */
    public function setPanelActifConstruction($panelActifConstruction)
    {
        $this->panelActifConstruction = $panelActifConstruction;

        return $this;
    }

    /**
     * Get the value of panelActifCloture
     */
    public function getPanelActifCloture()
    {
        return $this->panelActifCloture;
    }

    /**
     * Set the value of panelActifCloture
     *
     * @return  self
     */
    public function setPanelActifCloture($panelActifCloture)
    {
        $this->panelActifCloture = $panelActifCloture;

        return $this;
    }

    /**
     * Détermine le(s) panel(s) actif(s) résultant en fonction du commentaire
     *
     * @param  Commentaire $commentaire
     * @return PanelActifSejour
     */
    public function setPanelActifEnFonctionDuCommentaire(Commentaire $commentaire)
    {
        $panelsAActiver = self::MAP_PANEL_ACTIF_PAR_COMMENTAIRE[$commentaire->getSujet()];
        if (array_key_exists("panelActifGeneral", $panelsAActiver)) {
            $this->panelActifGeneral = $panelsAActiver["panelActifGeneral"];
        }
        if (array_key_exists("panelActifConstruction", $panelsAActiver)) {
            $this->panelActifConstruction = $panelsAActiver["panelActifConstruction"];
        }
        if (array_key_exists("panelActifCloture", $panelsAActiver)) {
            $this->panelActifCloture = $panelsAActiver["panelActifCloture"];
        }
        return $this;
    }
}
