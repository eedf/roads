<?php

    namespace App\Utils;


/**
 * Récapitutlatif des effectifs d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ResumeEffectifsSejour    
{
    private bool $taux_encadrement_correct=true;
    private array $errors_taux_encadrement;

    //Enfants
    private int $nbTotalEnfants=0;
    private int $nbEnfantsStructureNonEEDF=0;
    private int $nbEnfantsLutins=0;
    private int $nbEnfantsLouveteaux=0;
    private int $nbEnfantsEcles=0;
    private int $nbEnfantsAines=0;

    private int $nbEnfantsPlus14ans=0;
    private int $nbEnfantsMoins14ans=0;

    private  bool  $auMoinsUnEnfantsDeMoinsDe14Ans=false;

    //Encadrants
    private int $nbTotalEncadrants=0; 
    private int $nbEncadrantsLutins=0;
    private int $nbEncadrantsLouveteaux=0;
    private int $nbEncadrantsEcles=0;
    private int $nbEncadrantsAines=0;
    private int $nbEncadrantsStructureNonEEDF=0;
    private int $nbEncadrantsEquipeAnimationAutres=0;
    private int $nbEncadrantsEquipeSupport=0;

    private int $nbEncNonDiplomes=0;
    private int $nbEncStagiaires=0;
    private int $nbEncDiplomes=0;
    private int $nbEncPSC1=0;
    private int $nbEncPermisB=0;

    private int $nbEnfantsSituationHandicap=0;

    public function __construct()
    {
        $this->errors_taux_encadrement = array();
    }
    /**
     * Get the value of nbEncadrantsEquipeAnimationAutres
     */ 
    public function getNbEncadrantsEquipeAnimationAutres():int
    {
        return $this->nbEncadrantsEquipeAnimationAutres;
    }

    /**
     * Set the value of nbEncadrantsEquipeAnimationAutres
     *
     * @return  self
     */ 
    public function setNbEncadrantsEquipeAnimationAutres( int $nbEncadrantsEquipeAnimationAutres)
    {
        $this->nbEncadrantsEquipeAnimationAutres = $nbEncadrantsEquipeAnimationAutres;

        return $this;
    }
    
    /**
     * Augmente d'autant le nombre d'encadrant·e·s dans l'équipe d'animation Autres
     * @param  int $nbEncadrantsEquipeAnimationAutres
     * @return void
     */
    public function addNbEncadrantsEquipeAnimationAutres( int $nbEncadrantsEquipeAnimationAutres)
    {
        $this->nbEncadrantsEquipeAnimationAutres += $nbEncadrantsEquipeAnimationAutres;

        return $this;
    }

    /**
     * Get the value of nbEncNonDiplomes
     */ 
    public function getNbEncNonDiplomes():int
    {
        return $this->nbEncNonDiplomes;
    }

    /**
     * Set the value of nbEncNonDiplomes
     *
     * @return  self
     */ 
    public function setNbEncNonDiplomes(int $nbEncNonDiplomes)
    {
        $this->nbEncNonDiplomes = $nbEncNonDiplomes;

        return $this;
    }
    /**
     * Augmente d'autant le nombre d'encadrant·e·s non dilpômés
     * @param  int $nbEncNonDiplomes
     * @return void
     */
    public function addNbEncNonDiplomes(int $nbEncNonDiplomes)
    {
        $this->nbEncNonDiplomes += $nbEncNonDiplomes;

        return $this;
    }

    /**
     * Get the value of nbEncPermisB
     */ 
    public function getNbEncPermisB():int
    {
        return $this->nbEncPermisB;
    }

    /**
     * Set the value of nbEncPermisB
     *
     * @return  self
     */ 
    public function setNbEncPermisB(int $nbEncPermisB)
    {
        $this->nbEncPermisB = $nbEncPermisB;

        return $this;
    }

    /**
     * Augmente d'autant le nombre d'encadrant·e·s avec le permis B
     * @param  int $nbEncPermisB
     * @return void
     */
    public function addNbEncPermisB(int $nbEncPermisB)
    {
        $this->nbEncPermisB += $nbEncPermisB;

        return $this;
    }

    /**
     * Get the value of nbEncPSC1
     */ 
    public function getNbEncPSC1():int
    {
        return $this->nbEncPSC1;
    }

    /**
     * Set the value of nbEncPSC1
     *
     * @return  self
     */ 
    public function setNbEncPSC1(int $nbEncPSC1)
    {
        $this->nbEncPSC1 = $nbEncPSC1;

        return $this;
    }

    /**
     * Augmente d'autant le nombre d'encadrant·e·s avec le PSC1
     * @param  int $nbEncPSC1
     * @return void
     */
    public function addNbEncPSC1(int $nbEncPSC1)
    {
        $this->nbEncPSC1 += $nbEncPSC1;

        return $this;
    }

    /**
     * Get the value of nbEncDiplomes
     */ 
    public function getNbEncDiplomes():int
    {
        return $this->nbEncDiplomes;
    }

    /**
     * Set the value of nbEncDiplomes
     *
     * @return  self
     */ 
    public function setNbEncDiplomes(int $nbEncDiplomes)
    {
        $this->nbEncDiplomes = $nbEncDiplomes;

        return $this;
    }

    /**
     * Augmente d'autant le nombre d'encadrant·e·s diplômés
     * @param  int $nbEncDiplomes
     * @return void
     */
    public function addNbEncDiplomes(int $nbEncDiplomes)
    {
        $this->nbEncDiplomes += $nbEncDiplomes;

        return $this;
    }

    /**
     * Get the value of nbEncStagiaires
     */ 
    public function getNbEncStagiaires():int
    {
        return $this->nbEncStagiaires;
    }

    /**
     * Set the value of nbEncStagiaires
     *
     * @return  self
     */ 
    public function setNbEncStagiaires(int $nbEncStagiaires)
    {
        $this->nbEncStagiaires = $nbEncStagiaires;

        return $this;
    }
    /**
     * Augmente d'autant le nombre d'encadrant·e·s stagiaires
     * @param  int $nbEncStagiaires
     * @return void
     */
    public function addNbEncStagiaires(int $nbEncStagiaires)
    {
        $this->nbEncStagiaires += $nbEncStagiaires;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsStructureNonEEDF
     */ 
    public function getNbEncadrantsStructureNonEEDF():int
    {
        return $this->nbEncadrantsStructureNonEEDF;
    }

    /**
     * Set the value of nbEncadrantsStructureNonEEDF
     *
     * @return  self
     */ 
    public function setNbEncadrantsStructureNonEEDF(int $nbEncadrantsStructureNonEEDF)
    {
        $this->nbEncadrantsStructureNonEEDF = $nbEncadrantsStructureNonEEDF;

        return $this;
    }
    /**
     * Augmente d'autant le nombre d'encadrant·e·s d'une structure non EEDF
     * @param  int $nbEncadrantsStructureNonEEDF
     * @return void
     */
    public function addNbEncadrantsStructureNonEEDF(int $nbEncadrantsStructureNonEEDF)
    {
        $this->nbEncadrantsStructureNonEEDF += $nbEncadrantsStructureNonEEDF;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsAines
     */ 
    public function getNbEncadrantsAines():int
    {
        return $this->nbEncadrantsAines;
    }

    /**
     * Set the value of nbEncadrantsAines
     *
     * @return  self
     */ 
    public function setNbEncadrantsAines(int $nbEncadrantsAines)
    {
        $this->nbEncadrantsAines = $nbEncadrantsAines;

        return $this;
    }

    /**
     * Augmente d'autant le nombre d'encadrant·e·s aîné·e·s
     * @param  int $nbEncadrantsAines
     * @return void
     */
    public function addNbEncadrantsAines(int $nbEncadrantsAines)
    {
        $this->nbEncadrantsAines += $nbEncadrantsAines;

        return $this;
    }

    /**
     * Get the value of nbTotalEnfants
     */ 
    public function getNbTotalEnfants():int
    {
        return $this->nbTotalEnfants;
    }

    /**
     * Set the value of nbTotalEnfants
     *
     * @return  self
     */ 
    public function setNbTotalEnfants(int $nbTotalEnfants)
    {
        $this->nbTotalEnfants = $nbTotalEnfants;

        return $this;
    }
    /**
     * Augmente d'autant le nombre total d'enfants
     * @param  int $nbTotalEnfants
     * @return void
     */
    public function addNbTotalEnfants(?int $nbTotalEnfants)
    {
        $this->nbTotalEnfants += $nbTotalEnfants;

        return $this;
    }

    /**
     * Get the value of nbEnfantsStructureNonEEDF
     */ 
    public function getNbEnfantsStructureNonEEDF():int
    {
        return $this->nbEnfantsStructureNonEEDF;
    }

    /**
     * Set the value of nbEnfantsStructureNonEEDF
     *
     * @return  self
     */ 
    public function setNbEnfantsStructureNonEEDF(int $nbEnfantsStructureNonEEDF)
    {
        $this->nbEnfantsStructureNonEEDF = $nbEnfantsStructureNonEEDF;

        return $this;
    }
    /**
     * Augmente d'autant le nombre total d'enfants de structures non EEDF
     * @param  int $nbEnfantsStructureNonEEDF
     * @return void
     */
    public function addNbEnfantsStructureNonEEDF(?int $nbEnfantsStructureNonEEDF)
    {
        $this->nbEnfantsStructureNonEEDF += $nbEnfantsStructureNonEEDF;

        return $this;
    }

    /**
     * Get the value of taux_encadrement_correct
     */ 
    public function getTaux_encadrement_correct():bool
    {
        return $this->taux_encadrement_correct;
    }

    /**
     * Set the value of taux_encadrement_correct
     *
     * @return  self
     */ 
    public function setTaux_encadrement_correct(bool $taux_encadrement_correct)
    {
        $this->taux_encadrement_correct = $taux_encadrement_correct;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsEcles
     */ 
    public function getNbEncadrantsEcles():int
    {
        return $this->nbEncadrantsEcles;
    }

    /**
     * Set the value of nbEncadrantsEcles
     *
     * @return  self
     */ 
    public function setNbEncadrantsEcles(int $nbEncadrantsEcles)
    {
        $this->nbEncadrantsEcles = $nbEncadrantsEcles;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'encadrant·e·s de la branche écléE
     * @param  int $nbEncadrantsEcles
     * @return void
     */
    public function addNbEncadrantsEcles(int $nbEncadrantsEcles)
    {
        $this->nbEncadrantsEcles += $nbEncadrantsEcles;

        return $this;
    }

    /**
     * Get the value of nbTotalEncadrants
     */ 
    public function getNbTotalEncadrants():int
    {
        return $this->nbTotalEncadrants;
    }

    /**
     * Set the value of nbTotalEncadrants
     *
     * @return  self
     */ 
    public function setNbTotalEncadrants(int $nbTotalEncadrants)
    {
        $this->nbTotalEncadrants = $nbTotalEncadrants;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'encadrant·e·s
     * @param  int $nbTotalEncadrants
     * @return void
     */
    public function addNbTotalEncadrants(int $nbTotalEncadrants)
    {
        $this->nbTotalEncadrants += $nbTotalEncadrants;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsLouveteaux
     */ 
    public function getNbEncadrantsLouveteaux():int
    {
        return $this->nbEncadrantsLouveteaux;
    }

    /**
     * Set the value of nbEncadrantsLouveteaux
     *
     * @return  self
     */ 
    public function setNbEncadrantsLouveteaux(int $nbEncadrantsLouveteaux)
    {
        $this->nbEncadrantsLouveteaux = $nbEncadrantsLouveteaux;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'encadrant·e·s de la branche louveteaux
     * @param  int $nbEncadrantsLouveteaux
     * @return void
     */
    public function addNbEncadrantsLouveteaux(int $nbEncadrantsLouveteaux)
    {
        $this->nbEncadrantsLouveteaux += $nbEncadrantsLouveteaux;

        return $this;
    }

    /**
     * Get the value of nbEnfantsLouveteaux
     */ 
    public function getNbEnfantsLouveteaux():int
    {
        return $this->nbEnfantsLouveteaux;
    }

    /**
     * Set the value of nbEnfantsLouveteaux
     *
     * @return  self
     */ 
    public function setNbEnfantsLouveteaux(int $nbEnfantsLouveteaux)
    {
        $this->nbEnfantsLouveteaux = $nbEnfantsLouveteaux;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'enfants de la branche louveteaux
     * @param  int $nbEnfantsLouveteaux
     * @return void
     */
    public function addNbEnfantsLouveteaux(int $nbEnfantsLouveteaux)
    {
        $this->nbEnfantsLouveteaux += $nbEnfantsLouveteaux;

        return $this;
    }

    /**
     * Get the value of nbEnfantsEcles
     */ 
    public function getNbEnfantsEcles():int
    {
        return $this->nbEnfantsEcles;
    }

    /**
     * Set the value of nbEnfantsEcles
     *
     * @return  self
     */ 
    public function setNbEnfantsEcles(int $nbEnfantsEcles)
    {
        $this->nbEnfantsEcles = $nbEnfantsEcles;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'enfants de la branche éclées
     * @param  int $nbEnfantsEcles
     * @return void
     */
    public function addNbEnfantsEcles(int $nbEnfantsEcles)
    {
        $this->nbEnfantsEcles += $nbEnfantsEcles;

        return $this;
    }

    /**
     * Get the value of auMoinsUnEnfantsDeMoinsDe14Ans
     */ 
    public function getAuMoinsUnEnfantsDeMoinsDe14Ans():bool
    {
        return $this->auMoinsUnEnfantsDeMoinsDe14Ans;
    }

    /**
     * Set the value of auMoinsUnEnfantsDeMoinsDe14Ans
     *
     * @return  self
     */ 
    public function setAuMoinsUnEnfantsDeMoinsDe14Ans(bool $auMoinsUnEnfantsDeMoinsDe14Ans)
    {
        $this->auMoinsUnEnfantsDeMoinsDe14Ans = $auMoinsUnEnfantsDeMoinsDe14Ans;

        return $this;
    }

    /**
     * Get the value of nbEnfantsLutins
     */ 
    public function getNbEnfantsLutins()
    {
        return $this->nbEnfantsLutins;
    }

    /**
     * Set the value of nbEnfantsLutins
     *
     * @return  self
     */ 
    public function setNbEnfantsLutins( int $nbEnfantsLutins)
    {
        $this->nbEnfantsLutins = $nbEnfantsLutins;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'enfants de la branche lutin
     * @param  int $nbEnfantsLutins
     * @return void
     */
    public function addNbEnfantsLutins( int $nbEnfantsLutins)
    {
        $this->nbEnfantsLutins += $nbEnfantsLutins;

        return $this;
    }

    /**
     * Get the value of nbEnfantsAines
     */ 
    public function getNbEnfantsAines()
    {
        return $this->nbEnfantsAines;
    }

    /**
     * Set the value of nbEnfantsAines
     *
     * @return  self
     */ 
    public function setNbEnfantsAines(int $nbEnfantsAines)
    {
        $this->nbEnfantsAines = $nbEnfantsAines;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'enfants de la branche aînée
     * @param  int $nbEnfantsAines
     * @return void
     */
    public function addNbEnfantsAines(int $nbEnfantsAines)
    {
        $this->nbEnfantsAines += $nbEnfantsAines;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsLutins
     */ 
    public function getNbEncadrantsLutins()
    {
        return $this->nbEncadrantsLutins;
    }

    /**
     * Set the value of nbEncadrantsLutins
     *
     * @return  self
     */ 
    public function setNbEncadrantsLutins(int $nbEncadrantsLutins)
    {
        $this->nbEncadrantsLutins = $nbEncadrantsLutins;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'encadrant·e·s de la branche lutin
     * @param  int $nbEncadrantsLutins
     * @return void
     */
    public function addNbEncadrantsLutins(int $nbEncadrantsLutins)
    {
        $this->nbEncadrantsLutins += $nbEncadrantsLutins;

        return $this;
    }

    /**
     * Get the value of errors_taux_encadrement
     */ 
    public function getErrors_taux_encadrement():array
    {
        return $this->errors_taux_encadrement;
    }

    /**
     * Set the value of errors_taux_encadrement
     *
     * @return  self
     */ 
    public function setErrors_taux_encadrement(array $errors_taux_encadrement)
    {
        $this->errors_taux_encadrement = $errors_taux_encadrement;

        return $this;
    }

    public function addErrors_taux_encadrement(string $error_taux_encadrement)
    {
        $this->errors_taux_encadrement[] = $error_taux_encadrement;

        return $this;
    }

    /**
     * Get the value of nbEnfantsSituationHandicap
     */ 
    public function getNbEnfantsSituationHandicap()
    {
        return $this->nbEnfantsSituationHandicap;
    }

    /**
     * Set the value of nbEnfantsSituationHandicap
     *
     * @return  self
     */ 
    public function setNbEnfantsSituationHandicap($nbEnfantsSituationHandicap)
    {
        $this->nbEnfantsSituationHandicap = $nbEnfantsSituationHandicap;

        return $this;
    }

    /**
     * Get the value of nbEnfantsPlus14ans
     */ 
    public function getNbEnfantsPlus14ans()
    {
        return $this->nbEnfantsPlus14ans;
    }

    /**
     * Set the value of nbEnfantsPlus14ans
     *
     * @return  self
     */ 
    public function setNbEnfantsPlus14ans($nbEnfantsPlus14ans)
    {
        $this->nbEnfantsPlus14ans = $nbEnfantsPlus14ans;

        return $this;
    }
    /**
     * Augmente d'autant le nombre total d'enfant de plus de 14 ans
     * @param  int $nbEnfantsPlus14ans
     * @return void
     */
    public function addNbEnfantsPlus14ans($nbEnfantsPlus14ans)
    {
        $this->nbEnfantsPlus14ans += $nbEnfantsPlus14ans;

        return $this;
    }

    /**
     * Get the value of nbEnfantsMoins14ans
     */ 
    public function getNbEnfantsMoins14ans()
    {
        return $this->nbEnfantsMoins14ans;
    }

    /**
     * Set the value of nbEnfantsMoins14ans
     *
     * @return  self
     */ 
    public function setNbEnfantsMoins14ans($nbEnfantsMoins14ans)
    {
        $this->nbEnfantsMoins14ans = $nbEnfantsMoins14ans;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'enfant de moins de 14 ans
     * @param  int $nbEnfantsMoins14ans
     * @return void
     */
    public function addNbEnfantsMoins14ans($nbEnfantsMoins14ans)
    {
        $this->nbEnfantsMoins14ans += $nbEnfantsMoins14ans;

        return $this;
    }

    /**
     * Get the value of nbEncadrantsEquipeSupport
     */ 
    public function getNbEncadrantsEquipeSupport()
    {
        return $this->nbEncadrantsEquipeSupport;
    }

    /**
     * Set the value of nbEncadrantsEquipeSupport
     *
     * @return  self
     */ 
    public function setNbEncadrantsEquipeSupport($nbEncadrantsEquipeSupport)
    {
        $this->nbEncadrantsEquipeSupport = $nbEncadrantsEquipeSupport;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total d'encadrant·e·s dans l'équipe support
     * @param  int $nbEncadrantsEquipeSupport
     * @return void
     */
    public function addNbEncadrantsEquipeSupport($nbEncadrantsEquipeSupport)
    {
        $this->nbEncadrantsEquipeSupport += $nbEncadrantsEquipeSupport;

        return $this;
    }
}
