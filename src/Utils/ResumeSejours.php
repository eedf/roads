<?php

namespace App\Utils;

use App\Entity\Sejour;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Résumé chiffré des séjours
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ResumeSejours
{

    private int $nbTotalSejours = 0;
    private int $nbTotalSejoursNonClotures = 0;
    private array $arrayNbSejourParEtat = [];
    private int $nbSejourUnite = 0;
    private int $nbSejourGroupe = 0;
    private int $nbSejourRegroupement = 0;
    private int $nbSejourRassemblement = 0;
    private int $nbSejourInternational = 0;
    private int $nbSejourAccueilEtranger = 0;
    private int $nbSejourOuvert = 0;
    private int $nbSejourColosApprenantes = 0;
    private int $nbSejourAvecAppui = 0;
    private int $nbSejourItinerant = 0;


    /**
     * Gestionnaire d'état symfony
     *
     * @var WorkflowInterface
     */
    public $sejourWorkflow;

    /**
     * constructeur
     *
     * @param  WorkflowInterface $sejourWorkflow
     * @return void
     */
    public function __construct(WorkflowInterface $sejourWorkflow)
    {
        $this->sejourWorkflow = $sejourWorkflow;
    }

    /**
     * Get the value of nbTotalSejoursNonClotures
     */
    public function getNbTotalSejoursNonClotures()
    {
        return $this->nbTotalSejoursNonClotures;
    }

    /**
     * Set the value of nbTotalSejoursNonClotures
     *
     * @return  self
     */
    public function setNbTotalSejoursNonClotures($nbTotalSejoursNonClotures)
    {
        $this->nbTotalSejoursNonClotures = $nbTotalSejoursNonClotures;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours
     * @param  int $nbTotalSejoursNonClotures
     * @return void
     */
    public function addNbTotalSejoursNonClotures($nbTotalSejoursNonClotures)
    {
        $this->nbTotalSejoursNonClotures += $nbTotalSejoursNonClotures;

        return $this;
    }


    /**
     * Get the value of nbTotalSejours
     */
    public function getNbTotalSejours()
    {
        return $this->nbTotalSejours;
    }

    /**
     * Set the value of nbTotalSejours
     *
     * @return  self
     */
    public function setNbTotalSejours($nbTotalSejours)
    {
        $this->nbTotalSejours = $nbTotalSejours;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours
     * @param  int $nbTotalSejours
     * @return void
     */
    public function addNbTotalSejours($nbTotalSejours)
    {
        $this->nbTotalSejours += $nbTotalSejours;

        return $this;
    }

    /**
     * Get the value of arrayNbSejourParEtat
     */
    public function getArrayNbSejourParEtat()
    {
        return $this->arrayNbSejourParEtat;
    }

    /**
     * Set the value of arrayNbSejourParEtat
     *
     * @return  self
     */
    public function setArrayNbSejourParEtat($arrayNbSejourParEtat)
    {
        $this->arrayNbSejourParEtat = $arrayNbSejourParEtat;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours dans un état spécifique
     * @param  String $etat
     * @param  int $nbSejour
     * @return void
     */
    public function addNbSejourParEtat(String $etat, int $nbSejour)
    {
        if (!array_key_exists($etat, $this->arrayNbSejourParEtat)) {
            $this->arrayNbSejourParEtat[$etat] = $nbSejour;
        } else {
            $this->arrayNbSejourParEtat[$etat] = $this->arrayNbSejourParEtat[$etat] + $nbSejour;
        }
    }

    /**
     * Get the value of nbSejourUnite
     */
    public function getNbSejourUnite()
    {
        return $this->nbSejourUnite;
    }

    /**
     * Set the value of nbSejourUnite
     *
     * @return  self
     */
    public function setNbSejourUnite($nbSejourUnite)
    {
        $this->nbSejourUnite = $nbSejourUnite;

        return $this;
    }
    /**
     * Augmente d'autant le nombre total de séjours d'unité
     * @param  int $nbSejourUnite
     * @return void
     */
    public function addNbSejourUnite($nbSejourUnite)
    {
        $this->nbSejourUnite += $nbSejourUnite;

        return $this;
    }

    /**
     * Get the value of nbSejourGroupe
     */
    public function getNbSejourGroupe()
    {
        return $this->nbSejourGroupe;
    }

    /**
     * Set the value of nbSejourGroupe
     *
     * @return  self
     */
    public function setNbSejourGroupe($nbSejourGroupe)
    {
        $this->nbSejourGroupe = $nbSejourGroupe;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours de groupe
     * @param  int $nbSejourGroupe
     * @return void
     */
    public function addNbSejourGroupe($nbSejourGroupe)
    {
        $this->nbSejourGroupe += $nbSejourGroupe;

        return $this;
    }

    /**
     * Get the value of nbSejourRegroupement
     */
    public function getNbSejourRegroupement()
    {
        return $this->nbSejourRegroupement;
    }

    /**
     * Set the value of nbSejourRegroupement
     *
     * @return  self
     */
    public function setNbSejourRegroupement($nbSejourRegroupement)
    {
        $this->nbSejourRegroupement = $nbSejourRegroupement;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours de regroupement
     * @param  int $nbSejourRegroupement
     * @return void
     */
    public function addNbSejourRegroupement($nbSejourRegroupement)
    {
        $this->nbSejourRegroupement += $nbSejourRegroupement;

        return $this;
    }

    /**
     * Get the value of nbSejourRassemblement
     */
    public function getNbSejourRassemblement()
    {
        return $this->nbSejourRassemblement;
    }

    /**
     * Set the value of nbSejourRassemblement
     *
     * @return  self
     */
    public function setNbSejourRassemblement($nbSejourRassemblement)
    {
        $this->nbSejourRassemblement = $nbSejourRassemblement;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de rassemblements
     * @param  int $nbSejourRassemblement
     * @return void
     */
    public function addNbSejourRassemblement($nbSejourRassemblement)
    {
        $this->nbSejourRassemblement += $nbSejourRassemblement;

        return $this;
    }

    /**
     * Get the value of nbSejourInternational
     */
    public function getNbSejourInternational()
    {
        return $this->nbSejourInternational;
    }

    /**
     * Set the value of nbSejourInternational
     *
     * @return  self
     */
    public function setNbSejourInternational($nbSejourInternational)
    {
        $this->nbSejourInternational = $nbSejourInternational;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjour internationaux
     * @param  int $nbSejourInternational
     * @return void
     */
    public function addNbSejourInternational($nbSejourInternational)
    {
        $this->nbSejourInternational += $nbSejourInternational;

        return $this;
    }

    /**
     * Get the value of nbSejourAccueilEtranger
     */
    public function getNbSejourAccueilEtranger()
    {
        return $this->nbSejourAccueilEtranger;
    }

    /**
     * Set the value of nbSejourAccueilEtranger
     *
     * @return  self
     */
    public function setNbSejourAccueilEtranger($nbSejourAccueilEtranger)
    {
        $this->nbSejourAccueilEtranger = $nbSejourAccueilEtranger;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjour avec accueil d'étrangers
     * @param  int $nbSejourAccueilEtranger
     * @return void
     */
    public function addNbSejourAccueilEtranger($nbSejourAccueilEtranger)
    {
        $this->nbSejourAccueilEtranger += $nbSejourAccueilEtranger;

        return $this;
    }
    /**
     * Get the value of nbSejourOuvert
     */
    public function getNbSejourOuvert()
    {
        return $this->nbSejourOuvert;
    }

    /**
     * Set the value of nbSejourOuvert
     *
     * @return  self
     */
    public function setNbSejourOuvert($nbSejourOuvert)
    {
        $this->nbSejourOuvert = $nbSejourOuvert;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours ouverts
     * @param  int $nbSejourOuvert
     * @return void
     */
    public function addNbSejourOuvert($nbSejourOuvert)
    {
        $this->nbSejourOuvert += $nbSejourOuvert;

        return $this;
    }

    /**
     * Get the value of nbSejourColosApprenantes
     */
    public function getNbSejourColosApprenantes()
    {
        return $this->nbSejourColosApprenantes;
    }

    /**
     * Set the value of nbSejourColosApprenantes
     *
     * @return  self
     */
    public function setNbSejourColosApprenantes($nbSejourColosApprenantes)
    {
        $this->nbSejourColosApprenantes = $nbSejourColosApprenantes;

        return $this;
    }

    public function adNbSejourColosApprenantes($nbSejourColosApprenantes)
    {
        $this->nbSejourColosApprenantes += $nbSejourColosApprenantes;

        return $this;
    }

    /**
     * Get the value of nbSejourAvecAppui
     */
    public function getNbSejourAvecAppui()
    {
        return $this->nbSejourAvecAppui;
    }

    /**
     * Set the value of nbSejourAvecAppui
     *
     * @return  self
     */
    public function setNbSejourAvecAppui($nbSejourAvecAppui)
    {
        $this->nbSejourAvecAppui = $nbSejourAvecAppui;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours avec appui
     * @param  int $nbSejourAvecAppui
     * @return void
     */
    public function addNbSejourAvecAppui($nbSejourAvecAppui)
    {
        $this->nbSejourAvecAppui += $nbSejourAvecAppui;

        return $this;
    }

    /**
     * Get the value of nbSejourItinerant
     */
    public function getNbSejourItinerant()
    {
        return $this->nbSejourItinerant;
    }

    /**
     * Set the value of nbSejourItinerant
     *
     * @return  self
     */
    public function setNbSejourItinerant($nbSejourItinerant)
    {
        $this->nbSejourItinerant = $nbSejourItinerant;

        return $this;
    }

    /**
     * Augmente d'autant le nombre total de séjours itinérants
     * @param  int $nbSejourItinerant
     * @return void
     */
    public function addNbSejourItinerant($nbSejourItinerant)
    {
        $this->nbSejourItinerant += $nbSejourItinerant;

        return $this;
    }

    /**
     * Ajoute un séjour au résumé des séjours
     *
     * @param Sejour $sejour
     * @return void
     */
    public function ajouterSejour(Sejour $sejour)
    {
        $statusSejour = $this->sejourWorkflow->getMarking($sejour);
        $this->addNbTotalSejours(1);
        if (!$statusSejour->has(Sejour::STATUT_SEJOUR_CLOTURE)) {
            $this->addNbTotalSejoursNonClotures(1);


            if ($sejour->getTypeSejour() == Sejour::TYPE_SEJOUR_UNITE) {
                $this->addNbSejourUnite(1);
            } else if ($sejour->getTypeSejour() == Sejour::TYPE_SEJOUR_GROUPE) {
                $this->addNbSejourGroupe(1);
            } else if ($sejour->getTypeSejour() == Sejour::TYPE_SEJOUR_REGROUPEMENT) {
                $this->addNbSejourRegroupement(1);
            } else if ($sejour->getTypeSejour() == Sejour::TYPE_SEJOUR_RASSEMBLEMENT) {
                $this->addNbSejourRassemblement(1);
            }

            if ($sejour->getInternational()) {
                $this->addNbSejourInternational(1);
            }

            if ($sejour->getSejourFranceAvecAccueilEtranger()) {
                $this->addNbSejourAccueilEtranger(1);
            }

            if ($sejour->getSejourItinerant()) {
                $this->addNbSejourItinerant(1);
            }
            if ($sejour->getSejourOuvert()) {
                $this->addNbSejourOuvert(1);
            }
            if ($sejour->getColoApprenante()) {
                $this->adNbSejourColosApprenantes(1);
            }

            if ($sejour->getDispositifAppui() !== "Non") {
                $this->addNbSejourAvecAppui(1);
            }
        }
        foreach ($sejour->getStatus() as $etat => $nb) {
            $this->addNbSejourParEtat($etat, 1);
        }
    }
}
