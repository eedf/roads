<?php

namespace App\Form;

use App\Entity\ValidationInternationaleSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des informations de validation internationale d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ValidationInternationaleSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentaires', CKEditorType::class, [
                'required' =>true,
                'label' => "Commentaires généraux sur la validation",
            ])
            ->add('estValide',ChoiceType::class,[
                'choices'=>[
                    "Favorable"=>true,
                    "Défavorable"=>false,
                ],
                'expanded'=>true,
                'multiple'=>false,
                'label'=>"Avis",
            ])
            ->add('documentValidationInternationale', DocumentType::class, [
                'required' =>false,
                'label' => "Insérer le document de validation internationale",
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ValidationInternationaleSejour::class,
        ]);
    }
}
