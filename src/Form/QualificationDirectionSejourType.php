<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des qualifiaction du·de la direct·eur·rice du séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class QualificationDirectionSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('qualificationDirection',QualificationDirectionType::class,[
            
            'label'=>false,
        ])

        ->add('save', SubmitType::class,[
            'label'=>'Enregistrer'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
