<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects budgétaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectBudgetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('aspectBudgetSejour', AspectBudgetSejourType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
