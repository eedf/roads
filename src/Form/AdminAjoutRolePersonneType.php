<?php

namespace App\Form;

use App\Entity\Personne;
use App\Entity\Sejour;
use App\Entity\Structure;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'ajout de rôles à une personne (administration)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AdminAjoutRolePersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', ChoiceType::class, [
                'choices' => [
                    Personne::LABEL_ROLE_METIER_VALIDATION_NATIONALE => Personne::ROLE_VALIDATION_NATIONALE,
                    Personne::LABEL_ROLE_METIER_VALIDATION_INTERNATIONALE => Personne::ROLE_VALIDATION_INTERNATIONALE,
                    Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR => Personne::LABEL_ROLE_METIER_VALIDATION_SEJOUR,
                    Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR => Personne::LABEL_ROLE_METIER_VALIDATION_DELEGUEE_SEJOUR,
                    Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR => Personne::LABEL_ROLE_METIER_SUIVI_SEJOUR,
                    Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR => Personne::LABEL_ROLE_METIER_ORGANISATION_SEJOUR,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => 'Rôle'
            ])
            ->add('structure', EntityType::class, [
                'class' => Structure::class,
                'mapped' => false,
                'choice_label' => function (Structure $structure) {
                    if ($structure->getStatut() == Structure::STATUT_STRUCTURE_RATTACHEE) {
                        return $structure->getNom() . " (" . $structure->getType() . " sous tutelle)";
                    } else {
                        return $structure->getNom() . " (" . $structure->getType() . ")";
                    }
                },
                'label' => 'Sructure',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.statut in (:statutsActifs)')
                        ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
                        ->orderBy('u.nom', 'ASC');
                }
            ])
            ->add('all', CheckboxType::class, [
                'label' => 'Toute la structure',
                'required' => false
            ])
            ->add('sejour', EntityType::class, [
                'class' => Sejour::class,
                'mapped' => false,
                'choice_label' => 'intitule',
                'label' => 'Séjour',
                'choice_filter' => 'isStatusVisible',

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);

        $builder->get('structure')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();
                $structure = $form->getData();
                $this->addSejoursFieldForStructureSelection($form->getParent(), $structure);
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                // On crée les 2 champs en les laissant vide (champs utilisé pour le JavaScript)
                $this->addSejoursFieldForStructureSelection($form, null);
            }
        );
    }

    private function addSejoursFieldForStructureSelection(FormInterface $form, ?Structure $structure)
    {
        $form->add('sejour', EntityType::class, [
            'class' => Sejour::class,
            'choice_label' => 'intitule',
            'required'   => false,
            'label' => 'Séjour',
            'placeholder' => $structure ? 'Sélectionnez le séjour' : 'Sélectionnez d\'abord la structure',
            'choices' => $structure ? $structure->getSejoursOrganises() : [],
            'choice_filter' => 'isStatusVisible',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
