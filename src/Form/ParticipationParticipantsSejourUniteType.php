<?php

namespace App\Form;

use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\ParticipationSejourUnite;
use App\Repository\AdherentRepository;
use App\Repository\PersonneRepository;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de sélection des adhérent·e·s participant un séjour sur une unité EEDF
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ParticipationParticipantsSejourUniteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Equipe $equipe */
        $equipe = $options['equipe'];
        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('participantsAdherents', EntityType::class, [
                'class' => Personne::class,
                /** @var Personne $personne */
                'choice_label' => function (Personne $personne) {
                    return $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ")";
                },
                'query_builder' => function (PersonneRepository $personneRepository) use ($equipe, $sejour) {
                    return $personneRepository->createQueryBuilder('p')
                        ->orderBy('p.nom', 'ASC')
                        ->join('p.fonctions', 'f')
                        ->join('f.equipe', 'e')
                        ->andWhere('e.id = :equipe_id')
                        ->andWhere('f.type = :type_participant')
                        ->andWhere('f.statut = :statut_participant')
                        ->andWhere('f.dateDebut <= :dateDebutSejour')
                        ->andWhere('f.dateFin >= :dateDebutSejour')
                        ->setParameter('equipe_id', $equipe->getId())
                        ->setParameter('dateDebutSejour', $sejour->getDateDebut())
                        ->setParameter('statut_participant', Fonction::CATEGORY_FONCTION_PARTICIPANT)
                        ->setParameter('type_participant', Fonction::TYPE_FONCTION_PARTICIPANT_SIMPLE);
                },
                'expanded' => true,
                'multiple' => true,
                'label' => "Ajouter ou enlever des participant·e·s adhérent·e·s de l'unité au séjour",
            ])
            ->add('participantsNonAdherents', CollectionType::class, [
                'mapped' => true,
                'entry_type' => ParticipantNonAdherentEEDFType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParticipationSejourUnite::class,
            'equipe' => null,
            'sejour' => null
        ]);
    }
}
