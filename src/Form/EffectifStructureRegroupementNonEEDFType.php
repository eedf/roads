<?php

namespace App\Form;

use App\Entity\StructureRegroupementSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des effectifs d'une structure regroupée n'appartenant pas aux EEDF 
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class EffectifStructureRegroupementNonEEDFType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbEnfantMoins14StructNonEEDF',IntegerType::class,[
                'label' => "Nombre de particpant·e·s de moins de 14 ans"
            ])
            ->add('nbEnfantsPlus14StructNonEEDF',IntegerType::class,[
                'label' => "Nombre de particpant·e·s de plus de 14 ans"
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StructureRegroupementSejour::class,
        ]);
    }
}
