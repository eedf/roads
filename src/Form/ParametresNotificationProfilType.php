<?php

namespace App\Form;

use App\Entity\Adherent;
use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de modification des paramètres de notification d'un·e adhérent·e
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ParametresNotificationProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('frequenceNotification', ChoiceType::class, [
                'choices' => [
                    Personne::FREQUENCE_NOTIFICATION_JAMAIS => Personne::FREQUENCE_NOTIFICATION_JAMAIS,
                    Personne::FREQUENCE_NOTIFICATION_TEMPS_REEL => Personne::FREQUENCE_NOTIFICATION_TEMPS_REEL,
                    Personne::FREQUENCE_NOTIFICATION_QUOTIDIEN => Personne::FREQUENCE_NOTIFICATION_QUOTIDIEN,
                    Personne::FREQUENCE_NOTIFICATION_HEBDO => Personne::FREQUENCE_NOTIFICATION_HEBDO,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Paramètres de notifications par mail",
                'help' => "Fréquence des envois"
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
