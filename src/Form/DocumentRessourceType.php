<?php

namespace App\Form;

use App\Entity\DocumentRessource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un document ressource de ROADS (animation)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DocumentRessourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    DocumentRessource::TYPE_FICHIER => DocumentRessource::TYPE_FICHIER,
                    DocumentRessource::TYPE_LIEN => DocumentRessource::TYPE_LIEN,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => false,
            ])
            ->add('lien', UrlType::class, [
                'required' => true,
                'label' => "Lien",
            ])
            ->add('document', DocumentType::class, [
                'required' => true,
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentRessource::class,
        ]);
    }
}
