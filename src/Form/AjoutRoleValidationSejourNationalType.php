<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Entity\Structure;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'ajout du rôle de délégation de validation d'un séjour à dimension nationale (*ex de CPN")
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AjoutRoleValidationSejourNationalType  extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder
            ->add('personne', PersonneSelectTextType::class, [
                'label' => "Sélection de la personne",
            ])
            ->add('structure', EntityType::class, [
                'class' => Structure::class,
                'mapped' => false,
                'choice_label' => function (Structure $structure) {
                    if ($structure->getStatut() == Structure::STATUT_STRUCTURE_RATTACHEE) {
                        return $structure->getNom() . " (" . $structure->getType() . " sous tutelle)";
                    } else {
                        return $structure->getNom() . " (" . $structure->getType() . ")";
                    }
                },
                'label' => 'Sructure',
                'query_builder' => function (EntityRepository $er) {
                    return $er->findStructuresAValidationNationale();
                },
                'placeholder' => "Sélectionnez la structure",
                'choice_filter' => 'isActiveOuSousTutelle',

            ])
            ->add('all', CheckboxType::class, [
                'label' => 'Tous les séjours en cours de la structure',
                'required' => false
            ])
            ->add('sejour', EntityType::class, [
                'class' => Sejour::class,
                'mapped' => false,
                'choice_label' => 'intitule',
                'label' => 'Séjour'

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);

        $builder->get('structure')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();
                $structure = $form->getData();
                //On rend le champ séjour obligatoire uniquement si la case all est cochee
                $champSejourRequired = !$form->getParent()->get('all');
                $this->addSejoursFieldForStructureSelection($form->getParent(), $structure, $champSejourRequired);
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                // On crée le champ sejour en le laissant vide (champ utilisé pour le JavaScript)
                $this->addSejoursFieldForStructureSelection($form, null, true);
            }
        );
    }

    private function addSejoursFieldForStructureSelection(FormInterface $form, ?Structure $structure, bool $champSejourRequired)
    {
        $form->add('sejour', EntityType::class, [
            'class' => Sejour::class,
            'choice_label' => 'intitule',
            'required'   => $champSejourRequired,
            'label' => 'Séjour',
            'placeholder' => $structure ? 'Sélectionnez le séjour' : 'Sélectionnez d\'abord la structure',
            'choices' => $structure ? $structure->getSejoursOrganises() : [],
            'choice_filter' => 'isStatusVisible',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'structure' => null
        ]);
    }
}
