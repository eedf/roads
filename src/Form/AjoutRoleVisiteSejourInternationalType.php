<?php

namespace App\Form;

use App\Entity\Sejour;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'ajout du rôle de visite pour un séjour international
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AjoutRoleVisiteSejourInternationalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder
            ->add('personne', PersonneSelectTextType::class, [
                'label' => "Sélection de la personne",
            ])
            ->add('sejour', EntityType::class, [
                'class' => Sejour::class,
                'choice_label' => function (Sejour $sejour) {
                    return $sejour->getintitule() . " (" . $sejour->getStructureOrganisatrice()->getNom() . ")";
                },
                'label' => 'Séjour',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.sejourFranceAvecAccueilEtranger = TRUE')
                        ->orWhere('s.international = TRUE')
                        ->orderBy('s.intitule', 'ASC');
                },
                'placeholder' =>  "Sélectionnez le séjour",
                'choice_filter' => 'isVisitable'

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
