<?php

namespace App\Form;

use App\Entity\Localisation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'une localisation de séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class LocalisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rechercheAdresse', TextType::class, [
                'label' => 'Recherche textuelle',
                'help' => "Saisissez l'adresse du lieu",
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'js-user-autocomplete rechercheAdresse',
                    'data-autocomplete-url' => 'https://nominatim.openstreetmap.org/search',
                    'style' => 'display:block'
                ],

            ])
            ->add('adresse', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'adresse',
                ]
            ])
            ->add('complementAdresse', TextType::class, [
                'required' => false,
                'label' => "Complément d'adresse",
            ])
            ->add('codePostalCommune', TextType::class, [
                'required' => true,
                'label' => "Code postal",
                'attr' => [
                    'class' => 'codePostalCommune',
                ]
            ])
            ->add('nomCommune', TextType::class, [
                'required' => true,
                'label' => "Commune",
                'attr' => [
                    'class' => 'nomCommune',
                ]
            ])
            ->add('departement', HiddenType::class, [
                'required' => true,
                'label' => "Département",
                'attr' => [
                    'class' => 'departement',
                ]
            ])
            ->add('pays', TextType::class, [
                'mapped' => true,
                'attr' => [
                    'class' => 'pays',
                    'readonly' => true
                ]
            ])
            ->add('coordonneesGeo', PointGeoType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'class' => 'coordonnees GPS',

                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Localisation::class,
        ]);
    }
}
