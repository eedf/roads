<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;

/**
 * Formulaire des dates du séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DatesLieuSejourDatesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sejour=$options['sejour'];
        $builder
        ->add('dateDebut', DateType::class,[
            'label'=>"Date de début de séjour",
            'help' => "Est attendue ici la date à partir de laquelle débute la présence des participant·e·s sur le site du séjour. Même si c'est un \"précamp\" où ne seront présent·e·s que quelques participant·e·s.",
            'widget' => 'single_text',
            'html5' => false,
            'format'=>"dd/MM/yyyy",
            'attr'=>['class' => 'form-control js-datepicker'],
            'constraints'=> 
            [
                new LessThan([
                    'propertyPath' => 'parent.all[dateFin].data',
                    'message' => 'la date de début doit être avant la date de fin'
                ]),
            ]

        ])
        ->add('dateFin', DateType::class,[
            'label'=>"Date de fin de séjour",
            'help' => "Nécessaire pour déclarer la fiche complémentaire sur TAM. Un séjour doit forcément faire au moins 5 jours et 4 nuits pour être déclaré en tant que séjour. Si le séjour est plus court, c'est alors un mini-séjour (ou mini-camp). Il s'inscrit donc dans la déclaration trimestrielle.",
            'widget' => 'single_text',
            'html5' => false,
            'format'=>"dd/MM/yyyy",
            'attr'=>['class' => 'form-control js-datepicker'],
            'constraints'=> 
            [
                new GreaterThan([
                    'propertyPath' => 'parent.all[dateDebut].data',
                    'message' => 'la date de fin doit être après la date de début'
                ]),
            ]
            
        ])

        ->add('save', SubmitType::class,[
            'label'=>'Enregistrer'
        ]);
}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}

