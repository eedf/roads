<?php

namespace App\Form;

use App\Entity\VisiteSejourEtat;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'une visite de l'état sur un séjour (clôture)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class VisiteSejourEtatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, [
                'label' => "Date de la visite",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],

            ])
            ->add('serviceConcerne', TextType::class, [
                'label' => "Identification du service concerné",
            ])
            ->add('retours', CKEditorType::class, [
                'required' => true,
                'label' => "Retours sur cette visite"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VisiteSejourEtat::class,
        ]);
    }
}
