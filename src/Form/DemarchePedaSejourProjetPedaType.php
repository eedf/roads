<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects pédagogique d'un séjour (construction de séjour)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DemarchePedaSejourProjetPedaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('documentProjetPeda', DocumentType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'sejour' => null
        ]);
    }
}
