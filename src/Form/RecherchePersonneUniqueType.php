<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de recherche d'un·e adhérent·e ou d'un·e salarié·e
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class RecherchePersonneUniqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('champRecherche', PersonneSelectTextType::class, [
                'mapped' => false,
                'help' => "numéro d'adhérent.e, matricule salarié, nom ou prénom",
                'required' => false,
                'label' => false

            ])
            ->add('search', SubmitType::class, [
                'label' => 'Ajouter'
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
