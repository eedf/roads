<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;

/**
 * Formulaire de saisie des information de déclaration d'intention d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class SejourDeclarationIntentionType extends AbstractType
{
    private $router;
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Sejour sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('intitule', TextType::class, [
                'label' => 'Intitulé du séjour',
                'help' => "Nommez le séjour pour qu'il soit facilement identifiable. Exemple : Groupe(s)/Unité(s)/mois/lieu"
            ])
            ->add('accompagnant', PersonneSelectTextType::class, [
                'label' => "Personne en charge du suivi du séjour (si différente de l'organisat·eur·rice ou du·de la responsable de structure)",
                'help' => "Pour tout séjour et activité, l'organisat·eur·rice doit garantir au direct·eur·rice un accompagnement et une capacité de réaction rapide en cas de problème.",
                'required' => false
            ])
            ->add('ficheComplementaire', DocumentType::class, [
                'required' => false,
                'label' => "Fiche complémentaire - si déjà déposée",
                'help' => "Insérez la copie du récépissé de déclaration complémentaire au service de la jeunesse de l’engagement et des sports de votre département et qui est à réaliser au minimum 31 jours avant le début du séjour.",
            ])
            ->add('typeSejour', ChoiceType::class, [
                'choices' => [
                    Sejour::TYPE_SEJOUR_UNITE => Sejour::TYPE_SEJOUR_UNITE,
                    Sejour::TYPE_SEJOUR_GROUPE => Sejour::TYPE_SEJOUR_GROUPE,
                    Sejour::TYPE_SEJOUR_RASSEMBLEMENT => Sejour::TYPE_SEJOUR_RASSEMBLEMENT,
                    Sejour::TYPE_SEJOUR_REGROUPEMENT => Sejour::TYPE_SEJOUR_REGROUPEMENT,
                    Sejour::TYPE_SEJOUR_SV => Sejour::TYPE_SEJOUR_SV,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Type de séjour",
                'help_html' => true,
                'help' => "Préciser le type de séjour organisé: 
                    <ul>
                        <li>Camp/Séjour d'unité = 1 direct·eur·rice pour une unité,</li>
                        <li>Camp/Séjour de groupe = 1 direct·eur·rice pour plusieurs unités d'un même groupe local</li>
                        <li>Camp/Séjour de regroupement = 1 direct·eur·rice pour plusieurs unités de groupes locaux différents.</li>
                        <li>Une unité = participant·e·s et responsables d'1 tranche d'âge (exemple : 6-8 ans = les lutin·es).</li>
                    </ul>"
            ])
            ->add('sejourOuvert', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour ouvert",
                'help' => "Un séjour ouvert est un séjour sur lequel sont accueilli·e·s des participant·e·s ne participant pas à des activités à l'année et qui souvent propose une démarche de découverte du scoutisme"
            ])
            ->add('sejourItinerant', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour itinérant",
                'help' => "Un séjour itinérant est un séjour qui ne se vit pas sur un lieu unique, durant le séjour il y a déplacements des participant·e·s et des équipes sur plusieurs lieux."
            ])
            ->add('international', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour à l'international",
                'help' => "Un séjour à l'international est un séjour où les participant·e·s et l'équipe séjournent dans un autre pays que la France"
            ])
            ->add('sejourFranceAvecAccueilEtranger', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour en France avec accueil d’un groupe étranger",
            ])
            ->add('coloApprenante', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Colos apprenantes",
                'help_html' => true,
                'help' => "Séjour prenant en compte le cahier des charges du dispositif du même nom"
            ])
            ->add('dispositifAppui', ChoiceType::class, [
                'choices' => [
                    Sejour::DISPOSITIF_APPUI_CAMP_ACCOMPAGNE => Sejour::DISPOSITIF_APPUI_CAMP_ACCOMPAGNE,
                    Sejour::DISPOSITIF_APPUI_CAMP_BASE => Sejour::DISPOSITIF_APPUI_CAMP_BASE,
                    "Non" => "Non"
                ],
                'expanded' => true,
                'multiple' => false,
                'help_html' => true,
                'label' => "Dispositif d'appui à la réalisation du camp",
                'help' => "Est-ce que le séjour bénéficie d'un appui spécifique ?"
            ])
            ->add('directeur', PersonneSelectTextType::class, [
                'label' => "Sélection de la personne qui dirige le séjour",
                'help' => "Permet de sélectionner la directrice ou le directeur du séjour",
                'row_attr' => [
                    'class' => "infos-personnes",
                    'infos-personnes-url' => $this->router->generate('sejour_infos_personnes'),
                    "sejour_id" => $sejour->getId(),
                    "structure_id" => $sejour->getStructureOrganisatrice()->getId()
                ],
            ])
            ->add('qualificationDirection', QualificationDirectionDeclarationIntentionType::class, [
                'label' => false,
            ])
            ->add('effectifsPrevisionnels', EffectifsPrevisionnelsType::class, [
                'label' => false,
                'error_bubbling' => false,
            ])
            ->add('structuresRegroupees', CollectionType::class, [
                'mapped' => false,
                'entry_type' => StructureRegroupementSejourType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,
                'label' => false,
                'data' => $sejour->getStructuresRegroupees(),

            ])
            ->add('utilisationsLieuSejour', CollectionType::class, [
                'entry_type' => UtilisationLieuSejourDeclarationIntentionType::class,
                'entry_options' => [
                    'label' => false,
                    'sejour' => $sejour
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,

            ])

            ->add('dateDebut', DateType::class, [
                'label' => "Date de début de séjour envisagée",
                'help' => "Est attendue ici la date à partir de laquelle débute la présence des participant·e·s sur le site du séjour. Même si c'est un \"précamp\" où ne seront présent·e·s que quelques participant·e·s.",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],
                'constraints' =>
                [
                    new LessThan([
                        'propertyPath' => 'parent.all[dateFin].data',
                        'message' => 'la date de début doit être avant la date de fin'
                    ]),
                ]

            ])
            ->add('dateFin', DateType::class, [
                'label' => "Date de fin de séjour envisagée",
                'help' => "Nécessaire pour déclarer la fiche complémentaire sur TAM. Un séjour doit forcément faire au moins 5 jours et 4 nuits pour être déclaré en tant que séjour. Si le séjour est plus court, c'est alors un mini-séjour (ou mini-camp). Il s'inscrit donc dans la déclaration trimestrielle.",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],
                'constraints' =>
                [
                    new GreaterThan([
                        'propertyPath' => 'parent.all[dateDebut].data',
                        'message' => 'la date de fin doit être après la date de début'
                    ]),
                ]

            ])

            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer le brouillon'
            ])

            ->add('publish', SubmitType::class, [
                'label' => 'Publier la déclaration d\'intention',
                'attr' => [
                    'class' => 'btn btn-outline-secondary m-3',

                ]
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
