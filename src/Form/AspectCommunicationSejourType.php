<?php

namespace App\Form;

use App\Entity\AspectCommunicationSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects communication d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectCommunicationSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('presentationAmont', CKEditorType::class, [
                'required' => false,
                'label' => "Présentation du projet de séjour en amont",
                'help' => "Par quel moyens le séjour est présenté aux familles ?"
            ])
            ->add('dateReunionInformation', DateType::class, [
                'required' => false,
                'label' => "Menée d’une réunion d’information",
                'help' => "Préciser la date de la réunion de présentation si elle est prévue.",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker']

            ])
            ->add('moyenCommunicationPendant', CKEditorType::class, [
                'required' => false,
                'label' => "Quels moyens de communication durant le séjour ?",
                'help' => "Quels sont les moyens utilisés pour communiquer avec les familles durant le séjour (ex : répondeur, site internet, courrier, sms, ...) ?"
            ])
            ->add('moyenRestitution', CKEditorType::class, [
                'required' => false,
                'label' => "Moyens de restitution",
                'help' => "Quels moyens de communication après le séjour (projection d’un diaporama, remise d’un CD de photos, création d’un groupe fermé sur les réseaux sociaux, etc.) ?"
            ])
            ->add('siteValorisation', UrlType::class, [
                'required' => false,
                'label' => "Site de valorisation",
                'help' => "Vous pouvez renseigner ici un lien vers le site que vous utilisez pour valoriser et où communiquer sur votre séjour."
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AspectCommunicationSejour::class,
        ]);
    }
}
