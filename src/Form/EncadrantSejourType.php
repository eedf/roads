<?php

namespace App\Form;

use App\Entity\ParticipationSejourUnite;
use App\Entity\RoleEncadrantSejour;
use App\Entity\StructureRegroupementSejour;
use App\Repository\ParticipationSejourUniteRepository;
use App\Repository\StructureRegroupementSejourRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un·e encadrant·e d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class EncadrantSejourType extends AbstractType
{
    /**
     * @var ParticipationSejourUniteRepository $participationSejourUniteRepository
     */
    protected $participationSejourUniteRepository;

    /**
     * @var StructureRegroupementSejourRepository $structureRegroupementSejourRepository
     */
    protected $structureRegroupementSejourRepository;

    public function __construct(ParticipationSejourUniteRepository $participationSejourUniteRepository, StructureRegroupementSejourRepository $structureRegroupementSejourRepository)
    {
        $this->participationSejourUniteRepository = $participationSejourUniteRepository;
        $this->structureRegroupementSejourRepository = $structureRegroupementSejourRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sejour = $options['sejour'];
        $builder

            // Sélection de l'encadrant
            ->add('estEncadrantEEDF', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "L'encadrant·e est-il·elle adhérent·e aux EEDF ?",
            ])
            ->add('encadrantEEDF', PersonneSelectTextType::class, [
                'label' => "Sélection de l'adhérent·e",
            ])
            ->add('infosEncadrantNonEEDF', InfosEncadrantNonEEDFType::class, [
                'label' => "Sélection de l'encadrant non adhérent·e"
            ])
            //Formations et qualifications
            ->add('diplomesAnimation', ChoiceType::class, [
                'choices' => [
                    RoleEncadrantSejour::DIPLOME_ANIMATION_ASF => RoleEncadrantSejour::DIPLOME_ANIMATION_ASF,
                    RoleEncadrantSejour::DIPLOME_ANIMATION_BAFA => RoleEncadrantSejour::DIPLOME_ANIMATION_BAFA,
                    RoleEncadrantSejour::DIPLOME_ANIMATION_EQUIV . " (dont diplôme de direction)" => RoleEncadrantSejour::DIPLOME_ANIMATION_EQUIV,
                ],
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'label' => "Sélectionnez le(s) diplôme(s) d'animation de cet·te encadrant·e",
            ])
            ->add('equivalenceDiplomeAnimation', TextType::class, [
                'label' => "Précisez",
                'help' => "Exemple: \"BAFD stagiaire\""
            ])
            ->add('qualiteAnimation', ChoiceType::class, [
                'choices' => [
                    RoleEncadrantSejour::QUALITE_NON_DIPLOME => RoleEncadrantSejour::QUALITE_NON_DIPLOME,
                    RoleEncadrantSejour::QUALITE_STAGIAIRE => RoleEncadrantSejour::QUALITE_STAGIAIRE,
                    RoleEncadrantSejour::QUALITE_TITULAIRE => RoleEncadrantSejour::QUALITE_TITULAIRE,
                    RoleEncadrantSejour::QUALITE_TITULAIRE_STAGIAIRE => RoleEncadrantSejour::QUALITE_TITULAIRE_STAGIAIRE,
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => "Qualité en tant qu'animat·eur·rice",
                'empty_data' => RoleEncadrantSejour::QUALITE_NON_DIPLOME
            ])
            ->add('statutAssistantSanitaire', ChoiceType::class, [
                'choices' => [
                    RoleEncadrantSejour::STATUT_AS_NON_DIPLOME => false,
                    RoleEncadrantSejour::STATUT_AS_PSC1 => true,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Diplôme assistant·e sanitaire",
            ])
            ->add('permisB', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "L'encadrant·e a-t'il·elle le permis B?",
            ])
            //Rôles et fonctions
            ->add('equipe', ChoiceType::class, [
                'choices' => [
                    RoleEncadrantSejour::EQUIPE_ANIMATION => RoleEncadrantSejour::EQUIPE_ANIMATION,
                    RoleEncadrantSejour::EQUIPE_SUPPORT => RoleEncadrantSejour::EQUIPE_SUPPORT,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "L'encadrant fait partie de ",
            ])
            ->add('fonction', ChoiceType::class, [
                'choices' => [
                    RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE => RoleEncadrantSejour::FONCTION_DIRECTION_ADJOINTE,
                    RoleEncadrantSejour::FONCTION_INTENDANT => RoleEncadrantSejour::FONCTION_INTENDANT,
                    RoleEncadrantSejour::FONCTION_AS => RoleEncadrantSejour::FONCTION_AS,
                    RoleEncadrantSejour::FONCTION_MATOS => RoleEncadrantSejour::FONCTION_MATOS,
                    RoleEncadrantSejour::FONCTION_TRESORIER => RoleEncadrantSejour::FONCTION_TRESORIER,
                    RoleEncadrantSejour::FONCTION_PEDA => RoleEncadrantSejour::FONCTION_PEDA

                ],
                'expanded' => true,
                'multiple' => true,
                'label' => "L'encadrant·e a t'il·elle une fonction particulière sur le séjour ?",
            ])

            ->add('attacher', ChoiceType::class, [
                'choices' => [
                    "non" => RoleEncadrantSejour::ATTACHER_NON,
                    "unite EEDF" => RoleEncadrantSejour::ATTACHER_EEDF,
                    "structure non EEDF" => RoleEncadrantSejour::ATTACHER_NON_EEDF,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Attacher l'encadrant à une unité ?",
                'empty_data' => 'non'
            ])

            ->add('uniteParticipante', EntityType::class, [
                'class' => ParticipationSejourUnite::class,
                'label' => "Attachez l'encadrant·e à une unité EEDF participante",
                'choice_label' => function ($participationSejourUnite) {
                    return $participationSejourUnite->getEquipe()->getNom() . " (" . $participationSejourUnite->getEquipe()->getStructure()->getNom() . ")";
                },
                'placeholder' => "Sélectionnez l'unité dans la liste",
                'choices' => $this->participationSejourUniteRepository->findBySejour($sejour)

            ])

            ->add('structureNonEEDF', EntityType::class, [
                'class' => StructureRegroupementSejour::class,
                'label' => "Attachez l'encadrant·e à une unité non EEDF participante",
                'choice_label' => function ($structureRegroupementNonEEDF) {
                    return $structureRegroupementNonEEDF->getNomStructureNonEEDF();
                },
                'placeholder' => "Sélectionnez la structure dans la liste",
                'choices' => $this->structureRegroupementSejourRepository->findNonEEDFBySejour($sejour)

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RoleEncadrantSejour::class,
            'sejour' => null
        ]);
    }
}
