<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour ajouter le compte de résultat d'un séjour (clôture)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectsLogistiquesClotureSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('compteResultat', DocumentType::class, [
                'required' => true,
                'label' => "Insérer son compte de résultat",
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
