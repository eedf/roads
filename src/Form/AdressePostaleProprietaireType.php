<?php

namespace App\Form;

use App\Entity\AdressePostale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour saisir l'adresse postale d'un propriétaire
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AdressePostaleProprietaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adresse')
            ->add('complementAdresse')
            ->add('codePostal')
            ->add('ville')
            ->add('pays')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdressePostale::class,
        ]);
    }
}
