<?php

namespace App\Form;

use App\Entity\Personne;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des informations de la personne à contacter en cas d'urgence (profil)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class InfosPersoProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomUrgence', TextType::class, [
                'label' => 'Nom de la personne à
                appeler en cas d\'urgence'
            ])
            ->add('prenomUrgence', TextType::class, [
                'label' => 'Prénom de la personne à
                appeler en cas d\'urgence'
            ])
            ->add('telUrgence', PhoneNumberType::class, [
                'label' => 'Numéro de la personne à appeler en cas d\'urgence',
                "default_region" => "FR",
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'preferred_country_choices' => ['FR']
            ])
            ->add('lienUrgence', TextType::class, [
                'label' => "Lien entre l'adhérent·e et la personne à appeler en cas d'urgence"
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
