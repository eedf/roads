<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour renseignes les informations sur les Colos apprenantes du séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DemarchePedaSejourColoApprenanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('documentSocleCommunColosApprenantes', DocumentType::class, [
                'required' => false,
                'label' => "Insérer son document de socle commun",
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
