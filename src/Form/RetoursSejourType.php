<?php

namespace App\Form;

use App\Entity\RetourSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType as TypeDateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des retours d'un séjour (clôture)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class RetoursSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('visMonCamp', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => "Votre séjour a t-il fait l’objet d’une activité “Vis mon camp” (activités communes ou journées d’échanges avec des associations du scoutisme français)?",
            ])
            ->add('precisionsVisMonCamp', CKEditorType::class, [
                'required' => true,
                'label' => "Si oui, merci de préciser le nombre de participant·e·s concerné·e·s et la ou les autres associations SF concernées.",
            ])
            ->add('parutionMedias', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => "Votre camp a t-il fait l'objet d'un reportage ou d'un article dans les médias ?",
            ])
            ->add('nomMedia', TextType::class, [
                'label' => "Précisez quel média (nom)",
                'required' => false
            ])
            ->add('dateMedia', TypeDateType::class, [
                'required' => false,
                'label' => "Précisez quel média (date)",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],

            ])
            ->add('visiteEtat', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => "Notre camp a été visité/inspecté par un service de l'état ?",
            ])
            ->add('visites', CollectionType::class, [
                'mapped' => true,
                'entry_type' => VisiteSejourEtatType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,

            ])
            ->add('evenementsGraves', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => "Y-a-t'il eu des évenements graves sur mon séjour ?",
            ])


            ->add('descriptionEvenementsGraves', CollectionType::class, [
                'mapped' => true,
                'entry_type' => EvenementGraveType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,

            ])
            ->add('incidentsMineurs', CKEditorType::class, [
                'required' => false,
                'help' => "Ce partage permettra si besoin un meilleur accompagnement et/ou une évolution des outils et formations.",
                "label" => "Avez-vous un ou plusieurs incidents sans gravité que vous voulez partager?"
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RetourSejour::class,
        ]);
    }
}
