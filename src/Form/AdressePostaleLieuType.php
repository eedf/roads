<?php

namespace App\Form;

use App\Entity\AdressePostale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Formulaire pour saisir une adresse postale
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AdressePostaleLieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adresse',null,[
                'required'=>true
            ])
            ->add('complementAdresse')
            ->add('codePostal',null,[
                'required'=>true
            ])
            ->add('ville',null,[
                'required'=>true
            ])
            ->add('pays',null,[
                'required'=>true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdressePostale::class,
        ]);
    }
}
