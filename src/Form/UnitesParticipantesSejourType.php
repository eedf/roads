<?php

namespace App\Form;

use App\Entity\Equipe;
use App\Entity\ParticipationSejourUnite;
use App\Entity\Sejour;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire des sélection des unités participantes d'un séjour 
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class UnitesParticipantesSejourType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('typeSejour', ChoiceType::class, [
                'choices' => [
                    Sejour::TYPE_SEJOUR_UNITE => Sejour::TYPE_SEJOUR_UNITE,
                    Sejour::TYPE_SEJOUR_GROUPE => Sejour::TYPE_SEJOUR_GROUPE,
                    Sejour::TYPE_SEJOUR_RASSEMBLEMENT => Sejour::TYPE_SEJOUR_RASSEMBLEMENT,
                    Sejour::TYPE_SEJOUR_REGROUPEMENT => Sejour::TYPE_SEJOUR_REGROUPEMENT,
                    Sejour::TYPE_SEJOUR_SV => Sejour::TYPE_SEJOUR_SV,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Type de séjour",
                'help_html' => true,
                'help' => "Préciser le type de séjour organisé: 
                    <ul>
                        <li>Camp/Séjour d'unité = 1 direct·eur·rice pour une unité,</li>
                        <li>Camp/Séjour de groupe = 1 direct·eur·rice pour plusieurs unités d'un même groupe local</li>
                        <li>Camp/Séjour de regroupement = 1 direct·eur·rice pour plusieurs unités de groupes locaux différents.</li>
                        <li>Une unité = participant·e·s et responsables d'1 tranche d'âge (exemple : 6-8 ans = les lutin·es).</li>
                    </ul>"
            ])
            ->add(
                'unites',
                EntityType::class,
                [
                    'class' => Equipe::class,
                    'mapped' => false,
                    'choice_label' => 'nom',
                    'choices' => $sejour->getStructureOrganisatrice()->getEquipesUnitesActives(),
                    'choice_attr' => function ($choice, $key, $unite) use ($sejour) {
                        /** @var ParticipationSejourUnite $uniteStructureOrganisatrice */
                        foreach ($sejour->getUnitesStructureOrganisatrice() as $uniteStructureOrganisatrice) {
                            if ($unite == $uniteStructureOrganisatrice->getEquipe()->getId()) {
                                return ['checked' => 'checked'];
                            }
                        }
                        return ['checked' => false];
                    },
                    'label' => "Unites participantes",
                    'help' => "Sélectionnez les unités participant au séjour pour la structure organisatrice",
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false
                ]
            )
            ->add('structuresRegroupees', CollectionType::class, [
                'mapped' => false,
                'entry_type' => StructureRegroupementConstructionSejourType::class,
                'entry_options' => [
                    'label' => false
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label' => false,
                /** @var Sejour $options['sejour'] */
                'data' => $options['sejour']->getStructuresRegroupees(),
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
