<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Security\Voter\SejourVoter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Formulaire de saisie des information générales d'un séjour (construction et clôture)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class InformationsGeneralesSejourType extends AbstractType
{

    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('intitule', TextType::class, [
                'label' => 'Intitulé du séjour',
                'help' => "Nommez le séjour pour qu'il soit facilement identifiable. Exemple : Groupe(s)/Unité(s)/mois/lieu"
            ])
            ->add('ficheComplementaire', DocumentType::class, [
                'required' => false,
                'label' => "Fiche complémentaire",
                'help' => "Insérez la copie du récépissé de déclaration complémentaire au service de la jeunesse de l’engagement et des sports de votre département et qui est à réaliser au minimum 31 jours avant le début du séjour.",
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);

        if ($this->authorizationChecker->isGranted(SejourVoter::SEJOUR_ORGANISER, $sejour)) {
            $builder->add('accompagnant', PersonneSelectTextType::class, [
                'label' => "Personne en charge du suivi du séjour (si différente de l'organisat·eur·rice ou du·de la responsable de structure)",
                'help' => "Pour tout séjour et activité, l'organisat·eur·rice doit garantir au direct·eur·rice un accompagnement et une capacité de réaction rapide en cas de problème.",
                'required' => false
            ]);
        }
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
