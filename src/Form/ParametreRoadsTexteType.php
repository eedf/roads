<?php

namespace App\Form;

use App\Entity\ParametreRoadsTexte;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un texte paramétrable
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ParametreRoadsTexteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actif', ChoiceType::class,[
                'choices'=>[
                    "Oui"=>true,
                    "Non"=>false
                ],
                'multiple' =>false,
                'expanded' =>true,
                'label' => "Voulez-vous activer l'affichage de ce texte?"
            ])
            ->add('texte', CKEditorType::class, [
                'required' =>false,
                'label' => "Contenu du texte",
                'help' => "Vous pouvez définir une mise en forme particulière"
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParametreRoadsTexte::class,
        ]);
    }
}
