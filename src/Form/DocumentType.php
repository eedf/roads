<?php

namespace App\Form;

use App\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

/**
 * Formulaire de saisie d'un document
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('documentFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => true,
                'download_label' => new PropertyPath('documentName'),
                'label' => false,
                'help' => "Sélectionnez le fichier à charger (Taille limite des fichiers acceptés : 5 Mo)",
                'constraints' =>
                [
                    new File(
                        [
                            'maxSize' => "5M",
                            'mimeTypes' => [
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "application/vnd.ms-excel",
                                "application/vnd.ms-excel.sheet.macroEnabled.12",
                                "application/vnd.oasis.opendocument.spreadsheet",
                                "application/vnd.oasis.opendocument.text",
                                "application/msword",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.oasis.opendocument.presentation",
                                "application/pdf",
                                "application/vnd.ms-powerpoint",
                                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                                "application/x-rar-compressed",
                                "application/zip",
                            ],
                            'mimeTypesMessage' => "Le type du fichier est invalide (\"{{ type }}\").  Les types autorisés sont : .xlsx / .xls / .xlsm / .ods / .odt / .doc / .docx / .odp / .pdf / .ppt / .pptx / .rar / .zip."
                        ]
                    ),
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
