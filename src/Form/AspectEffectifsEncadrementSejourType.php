<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les effectifs d'encadrement d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectEffectifsEncadrementSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Sejour $sejour */
        $sejour=$options['sejour'];
        $builder
            ->add('encadrants',CollectionType::class,[
                'mapped' => false,
                'entry_type' => EncadrantSejourType::class,
                'entry_options' => [
                    'label' => false,
                    'sejour' => $sejour
                ],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'label'=>false,
                'data'=>$sejour->getRolesEncadrants(),
                
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
