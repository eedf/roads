<?php

namespace App\Form;

use App\Entity\ValidationSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des in formation de validation 'classique' d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ValidationSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentaires', CKEditorType::class, [
                'required' =>true,
                'label' => "Commentaires généraux sur la validation",
            ])
            ->add('estValide',ChoiceType::class,[
                'choices'=>[
                    "Favorable"=>true,
                    "Défavorable"=>false,
                ],
                'expanded'=>true,
                'multiple'=>false,
                'label'=>"Avis",
            ])
            ->add('documentValidation', DocumentType::class, [
                'required' =>false,
                'label' => "Insérer le document de validation",
            ])
            ->add('necessiteVisite',ChoiceType::class,[
                'choices'=>[
                    "Oui"=>true,
                    "Non"=>false,
                ],
                'expanded'=>true,
                'multiple'=>false,
                'label'=>"Une visite du séjour est-elle prévue ou souhaitée ?",
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ValidationSejour::class,
        ]);
    }
}
