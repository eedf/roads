<?php

namespace App\Form;

use App\Entity\QualificationDirection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des qualifiaction du·de la direct·eur·rice du séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class QualificationDirectionDeclarationIntentionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('diplomesDirection', ChoiceType::class, [
                'choices' => [
                    QualificationDirection::DIPLOME_DIRECTION_DSF => QualificationDirection::DIPLOME_DIRECTION_DSF,
                    QualificationDirection::DIPLOME_DIRECTION_BAFD => QualificationDirection::DIPLOME_DIRECTION_BAFD,
                    QualificationDirection::DIPLOME_DIRECTION_EQUIV => QualificationDirection::DIPLOME_DIRECTION_EQUIV,
                ],
                'required' => false,
                'expanded' => true,
                'multiple' => true,

                'label' => "Sélectionnez le(s) diplôme(s) de direction du·de la direct·eur·rice",
            ])
            ->add('equivalenceDiplomeDirection', TextType::class, [
                'label' => "Précisez",
            ])
            ->add('qualiteDirection', ChoiceType::class, [
                'choices' => [
                    QualificationDirection::QUALITE_NON_DIPLOME => QualificationDirection::QUALITE_NON_DIPLOME,
                    QualificationDirection::QUALITE_STAGIAIRE => QualificationDirection::QUALITE_STAGIAIRE,
                    QualificationDirection::QUALITE_TITULAIRE => QualificationDirection::QUALITE_TITULAIRE,
                    QualificationDirection::QUALITE_TITULAIRE_STAGIAIRE => QualificationDirection::QUALITE_TITULAIRE_STAGIAIRE,
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => "Qualité",
                'required' => false,
                'placeholder' => "Sélectionnez",
            ])
            ->add('permisB', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'data' => false,
                'label' => "Le·la direct·eur·rice a-t'il·elle le permis B?",
            ])
            ->add('statutAssistantSanitaire', ChoiceType::class, [
                'choices' => [
                    QualificationDirection::STATUT_AS_NON_DIPLOME => false,
                    QualificationDirection::STATUT_AS_PSC1 => true,
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'data' => false,
                'label' => "Diplôme assistant·e sanitaire",
            ])
            ->add('fonction', ChoiceType::class, [
                'choices' => [
                    QualificationDirection::FONCTION_INTENDANT => QualificationDirection::FONCTION_INTENDANT,
                    QualificationDirection::FONCTION_AS => QualificationDirection::FONCTION_AS,
                    QualificationDirection::FONCTION_MATOS => QualificationDirection::FONCTION_MATOS,
                    QualificationDirection::FONCTION_TRESORIER => QualificationDirection::FONCTION_TRESORIER,
                    QualificationDirection::FONCTION_PEDA => QualificationDirection::FONCTION_PEDA

                ],
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'label' => "Le·la direct·eur·rice a t'il·elle une fonction particulière sur le séjour ?",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => QualificationDirection::class,
        ]);
    }
}
