<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Entity\UtilisationLieuSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie de lieu de séjour (construction de séjour)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DatesLieuSejourLieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('utilisationsLieuSejour', CollectionType::class, [
                'mapped' => true,
                'entry_type' => UtilisationLieuSejourType::class,
                'entry_options' => [
                    'label' => false,
                    'sejour' => $sejour
                ],
                'prototype_data' => new UtilisationLieuSejour($sejour),
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label' => false

            ])

            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
