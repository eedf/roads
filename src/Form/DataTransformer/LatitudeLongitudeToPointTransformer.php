<?php

namespace App\Form\DataTransformer;

use LongitudeOne\Spatial\PHP\Types\Geography\Point;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Transformeur d'un Point Géographique vers une latitude et une longitude, et vice versa
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class LatitudeLongitudeToPointTransformer implements DataTransformerInterface
{

    public function __construct()
    {
    }

    public function transform($value): ?array
    {
        if (null === $value)
            return null;
        if ($value instanceof Point) {
            return array(
                'latitude' => $value->getLatitude(),
                'longitude' => $value->getLongitude()
            );
        }


        return null;
    }


    public function reverseTransform($value): ?Point
    {
        if (null === $value)
            return null;

        if (is_array($value) && array_key_exists('latitude', $value) && array_key_exists('longitude', $value) && $value['latitude'] && $value['longitude']) {
            $point = new \LongitudeOne\Spatial\PHP\Types\Geography\Point(0, 0);
            $point->setLatitude($value['latitude']);
            $point->setLongitude($value['longitude']);
            return $point;
        }

        return null;
    }
}
