<?php

namespace App\Form\DataTransformer;

use App\Entity\Adherent;
use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Transformeur de l'entité Personne vers une chaîne de caractères, et vice versa
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class NumAdhToPersonneTransformer implements DataTransformerInterface
{
    const SALARIE = "Salarié·e";
    private $personneRepository;
    public function __construct(PersonneRepository $personneRepository)
    {
        $this->personneRepository = $personneRepository;
    }

    /**
     * Transforme l'entité Personne en chaîne de caractères
     *
     * @param  mixed $value la personne
     * @return void renvoie une chaîne de cartactère correspondant à {prénom} {nom} ({numéro d'adhérent·e ou matricule salarié·e}) (*ex: Michèle Bachelet (111111)*)
     * @throws LogicException si la valeur passée en paramètre n'est pas du type Personne
     */
    public function transform($value): ?String
    {
        if (null === $value) {
            return '';
        }
        if (!$value instanceof Personne) {
            throw new \LogicException('The UserSelectTextType can only be used with Personne objects');
        }
        return $value->getAffichagePourRechercheAuto();
    }

    /**
     * Récupère une chaîne de caractères et renvoie la personne correspondante
     *
     * @param  String $value la chaine de caractère contenant le nom de personne sous le format {prénom} {nom} ({numéro d'adhérent·e matricule salarié·e}) (*ex: Michèle Bachelet (111111)*)
     * @return Personne la personne correspondante
     * @throws TransformationFailedException si la chaîne de caractère ne contient pas de numéro d'adhérent·e ou de matricule salarié
     */
    public function reverseTransform($value): ?Personne
    {
        if (!$value) {
            return null;
        }
        //Cas d'une adhérent·e actif
        if (str_contains($value, " (" . Adherent::ADHERENT . " n°") && str_contains($value, ')')) {
            $temp = explode(" (" . Adherent::ADHERENT . " n°", $value);
            $numAdh = "";
            if (count($temp) > 0) {
                //Cas ou la personne est uniquement adhérente
                $temp2 = explode(")", $temp[count($temp) - 1]);
                if (count($temp2) > 0) {
                    $numAdh = $temp2[0];
                } else {
                    //Cas ou la personne est adhérente et salariée
                    $numAdh = explode(' - ' . self::SALARIE . ' n°', $temp[count($temp) - 1])[0];
                }
            }
        } else if (str_contains($value, " (" . Adherent::ANCIEN_ADHERENT . " n°") && str_contains($value, ')')) {
            $temp = explode(" (" . Adherent::ANCIEN_ADHERENT . " n°", $value);
            $numAdh = "";
            if (count($temp) > 0) {
                //Cas ou la personne est uniquement adhérente
                $temp2 = explode(")", $temp[count($temp) - 1]);
                if (count($temp2) > 0) {
                    $numAdh = $temp2[0];
                } else {
                    //Cas ou la personne est adhérente et salariée
                    $numAdh = explode(' - ' . self::SALARIE . ' n°', $temp[count($temp) - 1])[0];
                }
            }
            //Cas d'un·e salarié·e
        } else if (str_contains($value, ' (' . self::SALARIE . ' n°') && str_contains($value, ')')) {
            $temp = explode(' (' . self::SALARIE . ' n°', $value);
            $temp2 = explode(")", $temp[count($temp) - 1]);
            if (count($temp2) > 0) {
                $numAdh = $temp2[0];
            }
        } else {
            throw new TransformationFailedException(sprintf('Aucune personne trouvée pour "%s"', $value));
        }
        $personne = $this->personneRepository->findByNumAdhOrMatriculeSalarieField($numAdh);
        if (!$personne) {
            throw new TransformationFailedException(sprintf('Aucune personne trouvée pour "%s"', $value));
        }
        return $personne;
    }
}
