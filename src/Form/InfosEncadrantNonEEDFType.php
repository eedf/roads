<?php

namespace App\Form;

use App\Entity\InfosEncadrantNonEEDF;
use App\Entity\Personne;
use DateTime;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire des informations d'un·e encadrant·e d'un séjour non adhérent·e aux EEDF
 */
class InfosEncadrantNonEEDFType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom de l\'encadrant·e'
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Prénom de l\'encadrant·e'
            ])
            ->add('dateNaissance', DateType::class, [
                'label' => "Date de naissance de l'encadrant·e",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],
                'help' => "Nécessaire pour TAM",
                'data' => new DateTime(),
            ])
            ->add('telephone', PhoneNumberType::class, [
                'help' => "Nécessaire pour TAM",
                'label' => 'Téléphone de l\'encadrant·e',
                "default_region" => "FR",
            ])
            ->add('departementOuPaysNaissance', TextType::class, [
                'help' => "Nécessaire pour TAM",
                'label' => 'Département ou pays de naissance de l\'encadrant·e'
            ])
            ->add('communeNaissance', TextType::class, [
                'label' => 'Commune de naissance de l\'encadrant·e',
                'help' => "Nécessaire pour TAM",
            ])
            ->add('genre', ChoiceType::class, [
                'choices' => [
                    Personne::PERSONNE_GENRE_FEMININ => "Féminin",
                    Personne::PERSONNE_GENRE_MASCULIN => "Masculin",
                    Personne::PERSONNE_GENRE_AUTRE => "Autre",
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Genre de l'encadrant·e",
                'help' => "Nécessaire pour TAM",
            ])
            ->add('docConsentement', DocumentType::class, [
                'required' => true,
                'label' => "Insérer le document de consentement de l'encadrant·e à l'utilisation de ses données",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InfosEncadrantNonEEDF::class,
        ]);
    }
}
