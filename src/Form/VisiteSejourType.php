<?php

namespace App\Form;

use App\Entity\VisiteSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie / modification de compte rendu d'une visite d'un séjour par un·e visiteu·r·se
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class VisiteSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateVisite', DateType::class, [
                'label' => "Date de la visite",
                'widget' => 'single_text',
                'html5' => false,
                'format' => "dd/MM/yyyy",
                'attr' => ['class' => 'form-control js-datepicker'],

            ])
            ->add('choixCompteRendu', ChoiceType::class, [
                'choices' => [
                    "remplir le compte-rendu de visite directement sur ROADS" => true,
                    "charger le document de visite" => false,
                ],
                'mapped' => false,
                'expanded' => true,
                'multiple' => false,
                'label' => "Je souhaite…",
            ])
            ->add('evalManquesClasseurDirecteur', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false,
                ],
                'expanded' => true,
                'multiple' => false,

                'label' => "Classeur du·de la direct·eur·rice",
                "help" => "Est-ce que des documents de la check-list du classeur du·de la direct·eur·rice sont manquants lors de la visite ?"
            ])
            ->add('precisionsManquesClasseurDirecteur', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés",
            ])
            ->add('evalMesuresSanitaires', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Mesures et protocoles sanitaires spécifiques",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Le·la référent·e covid est identifié·e et ses missions sont bien identifiées.</li>
                        <li>Les mesures annoncés dans le PP sont visiblement mise en œuvre, notamment sur l'hygiène des mains et des surfaces contacts.</li>
                        <li>En cas de suspicion, le protocole à mettre en œuvre et les moyens dédiés sont bien présents.</li>
                    </ul>"
            ])
            ->add('autresMesuresSanitaires', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés",
            ])
            ->add('evalPedagogie', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Pédagogie et activité",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Le projet pédagogique est disponible, l’équipe le connaît, une évaluation est prévue pendant le camp, un bilan du projet est prévu.</li>
                        <li>Le rythme des journées et les activités vécues sont adaptées à la(aux) tranche(s) d'âge accueillie(s).</li>
                        <li>Les horaires types sont respectés (heures de repas notamment).</li>
                        <li>Les règles de vie (ou charte) ainsi que la grille d'activités sont clairement affichées.</li>
                    </ul>"
            ])
            ->add('autresPedagogie', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés",
            ])
            ->add('evalFormation', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Pédagogie et activité",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Les stagiaires BAFA/ASF font l’objet d’un accompagnement formalisé, avec des critères, des temps et des objectifs identifiés.</li>
                        <li>Le·la direct·eur·rice est accompagné·e également par l'organisat·eur·rice ou un·e représentant·e</li>
                        <li>Le·la direct·eur·rice se sent en capacité de rédiger les appréciations de stages pratiques.</li>
                    </ul>"
            ])
            ->add('autresFormation', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés",
            ])
            ->add('sujetsComplementairesFormation', CKEditorType::class, [
                'required' => false,
                'label' => "Besoins complémentaires",
                "help" => "Quels sont les sujets sur lesquels une formation supplémentaire est souhaitée ou nécessaire ?"
            ])
            ->add('evalAmenagement', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Les installations et les aménagements du camp",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Les zones d'ombres sont suffisantes pour éviter les heures les plus chaudes.</li>
                        <li>La délimitation du lieu et des dangers sont bien définies.</li>
                        <li>Les installations (tables, vaisseliers, tentes…) sont en quantité suffisante, bien montées et en bon état.</li>
                    </ul>"
            ])
            ->add('autresAmenagement', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés",
            ])
            ->add('evalHygiene', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "L'hygiène",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Les lieux d'hygiènes (toilettes, douches, points lavage de mains) sont en quantité suffisante.</li>
                        <li>Le nettoyage des espaces collectifs et des lieux d'hygiènes est organisé et bien effectué.</li>
                        <li>Le lieu est globalement propre, bien rangé.</li>
                    </ul>"
            ])
            ->add('autresHygiene', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('evalAlimentation', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Cuisine et alimentation",
                "help_html" => true,
                "help" => "
                <ul>
                    <li>Le lieu de stockage des denrées et la cuisine sont correctement équipés.</li>
                    <li>Les relevés de température, les échantillons témoins, sont faits.</li>
                    <li>Les différents régimes alimentaires sont identifiés et respectés.</li>
                </ul>"
            ])
            ->add('autresAlimentation', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('evalGestionEquipe', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "La gestion administrative et financière",
                "help_html" => true,
                "help" => "
                <ul>
                    <li>Les membres de l’équipe connaissent leur rôle, et s’impliquent dans la vie du camp.</li>
                    <li>Il y a des réunions régulières, dont la durée est maîtrisée et qui permettent à chacun·e de s’exprimer.</li>
                    <li>Les membres de l'équipe ne présentent pas de signes de fatigue ou de tension.</li>
                </ul>"
            ])
            ->add('autresGestionEquipe', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('evalGestionAdminFinanciere', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "La gestion administrative et financière",
                "help_html" => true,
                "help" => "
                <ul>
                    <li>La comptabilité est tenue à jour régulièrement (< 3 jrs), les justificatifs sont classés.</li>
                    <li>Les affichages obligatoires sont bien présents.</li>
                </ul>"
            ])
            ->add('autresGestionAdminFinanciere', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('sousEvaluationBudget', CKEditorType::class, [
                'required' => false,
                'label' => "Une ligne de dépenses du budget a t-elle été sous évaluée ?"
            ])
            ->add('evalCommunicationExterne', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "La gestion administrative et financière",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Un dispositif d'information régulier aux parents est en place.</li>
                        <li>Le lien avec l'organisat·eur·rice (ou la région) est régulier (~ 3 ou 4 jours).</li>
                        <li>Le camp a fait l'objet d'une communication extérieure (en amont et/ou sur place).</li>
                    </il>"
            ])
            ->add('autresCommunicationExterne', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('dispositifInfosParents', CKEditorType::class, [
                'required' => false,
                'label' => "Quel est le dispositif d’information régulier aux parents utilisé ?"
            ])
            ->add('evalSante', ChoiceType::class, [
                'choices' => [
                    VisiteSejour::EVAL_OK => VisiteSejour::EVAL_OK,
                    VisiteSejour::EVAL_MOYENNEMENT_OK => VisiteSejour::EVAL_MOYENNEMENT_OK,
                    VisiteSejour::EVAL_NON_OK => VisiteSejour::EVAL_NON_OK,
                    VisiteSejour::EVAL_NON_EVALUE => VisiteSejour::EVAL_NON_EVALUE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "La santé",
                "help_html" => true,
                "help" => "
                    <ul>
                        <li>Le registre des soins est tenu quotidiennement.</li>
                        <li>Des trousses de secours sont déployées à proximité des activités.</li>
                        <li>Des outils et/ou temps de prévention sont déployés (exemple sur les sujets de maltraitance, sexualité, harcèlement...).</li>
                    </ul>"
            ])
            ->add('autresSante', CKEditorType::class, [
                'required' => false,
                'label' => "Autres points repérés"
            ])
            ->add('evalPointsGeneraux', CKEditorType::class, [
                'required' => true,
                'label' => "Points à valoriser et/ou points à améliorer et/ou questionnements",
            ])
            ->add('evalAvisGeneral', CKEditorType::class, [
                'required' => true,
                'label' => "Avis général sur la visite et injonctions éventuelles (dont délais de mise en oeuvre)"
            ])
            ->add('compteRendu', DocumentType::class, [
                'required' => false,
                'label' => "Insérer le compte-rendu de visite",
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                /** @var VisiteSejour $visiteSejour */
                $visiteSejour = $form->getData();
                $form->add('choixCompteRendu', ChoiceType::class, [
                    'choices' => [
                        "remplir le compte-rendu de visite directement sur ROADS" => true,
                        "charger le document de visite" => false,
                    ],
                    'mapped' => false,
                    'expanded' => true,
                    'multiple' => false,
                    'label' => "Je souhaite…",
                    'data' => $visiteSejour ? ($visiteSejour->getCompteRendu() ? false : true) : null
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VisiteSejour::class,
        ]);
    }
}
