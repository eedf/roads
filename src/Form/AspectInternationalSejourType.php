<?php

namespace App\Form;

use App\Entity\AspectInternationalSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects internationaux d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectInternationalSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('participationFormationInternationale', ChoiceType::class,[
                'choices'=>[
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded'=>true,
                'multiple'=>false,
                'label'=>"Participation à un temps de formation avec l’équipe internationale des EEDF dans les 24 derniers mois",
                'help'=>"La participation à un temps de formation avec l'ENI maximum 24 mois avant le départ est obligatoire"
            ])
            ->add('dateFormation', DateType::class,[
                'label'=>"Date de la formation aux projets internationaux",
                'widget' => 'single_text',
                'html5' => false,
                'format'=>"dd/MM/yyyy",
                'attr'=>['class' => 'form-control js-datepicker'],
    
            ])
            ->add('lieuFormation', TextType::class, [
                'required' =>false,
                'label' => "Lieu de la formation",
            ])
            ->add('referentFormation', TextType::class, [
                'required' =>false,
                'label' => "Format·eur·rice, direct·eur·rice de la session de formation",
                'help' => "Merci de mentionner la personne référente qui a animé la formation pour faciliter la mise en lien."
            ])
            ->add('partenaireInternational', CKEditorType::class, [
                'required' =>false,
                'label' => "Identification du partenaire",
                'help' => "Préciser ici les informations sur les structures partenaires du projet de séjour"
            ])
            ->add('financements', CKEditorType::class, [
                'required' =>false,
                'label' => "Identification des éventuels financements",
                'help' => "Identifiez ici les dispositifs de financements sollicités ou identifiés pour soutenir ce projet de séjour."
            ])
            ->add('planRepli', CKEditorType::class, [
                'required' =>false,
                'label' => "Identification d'un plan de repli",
                'help' => "Renseignez ici les informations du \"plan B\" prévu en cas de difficultés de réalisation du projet de séjour initial"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AspectInternationalSejour::class,
        ]);
    }
}
