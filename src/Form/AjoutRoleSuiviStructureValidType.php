<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Entity\Structure;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'ajout du rôle de suivi pour toutes ou une seule les structures dépendantes, ou pour un séjour organisé par ces structures
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AjoutRoleSuiviStructureValidType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder
            ->add('personne', PersonneSelectTextType::class, [
                'label' => "Sélection de la personne",
            ])
            ->add('allStructures', CheckboxType::class, [
                'label' => 'Toutes les structures dépendantes',
                'required' => false
            ])
            ->add('structure', EntityType::class, [
                'class' => Structure::class,
                'mapped' => false,
                'label' => 'Sructure',
                'choices' => $options['structure']->getStructuresFilles(),
                'choice_filter' => 'isActiveOuSousTutelle',
                'placeholder' => "Sélectionnez la structure",
                'choice_label' => function (Structure $structure) {
                    if ($structure->getStatut() == Structure::STATUT_STRUCTURE_RATTACHEE) {
                        return $structure->getNom() . " (" . $structure->getType() . " sous tutelle)";
                    } else {
                        return $structure->getNom() . " (" . $structure->getType() . ")";
                    }
                },

            ])
            ->add('all', CheckboxType::class, [
                'label' => 'Toute la structure',
                'required' => false
            ])
            ->add('sejour', EntityType::class, [
                'class' => Sejour::class,
                'mapped' => false,
                'choice_label' => 'intitule',
                'label' => 'Séjour'

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);

        $builder->get('structure')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();
                $structure = $form->getData();
                //On rend le champ séjour obligatoire uniquement si la case all est cochee
                $champSejourRequired = !$form->getParent()->get('all');
                $this->addSejoursFieldForStructureSelection($form->getParent(), $structure, $champSejourRequired);
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                // On crée le champ sejour en le laissant vide (champ utilisé pour le JavaScript)
                $this->addSejoursFieldForStructureSelection($form, null, true);
            }
        );
    }

    private function addSejoursFieldForStructureSelection(FormInterface $form, ?Structure $structure, bool $champSejourRequired)
    {
        $form->add('sejour', EntityType::class, [
            'class' => Sejour::class,
            'choice_label' => 'intitule',
            'required'   => $champSejourRequired,
            'label' => 'Séjour',
            'placeholder' => $structure ? 'Sélectionnez le séjour' : 'Sélectionnez d\'abord la structure',
            'choices' => $structure ? $structure->getSejoursOrganises() : [],
            'choice_filter' => 'isStatusVisible',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'structure' => null
        ]);
    }
}
