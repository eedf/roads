<?php

namespace App\Form;

use App\Entity\Sejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects de transports d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectsTransportsSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descriptionOrganisationTransport', CKEditorType::class, [
                'required' =>false,
                'label' => "L’organisation du transport des participant·e·s",
                'help_html' => true,
                'help' => "<p>L’organisation du transport des participant·e·s aller et retour :
                    <ul>
                        <li>Quels moyens de transport sont utilisés ?</li>
                        <li>Si collectif, qui est responsable de convoi ?</li>
                        <li>Le top départ est-il prêt ? </li>
                    </ul>
                </p>"
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
