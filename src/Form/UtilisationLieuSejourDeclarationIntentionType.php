<?php

namespace App\Form;

use App\Entity\UtilisationLieuSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie de l'utilisation d'un lieu de séjour (déclaration d'intention)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class UtilisationLieuSejourDeclarationIntentionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('zoneEnvisagee', TextType::class, [
                'label' => "Zone envisagée",
                'help' => "Préciser ici votre lieu de séjour ou la zone de recherche sur laquelle vous vous concentrez."
            ])
            ->add('estDefini', ChoiceType::class, [
                'choices' => [
                    "Déterminé" => true,
                    "A déterminer" => false
                ],
                'label' => "Choix du Lieu"
            ])
            ->add('lieu', LieuType::class, [
                'label' =>  false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UtilisationLieuSejour::class,
            'sejour' => null
        ]);
    }
}
