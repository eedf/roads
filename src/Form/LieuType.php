<?php

namespace App\Form;

use App\Entity\LieuSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un lieu de séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => "Nom du lieu",
            ])
            ->add('siteInternet', UrlType::class, [
                'required' => false,
                'label' => "Site internet",
            ])
            ->add('estEEDF', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'multiple' => false,
                'expanded' => true,
                'label' => "Le lieu est-il un lieu géré par les EEDF ?"
            ])
            ->add('proprietaireEstPersonnePhysique', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'multiple' => false,
                'expanded' => true,
                'label' => "Le.la propriétaire du lieu est-il.elle un particulier ?"
            ])
            ->add('coordonneesProprietaire', CoordonneesProprietaireType::class, [
                'label' => false
            ])
            ->add('conventionLieu', DocumentType::class, [
                'required' => false,
                'label' => "Insérer la convention de lieu",
            ])
            ->add('localisation', LocalisationType::class, [
                'required' => true,
                'label' => false,
                'error_bubbling' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LieuSejour::class,
            'sejour' => null
        ]);
    }
}
