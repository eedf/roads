<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'ajout du rôle de suivi pour toute la structure ou pour un séjour de la structure
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AjoutRoleSuiviStructureOrgaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('personne', PersonneSelectTextType::class, [
                'label' => "Sélection de la personne",
            ])
            ->add('all', CheckboxType::class, [
                'label' => 'Toute la structure',
                'required' => false
            ])
            ->add('sejour', EntityType::class, [
                'class' => Sejour::class,
                'choice_label' => 'intitule',
                'label' => 'Séjour',
                'choices' => $options['structure']->getSejoursOrganises(),
                'choice_filter' => 'isStatusVisible',

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "structure" => null
        ]);
    }
}
