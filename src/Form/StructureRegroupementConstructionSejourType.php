<?php

namespace App\Form;

use App\Entity\Equipe;
use App\Entity\Structure;
use App\Entity\StructureRegroupementSejour;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de sélection d'une structure de regroupement participant au séjour (construction de séjour)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class StructureRegroupementConstructionSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estEEDF', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'label' => "S'agit-il d'une structure EEDF ?",
                'multiple' => false,
                'expanded' => true
            ])
            ->add('nomStructureNonEEDF', TextType::class, [
                'label' => "Nom de la structure non EEDF concernée par ce regroupement"
            ])
            ->add('descriptionStructureNonEEDF', TextareaType::class, [
                'label' => "Description de la structure non EEDF concernée par ce regroupement",
                'help' => "Veuillez renseigner avec le format suivant : Association / nom de la tranche d'âge concernée / un contact"
            ])
            ->add('structureEEDF', EntityType::class, [
                'class' => Structure::class,
                'label' => "Nom de la structure EEDF concernée par ce regroupement",
                'choice_label' => function (Structure $structure) {
                    if ($structure->getStatut() == Structure::STATUT_STRUCTURE_RATTACHEE) {
                        return $structure->getNom() . " (" . $structure->getType() . " sous tutelle)";
                    } else {
                        return $structure->getNom() . " (" . $structure->getType() . ")";
                    }
                },
                'placeholder' => "Sélectionnez la structure dans la liste",
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.echelon IN (:array_echelons_utiles)')
                        ->andWhere('s.statut in (:statutsActifs)')
                        ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
                        ->setParameter('array_echelons_utiles', [Structure::ECHELON_STRUCTURE_LOCAL, Structure::ECHELON_STRUCTURE_REGIONAL])
                        ->orderBy('s.echelon, s.nom', 'ASC');
                },
            ])
            ->add(
                'unites',
                EntityType::class,
                [
                    'class' => Equipe::class,
                    'choice_label' => 'nom',
                    'choices' => [],
                    'label' => "Unites Participantes",
                    'help' => "Sélectionnez les unités participant au séjour pour cette structure",
                    'multiple' => true,
                    'expanded' => true,
                    'by_reference' => false,
                ]
            );

        $builder->get('structureEEDF')->addEventListener(
            FormEvents::POST_SET_DATA,

            function (FormEvent $event) {
                /** @var Structure|null $data */
                $structure = $event->getData();
                if (!$structure) {
                    return;
                }
                $form = $event->getForm();
                $this->addUnitesFieldForStructureSelection($form->getParent(), $structure);
            }
        );

        $builder->get('structureEEDF')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();
                $structure = $form->getData();
                $this->addUnitesFieldForStructureSelection($form->getParent(), $structure);
            }
        );
    }

    private function addUnitesFieldForStructureSelection(FormInterface $form, ?Structure $structure)
    {
        $form->add(
            'unites',
            EntityType::class,
            [
                'class' => Equipe::class,
                'choice_label' => 'nom',
                'choices' => $structure ? $structure->getEquipesUnitesActives() : [],
                'label' => "Unites Participantes",
                'help' => "Sélectionnez les unités participant au séjour pour cette structure",
                'multiple' => true,
                'expanded' => true,
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StructureRegroupementSejour::class,
        ]);
    }
}
