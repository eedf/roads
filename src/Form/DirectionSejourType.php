<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Repository\EquipeRepository;
use App\Repository\UniteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Formulaire de saisie du·de la direct·eur·rice du séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DirectionSejourType extends AbstractType
{

    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Sejour sejour */
        $sejour = $options['sejour'];
        $builder->add('directeur', PersonneSelectTextType::class, [
            'label' => "Sélection de la personne qui dirige le séjour",
            'help' => "Permet de sélectionner la directrice ou le directeur du séjour",
            'row_attr' => [
                'class' => "infos-personnes",
                'infos-personnes-url' => $this->router->generate('sejour_infos_personnes'),
                "sejour_id" => $sejour->getId(),
                "structure_id" => $sejour->getStructureOrganisatrice()->getId()
            ],
        ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
