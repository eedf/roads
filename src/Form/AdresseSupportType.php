<?php

namespace App\Form;

use App\Entity\ParametreRoads;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour saisir/modifier l'adresse de support
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AdresseSupportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('valeurParametre', EmailType::class,[
                'label'=>'Adresse e-mail vers qui les messages de support sont envoyés'
            ])
            ->add('save', SubmitType::class,[
                'label'=>'Enregistrer'
            ])
        ;
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParametreRoads::class,
        ]);
    }
}
