<?php

namespace App\Form;

use App\Entity\AspectAlimentaireSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects alimentaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectAlimentaireSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('choixOrganisation', CKEditorType::class, [
                'required' =>false,
                'label' => "Choix d'organisations alimentaires",
                'help' => "Décrire ici les éléments d'organisations liés à l'intendance, la cuisine, la traçabilité, gestion des régimes spéciaux, les producteurs locaux..."
            ])
            ->add('grilleMenus', DocumentType::class, [
                'required' =>false,
                'label' => "Insérer sa grille de menus",
                'help' => "Si votre grille de menus est prête vous pouvez l'insérer ici"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AspectAlimentaireSejour::class,
        ]);
    }
}
