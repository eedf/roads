<?php

namespace App\Form;

use App\Entity\AspectsParticipantsBesoinsSpecifiquesSejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Formulaire pour les participants à besoin spécifique d'un séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectsParticipantsBesoinsSpecifiquesSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreParticipantsHandicap',IntegerType::class,[
                'label' => "Nombre de mineur.e.s en situation de handicap",
                'help' => 'Nécessaire pour déclarer la fiche complémentaire sur TAM'
            ])
            ->add('descParticipantsBesoinsSpecifiques', CKEditorType::class, [
                'required' =>false,
                'label' => "Participant·e·s à besoins spécifiques",
                'help' => "Renseignez ici si des participant·e·s devront faire l’objet d’attentions particulières (nature du besoin et organisation mise en place pour y répondre). Par exemple: participation d’un enfant malentendant et appareillé, un·e responsable sait s’exprimer par langage des signes et la recharge de l’appareil est prévu·e chaque soir..."
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AspectsParticipantsBesoinsSpecifiquesSejour::class,
        ]);
    }
}
