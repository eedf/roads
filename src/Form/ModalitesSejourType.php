<?php

namespace App\Form;

use App\Entity\Sejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des modalités de séjour (construction de séjour)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ModalitesSejourType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sejourOuvert', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour ouvert",
                'help' => "Un séjour ouvert est un séjour sur lequel sont accueilli·e·s des participant·e·s ne participant pas à des activités à l'année et qui souvent propose une démarche de découverte du scoutisme"
            ])
            ->add('sejourItinerant', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour itinérant",
                'help' => "Un séjour itinérant est un séjour qui ne se vit pas sur un lieu unique, durant le séjour il y a déplacements des participant·e·s et des équipes sur plusieurs lieux."
            ])
            ->add('international', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour à l'international",
                'help' => "Un séjour à l'international est un séjour où les participant·e·s et l'équipe séjournent dans un autre pays que la France"
            ])
            ->add('sejourFranceAvecAccueilEtranger', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Séjour en France avec accueil d’un groupe étranger",
            ])
            ->add('coloApprenante', ChoiceType::class, [
                'choices' => [
                    "Oui" => true,
                    "Non" => false
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => "Colos apprenantes",
                'help_html' => true,
                'help' => "Séjour prenant en compte le cahier des charges du dispositif du même nom"
            ])
            ->add('dispositifAppui', ChoiceType::class, [
                'choices' => [
                    Sejour::DISPOSITIF_APPUI_CAMP_ACCOMPAGNE => Sejour::DISPOSITIF_APPUI_CAMP_ACCOMPAGNE,
                    Sejour::DISPOSITIF_APPUI_CAMP_BASE => Sejour::DISPOSITIF_APPUI_CAMP_BASE,
                    "Non" => "Non"
                ],
                'expanded' => true,
                'multiple' => false,
                'help_html' => true,
                'label' => "Dispositif d'appui à la réalisation du camp",
                'help' => "Est-ce que le séjour bénéficie d'un appui spécifique ?"
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
