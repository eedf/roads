<?php

namespace App\Form;

use App\Entity\EffectifsPrevisionnels;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des effectifs prévisionnels d'un séjour (déclaration d'intention)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class EffectifsPrevisionnelsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbLutins', IntegerType::class, [
                'label' => "Effectifs prévisionnels lutins et lutines (6-8 ans)",
                'empty_data' => 0,
                'help' => "combien d'enfants de l'unité sont attendus sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbLouveteaux', IntegerType::class, [
                'label' => "Effectifs prévisionnels louveteaux et louvettes (8-11 ans)",
                'empty_data' => 0,
                'help' => "combien d'enfants de l'unité sont attendus sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbEcles', IntegerType::class, [
                'label' => "Effectifs prévisionnels éclaireurs et éclaireuses (11-15 ans)",
                'empty_data' => 0,
                'help' => "combien d'enfants de l'unité sont attendus sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbAines', IntegerType::class, [
                'label' => "Effectifs prévisionnels aînés et aînées (15-18 ans)",
                'empty_data' => 0,
                'help' => "combien d'enfants de l'unité sont attendus sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbEncadrantsLutins', IntegerType::class, [
                'label' => "Effectifs prévisionnels encadrement lutins et lutines",
                'empty_data' => 0,
                'help' => "combien d'encadrant·e·s de l'unité sont attendu·e·s sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbEncadrantsLouveteaux', IntegerType::class, [
                'label' => "Effectifs prévisionnels encadrement louveteaux et louvettes",
                'empty_data' => 0,
                'help' => "combien d'encadrant·e·s de l'unité sont attendu·e·s sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbEncadrantsEcles', IntegerType::class, [
                'label' => "Effectifs prévisionnels encadrement éclaireurs et éclaireuses",
                'empty_data' => 0,
                'help' => "combien d'encadrant·e·s de l'unité sont attendu·e·s sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbEncadrantsAines', IntegerType::class, [
                'label' => "Effectifs prévisionnels encadrement aînés et aînée",
                'empty_data' => 0,
                'help' => "combien d'encadrant·e·s de l'unité sont attendu·e·s sur le séjour",
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('nbHandicap', IntegerType::class, [
                'label' => "Effectifs prévisionnels mineur·e·s en situation de handicap",
                'empty_data' => 0,
                'help' => "Nécessaire pour déclarer la fiche complémentaire sur TAM",
                'attr' => [
                    'min' => 0
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EffectifsPrevisionnels::class,
        ]);
    }
}
