<?php

namespace App\Form;

use App\Entity\Structure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie des informations de déclaration SDJES de la structure (Périmètres)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class InfosStructureFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroDeclarationInitiale', TextType::class, [
                'label' => "Numéro de déclaration initiale de la déclaration sur TAM (déclaration pour l'année à la SDJES)",
                'help' => "Pour être autorisé, un séjour doit être doublement déclaré à Jeunesse et Sport via le site TAM, et à l'association via cet outil",
            ])
            ->add('prenomOrganisateurDDCS', TextType::class, [
                'required' => false,
                'label' => "Prénom du·de la représentant·e de l'organisateur au regard de la fiche initiale",
                'help' => "Si différent du responsable de la structure"
            ])
            ->add('nomOrganisateurDDCS', TextType::class, [
                'required' => false,
                'label' => "Nom du·de la représentant·e de l'organisateur au regard de la fiche initiale",
                'help' => "Si différent du responsable de la structure"
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Structure::class,
        ]);
    }
}
