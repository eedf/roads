<?php

namespace App\Form;

use App\Entity\DemandeModerationCommentaire;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Formulaire de demande de modération d'un commentaire
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DemandeModerationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raison', CKEditorType::class, [
                'label' => 'Raison du signalement',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
                'help' => "Explicitez ici brièvement les raisons de votre demande de modération"
            ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Envoyer la demande'
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DemandeModerationCommentaire::class,
        ]);
    }
}
