<?php

namespace App\Form;

use App\Entity\Coordonnees;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour saisir les coordonnées du propriétaire d'un lieu de séjour
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class CoordonneesProprietaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => "Nom du·de la propriétaire",
                'required' => false
            ])
            ->add('prenom', TextType::class, [
                'label' => "Prenom du·de la propriétaire",
                'required' => false
            ])
            ->add('telMobile', PhoneNumberType::class, [
                'label' => 'Téléphone du·de la propriétaire',
                "default_region" => "FR",
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'preferred_country_choices' => ['FR'],
                'required' => false
            ])
            ->add('adresseMail', EmailType::class, [
                'label' => 'Email du·de la propriétaire',
                'required' => false
            ])
            ->add('adressePostale', AdressePostaleProprietaireType::class, [
                'label' => false,
                'help' => "Si différente de l'adresse du lieu",
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Coordonnees::class,
        ]);
    }
}
