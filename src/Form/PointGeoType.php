<?php

namespace App\Form;

use App\Form\DataTransformer\LatitudeLongitudeToPointTransformer;
use LongitudeOne\Spatial\PHP\Types\Geography\Point;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un point géogrpahique
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class PointGeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('latitude', HiddenType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'latitude',
                ]
            ])
            ->add('longitude', HiddenType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'longitude ',
                ]
            ])
            ->addModelTransformer(new LatitudeLongitudeToPointTransformer());
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
