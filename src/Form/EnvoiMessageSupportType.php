<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire d'un voi d'un message à l'adresse support
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class EnvoiMessageSupportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', null, [
                'label' => 'Nom',
                'required' => false
            ])
            ->add('prenom', null, [
                'label' => 'Prénom',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'e-mail',
                'required' => false
            ])
            ->add('numeroAdherentOuMatricule', NumeroAdherentType::class, [
                'label' => 'Saisissez votre numéro d\'adhérent.e ou votre matricule salarié',
                'required' => false
            ])
            ->add('rubrique', ChoiceType::class, [

                'choices'  => [
                    'Authentification/connexion' => 'Authentification/connexion',
                    'Profil utilisateur' => 'Profil utilisateur',
                    'Déclaration de séjour' => 'Déclaration de séjour',
                    'Recherche de lieux de séjour' => 'Recherche de lieux de séjour',
                    'Recherche de séjours' => 'Recherche de séjours',
                    'Notifications' => 'Notifications',
                    'Gestion de structure' => 'Gestion de structure',
                    'Validation Nationale de séjours' => 'Validation Nationale de séjours',
                    'Validation Internationale de séjours' => ' Validation Internationale de séjours',
                    'Autres' => 'Autres'

                ],
                'multiple' => false,
                'expanded' => false,
                'placeholder' => 'Sélectionnez le sujet',
                'label' => 'Mon message concerne'
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Contenu du message'
            ])
            ->add('envoyer', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
