<?php

namespace App\Form;

use App\Entity\UtilisationLieuSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie de l'utilisation d'un lieu de séjour (construction de séjour)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class UtilisationLieuSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Sejour $sejour */
        $sejour = $options['sejour'];
        $builder
            ->add('lieu', LieuType::class, [
                'label' =>  false,
            ]);
        if ($sejour->getSejourItinerant()) {
            $builder
                ->add('dateArrivee', DateType::class, [
                    'label' => "Date d'arrivée sur le lieu",
                    'widget' => 'single_text',
                    'html5' => false,
                    'format' => "dd/MM/yyyy",
                    'attr' => ['class' => 'form-control js-datepicker'],

                ])
                ->add('dateDepart', DateType::class, [
                    'label' => "Date de départ du lieu",
                    'widget' => 'single_text',
                    'html5' => false,
                    'format' => "dd/MM/yyyy",
                    'attr' => ['class' => 'form-control js-datepicker'],

                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UtilisationLieuSejour::class,
            'sejour' => null
        ]);
    }
}
