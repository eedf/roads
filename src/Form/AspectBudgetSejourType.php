<?php

namespace App\Form;

use App\Entity\AspectBudgetSejour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects budgétaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectBudgetSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prixMoyen', MoneyType::class, [
                'required' => false,
                'currency' => 'EUR',
                'label' => "Prix moyen du séjour",
                'help' => "Mettre le prix moyen demandé aux parents (en cas de plusieurs tarifs faire la moyenne des tarifs)"
            ])
            ->add('budgetPrevisionnel', DocumentType::class, [
                'required' => false,
                'label' => "Insérer son budget prévisionnel",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AspectBudgetSejour::class,
        ]);
    }
}
