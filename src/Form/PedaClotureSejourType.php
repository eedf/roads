<?php

namespace App\Form;

use App\Entity\Sejour;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie de bilan pédagogique du séjour (clôture)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class PedaClotureSejourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Sejour $sejour */
        $sejour = $options['sejour'];

        $builder
            ->add('bilanPeda', CKEditorType::class, [
                'required' => true,
                "label" => "Bilan du séjour au regard des critères choisis"
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);

        if ($sejour->getInternational() || $sejour->getSejourFranceAvecAccueilEtranger()) {
            $builder
                ->add('retourCampInternational', DocumentType::class, [
                    'required' => false,
                    'label' => "Insérer son bilan international",
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
            'sejour' => null
        ]);
    }
}
