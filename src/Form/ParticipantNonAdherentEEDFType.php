<?php

namespace App\Form;

use App\Entity\ParticipantNonAdherent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un·e participant·e non adhérent·e à un séjour sur une unité EEDF
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class ParticipantNonAdherentEEDFType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom du participant ou de la participante',
                'required' => true
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Prénom du participant ou de la participante',
                'required' => true
            ])
            ->add('age', IntegerType::class, [
                'label' => "Âge de l'enfant non adhérent",
                'required' => true
            ])
            ->add('commentaire', TextareaType::class, [
                'required' => true,
                'label' => "Commentaire",
                'help' => "Champ disponible pour renseigner une information sur ce·tte participant·e vous permettant de mieux l'identifier"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParticipantNonAdherent::class,
        ]);
    }
}
