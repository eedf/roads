<?php

namespace App\Form;

use App\Form\DataTransformer\NumAdhToPersonneTransformer;
use App\Repository\PersonneRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * Formulaire de saisie des adhérents et salariés avec auto complete
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class PersonneSelectTextType extends AbstractType
{
    private $personneRepository;
    private $router;

    public function __construct(PersonneRepository $personneRepository, RouterInterface $router)
    {
        $this->personneRepository = $personneRepository;
        $this->router = $router;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new NumAdhToPersonneTransformer(
            $this->personneRepository,
            $options['finder_callback']
        ));
    }
    public function getParent(): ?string
    {
        return TextType::class;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'invalid_message' => 'Personne non trouvé·e',
            'finder_callback' => function (PersonneRepository $personneRepository, string $numAdh) {
                return $personneRepository->findOneBy(['numeroAdherent' => $numAdh]);
            },
            'attr' => [
                'class' => 'js-personne-autocomplete',
                'data-autocomplete-url' => $this->router->generate('roads_utility_personnes')
            ],
            'placeholder' => 'numéro d\'adhérent·e, nom ou prénom'
        ]);
    }
}
