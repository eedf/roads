<?php

namespace App\Form;

use App\Entity\EvenementGrave;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un évenement grave sur une séjour (clôture)
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class EvenementGraveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('natureEvenement', ChoiceType::class,[
                'choices'=>[
                    EvenementGrave::NATURE_METEO=>EvenementGrave::NATURE_METEO,
                    EvenementGrave::NATURE_MEMBRE_EQUIPE_SANS_RENVOI=>EvenementGrave::NATURE_MEMBRE_EQUIPE_SANS_RENVOI,
                    EvenementGrave::NATURE_MEMBRE_EQUIPE_AVEC_RENVOI=>EvenementGrave::NATURE_MEMBRE_EQUIPE_AVEC_RENVOI,
                    EvenementGrave::NATURE_ACCIDENT_SANS_RENVOI=>EvenementGrave::NATURE_ACCIDENT_SANS_RENVOI,
                    EvenementGrave::NATURE_ACCIDENT_AVEC_RENVOI=>EvenementGrave::NATURE_ACCIDENT_AVEC_RENVOI,
                    EvenementGrave::NATURE_SANTE_SANS_RENVOI=>EvenementGrave::NATURE_SANTE_SANS_RENVOI,
                    EvenementGrave::NATURE_SANTE_AVEC_RENVOI=>EvenementGrave::NATURE_SANTE_AVEC_RENVOI,
                    EvenementGrave::NATURE_PARTICIPANT_SANS_RENVOI=>EvenementGrave::NATURE_PARTICIPANT_SANS_RENVOI,
                    EvenementGrave::NATURE_PARTICIPANT_AVEC_RENVOI=>EvenementGrave::NATURE_PARTICIPANT_AVEC_RENVOI
                ],
                'multiple' =>false,
                'expanded' =>true,
                'label' => "Nature de l'évenement"
            ])
            ->add('precisions', CKEditorType::class, [
                'required' =>true,
                'label' => "Précision"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EvenementGrave::class,
        ]);
    }
}
