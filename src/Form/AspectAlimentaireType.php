<?php

namespace App\Form;

use App\Entity\Sejour;
use App\Form\AspectAlimentaireSejourType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour les aspects alimentaires d'un séjour
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AspectAlimentaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('aspectAlimentaireSejour', AspectAlimentaireSejourType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejour::class,
        ]);
    }
}
