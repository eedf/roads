#!/bin/bash

set -x
GLOBAL_STATUS=0

echo "======== BEGIN `date` ========"

# Rotation des logs
logrotate logrotate.conf || GLOBAL_STATUS=1

# Synchronisation quotidienne avec Jéito
php bin/console roads:synchro-jeito --env=prod -vvv || GLOBAL_STATUS=1

# Envoi des rappels des notifications sur les séjours
php bin/console roads:envoi-rappels-notifications -vvv || GLOBAL_STATUS=1

# Envoi des notifications quotidiennes
php bin/console roads:envoi-notifications -vvv || GLOBAL_STATUS=1

# Mise à jours des états des séjours de Validé à En cours et de En cours à Terminé
php bin/console roads:maj-etats-sejours -vvv || GLOBAL_STATUS=1

echo "======== END `date` ========"

exit $GLOBAL_STATUS
